/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package unfi;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
 
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import org.apache.commons.lang.StringUtils;
 
/**
 *
 * @author US083274
 */
public class DrawPackLogo {
    public static void main(String args[]) throws Exception{
       // DrawPackLogo.drawAmazonPackLogo("C:\\Temp2012\\images\\41i7ruGu9IL._SX425_.jpg", 4);
        String PACK = System.getProperty("PACK", null);
        if(PACK==null){
            System.out.println("PLEASE ENTER HOW MANY PACKS LOGO YOU WANT ON THE TOP OF PRODUCT IMAE");
            BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
            PACK=buffer.readLine();
        }
         
 
        /**
         * Read a background image
         */
        BufferedImage bgImage = null;
        
        readImage("C:\\Google Drive\\Dropbox\\harrisburgstore\\product.png");
        if(bgImage==null)
            bgImage = readImage("C:\\Google Drive\\Dropbox\\harrisburgstore\\product.jpg");
        if(bgImage==null)
            bgImage = readImage("C:\\Google Drive\\Dropbox\\harrisburgstore\\product.jpeg");
       
        /**
         * Read a foreground image
         */
        BufferedImage fgImage = readImage("C:\\Google Drive\\Dropbox\\harrisburgstore\\Multipack logo\\"+PACK+"_pack.png");
 
        /**
         * Do the overlay of foreground image on background image
         */
        BufferedImage overlayedImage = overlayImages(bgImage, fgImage);
 
        /**
         * Write the overlayed image back to file
         */
        if (overlayedImage != null){
            writeImage(overlayedImage, "C:\\Google Drive\\Dropbox\\harrisburgstore\\overLayedImage.jpg", "JPG");
            System.out.println("Overlay Completed...");
        }else
            System.out.println("Problem With Overlay...");
 
    }
    public static void drawAmazonPackLogo(String localImage,int pkg_qty){
     System.out.println("inside drawAmazonPackLogo for amazon")   ;
     try{
         
   
         BufferedImage  bgImage = readImage(localImage);
        BufferedImage  fgImage = readImage("C:\\Temp2012\\images\\amazon_multipack\\"+pkg_qty+"pack.png");
        
          BufferedImage overlayedImage = overlayImagesForAmazon(bgImage, fgImage);
        if (overlayedImage != null){
            String overlayedFolder=StringUtils.substringBeforeLast(localImage, "\\");
            writeImage(overlayedImage, overlayedFolder+"\\overLayedImage.jpg", "JPG");
            System.out.println("Overlay Completed...");
        }else
            System.out.println("Problem With Overlay...");          
       }catch (Exception e2){e2.printStackTrace();}
    
    
}
  public static BufferedImage overlayImagesForAmazon(BufferedImage bgImage,
            BufferedImage fgImage) {
 
         BufferedImage finalImg = new BufferedImage(bgImage.getWidth(), bgImage.getHeight() ,bgImage.getType());
 
        /**Create a Graphics  from the background image**/
        Graphics2D g = finalImg.createGraphics();
        g.setColor(Color.WHITE);  
        g.fillRect(0, 0,bgImage.getWidth(), bgImage.getHeight());  
        /**Set Antialias Rendering**/
      //  g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
         //       RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, 
               RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        /**
         * Draw background image at location (0,0)
         * You can change the (x,y) value as required
         */
        g.drawImage(bgImage, 0, 0, null);
 
        /**
         * Draw foreground image at location (0,0)
         * Change (x,y) value as required.
         */
        int  packH,packW;
        if(bgImage.getWidth()>1000){
            packW=fgImage.getWidth();
            packH=fgImage.getHeight();
        }else{
            int factor=bgImage.getWidth()*100/1000;
            packW=fgImage.getWidth()*factor/100;
            packH=fgImage.getHeight()*factor/100;
            
        }
        g.drawImage(fgImage,bgImage.getWidth()-packW,0,packW,packH,null);
 
        g.dispose();
        return finalImg;
    }    
 
    /**
     * Method to overlay Images
     *
     * @param bgImage --> The background Image
     * @param fgImage --> The foreground Image
     * @return --> overlayed image (fgImage over bgImage)
     */
 
    public static BufferedImage overlayImages(BufferedImage bgImage,
            BufferedImage fgImage) {
 
        /**
         * Doing some preliminary validations.
         * Foreground image height cannot be greater than background image height.
         * Foreground image width cannot be greater than background image width.
         *
         * returning a null value if such condition exists.
         */
      /*  if (fgImage.getHeight() > bgImage.getHeight()
                || fgImage.getWidth() > fgImage.getWidth()) {
            JOptionPane.showMessageDialog(null,
                    "Foreground Image Is Bigger In One or Both Dimensions"
                            + "\nCannot proceed with overlay."
                            + "\n\n Please use smaller Image for foreground");
            return null;
        }*/
         BufferedImage finalImg = new BufferedImage(bgImage.getWidth(), bgImage.getHeight()+100,bgImage.getType());
 
        /**Create a Graphics  from the background image**/
        Graphics2D g = finalImg.createGraphics();
        g.setColor(Color.WHITE);  
        g.fillRect(0, 0,bgImage.getWidth(), bgImage.getHeight()+100);  
        /**Set Antialias Rendering**/
      //  g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
         //       RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, 
               RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        /**
         * Draw background image at location (0,0)
         * You can change the (x,y) value as required
         */
        g.drawImage(bgImage, 0, 100, null);
 
        /**
         * Draw foreground image at location (0,0)
         * Change (x,y) value as required.
         */
        g.drawImage(fgImage, 0, 0,bgImage.getWidth(),100,null);
 
        g.dispose();
        return finalImg;
    }
 
    /**
     * This method reads an image from the file
     * @param fileLocation -- > eg. "C:/testImage.jpg"
     * @return BufferedImage of the file read
     */
    public static BufferedImage readImage(String fileLocation) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(fileLocation));
        } catch (IOException e) {
            if(fileLocation.indexOf("product")<0)
                    e.printStackTrace();
        }
        return img;
    }
 
    /**
     * This method writes a buffered image to a file
     * @param img -- > BufferedImage
     * @param fileLocation --> e.g. "C:/testImage.jpg"
     * @param extension --> e.g. "jpg","gif","png"
     */
    public static void writeImage(BufferedImage img, String fileLocation,
            String extension) {
        try {
            BufferedImage bi = img;
            File outputfile = new File(fileLocation);
            ImageIO.write(bi, extension, outputfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /*
     * SANJAAL CORPS MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
     * THE SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED
     * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
     * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SANJAAL CORPS SHALL NOT BE LIABLE FOR
     * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
     * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
     *
     * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
     * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
     * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
     * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
     * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
     * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
     * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). SANJAAL CORPS
     * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
     * HIGH RISK ACTIVITIES.
     */
}
 