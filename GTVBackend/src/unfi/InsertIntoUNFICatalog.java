/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package unfi;
 
import amazon.product.CreateCatalogNew;
import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
 
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;
 
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import com.tajplaza.products.ListMatchingProductsSample;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import javax.net.ssl.HttpsURLConnection;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

 

/**
 *
 * @author us083274
 */
public class InsertIntoUNFICatalog {
    public static boolean COREMARK=false;
    public static boolean DOTFOODS=false;
    public static void main(String args[]) throws Exception{
        String id="25204";
        if(System.getProperty("SLEEP_TIME_ALL_PROXIES_USED_ONCE")!=null)
            RetrievePage.SLEEP_TIME_ALL_PROXIES_USED_ONCE =Integer.parseInt(System.getProperty("SLEEP_TIME_ALL_PROXIES_USED_ONCE"));
        if(System.getProperty("NUMBER_TIMES_PROXIES_BEFORE_SLEEP")!=null)
            RetrievePage.NUMBER_TIMES_PROXIES_BEFORE_SLEEP =Integer.parseInt(System.getProperty("NUMBER_TIMES_PROXIES_BEFORE_SLEEP"));        
        System.out.println("Value of NUMBER_TIMES_PROXIES_BEFORE_SLEEP is "+  RetrievePage.NUMBER_TIMES_PROXIES_BEFORE_SLEEP);
        loadUNFIProductsToTable();
        //loadCustomFields(id,null);
         
       // loadUNFIProductsToTable() ;
    }
    
    public static ArrayList<String>  itemCodesProcessedInThisSession=new ArrayList();
        public static void loadUNFIProductsToTable() throws Exception {
            RetrievePage.USE_PROXIES=true;
             Connection con = RetrievePage.getConnection();
             System.out.println("Inside loadUNFIProductsToTable  Processing 10000 rows");
             PreparedStatement selectStmt = con.prepareStatement(" select  distinct upc,item_code,brand,pack from hb_bc.unfi_input_catalog where item_code not in(select item_code from hb_bc.phi_hva_productfeed) and item_code not in(select item_code from hb_bc.unfi_catalog)   ");
            PreparedStatement insertStmt_Found=con.prepareStatement(" insert into hb_bc.unfi_catalog (item_code,  asin,upc, title, descr, brand, price,image_file,rank) values (?,?,?,?,?,?,?,?,?) ");
             PreparedStatement insertStmt_NF=con.prepareStatement(" insert into hb_bc.unfi_catalog (item_code,  upc,asin) values (?,?,'NA') ");
              if(COREMARK){
                  selectStmt = con.prepareStatement("select distinct upc,item_code,brand,pack from hb_bc.wholesaler_input_catalog where item_code not in (select item_code from hb_bc.wholesaler_catalog where item_code is not null) ");
                  insertStmt_Found=con.prepareStatement(" insert into hb_bc.wholesaler_catalog (item_code,  asin,upc, title, descr, brand, price,image_file,rank,category) values (?,?,?,?,?,?,?,?,?,?) ");
                   insertStmt_NF=con.prepareStatement(" insert into hb_bc.wholesaler_catalog (item_code,  upc,asin) values (?,?,'NA') ");
              }
              if(DOTFOODS){
                  String PARTICULAR_DOT_SQL="  ";
                  String sql=" (select  'dotfoods_product_each',upc,item_code,'' brand,a.packs_in_case pack, d.pack_size from  harrisburgstore.dotfoods_product_each  a, harrisburgstore.dotfoods_instock d   where   a.item_code=d.Dot_Item and d.dot_item not in (select item_code from hb_bc.dotfoods_catalog)   "+PARTICULAR_DOT_SQL+"  )  union   (select 'dotfoods_product_case', upc,item_code,'' brand,a.packs_in_case pack, d.pack_size from  harrisburgstore.dotfoods_product_case  a, harrisburgstore.dotfoods_instock d    where   a.item_code=d.Dot_Item and d.dot_item not in (select item_code from hb_bc.dotfoods_catalog)   "+PARTICULAR_DOT_SQL+"  ) limit 5000";
                  selectStmt = con.prepareStatement(sql) ; 
                  insertStmt_Found=con.prepareStatement(" insert into hb_bc.dotfoods_catalog (item_code,  asin,upc, title, descr, brand, price,image_file,rank,category) values (?,?,?,?,?,?,?,?,?,?) ");
                   insertStmt_NF=con.prepareStatement(" insert into hb_bc.dotfoods_catalog (item_code,  upc,asin) values (?,?,'NA') ");
              }              

             System.out.println("Executing query : "+selectStmt);
            ResultSet rs=selectStmt.executeQuery();
            ResultSet rs2;
            int itemCount=0;
            while(rs.next()){
                  String upc=rs.getString("upc");

                  String unfi_brand=rs.getString("brand");
                  unfi_brand=WordUtils.capitalize(unfi_brand);
                  String item_code=rs.getString("item_code");
                  if(itemCodesProcessedInThisSession.contains(item_code))
                      continue;
                  itemCodesProcessedInThisSession.add(item_code);                  
                  String pack=rs.getString("pack");
                upc=upc.replaceAll("UPC:","");
                upc=upc.replaceAll("upc:","");
                upc=upc.replaceAll("-","");
                upc=upc.replaceAll("'","");
                upc=upc.replaceAll(" ","");
                upc=upc.trim();
                if(upc.length()>13)
                             upc=upc.substring(1,14);
                MatchThePrice.JAVASCRIPTNONO=true;
                MatchThePrice.DOWNLOAD_IMAGE=false;
                if(DOTFOODS)
                      MatchThePrice.DOWNLOAD_IMAGE=false;
                System.out.println("Processing UPC:"+upc);
 
                String title="NA";
                String manu="NA";
                String asin="NA";
                 float bestprice=(float)0;
                //ArrayList items=CreateCatalog.getJustUPCList(upc);    
                 CreateCatalogNew.UNFI_FIND_CASE_ASIN=true;
                CreateCatalogNew.MIN_REVIEWS_REQ=0;
               CreateCatalog.ERROR_MOVE_TO_NEXT_PAGE=true;
               
               ArrayList <Product> arr=CreateCatalogNew.getUPCDetailsNewMWS(upc);   
               if(arr.size()==0){
                   insertStmt_NF.setString(1, item_code); 
                   insertStmt_NF.setString(2, upc); 
                   insertStmt_NF.executeUpdate();                   
                   continue;
               }
                   
               Product pr=arr.get(0);
                          
              // Product pr=CreateCatalogNew.getUPCDetailsOld(upc);                 
                //if(items.size()>0)
                if(pr.getAsin()!=null && pr.getAsin().indexOf("B")>=0)
                {
                
                    asin= pr.getAsin();
                   // HashMap hm=MatchThePrice.getProductDetails(asin, false,false,false);
                    HashMap hm=null;
                ArrayList<HashMap> matchingProducts = null;
                matchingProducts=ListMatchingProductsSample.getMatchingProducts(asin);
                for(int i=0;i<matchingProducts.size();i++){
                HashMap tmpMap=matchingProducts.get(i);
                if(tmpMap.get("asin").equals(asin)){
                    System.out.println("Matching asin found throgh MWS API");
                    hm=tmpMap;
                    //Reason for parsing through here is to see if there is an exisitng multi pack quantity exists.
                    }
                 }                    
                    if(hm==null){
                       insertStmt_NF.setString(1, item_code); 
                       insertStmt_NF.setString(2, upc); 
                       insertStmt_NF.executeUpdate();                        
                        continue;
                    }
                     //pw.storeProduct1(p, con, "amazon_catalog_un_hb");
                     
                    title=(String)(hm.get("title")!=null?hm.get("title"):"");
                    title=title.replaceAll("Amazon.com:", "");
                    if(DOTFOODS){
                        if(Integer.valueOf(pack)>1)
                            title=pack+" PACKS : " + title;
                        
                    }
                    manu=(String)(hm.get("manu")!=null?hm.get("manu"):"");
                    if(manu.length()==0)
                         manu=(String)(hm.get("Brand")!=null?hm.get("Brand"):"");                    
                    String des=(String)(hm.get("des")!=null?hm.get("des"):"");
                    String bb1=(String)(hm.get("bb1")!=null?hm.get("bb1"):"");
                    String bb2=(String)(hm.get("bb2")!=null?hm.get("bb2"):"");
                    String bb3=(String)(hm.get("bb3")!=null?hm.get("bb3"):"");
                    String bb4=(String)(hm.get("bb4")!=null?hm.get("bb4"):"");
                     bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
                    
                        String imageurl=(String)(hm.get("imageurl")!=null?hm.get("imageurl"):"");
                    Integer rank=(Integer)(hm.get("rank")!=null?hm.get("rank"):99999);
                    String category=(String)(hm.get("category")!=null?hm.get("category"):"Misc");
                     
                    String descr=des+"<ul>";
                    if(bb1!=null && bb1.toUpperCase().equals("NULL") ==false)
                        descr+="<li>"+bb1+"</li>";
                    if(bb2!=null&& bb1.toUpperCase().equals("NULL") ==false)
                        descr+="<li>"+bb2+"</li>";
                    if(bb3!=null&& bb1.toUpperCase().equals("NULL") ==false)
                        descr+="<li>"+bb3+"</li>";
                    if(bb4!=null&& bb1.toUpperCase().equals("NULL") ==false)
                        descr+="<li>"+bb4+"</li>";
                    
                    descr+="</ul>";
                                
                    if(COREMARK){
                        try{
                            if(Integer.valueOf(pack)>1)
                             descr="<div id=pack_be style='display:none'>Packs In Case:"+pack+"</div>"+descr;
                        }catch (Exception e2){}
                        
                    }  
                      
                    if(DOTFOODS){
                         
                        String pack_size=rs.getString("pack_size");
                        String pack_info="";
                        try{
                            if(Integer.valueOf(pack)>1  &&  pack_size.length()>0){
                                  pack_info="<div id=pack_be style='display:none'><table id='case_info'><tbody><tr><td> Packs in one case  </td><td> : <span id='packs_in_case'>"+pack+"</span> </td></tr><tr><td> Size information </td><td>: <span id='size_info'>"+pack_size+"</span> </td></tr></tbody></table></div>";
                                
                            }else  if(Integer.valueOf(pack)>1){
                                pack_info="<div id=pack_be style='display:none'>Packs In Case:"+pack+"</div>"+descr;
                            }  else if (pack_size.length()>0){
                                 pack_info="<div id=pack_be style='display:none'><table id='case_info'><tbody><tr><td> Size information </td><td>: <span id='size_info'>"+pack_size+"</span> </td></tr></tbody></table></div>";
                            }
                            descr= pack_info+descr;
                            
                        }catch (Exception e2){e2.printStackTrace();}
                        
                    }                      
                    descr=StringUtils.substring(descr, 0,9999);

                    
                    insertStmt_Found.setString(1, item_code);
                    insertStmt_Found.setString(2, asin);
                    insertStmt_Found.setString(3, upc);
                    insertStmt_Found.setString(4, title);
                    insertStmt_Found.setString(5, descr);
                    insertStmt_Found.setString(6, manu);
                    insertStmt_Found.setString(7, "0");
                    insertStmt_Found.setString(8, imageurl);
                    insertStmt_Found.setInt(9, rank);
                    if(COREMARK || DOTFOODS){
                        insertStmt_Found.setString(10, category);
                    }
                        
                    
                    insertStmt_Found.executeUpdate();


                   
                }else{
                   insertStmt_NF.setString(1, item_code); 
                   insertStmt_NF.setString(2, upc); 
                   insertStmt_NF.executeUpdate();
                }
                             Product p=new Product();
                    p.setItem_Code(item_code);
                    p.setTitle(title);
     
                    p.setBrand(manu);                    
                    p.setAsin(asin);
                    p.setUpc(upc);
                    p.setBestprice(bestprice);
                   p.setWHOLESALERNAME("UN_HB");
                    ProcessWorkingCatalog pw=new ProcessWorkingCatalog();
                  //  pw.storeProduct3(p,  "amazon_catalog_un_hb");
                 
            }
            
           con.close();        
    }
   public static String updateProduct1(String productid, JSONObject jSONObject) {
        String webPage = "https://harrisburgstore.com/api/v2/products/"+productid+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
             
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }            
   public static void loadCustomFields(String productid, JSONObject jSONObject)  throws Exception{
                WebClient webClient1 ;
               webClient1=new WebClient( );
        
            
                System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "debug");
                //webClient1.getOptions().setJ
                webClient1.getOptions().setThrowExceptionOnScriptError(false);
                 webClient1.getOptions().setJavaScriptEnabled(true);
                 webClient1.getOptions().setCssEnabled(false);
                 webClient1.waitForBackgroundJavaScript(15000);
                 
                 
                HtmlPage page = webClient1.getPage("https://store-0p5zzjir.mybigcommerce.com/admin/");
                //System.out.println(page.asText());
                ((HtmlTextInput) page.getElementById("user_email")).setText("mprabagar80@gmail.com");
                ((HtmlPasswordInput) page.getElementById("user_password")).setText("Dummy123");
                
                 System.out.println("Signing In");
                  page=((HtmlSubmitInput)page.getByXPath("//input[@type='submit']").get(0)).click();
                  webClient1.getOptions().setThrowExceptionOnScriptError(true);
                 page= webClient1.getPage("https://store-0p5zzjir.mybigcommerce.com/admin/index.php?ToDo=editProduct&productId=25204&returnUrl=index.php%3FToDo%3DviewProducts&current_tab=custom-fields");
                 System.out.println("page xml:"+page.asXml())   ;
                 
                 page.executeJavaScript("product.attributes.custom_fields[0].value='JUNK';");
                 page=((HtmlButton )page.getByXPath("//button[@id='save-product-button']").get(0)).click();
                 
                  
                  /*
                
                
                
                
                
        String webPage = "https://store-0p5zzjir.mybigcommerce.com/api/v2/products/"+productid+"/custom_fields";
        String token = "807c960064a4ef27b5516c7e53c842c1f41a1f7e";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpsURLConnection  httpCon = (HttpsURLConnection ) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("POST");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
             
//            String strJString = jSONObject.toString();
           // System.out.println("Request : "+strJString);
            out.write("{sn=C02G8416DRJM}");
            out.flush();
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        */
    }               
}
