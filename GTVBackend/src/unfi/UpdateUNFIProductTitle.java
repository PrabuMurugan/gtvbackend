package unfi;

import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.StringWebResponse;
import com.gargoylesoftware.htmlunit.TopLevelWindow;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HTMLParser;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;



public class UpdateUNFIProductTitle {
    private static String QUERY = "select  product_id,code,product_images, name,pack,size,category, description,prod_info,category_string,asin"
            + "  from harrisburgstore.mp_update_titles      ";

    private static String attrCatQuery = "select searchStr from hb_bc.attr_cat  ";
     public static ArrayList arrAttrCatSearchStr=new ArrayList();
    
    public static void main(String [] args) throws Exception {
         
        if(args.length ==0) {
            System.out.println("Invalid argument. Usage : bigcommerce.UpdateUNFIProductTitle <no-of-records [-1 : All]>\n ");
            //System.exit(1);
        } else if(!args[0].equalsIgnoreCase("-1")) {
            QUERY = QUERY + " limit " + args[0];
        }
         
        System.out.println("Inside NEW main of UpdateUNFIProductTitle");
        long iStart = System.currentTimeMillis();
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap<String, JSONObject > map = new HashMap<String, JSONObject>();
                
        try {

            connection =  RetrievePage.getConnection();
              ps=connection.prepareStatement("drop table if exists hb_bc.exported_products_from_hb_needs_update ");
            ps.executeUpdate();
 
              ps=connection.prepareStatement("create table hb_bc.exported_products_from_hb_needs_update as select * from hb_bc.exported_products_from_hb where (code like 'DT%' or code like 'UN%') order by case when description not like '%pack_be%' then 1 else 99 end limit 1000");
            ps.executeUpdate();
 



              ps=connection.prepareStatement("truncate table   harrisburgstore.mp_update_titles ");
            ps.executeUpdate();
            
            ps=connection.prepareStatement("insert into harrisburgstore.mp_update_titles select p.product_id,code,product_images,p.name,pack,size,category,p.description,prod_info,category_string,u.asin, date_modified,length(p.description) len   from hb_bc.exported_products_from_hb_needs_update p, hb_bc.unfi_input_catalog ui ,  hb_bc.original_cost u where code= u.sku and code = concat('UN_', ui.item_code)      ");
             ps.executeUpdate();
            ps=connection.prepareStatement("insert into harrisburgstore.mp_update_titles select p.product_id,code,product_images,p.name,packs_in_case,'' size,'' category,p.description,'' prod_info,category_string,u.asin, date_modified,length(p.description) len   from hb_bc.exported_products_from_hb_needs_update p, harrisburgstore.dotfoods_product_each d ,  hb_bc.original_cost u where code= u.sku and code = concat('DT_', d.item_code)  ");
             ps.executeUpdate();
             ps=connection.prepareStatement("insert into harrisburgstore.mp_update_titles select p.product_id,code,product_images,p.name,packs_in_case,'' size,'' category,p.description,'' prod_info,category_string,u.asin, date_modified,length(p.description) len   from hb_bc.exported_products_from_hb_needs_update p, harrisburgstore.dotfoods_product_case d ,  hb_bc.original_cost u where code= u.sku and code = concat('DT_', d.item_code)  ");
              ps.executeUpdate();
            //ps=connection.prepareStatement("insert into harrisburgstore.mp_update_titles select p.product_id,code,product_images,p.name,pack,size,category,p.description,prod_info,category_string,u.asin, date_modified,length(p.description) len   from hb_bc.exported_products_from_hb p, hb_bc.unfi_input_catalog ui ,  hb_bc.original_cost u where code= u.sku and code = concat('UN_', ui.item_code)   and ui.prod_info like '%m%'    ");
             

            
            
            System.out.println("Going to execute Query : " + QUERY);
            ps = connection.prepareStatement(QUERY);
            rs = ps.executeQuery();
            
            PreparedStatement psAtrCat=connection.prepareStatement(attrCatQuery);
            ResultSet rsAttrCat=psAtrCat.executeQuery();
           
            while(rsAttrCat.next()){
                arrAttrCatSearchStr.add(rsAttrCat.getString(1));
            }
            System.out.println("After executing Query");
            while(rs.next()) {
                String strProductId = rs.getString("product_id");
               // String strName = rs.getString("name");
               // String origName=strName;
                String strCode = rs.getString("code");
                String pack = rs.getString("pack");
                if(pack==null)
                    pack="";
                String size = rs.getString("size");
                if(size==null)
                    size="";
                
                String category = rs.getString("category");
                String asin = rs.getString("asin");
                String prod_info = rs.getString("prod_info");
                if(prod_info==null)
                    prod_info="";
                String description=rs.getString("description");
                String name=rs.getString("name");                
                  
                
                if(description==null)
                    continue;
               // String description = getDescription(asin);
                String origDesc=description;
                String category_string = rs.getString("category_string");
               System.out.println("Processing SKU:"+strCode);
               if(category.toUpperCase().indexOf("BULK")>=0)
                       pack="1";                
               
                
              //  if(strName.indexOf("Pack :")>=0)
                //    strName=StringUtils.substringAfterLast(strName, "Pack :");
               // if(strName.indexOf("  ")>=0)
                 //  strName=strName.replaceAll("  ", " ");
                pack=pack.trim();
                size=size.trim();
              //  if( Integer.valueOf(pack)>1 && size.equals("#")==false && size.equals("LB")==false){
              //      strName=pack +" Pack :"+strName;
             //   }
                 
                 
                String packStr="<div id=pack_be style='display:none'><table id='case_info'><tbody><tr><td> Packs in one case  </td><td> : <span id='packs_in_case'>PACK</span> </td></tr><tr><td> Size information </td><td>: <span id='size_info'>SIZE</span> </td></tr></tbody></table></div>";
                if(strCode.indexOf("DT_")>=0){
                    packStr=packStr.replaceAll("<tr><td> Size information </td><td>: <span id='size_info'>SIZE</span> </td></tr>", "");
                }
                packStr=packStr.replaceAll("PACK", pack);
                packStr=packStr.replaceAll("SIZE", size);
                     //Remove non ascii Characters
                    StringBuilder result = new StringBuilder();
                  //  for(char val : strName.toCharArray()) {
                  //      if(val < 192) result.append(val);
                  //  }        
                  //  strName=result.toString();
                
                boolean updateNeeded=false;
               // if(strName.equals(origName)==false)
               //     updateNeeded=true;
                String existingPackStr=StringUtils.substringBetween(description,"<div id=\"pack_be\" style=\"display: none;\">", "</div>");
 

                if(description.indexOf("pack_be")<0 || existingPackStr.equals(packStr)==false){
                    if(size.equals("#")==false && size.equals("LB")==false){
                       try{
                       description=getDescriptionFromHB(strProductId);
                   }catch (Exception e3){e3.printStackTrace();}                        
                        updateNeeded=true;
                        description=description.replaceAll("pack_be", "pack_beNA");
                        description+=packStr;

                    }
                }
                if(   description.indexOf("mpattr")<0)
                // if(  true|| description.indexOf("mpattr")<0)
                {
                  //   description = getDescriptionFromAmazon(asin);
                       try{
                       description=getDescriptionFromHB(strProductId);
                   }catch (Exception e3){e3.printStackTrace();}
                    
                        updateNeeded=true;
                         if(StringUtils.countMatches(category_string,";")>1){
                             category_string=StringUtils.substringBefore(category_string, ";");
                         }

                         if(StringUtils.countMatches(category_string,"/")>=2){
                             category_string=StringUtils.split(category_string,"/")[2];
                         }
                          String origCatString=category_string;
                             category_string=category_string.replaceAll("[^A-Za-z]", "");

                         String attrStr="",displayAttrStr="";
                         if(prod_info.indexOf("K")>=0){
                             attrStr+=category_string+"KOSHER";
                             displayAttrStr+="<li>Kosher</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"KOSHER")==false){
                                 insertIntoattr_cat("Kosher",origCatString,category_string+"KOSHER");
                             }

                         }

                         if(prod_info.indexOf("O1")>=0){
                             attrStr+=","+category_string+"ORGANIC";
                             displayAttrStr+="<li>100% Organic (USDA Certified)</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"ORGANIC")==false){
                                 insertIntoattr_cat("100% Organic (USDA Certified)",origCatString,category_string+"ORGANIC");
                             }                         

                         }

                         if(prod_info.indexOf("O2")>=0){
                             attrStr+=","+category_string+"NINETYORGANIC";
                             displayAttrStr+="<li>90% Organic</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"NINETYORGANIC")==false){
                                 insertIntoattr_cat("90% Organic",origCatString,category_string+"NINETYORGANIC");
                             }                         

                         }

                         if(prod_info.indexOf("c")>=0){
                             attrStr+=","+category_string+"LOWCARB";
                             displayAttrStr+="<li>Low Carb</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"LOWCARB")==false){
                                 insertIntoattr_cat("Low Carb",origCatString,category_string+"LOWCARB");
                             }                         

                         }

                         if(prod_info.indexOf("d")>=0){
                             attrStr+=","+category_string+"DAIREFREE";
                             displayAttrStr+="<li>Dairy Free</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"DAIREFREE")==false){
                                 insertIntoattr_cat("Dairy Free",origCatString,category_string+"DAIREFREE");
                             }                         

                         }


                         if(prod_info.indexOf("g")>=0){
                             attrStr+=","+category_string+"GLUTENFREE";
                             displayAttrStr+="<li>Gluten Free</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"GLUTENFREE")==false){
                                 insertIntoattr_cat("Gluten Free",origCatString,category_string+"GLUTENFREE");
                             }                         

                         }

                         if(prod_info.indexOf("m")>=0){
                             attrStr+=","+category_string+"NONGMO";
                             displayAttrStr+="<li>Non-GMO Project Verified</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"NONGMO")==false){
                                 insertIntoattr_cat("Non GMO",origCatString,category_string+"NONGMO");
                             }                         

                         }

                         if(prod_info.indexOf("l")>=0){
                             attrStr+=","+category_string+"LOWSODIUM";
                             displayAttrStr+="<li>Low Sodium</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"LOWSODIUM")==false){
                                 insertIntoattr_cat("Low Sodium",origCatString,category_string+"LOWSODIUM");
                             }                         

                         }

                         if(prod_info.indexOf("f")>=0){
                             attrStr+=","+category_string+"FAIRTRADE";
                             displayAttrStr+="<li>Fair Trade</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"FREETRADE")==false){
                                 insertIntoattr_cat("Fair Trade",origCatString,category_string+"FREETRADE");
                             }                         


                         }

                         if(prod_info.indexOf("v")>=0){
                                attrStr+=","+category_string+"VEGAN";
                             displayAttrStr+="<li>Vegan</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"VEGAN")==false){
                                 insertIntoattr_cat("Vegan",origCatString,category_string+"VEGAN");
                             }                         

                         }

                         if(prod_info.indexOf("w")>=0){
                             attrStr+=","+category_string+"WHEATFREE";
                             displayAttrStr+="<li>Wheat Free</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"WHEATFREE")==false){
                                 insertIntoattr_cat("Wheat Free",origCatString,category_string+"WHEATFREE");
                             }                       


                         }

                         if(prod_info.indexOf("y")>=0){
                             attrStr+=","+category_string+"YEASTFREE";
                             displayAttrStr+="<li>Yeast Free</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"YEASTFREE")==false){
                                 insertIntoattr_cat("Yeast Free",origCatString,category_string+"YEASTFREE");
                             }                         

                         }
                         if(prod_info.indexOf("s")>=0){
                             attrStr+=","+category_string+"LOWSODLOWSALT";
                             displayAttrStr+="<li>Low Sodium, Low Salt</li>";
                             if(arrAttrCatSearchStr.contains(category_string+"LOWSODLOWSALT")==false){
                                 insertIntoattr_cat("Low Sodium, Low Salt",origCatString,category_string+"LOWSODLOWSALT");
                             }                         

                         }                     
                         /*Old Mess, can be removed later*/
                         // description=getDescriptionFromHB(strProductId);
                             if(description.indexOf("<p id=\"mpattr\">&nbsp;</p>")>=0)
                                 description=StringUtils.substringAfter(description,"<p id=\"mpattr\">&nbsp;</p>");
                            if(description.indexOf("<p id='mpattr'>&nbsp;</p>")>=0)
                                 description=StringUtils.substringAfter(description,"<p id='mpattr;>&nbsp;</p>");
                            if(description.indexOf("<p id=mpattr>&nbsp;</p>")>=0)
                                 description=StringUtils.substringAfter(description,"<p id=mpattr>&nbsp;</p>");

                             String prefixDescription="<ul  id=displayAttr>"+displayAttrStr+"</ul>";           
                            prefixDescription+="<div  id=attrListNew style='display:none'>"+attrStr+" </div>";    
                            prefixDescription+="<p id=mpattr>&nbsp;</p>";
                             description=prefixDescription+description;                         
 
                          
                       
                }else if(updateNeeded==false){
                    //Description already contains mpattr. nothing to replace.
                    description=origDesc;
                }
                result = new StringBuilder();
                for(char val : description.toCharArray()) {
                    if(val < 192) result.append(val);
                }        
                description=result.toString();                 
                if(origDesc.equals(description)==false)
                    updateNeeded=true;
                
                if(  updateNeeded){
                    System.out.println(strProductId + "\t" + strCode + "\t"   );
                    JSONObject jsono = new JSONObject();
                   
                   // jsono.put("name", strName);
                    if(origDesc.equals(description)==false)
                        jsono.put("description", description);
                    if(strCode.indexOf("DT_")>=0 && name.indexOf("Pack")<0 ){
                        name=pack +"  PACKS :"+name ;
                        jsono.put("name", name);
                    }
                    updateProduct(strProductId, jsono);                    
                }
                
               
                 
            }
            
               
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                //if(rs != null && !rs.isClosed()) rs.close();
                //if(ps != null && !ps.isClosed()) ps.close();
                if(connection != null && !connection.isClosed()) connection.close();
            } catch (SQLException ex) {
                            }
        }
        
     
    }
    public static String getDescriptionFromAmazon(String asin){
         
        HashMap hm=MatchThePrice.getProductDetails(asin, true, true, true);
        String des=(String)(hm.get("des")!=null?hm.get("des"):null);
        String aplusProductDescription=(String)(hm.get("aplusProductDescription")!=null?hm.get("aplusProductDescription"):null);
        String bb1=(String)(hm.get("bb1")!=null?hm.get("bb1"):null);
        String bb2=(String)(hm.get("bb2")!=null?hm.get("bb2"):null);
        String bb3=(String)(hm.get("bb3")!=null?hm.get("bb3"):null);
        String bb4=(String)(hm.get("bb4")!=null?hm.get("bb4"):null);


            String descr=des+"<ul>";
            if(bb1!=null && bb1.toUpperCase().equals("NULL") ==false)
            descr+="<li>"+bb1+"</li>";
            if(bb2!=null&& bb1.toUpperCase().equals("NULL") ==false)
            descr+="<li>"+bb2+"</li>";
            if(bb3!=null&& bb1.toUpperCase().equals("NULL") ==false)
            descr+="<li>"+bb3+"</li>";
            if(bb4!=null&& bb1.toUpperCase().equals("NULL") ==false)
            descr+="<li>"+bb4+"</li>";

            descr+="</ul>";  
            if(aplusProductDescription!=null){
                 descr+=aplusProductDescription; 
            }
        return descr;
        
    }
     public static String updateProduct(String productid, JSONObject jSONObject) {
            String webPage = "https://www.harrisburgstore.com/api/v2/products/"+productid+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
         StringBuilder sb1 = new StringBuilder();
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            httpCon.setConnectTimeout(300000);
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
              
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
           
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
           
        } catch(Exception exception) {
            exception.printStackTrace();
        }
         return  new String(sb1);
    }    

    private static void insertIntoattr_cat(String attr,String cat,String searchStr) throws Exception{
         Connection connection= RetrievePage.getConnection();
        arrAttrCatSearchStr.add(searchStr);
         PreparedStatement ps=connection.prepareStatement("insert into hb_bc.attr_cat (attr,cat,searchStr) values(?,?,?)");
         ps.setString(1, attr);
         ps.setString(2, cat);
         ps.setString(3, searchStr);
         ps.executeUpdate();
         connection.close();
         
    }

    private static String getDescriptionFromHB(String id) throws Exception {
        String webPage = "https://www.harrisburgstore.com/api/v2/products/"+id+".json";
           String str=getJSONString(webPage);
           JSONObject pr=JSONObject.fromObject(str); 
           String desc="";
           if(pr.containsKey("description"))
               desc=pr.getString("description");
           return desc;
           
    }
public static String getJSONString(String strAPI) throws Exception {
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
         
        String authString = username + ":" + token;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
      
        HttpURLConnection httpCon = (HttpURLConnection) (new URL(strAPI)).openConnection();
        httpCon.setRequestProperty("Content-Type", "application/json");
        httpCon.setRequestProperty("Accept", "application/json");
        httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
        httpCon.setDoOutput(true);
        httpCon.setDoInput(true);
        httpCon.setRequestMethod("GET");

        InputStream responseIS = httpCon.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
        String line ="";
        StringBuilder sb = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        return new String(sb);
    }     
   
}
