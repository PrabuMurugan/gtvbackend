/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package unfi;

import bombayplaza.pricematch.RetrievePage;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
 
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author US083274
 */
public class DrawPackLogoForAllImages {
    public static String ORIG_IMAGE_FOLDER="C:\\Temp2012\\images\\";
   public static String OVERLAYED_IMAGE_FOLDER="C:\\Temp2012\\images\\overlayed\\";
    public static void main(String args[]) throws Exception{

 
         final File folder = new File(ORIG_IMAGE_FOLDER);
        processDir(folder);
        
        /**
         * Read a background image
         */
 
    }
public static void processDir(final File folder) throws Exception {
    Connection con = RetrievePage.getConnection();
    for (final File fileEntry : folder.listFiles()) {
        if (fileEntry.isDirectory()) {
            //processDir(fileEntry);
        } else {
            //if(fileEntry.getName().indexOf("71vhgKDSNXL._SL1500_")>=0)
              //  cropImage(fileEntry.getName());
             processFile(fileEntry.getName(),con);
                
        }
    }
    con.close();
}
public static void cropImage(String file){
    BufferedImage inImage = null;
        
        inImage=readImage(ORIG_IMAGE_FOLDER+file);
        BufferedImage fgImage=inImage.getSubimage(0, 0, inImage.getWidth(),100);
        BufferedImage bgImage=inImage.getSubimage(0, 200, inImage.getWidth(),inImage.getHeight()-200);
        BufferedImage overlayedImage = overlayImages(bgImage, fgImage);
 
        /**
         * Write the overlayed image back to file
         */
        if (overlayedImage != null){
            writeImage(overlayedImage, OVERLAYED_IMAGE_FOLDER+file, "JPG");
            new File(ORIG_IMAGE_FOLDER+file).delete();
            System.out.println("Overlay Completed...");
        }else
            System.out.println("Problem With Overlay...");
          
}
public static boolean COREMARK=false;
public static void processFile(String file,Connection con) throws Exception{
    System.out.println("Processing file:"+file);
      String COREMARKStr = System.getProperty("COREMARK", "false");    
      if(COREMARKStr.equals("true")){
          System.out.println("Running for Coremark products");
            COREMARK=true;    
      }
          


    PreparedStatement selectStmt = con.prepareStatement(" select ui.item_code,ui.pack,ui.size,catg_desc from hb_bc.unfi_catalog u, hb_bc.unfi_input_catalog ui where u.item_code=ui.item_code"
            + " and u.image_file=? ");
    if(COREMARK){
        selectStmt = con.prepareStatement("select ui.item_code,ui.pack,ui.size,catg_desc from hb_bc.wholesaler_catalog u, hb_bc.wholesaler_input_catalog ui where u.item_code=ui.item_code"
                 + " and u.image_file=? ");
    }
     selectStmt.setString(1, ORIG_IMAGE_FOLDER+file);
     
    ResultSet rs=selectStmt.executeQuery();
    String PACK = "1";
    String SIZE = "1";
    String catg_desc="";
    String item_code=null;
    if(rs.next()){
        
        SIZE=rs.getString("size");
        if(SIZE==null)
            SIZE="";
        SIZE=SIZE.trim();
        item_code=rs.getString("item_code");
          catg_desc=rs.getString("catg_desc");
          PACK=rs.getString("pack");
          
          try{
              if(PACK.trim().equals("1") && SIZE.indexOf("/")>=0 && SIZE.toUpperCase().indexOf("SET")<0)
                  PACK=StringUtils.substringBefore(SIZE, "/");
                  
                  
             }catch (Exception e3){e3.printStackTrace();}
        
    }
    System.out.println("Processing : item_code"+item_code+", PACK:"+PACK+" file name :"+file);
     
        BufferedImage bgImage = null;
        
        bgImage=readImage(ORIG_IMAGE_FOLDER+file);
 
       
        /**
         * Read a foreground image
         */
        if(PACK.toUpperCase().equals("PACK"))
        {
            return;
        }
        if(SIZE.trim().equals("#") || catg_desc.toUpperCase().indexOf("BULK")>=0)
            PACK="1";
        
        BufferedImage fgImage = readImage("C:\\Google Drive\\Dropbox\\harrisburgstore\\Multipack logo\\"+PACK+"_pack.jpg");
        if(fgImage==null){
            System.out.println(" MULTI PACK LOGO DOES NOT EXISTS FOR "+ PACK +" AT C:\\Temp2012\\images\\multi pack logo\\"+PACK+"_pack.png");
            return;
        }
 
        /**
         * Do the overlay of foreground image on background image
         */
        BufferedImage overlayedImage = overlayImages(bgImage, fgImage);
 
        /**
         * Write the overlayed image back to file
         */
        if (overlayedImage != null){
            writeImage(overlayedImage, OVERLAYED_IMAGE_FOLDER+file, "JPG");
            new File(ORIG_IMAGE_FOLDER+file).delete();
            System.out.println("Overlay Completed...");
        }else
            System.out.println("Problem With Overlay...");
    
    
}

    /**
     * Method to overlay Images
     *
     * @param bgImage --> The background Image
     * @param fgImage --> The foreground Image
     * @return --> overlayed image (fgImage over bgImage)
     */
    public static BufferedImage overlayImages(BufferedImage bgImage,
            BufferedImage fgImage) {
 
        /**
         * Doing some preliminary validations.
         * Foreground image height cannot be greater than background image height.
         * Foreground image width cannot be greater than background image width.
         *
         * returning a null value if such condition exists.
         */
      /*  if (fgImage.getHeight() > bgImage.getHeight()
                || fgImage.getWidth() > fgImage.getWidth()) {
            JOptionPane.showMessageDialog(null,
                    "Foreground Image Is Bigger In One or Both Dimensions"
                            + "\nCannot proceed with overlay."
                            + "\n\n Please use smaller Image for foreground");
            return null;
        }*/
         BufferedImage finalImg = new BufferedImage(bgImage.getWidth(), bgImage.getHeight()+100,bgImage.getType());
        // BufferedImage finalImg = new BufferedImage(bgImage.getWidth(), bgImage.getHeight(),bgImage.getType());
 
        /**Create a Graphics  from the background image**/
        Graphics2D g = finalImg.createGraphics();
        g.setColor(Color.WHITE);  
        g.fillRect(0, 0,bgImage.getWidth(), bgImage.getHeight()+100);  
 
        //g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, 
          //     RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        /**
         * Draw background image at location (0,0)
         * You can change the (x,y) value as required
         */
        //FIRST DRAW THE BACKGROUND IMAGE
        g.drawImage(bgImage, 0, 100, null);
 
        /**
         * Draw foreground image at location (0,0)
         * Change (x,y) value as required.
         */
        g.drawImage(fgImage, 0, 0,bgImage.getWidth(),100,null);
 
        g.dispose();
        return finalImg;
    }
 
    /**
     * This method reads an image from the file
     * @param fileLocation -- > eg. "C:/testImage.jpg"
     * @return BufferedImage of the file read
     */
    public static BufferedImage readImage(String fileLocation) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(fileLocation));
        } catch (IOException e) {
            if(fileLocation.indexOf("product")<0 && fileLocation.indexOf("Multipack")<0)
                    e.printStackTrace();
        }
        return img;
    }
 
    /**
     * This method writes a buffered image to a file
     * @param img -- > BufferedImage
     * @param fileLocation --> e.g. "C:/testImage.jpg"
     * @param extension --> e.g. "jpg","gif","png"
     */
    public static void writeImage(BufferedImage img, String fileLocation,
            String extension) {
        try {
            BufferedImage bi = img;
            File outputfile = new File(fileLocation);
            ImageIO.write(bi, extension, outputfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /*
     * SANJAAL CORPS MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
     * THE SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED
     * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
     * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SANJAAL CORPS SHALL NOT BE LIABLE FOR
     * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
     * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
     *
     * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
     * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
     * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
     * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
     * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
     * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
     * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). SANJAAL CORPS
     * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
     * HIGH RISK ACTIVITIES.
     */
}
 