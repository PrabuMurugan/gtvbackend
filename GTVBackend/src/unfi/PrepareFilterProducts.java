/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package unfi;

import bombayplaza.pricematch.RetrievePage;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
 

/**
 *
 * @author US083274
 */
public class PrepareFilterProducts {
    public static void main(String[] args) throws Exception{
        //get  jsonQry from request parameter below and pass.
        String jsonQry="{\"Attribute\"%3A[\"Gluten Free\"%2C\"Kosher\"]%2C\"Brand\"%3A[\"Beige\"%2C\"Green\"]%2C\"Category\"%3A[\"Pasta\"%2C\"Green\"]%2C\"Type\"%3A\"Attributes\"}";
        
        PrepareFilterProducts f=new PrepareFilterProducts();
       // System.out.println("f.getSizeSet(): "+f.getSizeSet());
       // System.out.println("f.getColorSet(): "+f.getColorSet());
       //f.filterProductsByQry(jsonQry);
        
        
        f.prepare_mp_grocery_fields();
        f.update_non_gmo();
       //f.prepare_option_products();
       // f.putAsininPeachTree();
       
        
    }

         
     public void prepare_option_products() throws Exception{
         System.out.println("Inside prepare_option_products");
                 
             Connection con=RetrievePage.getConnection("hb_bc");
            PreparedStatement pst = null;
            ResultSet rs = null;
            pst=con.prepareStatement("truncate table mp_option_products");
            pst.executeUpdate();  
            pst=con.prepareStatement("insert into  mp_option_products   select product_id,ov.label,ov.option_value_id  from              product_skus ps, product_sku_options pso ,option_values ov                      where                      ps.id = pso.product_sku_id             and  pso.option_value_id=ov.option_value_id                       and ps.inventory_level>0");
            pst.executeUpdate();  
            
            con.close();
     }
        public void update_non_gmo() throws Exception{
            StringBuffer st=new StringBuffer();
            Connection con=RetrievePage.getConnection("hb_bc");
            PreparedStatement pst = null;
            PreparedStatement pst2 = null;
            ResultSet rs = null; 
            //First update the product_id
            pst=con.prepareStatement(" select product from hb_bc.nongmo_from_unfi ");
             pst2=con.prepareStatement(" update  mp_grocery_fields set gmo_free=true   where unfi_item_code like ? ");

            rs=pst.executeQuery();
            System.out.println("Updating GMO Free");
           while( rs.next()){
               String item_code=rs.getString(1);
               
                
               pst2.setString(1, "%"+item_code+"%");
  
                System.out.println("updating unfi_item_code:"+item_code + " setting  gmo free to true");
                pst2.executeUpdate();
                
           }            
            if(!con.isClosed()) con.close();
        }
        
   public void prepare_mp_grocery_fields() throws Exception{
       
            StringBuffer st=new StringBuffer();
            Connection con=RetrievePage.getConnection("hb_bc");
            PreparedStatement pst = null;
            PreparedStatement pst2 = null;
            ResultSet rs = null;
          // update rank
 
           
            pst=con.prepareStatement("truncate table mp_grocery_fields");
            pst.executeUpdate();
           
            pst=con.prepareStatement("delete  from  unfi_input_catalog  where item_code='Product'");
            pst.executeUpdate();

            pst=con.prepareStatement("delete from unfi_hb_product where sku='UN_Product'");
            pst.executeUpdate();
            pst=con.prepareStatement("delete from unfi_hb_product where sku='UN_item_code'");
            pst.executeUpdate();

            
            String insert_sql="insert into mp_grocery_fields (product_id,unfi_item_code,cat_desc,rank)  select 1,item_code, replace(replace(catg_Desc,'\"',''),',',''),999999  from  unfi_input_catalog  where concat('UN_',item_code)  in  (select sku from hb_bc.unfi_hb_product)";
             pst=con.prepareStatement(insert_sql);
            pst.executeUpdate();
            
            //First update the product_id
            //pst=con.prepareStatement(" select distinct sku,p.id, b.name  from unfi_hb_product p, product_brands pb, brands b where  p.id = pb.product_id and replace(pb.resource,'/brands/','')  = b.id  and sku like 'UN_%'");
            pst=con.prepareStatement(" select distinct code,p.product_id, brand as 'name'  from exported_products_from_hb p where code like 'UN_%'");
             pst2=con.prepareStatement(" update mp_grocery_fields set product_id=?, brand_desc=? where unfi_item_code=? ");

            rs=pst.executeQuery();
            System.out.println("Updating-t brand");
           while( rs.next()){
               String sku=rs.getString(1);
               String product_id=rs.getString(2);
               String brand_name=rs.getString(3);
               if(sku.indexOf("UN_")<0)
                   continue;
               String unfi_item_code=sku.replaceAll("UN_", "").replaceAll("DUP1","").replaceAll("DUP2","");
               pst2.setString(1, product_id);
               if(brand_name.length()>40)
                   brand_name=StringUtils.substring(brand_name, 0,40);
               pst2.setString(2, brand_name.replaceAll("\"", "").replaceAll("\'","").replaceAll(",", ""));
                pst2.setString(3, unfi_item_code);
                System.out.println("updating unfi_item_code:"+unfi_item_code + " setting product_id :"+product_id+ " setting brand_name :"+brand_name);
                pst2.executeUpdate();
                
           }
          // update rank
          /* System.out.println("Updating rank in mp_grocery_fields ");
            pst=con.prepareStatement(" select item_code,rank from unfi_catalog where rank is not null  and concat('UN_',item_code) in (select sku from unfi_hb_product)");
             pst2=con.prepareStatement(" update mp_grocery_fields set rank=?  where unfi_item_code=? ");

            rs=pst.executeQuery();
            
           while( rs.next()){
               String unfi_item_code=rs.getString(1);
               String rank=rs.getString(2);
                pst2.setString(1, rank);
                pst2.setString(2, unfi_item_code);
                pst2.executeUpdate();
                System.out.println("updating rank, unfi_item_code:"+unfi_item_code + " setting rank:"+rank);
           }           */
          //  pst=con.prepareStatement("select seller_sku ,item_name from existing_all_inventory where open_date not like '%open_date%'  and  str_to_date(substring(open_date,1,10),'%Y-%m-%d') < date_sub(curdate(), interval 30 day)  and seller_sku  not in ( select sellersku from orders where   purchaseDate > date_sub(curdate(), interval 30 day)  ) and fulfillment_channel='DEFAULT' ");
            pst=con.prepareStatement(" select  item_code from unfi_input_catalog where prod_info like '%O1%' and item_code in (select unfi_item_code from mp_grocery_fields)");
             pst2=con.prepareStatement(" update mp_grocery_fields set 100_organic=true where unfi_item_code=? ");

            rs=pst.executeQuery();
            System.out.println("Updating 100_organic flag");
           while( rs.next()){
               String unfi_item_code=rs.getString(1);
                pst2.setString(1, unfi_item_code);
                pst2.executeUpdate();
                System.out.println("updating unfi_item_code:"+unfi_item_code);
           }
           
            pst=con.prepareStatement(" select  item_code from unfi_input_catalog where prod_info like '%O2%' and item_code in (select unfi_item_code from mp_grocery_fields)");
            pst2=con.prepareStatement(" update mp_grocery_fields set 95_Organic=true where unfi_item_code=? ");

            rs=pst.executeQuery();
            System.out.println("Updating 95_Organic flag");
           while(rs.next()){
               String unfi_item_code=rs.getString(1);
                pst2.setString(1, unfi_item_code);
                pst2.executeUpdate();
                System.out.println("updating unfi_item_code:"+unfi_item_code);
           }
           //Lets update
           
            pst=con.prepareStatement(" select  item_code from unfi_input_catalog where prod_info like '%K%' and item_code in (select unfi_item_code from mp_grocery_fields)");
            pst2=con.prepareStatement(" update mp_grocery_fields set Kosher=true where unfi_item_code=? ");

            rs=pst.executeQuery();
            System.out.println("Updating Kosher flag");
           while(rs.next()){
               String unfi_item_code=rs.getString(1);
                pst2.setString(1, unfi_item_code);
                pst2.executeUpdate();
                System.out.println("updating unfi_item_code:"+unfi_item_code);
           }
           //Lets update
            pst=con.prepareStatement(" select  item_code from unfi_input_catalog where prod_info like '%d%' and item_code in (select unfi_item_code from mp_grocery_fields)");
            pst2=con.prepareStatement(" update mp_grocery_fields set dairy_free=true where unfi_item_code=? ");

            rs=pst.executeQuery();
            System.out.println("Updating dairy_free flag");
           while(rs.next()){
               String unfi_item_code=rs.getString(1);
                pst2.setString(1, unfi_item_code);
                pst2.executeUpdate();
                System.out.println("updating unfi_item_code:"+unfi_item_code);
           }
           
            pst=con.prepareStatement(" select  item_code from unfi_input_catalog where prod_info like '%g%' and item_code in (select unfi_item_code from mp_grocery_fields)");
            pst2=con.prepareStatement(" update mp_grocery_fields set gluten_free=true where unfi_item_code=? ");

            rs=pst.executeQuery();
            System.out.println("Updating gluten_free flag");
           while(rs.next()){
               String unfi_item_code=rs.getString(1);
                pst2.setString(1, unfi_item_code);
                pst2.executeUpdate();
                System.out.println("updating unfi_item_code:"+unfi_item_code);
           }
           
            pst=con.prepareStatement(" select  item_code from unfi_input_catalog where prod_info like '%l%' and item_code in (select unfi_item_code from mp_grocery_fields)");
            pst2=con.prepareStatement(" update mp_grocery_fields set low_sodium=true where unfi_item_code=? ");

            rs=pst.executeQuery();
            System.out.println("Updating low_sodium flag");
           while(rs.next()){
               String unfi_item_code=rs.getString(1);
                pst2.setString(1, unfi_item_code);
                pst2.executeUpdate();
                System.out.println("updating unfi_item_code:"+unfi_item_code);
           }
           
            pst=con.prepareStatement(" select  item_code from unfi_input_catalog where prod_info like '%f%' and item_code in (select unfi_item_code from mp_grocery_fields)");
            pst2=con.prepareStatement(" update mp_grocery_fields set fair_trade=true where unfi_item_code=? ");

            rs=pst.executeQuery();
            System.out.println("Updating fair_trade flag");
           while( rs.next()){
               String unfi_item_code=rs.getString(1);
                pst2.setString(1, unfi_item_code);
                pst2.executeUpdate();
                System.out.println("updating unfi_item_code:"+unfi_item_code);
           }
            pst=con.prepareStatement(" select  item_code from unfi_input_catalog where prod_info like '%v%' and item_code in (select unfi_item_code from mp_grocery_fields)");
            pst2=con.prepareStatement(" update mp_grocery_fields set vegan=true where unfi_item_code=? ");

            rs=pst.executeQuery();
            System.out.println("Updating vegan flag");
           while( rs.next()){
               String unfi_item_code=rs.getString(1);
                pst2.setString(1, unfi_item_code);
                pst2.executeUpdate();
                System.out.println("updating unfi_item_code:"+unfi_item_code);
           }
           
            pst=con.prepareStatement(" select  item_code from unfi_input_catalog where prod_info like '%w%' and item_code in (select unfi_item_code from mp_grocery_fields)");
            pst2=con.prepareStatement(" update mp_grocery_fields set wheat_free=true where unfi_item_code=? ");

            rs=pst.executeQuery();
            System.out.println("Updating wheat_free flag");
           while( rs.next()){
               String unfi_item_code=rs.getString(1);
                pst2.setString(1, unfi_item_code);
                pst2.executeUpdate();
                System.out.println("updating unfi_item_code:"+unfi_item_code);
           }
            pst=con.prepareStatement(" select  item_code from unfi_input_catalog where prod_info like '%y%' and item_code in (select unfi_item_code from mp_grocery_fields)");
            pst2=con.prepareStatement(" update mp_grocery_fields set yeast_free=true where unfi_item_code=? ");

            rs=pst.executeQuery();
            System.out.println("Updating yeast_free flag");
           while( rs.next()){
               String unfi_item_code=rs.getString(1);
                pst2.setString(1, unfi_item_code);
                pst2.executeUpdate();
                System.out.println("updating unfi_item_code:"+unfi_item_code);
           }
            pst=con.prepareStatement(" select  item_code from unfi_input_catalog where prod_info like '%s%' and item_code in (select unfi_item_code from mp_grocery_fields)");
            pst2=con.prepareStatement(" update mp_grocery_fields set low_sodium_low_salt_reduced_sodium=true where unfi_item_code=? ");

            rs=pst.executeQuery();
            System.out.println("Updating low_sodium_low_salt_reduced_sodium flag");
           while(rs.next()){
               String unfi_item_code=rs.getString(1);
                pst2.setString(1, unfi_item_code);
                pst2.executeUpdate();
                System.out.println("updating unfi_item_code:"+unfi_item_code);
           }
           
           con.close();        
        
    }      
}
