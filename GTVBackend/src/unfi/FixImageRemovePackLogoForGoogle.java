/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package unfi;

import bombayplaza.pricematch.RetrievePage;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import net.sf.json.JSONObject;
import net.sf.json.JSONArray;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import javax.imageio.ImageIO;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import samsclub.CreateHBProduct;


/**
 *
 * @author prabu
 */
public class FixImageRemovePackLogoForGoogle {

        public static void main(String args[]) throws Exception {
        // Product  p = getUPCDetails("B00H9H56QA");
        //console(p);
        fixImageRemovePackLogoForGoogle();

    }

    public static void fixImageRemovePackLogoForGoogle() throws Exception {
        Connection con = RetrievePage.getConnection();
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username = "prabu";
        String authString = username + ":" + token;
         
        PreparedStatement pst = null, pst2 = null;
        ResultSet rs = null, rs2 = null;
        String sort_order = "";
        sort_order = String.valueOf(System.getProperty("sort_order", ""));

       //String sql =  "select product_id,code,product_images from hb_bc.exported_products_from_hb p, hb_bc.unfi_input_catalog ui where code = concat('UN_', item_code)    and Size != '#' and Category not like '%BULK%' and Pack != 1  ";
        String sql =  "select product_id,code,product_images from hb_bc.exported_products_from_hb p, hb_bc.unfi_input_catalog ui where code = concat('UN_', item_code) "
                + " union   select product_id,code,product_images from hb_bc.exported_products_from_hb p where code like 'CM_%'  ";
        pst = con.prepareStatement(sql);
        rs = pst.executeQuery();
        int count = 0;
        
        while (rs.next()) {
            

            try{
                
           
            Integer id = rs.getInt("product_id");
            String sku = rs.getString("code");
            String product_image_url = rs.getString("product_images");
            if(product_image_url==null || product_image_url.trim().length()==0)
                continue;
            System.out.println("Processign count:" + count++ +" sku:"+sku);
             removePackLogo( id,sku, product_image_url,con);
            
              }catch (Exception e2){e2.printStackTrace();}
        }
        if (!con.isClosed()) {
            con.close();
        }
    }
    public static void removePackLogo(Integer product_id,String sku,String product_image_url,Connection con) throws Exception{

            BufferedImage image =null;
            URL url_image =new URL(product_image_url.replace("%", "%25"));
            image = ImageIO.read(url_image);
            
            int pack_logo_height=100;
  
            if( image.getHeight()-pack_logo_height<10){
                System.out.println("sku:"+sku + "does not have minimum image height of "+pack_logo_height);
                 return ;
            }
            
                
            BufferedImage finalImg =  image.getSubimage(0,pack_logo_height, image.getWidth(), image.getHeight()-pack_logo_height);
            String image_file_name=StringUtils.substringAfterLast(product_image_url, "/") ;
            image_file_name=URLDecoder.decode(image_file_name);
            String portion1=StringUtils.substringBeforeLast(image_file_name,".");
            String portion2=StringUtils.substringAfterLast(image_file_name,".");
            image_file_name=portion1+"-nopack."+portion2;
            if(new File("C:\\apache-tomcat-7-prod\\webapps\\HBImages\\"+image_file_name).exists())
                return;
            System.out.println("Creating file : "+"C:\\apache-tomcat-7-prod\\webapps\\HBImages\\"+image_file_name);
             ImageIO.write(finalImg, "jpg",new File("C:\\apache-tomcat-7-prod\\webapps\\HBImages\\"+image_file_name)); 
               String imageurl="http://98.235.197.242/HBImages/"+image_file_name;
                if(imageurl!=null && imageurl.length()>10 && imageurl.indexOf("51m1gdQJW2L")<0){
               
                  String uploadImage = CreateHBProduct.uploadImage(product_id,imageurl,true);
                  PreparedStatement pst2=con.prepareStatement("delete from hb_bc.hb_put_more_images where code=?");
                  pst2.setString(1, sku);
                  pst2.executeUpdate();;
                }               
    }
 
}
