package unfi;

import bombayplaza.pricematch.RetrievePage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;


public class UpdateGoogleErrorsTitles {
    //All descriptions before replacing them with title are stored at google_errors_before_fix
    private static String QUERY = "select   product_id,code,name,description  from hb_bc.exported_products_from_hb p  where code in (select   replace(sku,'_fixed_round1','')  from harrisburgstore.google_errors where error_type='Automatic item disapprovals due to policy violation') limit 10000";
    
    
    public static void main(String [] args) {
        
        if(args.length ==0) {
            System.out.println("Invalid argument. Usage : bigcommerce.UpdateUNFIProductTitle <no-of-records [-1 : All]>\n ");
            //System.exit(1);
        } else if(!args[0].equalsIgnoreCase("-1")) {
            QUERY = QUERY + " limit " + args[0];
        }
        System.out.println("Inside main of UpdateUNFIProductTitle");
        long iStart = System.currentTimeMillis();
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap<String, JSONObject > map = new HashMap<String, JSONObject>();
                
        try {

            connection =  RetrievePage.getConnection();
            System.out.println("Going to execute Query : " + QUERY);
            ps = connection.prepareStatement(QUERY);
            rs = ps.executeQuery();
            System.out.println("After executing Query");
            while(rs.next()) {
                String strProductId = rs.getString("product_id");
                String strName = rs.getString("name");
               
                String strCode = rs.getString("code");
                String strDesc= rs.getString("description");
                System.out.println(strProductId + "\t" + strCode + "\t" + strName + "\t" + strName);
                JSONObject jsono = new JSONObject();
                jsono.put("name", strName);
                jsono.put("description", strName);
               
                 updateProduct(strProductId, jsono);
            }
            
               
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                //if(rs != null && !rs.isClosed()) rs.close();
                //if(ps != null && !ps.isClosed()) ps.close();
                if(connection != null && !connection.isClosed()) connection.close();
            } catch (SQLException ex) {
                            }
        }
        
     
    }
    
     public static String updateProduct(String productid, JSONObject jSONObject) {
            String webPage = "https://www.harrisburgstore.com/api/v2/products/"+productid+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            httpCon.setConnectTimeout(300000);
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
              
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }    
   
}
