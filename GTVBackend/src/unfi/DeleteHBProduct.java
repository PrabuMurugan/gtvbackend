/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package unfi;

import amazon.product.CreateCatalogNew;
import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
 
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;
 
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.net.ssl.HttpsURLConnection;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

/**
 *
 * @author prabu
 */
public class DeleteHBProduct {
    public static void main (String args[]) throws Exception{
        String PRODUCTION=System.getProperty("PRODUCTION","false");
        String QUERY=System.getProperty("QUERY","");
        Connection con=RetrievePage.getConnection();
        
        String query="select product_id,code,product_upc_ean from hb_bc.exported_products_from_hb e,hb_bc.phi_hva_productfeed p where e.code = concat('UN_',p.item_code) and e.name != p.product_name";
        if(PRODUCTION!=null && PRODUCTION.equals("true") && QUERY!=null &&  QUERY.length()>0){
            query=QUERY;
        }
        System.out.println("PRODUCTION 1:"+PRODUCTION+"QUERY RUNNING :"+query);        
       //PreparedStatement ps= con.prepareStatement("select p.product_id,p.code,p.name,pf.product_name from hb_bc.exported_products_from_hb p, hb_bc.phi_hva_productfeed pf where replace(p.code,'UN_','') = pf.item_code   and p.name!=pf.product_name ");
       //PreparedStatement ps= con.prepareStatement("select p.product_id,p.product_url,p.code from harrisburgstore.dotfoods_product_each d, hb_bc.exported_products_from_hb p where  concat('DT_',item_code)= p.code and additional_column2='1-1 EACH' and packs_in_case!=1 ");
       // PreparedStatement ps= con.prepareStatement("select product_id,code,concat('UN_',trim(leading '0' from replace(code ,'UN_',''))) from hb_bc.exported_products_from_hb where code like 'UN%' and replace(code ,'UN_','') !=trim(leading '0' from replace(code ,'UN_','')) ");        
        // PreparedStatement ps= con.prepareStatement("select product_id,code from hb_bc.exported_products_from_hb where  code like '%VAR%' ");        
         //PreparedStatement ps= con.prepareStatement("select product_id,code from hb_bc.hva_catalog h, hb_bc.exported_products_from_hb  p where concat('UN_',item_code)=p.code and  stock_level > 0 and code like 'UN_%' and  item_code not in(select item_code from hb_bc.phi_hva_productfeed)  and cast(minimum_order_qty as decimal)>0");        
        //Delete all those old o/s products
        PreparedStatement ps= con.prepareStatement(query);                
        //PreparedStatement ps= con.prepareStatement("select product_id,code from hb_bc.exported_products_from_hb where  (code like 'UN%'   or code like 'CM%') and price <2  ");                
        //Wrong DT pkg_qty
        //PreparedStatement ps= con.prepareStatement("select product_id,code from hb_bc.exported_products_from_hb where code in(select concat('UN_',item_code) from hb_bc.hva_catalog where minimum_order_qty = 3) and stock_level > 0");
       // PreparedStatement ps= con.prepareStatement("select product_id,code from hb_bc.exported_products_from_hb where title like '%condom%'");
        
       ResultSet rs=ps.executeQuery();
       while(rs.next()){
           System.out.println("Going to delete :"+rs.getString("code"));
           deleteProduct(rs.getString("product_id"));;
       }
                
        
    }
     public static String deleteProduct(String productid ) {
        String webPage = "https://harrisburgstore.com/api/v2/products/"+productid+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("DELETE");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
             
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }
}
