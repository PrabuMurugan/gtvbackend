/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package unfi;

import bombayplaza.catalogcreation.CamelCamelCamel;
import bombayplaza.pricematch.RetrievePage;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import net.sf.json.JSONObject;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.HashMap;
import org.apache.commons.lang.StringUtils;
 
/**
 *
 * @author prabu
 */
public class UpdateUNFIUnavailableProducts {
   public static void main(String args[])   throws Exception {
       Connection con=RetrievePage.getConnection();
       try{
        processUpdateUNFIUnavailableProducts(con);    
       }catch (Exception e){
         if(e.getMessage().indexOf("max_allowed_packet")>=0 ){
             try{
                 System.out.println("Excced maximum package, resettimg it");
                 PreparedStatement pst=con.prepareStatement("SET GLOBAL max_allowed_packet=16777216");
                 pst.executeUpdate();                     
                 processUpdateUNFIUnavailableProducts(con);               
             }catch (Exception e2){e2.printStackTrace();}
         }   else{
             e.printStackTrace();
         }        
       }
       
   
         
 } 
  public static void processUpdateUNFIUnavailableProducts(Connection con) throws Exception{
     
        PreparedStatement pst = null,pst2=null;
        ResultSet rs = null,rs2=null;
        String sql=RetrievePage.getWebSql("112");
       // String sql=RetrievePage.getWebSql("123"); // For costco unavailable products
       // sql="select product_id,code,stock_level,product_not_visible from hb_bc.exported_products_from_hb where stock_level > 0 and product_not_visible = 1";
         System.out.println("Executing query# 112 to find unfi unavailable items :"+sql);
         PreparedStatement psUpdate=con.prepareStatement("update hb_bc.exported_products_from_hb  set stock_level=? where code=?");
        pst=con.prepareStatement(sql);        
        //pst=con.prepareStatement("select  product_id,code from  hb_bc.exported_products_from_hb p  where code like 'UN_%' and cast(stock_level as decimal)=0");        
        rs=pst.executeQuery();
        
 
        int count=0;
          int amazonVisitCounter=0;
        while(rs.next()){
            
            
            String product_id=rs.getString(1);
            String code=rs.getString(2);
            //if(code.indexOf("PACK")>=0)
//                continue;
            int changeCount=0;
            String quantity=rs.getString(3);
                JSONObject jSONObject = new JSONObject();
                boolean is_visible=true;
                // if(code.indexOf("AMA") >=0 &&   Integer.valueOf(quantity)<3)
                //    is_visible=false;
                jSONObject.put("is_visible", is_visible); 
                System.out.println("Setting inventory level "+quantity+" for product_id:"+product_id +" , sku:"+code);
                jSONObject.put("inventory_level", quantity);
               
                if(Integer.valueOf(quantity)>0)
                    is_visible=true;
                else
                    is_visible=true;
                jSONObject.put("is_visible", is_visible);
                try{
                    BigCommerce.updateProduct(product_id, jSONObject);  
                   // psUpdate.setString(1, quantity);
                   // psUpdate.setString(2, code);
                  //  psUpdate.executeUpdate();
                    changeCount++;
                    
                }catch (Exception e2){e2.printStackTrace();}
                if(changeCount>2000){
                    System.out.println("Exception, 2000+ units changed their quantity , something is wrong");
                    
                }
             
        }      
  } 
}
