/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package unfi;

import bombayplaza.pricematch.RetrievePage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import net.sf.json.JSONObject;
import net.sf.json.JSONArray;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.commons.codec.binary.Base64;


/**
 *
 * @author prabu
 */
public class UpdateBrandNameFromUnfiBrands {

    public static void main(String args[]) throws Exception {
        // Product  p = getUPCDetails("B00H9H56QA");
        //console(p);
        updateWeight();

    }

    public static void updateWeight() throws Exception {
        Connection con = RetrievePage.getConnection();
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username = "prabu";
        String authString = username + ":" + token;
      //addAllBrands(authString, con);
        PreparedStatement pst = null, pst2 = null;
        ResultSet rs = null, rs2 = null;
        String sort_order = "";
        sort_order = String.valueOf(System.getProperty("sort_order", ""));

       String sql = "  select p.product_id,p.code,ub.brand_name,p.brand  from hb_bc.unfi_input_catalog u1, hb_bc.unfi_brands ub , hb_bc.exported_products_from_hb p where (u1.brand=ub.brand_code  or u1.brand=ub.brand_name  ) and concat('UN_',u1.item_code)=p.code and ub.brand_name!=p.brand    ";
        pst = con.prepareStatement(sql);
        rs = pst.executeQuery();
        int count = 0;
        while (rs.next()) {
            System.out.println("Processign count:" + count);


            String id = rs.getString("product_id");
            String sku = rs.getString("code");
            String brand = rs.getString("brand_name");
            if(brand != null && !brand.equalsIgnoreCase("")) {
                brand = brand.trim().replace("\"", ""); 
            
                String brandSql = "  select *  from hb_bc.brands b where b.name=?";
                pst2 = con.prepareStatement(brandSql);
                pst2.setString(1, brand);
                rs2 = pst2.executeQuery();
                if(rs2.next()){
                    updateBrand(authString, id, rs2.getString("id"),false, con);
                }else{
                    updateBrand(authString, id, brand, true, con);
                }
            }


        }
        if (!con.isClosed()) {
            con.close();
        }
    }

    public static JSONObject createBrand(String authString, String brandName) {
        String webPage = "https://www.harrisburgstore.com/api/v2/brands";
        try {
            URL uri = new URL(webPage);

            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);

            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("POST");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());

            JSONObject jSONObject = new JSONObject();
            if (brandName != null) {
                jSONObject.put("name", brandName);
            }

            String strJString = jSONObject.toString();
            System.out.println("Request : " + strJString);
            out.write(strJString);
            out.close();
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = "";
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            String restult = new String(sb);
            

            JSONObject productBrand = JSONObject.fromObject(restult);
            System.out.println("Response : " + restult);
            if (restult != null && restult.length() > 0) {
                return productBrand;
            } else {
                return null;
            }


            //return  new String(sb1);
        } catch (Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static String updateBrand(String authString, String productid, String brand, boolean newBrand, Connection con)throws Exception {
        System.out.println("Brand : " + brand + "\tnewBrand : " + newBrand);
        String webPage = "https://www.harrisburgstore.com/api/v2/products/"+productid+".json";
        long brandId = 0;
        if(newBrand){
          JSONObject  productBrand =createBrand(authString, brand);
          if(productBrand != null){
            PreparedStatement inserts = con.prepareStatement("INSERT INTO hb_bc.brands (id, name,page_title, meta_keywords, meta_description, image_file, search_keywords) VALUES ( ?, ?, ?,?,?,?,?)");
            inserts.setString(1, productBrand.getString("id"));
            inserts.setString(2, productBrand.getString("name"));
            inserts.setString(3, productBrand.getString("page_title"));
            inserts.setString(4, productBrand.getString("meta_keywords"));
            inserts.setString(5, productBrand.getString("meta_description"));
            inserts.setString(6, productBrand.getString("image_file"));
            inserts.setString(7, productBrand.getString("search_keywords"));

            inserts.executeUpdate();
            brandId = Long.valueOf(productBrand.getString("id")); 
          }
        }else{
           brandId = Long.valueOf(brand); 
        }
        try {
            URL uri = new URL(webPage);


            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);

            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());

            JSONObject jSONObject = new JSONObject();
            if (brandId > 0) {
                jSONObject.put("brand_id", brandId);
            }

            String strJString = jSONObject.toString();
            System.out.println("Request : " + strJString);
            out.write(strJString);
            out.close();
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = "";
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            String restult = new String(sb);
            System.out.println("Response : " + restult);

            return restult;
        } catch (Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }

    }
    static boolean STATUS_204 = false;

    private static String getResultJSONString(String authString, String url) {
        try {
            if (url == null) {
                return null;
            }
            //System.out.println("Url trying to fetch is:" + url);
            URL uri = new URL(url.trim());
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);

            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setDoInput(true);
            httpCon.setRequestMethod("GET");
            if (httpCon.getResponseCode() == 204) {
                System.out.println("status is 204..");
                STATUS_204 = true;
                return null;
            }
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = "";
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            String restult = new String(sb);
            if (restult != null && restult.length() > 0) {
                return restult;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void addAllBrands(String authString, Connection con) throws Exception {
        PreparedStatement delete = con.prepareStatement("delete from hb_bc.brands");
        delete.execute();
        int i = 1;
        String storeURL = "https://www.harrisburgstore.com";
        while (true) {
            try {
                String webPage = storeURL + "/api/v2/brands.json?limit=200&page=" + i;

                String result = getResultJSONString(authString, webPage);
                if (result == null) {
                    break;
                }
                String jsonString = "{\"productBrands\":" + result + "}";

                System.out.println("API response completed");

                
                JSONArray productBrands = JSONArray.fromObject(result);
                System.out.println("API response productBrands" + productBrands);
                PreparedStatement inserts = con.prepareStatement("INSERT INTO hb_bc.brands (id, name,page_title, meta_keywords, meta_description, image_file, search_keywords) VALUES ( ?, ?, ?,?,?,?,?)");

               for(int j=0; j< productBrands.size(); j++){
                    JSONObject productBrand = productBrands.getJSONObject(j);
                    inserts.setString(1, productBrand.getString("id"));
                    inserts.setString(2, productBrand.getString("name"));
                    inserts.setString(3, productBrand.getString("page_title"));
                    inserts.setString(4, productBrand.getString("meta_keywords"));
                    inserts.setString(5, productBrand.getString("meta_description"));
                    inserts.setString(6, productBrand.getString("image_file"));
                    inserts.setString(7, productBrand.getString("search_keywords"));

                    inserts.addBatch();
                }
                System.out.println("Searching Brands is completed");
                inserts.executeBatch();
                System.out.println("Batchupdate completed for Brands");
                inserts.close();
                if (productBrands != null && productBrands.size() < 200) {
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
            i++;
        }
    }
}
