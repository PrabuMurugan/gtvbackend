/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package unfi;



import bombayplaza.catalogcreation.CamelCamelCamel;
import bombayplaza.pricematch.RetrievePage;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import net.sf.json.JSONObject;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
/**
 *
 * @author prabu
 */
public class Fix_Unfi_input_catalog_Capitalize_First_Letter_Cat_Desc {
    public static void main(String args[])  throws Exception  {
     System.out.println("Fix_Unfi_input_catalog_Capitalize_First_Letter_Cat_Desc start:"+ new Date());
        Connection con=RetrievePage.getConnection();
        PreparedStatement pst = null,pst2=null;
        ResultSet rs = null,rs2=null;
        pst=con.prepareStatement("select distinct catg_desc from hb_bc.unfi_input_catalog ");
        rs=pst.executeQuery();
        
        String insert_sql="update hb_bc.unfi_input_catalog set catg_desc=? where catg_desc=? ";
        pst2=con.prepareStatement(insert_sql);
        int count=0;
        while(rs.next()){
            System.out.println("count:"+count++);
            String catg_desc=rs.getString("catg_desc");
            String catg_desc_capitalized=WordUtils.capitalize(catg_desc.toLowerCase().replaceAll("\"", ""));
             
            pst2.setString(1, catg_desc_capitalized);
            pst2.setString(2, catg_desc);
            // pst2.executeUpdate();
            pst2.executeUpdate();
           /* if(count%1000==0){
                System.out.println("Committing at "+count);
                pst2.executeBatch();
            }*/
                
             
        }
         pst2.executeBatch();
          
    }
 
}
