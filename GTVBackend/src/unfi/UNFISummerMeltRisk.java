package unfi;

import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;



public class UNFISummerMeltRisk {
    private static String QUERY = "select  product_id,code, name, p.description,category_string,asin,concat('http://harrisburgstore.com',product_url)   from hb_bc.phi_hva_productfeed h, hb_bc.exported_products_from_hb p "
            + " where ((p.code like 'UN_%' and concat('UN_',h.item_code)=p.code and summer_melt_risk = 'Y' )"
           // + " or lower(p.name) like '%chocolate%' "
            + ") and p.description not like '%Summer Melt Risk%'";
 
    public static void main(String [] args) throws Exception {
          
        System.out.println("Inside main of UNFISummerMeltRisk");
        long iStart = System.currentTimeMillis();
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap<String, JSONObject > map = new HashMap<String, JSONObject>();
                
        try {

           connection=RetrievePage.getConnection();
           System.out.println("Executing QUERY:"+QUERY);
            ps = connection.prepareStatement(QUERY);
            
            rs = ps.executeQuery();
            
           System.out.println("Executing query:"+QUERY);
            while(rs.next()) {
                String strProductId = rs.getString("product_id");
                String strCode = rs.getString("code");
                String description=rs.getString("description");
                
                  description="<p id=summer_melt><span style=\"font-size: medium; color: #ff0000; background-color: #ffffff;\">**Summer Melt Risk : &nbsp;This product may have summer MELT risk. &nbsp;This product is shipped from New York, USA and&nbsp;&nbsp;our UPS and USPS boxes do not have ice packs or dry ice. If you are not in the east coast or you feel the product may melt in transit, we recommend you not to order this product.**</span></p>"+description;
                
                if(description==null)
                    description="";
               // String description = getDescription(asin);
                String origDesc=description;
                System.out.println(strProductId + "\t" + strCode + "\t"   );
                JSONObject jsono = new JSONObject();

                // jsono.put("name", strName);
                if(origDesc.equals(description)==false)
                jsono.put("description", description);

                
               
                 
            }
            
               
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                //if(rs != null && !rs.isClosed()) rs.close();
                //if(ps != null && !ps.isClosed()) ps.close();
                if(connection != null && !connection.isClosed()) connection.close();
            } catch (SQLException ex) {
                            }
        }
        
     
    }
    public static String getDescriptionFromAmazon(String asin){
         
        HashMap hm=MatchThePrice.getProductDetails(asin, true, true, true);
        String des=(String)(hm.get("des")!=null?hm.get("des"):null);
        String aplusProductDescription=(String)(hm.get("aplusProductDescription")!=null?hm.get("aplusProductDescription"):null);
        String bb1=(String)(hm.get("bb1")!=null?hm.get("bb1"):null);
        String bb2=(String)(hm.get("bb2")!=null?hm.get("bb2"):null);
        String bb3=(String)(hm.get("bb3")!=null?hm.get("bb3"):null);
        String bb4=(String)(hm.get("bb4")!=null?hm.get("bb4"):null);


            String descr=des+"<ul>";
            if(bb1!=null && bb1.toUpperCase().equals("NULL") ==false)
            descr+="<li>"+bb1+"</li>";
            if(bb2!=null&& bb1.toUpperCase().equals("NULL") ==false)
            descr+="<li>"+bb2+"</li>";
            if(bb3!=null&& bb1.toUpperCase().equals("NULL") ==false)
            descr+="<li>"+bb3+"</li>";
            if(bb4!=null&& bb1.toUpperCase().equals("NULL") ==false)
            descr+="<li>"+bb4+"</li>";

            descr+="</ul>";  
            if(aplusProductDescription!=null){
                 descr+=aplusProductDescription; 
            }
        return descr;
        
    }
     public static String updateProduct(String productid, JSONObject jSONObject) {
            String webPage = "https://www.harrisburgstore.com/api/v2/products/"+productid+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
         StringBuilder sb1 = new StringBuilder();
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            httpCon.setConnectTimeout(300000);
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
              
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
           
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
           
        } catch(Exception exception) {
            exception.printStackTrace();
        }
         return  new String(sb1);
    }    
 
public static String getJSONString(String strAPI) throws Exception {
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
         
        String authString = username + ":" + token;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
      
        HttpURLConnection httpCon = (HttpURLConnection) (new URL(strAPI)).openConnection();
        httpCon.setRequestProperty("Content-Type", "application/json");
        httpCon.setRequestProperty("Accept", "application/json");
        httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
        httpCon.setDoOutput(true);
        httpCon.setDoInput(true);
        httpCon.setRequestMethod("GET");

        InputStream responseIS = httpCon.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
        String line ="";
        StringBuilder sb = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        return new String(sb);
    }     
   
}
