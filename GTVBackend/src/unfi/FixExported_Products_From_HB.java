/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package unfi;


import bombayplaza.catalogcreation.CamelCamelCamel;
import bombayplaza.pricematch.RetrievePage;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import net.sf.json.JSONObject;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import org.apache.commons.lang.StringUtils;
/**
 *
 * @author prabu
 */
public class FixExported_Products_From_HB {
    /*
     * Make sure you export all products from HB using Prabu template and store it in  C:\\Google Drive\\Dropbox\\tmp\\exported_products_from_hb.txt
     * then run below command and make sure all 20K+ products are loaded first.
     * LOAD DATA LOCAL INFILE 'C:\\Google Drive\\Dropbox\\tmp\\exported_products_from_hb.txt' 
INTO TABLE exported_products_from_hb 
FIELDS TERMINATED BY ','  OPTIONALLY ENCLOSED BY '\"'  
LINES TERMINATED BY '\r\n' 

     * 
     */
 public static void main(String args[])  throws Exception  {
    //  putAsininPeachTreeCostco();
     //System.out.println("FixExported_Products_From_HB start:"+ new Date());
        Connection con=RetrievePage.getConnection();
        PreparedStatement pst = null,pst2=null;
        ResultSet rs = null,rs2=null;
        pst=con.prepareStatement("select code,Product_Images  from hb_bc.exported_products_from_hb where  Product_Images like 'Product Image ID%'  ");
        PreparedStatement pst_good_selling_unfi_products=con.prepareStatement("select  sku     from hb_bc.recent_ordered_products op, hb_bc.recent_orders o where o.id=op.order_id   and str_to_date(o.date_created,'%a, %d %b %Y %H:%i:%S %f') >(curdate() - interval 60 day)   and status='Shipped' and sku like 'UN_%' group by sku having count(distinct o.id) >=1  "
                + " union "
                + " select  sku from tajplaza.sellery_export  where cast(sales_rank as decimal)<100000 "
                + " union "
                + " select concat('UN_',ui.item_code)  from  hb_bc.unfi_input_catalog ui  where (cast(pack as decimal)= 1 or  size='#')  ");
        ResultSet rs_good_selling_unfi_products=pst_good_selling_unfi_products.executeQuery();
        ArrayList<String>  good_selling_unfi_products = new ArrayList();
        while(rs_good_selling_unfi_products.next())
            good_selling_unfi_products.add(rs_good_selling_unfi_products.getString(1));
        //Lets remove from the list, what Google gave in warning
        good_selling_unfi_products.remove("UN_907626");
        good_selling_unfi_products.remove("UN_204172");
        good_selling_unfi_products.remove("CM_85431");
        good_selling_unfi_products.remove("CM_78071");
        good_selling_unfi_products.remove("UN_486571");
        good_selling_unfi_products.remove("UN_919803");
        
        
        rs=pst.executeQuery();
        
        String insert_sql="update hb_bc.exported_products_from_hb set Product_Images=? where Code=? ";
        pst2=con.prepareStatement(insert_sql);
        int count=0;
        while(rs.next()){
            String code=rs.getString("Code");
            
            System.out.println("Processed count:"+count++);
            String Product_Images=rs.getString("Product_Images");
            if(Product_Images==null)
                continue;
            String[] Product_Images_Arr=Product_Images.split(",");
            String img;
            String current_product_image_url=null,thumb_product_image_url=null;
            for(int i=0;i<Product_Images_Arr.length;i++){
                img=Product_Images_Arr[i];
                if(img.indexOf("Product Image URL:")>=0){
                    current_product_image_url=img;
                    current_product_image_url=StringUtils.substringAfter(current_product_image_url, " Product Image URL:");
                    current_product_image_url=current_product_image_url.trim();
                }
                if(thumb_product_image_url==null && img.indexOf("Product Image Is Thumbnail: 1")>=0){
                    thumb_product_image_url=current_product_image_url;
                }
            }
            
            //Check if nopack image exists.
            String image_file_name=StringUtils.substringAfterLast(thumb_product_image_url, "/") ;
            
            String portion1=StringUtils.substringBeforeLast(image_file_name,".");
            String portion2=StringUtils.substringAfterLast(image_file_name,".");
            
            String nopack_image_url="C:\\apache-tomcat-7-prod\\webapps\\HBImages\\"+portion1+"-nopack."+portion2;
            if(new File(nopack_image_url).exists()&& good_selling_unfi_products.contains(code)==false){
                thumb_product_image_url="http://localhost/HBImages/"+portion1+"-nopack."+portion2;
            }  
            //Prabu, If you get a violation from Google, uncomment below loop
           /* if(code.indexOf("UN_")>=0 && new File(nopack_image_url).exists()==false && good_selling_unfi_products.contains(code)==false){
                    //Lets remove the pack logo for UNFI 
                    try{
                    FixImageRemovePackLogoForGoogle.removePackLogo(code, thumb_product_image_url);
                    thumb_product_image_url="http://localhost/HBImages/"+portion1+"-nopack."+portion2;
                    }catch (Exception e2){
                     System.out.println("Pack logo removed logic failed for SKU:"+code);
                    e2.printStackTrace();}
                
            }*/
            pst2.setString(1, thumb_product_image_url);
            pst2.setString(2, code);
            // pst2.executeUpdate();
            pst2.addBatch();
            
             
        }
         pst2.executeBatch();
         System.out.println("FixExported_Products_From_HB part-1 done:"+ new Date());
         updateExcludeUNFIProducts( con) ;
         System.out.println("FixExported_Products_From_HB part-2 done:"+ new Date());
       /*  String sql="truncate table hb_bc.unfi_hb_product ";
         pst=con.prepareStatement(sql);
         pst.executeUpdate();;

         sql="insert into hb_bc.unfi_hb_product (id,name,description,sku,custom_url,availability,price)  select product_id,name,description,code,product_url,'available',price from hb_bc.exported_products_from_hb where code like 'UN_%'";
         pst=con.prepareStatement(sql);
         pst.executeUpdate();*/
          //putAsininPeachTree();
        //  putAsininPeachTreeCostco();
         System.out.println("FixExported_Products_From_HB part-3 done End:"+ new Date());
         if(!con.isClosed()) con.close();
        
        
 }
     private static void updateExcludeUNFIProducts(Connection connection) throws Exception{
//If we are fetching all products, lets update program/product_excludes file
          PreparedStatement pst=connection.prepareStatement("select min(product_id),max(product_id) from hb_bc.exported_products_from_hb where code like 'UN_%'");
          ResultSet rs=pst.executeQuery();
          if(rs.next()){
              Long minUnfiID=rs.getLong(1);
              Long maxUnfiID=rs.getLong(2);
              FileWriter fstream_out = new FileWriter("C:/Google Drive/Dropbox/program/product_excludes.properties");
              BufferedWriter out = new BufferedWriter(fstream_out);
              out.write("# To change this template, choose Tools | Templates\n# and open the template in the editor.\n# hb.bc.product.exclude.min.id is for lower endpoint and hb.bc.product.exclude.max.id is for higher endpoint\nhb.bc.product.exclude.min.id="+minUnfiID+"\nhb.bc.product.exclude.max.id="+maxUnfiID);

              out.close();

          }
    }
    public static void putAsininPeachTreeCostco() throws Exception{
              //I DO NOT NEED THIS SINCE Query 74 does not depend on this anymore.
             // if(true)
               //   return;
         System.out.println("Inside putAsininPeachTree");
             Connection con=RetrievePage.getConnection("hb_bc");
            PreparedStatement pst = null;
            //PreparedStatement pst1 = null;
            PreparedStatement pst2 = null;
            
           // PreparedStatement pst3 = null;
            ResultSet rs = null;
            ResultSet rs1 = null;
            ResultSet rs2 = null;
          
// update rank
           System.out.println("Updating peachtree in product ");
           // pst=con.prepareStatement("select  p.id , p.sku, tajplaza.cleanupc(p.upc) upc from product p where sku like 'CO_%'");
           // pst1=con.prepareStatement("select  tajplaza.cleanupc(c2) upc,c3 from costco_input_catalog where tajplaza.cleanupc(c2)=?");
            pst2=con.prepareStatement("select  tajplaza.cleanupc(upc) upc, asin from costco_catalog where tajplaza.cleanupc(upc)=?");
            
          //  pst3=con.prepareStatement("select  reg_cost  from     unfi_input_catalog_may ui  where item_code=?");
             PreparedStatement pstUpdate=con.prepareStatement(" update  product set peachtree_gl_account=?  where sku=? ");

            rs=pst.executeQuery();
             int count=0;
           while( rs.next()){
               String id=rs.getString("id");
               String sku=rs.getString("sku");
               String upc=rs.getString("upc");
                
               //pst1.setString(1, upc);
              // rs1=pst1.executeQuery();
               
               String c3=null;
             //  if(rs1.next())
                //   c3=rs1.getString("c3");
              // if(c3==null)
                 //      continue;
               String asin="NA";
               pst2.setString(1, upc);
               rs2=pst2.executeQuery();
               if(rs2.next())
                   asin=rs2.getString("asin");
               
               
              // pst3.setString(1, unfi_item_code);
               //ResultSet rs3=pst3.executeQuery();
             
              System.out.println("Updating count:"+(count++) +" sku:"+sku);
                String peachtree=asin+"||"+c3;
               pstUpdate.setString(1, peachtree);
               pstUpdate.setString(2, sku);
               pstUpdate.executeUpdate();
               
           }           
          pst2.executeBatch();
            con.close();
     }   
  public static void putAsininPeachTree() throws Exception{
              //I DO NOT NEED THIS SINCE Query 74 does not depend on this anymore.
             // if(true)
               //   return;
         System.out.println("Inside putAsininPeachTree");
             Connection con=RetrievePage.getConnection("hb_bc");
            PreparedStatement pst = null;
            PreparedStatement pst2 = null;
           // PreparedStatement pst3 = null;
            ResultSet rs = null;
          
// update rank
           System.out.println("Updating peachtree in unfi_hb_product ");
            pst=con.prepareStatement("select distinct item_code,asin from unfi_catalog where concat('UN_',item_code) in (select sku from unfi_hb_product)");
          //  pst3=con.prepareStatement("select  reg_cost  from     unfi_input_catalog_may ui  where item_code=?");
             pst2=con.prepareStatement(" update unfi_hb_product set peachtree_gl_account=?  where sku=? ");

            rs=pst.executeQuery();
             int count=0;
           while( rs.next()){
               String unfi_item_code=rs.getString(1);
               String asin=rs.getString(2);
               String sku="UN_"+unfi_item_code;
               
              // pst3.setString(1, unfi_item_code);
               //ResultSet rs3=pst3.executeQuery();
               String reg_cost="XX";
              System.out.println("Updating count:"+(count++));
              /*if(rs3.next()){
                   
                   //Float reg_costF=rs3.getFloat("reg_cost")*(float).85;
                   Float reg_costF=rs3.getFloat("reg_cost");
                   reg_costF=Math.round(reg_costF*(float)100)/(float)100;;
                   reg_cost=String.valueOf(reg_costF);
               }*/
                   
               
               String peachtree=asin+"||"+reg_cost;
                pst2.setString(1, peachtree);
                pst2.setString(2, sku);
                pst2.addBatch();
                System.out.println("updating peachtree, sku:"+sku + " setting peachtree:"+peachtree);
                /*if(count%1000==0){
                    System.out.println("Committing");
                    pst2.executeBatch();
                }*/
           }           
          pst2.executeBatch();
            con.close();
     }       
}
