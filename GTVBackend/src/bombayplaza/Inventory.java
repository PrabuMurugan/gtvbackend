/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bombayplaza;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Acer
 */
public class Inventory {
   static  int collectedProducts=0;
   static ArrayList visitedURLS=new ArrayList();
    public static void main(String args[]){
          final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_38);
           
          webClient.getOptions().setThrowExceptionOnScriptError(false);
         // webClient.getOptions().setJavaScriptEnabled(false);
          webClient.getOptions().setCssEnabled(false);
     HtmlPage page;
        try {
            //for(int count=0;count<100;count++)
            {
                //http://www.amazon.com/s/qid=1328777704/ref=sr_pg_2?ie=UTF8&me=A30ZA0TJNSOPSP&rh=&page=40
                        //page = webClient.getPage("http://www.amazon.com/s/qid=1328777704/ref=sr_pg_2?ie=UTF8&me=A30ZA0TJNSOPSP&rh=&page="+count);
                page = webClient.getPage("https://www.patelbrothersusa.com/newsite/get_items1.asp?cat_id=3&sub_cat_id=51&cat_name=Snacks&sub_cat_name=Anand&sub_cat_id_1=0&more_cat=N");
             //   http://www.amazon.com/gp/browse.html?ie=UTF8&marketplaceID=ATVPDKIKX0DER&me=A33HRFTZ9IMKIK
                
            //           System.out.println("PAge : "+page.asText());

                   // DomElement div=page.getElementById("searchResults");
                    DomNodeList<DomElement> aTags = page.getElementsByTagName("a");
                    System.out.println("Got page1. LEngth of aTags:"+aTags.size());

                   int  prevCollectedProducts=collectedProducts;
                    for(int i=0;i<aTags.size();i++){
                        //HtmlAnchor a=(HtmlAnchor) aTags.get(i);
                        //HtmlPage page2=a.click();
                        //System.out.println("pAGE IS"+page2.asText());

                        collectAttributes(aTags.get(i));
                    }
                    if(prevCollectedProducts==collectedProducts)
                    {
                        System.out.println("NO ITEMS COLLECTED. EXITING");
                      //  break;
                    }
                
            }
        
     //webClient.closeAllWindows();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
 

    private static void collectAttributes(DomElement a) {
            
         HtmlAnchor al=(HtmlAnchor)a;
        // System.out.println("Inside collectAttributes"+al.getHrefAttribute());
         if(al.getHrefAttribute()==null|| al.getHrefAttribute().indexOf("amazon.com")<0)
             return;
         if(al.getHrefAttribute().indexOf("A11EIBJBV5KQME")<0)
         if(visitedURLS.contains(al.getHrefAttribute()))
             return;
         visitedURLS.add(al.getHrefAttribute());
        try {
            System.out.println("Clicking "+al.getHrefAttribute());
            WebClient webClient = new WebClient(BrowserVersion.FIREFOX_38);
                     webClient.getOptions().setJavaScriptEnabled(false);
          webClient.getOptions().setCssEnabled(false);
           HtmlPage page = webClient.getPage(al.getHrefAttribute());
           //System.out.println("PAge : "+page.asText());
           if(page.getElementById("btAsinTitle")==null)
               return;
           String title=page.getElementById("btAsinTitle").asText();
           String features="";
           if(page.getByXPath("//div[@class='content']").size()>0)
               features=((DomElement)page.getByXPath("//div[@class='content']").get(0)).asText();
           String desc="";
           if(page.getByXPath("//div[@class='productDescriptionWrapper']").size()>0)
                   desc=((DomElement)page.getByXPath("//div[@class='productDescriptionWrapper']").get(0)).asText();
           String manu="";
           if(page.getByXPath("//div[@class='buying']/span").size()>0)
                   manu=((DomElement)page.getByXPath("//div[@class='buying']/span").get(0)).asText();
           manu=manu.replaceAll("by ", "").trim();
           String price=((DomElement)page.getByXPath("//b[@class='priceLarge']").get(0)).asText();
           String image_str=null;
           if(page.getByXPath("//td[@id='prodImageCell']//img").size()>0)
                image_str=((HtmlImage)page.getByXPath("//td[@id='prodImageCell']//img").get(0)).getSrcAttribute();
           else if (page.getByXPath("//div[@id='prodImageCell']//img").size()>0)
               image_str=((HtmlImage)page.getByXPath("//div[@id='prodImageCell']//img").get(0)).getSrcAttribute();
           else
               return;
            BufferedImage image = null;
            image = ImageIO.read( new URL(image_str ));
            String local_img_file=getfileName(title)+".jpg";
            ImageIO.write( image, "jpeg" /* "png" "jpeg" ... format desired */,
               new File ( "C:\\Temp2012\\images\\"+ local_img_file) /* target */ );
            
            writeToCsv(al.getHrefAttribute(),title,desc,manu,features,price,local_img_file);
           System.out.println("Product name:"+title);
           if(collectedProducts++>5000)
             System.exit(0);
           // HtmlPage page=al.click();
            //System.out.println("pAGE IS"+page.asText());
        } catch (Exception ex) {    
            System.err.println("ERROR WHILE COLLECTING URL:"+((HtmlAnchor)a).getHrefAttribute());
           ex.printStackTrace();
        }
    }
    

static String getfileName(String s){
StringBuffer str=new StringBuffer();
for(int i=0;i<s.length();i++){
    if((s.charAt(i)>='0' &&s.charAt(i)<='9') ||(s.charAt(i)>='A' && s.charAt(i)<='Z') || (s.charAt(i)>='a' &&s.charAt(i)<='z' ) )
    {
        str.append(s.charAt(i));
    }  else{
        str.append("_");
    }
}       




return str.toString();
}

    private static void writeToCsv(String url,String title, String desc, String manu,String features, String price, String local_img_file) throws IOException {
        title=title.replaceAll(","," ");
        desc=desc.replaceAll(",","");
        desc=desc.replaceAll("\r\n","");
        desc=desc.replaceAll("\n","");
        
        features=features.replaceAll(",","");
        features=features.replaceAll("\r\n","");
        features=features.replaceAll("\n","");
        
        price=price.replaceAll(",","");
         FileWriter fstream = new FileWriter("C:\\Temp2012\\inventory.csv",true);
  BufferedWriter out = new BufferedWriter(fstream);
  out.write(url+","+title+","+desc+","+manu+","+price+","+local_img_file+","+features);
  out.newLine();
  //Close the output stream
  out.close();
    }
}
