/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bombayplaza;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Acer
 */
public class Inventory_Amazon {
   static  int collectedProducts=0;
   static ArrayList visitedURLS2=new ArrayList();
   static ArrayList visitedURLS_Products=new ArrayList();
   //static String SELLERID="A11EIBJBV5KQME";
   //static String SELLERID="A20RGF2W7MYP64";
  // static String SELLERID="A3GO3ZGMFW0B3F";
   static String SELLERID="";
  
  //  static String SELLERID="A1TAVADKTUUTLO";
   static ArrayList products=new ArrayList();
   static String FILE="C:\\Temp2012\\inventory_swad.csv";
   static String FILE2="C:\\Temp2012\\visitedurls_NA.csv";
    public static void main(String args[]){
           WebClient webClient1 = new WebClient(BrowserVersion.FIREFOX_38);
           
          webClient1.getOptions().setThrowExceptionOnScriptError(false);
          webClient1.getOptions().setJavaScriptEnabled(true);
          webClient1.getOptions().setCssEnabled(false);
     HtmlPage page;
     openProducts(FILE,FILE2);
        try {
            for(int count=2;count<=2;count++){
                //http://www.amazon.com/s/qid=1328777704/ref=sr_pg_2?ie=UTF8&me=A30ZA0TJNSOPSP&rh=&page=40
                        //page = webClient.getPage("http://www.amazon.com/s/qid=1328777704/ref=sr_pg_2?ie=UTF8&me=A30ZA0TJNSOPSP&rh=&page="+count);
                
                //String url="http://www.amazon.com/s/qid=1329064281/ref=sr_pg_2?ie=UTF8&me=SELLERID&rh=&page="+count;
                String url="http://www.amazon.com/s/ref=bl_sr_grocery?_encoding=UTF8&node=16310101&page="+count;
                url=url.replaceAll("SELLERID",SELLERID);
                System.out.println("Opening url : "+url);
                WebClient webClient2;
                page = webClient1.getPage(url);
                webClient1.waitForBackgroundJavaScript(30000);
             //   http://www.amazon.com/gp/browse.html?ie=UTF8&marketplaceID=ATVPDKIKX0DER&me=A33HRFTZ9IMKIK
                
            //           System.out.println("PAge : "+page.asText());

                   // DomElement div=page.getElementById("searchResults");
                    DomNodeList<DomElement> aTags = page.getElementsByTagName("a");
                    System.out.println("Got page1. LEngth of aTags:"+aTags.size());
                    if(aTags.size()==0)
                        break;
                   int  prevCollectedProducts=collectedProducts;
                    for(int i=0;i<aTags.size();i++){
                        //HtmlAnchor a=(HtmlAnchor) aTags.get(i);
                        //HtmlPage page2=a.click();
                        //System.out.println("pAGE IS"+page2.asText());

                        collectAttributes(aTags.get(i));
                    }
                    if(prevCollectedProducts==collectedProducts)
                    {
                        System.out.println("NO ITEMS COLLECTED. EXITING");
                        break;
                    }
                
            }
        
      
        }catch (Exception e){
            e.printStackTrace();
        }
    }
 

    private static void collectAttributes(DomElement a) throws IOException {
            
         HtmlAnchor al=(HtmlAnchor)a;
         
         
         if(al.getHrefAttribute()==null|| al.getHrefAttribute().indexOf("amazon.com")<0)
             return;
         if(al.getHrefAttribute().indexOf(SELLERID)<0)
             return;
              if(al.getHrefAttribute().indexOf("http://www.amazon.com/gp/feature.html/")>=0)
             return;
      // System.out.println("Inside collectAttributes"+StringUtils.substring(al.getHrefAttribute(),0,al.getHrefAttribute().indexOf("/", 22)));
       //System.out.println("Inside collectAttributes:"+al.getHrefAttribute());
          if(visitedURLS_Products.contains(StringUtils.substring(al.getHrefAttribute(),0,al.getHrefAttribute().indexOf("/", 22))))
              collectedProducts++;
         if(visitedURLS2.contains(StringUtils.substring(al.getHrefAttribute(),0,al.getHrefAttribute().indexOf("/", 22))))
             return;
         visitedURLS2.add(StringUtils.substring(al.getHrefAttribute(),0,al.getHrefAttribute().indexOf("/", 22)));
         writevisitedUrl(al.getHrefAttribute());
        try {
            System.out.println("Clicking "+al.getHrefAttribute());
            WebClient webClient2 = new WebClient(BrowserVersion.FIREFOX_38);
                     webClient2.getOptions().setJavaScriptEnabled(true);
          webClient2.getOptions().setCssEnabled(false);
          webClient2.getOptions().setThrowExceptionOnScriptError(false);
           HtmlPage page = webClient2.getPage(al.getHrefAttribute());
           
           //System.out.println("PAge : "+page.asText());
           if(page.getElementById("btAsinTitle")==null)
               return;
           String title=page.getElementById("btAsinTitle").asText();
           String features="";
           if(page.getByXPath("//div[@class='content']").size()>0)
               features=((DomElement)page.getByXPath("//div[@class='content']").get(0)).asText();
           String desc="";
           if(page.getByXPath("//div[@class='productDescriptionWrapper']").size()>0)
                   desc=((DomElement)page.getByXPath("//div[@class='productDescriptionWrapper']").get(0)).asText();
           String manu="";
           if(page.getByXPath("//div[@class='buying']/span").size()>0)
                   manu=((DomElement)page.getByXPath("//div[@class='buying']/span").get(0)).asText();
           manu=manu.replaceAll("by ", "").trim();
           String price=((DomElement)page.getByXPath("//b[@class='priceLarge']").get(0)).asText();
           String image_str=null;
           if(page.getByXPath("//td[@id='prodImageCell']//img").size()>0)
                image_str=((HtmlImage)page.getByXPath("//td[@id='prodImageCell']//img").get(0)).getSrcAttribute();
           else if (page.getByXPath("//div[@id='prodImageCell']//img").size()>0)
               image_str=((HtmlImage)page.getByXPath("//div[@id='prodImageCell']//img").get(0)).getSrcAttribute();
           else
               return;
            BufferedImage image = null;
            image = ImageIO.read( new URL(image_str ));
            String local_img_file=getfileName(title)+".jpg";
            ImageIO.write( image, "jpeg" /* "png" "jpeg" ... format desired */,
               new File ( "C:\\Temp2012\\images_swad\\"+ local_img_file) /* target */ );
            
            writeToCsv(al.getHrefAttribute(),title,desc,manu,features,price,local_img_file);
           System.out.println("Product name:"+title);
           if(collectedProducts++>20000)
             System.exit(0);
           // HtmlPage page=al.click();
            //System.out.println("pAGE IS"+page.asText());
        } catch (Exception ex) {    
            System.err.println("ERROR WHILE COLLECTING URL:"+((HtmlAnchor)a).getHrefAttribute());
           ex.printStackTrace();
        }
    }
    

static String getfileName(String s){
StringBuffer str=new StringBuffer();
for(int i=0;i<s.length();i++){
    if((s.charAt(i)>='0' &&s.charAt(i)<='9') ||(s.charAt(i)>='A' && s.charAt(i)<='Z') || (s.charAt(i)>='a' &&s.charAt(i)<='z' ) )
    {
        str.append(s.charAt(i));
    }  else{
        str.append("_");
    }
}       




return str.toString();
}
private static void writevisitedUrl(String url) throws IOException {
        
        
         FileWriter fstream = new FileWriter(FILE2,true);
          
         
  BufferedWriter out = new BufferedWriter(fstream);
  out.write(url);
  out.newLine();
  //Close the output stream
  out.close();
    }

    private static void writeToCsv(String url,String title, String desc, String manu,String features, String price, String local_img_file) throws IOException {
        title=title.replaceAll(","," ");
         if(products.contains(title) )
             return;
         products.add(title);
        desc=desc.replaceAll(",","");
        desc=desc.replaceAll("\r\n","");
        desc=desc.replaceAll("\n","");
        
        features=features.replaceAll(",","");
        features=features.replaceAll("\r\n","");
        features=features.replaceAll("\n","");
        
        price=price.replaceAll(",","");
        
         FileWriter fstream = new FileWriter(FILE,true);
          
         
  BufferedWriter out = new BufferedWriter(fstream);
  //out.write(url+","+title+","+desc+","+manu+","+price+","+local_img_file+","+features);
  out.write(url+","+title+","+desc+","+manu+","+price+","+local_img_file+","+"");
  out.newLine();
  //Close the output stream
  out.close();
    }

  
    private static boolean openProducts(String file,String file2) {
    try {
        Scanner scanner = new Scanner(new FileInputStream(file));
    String line;
      while (scanner.hasNextLine()){
          line=scanner.nextLine();
        products.add(StringUtils.substringBefore( StringUtils.substringAfter( line,","),","));
           visitedURLS2.add(StringUtils.substring(line,0,line.indexOf("/", 22)));
         visitedURLS_Products.add(StringUtils.substring(line,0,line.indexOf("/", 22)));
      }
    }
    catch (Exception e){
        e.printStackTrace();
    }
    try {
        Scanner scanner = new Scanner(new FileInputStream(file2));
    String line;
      while (scanner.hasNextLine()){
          line=scanner.nextLine();
        if(visitedURLS2.contains(StringUtils.substring(line,0,line.indexOf("/", 22)))==false)
            visitedURLS2.add(StringUtils.substring(line,0,line.indexOf("/", 22)));
     
         
      }
    }
    catch (Exception e){
        e.printStackTrace();
    }

    
         return false;
    }
}
