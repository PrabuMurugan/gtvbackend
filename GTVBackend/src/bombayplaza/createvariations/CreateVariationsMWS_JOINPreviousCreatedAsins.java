/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bombayplaza.createvariations;

import amazon.product.ProcessWorkingCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
//import com.tajplaza.products.GetCompetitivePricingForASINSample;
//import com.tajplaza.products.ListMatchingProductsSample;
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetMatchingProductSample;
import com.tajplaza.products.ListMatchingProductsSample;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import unfi.DrawPackLogo;

/**
 *
 * @author US083274
 */
public class CreateVariationsMWS_JOINPreviousCreatedAsins {
    static String FILTER_ITEM="";
   static String PARENT_TEMPLATE="";
   static String ONEPACK_TEMPLATE="";
   static String JUSTONEPACK_TEMPLATE="";
    static String MULTIPACK_TEMPLATE="";
 
    static BufferedWriter out0=null;
    public  static String   feedfile_name;
   static boolean DONOT_JOIN = false;
 static String oc_singlepack="";
 static String  multipacks_sku="";
 static int singlePkgQty=1; 
  static    HashMap bestPriceMap=null;
  
 static WebClient webClient1 ;
  
    public static void createVariation() {
       try{ 
          System.out.println("Value of DONOT_JOIN IS "+DONOT_JOIN);
          // DONOT_JOIN="true";
          if(DONOT_JOIN==true){
                   PARENT_TEMPLATE="";
                 ONEPACK_TEMPLATE="";
                 JUSTONEPACK_TEMPLATE="";
                  MULTIPACK_TEMPLATE="<SKU>	TTT <UPC>	UPC	<TITLE>	<BRAND>	<MANUFA>	<BB1>	<BB2>	<BB3>	<BB4>	<BB5>	<DESCRIPTION>	Food	<PACKAGING-QUAN>	<PRICE>		USD	0				<ITEM-TYPE>																					<IMAGE-URL>									<FULFILL>	<HEIGHT>	<WIDTH>	<LENGTH>	<LEN-MEAS>	<WEIGHT>	<WEIGHT-MEAS>																																																																																																																									";

          }
        String strLine0=null,asin=null,upc=null,sku,title,brand,manu,bb1,bb2,bb3,bb4,bb5,desc,package_quan,price,item_type,image_url;
          FileWriter f= new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\multipack.txt");
          f.close();
        System.out.println("Creating variations DO_NOT_JOIN VALUE is "+DONOT_JOIN);
        boolean variationstobesent=false;
        if(RetrievePage.isOffice()==false){
            FILTER_ITEM="";
        }
        BufferedReader br0;
        FileWriter fstream_out0;
        //Write to the product file
        


        //Get UPC for asin
        webClient1 = new WebClient(BrowserVersion.getDefault(),"proxydirect.tycoelectronics.com", 80);
       

 
        br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\existing-all-inventory.txt")));
        System.out.println("I AM NOT USING MY EXISTING SKUS WHILE CREATING NEW VARIATIONS");
        /*
        while ((strLine0 = br0.readLine()) != null)   {
            existingProductsArr.add(strLine0 );
        }*/
        br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\feeds\\createvariations.txt")));
        boolean headerWriten=false;
      while ((strLine0 = br0.readLine()) != null)   {

          String packagingQuant=null;
        
          System.out.println("line :"+strLine0);
          title=null;
          if(strLine0.split("\t").length>0){
              //Line has both upc and asin
              upc=strLine0.split("\t")[0];
              if(upc.trim().length()==0)
                  continue;
              upc=upc.replaceAll("\"","").replaceAll("'", "");
              upc=upc.trim();
              asin=strLine0.split("\t")[1];
              if(asin.trim().length()==0 || asin.indexOf("B")<0)
                  continue;
              asin=asin.trim();
              asin=asin.replaceAll("\"","").replaceAll("'", "");

              if(upc.indexOf("NA")>=0)
                  continue;
               if(strLine0.split("\t").length >=3){
                   title=strLine0.split("\t")[2];
               }else{
                  continue;
               }
                if(strLine0.split("\t").length >=4){
                   packagingQuant=strLine0.split("\t")[3].trim();
               }
                if(strLine0.split("\t").length >=5){
                   oc_singlepack=strLine0.split("\t")[4].trim();
               }      
                singlePkgQty=1;
                if(strLine0.split("\t").length >=6){
                   singlePkgQty=Integer.parseInt(strLine0.split("\t")[5].trim());
               }        
                  
                if(strLine0.split("\t").length >=7 && strLine0.split("\t")[6].trim().length()>0){
                    multipacks_sku=strLine0.split("\t")[6].trim();
                   if(multipacks_sku.indexOf("PACKS")<0){
                       if(multipacks_sku.indexOf("AMA")>=0){
                           String[] arr=multipacks_sku.split("_");
                           String newmultipacksku="VAR_";
                           for(int i=0;i<arr.length;i++){
                               String txt=arr[i];
                               if(arr[i].indexOf("PACK")>=0){
                                   txt=packagingQuant+"PACKS";
                               }
                               else if(i==arr.length-1){
                                    txt=String.valueOf(Integer.valueOf(txt)*Integer.valueOf(packagingQuant));
                               }
                               newmultipacksku=newmultipacksku+txt;
                               if(i<arr.length-1)
                                   newmultipacksku=newmultipacksku+"_";
                           }
                           multipacks_sku=newmultipacksku;
                       }
                       else if(multipacks_sku.indexOf("_")>0 &&multipacks_sku.indexOf("VAR_")<0 ){
                           String tmp=StringUtils.substringBeforeLast(multipacks_sku, "_");
                           multipacks_sku=tmp+"_"+asin+"_"+packagingQuant+"PACKS_"+StringUtils.substringAfterLast(multipacks_sku, "_");
                           String lastPortion=StringUtils.substringAfterLast(multipacks_sku, "_");
                           if(lastPortion!=null && Float.valueOf(lastPortion).equals(Float.valueOf(oc_singlepack))){
                                tmp=StringUtils.substringBeforeLast(multipacks_sku, "_");
                                multipacks_sku=tmp+"_"+Integer.valueOf(lastPortion)*Integer.valueOf(packagingQuant);
                           }
                       }
                       
                   }
               }else{
                    Integer oc_multipack= Integer.valueOf(oc_singlepack)*Integer.valueOf(packagingQuant);
                    multipacks_sku="VAR_"+asin+"_AMA_"+packagingQuant+"PACKS_"+oc_multipack;
                }                
              if(FILTER_ITEM.length()>0 && strLine0.indexOf(FILTER_ITEM)<0)
                  continue;
                if(title.toLowerCase().indexOf("pack")  >0){
                    System.out.println("WARNING title already contains pack "+title);
                   // continue;
                }
               HashMap hmNew=null;
                 bestPriceMap=null;
              feedfile_name = "multipack.txt";
            fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+feedfile_name,true);
            out0 = new BufferedWriter(fstream_out0);
            BufferedReader br00=null;
            if(DONOT_JOIN==false)
                //br00=new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\feeds\\Flat.File.HomeAndGarden-template.txt")));
                br00=new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\feeds\\Flat.File.FoodAndBeverages-template.txt")));
            else
                br00=new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\feeds\\Flat.File.FoodAndBeverages-template_JUSTMULTIPACK.txt")));
           for(int i=0;i<10;i++){
               strLine0 = br00.readLine();
               if(strLine0==null)
                   break;
               if(i<=2 && headerWriten==false){
                out0.write(strLine0);
                out0.newLine();                   
               } 
               
               if(DONOT_JOIN==false){
                   if(i==3)
                    PARENT_TEMPLATE=strLine0;
                   if(i==4)
                    ONEPACK_TEMPLATE=strLine0;
                   if(i==5)
                    MULTIPACK_TEMPLATE=strLine0;
                   
               }else{
                   if(i==3)
                    MULTIPACK_TEMPLATE=strLine0;                   
               }
                
               
           }
           headerWriten=true;
           /* while ((strLine0 = br00.readLine()) != null )   {
                out0.write(strLine0);
                out0.newLine();
            }*/
            br00.close();
            out0.close();
 
                
        HashMap hmF=RetrievePage.getDetailsFromFbaCalculatorNewMWS(null,asin,null,true);
        
         hmNew=createVaration(null,hmF,upc,asin,Integer.valueOf(packagingQuant),"DEFAULT",title);
       //  if(Integer.valueOf(packagingQuant)!=1)
          //   hm=createVaration(hm,hmF,upc,asin,Integer.valueOf(packagingQuant),"DEFAULT",title);


            if(hmNew!=null)
               variationstobesent=true;

          }
      }
      br0.close();
      if(false && variationstobesent){
        System.out.println("Calling email service to email new-inventroy.csv");
          String[] to={"mprabagar80@gmail.com" };
        String[] cc={};
        String[] bcc={};

        Mail.sendMail( "smtpout.secureserver.net","465","true",
        "true",true,"javax.net.ssl.SSLSocketFactory","false",to,cc,bcc,
        "NEW VARIATIONS - PRODUCTS TO BE CREATED", "Please veriy and upload the attached variations file Flat.File.FoodAndBeverages.txt",false,true,false,false,null);
          
      }

    }catch (Exception e){
        e.printStackTrace();
    }
    }
     boolean JUST_MULTIPACK=true;
     
public static void main(String args[]) throws Exception{
    con=RetrievePage.getConnection();
     
    createVariation();
    
}

    private static HashMap createVaration(HashMap hmNew, HashMap hmF,String upc, String asin, int packaging_quant, String fulfill,String title)  throws Exception{
        String strLine0=null,sku,brand,manu,bb1,bb2,bb3,bb4,bb5,desc,package_quan,price,item_type,image_url;
        String curL;

       
          asin=asin.trim();
          String onepacksku=null;
 
        if(onepacksku==null){
                Date date = Calendar.getInstance().getTime();
                DateFormat formatter = new SimpleDateFormat("MMddhhmmssSSS");
                String today = formatter.format(date);
                System.out.println("Today : " + today);
                onepacksku="1PACK_"+today;            
         //   System.out.println("I can not proceed without 1 pack SKU");
           // return hm;
        }

        System.out.println("GOING TO CREATE VARIATION:"+upc+","+asin+","+title+","+packaging_quant+","+fulfill);
            if(hmNew==null){
                ArrayList<HashMap> matchingProducts = null;
                matchingProducts=ListMatchingProductsSample.getMatchingProducts(upc);
                for(int i=0;i<matchingProducts.size();i++){
                    HashMap tmpMap=matchingProducts.get(i);
                    if(tmpMap.get("asin").equals(asin)){
                        System.out.println("Matching asin found throgh MWS API");
                        hmNew=tmpMap;
                        //Reason for parsing through here is to see if there is an exisitng multi pack quantity exists.
                        
                    }
                    
                }
               
                 

            }else{
            //Wait a 100 ms, so i get a new SKU
                Thread.sleep(100);
            }
            if(hmNew==null){
                System.out.println("hmNew is null");
                return null;
            }
            
            if( 
                    ((String)hmNew.get("productCategoryId")).indexOf("grocery")<0
                   
                    )
            {
                System.err.println("This create variation supports only grocery at this time ..NOT "+hmNew.get("productCategoryId"));
                System.exit(1);;
            }
            
              //Create new SKU
                Date date = Calendar.getInstance().getTime();
                DateFormat formatter = new SimpleDateFormat("MMddhhmmssSSS");
                String today = formatter.format(date);
                System.out.println("Today : " + today);
                sku="VAR_"+today;
                String parentSku=null;

                if(hmNew.get("parentSku")==null)
                    parentSku="PARENT"+today;
                else
                   parentSku= (String) hmNew.get("parentSku");
                hmNew.put("parentSku", parentSku);


               /* if(packaging_quant>1){
                    //In case of amazon fulfillment, i hope already the other rows are written..so just add the fba row.
                  sku+="_VAR";
                  writeToNewVariationFile("multipack",sku, hm,hmF, upc,parentSku,  asin,  packaging_quant,  fulfill, title);
                  return hm;
                }     */           
                if(packaging_quant==1){
                    writeToNewVariationFile("just1pack",onepacksku, hmNew,hmF, upc,"",  asin,  1,  fulfill, title);
                    return hmNew;
                }
                 writeToNewVariationFile("parent",parentSku,hmNew,hmF, upc,null,  asin,  1,  fulfill, title);
                 writeToNewVariationFile("1pack",onepacksku, hmNew,hmF, upc,parentSku,  asin,  1,  fulfill, title);
                 sku+="_PRAB";
                 
                 if(multipacks_sku.length()>0){
                     sku=multipacks_sku ;
                 }
                 

                     
                 
                 writeToNewVariationFile("multipack",sku, hmNew,hmF, upc, parentSku, asin,  packaging_quant,  fulfill, title);
                 insertintovariation_sku_upcs(sku,upc,title,singlePkgQty*packaging_quant,Float.valueOf(oc_singlepack)*packaging_quant,asin);
                return hmNew;
    }
    public static Connection con;
    public static void insertintovariation_sku_upcs(String sku, String upc, String title, int pkg_qty,float oc_multipack,String onepack_asin) throws Exception{
        
        PreparedStatement ps=con.prepareStatement("insert into tajplaza.variation_sku_upcs (sku,upc,pkg_qty,title,oc_multipack,onepack_asin) values (?,?,?,?,?,?)");
        ps.setString(1, sku);
        ps.setString(2, upc);
        ps.setInt(3, pkg_qty);
        if(title.length()>100)
            title=title.substring(0,99);
        ps.setString(4, title);
        ps.setFloat(5, oc_multipack);
        ps.setString(6, onepack_asin);
        ps.executeUpdate();
        
    }
 private static void writeToNewVariationFile(String type,String sku, HashMap hmNew,HashMap hmF, String upc,String parentSku, String asin, int packaging_quant, String fulfill,String title) throws IOException, Exception {
                String strLine0=null ,brand,manu,bb1,bb2,bb3,bb4,bb5,desc,package_quan,price,item_type,image_url;
                String newproduct=null;
                if(type.equals("parent")){
                    newproduct=PARENT_TEMPLATE;
                }else if(type.equals("1pack")){
                    newproduct=ONEPACK_TEMPLATE;
                }else if(type.equals("multipack")){
                    newproduct=MULTIPACK_TEMPLATE;
                }
                else if(type.equals("just1pack")){
                    newproduct=JUSTONEPACK_TEMPLATE;
                }

                if(newproduct.length()==0)
                    return;
                newproduct=newproduct.replaceAll("<SKU>", sku);
                newproduct=newproduct.replaceAll("<UPC>", upc);

                title=(String)(hmNew.get("Title")!=null?hmNew.get("Title"):"");
                if(title.indexOf("Amazon.com")>=0)
                    title=title.replaceAll("Amazon.com : ","");
               // int titlteContainsPackOf=0;
                if(title.indexOf("Pack")>0 && title.indexOf("(")>0){
                    String pack=StringUtils.substringAfter(title, "(");
                     pack=StringUtils.substringBefore(pack, ")");
                     // titlteContainsPackOf=ProcessWorkingCatalog.calculatePackingQntFromTitle(title);
                     
                     
                   System.err.println("Single pack contains pack of..Update the single and multi pack title accordingly:"+pack);
                   // title=StringUtils.substringBefore(title, "(");
                    //title=StringUtils.substringBeforeLast(title, "(");
                    //System.out.println("Its not a single pack")
                }
                 String multiSize="";
                if(packaging_quant>1){
                            if(title.toLowerCase().indexOf("pack")>=0)
                                multiSize=packaging_quant  +" X ";
                            else
                                multiSize=packaging_quant  +" X ";
                                //multiSize=packaging_quant  +" Packs :";
                            title=multiSize+title;
                         //title= packaging_quant+"X"+ singlePkgQty +" Packs :"+title;
                        
                }
                    //title= title + ":(Pack Of "+packaging_quant+")";
                String productCategoryId=(String)(hmNew.get("productCategoryId")!=null?hmNew.get("productCategoryId"):"");
                newproduct=newproduct.replaceAll("<productCategoryId>", productCategoryId);
                newproduct=newproduct.replaceAll("<TITLE>", title);

                brand=(String)(hmNew.get("Manufacturer")!=null?hmNew.get("Manufacturer"):"");
                newproduct=newproduct.replaceAll("<BRAND>", brand);

                newproduct=newproduct.replaceAll("<MANUFA>", brand);


                bb1=(String)(hmNew.get("bb1")!=null?hmNew.get("bb1"):"");
                /*if(packaging_quant>1){
                    bb1="'"+packaging_quant+"'' packs";
                }*/
                bb1=bb1.replaceAll("\\$","");
                if(bb1.indexOf("pack")>=0  )
                    bb1="";
                
                newproduct=newproduct.replaceAll("<BB1>", bb1);

                bb2=(String)(hmNew.get("bb2")!=null?hmNew.get("bb2"):"");
                bb2=bb2.replaceAll("\\$","");
                if(bb2.indexOf("pack")>=0  )                
                    bb2="";
                newproduct=newproduct.replaceAll("<BB2>", bb2);

                bb3=(String)(hmNew.get("bb3")!=null?hmNew.get("bb3"):"");
                bb3=bb3.replaceAll("\\$","");
                if(bb3.indexOf("pack")>=0  )
                    bb3="";
                newproduct=newproduct.replaceAll("<BB3>", bb3);

                bb4=(String)(hmNew.get("bb4")!=null?hmNew.get("bb4"):"");
                bb4=bb4.replaceAll("\\$","");
                if(bb4.indexOf("pack")>=0  )                
                    bb4="";
                newproduct=newproduct.replaceAll("<BB4>", bb4);

                bb5=(String)(hmNew.get("bb5")!=null?hmNew.get("bb5"):"");
                bb5=bb5.replaceAll("\\$","");
                if(bb5.indexOf("pack")>=0  )                
                    bb5="";
                newproduct=newproduct.replaceAll("<BB5>", bb5);

                desc=(String)(hmNew.get("desc_txt")!=null?hmNew.get("desc_txt"):"");
                desc = desc.replaceAll("[^\\x00-\\x7F]", "");
                desc=desc.replaceAll("\n","").replaceAll("\r","");
                if(desc.length()==0)
                    desc=title;
                newproduct=newproduct.replaceAll("<DESCRIPTION>", desc);

                    
                int tmpInt=1;
                if(packaging_quant==1 && hmNew.containsKey("PackageQuantity")){
                    //For 1 pack, use existing packing quantity
                    newproduct=newproduct.replaceAll("<PACKAGING-QUAN>",(String)hmNew.get("PackageQuantity"));
                }else{
                    /*if( hmNew.containsKey("Size")){
                          String tmp=(String)hmNew.get("Size");
                         tmp=tmp.replaceAll("\\D+","");
                         if(tmp==null || Integer.valueOf(tmp)==0)
                             tmpInt=1;
                         else
                            tmpInt= Integer.valueOf(tmp);
                    }*/
                    System.out.println("Querying amazon for UPC:"+upc+" to make sure  packing quantity :" +singlePkgQty*packaging_quant+ " does not exists");
                     ArrayList<HashMap> products=ListMatchingProductsSample.getMatchingProducts(upc);
                     for(int i=0;i<products.size();i++){
                         HashMap hm=products.get(i);
                         
                         if(hm.containsKey("PackageQuantity")){
                             String existPkgQty=  (String) hm.get("PackageQuantity");
                             try{
                                 if(Integer.valueOf(existPkgQty)==singlePkgQty*packaging_quant){
                                     tmpInt=(int) Math.floor(Math.random() * 10);
                                 }
                                 }catch (Exception e2){e2.printStackTrace();}
                             }
                            if(hm.containsKey("asin")){
                                //PRABU : TO DO , IS THERE ANY CHILD VARIATION OF EACH ASIN THAT MATCHES THE PACKING QUANTITY I WANT
                                //TOO MUCH OVERCOMPLICATING??
                                //TO BE COMMENTED LATER
                                ArrayList<HashMap> arr=GetMatchingProductSample.getAllOtherVariationsBySize((String)hm.get("asin"));
                                PreparedStatement ps=con.prepareStatement("select distinct e.asin1   from variation_sku_upcs v , existing_all_inventory_bk e  where  e.seller_sku=v.sku and onepack_asin is not null and v.sku=?");                                
                                ps.setString(1, multipacks_sku);
                                ResultSet rs=ps.executeQuery();
                                if(rs.next()){
                                    for(int a=0;a<arr.size();a++){
                                        HashMap hmV=arr.get(a);
                                        String existing_created_variation_asin=(String) hmV.get("asin");
                                        String myAsin=rs.getString(1);
                                        if(existing_created_variation_asin.equals(myAsin)){
                                            System.out.println("Prabu what to do ???");
                                        }
                                        
                                    }
                                    
                                }
                            }
                            
                     }
                     
                    PreparedStatement ps=con.prepareStatement("select 1 from tajplaza.variation_sku_upcs  where upc=? and pkg_qty=? and ts< (curdate() - interval 1  day)");
                    //PreparedStatement ps=con.prepareStatement("select 1 from tajplaza.variation_sku_upcs  where upc=? and pkg_qty=? ");
                    ps.setString(1, upc);
                    ps.setInt(2, singlePkgQty*packaging_quant);
                    ResultSet rs=ps.executeQuery();
                    if(rs.next()){
                         tmpInt=(int) Math.floor(Math.random() * 10);
                    }  
                   //newproduct=newproduct.replaceAll("<PACKAGING-QUAN>", Integer.toString(packaging_quant*(int) Math.floor(Math.random() * 101)));
                    //NEW LOGIC FOR USING EXISTING PACKING QTY
                    
                    newproduct=newproduct.replaceAll("<PACKAGING-QUAN>", Integer.toString(packaging_quant*tmpInt*singlePkgQty*Integer.valueOf(tmpInt)));
                  //   newproduct=newproduct.replaceAll("<PACKAGING-QUAN>", Integer.toString(packaging_quant));
                }
                if(hmNew.containsKey("Size")){
                    if(packaging_quant==1 ){
                       newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", (String)hmNew.get("Size") );
                    }else{
                       newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", packaging_quant+" X "+(String)hmNew.get("Size") ); 
                    }
                    
                    //For 1 pack, use existing packing quantity
                    
                }else{
                    if(packaging_quant==1 ){
                        if(singlePkgQty==1){
                            newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", "1 pack" );
                        }else{
                            newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", singlePkgQty+" packs" );
                        }
                       
                    }else{
                        if(singlePkgQty==1){
                            newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", packaging_quant+ " packs" ); 
                        }else{
                            newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", singlePkgQty*packaging_quant+ " packs" ); 
                        }
                       
                    }
                    
                }
                 if(hmNew.containsKey("Flavor")){
                     newproduct=newproduct.replaceAll("<FLAVOR>", (String)hmNew.get("Flavor") );
                     newproduct=newproduct.replaceAll("SizeName", "Flavor-Size" );
                 }else{
                      newproduct=newproduct.replaceAll("<FLAVOR>", "" );
                 }
                newproduct=newproduct.replaceAll("<UNIT_COUNT>", String.valueOf(singlePkgQty*packaging_quant)  ); 
                
               /* if(packaging_quant>1)
                    newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", Integer.toString(packaging_quant) );
                else
                    newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", Integer.toString(packaging_quant)  );
                 * 
                 */

                //float bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
                 Integer rank=(Integer)(hmNew.get("rank")!=null?hmNew.get("rank"):99999);
                

                 float discount_percent=getDiscount(packaging_quant,rank,99999)   ;
                 
                  
   
                String nodeid="";
                String parentnodeid="";
                if(hmNew.containsKey("query-0") && ((String)hmNew.get("query-0")).indexOf("item_type_keyword:")>=0){
                    String query=(String)hmNew.get("query-0");
                    query=query.replaceAll("item_type_keyword:", "");
                    newproduct=newproduct.replaceAll("<ITEM-TYPE>",query);
                } else if(hmNew.containsKey("query-1") && ((String)hmNew.get("query-1")).indexOf("item_type_keyword:")>=0){
                    String query=(String)hmNew.get("query-1");
                    query=query.replaceAll("item_type_keyword:", "");
                    newproduct=newproduct.replaceAll("<ITEM-TYPE>",query);
                }
                else{
                    newproduct=newproduct.replaceAll("<ITEM-TYPE>", "<ITEM-TYPE>");
                }
                    

                String imageurl=(String)(hmNew.get("imageurl")!=null?hmNew.get("imageurl"):"");
               // imageurl="http://localhost/HBImages/"+upc+".jpg";
                newproduct=newproduct.replaceAll("<IMAGE-URL>", imageurl);

                newproduct=newproduct.replaceAll("<FULFILL>", fulfill);

                if(parentSku!=null)
                    newproduct=newproduct.replaceAll("<PARENTSKU>", parentSku);
                //Weight and height measurement.
               // String weight_measure=(String)(hm.get("weight_measure")!=null?hm.get("weight_measure"):"LB");
                 String weight_measure="LB";
                 
                String height="", width="",length="",len_mes="IN",weight="";
                
        

                length=String.valueOf(hmF.get("productInfoLength"));
                width=String.valueOf(hmF.get("productInfoWidth"));
                Float heightF=(Float)hmF.get("productInfoHeight");
                if(heightF==null)
                    heightF=(float)1;
                
                heightF=Math.round(heightF*(float)100)/(float)100;
                
                if(heightF>14)
                    heightF=(float)14;
                        
                height=String.valueOf(heightF*packaging_quant);
                if(height.indexOf(".")>=0)
                    height=StringUtils.substringBefore(height, ".");
                Float weightF=(float)1;
                if(hmF.get("productInfoWeight")!=null)
                   weightF=(Float)hmF.get("productInfoWeight")*packaging_quant;
                weightF=Math.round(weightF*(float)100)/(float)100;
                //if(weightF>19)
                  //  weightF=(float)19;
                weight=String.valueOf(weightF);
                
                 try{
                     if(bestPriceMap==null)
                         bestPriceMap =GetCompetitivePricingForASINSample.getJustBBPriceandRankMWS(asin);
                 }catch (Exception e2){e2.printStackTrace();}
                float bestprice=(Float)(bestPriceMap.get("bestprice")!=null?bestPriceMap.get("bestprice"):(float)0);
                Float suggestedprice= bestprice;
                if(packaging_quant>1){
                    suggestedprice=(float) bestprice*(float)packaging_quant*(float)discount_percent;
                    if(oc_singlepack!=null && oc_singlepack.length()>0){
                         
                        Float minPrice=(Float.valueOf(oc_singlepack)*packaging_quant*(float)1.2+(float)6+weightF*(float).6)/(float).85;
                        Float maxPrice=(Float.valueOf(oc_singlepack)*packaging_quant*(float)1.4+(float)6+weightF*(float).6)/(float).85;
                        if(suggestedprice<minPrice){
                            System.err.println("Exception, not possible min price for multi packs is "+minPrice +" and suggested price is "+suggestedprice);
                            suggestedprice=minPrice;
                          //  System.exit(1);;
                        }else if(suggestedprice>maxPrice){
                            System.err.println(" Limiting multi pack price to 25% margin "+maxPrice +" and suggested price is "+suggestedprice);
                            suggestedprice=maxPrice;
                            
                        }
                    }
                    
                }
                suggestedprice=Math.round(suggestedprice*(float)100)/(float)100;
                newproduct=newproduct.replaceAll("<PRICE>",Float.toString(suggestedprice));
                
                
                newproduct=newproduct.replaceAll("<HEIGHT>", height);
                newproduct=newproduct.replaceAll("<WIDTH>", width);
                newproduct=newproduct.replaceAll("<LENGTH>", length);
                newproduct=newproduct.replaceAll("<LEN-MEAS>", len_mes);
       

                newproduct=newproduct.replaceAll("<WEIGHT>", weight);
                newproduct=newproduct.replaceAll("<WEIGHT-MEAS>", weight_measure);

                FileWriter fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+feedfile_name,true);
                out0 = new BufferedWriter(fstream_out0);

                out0.write(newproduct);
                out0.newLine();
                out0.close();

    }

 

    public static float getDiscount(int packaging_quant,Integer onepackrank,Integer currentItemRank) {
         //return (float).9;
        if(packaging_quant<=3)
           return (float).92;
        else
            return (float).87;
        //else
          //  return (float).9;
        //    if(rank_float<2000)
          //     return (float).98;
        /*if(currentItemRank==null || currentItemRank.equals("NA")){
            if(onepackrank!=null &&currentItemRank.equals("NA")==false )
                currentItemRank=String.valueOf(Integer.valueOf(onepackrank)*2);
            else
                currentItemRank="999999";
            
        }*/
            /*if(Integer.valueOf(currentItemRank)<10000){
                 return (float).99;
            }        
            if(Integer.valueOf(currentItemRank)<20000){
                 return (float).98;
            }        
        
            if(Integer.valueOf(currentItemRank)<40000){
                 return (float).95;
            }        
            if(Integer.valueOf(currentItemRank)<80000){
                 return (float).90;
            }
             if(Integer.valueOf(onepackrank)<1000){
                  return (float).95;
            } */
 
            
           // return (float).95;
 
    }


}
