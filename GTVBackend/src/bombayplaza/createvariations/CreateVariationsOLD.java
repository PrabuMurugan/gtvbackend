/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bombayplaza.createvariations;

import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
 
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import unfi.DrawPackLogo;

/**
 *
 * @author US083274
 */
public class CreateVariationsOLD {
    static String FILTER_ITEM="";
   static String PARENT_TEMPLATE="";
   static String ONEPACK_TEMPLATE="";
   static String JUSTONEPACK_TEMPLATE="";
    static String MULTIPACK_TEMPLATE="";
    static HashMap<String,String> nodes=new HashMap();
    static BufferedWriter out0=null;
    static ArrayList existingProductsArr;
    static HashMap<String,String> existingItemsMap;
    public  static String   feedfile_name;
   static boolean DONOT_JOIN = false;
     static String oc_multipacks="";
     static String  multipacks_sku="";

    public static void createVariation() {
       try{ 
          System.out.println("Value of DONOT_JOIN IS "+DONOT_JOIN);
          // DONOT_JOIN="true";
          if(DONOT_JOIN==true){
                   PARENT_TEMPLATE="";
                 ONEPACK_TEMPLATE="";
                 JUSTONEPACK_TEMPLATE="";
                  MULTIPACK_TEMPLATE="<SKU>	TTT <UPC>	UPC	<TITLE>	<BRAND>	<MANUFA>	<BB1>	<BB2>	<BB3>	<BB4>	<BB5>	<DESCRIPTION>	Food	<PACKAGING-QUAN>	<PRICE>		USD	0				<ITEM-TYPE>																					<IMAGE-URL>									<FULFILL>	<HEIGHT>	<WIDTH>	<LENGTH>	<LEN-MEAS>	<WEIGHT>	<WEIGHT-MEAS>																																																																																																																									";

          }
        String strLine0=null,asin=null,upc=null,sku,title,brand,manu,bb1,bb2,bb3,bb4,bb5,desc,package_quan,price,item_type,image_url;
          FileWriter f= new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\multipack.txt");
          f.close();
        System.out.println("Creating variations DO_NOT_JOIN VALUE is "+DONOT_JOIN);
        existingProductsArr=new ArrayList();
        existingItemsMap=new HashMap();
        boolean variationstobesent=false;
        if(RetrievePage.isOffice()==false){
            FILTER_ITEM="";
        }
        BufferedReader br0;
        FileWriter fstream_out0;
        //Write to the product file
        


        //Populate ignored UPC
        BufferedReader br01  = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\inventorytotallist.txt")));
        String packaging_quant="1",fulfillment="DEFAULT";
        String skuOnFile="NOSKU";
        while ((strLine0 = br01.readLine()) != null )   {
            try{
                upc=strLine0.split("\t")[0].replaceAll("'", "");
                upc=upc.replaceAll("UPC:","");
                upc=upc.replaceAll("'","");
                packaging_quant=strLine0.split("\t")[5];
                fulfillment=strLine0.split("\t")[4];
                skuOnFile=strLine0.split("\t")[1];

            }catch (Exception e){}
            if(upc!=null && upc.length()>0){
                if(upc.indexOf("3284")>=0)
                   // System.out.println("debug");
                    existingItemsMap.put(upc.trim()+"-"+packaging_quant.trim()+"-"+fulfillment.trim(),skuOnFile);
                    String dummyupc;
                    dummyupc=StringUtils.leftPad(upc, 13,'0');
                    existingItemsMap.put(dummyupc.trim()+"-"+packaging_quant.trim()+"-"+fulfillment.trim(),skuOnFile);
                    if(upc.charAt(0)=='0'){
                        dummyupc=upc.substring(1, upc.length());
                        existingItemsMap.put(dummyupc.trim()+"-"+packaging_quant.trim()+"-"+fulfillment.trim(),skuOnFile);
                    }
                    if(upc.charAt(1)=='0'){
                        dummyupc=upc.substring(2, upc.length());
                        existingItemsMap.put(dummyupc.trim()+"-"+packaging_quant.trim()+"-"+fulfillment.trim(),skuOnFile);
                    }

            }



        }
        br01.close();

        populateNodeLinks();
        br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\existing-all-inventory.txt")));
        System.out.println("I AM NOT USING MY EXISTING SKUS WHILE CREATING NEW VARIATIONS");
        /*
        while ((strLine0 = br0.readLine()) != null)   {
            existingProductsArr.add(strLine0 );
        }*/
        br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\feeds\\createvariations.txt")));
        boolean headerWriten=false;
      while ((strLine0 = br0.readLine()) != null)   {

          String packagingQuant=null;
        
          System.out.println("line :"+strLine0);
          title=null;
          if(strLine0.split("\t").length>0){
              //Line has both upc and asin
              upc=strLine0.split("\t")[0];
              if(upc.trim().length()==0)
                  continue;
              upc=upc.replaceAll("\"","").replaceAll("'", "");
              upc=upc.trim();
              asin=strLine0.split("\t")[1];
              if(asin.trim().length()==0)
                  continue;
              asin=asin.trim();
              asin=asin.replaceAll("\"","").replaceAll("'", "");

              if(upc.indexOf("NA")>=0)
                  continue;
               if(strLine0.split("\t").length >=3){
                   title=strLine0.split("\t")[2];
               }else{
                  continue;
               }
                if(strLine0.split("\t").length >=4){
                   packagingQuant=strLine0.split("\t")[3].trim();
               }
                if(strLine0.split("\t").length >=5){
                   oc_multipacks=strLine0.split("\t")[4].trim();
               }        
                if(strLine0.split("\t").length >=6){
                   multipacks_sku=strLine0.split("\t")[5].trim();
               }                 
              if(FILTER_ITEM.length()>0 && strLine0.indexOf(FILTER_ITEM)<0)
                  continue;
                if(title.toLowerCase().indexOf("pack")  >0){
                    System.out.println("WARNING title already contains pack "+title);
                   // continue;
                }
               HashMap hm=null;
               
              feedfile_name = "multipack.txt";
            fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+feedfile_name,true);
            out0 = new BufferedWriter(fstream_out0);
            BufferedReader br00=null;
            if(DONOT_JOIN==false)
                br00=new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\feeds\\Flat.File.FoodAndBeverages-template.txt")));
            else
                br00=new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\feeds\\Flat.File.FoodAndBeverages-template_JUSTMULTIPACK.txt")));
           for(int i=0;i<10;i++){
               strLine0 = br00.readLine();
               if(strLine0==null)
                   break;
               if(i<=2 && headerWriten==false){
                out0.write(strLine0);
                out0.newLine();                   
               } 
               
               if(DONOT_JOIN==false){
                   if(i==3)
                    PARENT_TEMPLATE=strLine0;
                   if(i==4)
                    ONEPACK_TEMPLATE=strLine0;
                   if(i==5)
                    MULTIPACK_TEMPLATE=strLine0;
                   
               }else{
                   if(i==3)
                    MULTIPACK_TEMPLATE=strLine0;                   
               }
                
               
           }
           headerWriten=true;
           /* while ((strLine0 = br00.readLine()) != null )   {
                out0.write(strLine0);
                out0.newLine();
            }*/
            br00.close();
            out0.close();
            if(existingItemsMap.containsKey(upc.trim()+"-"+String.valueOf(packagingQuant).trim()+"-DEFAULT") ){

              String skuonfile=existingItemsMap.get(upc.trim()+"-"+String.valueOf(packagingQuant).trim()+"-DEFAULT");
                    //sku=existingItemsMap.get(upc.trim()+"-"+String.valueOf(packaging_quant).trim()+"-"+fulfill.trim());
               // System.out.println("ITEM ALREADY EXISTS BUT STILL I GO AND CREATE.  SKU for "+upc.trim()+"-"+String.valueOf(packaging_quant).trim()+"-AMAZON_NA"+" SKU:"+skuonfile);
               // continue;

            }
                
        HashMap hmF=RetrievePage.getDetailsFromFbaCalculatorNewMWS(null,asin,null,true);
        
         hm=createVaration(null,hmF,upc,asin,Integer.valueOf(packagingQuant),"DEFAULT",title);
       //  if(Integer.valueOf(packagingQuant)!=1)
          //   hm=createVaration(hm,hmF,upc,asin,Integer.valueOf(packagingQuant),"DEFAULT",title);


            if(hm!=null)
               variationstobesent=true;

          }
      }
      br0.close();
      if(false && variationstobesent){
        System.out.println("Calling email service to email new-inventroy.csv");
          String[] to={"mprabagar80@gmail.com" };
        String[] cc={};
        String[] bcc={};

        Mail.sendMail( "smtpout.secureserver.net","465","true",
        "true",true,"javax.net.ssl.SSLSocketFactory","false",to,cc,bcc,
        "NEW VARIATIONS - PRODUCTS TO BE CREATED", "Please veriy and upload the attached variations file Flat.File.FoodAndBeverages.txt",false,true,false,false,null);
          
      }

    }catch (Exception e){
        e.printStackTrace();
    }
    }
     boolean JUST_MULTIPACK=true;
public static void main(String args[]){
    createVariation();
    
}

    private static HashMap createVaration(HashMap hm, HashMap hmF,String upc, String asin, int packaging_quant, String fulfill,String title)  throws Exception{
        String strLine0=null,sku,brand,manu,bb1,bb2,bb3,bb4,bb5,desc,package_quan,price,item_type,image_url;
        String curL;

       
          asin=asin.trim();
          String onepacksku=null;
       for(int i=0;i<existingProductsArr.size();i++){
            //check if the particular item is already created.
            //also check for the fulfill also.
            curL=((String) existingProductsArr.get(i)) ;
           ;
           if(curL.indexOf(asin)  >=0  && curL.indexOf("DEFAULT")>0 ){
               System.out.println("Asin already exists in :"+curL);
               onepacksku=curL.split("\t")[3];
               
           }
        }
        if(onepacksku==null){
                Date date = Calendar.getInstance().getTime();
                DateFormat formatter = new SimpleDateFormat("MMddhhmmssSSS");
                String today = formatter.format(date);
                System.out.println("Today : " + today);
                onepacksku="VAR_"+today;            
         //   System.out.println("I can not proceed without 1 pack SKU");
           // return hm;
        }

        System.out.println("GOING TO CREATE VARIATION:"+upc+","+asin+","+title+","+packaging_quant+","+fulfill);
            if(hm==null){
                MatchThePrice m=new MatchThePrice();
                m.DOWNLOAD_IMAGE=true;
                 hm=m.getProductDetails(asin,false,false,false);
                 

            }else{
            //Wait a 100 ms, so i get a new SKU
                Thread.sleep(100);
            }
            
              if(false && hm.containsKey("LOCAL_IMAGE") && packaging_quant>1){
                  String localImage=(String) hm.get("LOCAL_IMAGE");
                  DrawPackLogo.drawAmazonPackLogo(localImage, packaging_quant);
                  FileUtils.copyFile(new File("C:\\Temp2012\\images\\overLayedImage.jpg"), new File("C:\\apache-tomcat-7-prod\\webapps\\HBImages\\"+upc+".jpg"));
              }
              //Create new SKU
                Date date = Calendar.getInstance().getTime();
                DateFormat formatter = new SimpleDateFormat("MMddhhmmssSSS");
                String today = formatter.format(date);
                System.out.println("Today : " + today);
                sku="VAR_"+today;
                String parentSku=null;

                if(hm.get("parentSku")==null)
                    parentSku="PARENT"+today;
                else
                   parentSku= (String) hm.get("parentSku");
                hm.put("parentSku", parentSku);


               /* if(packaging_quant>1){
                    //In case of amazon fulfillment, i hope already the other rows are written..so just add the fba row.
                  sku+="_VAR";
                  writeToNewVariationFile("multipack",sku, hm,hmF, upc,parentSku,  asin,  packaging_quant,  fulfill, title);
                  return hm;
                }     */           
                if(packaging_quant==1){
                    writeToNewVariationFile("just1pack",onepacksku, hm,hmF, upc,"",  asin,  1,  fulfill, title);
                    return hm;
                }
                 writeToNewVariationFile("parent",parentSku,hm,hmF, upc,null,  asin,  1,  fulfill, title);
                 writeToNewVariationFile("1pack",onepacksku, hm,hmF, upc,parentSku,  asin,  1,  fulfill, title);
                 sku+="_FBA";
                 if(oc_multipacks.length()>0)
                     sku=sku+"_"+oc_multipacks;                 
                 if(multipacks_sku.length()>0){
                     sku=multipacks_sku ;
                 }
                 

                     
                 
                 writeToNewVariationFile("multipack",sku, hm,hmF, upc, parentSku, asin,  packaging_quant,  fulfill, title);
                return hm;
    }
 private static void writeToNewVariationFile(String type,String sku, HashMap hm,HashMap hmF, String upc,String parentSku, String asin, int packaging_quant, String fulfill,String title) throws IOException {
                String strLine0=null ,brand,manu,bb1,bb2,bb3,bb4,bb5,desc,package_quan,price,item_type,image_url;
                String newproduct=null;
                if(type.equals("parent")){
                    newproduct=PARENT_TEMPLATE;
                }else if(type.equals("1pack")){
                    newproduct=ONEPACK_TEMPLATE;
                }else if(type.equals("multipack")){
                    newproduct=MULTIPACK_TEMPLATE;
                }
                else if(type.equals("just1pack")){
                    newproduct=JUSTONEPACK_TEMPLATE;
                }

                if(newproduct.length()==0)
                    return;
                newproduct=newproduct.replaceAll("<SKU>", sku);
                newproduct=newproduct.replaceAll("<UPC>", upc);

                title=(String)(hm.get("title")!=null?hm.get("title"):"NA");
                if(title.indexOf("Amazon.com")>=0)
                    title=title.replaceAll("Amazon.com : ","");
                if(title.indexOf("Pack")>0 && title.indexOf("(")>0)
                    title=StringUtils.substringBeforeLast(title, "(");
                if(packaging_quant>1)
                    //title= title + ":(Pack Of "+packaging_quant+")";
                    title=packaging_quant +" Packs :"+title;
                newproduct=newproduct.replaceAll("<TITLE>", title);

                brand=(String)(hm.get("manu")!=null?hm.get("manu"):"NA");
                newproduct=newproduct.replaceAll("<BRAND>", brand);

                newproduct=newproduct.replaceAll("<MANUFA>", brand);


                bb1=(String)(hm.get("bb1")!=null?hm.get("bb1"):"");
                /*if(packaging_quant>1){
                    bb1="'"+packaging_quant+"'' packs";
                }*/
                newproduct=newproduct.replaceAll("<BB1>", bb1);

                bb2=(String)(hm.get("bb2")!=null?hm.get("bb2"):"");
                newproduct=newproduct.replaceAll("<BB2>", bb2);

                bb3=(String)(hm.get("bb3")!=null?hm.get("bb3"):"");
                newproduct=newproduct.replaceAll("<BB3>", bb3);

                bb4=(String)(hm.get("bb4")!=null?hm.get("bb4"):"");
                newproduct=newproduct.replaceAll("<BB4>", bb4);

                bb5=(String)(hm.get("bb5")!=null?hm.get("bb5"):"");
                newproduct=newproduct.replaceAll("<BB5>", bb5);

                desc=(String)(hm.get("desc_txt")!=null?hm.get("desc_txt"):"");
                desc = desc.replaceAll("[^\\x00-\\x7F]", "");
                desc=desc.replaceAll("\n","").replaceAll("\r","");
                newproduct=newproduct.replaceAll("<DESCRIPTION>", desc);

                    
                
                if(packaging_quant==1){
            //First get the package quantity and size from amazon
                    ArrayList<HashMap> matchingProducts =null;// ListMatchingProductsSample.getMatchingProducts(upc);
                  //  matchingProducts.get(0).
                }
                newproduct=newproduct.replaceAll("<PACKAGING-QUAN>", Integer.toString(packaging_quant*(int) Math.floor(Math.random() * 101)));
                if(packaging_quant>1)
                    newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", Integer.toString(packaging_quant) +" packs");
                else
                    newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", Integer.toString(packaging_quant));

                float bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
                 String rank=(String)(hm.get("rank")!=null?hm.get("rank"):"NA");
               
                if(rank!=null && rank.indexOf("#") >=0&& rank.indexOf("in")>=0  ){
                    System.out.println("Input rank CONTAINS # :"+rank);
                }
          


                 float discount_percent=getDiscount(packaging_quant,rank,"99999")   ;
                 
                  


                Float suggestedprice= bestprice;
                if(packaging_quant>1)
                    suggestedprice=(float) bestprice*(float)packaging_quant*(float)discount_percent;
               suggestedprice=suggestedprice-5;
               if(suggestedprice>100)
                   suggestedprice=suggestedprice*(float).9;
                suggestedprice=Math.round(suggestedprice*(float)100)/(float)100;
               /* if(suggestedprice>100){
                    System.out.println("I do not sell anything more than $100. Continue");
                    return ;
                }*/
                newproduct=newproduct.replaceAll("<PRICE>",Float.toString(suggestedprice));

                String nodeid=(String)(hm.get("nodeid")!=null?hm.get("nodeid"):"");
                String parentnodeid=(String)(hm.get("parentnodeid")!=null?hm.get("parentnodeid"):"");
                
                if(nodes.get(nodeid)!=null)
                    newproduct=newproduct.replaceAll("<ITEM-TYPE>", nodes.get(nodeid));
                else if (nodes.get(parentnodeid)!=null && nodes.get(parentnodeid).length()>0)
                    newproduct=newproduct.replaceAll("<ITEM-TYPE>", nodes.get(parentnodeid));
                else
                    newproduct=newproduct.replaceAll("<ITEM-TYPE>", "indian-food");

                String imageurl=(String)(hm.get("imageurl")!=null?hm.get("imageurl"):"");
               // imageurl="http://localhost/HBImages/"+upc+".jpg";
                newproduct=newproduct.replaceAll("<IMAGE-URL>", imageurl);

                newproduct=newproduct.replaceAll("<FULFILL>", fulfill);

                if(parentSku!=null)
                    newproduct=newproduct.replaceAll("<PARENTSKU>", parentSku);
                //Weight and height measurement.
               // String weight_measure=(String)(hm.get("weight_measure")!=null?hm.get("weight_measure"):"LB");
                 String weight_measure="LB";
                 
                String height="", width="",length="",len_mes="IN",weight="";
                
        

                length=String.valueOf(hmF.get("productInfoLength"));
                width=String.valueOf(hmF.get("productInfoWidth"));
                Float heightF=(Float)hmF.get("productInfoHeight");
                if(heightF==null)
                    heightF=(float)1;
                
                heightF=Math.round(heightF*(float)100)/(float)100;
                
                if(heightF>14)
                    heightF=(float)14;
                        
                height=String.valueOf(heightF*packaging_quant);
                if(height.indexOf(".")>=0)
                    height=StringUtils.substringBefore(height, ".");
                Float weightF=(float)1;
                if(hmF.get("productInfoWeight")!=null)
                   weightF=(Float)hmF.get("productInfoWeight")*packaging_quant;
                weightF=Math.round(weightF*(float)100)/(float)100;
                //if(weightF>19)
                  //  weightF=(float)19;
                weight=String.valueOf(weightF);
                

                newproduct=newproduct.replaceAll("<HEIGHT>", height);
                newproduct=newproduct.replaceAll("<WIDTH>", width);
                newproduct=newproduct.replaceAll("<LENGTH>", length);
                newproduct=newproduct.replaceAll("<LEN-MEAS>", len_mes);
       

                newproduct=newproduct.replaceAll("<WEIGHT>", weight);
                newproduct=newproduct.replaceAll("<WEIGHT-MEAS>", weight_measure);

                FileWriter fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+feedfile_name,true);
                out0 = new BufferedWriter(fstream_out0);

                out0.write(newproduct);
                out0.newLine();
                out0.close();

    }

    private static void populateNodeLinks() throws Exception{
        String strLine0;
       BufferedReader br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\grocery_browse_tree_guide.txt")));
      while ((strLine0 = br0.readLine()) != null )   {
              //System.out.println("Inside populateNodeLinks:"+strLine0);
          if(strLine0.split("\t").length>2){
            String nodeid=strLine0.split("\t")[0];
            String nodequery=strLine0.split("\t")[2];
            nodequery=nodequery.replaceAll("item_type_keyword:", "");
            if(nodes.containsKey(nodeid)){
              //  System.out.println("nodeid :"+nodeid+" already present");
                continue;
            }
            nodes.put(nodeid,nodequery);

          }

      }
    }

    public static float getDiscount(int packaging_quant,String onepackrank,String currentItemRank) {
        
        if(packaging_quant<=3)
            return (float).95;
        else
            return (float).9;
        //    if(rank_float<2000)
          //     return (float).98;
        /*if(currentItemRank==null || currentItemRank.equals("NA")){
            if(onepackrank!=null &&currentItemRank.equals("NA")==false )
                currentItemRank=String.valueOf(Integer.valueOf(onepackrank)*2);
            else
                currentItemRank="999999";
            
        }*/
            /*if(Integer.valueOf(currentItemRank)<10000){
                 return (float).99;
            }        
            if(Integer.valueOf(currentItemRank)<20000){
                 return (float).98;
            }        
        
            if(Integer.valueOf(currentItemRank)<40000){
                 return (float).95;
            }        
            if(Integer.valueOf(currentItemRank)<80000){
                 return (float).90;
            }
             if(Integer.valueOf(onepackrank)<1000){
                  return (float).95;
            } */
 
            
           // return (float).95;
 
    }


}
