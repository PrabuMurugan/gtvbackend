/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bombayplaza.createvariations;

/**
 *
 * @author prabu
 */

import amazon.product.ProcessWorkingCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
//import com.tajplaza.products.GetCompetitivePricingForASINSample;
//import com.tajplaza.products.ListMatchingProductsSample;
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetLowestPricedOffersForASINSample;
import com.tajplaza.products.ListMatchingProductsSample;
import com.tajplaza.upload.UploadInventoryLoaderFeedFile;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

public class AdjustCreatedVariationsPrices {
    //update variation_sku_upcs set ts=(curdate() - interval 180 day)  where sku='VAR_B00TGE5U0G_AMA_3PACKS_24'
    public static String DEBUG_SQL=" and sku='VAR_B00BSHB4XG_AMA_3PACKS_21'";
  // public static String DEBUG_SQL="";
   static HashMap<String,HashMap> matchingProductsMap=new HashMap();
 public static void  main(String args[]) throws Exception  {
            String PRODUCTION=System.getProperty("PRODUCTION","false");
             
            if(PRODUCTION.equals("true")){
                DEBUG_SQL="";
            }

                
     Connection con=RetrievePage.getConnection();
     //String sql="select seller_sku,asin1,v.pkg_qty,oc_multipack,e.price,onepack_asin from tajplaza.existing_all_inventory e , tajplaza.variation_sku_upcs v where  onepack_asin is  not null and v.sku=e.seller_sku  and (quantity>0 or 1pack_rank is null or min_price is null or cast(1pack_bestprice as decimal)=0 or (v.sku in (select sellersku from tajplaza.orders where  purchaseDate > date_sub(curdate(), interval 90 day)  and v.ts< (curdate() - interval 7  day)))) "+DEBUG_SQL+"  order by v.ts  ";
     String sql="  select seller_sku,asin1,v.pkg_qty,oc_multipack,e.price,onepack_asin,1pack_rank from tajplaza.existing_all_inventory e , tajplaza.variation_sku_upcs v where  onepack_asin is  not null and v.sku=e.seller_sku  and (quantity>0 or 1pack_rank is null or min_price is null or cast(1pack_bestprice as decimal)=0 or   v.ts< (curdate() - interval 7  day) ) "+DEBUG_SQL+"  order by v.ts  ";
     System.out.println("Executing sql:"+sql);
     PreparedStatement ps=con.prepareStatement(sql);
     PreparedStatement update_variation_sku_upcs=con.prepareStatement("update tajplaza.variation_sku_upcs  set 1pack_bestprice=?, 1pack_rank=?, multipack_bestprice=?, suggested_price=?,min_price=?,weight=?  where sku=?");
       //      + "and seller_sku='CO_125_43607_B002978HHA_3PACKS_24'");
     //PreparedStatement psStuck=con.prepareStatement("select v.sku from tajplaza.variation_sku_upcs v, tajplaza.amazon_min_price m where v.sku=m.sku and (m.custom_note like '%Stuck%' or m.custom_note like '%Expiring%') and v.sku=?");
     
     ResultSet rs=ps.executeQuery();
    FileWriter fstream_out0;
    BufferedWriter out0;
    fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\tmp\\prabu\\multiPACKSPrices.txt");
    out0 = new BufferedWriter(fstream_out0);
    out0.write("sku\tprice");
    out0.newLine();
    out0.close();
    PreparedStatement psSellery=con.prepareStatement("select  new_buy_box_price_plus_shipping_on_amazon from sellery_export where sku=? ");
     HashMap asinPricesOnAmazon = new HashMap();
     ArrayList asinsProcessed=new ArrayList();
     while(rs.next()){
         String notes="";
         try{
             String sku=rs.getString("seller_sku");
             String onepack_rank=rs.getString("1pack_rank");
             if(onepack_rank!=null && sku.indexOf("DT")>=0){
                 System.out.println("Dot sku, onepack_rank is present skipping");
                 continue;
             }
             String asin=rs.getString("asin1");
             System.out.println("Processing SKU:"+sku + "asin:"+asin);
             //if(asinsProcessed.contains(asin))
               //  continue;
             asinsProcessed.add(asin);
             Integer pkg_qty=rs.getInt("pkg_qty");
             Float curPrice=rs.getFloat("price");
             Float oc_multipack=rs.getFloat("oc_multipack");
            // if(oc_multipack==0)
             //    continue;
             String onePackAsin="";
             if(onePackAsin==null ||onePackAsin.length()==0 && rs.getString("onepack_asin")!=null &&  rs.getString("onepack_asin").indexOf("B")>= 0){
                 onePackAsin=rs.getString("onepack_asin");
             }
             if(sku.indexOf("PACKS")>=0 && (onePackAsin==null || onePackAsin.length()==0  )){
                 String arr[]=sku.split("_");

                 for(int i=0;i<arr.length;i++){
                     if(arr[i].indexOf("B")==0){
                         onePackAsin=arr[i];
                     }
                 }
                 
             }

             HashMap hm=RetrievePage.getDetailsFromFbaCalculatorNewMWS(con, asin, "100", true);
             Float weightF=(Float) hm.get("productInfoWeight");
             if(weightF == null)
                 weightF=(float)5.99;
             float shippingFee=0;
             if(weightF<15)
                shippingFee=(float)6+weightF*(float).6;
             else
                 shippingFee=(float)10+weightF*(float).4;
             if(sku.indexOf("DT_")>=0){
                 if(weightF<10)
                     shippingFee=14;
                 else
                     shippingFee=14+(weightF-10);
             }
                 
                 
             Float minPrice=(oc_multipack*(float)1.15+shippingFee)/(float).85;
             Float maxPrice=(float)999;
             float discount_percent=CreateVariationsMWS.getDiscount(pkg_qty,99,99999)   ;
             
            // psStuck.setString(1, sku);
            // ResultSet rsStuck=psStuck.executeQuery();
           //  if(rsStuck.next()){
           //      minPrice=(oc_multipack*(float).7*(float)1.2+shippingFee)/(float).85;
            //     discount_percent=(float).8;
           //  }
              Float suggestedprice=(float)100;
              Float lowPrice1Pack=(float)0;
              int onepackrank=99999;
              HashMap hm2=null;
             if(onePackAsin!=null && onePackAsin.indexOf("B")>=0){
                 if(asinPricesOnAmazon.containsKey(onePackAsin)){
                     lowPrice1Pack=(Float) asinPricesOnAmazon.get(onePackAsin);
                     
                      suggestedprice=(float) lowPrice1Pack*(float)pkg_qty*(float)discount_percent;
                     
                 }else{
                     if(matchingProductsMap.containsKey(onePackAsin)){
                         hm2=matchingProductsMap.get(onePackAsin);
                    }else{
                         hm2  =    GetCompetitivePricingForASINSample.getJustBBPriceandRankMWS(onePackAsin);
                         matchingProductsMap.put(onePackAsin, hm2);
                     }
                     lowPrice1Pack= (Float) hm2.get("bbPriceEvenIfMe");  
                     System.out.println("one pack best price for asin:"+onePackAsin +" is "+lowPrice1Pack);
                      onepackrank= (Integer) hm2.get("rank");  
                      suggestedprice=(float) lowPrice1Pack*(float)pkg_qty*(float)discount_percent;
                     
                 }
             }
              
             psSellery.setString(1, sku);
             psSellery.executeQuery();
             ResultSet rSellery=psSellery.executeQuery();
              Float currentLowPrice=(float)0;
              boolean rowExistsInSellery=false;
             if(rSellery.next()){
                 rowExistsInSellery=true;
                 try{
                     if(rSellery.getFloat(1)>0){
                         currentLowPrice=rSellery.getFloat(1);
                     }
                     if(currentLowPrice>0 && Math.abs(currentLowPrice-suggestedprice)>15){
                         currentLowPrice=(float)0;
                         rowExistsInSellery=false;
                         System.out.println("Sometimes the current low price is  not coming properly in sellery_export, lets get it from MWS API.");
                     }
                 }catch (Exception e2){e2.printStackTrace();}
             }
             if(rowExistsInSellery == false && currentLowPrice==0){
                 System.out.println("Why no price found in sellery for sku:"+sku);
                // GetCompetitivePricingForASINSample.I_NEED_NUMBER_OF_FBA_SELLES=true;
              HashMap currentHM= GetCompetitivePricingForASINSample.getJustBBPriceandRankMWS(asin);
              currentLowPrice= (Float) currentHM.get("bestprice");  
               
              String lowPriceFulfillmentChannel= "";
              if(currentHM.containsKey("lowPriceFulfillmentChannel"))
             lowPriceFulfillmentChannel= (String) currentHM.get("lowPriceFulfillmentChannel");  
              if( (sku.indexOf("VAR")>=0 ||sku.indexOf("AMA")>=0 ) && lowPriceFulfillmentChannel.equals("Merchant"))
                      currentLowPrice=currentLowPrice*(float)1.08;
              /*lowPriceFulfillmentChannel=(String)currentHM.get("lowPriceFulfillmentChannel");
              if(currentHM.containsKey("NUMBEROFFBASELLERS")==false)
                  System.out.println("Why");
              numberOfFbaSellers=(Integer)currentHM.get("NUMBEROFFBASELLERS");
              
              if(lowPriceFulfillmentChannel.toLowerCase().equals("amazon")){
                  currentLowPrice=currentLowPrice-(float).5;
              }*/
            } 
             
             
            
             
              
             
            if(currentLowPrice>0 && currentLowPrice<suggestedprice)
                suggestedprice=currentLowPrice;
            if(oc_multipack>0){
                if(suggestedprice<minPrice){
                    notes=" not possible min price for multi packs is "+minPrice +" and suggested price is "+suggestedprice +" weight is :"+weightF;
                    System.err.println(notes);
                    /*if(sku.indexOf("_AMA")>=0 || sku.indexOf("VAR_")>=0){
                        System.out.println("_AMA product, setting low price even if it is lower than in price");
                        if(suggestedprice<minPrice*.9){
                            suggestedprice=minPrice*(float).9;
                        }
                    }else
                     * */
                    {
                        suggestedprice=minPrice;
                    }

                  //  System.exit(1);;
                }else if(suggestedprice>maxPrice){
                    notes="Limiting multi pack price to 25% margin "+maxPrice +" and suggested price is "+suggestedprice;
                    System.err.println(notes);
                    suggestedprice=maxPrice;

                }
                
            }
             suggestedprice=Math.round(suggestedprice*(float)100)/(float)100;
             update_variation_sku_upcs.setFloat(1, lowPrice1Pack);
             update_variation_sku_upcs.setInt(2, onepackrank);
             update_variation_sku_upcs.setFloat(3, currentLowPrice);
             update_variation_sku_upcs.setFloat(4, suggestedprice);
             update_variation_sku_upcs.setFloat(5, minPrice);
             update_variation_sku_upcs.setFloat(6, weightF);
             
             
             update_variation_sku_upcs.setString(7, sku);
             update_variation_sku_upcs.executeUpdate();;
             //if(Math.abs(curPrice-suggestedprice)>.01)
             {
                 System.out.println("Suggested price for "+sku+ " is "+suggestedprice);
                fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\tmp\\prabu\\multiPACKSPrices.txt",true);
                out0 = new BufferedWriter(fstream_out0);

                out0.write(sku+"\t"+suggestedprice);
                out0.newLine();             
                out0.close();;
                 
             }
  
         }catch (Exception e){e.printStackTrace();}

         
    }
     out0.close();
    //  File file=new File("C:\\Google Drive\\Dropbox\\tmp\\prabu\\multiPACKSPrices.txt");
     // UploadInventoryLoaderFeedFile.uploadFileIntoAmazon(file);
       
     System.out.println("AdjustCreatedVariationsPrices completed");
 }
}
