/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bombayplaza.createvariations;

import amazon.product.ProcessWorkingCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
//import com.tajplaza.products.GetCompetitivePricingForASINSample;
//import com.tajplaza.products.ListMatchingProductsSample;
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetMatchingProductSample;
import com.tajplaza.products.ListMatchingProductsSample;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import unfi.DrawPackLogo;

/**
 *
 * @author US083274
 */
public class CreateVariationsMWS {
    static String FILTER_ITEM="";
   static String PARENT_TEMPLATE="";
   static String ONEPACK_TEMPLATE="";
   static String JUSTONEPACK_TEMPLATE="";
    static String MULTIPACK_TEMPLATE="";
 
    static BufferedWriter out0=null;
    
   static boolean DONOT_JOIN = true;
 static String oc_singlepack="0";
 static String  multipacks_sku="";
 static int singlePkgQty=1; 
  
  static ArrayList ourPreviousCreatedAsins=new ArrayList();
 static WebClient webClient1 ;
 static String globalProductCategoryId="";
 static String globalCategoryStringPrefix="";
 static String globalTemplateFile="";
 static String globalFeedFile="";
 static HashMap<String,HashMap> matchingProductsMap=new HashMap();
 private static HashMap<String,HashMap> bestPriceMapofAllAsins=new HashMap();
 static ArrayList skusWritten=new ArrayList();
  public static void main(String args[]) throws Exception{
    con=RetrievePage.getConnection();
   FileWriter  fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\new-inventory.txt");
    out0 = new BufferedWriter(fstream_out0);
    out0.write(NEW_INVENTORY_STR);
    out0.newLine();
    out0.close();

    fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\delete-old-inventory.txt");
    out0 = new BufferedWriter(fstream_out0);
    out0.write("sku\tadd-delete");
    out0.newLine();
    out0.close(); 
    
    fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\error-inventory.txt");
    out0 = new BufferedWriter(fstream_out0);
    out0.write("Error Message");
    out0.newLine();
    out0.close();   
    
    //Grocery    
    globalProductCategoryId="grocery_display_on_website";
    globalCategoryStringPrefix="NA" ;  
    globalTemplateFile="Flat.File.FoodAndBeverages-template.txt";
    globalFeedFile="multipack_grocery.txt";
    FileWriter f= new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+globalFeedFile);
    f.close();   
    createVariation();  
    
    globalProductCategoryId="health_and_beauty_display_on_website";
    globalCategoryStringPrefix="NA" ;  
    globalTemplateFile="Flat.File.Health-PersonalCare-template.txt";
    globalFeedFile="multipack_health.txt";
     f= new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+globalFeedFile);
    f.close();    
    createVariation();
    
    //Grocery    
    globalProductCategoryId="home_garden_display_on_website";
    globalCategoryStringPrefix="NA" ;
    globalTemplateFile="Flat.File.Home.txt";
    globalFeedFile="multipack_homegarden.txt";
    f= new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+globalFeedFile);
    f.close();        
    createVariation();
    
    //Grocery    
    globalProductCategoryId="art_and_craft_supply_display_on_website";
    globalCategoryStringPrefix="NA" ;
    globalTemplateFile="Flat.File.Home.txt";
    globalFeedFile="multipack_homeart.txt";
    f= new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+globalFeedFile);
    f.close();        
    createVariation();
    
    globalProductCategoryId="kitchen_display_on_website";
    globalCategoryStringPrefix="Home & Kitchen" ;
    globalTemplateFile="Flat.File.Home.txt";
    globalFeedFile="multipack_homekitchen.txt";
    f= new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+globalFeedFile);
    f.close();        
    createVariation();
    
  
       
        
  
    
    //Create new variation
    globalProductCategoryId="biss_display_on_website";
    globalCategoryStringPrefix="NA" ;  
    globalTemplateFile="Flat.File.Industrial.txt";
    globalFeedFile="multipack_industrial.txt";
    f= new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+globalFeedFile);
    f.close();    
    createVariation();        
  
    //Create new variation
    globalProductCategoryId="beauty_display_on_website";
    globalCategoryStringPrefix="NA" ;  
    globalTemplateFile="Flat.File.Beauty.txt";
    globalFeedFile="multipack_beauty.txt";
    f= new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+globalFeedFile);
    f.close();    
    createVariation();     

    //Create new variation
    globalProductCategoryId="office_product_display_on_website";
    globalCategoryStringPrefix="NA" ;  
    globalTemplateFile="Flat.File.Office.txt";
    globalFeedFile="multipack_office.txt";
    f= new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+globalFeedFile);
    f.close();    
    createVariation();     

        //Create new variation
    globalProductCategoryId="pet_products_display_on_website";
    globalCategoryStringPrefix="" ;  
    globalTemplateFile="Flat.File.PetSupplies.txt";
    globalFeedFile="multipack_pet.txt";
    f= new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+globalFeedFile);
    f.close();    
    createVariation();     

    //Scan through multi pack files and delete those that has only 3 lines
    deleteEmptyFeedFiles(new File("C:\\Google Drive\\Dropbox\\feeds\\"));
    
    System.out.println("UNPROCESSED ASINS : "+unprocessedAsins.toString());
    
}
  
  static ArrayList unprocessedAsins=new ArrayList();
  static ArrayList processedUPCs=new ArrayList();
  static int globalRequestedPkgQty=1;
    public static void createVariation() {
        System.out.println("Inside CreateVariation : Processing for :"+globalProductCategoryId);
       try{ 
          System.out.println("Value of DONOT_JOIN IS "+DONOT_JOIN);
          // DONOT_JOIN="true";
          if(DONOT_JOIN==true){
                   PARENT_TEMPLATE="";
                 ONEPACK_TEMPLATE="";
                 JUSTONEPACK_TEMPLATE="";
                  MULTIPACK_TEMPLATE="<SKU>	TTT <UPC>	UPC	<TITLE>	<BRAND>	<MANUFA>	<BB1>	<BB2>	<BB3>	<BB4>	<BB5>	<DESCRIPTION>	Food	<PACKAGING-QUAN>	<PRICE>		USD	0				<ITEM-TYPE>																					<IMAGE-URL>									<FULFILL>	<HEIGHT>	<WIDTH>	<LENGTH>	<LEN-MEAS>	<WEIGHT>	<WEIGHT-MEAS>																																																																																																																									";

          }
        String strLine0=null,asin=null,upc=null,sku,title,brand,manu,bb1,bb2,bb3,bb4,bb5,desc,package_quan,price,item_type,image_url;
 
        System.out.println("Creating variations DO_NOT_JOIN VALUE is "+DONOT_JOIN);
        boolean variationstobesent=false;
        if(RetrievePage.isOffice()==false){
            FILTER_ITEM="";
        }
        BufferedReader br0;
        FileWriter fstream_out0;
        //Write to the product file
        


        //Get UPC for asin
        webClient1 = new WebClient(BrowserVersion.getDefault(),"proxydirect.tycoelectronics.com", 80);
       

 
        br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\existing-all-inventory.txt")));
        System.out.println("I AM NOT USING MY EXISTING SKUS WHILE CREATING NEW VARIATIONS");
        /*
        while ((strLine0 = br0.readLine()) != null)   {
            existingProductsArr.add(strLine0 );
        }*/
        br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\feeds\\createvariations.txt")));
        boolean headerWriten=false;
      while ((strLine0 = br0.readLine()) != null)   {

          String packagingQuant="1";
        
          System.out.println("line :"+strLine0);
          title="";
          if(strLine0.split("\t").length>0){
              //Line has both upc and asin
              if(strLine0.indexOf("upc")>=0)
                  //header
                  continue;
              if(strLine0.indexOf("012546308014")>=0)
                System.out.println("debug");
              upc=strLine0.split("\t")[0];
              if(upc.trim().length()==0)
                  continue;
              upc=upc.replaceAll("\"","").replaceAll("'", "");
              upc=upc.trim();
              asin="NA";
              if(strLine0.indexOf("\t")>0 &&   strLine0.split("\t").length>=1){
                      asin=strLine0.split("\t")[1];
              }
                  
              //if(asin.trim().length()==0 || asin.indexOf("B")<0)
              //    continue;
              asin=asin.trim();
              asin=asin.replaceAll("\"","").replaceAll("'", "");

              if(upc.indexOf("NA")>=0)
                  continue;
               if(strLine0.split("\t").length >=3){
                   title=strLine0.split("\t")[2];
               } 
                if(strLine0.split("\t").length >=4){
                   packagingQuant=strLine0.split("\t")[3].trim();
                   globalRequestedPkgQty=Integer.valueOf(packagingQuant);
               }
                if(strLine0.split("\t").length >=5){
                   oc_singlepack=strLine0.split("\t")[4].trim();
               }      
                singlePkgQty=1;
                if(strLine0.split("\t").length >=6){
                   singlePkgQty=Integer.parseInt(strLine0.split("\t")[5].trim());
               }  
                  if( (strLine0.indexOf("SC_")>=0 ||strLine0.indexOf("CASE")>=0) && 
                      strLine0.split("\t").length >=7 && strLine0.split("\t")[6].trim().length()>0){
                    multipacks_sku=strLine0.split("\t")[6].trim();
                   if(multipacks_sku.indexOf("PACKS")<0){
                       if(multipacks_sku.indexOf("AMA")>=0){
                           String[] arr=multipacks_sku.split("_");
                           String newmultipacksku="VAR_";
                           for(int i=0;i<arr.length;i++){
                               String txt=arr[i];
                               if(arr[i].indexOf("PACK")>=0){
                                   txt=packagingQuant+"PACKS";
                               }
                               else if(i==arr.length-1){
                                    txt=String.valueOf(Integer.valueOf(txt)*Integer.valueOf(packagingQuant));
                               }
                               newmultipacksku=newmultipacksku+txt;
                               if(i<arr.length-1)
                                   newmultipacksku=newmultipacksku+"_";
                           }
                           multipacks_sku=newmultipacksku;
                       }
                       else if(multipacks_sku.indexOf("_")>0 &&multipacks_sku.indexOf("VAR_")<0 && multipacks_sku.indexOf("DT_")<0 ){
                           String tmp=StringUtils.substringBeforeLast(multipacks_sku, "_");
                           multipacks_sku=tmp+"_"+asin+"_"+packagingQuant+"PACKS_"+StringUtils.substringAfterLast(multipacks_sku, "_");
                           String lastPortion=StringUtils.substringAfterLast(multipacks_sku, "_");
                           if(lastPortion!=null &&StringUtils.isNumeric(lastPortion)  && Float.valueOf(lastPortion).equals(Float.valueOf(oc_singlepack))){
                                tmp=StringUtils.substringBeforeLast(multipacks_sku, "_");
                                multipacks_sku=tmp+"_"+Integer.valueOf(lastPortion)*Integer.valueOf(packagingQuant);
                           }
                       }
                       
                   }
               }else{
                    Integer oc_multipack= Integer.valueOf(oc_singlepack)*Integer.valueOf(packagingQuant);
                    multipacks_sku="VAR_"+asin+"_AMA_"+packagingQuant+"PACKS_"+oc_multipack;
                }                
              if(FILTER_ITEM.length()>0 && strLine0.indexOf(FILTER_ITEM)<0)
                  continue;
                if(title.toLowerCase().indexOf("pack")  >0){
                    System.out.println("WARNING title already contains pack "+title);
                   // continue;
                }
               HashMap hmNew=null;
 
              
            fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+globalFeedFile,true);
            out0 = new BufferedWriter(fstream_out0);
            BufferedReader br00=null;
            if(DONOT_JOIN==false)
                //br00=new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\feeds\\Flat.File.HomeAndGarden-template.txt")));
                br00=new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\feeds\\templates\\"+globalTemplateFile)));
            else
                br00=new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\feeds\\templates\\"+globalTemplateFile)));
           for(int i=0;i<10;i++){
               strLine0 = br00.readLine();
               if(strLine0==null)
                   break;
               if(i<=2 && headerWriten==false){
                out0.write(strLine0);
                out0.newLine();                   
               } 
               
               if(DONOT_JOIN==false){
                   if(i==3)
                    PARENT_TEMPLATE=strLine0;
                   if(i==4)
                    ONEPACK_TEMPLATE=strLine0;
                   if(i==5)
                    MULTIPACK_TEMPLATE=strLine0;
                   
               }else{
                   if(i==3)
                    MULTIPACK_TEMPLATE=strLine0;                   
               }
                
               
           }
           headerWriten=true;
            br00.close();
            out0.close();
            if(processedUPCs.contains(upc) && asin.indexOf("B")<0){
                System.out.println("We are running to add only the asins of upcs..skipping");
                continue;
            }
             processedUPCs.add(upc);   
            if(matchingProductsMap.containsKey(asin)){
                hmNew=matchingProductsMap.get(asin);
            }
            ArrayList<HashMap> matchingProducts = null;
            if(hmNew==null){
               
                matchingProducts=ListMatchingProductsSample.getMatchingProducts(upc);
                
                for(int i=0;i<matchingProducts.size();i++){
                    HashMap tmpMap=matchingProducts.get(i);
                    tmpMap=createNewInventoryFeed(upc,tmpMap,asin);
                    if(tmpMap.get("asin").equals(asin)){
                        System.out.println("Matching asin found throgh MWS API-1");
                        hmNew=tmpMap;
                        hmNew.put("upccount", matchingProducts.size());
                        matchingProductsMap.put(asin, hmNew);
                        

    
                    }
                }
                if(unprocessedAsins.contains(asin)==false)
                    unprocessedAsins.add(asin);
                if(hmNew==null){
                    System.out.println("No matching asin found matching the asin in createvariation file. Continuing");
                    continue;
                }
 
            }
            
            String productCategoryId=((String)hmNew.get("productCategoryId"));
            String categoryPrefix=StringUtils.substringBefore(((String)hmNew.get("category")), "/");
            boolean productMatchedTemplate=false;
            if(productCategoryId.equals(globalProductCategoryId))
                productMatchedTemplate=true;
            if(productCategoryId.equals(globalProductCategoryId)==false && productCategoryId.indexOf("website")<0){
                  if(categoryPrefix!=null && categoryPrefix.equals(globalCategoryStringPrefix))
                  {
                     productMatchedTemplate=true; 
                  }
            }
            if(productMatchedTemplate==false){
                System.out.println("Neither globalProductCategoryId Nor categoryPrefix matched. Continue");
                continue;
            }
          
        HashMap hmF=RetrievePage.getDetailsFromFbaCalculatorNewMWS(null,asin,null,true);
        
         hmNew=createVaration(hmNew,hmF,upc,asin,Integer.valueOf(packagingQuant),"DEFAULT",title,"");
       //  if(Integer.valueOf(packagingQuant)!=1)
          //   hm=createVaration(hm,hmF,upc,asin,Integer.valueOf(packagingQuant),"DEFAULT",title);


            if(hmNew!=null)
               variationstobesent=true;

          }
          
          unprocessedAsins.remove(asin);
      }//End of processing all lines from createvariation file
      br0.close();
      if(false && variationstobesent){
        System.out.println("Calling email service to email new-inventroy.csv");
          String[] to={"mprabagar80@gmail.com" };
        String[] cc={};
        String[] bcc={};

        Mail.sendMail( "smtpout.secureserver.net","465","true",
        "true",true,"javax.net.ssl.SSLSocketFactory","false",to,cc,bcc,
        "NEW VARIATIONS - PRODUCTS TO BE CREATED", "Please veriy and upload the attached variations file Flat.File.FoodAndBeverages.txt",false,true,false,false,null);
          
      }

    }catch (Exception e){
        e.printStackTrace();
    }
    }

    static ArrayList<String> newInventoryCreatedAsins=new ArrayList();
    private static HashMap createNewInventoryFeed(String upc,HashMap tmpMap,String onepack_asin  ) {
        //START TO CREATE NEW INVENTORY FILE
        try{
            String title=(String)tmpMap.get("title");
            String curAsin=(String)tmpMap.get("asin");
            String curSku="VAR_"+curAsin+"_PRAB";
            int curPkgQty=999;
            /*if(tmpMap.containsKey("PackageQuantity")){
                String existPkgQty=  (String) tmpMap.get("PackageQuantity");
              curPkgQty=Integer.valueOf(existPkgQty);
            }*/
            if(title.indexOf("Pack")>0 && title.indexOf("(")>0){
             int  titlteContainsPackOf=ProcessWorkingCatalog.calculatePackingQntFromTitle(title);
             curPkgQty=titlteContainsPackOf;
            }
            PreparedStatement ps=con.prepareStatement("select pkg_qty from tajplaza.variation_sku_upcs where sku=? and pkg_qty<999");
            ps.setString(1, curSku);
            ResultSet rs=ps.executeQuery();
            if(rs.next() && rs.getInt(1)<999){
                curPkgQty=rs.getInt(1);
            }   
            if(curAsin.equals(onepack_asin)){
                curPkgQty=1;
            }
            if(curSku.indexOf("B0012N6W5G_")>=0)
                System.out.println("Debug");
            
            writeToNewInventoryFileExistingAsins(curSku,curAsin,curPkgQty, upc, Float.valueOf(oc_singlepack), title,"",onepack_asin) ;                       

            //All sibling child asins
            ArrayList arr=GetMatchingProductSample.getAllOtherVariationsBySize(curAsin); 
            if(tmpMap!=null && (tmpMap.containsKey("childcount")==false || ((Integer)tmpMap.get("childcount"))<arr.size())) 
                tmpMap.put("childcount", arr.size());
            for(int c=0;c<arr.size();c++){
                HashMap childMapJustAsin=(HashMap) arr.get(c);
                String childAsin=(String)childMapJustAsin.get("childAsin");
                String childSize=(String)childMapJustAsin.get("childSize");
                String childSku="VAR_"+childAsin+"_PRAB";
                curPkgQty=999;
                 ps=con.prepareStatement("select pkg_qty from tajplaza.variation_sku_upcs where sku=?");
                ps.setString(1, childSku);
                 rs=ps.executeQuery();
                if(rs.next() && rs.getInt(1)<999){
                    curPkgQty=rs.getInt(1);
                }  
            if(childSku.indexOf("B0012N6W5G_")>=0)
                System.out.println("Debug");                
                writeToNewInventoryFileExistingAsins(childSku,childAsin,curPkgQty, upc, Float.valueOf(oc_singlepack), "Child of : "+title,childSize,onepack_asin) ;                       

                /* ArrayList<HashMap> childMatchingProducts=ListMatchingProductsSample.getMatchingProducts(childAsin);
                if(childMatchingProducts.size()>0){
                HashMap childMap=childMatchingProducts.get(0);


                String childTitle=(String)childMap.get("title");
                if(childTitle==null)
                childTitle=(String)childMap.get("Title");
                String childSku="VAR_"+childAsin+"_PRAB";
                int childPkgQty=1;
                if(childMap.containsKey("PackageQuantity")){
                 String existPkgQty=  (String) childMap.get("PackageQuantity");
                 childPkgQty=Integer.valueOf(existPkgQty);
                }
                if(childTitle.indexOf("Pack")>0 && childTitle.indexOf("(")>0){
                int  titlteContainsPackOf=ProcessWorkingCatalog.calculatePackingQntFromTitle(childTitle);
                childPkgQty=titlteContainsPackOf;
                }

                writeToNewInventoryFileExistingAsins(childSku,childAsin,childPkgQty, upc, Float.valueOf(oc_singlepack), childTitle,childSize) ;                       

                }*/

            }                                
        }catch (Exception childE){childE.printStackTrace();}

        //END TO  CREATE NEW INVENTORY FILE
        return tmpMap;
    }
    static boolean entryMadedeleteoldinventory=false;
    private static void deleteoldSku(String sku) throws Exception {
        PreparedStatement pst=con.prepareStatement("select e.seller_sku sku  from tajplaza.existing_all_inventory e, variation_sku_upcs v where v.sku=e.seller_sku   and e.seller_sku=?");
        pst.setString(1, sku);
        ResultSet rs=pst.executeQuery();
        if(rs.next()){
            FileWriter  fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\delete-old-inventory.txt",true);
            out0 = new BufferedWriter(fstream_out0);
            out0.write(sku+"\tx");
            out0.newLine();
            out0.close();  
            entryMadedeleteoldinventory=true;
            
        }
    }
     boolean JUST_MULTIPACK=true;


    private static HashMap createVaration(HashMap hmNew, HashMap hmF,String upc, String asin, int packaging_quant, String fulfill,String title,String childSize)  throws Exception{
        String strLine0=null,sku,brand,manu,bb1,bb2,bb3,bb4,bb5,desc,package_quan,price,item_type,image_url;
        String curL;

       
          asin=asin.trim();
          String onepacksku=null;
 
        if(onepacksku==null){
                Date date = Calendar.getInstance().getTime();
                DateFormat formatter = new SimpleDateFormat("MMddhhmmssSSS");
                String today = formatter.format(date);
                System.out.println("Today : " + today);
                onepacksku="1PACK_"+today;            
         //   System.out.println("I can not proceed without 1 pack SKU");
           // return hm;
        }

        System.out.println("GOING TO CREATE VARIATION:"+upc+","+asin+","+title+","+packaging_quant+","+fulfill);
            if(hmNew==null){
                ArrayList<HashMap> matchingProducts = null;
                matchingProducts=ListMatchingProductsSample.getMatchingProducts(upc);
                for(int i=0;i<matchingProducts.size();i++){
                    HashMap tmpMap=matchingProducts.get(i);
                    if(tmpMap.get("asin").equals(asin)){
                        System.out.println("Matching asin found throgh MWS API-3");
                        hmNew=tmpMap;
                        hmNew.put("upccount",getGoodRankedProductsSize(matchingProducts));
                        matchingProductsMap.put(asin, hmNew);
                        //Reason for parsing through here is to see if there is an exisitng multi pack quantity exists.

                    }else{
                        System.out.println("Some other asin is found");
                    }
                }
               
                 

            }else{
            //Wait a 100 ms, so i get a new SKU
                Thread.sleep(100);
            }
            if(hmNew==null){
                System.out.println("hmNew is null");
                return null;
            }
            
            /*if(hmNew.containsKey("productCategoryId") ==false || (
                    ((String)hmNew.get("productCategoryId")).indexOf("grocery")<0
                    && ((String)hmNew.get("productCategoryId")).indexOf("home_garden")<0
                     && ((String)hmNew.get("productCategoryId")).indexOf("pet")<0
                     && ((String)hmNew.get("productCategoryId")).indexOf("beauty")<0
                    )
                    )
            {
               // System.err.println("This create variation supports only grocery at this time ..NOT "+hmNew.get("productCategoryId"));
               // System.exit(1);;
            }*/
            /*if(((String)hmNew.get("productCategoryId")).indexOf("grocery")<0){
                System.err.println("This create variation supports only grocery at this time ..NOT "+hmNew.get("productCategoryId"));
                System.exit(1);;
                
            }*/
            String productCategoryId=((String)hmNew.get("productCategoryId"));
            String categoryPrefix=StringUtils.substringBefore(((String)hmNew.get("category")), "/");
            boolean productMatchedTemplate=false;
            if(productCategoryId.equals(globalProductCategoryId))
                productMatchedTemplate=true;
            if(productCategoryId.equals(globalProductCategoryId)==false && productCategoryId.indexOf("website")<0){
                  if(categoryPrefix.equals(globalCategoryStringPrefix) )
                  {
                     productMatchedTemplate=true; 
                  }
            }
            if(productMatchedTemplate==false){
                System.out.println("Neither globalProductCategoryId Nor categoryPrefix matched. Continue. IT SHLULD NOT HAVE COME ");
                System.out.println("globalProductCategoryId : " +globalProductCategoryId +",globalCategoryStringPrefix: "+globalCategoryStringPrefix);
                System.exit(1);
            }           
            
              //Create new SKU
                Date date = Calendar.getInstance().getTime();
                DateFormat formatter = new SimpleDateFormat("MMddhhmmssSSS");
                String today = formatter.format(date);
                System.out.println("Today : " + today);
                Thread.sleep(1000);
                sku="VAR_"+today;
                String parentSku=null;

                if(hmNew.get("parentSku")==null)
                    parentSku="PARENT"+today;
                else
                   parentSku= (String) hmNew.get("parentSku");
                hmNew.put("parentSku", parentSku);


               /* if(packaging_quant>1){
                    //In case of amazon fulfillment, i hope already the other rows are written..so just add the fba row.
                  sku+="_VAR";
                  writeToNewVariationFile("multipack",sku, hm,hmF, upc,parentSku,  asin,  packaging_quant,  fulfill, title);
                  return hm;
                }     */           
                if(packaging_quant==1){
                    writeToNewVariationFile("just1pack",onepacksku, hmNew,hmF, upc,"",  asin,  1,  fulfill, title);
                    return hmNew;
                }
                 boolean ret=writeToNewVariationFile("parent",parentSku,hmNew,hmF, upc,null,  asin,  1,  fulfill, title);
                 if(ret==false)
                     return hmNew;
                 writeToNewVariationFile("1pack",onepacksku, hmNew,hmF, upc,parentSku,  asin,  1,  fulfill, title);
                 sku+="_PRAB";
                 
                 if(multipacks_sku.length()>0){
                     sku=multipacks_sku ;
                 }
                 

                     
                 
                 writeToNewVariationFile("multipack",sku, hmNew,hmF, upc, parentSku, asin,  packaging_quant,  fulfill, title);
                 
                return hmNew;
    }
    public static int getGoodRankedProductsSize(ArrayList<HashMap> matchingProducts){
        int ret=0;
        for(int i=0;i<matchingProducts.size();i++){
            HashMap hm=matchingProducts.get(i);
            if(hm.containsKey("rank") && (Integer)hm.get("rank")<100000){
                ret++;
            }
        }
        return ret;
    }
    public static String NEW_INVENTORY_STR="sku	title	product-id	product_id_type	price	item-condition	quantity	add-delete	fulfillment_center_id	minimum-seller-allowed-price	maximum-seller-allowed-price	UPC	packingQty	ChildSize";
    public static void writeToNewInventoryFileExistingAsins(String sku,String asin,int pkg_qty,String upc,Float oc_singlepack,String title,String childSize,String onepack_asin ) throws Exception
            
    {
        if(newInventoryCreatedAsins.contains(asin)){
            System.out.println("Already newInventoryFeed created for asin"+asin);
            return;
        }
        newInventoryCreatedAsins.add(asin);;
         PreparedStatement ps=con.prepareStatement("select 1 from tajplaza.existing_all_inventory where asin1=? and seller_sku=?")   ;
         ps.setString(1, asin);
         ps.setString(2, sku);
         ResultSet rs=ps.executeQuery();
         if(rs.next()){
             System.out.println("We already sell sku:"+sku + " skipping, not writing to new-inventory file");
             return;
         }
         
         
        String str=NEW_INVENTORY_STR;
        str=str.replaceAll("sku", sku);
        str=str.replaceAll("product-id", asin);
        str=str.replaceAll("product_id_type", "1");
        str=str.replaceAll("minimum-seller-allowed-price", ".1");   
        str=str.replaceAll("maximum-seller-allowed-price", "199");   
        str=str.replaceAll("price", "99");
        str=str.replaceAll("item-condition", "11");
        str=str.replaceAll("quantity", "0");
        str=str.replaceAll("add-delete", "a");        
        str=str.replaceAll("fulfillment_center_id", "DEFAULT");                
        str=str.replaceAll("UPC", "UPC:"+upc);                
        str=str.replaceAll("packingQty", String.valueOf(pkg_qty));
        str=str.replaceAll("ChildSize", childSize);
        title=title.replaceAll("\"", "");
        str=str.replaceAll("title", title);
        
        
       FileWriter  fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\new-inventory.txt",true);
        out0 = new BufferedWriter(fstream_out0);
        out0.write(str);
        out0.newLine();
        out0.close();      

        
        insertintovariation_sku_upcs(sku,upc,title,pkg_qty,Float.valueOf(oc_singlepack)*pkg_qty,onepack_asin,99,99,Float.valueOf(oc_singlepack));

    }
    public static Connection con;
    public static void insertintovariation_sku_upcs(String sku, String upc, String title, int pkg_qty,float oc_multipack,String onepack_asin,float suggested_price,float onepack_bestprice,float oc_singlepack) throws Exception{
        
        PreparedStatement ps=con.prepareStatement("select 1  from tajplaza.variation_sku_upcs where sku=?");
        ps.setString(1, sku);
        ResultSet rs=ps.executeQuery();
        if(rs.next()){
            System.err.println("Row already exists in variation_sku_upcs, not updating it since it may have new single pack best price, pkg_qty etc everything");
            return;
        }
        
       /* ps=con.prepareStatement("delete from tajplaza.variation_sku_upcs where sku=?");
        ps.setString(1, sku);
        ps.executeUpdate();
        */
          ps=con.prepareStatement("insert into tajplaza.variation_sku_upcs (sku,upc,pkg_qty,title,oc_multipack,onepack_asin,suggested_price,1pack_bestprice,oc_singlepack,upc_pkg_qty) values (?,?,?,?,?,?,?,?,?,?)");
        ps.setString(1, sku);
        ps.setString(2, upc);
        ps.setInt(3, pkg_qty);
        if(title.length()>100)
            title=title.substring(0,99);
        ps.setString(4, title);
        ps.setFloat(5, oc_multipack);
        ps.setString(6, onepack_asin);
        ps.setFloat(7, suggested_price);
        ps.setFloat(8, onepack_bestprice);
        ps.setFloat(9, oc_singlepack);
        ps.setInt(10, pkg_qty*singlePkgQty);
        ps.executeUpdate();
        
    }
 private static boolean writeToNewVariationFile(String type,String sku, HashMap hmNew,HashMap hmF, String upc,String parentSku, String asin, int packaging_quant, String fulfill,String title) throws IOException, Exception {
                String strLine0=null ,brand,manu,bb1,bb2,bb3,bb4,bb5,desc,package_quan,price,item_type,image_url;
                String newproduct=null;
                if(type.equals("parent")){
                    newproduct=PARENT_TEMPLATE;
                }else if(type.equals("1pack")){
                    newproduct=ONEPACK_TEMPLATE;
                }else if(type.equals("multipack")){
                    newproduct=MULTIPACK_TEMPLATE;
                }
                else if(type.equals("just1pack")){
                    newproduct=JUSTONEPACK_TEMPLATE;
                }

                if(newproduct.length()==0)
                    return false;
                newproduct=newproduct.replaceAll("<SKU>", sku);
                newproduct=newproduct.replaceAll("<UPC>", upc);

                title=(String)(hmNew.get("Title")!=null?hmNew.get("Title"):"");
                if(title.indexOf("Amazon.com")>=0)
                    title=title.replaceAll("Amazon.com : ","");
               // int titlteContainsPackOf=0;
                if(title.indexOf("Pack")>0 && title.indexOf("(")>0){
                    String pack=StringUtils.substringAfter(title, "(");
                     pack=StringUtils.substringBefore(pack, ")");
                     // titlteContainsPackOf=ProcessWorkingCatalog.calculatePackingQntFromTitle(title);
                     
                     
                   System.err.println("Single pack contains pack of..Update the single and multi pack title accordingly:"+pack);
                   // title=StringUtils.substringBefore(title, "(");
                    //title=StringUtils.substringBeforeLast(title, "(");
                    //System.out.println("Its not a single pack")
                }
                 String multiSize="";
                if(packaging_quant>1){
                            if(title.toLowerCase().indexOf("pack")>=0 || singlePkgQty>1){
                                multiSize=packaging_quant  +" X ";
                                title=multiSize+title;
                            }
                                
                            else{
                                title=title +" (Pack of "+packaging_quant+ ")";
                            }
                                //multiSize=packaging_quant  +" X ";
                                
                            
                         //title= packaging_quant+"X"+ singlePkgQty +" Packs :"+title;
                        
                }
                    //title= title + ":(Pack Of "+packaging_quant+")";
                String productCategoryId=(String)(hmNew.get("productCategoryId")!=null?hmNew.get("productCategoryId"):"");
                newproduct=newproduct.replaceAll("<productCategoryId>", productCategoryId);
                newproduct=newproduct.replaceAll("<TITLE>", title);

                brand=(String)(hmNew.get("Brand")!=null?hmNew.get("Brand"):"");
                if(brand.length()==0){
                     brand=(String)(hmNew.get("Manufacturer")!=null?hmNew.get("Manufacturer"):"");
                }
                String part_number=upc;
                if(hmNew.containsKey("PartNumber")){
                    part_number=(String)hmNew.get("PartNumber");
                }
                 newproduct=newproduct.replaceAll("<PARTNUMBER>", part_number);
              //  if(brand.indexOf("UCCI")>=0)
                //    brand="Kirkland Signature";
                newproduct=newproduct.replaceAll("<BRAND>", brand);

                newproduct=newproduct.replaceAll("<MANUFA>", brand);


                bb1=(String)(hmNew.get("bb1")!=null?hmNew.get("bb1"):"");
                /*if(packaging_quant>1){
                    bb1="'"+packaging_quant+"'' packs";
                }*/
                bb1=bb1.replaceAll("\\$","");
                if(bb1.indexOf("pack")>=0  )
                    bb1="";
                
                newproduct=newproduct.replaceAll("<BB1>", bb1);

                bb2=(String)(hmNew.get("bb2")!=null?hmNew.get("bb2"):"");
                bb2=bb2.replaceAll("\\$","");
                if(bb2.indexOf("pack")>=0  )                
                    bb2="";
                newproduct=newproduct.replaceAll("<BB2>", bb2);

                bb3=(String)(hmNew.get("bb3")!=null?hmNew.get("bb3"):"");
                bb3=bb3.replaceAll("\\$","");
                if(bb3.indexOf("pack")>=0  )
                    bb3="";
                newproduct=newproduct.replaceAll("<BB3>", bb3);

                bb4=(String)(hmNew.get("bb4")!=null?hmNew.get("bb4"):"");
                bb4=bb4.replaceAll("\\$","");
                if(bb4.indexOf("pack")>=0  )                
                    bb4="";
                newproduct=newproduct.replaceAll("<BB4>", bb4);

                bb5=(String)(hmNew.get("bb5")!=null?hmNew.get("bb5"):"");
                bb5=bb5.replaceAll("\\$","");
                if(bb5.indexOf("pack")>=0  )                
                    bb5="";
                newproduct=newproduct.replaceAll("<BB5>", bb5);

                desc=(String)(hmNew.get("desc_txt")!=null?hmNew.get("desc_txt"):"");
                desc = desc.replaceAll("[^\\x00-\\x7F]", "");
                desc=desc.replaceAll("\n","").replaceAll("\r","");
                if(desc.length()==0){
                    desc=title;
                }
                    
                newproduct=newproduct.replaceAll("<DESCRIPTION>", desc);

                    
                int tmpInt=1;
                if(packaging_quant==1 && hmNew.containsKey("PackageQuantity")){
                    //For 1 pack, use existing packing quantity
                    newproduct=newproduct.replaceAll("<PACKAGING-QUAN>",(String)hmNew.get("PackageQuantity"));
                }else{
                    /*if( hmNew.containsKey("Size")){
                          String tmp=(String)hmNew.get("Size");
                         tmp=tmp.replaceAll("\\D+","");
                         if(tmp==null || Integer.valueOf(tmp)==0)
                             tmpInt=1;
                         else
                            tmpInt= Integer.valueOf(tmp);
                    }*/
                    /*System.out.println("I am NOT (DO I ????) Querying amazon for UPC:"+upc+" to make sure  packing quantity :" +packaging_quant+ " does not exists");
                     
                     ArrayList<HashMap> products=ListMatchingProductsSample.getMatchingProducts(upc);
                     for(int i=0;i<products.size();i++){
                         HashMap hm=products.get(i);
                         
                         if(hm.containsKey("PackageQuantity")){
                             String existPkgQty=  (String) hm.get("PackageQuantity");
                             try{
                                 if(Integer.valueOf(existPkgQty)==packaging_quant){
                                     tmpInt=(int) Math.floor(Math.random() * 10);
                                 }
                                 }catch (Exception e2){e2.printStackTrace();}
                             }
                            if(hm.containsKey("asin")){
                                //PRABU : TO DO , IS THERE ANY CHILD VARIATION OF EACH ASIN THAT MATCHES THE PACKING QUANTITY I WANT
                                //TOO MUCH OVERCOMPLICATING??
                               // ArrayList arr=GetMatchingProductSample.getAllOtherVariationsBySize((String)hm.get("asin"));
                            }
                            
                     }
                     */
                    PreparedStatement ps=con.prepareStatement("select 1 from tajplaza.variation_sku_upcs  where upc=? and pkg_qty=? and ts< (curdate() - interval 1  day)");
                    //PreparedStatement ps=con.prepareStatement("select 1 from tajplaza.variation_sku_upcs  where upc=? and pkg_qty=? ");
                    ps.setString(1, upc);
                    ps.setInt(2, packaging_quant);
                    ResultSet rs=ps.executeQuery();
                    if(rs.next()){
                         tmpInt=(int) Math.floor(Math.random() * 10);
                         if(tmpInt==0)
                              tmpInt=(int) Math.floor(Math.random() * 10);
                    }  
                   /* ps=con.prepareStatement("select pkg_qty from tajplaza.variation_sku_upcs v,tajplaza.existing_all_inventory e  where  v.sku=e.seller_sku and upc=? and pkg_qty=? and ts< (curdate() - interval 1  day)");
                    //PreparedStatement ps=con.prepareStatement("select 1 from tajplaza.variation_sku_upcs  where upc=? and pkg_qty=? ");
                    ps.setString(1, upc);
                    ps.setInt(2, packaging_quant);
                     rs=ps.executeQuery();
                    if(rs.next()){
                         tmpInt=rs.getInt(1);
                    }   */                            
                    //PRABU:::::for now tmpInt is always 1
                   // if(sku.indexOf("AMA")<0)
                      tmpInt=1;
                      packaging_quant=packaging_quant;
                   //newproduct=newproduct.replaceAll("<PACKAGING-QUAN>", Integer.toString(packaging_quant*(int) Math.floor(Math.random() * 101)));
                    newproduct=newproduct.replaceAll("<PACKAGING-QUAN>", Integer.toString(singlePkgQty*packaging_quant*Integer.valueOf(tmpInt)));
                  //   newproduct=newproduct.replaceAll("<PACKAGING-QUAN>", Integer.toString(packaging_quant));
                }
                
                if(hmNew.containsKey("Size")){
                    if(packaging_quant==1 ){
                       newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", (String)hmNew.get("Size") );
                    }else if ( ((String)hmNew.get("Size")).toLowerCase().equals("1 pack") || ((String)hmNew.get("Size")).toLowerCase().equals("pack of 1") || ((String)hmNew.get("Size")).toLowerCase().indexOf("pack")>=0 
                             || singlePkgQty>1 ||((String)hmNew.get("Size")).length()<2) {
                       newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", packaging_quant*singlePkgQty+" packs" ); 
                    }else{
                        newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", packaging_quant+" X "+(String)hmNew.get("Size") ); 
                        // newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", packaging_quant*singlePkgQty+" packs" ); 
                    }
                    
                    //For 1 pack, use existing packing quantity
                    
                }else{
                    if(packaging_quant==1 ){
                         newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", singlePkgQty+" pack" );
                       
                    }else{
                         newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", packaging_quant*singlePkgQty+ " packs" ); 
                       
                    }
                    
                }
                 if(hmNew.containsKey("Flavor")){
                     newproduct=newproduct.replaceAll("<FLAVOR>", (String)hmNew.get("Flavor") );
                     newproduct=newproduct.replaceAll("SizeName", "Flavor-Size" );
                 }else{
                      newproduct=newproduct.replaceAll("<FLAVOR>", "" );
                 }
                 if(hmNew.containsKey("Scent")){
                     newproduct=newproduct.replaceAll("<SCENT_NAME>", (String)hmNew.get("Scent") );
                     newproduct=newproduct.replaceAll("SizeName", "Size-Scent" );
                 }else{
                      newproduct=newproduct.replaceAll("<SCENT_NAME>", "" );
                 } 
                 if(hmNew.containsKey("Color")){
                     newproduct=newproduct.replaceAll("<COLOR_NAME>", (String)hmNew.get("Color") );
                     newproduct=newproduct.replaceAll("SizeName", "Size-Color" );
                 }else{
                      newproduct=newproduct.replaceAll("<COLOR_NAME>", "" );
                 }                 
                 if(hmNew.containsKey("ManufacturerMinimumAge")){
                     newproduct=newproduct.replaceAll("<ManufacturerMinimumAge>", (String)hmNew.get("ManufacturerMinimumAge") );
                     
                 }else{
                      newproduct=newproduct.replaceAll("<ManufacturerMinimumAge>", "" );
                 }                 
                 
                newproduct=newproduct.replaceAll("<UNIT_COUNT>", String.valueOf( singlePkgQty*packaging_quant)  ); 
                
               /* if(packaging_quant>1)
                    newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", Integer.toString(packaging_quant) );
                else
                    newproduct=newproduct.replaceAll("<SIZE_PACK_QUANT>", Integer.toString(packaging_quant)  );
                 * 
                 */

                //float bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
                 Integer rank=(Integer)(hmNew.get("rank")!=null?hmNew.get("rank"):99999);
                

                 float discount_percent=getDiscount(packaging_quant,rank,99999)   ;
                 
                  
   
                String nodeid="";
                String parentnodeid="";
                if(hmNew.containsKey("query-0") && ((String)hmNew.get("query-0")).indexOf("item_type_keyword:")>=0){
                    String query=(String)hmNew.get("query-0");
                    query=query.replaceAll("item_type_keyword:", "");
                    newproduct=newproduct.replaceAll("<ITEM-TYPE>",query);
                } else if(hmNew.containsKey("query-1") && ((String)hmNew.get("query-1")).indexOf("item_type_keyword:")>=0){
                    String query=(String)hmNew.get("query-1");
                    query=query.replaceAll("item_type_keyword:", "");
                    newproduct=newproduct.replaceAll("<ITEM-TYPE>",query);
                }else if(hmNew.containsKey("query-2") && ((String)hmNew.get("query-2")).indexOf("item_type_keyword:")>=0){
                    String query=(String)hmNew.get("query-2");
                    query=query.replaceAll("item_type_keyword:", "");
                    newproduct=newproduct.replaceAll("<ITEM-TYPE>",query);
                }
                else{
                    newproduct=newproduct.replaceAll("<ITEM-TYPE>", "");
                }
                    

                String imageurl=(String)(hmNew.get("imageurl")!=null?hmNew.get("imageurl"):"");
               // imageurl="http://localhost/HBImages/"+upc+".jpg";
                newproduct=newproduct.replaceAll("<IMAGE-URL>", imageurl);

                newproduct=newproduct.replaceAll("<FULFILL>", fulfill);

                if(parentSku!=null)
                    newproduct=newproduct.replaceAll("<PARENTSKU>", parentSku);
                //Weight and height measurement.
               // String weight_measure=(String)(hm.get("weight_measure")!=null?hm.get("weight_measure"):"LB");
                 String weight_measure="LB";
                 
                String height="", width="",length="",len_mes="IN",weight="";
                
        

                length=String.valueOf(hmF.get("productInfoLength"));
                width=String.valueOf(hmF.get("productInfoWidth"));
                Float heightF=(Float)hmF.get("productInfoHeight");
                if(heightF==null)
                    heightF=(float)1;
                
                heightF=Math.round(heightF*(float)100)/(float)100;
                
                if(heightF>14)
                    heightF=(float)14;
                        
                height=String.valueOf(heightF*packaging_quant);
                if(height.indexOf(".")>=0)
                    height=StringUtils.substringBefore(height, ".");
                Float weightF=(float)1;
                Float singlePackWeightF=(float)1;
                if(hmF.get("productInfoWeight")!=null){
                    weightF=(Float)hmF.get("productInfoWeight")*packaging_quant;
                    singlePackWeightF=(Float)hmF.get("productInfoWeight");
                }
                   
                weightF=Math.round(weightF*(float)100)/(float)100;
                //if(weightF>19)
                  //  weightF=(float)19;
                weight=String.valueOf(weightF);
                 HashMap currentBestPriceMap=null;
                 try{
                     if(bestPriceMapofAllAsins.containsKey(asin)==false){
                         currentBestPriceMap=GetCompetitivePricingForASINSample.getJustBBPriceandRankMWS(asin);
                         bestPriceMapofAllAsins.put(asin, currentBestPriceMap);
                     }else{
                         currentBestPriceMap=bestPriceMapofAllAsins.get(asin);
                     }
                         
                 }catch (Exception e2){e2.printStackTrace();}
                float bestprice=(Float)(currentBestPriceMap.get("bestprice")!=null?currentBestPriceMap.get("bestprice"):(float)0);
                //float bestprice=(float)99;
                Float suggestedprice= bestprice;
                
                float multipackweight=singlePackWeightF*globalRequestedPkgQty;
                Float shippingFee=(float)1;
                if(multipackweight<12)
                    shippingFee=(float)6+multipackweight*(float).5;
                else
                     shippingFee=(float)10+multipackweight*(float).25;
                             
                float lowestPricePossibleForMultiPacks=(Float.valueOf(oc_singlepack)*globalRequestedPkgQty*(float)1.15+shippingFee)/(float).85;
                int origglobalRequestedPkgQty=globalRequestedPkgQty;
                float origlowestPricePossibleForMultiPacks=lowestPricePossibleForMultiPacks;
                float origmultipackweight=multipackweight;
                PreparedStatement psShelf=con.prepareStatement("select 1 from tajplaza.scanner_data_shelf where upc=?");
                psShelf.setString(1, upc);
                ResultSet rsShelf=psShelf.executeQuery();
                boolean itemOnShelf=false;
                if(rsShelf.next()){
                    itemOnShelf=true;
                }
                if(itemOnShelf==false && (bestprice>0 && ((bestprice+1)*globalRequestedPkgQty)<lowestPricePossibleForMultiPacks && lowestPricePossibleForMultiPacks<80)){
               // if(bestprice>0 && (bestprice*6)<(Float.valueOf(oc_singlepack)*6*(float)1.15+(float)6+singlePackWeightF*6*(float).5)/(float).85){    
               
                globalRequestedPkgQty=globalRequestedPkgQty+1;
                multipackweight=singlePackWeightF*globalRequestedPkgQty;
                if(multipackweight<12)
                    shippingFee=(float)6+multipackweight*(float).5;
                else
                     shippingFee=(float)10+multipackweight*(float).25;
                 lowestPricePossibleForMultiPacks=(Float.valueOf(oc_singlepack)*globalRequestedPkgQty*(float)1.15+(float)6+shippingFee)/(float).85;
                 if(bestprice>0 && ((bestprice+1)*globalRequestedPkgQty)<lowestPricePossibleForMultiPacks && lowestPricePossibleForMultiPacks<80){
                    globalRequestedPkgQty=globalRequestedPkgQty+1;
                    multipackweight=singlePackWeightF*globalRequestedPkgQty;
                    if(multipackweight<12)
                        shippingFee=(float)6+multipackweight*(float).5;
                    else
                         shippingFee=(float)10+multipackweight*(float).25;
                     lowestPricePossibleForMultiPacks=(Float.valueOf(oc_singlepack)*globalRequestedPkgQty*(float)1.15+(float)6+shippingFee)/(float).85;
                     if(bestprice>0 && ((bestprice+1)*globalRequestedPkgQty)<lowestPricePossibleForMultiPacks  && lowestPricePossibleForMultiPacks<80){
                        globalRequestedPkgQty=globalRequestedPkgQty+1;
                        multipackweight=singlePackWeightF*globalRequestedPkgQty;
                        if(multipackweight<12)
                            shippingFee=(float)6+multipackweight*(float).5;
                        else
                             shippingFee=(float)10+multipackweight*(float).25;
                         lowestPricePossibleForMultiPacks=(Float.valueOf(oc_singlepack)*globalRequestedPkgQty*(float)1.15+(float)6+shippingFee)/(float).85;
                         if(bestprice>0 && ((bestprice+1)*globalRequestedPkgQty)<lowestPricePossibleForMultiPacks  ){
                             globalRequestedPkgQty=999;
                         }
                         
                     }
                 }
                 String errorStr=    "UPC:"+upc+"\tASIN:"+asin+"\tTitle:"+title+"\tNo point, the bestprice is cheaper than min price skipping, bestprice for "+origglobalRequestedPkgQty+" packs:"+((bestprice+1)*origglobalRequestedPkgQty)+"\t our min price" +origlowestPricePossibleForMultiPacks +"\t weight is :"+origmultipackweight +"\t Suggested packing quantity:" +globalRequestedPkgQty;
                FileWriter fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\error-inventory.txt",true);
                out0 = new BufferedWriter(fstream_out0);
                out0.write(errorStr);
                out0.newLine();
                out0.close();    
                 
                 System.err.println(errorStr);;
                 globalRequestedPkgQty=origglobalRequestedPkgQty;
                 return false;
                }
                if(packaging_quant>1){
                    suggestedprice=(float) bestprice*(float)packaging_quant*(float)discount_percent;
                   if(oc_singlepack!=null && oc_singlepack.length()>0){
                         
                         if(weightF<12)
                            shippingFee=(float)6+weightF*(float).5;
                         else
                             shippingFee=(float)10+weightF*(float).25;
                       //  Float minPrice=(Float.valueOf(oc_singlepack)*packaging_quant*(float)1.15+shippingFee)/(float).85;                         
                       // Float minPrice=(Float.valueOf(oc_singlepack)*packaging_quant*(float)1.15+(float)6+weightF*(float).6)/(float).85;
                      //  Float maxPrice=(Float.valueOf(oc_singlepack)*packaging_quant*(float)1.4+shippingFee)/(float).85;
                        /*(suggestedprice<minPrice){
                            System.err.println("Exception, not possible min price for multi packs is "+minPrice +" and suggested price is "+suggestedprice);
                            suggestedprice=minPrice;
                          //  System.exit(1);;
                        }else if(suggestedprice>maxPrice){
                            System.err.println(" Limiting multi pack price to 25% margin "+maxPrice +" and suggested price is "+suggestedprice);
                            suggestedprice=maxPrice;
                            
                        }*/
                    }
                    
                } 
                suggestedprice=Math.round(suggestedprice*(float)100)/(float)100;
                newproduct=newproduct.replaceAll("<PRICE>",Float.toString(suggestedprice));
                if(type.equals("multipack")){
                    insertintovariation_sku_upcs(sku,upc,title,packaging_quant,Float.valueOf(oc_singlepack)*packaging_quant,asin,suggestedprice,bestprice,Float.valueOf(oc_singlepack));
                }
                
                newproduct=newproduct.replaceAll("<HEIGHT>", height);
                newproduct=newproduct.replaceAll("<WIDTH>", width);
                newproduct=newproduct.replaceAll("<LENGTH>", length);
                newproduct=newproduct.replaceAll("<LEN-MEAS>", len_mes);
       

                newproduct=newproduct.replaceAll("<WEIGHT>", weight);
                newproduct=newproduct.replaceAll("<WEIGHT-MEAS>", weight_measure);
                if(hmNew.containsKey("upccount") && (Integer)(hmNew.get("upccount")) >10){
                    System.err.println("UPC : "+ upc + " already has "+hmNew.get("upccount") +" results, fill the new-inventory file and pkg_qty in variation_sku_upcs. Skipping to create variatuion");
                    
                } else if(skusWritten.contains(sku)){
                    System.out.println("sku : "+ sku  + "already written. skipping");
                  
                }
                    
                else{
                    skusWritten.add(sku);
                    deleteoldSku(sku);
                    FileWriter fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\feeds\\"+globalFeedFile,true);
                    out0 = new BufferedWriter(fstream_out0);

                    out0.write(newproduct);
                    out0.newLine();
                    out0.close();
                    
                }
                return true;
    }

 

    public static float getDiscount(int packaging_quant,Integer onepackrank,Integer currentItemRank) {
         //return (float).9;
        if(packaging_quant<=3)
           return (float).94;
        else
            return (float).85;
        //else
          //  return (float).9;
        //    if(rank_float<2000)
          //     return (float).98;
        /*if(currentItemRank==null || currentItemRank.equals("NA")){
            if(onepackrank!=null &&currentItemRank.equals("NA")==false )
                currentItemRank=String.valueOf(Integer.valueOf(onepackrank)*2);
            else
                currentItemRank="999999";
            
        }*/
            /*if(Integer.valueOf(currentItemRank)<10000){
                 return (float).99;
            }        
            if(Integer.valueOf(currentItemRank)<20000){
                 return (float).98;
            }        
        
            if(Integer.valueOf(currentItemRank)<40000){
                 return (float).95;
            }        
            if(Integer.valueOf(currentItemRank)<80000){
                 return (float).90;
            }
             if(Integer.valueOf(onepackrank)<1000){
                  return (float).95;
            } */
 
            
           // return (float).95;
 
    }
public  static void deleteEmptyFeedFiles(  File folder) throws Exception {
    for ( File fileEntry : folder.listFiles()) {
        if(fileEntry.getName().indexOf("multipack")<0)
            continue;
        BufferedReader br0 = new BufferedReader(new InputStreamReader( new FileInputStream(fileEntry)));
        String s;
        int lineCount=0;
        while ((s = br0.readLine()) != null)   {
                if(s.trim().length()>0)
                    lineCount++;
        }
        br0.close();
        if(lineCount<=3){
            FileUtils.deleteQuietly(fileEntry);
            fileEntry.delete();
            new File(fileEntry.getName()).delete();
        }
        
    }
    if(entryMadedeleteoldinventory==false){
        new File("C:\\Google Drive\\Dropbox\\feeds\\delete-old-inventory.txt").delete();
    }
}

}
