/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bombayplaza.createvariations;

/**
 *
 * @author prabu
 */

import amazon.product.ProcessWorkingCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
//import com.tajplaza.products.GetCompetitivePricingForASINSample;
//import com.tajplaza.products.ListMatchingProductsSample;
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetLowestPricedOffersForASINSample;
import com.tajplaza.products.GetMatchingProductSample;
import com.tajplaza.products.ListMatchingProductsSample;
import com.tajplaza.upload.UploadInventoryLoaderFeedFile;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

public class FindMultipacksNotJoined {
        public static String DEBUG_SQL=" and  v.sku='VAR_B005LTHRLK_AMA_6PACKS_84'";
  // public static String DEBUG_SQL="";
   static HashMap<String,HashMap> matchingProductsMap=new HashMap();
 public static void  main(String args[]) throws Exception  {
            String PRODUCTION=System.getProperty("PRODUCTION","false");
             
            if(PRODUCTION.equals("true")){
                DEBUG_SQL="";
            }

                
     Connection con=RetrievePage.getConnection();
     //String sql="select seller_sku,asin1,v.pkg_qty,oc_multipack,e.price,onepack_asin from tajplaza.existing_all_inventory e , tajplaza.variation_sku_upcs v where  onepack_asin is  not null and v.sku=e.seller_sku  and (quantity>0 or 1pack_rank is null or min_price is null or cast(1pack_bestprice as decimal)=0 or (v.sku in (select sellersku from tajplaza.orders where  purchaseDate > date_sub(curdate(), interval 90 day)  and v.ts< (curdate() - interval 7  day)))) "+DEBUG_SQL+"  order by v.ts  ";
         String sql=" select distinct   upc,v.onepack_asin,v.title  ,seller_sku,e.asin1 ,e.quantity,joined_with_one_pack_ts,joined_with_one_pack,ts  from tajplaza.variation_sku_upcs  v , tajplaza.existing_all_inventory e  where v.sku=e.seller_sku and onepack_asin like 'B%' and pkg_qty>1 and pkg_qty<999  and (seller_sku  like 'VAR%AMA%' or ( seller_sku  not like 'VAR%AMA%'   and joined_with_one_pack_ts<curdate() - interval 30 day))      and joined_with_one_pack_ts<(curdate() - interval 7 day)  and  (e.quantity>0 or upc in (select key1 from hb_bc.scanner_data) or upc in (select product_upc_ean from hb_bc.exported_products_from_hb  ))  "+DEBUG_SQL+"  order by  case when e.quantity>0 then 1 else 99 end, case when seller_sku  like 'VAR%AMA%' then 1 else 99 end,joined_with_one_pack_ts,ts desc limit 100 ";
     System.out.println("Executing sql:"+sql);
     PreparedStatement ps=con.prepareStatement(sql);
     PreparedStatement update_variation_sku_upcs=con.prepareStatement("update tajplaza.variation_sku_upcs  set joined_with_one_pack=? ,joined_with_one_pack_ts=now() where sku=?");
     
       //      + "and seller_sku='CO_125_43607_B002978HHA_3PACKS_24'");
     //PreparedStatement psStuck=con.prepareStatement("select v.sku from tajplaza.variation_sku_upcs v, tajplaza.amazon_min_price m where v.sku=m.sku and (m.custom_note like '%Stuck%' or m.custom_note like '%Expiring%') and v.sku=?");
     
     ResultSet rs=ps.executeQuery();
    
     while(rs.next()){
         String notes="";
         try{
             String upc=rs.getString("upc");
             
             String onepack_asin=rs.getString("onepack_asin");
             String title=rs.getString("title");
              String seller_sku=rs.getString("seller_sku");
             String asin1=rs.getString("asin1");
             System.out.println("Processing SKU:"+seller_sku + "asin:"+asin1);
             GetMatchingProductSample.FLAVOR_VARIATIONS_SKIP=false;
             ArrayList arr=new ArrayList();
             arr=GetMatchingProductSample.getAllOtherVariationsBySize(asin1);
             boolean onepack_joined=false;
             for (int j=0;j<arr.size();j++){
                 HashMap hm2=(HashMap) arr.get(j);
                 String childAsin=(String) hm2.get("childAsin");
                 if(childAsin!=null && childAsin.equals(onepack_asin)){
                  onepack_joined=true;   
                 }

             }
             if(onepack_joined==false){
                 ArrayList<HashMap> products=ListMatchingProductsSample.getMatchingProducts(asin1);
                 int rank=999999;
                 for(int i=0;i<products.size();i++){
                     HashMap hm=products.get(i);
                     if(hm.containsKey("rank")){
                         rank=(Integer)hm.get("rank");
                     }
                 }
                 if(rank>=90000){
                     System.out.println("asin :"+asin1 +" is not joined with onepack_asin:"+onepack_asin);
                      onepack_joined=false;
                     
                 }else{
                     onepack_joined=true;
                 }
             }
             update_variation_sku_upcs.setBoolean(1, onepack_joined);
             update_variation_sku_upcs.setString(2, seller_sku);
             update_variation_sku_upcs.executeUpdate();;
             
             

             
  
         }catch (Exception e){e.printStackTrace();}

         
    }
 
    //  File file=new File("C:\\Google Drive\\Dropbox\\tmp\\prabu\\multiPACKSPrices.txt");
     // UploadInventoryLoaderFeedFile.uploadFileIntoAmazon(file);
       
     System.out.println("FindMultipacksNotJoined completed");
 }
}
