/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bombayplaza.pricematch;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.gargoylesoftware.htmlunit.xml.XmlPage;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
/**
 *
 * @author US083274 test
 */
public class DownloadReportsNotAvailableInMWS {
     
    public static StringBuffer fbaFeesNotKnown=new StringBuffer();
    public static StringBuffer originalNotKnown=new StringBuffer();
    public static StringBuffer itemsInLoss1=new StringBuffer();
    public static StringBuffer newItemsInLoss1=new StringBuffer();
    public static TreeMap<Float,String> itemsInLossMap=new TreeMap<Float,String> ();
    private static Date date;
    private static String today;
    private static ArrayList<String> myinventory_Contents;
    private static ArrayList<String> existinginventoryFBA_Contents;
    public static HashMap<String,String> downloadMap=new HashMap ();
    private static final String MODULE = "[ DownloadReportsNotAvailableInMWS ] ";   
    public static void main(String args[]){
        System.out.println(MODULE+"Inside main at : " + new Date());
        Date date = Calendar.getInstance().getTime();
        DateFormat formatter = new SimpleDateFormat("E");
        String today = formatter.format(date);
        InputStream is;
        OutputStream out;
        int read;
            byte[] bytes;        
 
       //if( today.indexOf("Sat")>=0)
       {
      //  if( today.indexOf("Sun")>=0){
            downloadMap.put("REIMBURSEMENTS", "C:\\Google Drive\\Dropbox\\fba\\report-reimbursement-data.txt");
            downloadMap.put("REMOVAL_ORDER_DETAIL", "C:\\Google Drive\\Dropbox\\fba\\report-removal-order-detail.txt");
            
        }            
     
        DownloadReportsNotAvailableInMWS d=new DownloadReportsNotAvailableInMWS();
      
        List<DomElement>  elems;
        try{
            String process_type="REQUEST_REPORT";
            if(args.length>0){
                process_type=args[0];
            }

                WebClient webClient1 ;
               webClient1=new WebClient(BrowserVersion.FIREFOX_38 );
                if(RetrievePage.isOffice()){
                     System.setProperty("java.net.preferIPv4Stack", "true");
                    webClient1 = new WebClient(BrowserVersion.getDefault(),"proxydirect.tycoelectronics.com", 80);
                  //  DefaultCredentialsProvider credentialsProvider = (DefaultCredentialsProvider) webClient1.getCredentialsProvider();
                 // credentialsProvider.addCredentials("us083274",  RetrievePage.PASSWORD1);   
                  
                }    
            
                System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "error");
                LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
                java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF); 
                java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);
                webClient1.getOptions().setThrowExceptionOnScriptError(false);
                webClient1.getOptions().setJavaScriptEnabled(false);
                webClient1.getOptions().setCssEnabled(false);
                 webClient1.waitForBackgroundJavaScript(15000);
                 

                HtmlPage page = webClient1.getPage("https://sellercentral.amazon.com/gp/homepage.html");
                //System.out.println(page.asText());
                if(page.getElementById("username")!=null)
                    ((HtmlTextInput) page.getElementById("username")).setText("packfba@gmail.com");
                else
                    ((HtmlTextInput) page.getElementById("ap_email")).setValueAttribute("packfba@gmail.com");
                if(page.getElementById("password")!=null)
                    ((HtmlPasswordInput) page.getElementById("password")).setText("dummy123");
                else
                    ((HtmlPasswordInput) page.getElementById("ap_password")).setValueAttribute("dummy123");
                 System.out.println("Signing In");
                 if(page.getElementById("sign-in-button")!=null)
                    page=(HtmlPage) ((HtmlButton) page.getElementById("sign-in-button")).click();
                 else
                     page=(HtmlPage) ((HtmlImageInput) page.getElementById("signInSubmit")).click();
                HtmlSelect select;
                HtmlOption option ;
                     //make sure we set amazon.com in top marketplace name
                                        //SElect 15 days of report.
                try{
                    if(page.getElementById("dcq_question_subjective_1")!=null){
                        
                        ((HtmlTextInput) page.getElementById("dcq_question_subjective_1")).setText("17050");
                        page=(HtmlPage) ((HtmlSubmitInput) page.getElementById("dcq_question_submit")).click();
                    
                    }
                }catch (Exception e3){}
                elems=  (List<DomElement>) page.getByXPath("//select[@id='sc-mkt-switcher-select']");
                if(elems.size()>0){
                    select = (HtmlSelect) elems.get(0);
                    option = select.getOptionByText("www.amazon.com");
                    page= select.setSelectedAttribute(option, true);
                }
//                  
//                  
//               
                date = Calendar.getInstance().getTime();
              String month = new SimpleDateFormat("MM").format(date);
              String day = new SimpleDateFormat("dd").format(date);              
              String year = new SimpleDateFormat("yyyy").format(date);
              String lastyear=String.valueOf(Integer.valueOf(year)-1);                     
               Iterator reportIterator=downloadMap.keySet().iterator();
                 if(process_type.equals("REQUEST_REPORT")){
                     
                    while(reportIterator.hasNext()){
                            String report_type=reportIterator.next().toString();
                            String urlToVisit="https://sellercentral.amazon.com/gp/ssof/reports/request-download.html/ref=ag_xx_cont_fbafulrpts?recordType="+report_type+"&eventDateTypeFilterOption=orderDate&eventDateOption=0&fromDate=01%2F01%2F2012&toDate=01%2F01%2F2018&startDate=1%2F1%2F12&endDate=1%2F1%2F18&eventMonthOption=1&fromMonth=5&fromYear=2012&toMonth=5&toYear=2013&startMonth=&startYear=&endMonth=&endYear=&_=1369927820631";
                            System.out.println("Visiting urlToVisit:"+urlToVisit);
                            XmlPage xPage=  webClient1.getPage(urlToVisit);
                            System.out.println("request report "+report_type+" output:"+xPage.asXml());
    
                }                     
                 }//end of request_report
                 else  {
                    //DOWNLOAD THE TWO REPORTS
 
                        if(  true){
                        //Only on sunday download inventory-health report and received-inventory report
                           String href="https://sellercentral.amazon.com/gp/payments-account//export-transactions.html?ie=UTF8&pageSize=DownloadSize&startDate=6/17/11&subview=dateRange&mostRecentLast=0&endDate=6/17/18&view=filter&eventType=Refund";
                           String report_downloaded_file="C:\\Google Drive\\Dropbox\\fba\\report_refund_settlements.txt";
                            
                      
                                is =webClient1.getPage(href).getWebResponse().getContentAsStream();
                                out = new FileOutputStream(new File(report_downloaded_file));
                                read=0;
                                bytes = new byte[1024];

                                while((read = is.read(bytes))!= -1){
                                out.write(bytes, 0, read);
                                }

                                is.close();
                                out.flush();
                                out.close();

                                System.out.println("New file created!");
                                //Finally lets just remove the lines above header
                                
                                
                        }
                                
                    while(reportIterator.hasNext()){
                            String report_type=reportIterator.next().toString();
                            String report_downloaded_file=downloadMap.get(report_type);
                            System.out.println("going to download report "+report_type );
                        XmlPage xPage=  webClient1.getPage("https://sellercentral.amazon.com/gp/ssof/reports/download-history.html/ref=ag_xx_cont_fbafulrpts?reports="+report_type+"&_=1345569823608");
                        String t=xPage.asXml().toString();
                        t=StringUtils.substringAfter(t, "href");
                        t=StringUtils.substringBetween(t, "\"","\"");
                        if(t.length()>0){

                                System.out.println("Clicking on download button");
                                String href=t;
                                 
                                
                                is =webClient1.getPage(href).getWebResponse().getContentAsStream();
                                out = new FileOutputStream(new File(report_downloaded_file));
                                read=0;
                                bytes = new byte[1024];

                                while((read = is.read(bytes))!= -1){
                                out.write(bytes, 0, read);
                                }

                                is.close();
                                out.flush();
                                out.close();

                                System.out.println("New file created!");

                            }
                }                      
         }
        }catch (Exception e){
            e.printStackTrace();
        }   
        System.out.println(MODULE+"Leaving main at : " + new Date());
    }
}
