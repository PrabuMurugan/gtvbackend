/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bombayplaza.pricematch;

/**
 *
 * @author us083274
 */
 

import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.InputStream;

import org.xml.sax.XMLReader;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.XMLReaderFactory;
import org.xml.sax.helpers.DefaultHandler;

public class SaxTest1 extends DefaultHandler {

    public static void main(String args[]) throws Exception {
        XMLReader xr = XMLReaderFactory.createXMLReader();
        SaxTest1 handler = new SaxTest1();
        xr.setContentHandler(handler);
        xr.setErrorHandler(handler);

        String xml_string = "<?xml version=\"1.0\"?><rootnode><a>hello</a><b>world</b></rootnode>";
        InputStream xmlStream = new ByteArrayInputStream(xml_string.getBytes("UTF-8"));
        xr.parse(new InputSource(xmlStream));
    }

    public SaxTest1() {
        super();
    }

    ////////////////////////////////////////////////////////////////////
    // Event handlers.
    ////////////////////////////////////////////////////////////////////

    public void startDocument() {
        System.out.println("Start document");
    }

    public void endDocument() {
        System.out.println("End document");
    }

    public void startElement(String uri, String name, String qName, Attributes atts) {
        if ("".equals(uri))
            System.out.println("Start element: " + qName);
        else
            System.out.println("Start element: {" + uri + "}" + name);
    }

    public void endElement(String uri, String name, String qName) {
        if ("".equals(uri))
            System.out.println("End element: " + qName);
        else
            System.out.println("End element:   {" + uri + "}" + name);
    }

    public void characters(char ch[], int start, int length) {
        System.out.print("Characters:    \"");
        for (int i = start; i < start + length; i++) {
            switch (ch[i]) {
            case '\\':
                System.out.print("\\\\");
                break;
            case '"':
                System.out.print("\\\"");
                break;
            case '\n':
                System.out.print("\\n");
                break;
            case '\r':
                System.out.print("\\r");
                break;
            case '\t':
                System.out.print("\\t");
                break;
            default:
                System.out.print(ch[i]);
                break;
            }
        }
        System.out.print("\"\n");
    }
}