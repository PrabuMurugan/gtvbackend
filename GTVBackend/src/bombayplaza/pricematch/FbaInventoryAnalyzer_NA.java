/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bombayplaza.pricematch;

import bombayplaza.pricematch.Mail;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author US083274
 */
public class FbaInventoryAnalyzer_NA {
    private static Date date;
    public static  String DEBUG_SKU="RP0805015601585_FBA";
    
    private static SimpleDateFormat formatter;
    private static String today;
   static String EMAIL="false" ;
    static WebClient webClient1=null ;
    static int WEEKS_COVER_LIMIT=15;
      static  Integer DISCREPANCY_START_POSITION=12;
      public static  Integer SKU_COLUMN=1;
      public static  Integer ASIN_COLUMN=1;
      
      public static  Integer REQASINS_COLUMN=8;
      public static  Integer NOTES_COLUMN=9;
      public static  Integer RANK_COLUMN=11;
      public static  Integer SOLDBY_COLUMN=10;
      public static  Integer ORDERFROM_COLUMN=14;
      public static  Integer PROFIT_COLUMN=7;
    private static String getComments(String s ){
         StringBuffer comments=new StringBuffer();
         String[] splt=s.split("\t");
         for(int i=0;i<splt.length;i++){
             if(i<DISCREPANCY_START_POSITION)
                 continue;
             comments.append("\t");
             comments.append(splt[i]);
             
         }
         return comments.toString();
    }    
     public static void main(String args[]){
                          String strLine0;
        EMAIL= System.getProperty("EMAIL", "false");
                try{
                   // sortRestockFile();
            //At last we will find the items that are sold out from our inventory.
            //The rule is item sold more than 5 times and current inventory stock is zero
            MatchThePrice m=new MatchThePrice();
            m.readAndFillFileContents();
            ArrayList<String> existinginventoryFBA_Contents=m.existinginventoryFBA_Contents;
            ArrayList<String> myinventory_Contents=m.myinventory_Contents;
            ArrayList<String> inventorytotallist_Contents=m.inventorytotallist_Contents;
            ArrayList<String> runningFBAAnalzer_Contents=m.runningFBAAnalzer_Contents_AVOID;
            ArrayList<String> restock_Contents=m.restock_Contents;
            HashMap<String,String> my_inventory=new HashMap();
            HashMap<String,String> best_price=new HashMap();
            HashMap<String,String> soldby=new HashMap();
            HashMap<String,Integer> packingQty=new HashMap();
            HashMap<String,String> currentProfitMap=new HashMap();
            //HashMap<String,String> existingProfits=new HashMap();
            for(int i=1;i<myinventory_Contents.size();i++){
                String s=myinventory_Contents.get(i);
                String asin=s.split("\t")[2];
                if(s==null||s.split("\t").length<3)
                    continue;
                my_inventory.put(asin, s.split("\t")[1]);
                String soldbywhom=s.split("\t")[4];
                soldby.put(asin,soldbywhom );
                //if(soldbywhom.indexOf("timely goods")<0)
                String currentBestPrice=s.split("\t")[7] ;
                 best_price.put(s.split("\t")[2],currentBestPrice);
                 if(currentBestPrice!=null &&currentBestPrice.equals("null") ==false ){
                         Float profit=MatchThePrice.calculateProfitN_AVOID(asin,Float.valueOf(currentBestPrice))-2;
                        if(profit!=0){
                            System.out.println("If I sell this line item for best price to match "+soldby.get(asin) +" to match, my profit is :"+profit +" but I was thinking my profit was :"+s.split("\t")[8]+" current line item:"+s);
                            currentProfitMap.put(asin,String.valueOf(profit));
                        }                 

                 }

                
            }
           /*for(int i=0;i<restock_Contents.size();i++){
                String s=restock_Contents.get(i);
                String asin=s.split("\t")[1];
                if(s.split("\t").length<=RANK_COLUMN)
                    continue;
                if(s.indexOf("B00944ORXY")>=0)
                    System.out.println("Debug");
                String rank=s.split("\t")[RANK_COLUMN];
                String soldbywhom=s.split("\t")[SOLDBY_COLUMN];
                String profit=s.split("\t")[PROFIT_COLUMN];
                if(m.todayissunday == false){
                    
                    if(my_inventory.containsKey(asin)==false)
                    {
                        System.out.println("Today is not sunday..Get the soldby and rank from restock fiel rather than caclulatign them completely again and again, line :"+s +" usign rank:"+rank);
                        if(rank==null || rank.equals("null") && my_inventory.containsKey(asin)==false)
                            rank="999997";
                        my_inventory.put(asin, rank);
                        soldby.put(asin,soldbywhom );
                        currentProfitMap.put(asin,String.valueOf(profit));
                    }
                }
            }*/
            
            /*
             * for(int i=0;i<runningFBAAnalzer_Contents.size();i++){
                if(i==0)
                    continue;
                String s=runningFBAAnalzer_Contents.get(i);
                if(s==null||s.split("\t").length<3 || s.split("\t").length<=8)
                    continue;
                if(s.split("\t")[0].equals(DEBUG_SKU))
                    System.out.println("Debig");
                
                //now put the profit
                
                runningFbaAnalyzer.put(s.split("\t")[0],s.split("\t")[8]);
                 String asin="";
                try{
                    if(s.indexOf("B002GJ9JY6")>=0)
                        System.out.println("Debug");
                   asin=s.split("\t")[1];
                    String sku=s.split("\t")[0];
                    if(soldby.get(asin)!=null && soldby.get(asin).equals("null")==false && soldby.get(asin).indexOf("timely goods")<0){
    
                        
                         String bestprice=best_price.get(asin);
                         if(bestprice==null || bestprice.equals("null") ){
                             System.out.println("best price is null ?? for asin :"+asin +" It might be that the item got removed from amazon or I AM NOT SELLING IT");
                             continue;
                         }
                             
                         Float profit=MatchThePrice.calculateProfit(asin,Float.valueOf(bestprice));
                        if(profit!=0){
                            System.out.println("If I sell this line item for best price to match "+soldby.get(asin) +" to match, my profit is :"+profit +" but I was thinking my profit was :"+s.split("\t")[8]+" current line item:"+s);
                            runningFbaAnalyzer.put(sku,String.valueOf(profit));
                        }
                         
                         
                    }
                    
                }catch (Exception e){
                    System.out.println("Exception while getting profit : for asin:"+asin);
                    e.printStackTrace();
                }
                    
                 
            }*/
                 
            for(int i=0;i<inventorytotallist_Contents.size();i++){
                String s=inventorytotallist_Contents.get(i);
                if(s==null || s.split("\t").length<=5)
                    continue;
                String sku=s.split("\t")[1];
                Integer pk=Integer.parseInt(s.split("\t")[5].replaceAll(",","").replaceAll("\"",""));
                packingQty.put(sku, pk);
            }
            
            HashMap skuSoldMap=m.skuSoldMap;
            ArrayList existinginventoryAll_Contents=m.existinginventoryAll_Contents;
            String s,sku,quant,itemName,asin1;
            Integer soldsofar;
             StringBuffer stockStr=new StringBuffer();
            stockStr.append("<table border=2>");
            boolean restockMyInventory=false;
            String rank="999999";
            for (int i=0;i<existinginventoryAll_Contents.size();i++){
                s=(String) existinginventoryAll_Contents.get(i);
                itemName=s.split("\t")[0];
                sku=s.split("\t")[3];                
                quant=s.split("\t")[5];
                asin1=s.split("\t")[16];
                if(quant!=null && quant.trim().equals("0")){
                    //No quantity..How many times it is sold.
                    if(skuSoldMap.containsKey(sku)){
                        soldsofar=  (Integer) skuSoldMap.get(sku);
                        if(soldsofar>4){
                            //Sold more than 3 times
                            restockMyInventory=true;
                            System.out.println("Restock sku:"+sku+", Item Name:"+itemName +", Quantity sold:"+soldsofar);
                            stockStr.append("<tr> <td><a href='http://www.amazon.com/gp/product/"+asin1+"'>"+ itemName +"</a></td><td>"+soldsofar+"</td></tr>");        
                        }
                    }
                }
            }
            stockStr.append("</table>");
            if(false && restockMyInventory && EMAIL.contains("true")){
                //String[] to={"mprabagar80@gmail.com","nspraveen91@gmail.com"};
                String[] to={"mprabagar80@gmail.com" };
                String[] cc={};
                String[] bcc={};
                
                Mail.sendMail( "smtpout.secureserver.net","465","true",
                "true",false,"javax.net.ssl.SSLSocketFactory","false",to,cc,bcc,
                "RESTOCK IN OUR INVENTORY","<h3> FOllwing items are out of stock in our inventory and they have been sold more than 4 times in the past. Either increase the quantity or delete them.</h3> <br/>"+stockStr.toString(),false,false,false,false,null);                
            }
            
            
            
                    StringBuffer newDiscrepancy=new StringBuffer();
                    StringBuffer newRestock=new StringBuffer();
                    
                                  date = Calendar.getInstance().getTime();
              formatter = new SimpleDateFormat("MMM-dd-yyyy");
              today = formatter.format(date);
              
              createFbaanalyzerInBegining();
              
              File src=new File("C:\\Google Drive\\Dropbox\\fba\\fbaanalyzer.txt");
              File dest=new File("C:\\Google Drive\\Dropbox\\fba\\backup\\old_fbaanalyzer_"+System.currentTimeMillis()+".txt");
               FileUtils.copyFile(src, dest);
               String headerrow="";
              System.out.println("Sice of dest file:"+dest.length());
                  BufferedReader br0 = new BufferedReader(new
                  InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\fbaanalyzer.txt")));
                   
                  ArrayList<String> oldBuff=new ArrayList();
                while ((s = br0.readLine()) != null)   {
                     if(headerrow.length()==0) {
                         headerrow=s;
                     }
                    oldBuff.add(s);
                }
                br0.close();
                    
                    
                    br0 = new BufferedReader(new
                  InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\received-inventory.txt")));

                  String received_quantity;

                  HashMap inventory=new HashMap();
                  FileWriter fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\fbaanalyzer_new.txt");
                  BufferedWriter out0 = new BufferedWriter(fstream_out0);
                out0.write(headerrow);
                 out0.newLine();
                 boolean deleteduplicateitems=false;
                   HashMap<String, Integer> receivedMap=new HashMap<String, Integer>();
                   HashMap<String, Integer> healthTotalMap=new HashMap<String, Integer>();
                   HashMap<String, Integer> remainingMap=new HashMap<String, Integer>();
                   HashMap<String, Integer> reqPacks=new HashMap<String, Integer>();
                   
                   HashMap<String, Integer> healthCover365Map=new HashMap<String, Integer>();
                   HashMap<String, Integer> restockMap=new HashMap<String, Integer>();
                   HashMap<String, String> itemNameMap=new HashMap<String, String>();
                   HashMap<String, String> asinMap=new HashMap<String, String>();
                   HashMap<String, Integer> inboundMap=new HashMap<String, Integer>();
                   HashMap<String, Integer> reservedMap=new HashMap<String, Integer>();
                   ArrayList itemReceivedYesterday=new ArrayList();
                   //read the header
                   
                    br0.readLine();
                    String asin;
                    String recvd_dateS;
                    DateFormat formatter ; 
                    Date recvd_date ; 
                    Date curr = new Date();
                     formatter = new SimpleDateFormat("yyyy-MM-dd");
                  while ((strLine0 = br0.readLine()) != null)   {
                    sku=strLine0.split("\t")[2];
                      try{
                          recvd_dateS=StringUtils.substringBefore(strLine0.split("\t")[0],"T");
                          recvd_date = (Date)formatter.parse(recvd_dateS);  
                             
                            if((curr.getTime()-recvd_date.getTime())<1000*60*60*72){
                                System.out.println("Item received just yesterday..:"+strLine0);
                                itemReceivedYesterday.add(sku);
                                //continue;
                                
                            }
                      }catch (Exception e){
                          e.printStackTrace();
                      }
                     
                      
                    
                    String s1;
                    asin="NA";
                    Integer inboundQty=0;
                    for(int i=0;i<existinginventoryFBA_Contents.size();i++){
                        s1=existinginventoryFBA_Contents.get(i);
                        if(s1.indexOf(sku+"\t")>=0){
                            asin=s1.split("\t")[2];
                           inboundQty= Integer.parseInt(s1.split("\t")[16])+Integer.parseInt(s1.split("\t")[17]);
                            break;
                        }
                    }
                     asinMap.put(sku, asin);
                    inboundMap.put(sku,inboundQty);
                    received_quantity=strLine0.split("\t")[4];
                    itemName=strLine0.split("\t")[3];
                    try{
                        itemNameMap.put(sku, itemName);
                    if(receivedMap.containsKey(sku))
                        receivedMap.put(sku, receivedMap.get(sku)+Integer.parseInt(received_quantity.trim()));
                    else
                        receivedMap.put(sku, Integer.parseInt(received_quantity.trim()));
                    }catch (Exception e){e.printStackTrace();}
                  }
                  

                   String sellable_quantity, sold_quantity;
                   Integer total_quantity;
                   String weeksCover;
                  //Now open health file
                  int x0=1;
                  try{
                           
                           
                            for ( x0=0;x0<existinginventoryFBA_Contents.size();x0++){
                                
                                String tSku=existinginventoryFBA_Contents.get(x0).split("\t")[0];
                                String tResvd=existinginventoryFBA_Contents.get(x0).split("\t")[12];
                                if(tResvd.indexOf("afn-reserved-quantity")>=0)
                                    continue;
                                Integer tNResvd=Integer.parseInt(tResvd);
   
                                if(tNResvd>0){
                                    reservedMap.put(tSku, tNResvd);
                                }
                            }
                      
                  } catch (Exception e){
                      e.printStackTrace();
                       Mail.emailException(FbaInventoryAnalyzer_NA.class,e);
                  }
                  //Now open health file
                   br0 = new BufferedReader(new
                  InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\inventory-health.txt")));
                   
                   br0.readLine();                   
                  while ((strLine0 = br0.readLine()) != null)   {
                      System.out.println("Current line from received map :"+strLine0);
                    sku=strLine0.split("\t")[1];
                   sellable_quantity =strLine0.split("\t")[8];
                   sold_quantity =strLine0.split("\t")[20];
                   weeksCover=strLine0.split("\t")[23];
                   try{
                       if(weeksCover.indexOf(".")>=0)
                           weeksCover=String.format("%.1f", Float.valueOf(weeksCover) );
                      /* String weeksCover_7=strLine0.split("\t")[22];
                       if(Float.valueOf(weeksCover_7)<Float.valueOf(weeksCover)){
                           weeksCover=weeksCover_7;
                       }*/
                   }catch (Exception e){}
                   if(weeksCover==null || weeksCover.equals("null"))
                       weeksCover="0";
                   total_quantity=Integer.parseInt(sellable_quantity) + Integer.parseInt(sold_quantity);
                   if(itemNameMap.containsKey(sku)==false){
                       itemNameMap.put(sku,strLine0.split("\t")[4]);
                   }
                   healthTotalMap.put(sku,total_quantity);
                   remainingMap.put(sku, Integer.parseInt(sellable_quantity));
                   //calculated req packs
                   Integer soldhistory=0;
                 //  if(Integer.parseInt(strLine0.split("\t")[17])*8>soldhistory)
                   //    soldhistory=Integer.parseInt(strLine0.split("\t")[17])*8;
                   String RESTOCK_TXT="restock"                  ;
                   System.out.println("Items sold in last month:"+strLine0.split("\t")[18]+",Items sold in last week:"+strLine0.split("\t")[17]);
                       soldhistory=Integer.parseInt(strLine0.split("\t")[18])*3;
                   if(soldhistory>10 && Integer.parseInt(strLine0.split("\t")[17])*12<soldhistory*.6){
                      System.out.println("Something is not correct..Last week sales is declined compared to monthly sales");
                       RESTOCK_TXT+=" why less sales in last 7 days compared to last 1 month"                  ;
                      // soldhistory=Integer.parseInt(strLine0.split("\t")[17])*12;
                   }
                   try{
                       //Try to get the items needed for 12 weeks from my_inventory
                   }catch (Exception e){
                       e.printStackTrace();
                   }
                  // if(Integer.parseInt(strLine0.split("\t")[17])>5 && Integer.parseInt(strLine0.split("\t")[17])*8>soldhistory)
                      // soldhistory=Integer.parseInt(strLine0.split("\t")[17])*12;                   
                       
                        
                       Integer reqPacksN=soldhistory;
                       if(sellable_quantity!=null){
                           reqPacksN=soldhistory-Integer.parseInt(sellable_quantity);
                           
                       }
                       if(inboundMap.containsKey(sku) ){
                           reqPacksN=reqPacksN-inboundMap.get(sku);
                           
                       }
                       
                 //  if(Integer.parseInt(strLine0.split("\t")[19])>soldhistory)
                   //    soldhistory=Integer.parseInt(strLine0.split("\t")[19]);
                   //if(packingQty.containsKey(sku) && itemNameMap.get(sku).toLowerCase().indexOf("pack")>=0)
                     //   soldhistory=packingQty.get(sku)*soldhistory;
                   reqPacks.put(sku, reqPacksN);
                   
                   if(sku.equals(DEBUG_SKU))
                   {
                       System.out.println("break");
                   }
                   try{
                       System.out.println("Line :"+strLine0);
                     //  if(strLine0.indexOf(DEBUG_SKU)>=0)
                     //      System.out.println("debug");
                      // if(weeksCover.equals("Infinite") ==false && Float.parseFloat(weeksCover)<WEEKS_COVER_LIMIT &&( receivedMap.get(sku)==null|| receivedMap.get(sku)>4 )
                        if(weeksCover.equals("Infinite") ==false   &&( receivedMap.get(sku)==null|| receivedMap.get(sku)>4 )                       
                               
                               ){
                           
                           System.out.println("ITEM GOING TO GET OVER SOON..weeksCover:"+weeksCover+" ITEM DETAILS:"+strLine0);
                           if(false && (inboundMap.get(sku)>0 && inboundMap.get(sku) >reqPacks.get(sku)/3)){
                               System.out.println("Item to get over, but coming inbound..So skip");
                               
                           }else{
                                 asin=asinMap.get(sku) ;
                                 if(inboundMap.get(sku)==null)
                                     inboundMap.put(sku, (Integer)0);
                               s=sku+"\t"+asinMap.get(sku) +"\t"+itemNameMap.get(sku)+"\t"+receivedMap.get(sku)+"\t"+healthTotalMap.get(sku)+"\t"+(remainingMap.get(sku)+inboundMap.get(sku))+"\t"+weeksCover+"\t"+currentProfitMap.get(asin)+"\t"+reqPacks.get(sku) +"\t"+RESTOCK_TXT+"\t"+soldby.get(asinMap.get(sku))+"\t"+my_inventory.get(asinMap.get(sku));;

                               boolean sFound=false;
                               for(int i=0;i<oldBuff.size();i++){
                                  // if(oldBuff.get(i).indexOf(s)>=0 || oldBuff.get(i).indexOf(s1)>=0){
                                    if(oldBuff.get(i).indexOf(sku)>=0 && oldBuff.get(i).indexOf("restock")>=0){
                                       //s=oldBuff.get(i);
                                       String comments=getComments(oldBuff.get(i));
                                        s+=comments;
                                        sFound=true;

                                   }
                               }
                               if(sFound==false){
                                   s+="\t"+"no"+"\t"+today;
                                   newRestock.append(s);
                                   newRestock.append("<br/>");
                               }
                               out0.write(s);
                               out0.newLine();
                               
                           }
                       }
                   }catch (Exception e){
                       e.printStackTrace();
                       Mail.emailException(FbaInventoryAnalyzer_NA.class,e);
                   }
                   
                  }   
                  
                   
                Iterator iter =receivedMap.keySet().iterator();
                while(iter.hasNext()){
                    
                    sku=(String) iter.next();
                    System.out.println("Current sku from recvd inventory map:"+sku);
                    //   if(sku.indexOf(DEBUG_SKU)>=0) {
                    //    System.out.println("debug");
                   // }                    
                    //System.out.println("Analyzing SKU:"+sku);
                    if(  (healthTotalMap.containsKey(sku)==false || healthTotalMap.get(sku)==null)){
                        if(itemReceivedYesterday.contains(sku)==false){
                                System.out.println("ITEM IS COMPLETELY SOLD:"+sku+",TOTAL QUANTITY SENT:"+receivedMap.get(sku));
                                System.out.println("Now required packs will always be null. Lets try to calculate");
                                asin=asinMap.get(sku) ;
                                s=sku+"\t"+asinMap.get(sku) +"\t"+itemNameMap.get(sku)+"\t"+receivedMap.get(sku)+"\t"+healthTotalMap.get(sku)+"\t"+remainingMap.get(sku)+"\t0\t"+currentProfitMap.get(asin)+"\t"+receivedMap.get(sku)+"\trestock"+"\t"+soldby.get(asinMap.get(sku))+"\t"+my_inventory.get(asinMap.get(sku));
                                String sdiscrepancy=s;                                
                                boolean sFound=false;
                                boolean allsoldbutdiscrepancy=false;
                                for(int i=0;i<oldBuff.size();i++){
                                    //if(oldBuff.get(i).indexOf(s)>=0||oldBuff.get(i).indexOf(s1)>=0)
                                    if(oldBuff.get(i).indexOf(sku)>=0 &&oldBuff.get(i).indexOf("restock") >=0  )
                                    {
                                        //s=oldBuff.get(i);
                                        String comments=getComments(oldBuff.get(i));
                                            s+=comments;
                                            sFound=true;

                                    }
                                    
                                    if(oldBuff.get(i).indexOf(sku)>=0 &&oldBuff.get(i).indexOf("item discrepancy") >=0  )
                                    {
                                        //s=oldBuff.get(i);
                                        String comments=getComments(oldBuff.get(i));
                                            sdiscrepancy+=comments;
                                            sFound=true;
                                            allsoldbutdiscrepancy=true;

                                    }
                                    
                                }
                                if(receivedMap.get(sku)<5){
                                    sFound=true;
                                }
                                

                                if(sFound==false){

                                    newRestock.append(s);
                                    newRestock.append("<br/>");
                                    //Lets check if all items are really sold

                                   // Integer soldItems=checkAllItemsGotSold(sku);
                                    Integer soldItems=0;
 
                                    if(receivedMap.get(sku)>soldItems){
                                         s=sku+"\t"+asinMap.get(sku) +"\t"+itemNameMap.get(sku)+"\t"+receivedMap.get(sku)+"\t"+soldItems+"\t"+remainingMap.get(sku)+"\t"+currentProfitMap.get(asin)+"\t"+reqPacks.get(sku)+"\t0\trestock"+"\t"+soldby.get(asinMap.get(sku))+"\t"+my_inventory.get(asinMap.get(sku));
                                        s+="\t"+"yes";
                                        
                                       
                                        allsoldbutdiscrepancy=true;
                                    }else{
                                        s+="\t"+"no";
                                    }
                                    s+="\t"+today;
                                     sdiscrepancy=s;
                                    

                                }
                                if(s.indexOf(DEBUG_SKU)>=0)
                                    
                                    System.out.println("Debug");                           
                                out0.write(s);
                                out0.newLine();
                                if(allsoldbutdiscrepancy){
                                    String snew=sdiscrepancy.replaceAll("restock", "item discrepancy");
                                    out0.write(snew);
                                    out0.newLine();
                                    
                                }
                        }
                    }else{
                        if(healthTotalMap.get(sku) !=receivedMap.get(sku) &&  
                                 itemReceivedYesterday.contains(sku)==false){
                            if(reservedMap.containsKey(sku) && 
                            healthTotalMap.get(sku)+reservedMap.get(sku) ==receivedMap.get(sku)){
                                System.out.println("Item does not match, but if i include reserved map, it matches");
                            }
                            else if(reservedMap.containsKey(sku)){
                                System.out.println("Some of the items are in reserved. No point in matching now..Wait for all to get over.");
                            }
                            else{
                            //Integer soldItems=checkAllItemsGotSold(sku);

                                        s=sku+"\t"+asinMap.get(sku) +"\t"+itemNameMap.get(sku)+"\t"+receivedMap.get(sku)+"\t"+healthTotalMap.get(sku)+"\t"+remainingMap.get(sku)+"\t0\t0\t"+reqPacks.get(sku)+"\titem discrepancy"+"\t"+soldby.get(asinMap.get(sku))+"\t"+my_inventory.get(asinMap.get(sku));
                                    boolean sFound=false;
                                    for(int i=0;i<oldBuff.size();i++){
                                        //if(oldBuff.get(i).indexOf(s)>=0||oldBuff.get(i).indexOf(s1)>=0){
                                            if(oldBuff.get(i).indexOf(sku)>=0 && oldBuff.get(i).indexOf("item discrepancy")>=0){
                                            //s=oldBuff.get(i);
                                            String comments=getComments(oldBuff.get(i));
                                                s+=comments;
                                                sFound=true;

                                        }
                                    }
                                    if(sFound==false){
                                        s+="\t"+"no"+"\t"+today;
                                        newDiscrepancy.append(s);
                                        newDiscrepancy.append("<br/>");
                                    }

                                        out0.write(s);
                                        out0.newLine();                                
                            }

                        }
                    }
                }
           out0.close();;  
           
          
        
           /* String[] to={"mprabagar80@gmail.com" ,"rathnasivasailam@gmail.com"};
            String[] cc={};
            String[] bcc={};
            if(newRestock.toString().length()>0 && EMAIL.equals("true")){
                Mail.sendMail( "smtpout.secureserver.net","465","true",
                "true",false,"javax.net.ssl.SSLSocketFactory","false",to,cc,bcc,
                "FBA Analyzer: Restock Following Items: ", newRestock.toString(),false,false,false,false,null);           
                
            }
            
            if(newDiscrepancy.toString().length()>0&& EMAIL.equals("true")){
                Mail.sendMail( "smtpout.secureserver.net","465","true",
                "true",false,"javax.net.ssl.SSLSocketFactory","false",to,cc,bcc,
                "FBA Analyzer: Discrepancy found, Analyze below Items: ", newDiscrepancy.toString(),false,false,false,false,null);           
                
            }
           */
                   
            
                src=new File("C:\\Google Drive\\Dropbox\\fba\\fbaanalyzer_new.txt");
                dest=new File("C:\\Google Drive\\Dropbox\\fba\\fbaanalyzer.txt");
               FileUtils.copyFile(src, dest);
               splitFbaanalyzerInEnd();
         
              //Now get and put the new ranks in restock.txt
         //   VerifyFBASkuRank v=new VerifyFBASkuRank();
         // v.populateRanksForRestockFile();
         //sortRestockFile();
                    
            
        }catch (Exception e){
        e.printStackTrace();
        Mail.emailException(FbaInventoryAnalyzer_NA.class,e);
        }


    }

    private static Integer checkAllItemsGotSoldNOTUSE(String sku)  {
        Integer qtySold=0;
        try{
            System.out.println("Going to check if all items sold for SKU:"+sku);
            HtmlPage page;
                if(webClient1==null){
                    webClient1=new WebClient(BrowserVersion.FIREFOX_38 );
                        if(RetrievePage.isOffice()){
                             System.setProperty("java.net.preferIPv4Stack", "true");
                            webClient1 = new WebClient(BrowserVersion.getDefault(),"163.241.128.141", 80);
                            DefaultCredentialsProvider credentialsProvider = (DefaultCredentialsProvider) webClient1.getCredentialsProvider();
                        credentialsProvider.addCredentials("us083274",  RetrievePage.PASSWORD1);   

                        }    

                        System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "error");
                        webClient1.getOptions().setThrowExceptionOnScriptError(false);
                        webClient1.getOptions().setJavaScriptEnabled(false);
                        webClient1.getOptions().setCssEnabled(false);


                        page = webClient1.getPage("https://sellercentral.amazon.com/");
                        //System.out.println(page.asText());
                ((HtmlTextInput) page.getElementById("username")).setText("packfba@gmail.com");
                ((HtmlPasswordInput) page.getElementById("password")).setText("dummy123");
                 System.out.println("Signing In");
                page=(HtmlPage) ((HtmlButton) page.getElementById("sign-in-button")).click();

                    
                }
                
                String url="https://sellercentral.amazon.com/gp/orders-v2/list?searchType=MerchantSKU&searchKeyword="+sku+"&searchDateOption=preSelected&preSelectedRange=180&exactDateBegin=8%2F10%2F12&exactDateEnd=8%2F13%2F12&searchFulfillers=afn&statusFilter=Default&sitesall=sitesall&sortBy=OrderStatusDescending&showPending=1&isDebug=&isAdvancedSearch=1&ignoreSearchType=0&searchLanguage=en_US&itemsPerPage=1000";        
                System.out.println("To check if all items are sold, visiting URL:"+url);
                
                page=  webClient1.getPage(url);   
                List<DomElement>  elems =  (List<DomElement>) page.getByXPath("//td[@width='20%']");                
               
                if(elems.size()>0){
                    System.out.println("Size of orders which are strong and inside tiny td:"+elems.size());
                    for(int i=0;i<elems.size();i++){
                        if(elems.get(i).getParentNode().getParentNode().asText().indexOf(sku)<0){
                            System.out.println("Line item does not contain sku//.skkipping, line:"+elems.get(i).getParentNode().getParentNode().asText() +" SKU:"+sku);
                            continue;
                        }
                        if(elems.get(i).getParentNode().getParentNode().asText().indexOf("Refund")>=0){
                            System.out.println("Refind aplied for line item.skkipping, line:"+elems.get(i).getParentNode().getParentNode().asText() +" SKU:"+sku);
                            continue;
                        }
                        
                        if(elems.get(i).getParentNode().getParentNode().asXml().indexOf("grey")>=0){
                            System.out.println("Line item is greyd..skkipping, line:"+elems.get(i).getParentNode().getParentNode().asText() +" SKU:"+sku);
                            continue;
                        }
                        
                        String s=elems.get(i).asText();
                        if(s!=null && s.indexOf("QTY:")>=0){
                            s=StringUtils.substringAfter(s, "QTY:").trim();
                            qtySold+=Integer.parseInt(s);
                            
                        }
                    }
                }
                
        }catch (Exception e){
            e.printStackTrace();
        }
        return qtySold;
    }
    
  public static  void sortRestockFile(){
        //Put the rank as additional column in the existing-all-inventory file
        
        try{
    
          FileInputStream fstream = new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\restock.txt");
          // Get the object of DataInputStream
         DataInputStream  in = new DataInputStream(fstream);
         BufferedReader  br = new BufferedReader(new InputStreamReader(in));
          
          TreeMap<Float, String> items=new TreeMap();
          
           String header=null;
           String asin,rank;
           String str;
           while ((str = br.readLine()) != null)   {
               if(header==null){
                   header=str;
                   continue;
               }
             //  if(str.indexOf("Biscoff")>=0)
             //      System.out.println("Debug");
               Thread.sleep(100);
               String reqPacks=str.split("\t")[8];
               String orderFrom="";
               Float keyF;
               if(str.split("\t").length>14)
                   orderFrom=str.split("\t")[14];
               if(reqPacks.indexOf("null")>=0)
                   reqPacks=str.split("\t")[3];
               if(orderFrom!=null && orderFrom.toLowerCase().equals("no")){
                   reqPacks="-99999";
               }
               
               String profit=str.split("\t")[7];
               try{
                   
                  /* if(profit==null || profit.trim().equals("null") ){
                       ///Unknwon profits are treated as .01;
                       profit="0.01";
                      if(Integer.valueOf(reqPacks)<0)
                        profit="999" ;
                   else
                          profit="-999" ;
                   
                       
                   }*/
                   
                   /*if(profit!=null && profit.trim().equals("null") ==false && Float.valueOf(profit)<0){
                       Integer tProfit=Integer.valueOf(StringUtils.substringBefore(profit, "."));
                       qty=  String.valueOf(999*tProfit);
                       
                   }*/
               }catch (Exception e){
                   e.printStackTrace();
               }
               
               if(profit==null || profit.trim().equals("null") || profit.trim().equals("profit") ||(profit.length()>0&& Float.valueOf(profit)==0))
                   profit="0."+String.valueOf(System.currentTimeMillis()%1000000);
               if(Float.valueOf(reqPacks)==0)
                   reqPacks="0.001";
               
               Thread.sleep(100);
              if(profit.indexOf(".")<0) profit=profit +"."+ System.currentTimeMillis()%1000000;
              if(reqPacks.indexOf(".")<0) reqPacks=reqPacks +"."+ System.currentTimeMillis()%1000000;

               keyF=Float.valueOf(reqPacks)*Float.valueOf(profit) ;
               if(Float.valueOf(profit)<0 && Float.valueOf(reqPacks)<0)
                   keyF=keyF*(float)-1;
               
               Float origF=keyF;
               // keyF=999999-keyF;
                if(items.containsKey(keyF)){
                    
                    System.out.println("ERROR::::::::::::how is it possible , items already content keyF:"+keyF +" items:"+items.get(keyF) +" current item:"+str);
                }
                      
                
                
                
                items.put(keyF,str );
            }
          br.close();
         NavigableMap<Float,String>  itemsN= items.descendingMap();
        FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\fba\\restock.txt"),new File("C:\\Google Drive\\Dropbox\\fba\\restock-unsorted.txt"));

        FileWriter writer = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\restock.txt");
        writer.write(header);
        writer.write('\n');

        for(String val : itemsN.values()){
                writer.write(val);
                writer.write('\n');
        }
        writer.close();


        }catch (Exception e){
            e.printStackTrace();;
        }

    }

    private static void createFbaanalyzerInBegining() throws Exception {
          BufferedReader br0 = new BufferedReader(new
          InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\restock.txt")));

          String s;
          ArrayList lines=new ArrayList();
          FileWriter fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\fbaanalyzer.txt");
          BufferedWriter out0 = new BufferedWriter(fstream_out0);
        while ((s = br0.readLine()) != null)   {
           out0.write(s);
           out0.newLine();         
           
        }
        br0.close();

         br0 = new BufferedReader(new
          InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\discrepancy.txt")));
         //Ignore header line from dicrepancy file
         br0.readLine();
                 
        while ((s = br0.readLine()) != null)   {
            if(s.indexOf("restock")>=0){
                System.out.println("skipping from discrpeancy.txt since it contains line for restock:"+s);
            }
           out0.write(s);
           out0.newLine();            
        }
        br0.close();
         
        out0.close();
    }

    private static void splitFbaanalyzerInEnd() throws Exception {
          BufferedReader br0 = new BufferedReader(new
          InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\fbaanalyzer.txt")));

          String s;
          FileWriter fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\restock.txt");
          BufferedWriter out0 = new BufferedWriter(fstream_out0);
          
          FileWriter fstream_out1 = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\discrepancy.txt");
          BufferedWriter out1 = new BufferedWriter(fstream_out1);

           s=br0.readLine();
           out0.write(s);
           out0.newLine();
           
           out1.write(s);
           out1.newLine();
           
        while ((s = br0.readLine()) != null)   {
            if(s.indexOf("restock")>=0)    {
                try{
                    String sent_quantity=s.split("\t")[3];
                    if(sent_quantity.equals("null") ||
                        Integer.parseInt(sent_quantity)<5){
                        //Restock only itmes that I have sent atleast 5
                        continue;
                    }
                  
                }catch (Exception e){e.printStackTrace();}
                out0.write(s);
                out0.newLine();
            }
            if(s.indexOf("item discrepancy")>=0)    {
                out1.write(s);
                out1.newLine();
            }
            
                
                        
        }
        br0.close(); 
        out0.close();
        out1.close();
        
        new File("C:\\Google Drive\\Dropbox\\fba\\fbaanalyzer.txt").delete();
        new File("C:\\Google Drive\\Dropbox\\fba\\fbaanalyzer_new.txt").delete();
    }

}

