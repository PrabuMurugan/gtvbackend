/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bombayplaza.pricematch;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.lang.StringUtils;
/**
 *
 * @author us083274
 */
public class EmailUnShippedOrders {
     public static void main(String args[]){
         EmailUnShippedOrders e=new EmailUnShippedOrders();
         e.emailUnshippedOrderReport();
     }

    public void emailUnshippedOrderReport() {
         StringBuffer warningString=new StringBuffer();
         StringBuffer openOrders=new StringBuffer();
          boolean lateshipment=false;
          StringBuffer lateShipmentStr=new StringBuffer();
        try{
            
            BufferedReader  br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\order-unshipped.txt")));
            String strLine0;
            StringBuffer strB=new StringBuffer();
            HashMap orders=new HashMap();
            HashMap date_map=new HashMap();
            TreeMap<String,Integer> sorted_map = new TreeMap();
            String title,sku;
            Integer quantity;
            
            String left_days="";
            String order_id="";
           
            

            boolean firstrecord=true;
             lateShipmentStr.append("<table border=2>");
            while ((strLine0 = br0.readLine()) != null)   {
                if(firstrecord){
                    firstrecord=false;
                    continue;
                }
              
                  title=strLine0.split("\t")[11];
                  left_days=strLine0.split("\t")[6];
                  order_id=strLine0.split("\t")[0];
                  if(left_days!=null && (left_days.trim().equals("-1") || left_days.trim().equals("-2")) ){
                      lateshipment=true;
                       lateShipmentStr.append("<tr> <td><a href='http://www.teapplix.com/h/mprabagar80/admin/index.pl?Action=AgentOrderManager&reset_search=1'>"+ order_id +"</a></td><td>"+title +"</tr>");
                  }
                          
                    String origtitle=title;
                  title=title.replaceAll(",", " ");
                  title=title.toLowerCase();
                 /* title=title.replaceAll("lb", "bombay");
                  title=title.replaceAll("oz", "delhi");

                  title=title.toLowerCase().replaceAll("pound", " lb ");
                  title=title.replaceAll("ounce", " oz ");

                  title=title.replaceAll( "bombay"," pound ");
                  title=title.replaceAll("delhi"," ounce ");
                  * */
                   
                  title=title.toUpperCase();

                  System.out.println("origtitle:"+origtitle+",title:"+title);

                  quantity=Integer.valueOf(strLine0.split("\t")[12]);
                  sku=strLine0.split("\t")[10];

                  if(title.toLowerCase().indexOf("pack")>=0){
                        
                        String str=StringUtils.substringAfterLast(title,"(");
                        String a[]=StringUtils.split(str," - )");
                        int packcount=1;
                        for(int i=0;i<a.length;i++){
                        try{
                            packcount=Integer.valueOf(a[i].trim());

                        }catch (Exception e){}
                        }
                        quantity=quantity*packcount;
                        title=StringUtils.substringBefore(title,"(");
                        title=title.replaceAll(",", "").replaceAll("\r","").replaceAll("\n", "");
                  }
                  if(orders.containsKey(title)){
                      quantity=(Integer)orders.get(title)+quantity;

                  }
                  orders.put(title,quantity);
                  date_map.put(title, sku);
            }
             lateShipmentStr.append("</table>");

            sorted_map.putAll(orders);

          System.out.println("results");
            FileWriter fstream_out1 = new FileWriter("C:\\Google Drive\\Dropbox\\program\\order-unshipped-new.csv");
            BufferedWriter out1 = new BufferedWriter(fstream_out1);

             openOrders.append("<table border=2>");
            for (String key : sorted_map.keySet()) {
                System.out.println("key/value: " + key + "/"+sorted_map.get(key));
                 out1.write(key+","+sorted_map.get(key)  );
                  out1.newLine();
                  openOrders.append("<tr> <td><a href='http://www.amazon.com/s/ref=nb_sb_noss?url=me%3DA1E3GOW3768RBB&field-keywords="+date_map.get(key)+"'>"+ key +"</a></td><td>"+sorted_map.get(key) +"</tr>");

            }
            openOrders.append("</table>");
            out1.close();

            
        }catch (Exception e){
            e.printStackTrace();
            warningString.append(e.toString());
        }
          
            System.out.println("Calling email service to email new-inventroy.csv");
            //String[] to={"mprabagar80@gmail.com","nspraveen91@gmail.com"};
            String[] to={"mprabagar80@gmail.com" ,"swami.kumar@gmail.com"};
            String[] cc={};
            String[] bcc={};
            System.out.println("Sending email with body:"+warningString.toString());
            Mail.sendMail( "smtpout.secureserver.net","465","true",
            "true",false,"javax.net.ssl.SSLSocketFactory","false",to,cc,bcc,
            "ORDERSTOBESHIPPED",openOrders.toString(),false,false,true,false,null);
            
            
            if(lateshipment){
                System.out.println("Calling email service  send lateshipment");
 
                Mail.sendMail( "smtpout.secureserver.net","465","true",
                "true",false,"javax.net.ssl.SSLSocketFactory","false",to,cc,bcc,
                "LATESHIPMENT TOMORROW MORNING",lateShipmentStr.toString(),false,false,true,false,null);                
            }

    }
}

