/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bombayplaza.pricematch;

import bombayplaza.catalogcreation.CreateCatalog;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
//import com.tajplaza.products.GetMyFeesEstimateSample;
import com.tajplaza.products.GetMyFeesEstimateSample;
import com.tajplaza.products.ListMatchingProductsSample;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.bcel.generic.ARRAYLENGTH;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
/**
 *
 * @author us083274
 */
public class RetrievePage {
    public static String PASSWORD1="Naduvai25";
    public static boolean JAVASCRIPT=false;
    public static String [] prxies = {};
    public static boolean USE_LOCAL_CACHE=true;
    public static int SLEEP_TIME_ALL_PROXIES_USED_ONCE=10;
    public static int NUMBER_TIMES_PROXIES_BEFORE_SLEEP=10;
    private static int iIteration=0;
    public static int faildProxyCount=0;
    public static ArrayList failedProxy = new ArrayList<String>();
    static {
           try {
               //BufferedReader  br001 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\proxylist.txt")));
               BufferedReader  br001 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\proxylist.txt")));
                String strLine;
                ArrayList <String>list = new ArrayList<String>();
                while ((strLine= br001.readLine()) != null )   {
                    if(strLine.trim().indexOf("#")==0)
                        continue;
                    list.add(strLine);
                }
                System.out.println("[ RetrievePage ] Number of proxies in file : "+ list.size());
                prxies = new String[list.size()];
                list.toArray(prxies);
                br001.close();
                System.out.println("[ RetrievePage ] Number of proxies in ary : "+ prxies.length);
           } catch(Exception  e) {
              // e.printStackTrace();
               //System.exit(1);
           }
          
    }
 
    public static int iCounter1=0;
    public static int iCounter2=0;
    public static void main(String args[]){
        try {
            Connection con=getConnection();
            USE_PROXIES=true;
           // HtmlPage h = getPageInternal("http://whatismyipaddress.com/",false,false);
           // DomElement elem=(DomElement) h.getByXPath("//div[@id='section_left']").get(0);
          //  if(elem!=null)
          //      System.out.println("My IP  IS :"+elem.asText());
            RetrievePage.USE_PROXIES=true;
          HashMap hm= RetrievePage.getDetailsFromFbaCalculatorNewMWS(null,"B0090C1R4W","20",true);
          System.out.println("WEight returned:"+hm.get("productInfoWeight"));
            hm=MatchThePrice.getProductDetails("B002869DD4", true,true,false);
 
             // System.out.println("bestprice:"+hm.get("bestprice"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    public static boolean JUST_DIMENSIONS_NEEDED=false;
    public static boolean DIMENSION_WEIGHT_USED=false;
    public static HashMap getDetailsFromFbaCalculatorNewMWS(Connection con_in,String asin,String price,boolean gotointernet){
          HashMap hm=new HashMap();
            hm.put("AFNWeightHandlingFee",Float.valueOf("0"));
            hm.put("fbaFees",Float.valueOf("3"));
            Float productInfoWeight=(float)0;
            if(price!=null)
                    hm.put("fbaMargin",(Float.valueOf(price)*.85)-(Float)hm.get("fbaFees"));
           try{
               //System.out.println("Connecting to table to retrieve fba fees");
               Connection con1=null;
               if(con_in==null)
                   con1=getConnection();
             else
                   con1=con_in;
             PreparedStatement pst = null;
             ResultSet rs = null;
             pst=con1.prepareStatement("select asin, productInfoLength ,     productInfoWidth ,    productInfoHeight ,productInfoWeight ,        productInfoWeightUnits ,    AFNWeightHandlingFee ,    orderHandlingFee ,    fbaDeliveryServicesFee ,    pickAndPackFee ,    storageFee ,    fbaFees ,    fbaMargin,weight_from_amazon_page from fba_fees_all where asin=?");
             pst.setString(1, asin);
             rs=pst.executeQuery();
             if(rs.next()){
                  hm.put("productInfoLength",rs.getFloat("productInfoLength"));
                  hm.put("productInfoWidth",rs.getFloat("productInfoWidth"));
                  hm.put("productInfoHeight",rs.getFloat("productInfoHeight"));
                  hm.put("actualWeight",rs.getFloat("productInfoWeight"));
                  hm.put("origProductInfoWeight",rs.getFloat("productInfoWeight"));
                  hm.put("productInfoWeightUnits",rs.getString("productInfoWeightUnits"));
                  hm.put("AFNWeightHandlingFee",rs.getString("AFNWeightHandlingFee"));
                  hm.put("orderHandlingFee",rs.getString("orderHandlingFee"));
                  hm.put("fbaDeliveryServicesFee",rs.getString("fbaDeliveryServicesFee"));
                  hm.put("pickAndPackFee",rs.getString("pickAndPackFee"));
                  hm.put("storageFee",rs.getString("storageFee"));
                  hm.put("fbaFees",rs.getFloat("fbaFees"));
                  hm.put("fbaMargin",rs.getString("fbaMargin"));
                  hm.put("weight_from_amazon_page",rs.getBoolean("weight_from_amazon_page"));
                 
                  if(hm.get("productInfoWeight")==null ||hm.get("productInfoWeight").equals("null"))
                      hm.put("productInfoWeight", "0.00");
                  
                  Float weight=rs.getFloat("productInfoWeight");
                  Float productInfoWidth=rs.getFloat("productInfoWidth");
                  Float productInfoLength=rs.getFloat("productInfoLength");
                  Float productInfoHeight=rs.getFloat("productInfoHeight");
                  
                  if( weight>0 && productInfoWidth*productInfoLength*productInfoHeight/166 > weight){
                      DIMENSION_WEIGHT_USED=true;
                       hm.put("actualWeight",  weight);
                       System.out.println("Using dimension weight from FBA calcualtor: "+productInfoWidth*productInfoLength*productInfoHeight/166 +" instead of actual weight : "+weight);
                      weight=productInfoWidth*productInfoLength*productInfoHeight/166;
                      
                  }else{
                       DIMENSION_WEIGHT_USED=false;
                  }
                            
 
                   hm.put("productInfoWeight",weight);
                                     
                  pst.close();
                  rs.close();
                 if(con_in==null)
                      con1.close();   
                 System.out.println("FBA weight for asin:" +asin +":  "+hm.toString());
                  return hm;
             }
               
           }catch (Exception e){
               e.printStackTrace();
           }
           //NOW GET FROM ACTUAL CALCULATOR
          try{ 
             // hm=GetMyFeesEstimateSample.getFBAFeesMWS(asin,price);
              //hm=RetrievePage.getDetailsFromFbaCalculatorNew(con_in, asin, price, gotointernet);
                 hm=ListMatchingProductsSample.getBestRankedProduct(asin);
          }catch (Exception e2){e2.printStackTrace();}
          boolean weightFromAmazonPage=false;
            productInfoWeight=(Float)(hm.get("productInfoWeight")!=null?hm.get("productInfoWeight"):(float)0);
            Float productInfoHeight=(Float)(hm.get("productInfoHeight")!=null?hm.get("productInfoHeight"):(float)1);
           Float  productInfoWidth=(Float)(hm.get("productInfoWidth")!=null?hm.get("productInfoWidth"):(float)1);
            Float productInfoLength=(Float)(hm.get("productInfoLength")!=null?hm.get("productInfoLength"):(float)1);
            Float fbaFees=(Float)(hm.get("fbaFees")!=null?hm.get("fbaFees"): (float)5);
            Float fbaStorageFees=(float).11;
            if(productInfoWeight>0){
                Double WeightD=Double.valueOf(productInfoWeight);
                WeightD=Math.ceil(WeightD);
                fbaFees=fbaFees+ Float.valueOf(Double.toString(WeightD)) *(float).2;
                
            }
                  if( productInfoWeight>0 && productInfoWidth*productInfoLength*productInfoHeight/166 > productInfoWeight){
                      DIMENSION_WEIGHT_USED=true;
                       hm.put("actualWeight",  productInfoWeight);
                       System.out.println("Using dimension weight from FBA calcualtor: "+productInfoWidth*productInfoLength*productInfoHeight/166 +" instead of actual weight : "+productInfoWeight);
                      productInfoWeight=productInfoWidth*productInfoLength*productInfoHeight/166;
                      
                  }else{
                       DIMENSION_WEIGHT_USED=false;
                  }            
            Float volume=productInfoHeight*productInfoWidth*productInfoLength/1728;
            fbaStorageFees=volume*(float).54;
            if(fbaStorageFees>0)
                fbaFees+=fbaStorageFees;
             fbaFees=Math.round(fbaFees*(float)100)/(float)100;
            String productInfoWeightUnits=(String)(hm.get("productInfoWeightUnits")!=null?hm.get("productInfoWeightUnits"):"pounds");
            
            if(productInfoWeight==0){
             System.out.println("Weight is not available in fba calculator. Try to get it from amazon page");
             CreateCatalog.ERROR_MOVE_TO_NEXT_PAGE=true;
            HashMap hm1=MatchThePrice.getProductDetails(asin,true,false,false);
            if(hm1  == null ){
                hm1=new HashMap();
                return hm1;
            }
            if(hm1!=null && hm1.containsKey("weight")){
                    productInfoWeight=(Float)hm1.get("weightInPoundsFloat");                        

                    hm.put("productInfoWeight",productInfoWeight);
                    hm.put("productInfoLength",productInfoLength);
                    hm.put("productInfoWidth",productInfoWidth);
                    hm.put("productInfoHeight",productInfoHeight);                                 
                    System.out.println("Using weight fromamazon page "+productInfoWeight);
                    weightFromAmazonPage=true;
                }                        
            }
               System.out.println("Connecting to table to insert fba fees");
               try{
                     Connection con=getConnection();
                     PreparedStatement pst = null;
                     pst=con.prepareStatement("insert into tajplaza.fba_fees_all(asin, productInfoLength ,     productInfoWidth ,    productInfoHeight ,productInfoWeight ,        productInfoWeightUnits ,    AFNWeightHandlingFee ,    orderHandlingFee ,    fbaDeliveryServicesFee ,    pickAndPackFee ,    storageFee ,    fbaFees ,    fbaMargin,weight_from_amazon_page) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                     pst.setString(1, asin);
                     pst.setFloat(2, productInfoLength);
                     pst.setFloat(3, productInfoWidth);
                     pst.setFloat(4, productInfoHeight);
                     pst.setFloat(5, productInfoWeight);             
                     pst.setString(6, productInfoWeightUnits);
                     pst.setFloat(7, 0);
                     pst.setFloat(8, 0);
                     pst.setFloat(9, 0);
                     pst.setFloat(10, 0);
                     pst.setFloat(11, 0);
                     pst.setFloat(12, fbaFees);
                     pst.setDouble(13, 0);
                     pst.setBoolean(14, weightFromAmazonPage);
                    // if(productInfoWeight>0)
                        pst.executeUpdate();
                    pst.close(); 

                     con.close();                   
               }catch (Exception e){e.printStackTrace();}

        
            System.out.println("FBA weight for asin:" +asin +":  "+hm.toString());                
          return hm;
    }
    //OLD FROM FBA CALCULATOR
 
     public static HashMap getDetailsFromFbaCalculatorOLD(Connection con_in,String asin,String price,boolean gotointernet){
         //This fba fees already takes care of the weight also
           HashMap hm=new HashMap();
            //NOW GET THE AFN FEES
            hm.put("AFNWeightHandlingFee",Float.valueOf("0"));
            hm.put("fbaFees",Float.valueOf("3"));
            Float productInfoWeight=(float)0;
            if(price!=null)
                    hm.put("fbaMargin",(Float.valueOf(price)*.85)-(Float)hm.get("fbaFees"));
           try{
               //System.out.println("Connecting to table to retrieve fba fees");
               Connection con1=null;
               if(con_in==null)
                   con1=getConnection();
             else
                   con1=con_in;
             PreparedStatement pst = null;
             ResultSet rs = null;
             pst=con1.prepareStatement("select asin, productInfoLength ,     productInfoWidth ,    productInfoHeight ,productInfoWeight ,        productInfoWeightUnits ,    AFNWeightHandlingFee ,    orderHandlingFee ,    fbaDeliveryServicesFee ,    pickAndPackFee ,    storageFee ,    fbaFees ,    fbaMargin,weight_from_amazon_page from fba_fees_all where asin=?");
             pst.setString(1, asin);
             rs=pst.executeQuery();
             if(rs.next()){
                  hm.put("productInfoLength",rs.getFloat("productInfoLength"));
                  hm.put("productInfoWidth",rs.getFloat("productInfoWidth"));
                  hm.put("productInfoHeight",rs.getFloat("productInfoHeight"));
                  hm.put("productInfoWeight",rs.getFloat("productInfoWeight"));
                  hm.put("productInfoWeightUnits",rs.getString("productInfoWeightUnits"));
                  hm.put("AFNWeightHandlingFee",rs.getString("AFNWeightHandlingFee"));
                  hm.put("orderHandlingFee",rs.getString("orderHandlingFee"));
                  hm.put("fbaDeliveryServicesFee",rs.getString("fbaDeliveryServicesFee"));
                  hm.put("pickAndPackFee",rs.getString("pickAndPackFee"));
                  hm.put("storageFee",rs.getString("storageFee"));
                  hm.put("fbaFees",rs.getFloat("fbaFees"));
                  hm.put("fbaMargin",rs.getString("fbaMargin"));
                  hm.put("weight_from_amazon_page",rs.getBoolean("weight_from_amazon_page"));
                 
                  if(hm.get("productInfoWeight")==null ||hm.get("productInfoWeight").equals("null"))
                      hm.put("productInfoWeight", "0.00");
                  
                  Float weight=rs.getFloat("productInfoWeight");
                  Float productInfoWidth=rs.getFloat("productInfoWidth");
                  Float productInfoLength=rs.getFloat("productInfoLength");
                  Float productInfoHeight=rs.getFloat("productInfoHeight");
                  
                  if( productInfoWidth*productInfoLength*productInfoHeight/166 > weight){
                      DIMENSION_WEIGHT_USED=true;
                       hm.put("actualWeight",  weight);
                       System.out.println("Using dimension weight from FBA calcualtor: "+productInfoWidth*productInfoLength*productInfoHeight/166 +" instead of actual weight : "+weight);
                      weight=productInfoWidth*productInfoLength*productInfoHeight/166;
                      
                  }else{
                       DIMENSION_WEIGHT_USED=false;
                  }
                            
 
                   hm.put("productInfoWeight",weight);
                                     
                  pst.close();
                  rs.close();
                 if(con_in==null)
                      con1.close();                 
                  return hm;
             }
               
           }catch (Exception e){
               e.printStackTrace();
           }
           if(gotointernet==false)
               return hm;
         try{
             System.out.println("Going to calculate FBA Fees for ASIN:"+asin);
             if(price==null)
                 price="100";
             //NEw logic Prabu :06/14/2015
            // String url="https://sellercentral.amazon.com/hz/fba/profitabilitycalculator/productmatches?searchKey=XXX&language=en_US&profitcalcToken=zvympuD9YogAsC4TbYnaj2BtxvrXAj3D";
             
                WebClient webClient1 ;
      String strIP=null;
       String proxyUsername="mprab80";
       String proxypassword="dummy123";      
       if(USE_PROXIES && RetrievePage.prxies.length>0 && RetrievePage.isOffice()==false  && RetrievePage.isHome()==false){
            //System.out.println("[ RetrivePage ] iCounter : " + iCounter);
            strIP = RetrievePage.prxies[RetrievePage.iCounter2].split(":")[0];
            String strPort = RetrievePage.prxies[RetrievePage.iCounter2].split(":")[1];

            if(RetrievePage.prxies[RetrievePage.iCounter2].split(":").length>2){
                proxyUsername = RetrievePage.prxies[RetrievePage.iCounter2].split(":")[2];
                proxypassword = RetrievePage.prxies[RetrievePage.iCounter2].split(":")[3];
            }
            //System.out.println("[ RetrivePage ] strIP : " + strIP);
            //System.out.println("[ RetrivePage ] strPort : " + strPort);
            if(iCounter2 == (RetrievePage.prxies.length-1)) {
                System.out.println("IP Counter reset");
                iCounter2=0;
                iIteration++;
                System.out.println("[ RetrivePage ] All proxies are used. Now let us decide to sleep or not.");
                System.out.println("[ RetrivePage ] SLEEP_TIME_ALL_PROXIES_USED_ONCE : " + SLEEP_TIME_ALL_PROXIES_USED_ONCE);
                System.out.println("[ RetrivePage ] NUMBER_TIMES_PROXIES_BEFORE_SLEEP : " + NUMBER_TIMES_PROXIES_BEFORE_SLEEP);
                System.out.println("[ RetrivePage ] iIteration : " + iIteration);
                if(NUMBER_TIMES_PROXIES_BEFORE_SLEEP == iIteration) {
                    System.out.println("[ RetrivePage ] Number of times proxies used is equal to number of iteration. Going to sleep for SLEEP_TIME_ALL_PROXIES_USED_ONCE  :"+SLEEP_TIME_ALL_PROXIES_USED_ONCE+" minutes");
                    try {
                        Thread.sleep(1000*60*SLEEP_TIME_ALL_PROXIES_USED_ONCE);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(RetrievePage.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println("[ RetrivePage ] After waking up from sleep we reset iIteration");
                    iIteration=0;
                }
                

            } else {
                iCounter2++;
            }             
            webClient1 = new WebClient(BrowserVersion.FIREFOX_38,strIP,Integer.parseInt(strPort));
         } else {
           webClient1 = new WebClient(BrowserVersion.FIREFOX_38);
         }                
         if(proxyUsername!=null){
             //System.out.println("Setting proxy user name:"+proxyUsername+", password:"+proxypassword);
          DefaultCredentialsProvider credentialsProvider1 = (DefaultCredentialsProvider) webClient1.getCredentialsProvider();
            credentialsProvider1.addCredentials(  proxyUsername,   proxypassword);   
             
         }              
                  
            
                System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "error");
            WebClientOptions options = webClient1.getOptions();
                options.setThrowExceptionOnScriptError(false);
                options.setJavaScriptEnabled(false);
                options.setCssEnabled(false);
               

                 HtmlPage page=webClient1.getPage("https://sellercentral.amazon.com/hz/fba/profitabilitycalculator/index?lang=en_US");
                 if(page.getByXPath("//input[@name='profitcalcToken']").size()==0){
                     return hm;
                 }

                     HtmlInput inp=(HtmlInput) page.getByXPath("//input[@name='profitcalcToken']").get(0);
                     String token=inp.getValueAttribute();
                     String url="https://sellercentral.amazon.com/hz/fba/profitabilitycalculator/productmatches?searchKey=ASIN&language=en_US&profitcalcToken=TOKEN";
                     url=url.replaceAll("TOKEN", token);
                     url=url.replaceAll("ASIN", asin);
                     
                    String jsonResponse=webClient1.getPage(url).getWebResponse().getContentAsString();
                     
 
                   Float productInfoLength=(float)0;
                   Float productInfoWidth=(float)0;
                   Float productInfoHeight=(float)0;
                   
                   String productInfoWeightUnits="";
                   Float AFNWeightHandlingFee=(float)0;
                   Float orderHandlingFee=(float)0;
                   Float fbaDeliveryServicesFee=(float)0;
                   Float pickAndPackFee=(float)0;
                   Float storageFee=(float)0;
                   Float fbaFees=(float)0;
                   Double fbaMargin=(double)0;
                   
                    JSONObject jsobj=JSONObject.fromObject(jsonResponse);
                    JSONObject asinObject=null;
                    if(jsobj.containsKey("data") && jsobj.get("data") instanceof JSONArray &&  jsobj.getJSONArray("data").size()>0    ){
                        JSONArray dataArray=jsobj.getJSONArray("data");
                        for (int obj=0;obj<dataArray.size();obj++){
                            //sometimes amazon returns multiple asins. lets pick the one matching our input asin
                               JSONObject tmpJsObj=(JSONObject) dataArray.get(obj);    
                               if(tmpJsObj.containsKey("asin") && tmpJsObj.getString("asin").equals(asin)){
                                   jsobj=tmpJsObj;
                                   asinObject=tmpJsObj;
                               }
                        }
                        // jsobj=(JSONObject) jsobj.getJSONArray("data").get(0);
                        if(jsobj.containsKey("weight") && jsobj.containsKey("width")  &&jsobj.getDouble("width")>0 ){
                            hm.put("productInfoLength",(float)0);
                            hm.put("productInfoWidth",(float)0);
                            hm.put("productInfoHeight",(float)0);
 

                             productInfoWeight=Float.valueOf(Double.toString(jsobj.getDouble("weight")));
                             try{
                                 if(jsobj.containsKey("width") &&  jsobj.getDouble("width")>0){
                                     Double width=jsobj.getDouble("width");
                                     productInfoWidth=(float)((double)width);
                                     
                                     Double length=jsobj.getDouble("length");
                                     productInfoLength=(float)((double)length);
                                     
                                     Double height=jsobj.getDouble("height");
                                     productInfoHeight=(float)((double)height);
                                  hm.put("productInfoLength",productInfoLength);
                                  hm.put("productInfoWidth",productInfoWidth);
                                  hm.put("productInfoHeight",productInfoHeight);
                                                         
 
                                 }
                                 
                             }catch (Exception e3){}
                            hm.put("productInfoWeight",productInfoWeight);

                            productInfoWeightUnits=jsobj.getString("weightUnit");
                            hm.put("productInfoWeightUnits",productInfoWeightUnits);                                  
                        }
                        //if(JUST_DIMENSIONS_NEEDED ==false && productInfoWidth>0 && asinObject!=null){
                        if(  productInfoWidth>0 && asinObject!=null){
                                JSONObject productDataObj=new JSONObject();
                                productDataObj.put("productInfoMapping", asinObject);
                                productDataObj.put("afnPriceStr", price);
                                productDataObj.put("mfnPriceStr", 0);
                                productDataObj.put("mfnShippingPriceStr", 0);
                                productDataObj.put("currency", "USD");
                                productDataObj.put("marketPlaceId", "ATVPDKIKX0DER");
                                productDataObj.put("hasFutureFee", "false");
                                productDataObj.put("futureFeeDate", "2015-08-06 00:00:00");
                                
                                
                               
                                String afnReqModel=productDataObj.toString();
                                  afnReqModel=afnReqModel.replaceAll("\"data\"", "\"productInfoMapping\"")  ;

                            WebRequest requestSettings = new WebRequest(
                              new URL("https://sellercentral.amazon.com/hz/fba/profitabilitycalculator/getafnfee?profitcalcToken="+token), HttpMethod.POST);

                           // requestSettings.setRequestParameters(new ArrayList());
                            
                           // requestSettings.getRequestParameters().add(new NameValuePair("profitcalcToken",token));
                             webClient1.addRequestHeader("Accept", "application/json, text/javascript, * /*; q=0.01");
                             webClient1.addRequestHeader("Accept-Encoding", "gzip, deflate");
                             webClient1.addRequestHeader("Accept-Language", "en-US,en;q=0.8");
                             
                             webClient1.addRequestHeader("Content-Type", "application/json;charset=UTF-8");
                             
                             
                            requestSettings.setRequestBody(afnReqModel);

                            // Finally, we can get the page
                              Page page2 = webClient1.getPage(requestSettings);
                               jsobj=JSONObject.fromObject(page2.getWebResponse().getContentAsString());
                            if(jsobj.containsKey("data")   && jsobj.getJSONObject("data").containsKey("afnFees")  ){
                                 jsobj=(JSONObject) jsobj.getJSONObject("data").getJSONObject("afnFees") ;
                                     fbaFees= Float.valueOf("0");
                                 if(jsobj.containsKey("weightHandlingFee")){
                                     fbaFees=fbaFees+Float.valueOf(Double.toString(jsobj.getDouble("weightHandlingFee")));
                                     hm.put("AFNWeightHandlingFee",Float.valueOf(Double.toString(jsobj.getDouble("weightHandlingFee"))));
                                     AFNWeightHandlingFee=Float.valueOf(Double.toString(jsobj.getDouble("weightHandlingFee")));
                                 }
                                 if(jsobj.containsKey("orderHandlingFee")){
                                     fbaFees=fbaFees+Float.valueOf(Double.toString(jsobj.getDouble("orderHandlingFee")));
                                     orderHandlingFee=Float.valueOf(Double.toString(jsobj.getDouble("orderHandlingFee")));
                                 }

                                 if(jsobj.containsKey("fbaDeliveryServicesFee")){
                                     fbaFees=fbaFees+Float.valueOf(Double.toString(jsobj.getDouble("fbaDeliveryServicesFee")));
                                     fbaDeliveryServicesFee=Float.valueOf(Double.toString(jsobj.getDouble("fbaDeliveryServicesFee")));
                                 }
                                 if(jsobj.containsKey("pickAndPackFee")){
                                     fbaFees=fbaFees+Float.valueOf(Double.toString(jsobj.getDouble("pickAndPackFee")));
                                     pickAndPackFee=Float.valueOf(Double.toString(jsobj.getDouble("pickAndPackFee")));
                                 }

                                 if(jsobj.containsKey("storageFee")){
                                     fbaFees=fbaFees+Float.valueOf(Double.toString(jsobj.getDouble("storageFee")));
                                     storageFee=Float.valueOf(Double.toString(jsobj.getDouble("storageFee")));
                                 }

                                 //Inbound transportation fees
                                try{
                                    if(productInfoWeight>0){
                                        Double WeightD=Double.valueOf(productInfoWeight);
                                        WeightD=Math.ceil(WeightD);

                                        fbaFees=fbaFees+ Float.valueOf(Double.toString(WeightD)) *(float).2;

                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                fbaFees=Math.round(fbaFees*(float)100)/(float)100;
                                  hm.put("fbaFees",fbaFees);
                                 if(price!=null && price.equals("null")==false)
                                 {
                                   fbaMargin=Double.valueOf(price)*.85-fbaFees;

                                  hm.put("fbaMargin",fbaMargin);

                                 }
                               }                   
                        }//END OF IF DIMENSION ONLY NEEDED LOOP.
                             
         
                        
                    }
                    boolean weightFromAmazonPage=false;
                    if(productInfoWeight==0){
                        System.out.println("Weight is not available in fba calculator. Try to get it from amazon page");
                        
                        HashMap hm1=MatchThePrice.getProductDetails(asin,true,false,false);

                        if(hm1!=null && hm1.containsKey("weight")){
                                productInfoWeight=(Float)hm1.get("weightInPoundsFloat");                        
                                productInfoHeight=(float)1;
                                productInfoWidth=(float)1;
                                productInfoLength=(float)1;
                                hm.put("productInfoWeight",productInfoWeight);
                                hm.put("productInfoLength",productInfoLength);
                                hm.put("productInfoWidth",productInfoWidth);
                                hm.put("productInfoHeight",productInfoHeight);                                 
                                System.out.println("Using weight fromamazon page "+productInfoWeight);
                                weightFromAmazonPage=true;
                        }                        
                    }
               System.out.println("Connecting to table to insert fba fees");
             Connection con=getConnection();
             PreparedStatement pst = null;
             
             pst=con.prepareStatement("insert into fba_fees_all(asin, productInfoLength ,     productInfoWidth ,    productInfoHeight ,productInfoWeight ,        productInfoWeightUnits ,    AFNWeightHandlingFee ,    orderHandlingFee ,    fbaDeliveryServicesFee ,    pickAndPackFee ,    storageFee ,    fbaFees ,    fbaMargin,weight_from_amazon_page) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
             pst.setString(1, asin);
             pst.setFloat(2, productInfoLength);
             pst.setFloat(3, productInfoWidth);
             pst.setFloat(4, productInfoHeight);
             pst.setFloat(5, productInfoWeight);             
             pst.setString(6, productInfoWeightUnits);
             pst.setFloat(7, AFNWeightHandlingFee);
             pst.setFloat(8, orderHandlingFee);
             pst.setFloat(9, fbaDeliveryServicesFee);
             pst.setFloat(10, pickAndPackFee);
             pst.setFloat(11, storageFee);
             pst.setFloat(12, fbaFees);
             pst.setDouble(13, fbaMargin);
             pst.setBoolean(14, weightFromAmazonPage);
            // if(productInfoWeight>0)
                pst.executeUpdate();
            pst.close(); 
             
             con.close();
        
                
           }catch (Exception e){
               e.printStackTrace();
                // hm.put("productInfoWeight", (float)-1);
           }
            if(hm.get("productInfoWeight")==null ||hm.get("productInfoWeight").equals("null"))
                hm.put("productInfoWeight", (float)0);
                  
                
           return hm;
                  
           
       }    
    public static boolean isOffice(){
        try{
               InetAddress addr = InetAddress.getLocalHost();
                String ip=addr.getLocalHost().getHostAddress();
             //   System.out.println("IP :"+ip);
                if(ip!=null && (ip.indexOf("163.")==0||ip.indexOf("10.112")==0))
                    return true;
        }catch (Exception e){e.printStackTrace();;}
        return false;

    }
  public static boolean isHome(){
        try{
               InetAddress addr = InetAddress.getLocalHost();
                String ip=addr.getLocalHost().getHostAddress();
             //   System.out.println("IP :"+ip);
                if(ip!=null && (ip.indexOf("192.168")==0||ip.indexOf("192.168")==0))
                    return true;
        }catch (Exception e){e.printStackTrace();;}
        return false;

    }
   public static boolean USE_PROXIES=true;
   public static boolean USE_VPN=false;
  
   static  int userAgentCount=0;
   static WebClient amazonUPCSearchClient=null;
   public static  boolean USE_CACHE=false;
    public static HtmlPage getPageInternal(String url,boolean useproxy,boolean javascript) throws IOException{
        //PEOPLE SHOULD NOT BE USING THIS SLOWLY..THEY HAVE TO GO THROUGH getPage in CreateCatalog.java that has PROXY_NOT_WORKED handled.
       JAVASCRIPT=javascript;
   //     System.out.println("[ RetrivePage ] Inside getPage. URL : " + url);
   //     System.out.println("[ RetrivePage ] Inside getPage. useproxy : " + useproxy);
   //     System.out.println("[ RetrivePage ] Inside getPage. bjavascript : " + javascript);
        
/*
    WebRequest requestSettings=null;
      if(false && isOffice()==false && useproxy){
                requestSettings = new WebRequest(
      new URL("http://3.hidemyass.com/includes/process.php?action=update&idx=13"), HttpMethod.POST);

    // Then we set the request parameters
    requestSettings.setRequestParameters(new ArrayList());
    requestSettings.getRequestParameters().add(new NameValuePair("u", url));
    requestSettings.getRequestParameters().add(new NameValuePair("obfuscation", "1"));
  }
*/
// Finally, we can get the page
 
       //  System.setProperty("org.apache.commons.logging.diagnostics.dest", "STDOUT"); 
       System.setProperty("org.apache.commons.logging.simplelog.defaultlog", "ERROR"); 
       System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog"); 
       WebClient wc = new WebClient(BrowserVersion.FIREFOX_38); 
       url=url.trim();
      // if(url.indexOf("amazon")>=0)
        //   javascript=true;
       System.out.println("[ RetrivePage ] Visiting-10 URL: "+url +" ,javascript:"+javascript +" :USE_PROXIES:"+USE_PROXIES);
       WebClient webClient2;
 
        //System.out.println("Using user agent as 5.0 (Windows NT 6.1; WOW64) ...");
       
        
        
  //     System.out.println("[ RetrivePage ] BV : " + bv);
       String proxyUsername=null;
       String proxypassword=null;
      /* if(RetrievePage.prxies.length==0 && url.indexOf("amazon.com")>=0){
           System.out.println("I can not visit amazon without proxy.  Please make sure you have proxy IPs entered at C:\\temp2012\\proxylist.txt");
           System.exit(1);
       }*/
        String strIP=null;
       if(USE_PROXIES && RetrievePage.prxies.length>0 && RetrievePage.isOffice()==false ){
            //System.out.println("[ RetrivePage ] iCounter : " + iCounter);
            strIP = RetrievePage.prxies[RetrievePage.iCounter1].split(":")[0];
            String strPort = RetrievePage.prxies[RetrievePage.iCounter1].split(":")[1];

            if(RetrievePage.prxies[RetrievePage.iCounter1].split(":").length>2){
                proxyUsername = RetrievePage.prxies[RetrievePage.iCounter1].split(":")[2];
                proxypassword = RetrievePage.prxies[RetrievePage.iCounter1].split(":")[3];
            }
            System.out.println("[ RetrivePage ] strIP : " + strIP +" strPort:"+strPort);
            //System.out.println("[ RetrivePage ] strPort : " + strPort);
            if(iCounter1 == (RetrievePage.prxies.length-1)) {
                System.out.println("IP Counter reset");
                iCounter1=0;
                iIteration++;
                System.out.println("[ RetrivePage ] All proxies are used. Now let us decide to sleep or not.");
                System.out.println("[ RetrivePage ] SLEEP_TIME_ALL_PROXIES_USED_ONCE : " + SLEEP_TIME_ALL_PROXIES_USED_ONCE);
                System.out.println("[ RetrivePage ] NUMBER_TIMES_PROXIES_BEFORE_SLEEP : " + NUMBER_TIMES_PROXIES_BEFORE_SLEEP);
                System.out.println("[ RetrivePage ] iIteration : " + iIteration);
                if(NUMBER_TIMES_PROXIES_BEFORE_SLEEP == iIteration) {
                    System.out.println("[ RetrivePage ] Number of times proxies used is equal to number of iteration. Going to sleep for SLEEP_TIME_ALL_PROXIES_USED_ONCE  :"+SLEEP_TIME_ALL_PROXIES_USED_ONCE+" minutes");
                    try {
                        Thread.sleep(1000*60*SLEEP_TIME_ALL_PROXIES_USED_ONCE);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(RetrievePage.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    System.out.println("[ RetrivePage ] After waking up from sleep we reset iIteration");
                    iIteration=0;
                }
                

            } else {
                iCounter1++;
            }             
            webClient2 = new WebClient(BrowserVersion.FIREFOX_38,strIP,Integer.parseInt(strPort));
         } else {
           webClient2 = new WebClient(BrowserVersion.FIREFOX_38);
         }
       if(USE_CACHE==false){
           webClient2.getCache().setMaxSize(0);
       }
           
            webClient2.getOptions().setTimeout(30000); 
         if(proxyUsername!=null){
             //System.out.println("Setting proxy user name:"+proxyUsername+", password:"+proxypassword);
          DefaultCredentialsProvider credentialsProvider1 = (DefaultCredentialsProvider) webClient2.getCredentialsProvider();
            credentialsProvider1.addCredentials(  proxyUsername,   proxypassword);   
             
         }
         //webClient2.closeAllWindows();
        //  webClient2.getCache().clear();
        if(RetrievePage.isOffice()){
            System.setProperty("java.net.preferIPv4Stack", "true");
            webClient2 = new WebClient(BrowserVersion.FIREFOX_38,"proxydirect.tycoelectronics.com", 80);
            DefaultCredentialsProvider credentialsProvider = (DefaultCredentialsProvider) webClient2.getCredentialsProvider();
            credentialsProvider.addCredentials("us083274",  RetrievePage.PASSWORD1);    
        }
        
        WebClientOptions options = webClient2.getOptions();
        options.setThrowExceptionOnScriptError(false);
        options.setThrowExceptionOnFailingStatusCode(false);
        options.setJavaScriptEnabled(javascript);
        options.setCssEnabled(false);
        options.setPrintContentOnFailingStatusCode(false);
        LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit.javascript.StrictErrorReporter").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit.javascript.host.ActiveXObject").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit.javascript.host.html.HTMLDocument").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit.html.HtmlScript").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit.javascript.host.WindowProxy").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("org.apache").setLevel(Level.OFF);        
        MyAjaxController aj = new MyAjaxController();
        webClient2.setAjaxController(aj);
        
        MyWebConnection mc=new MyWebConnection(webClient2);
        webClient2.setWebConnection(mc);
       // webClient2.getOptions().setRedirectEnabled(false);
        
        HtmlPage mypage=null;
       /* if(isOffice()==false&& useproxy )
            mypage = webClient2.getPage(requestSettings);
        else
        * */
        
        try {
        //    System.out.println("[ RetrivePage ] Inside Try");
           
            options.setThrowExceptionOnScriptError(false);
            options.setCssEnabled(false);
            mypage = webClient2.getPage(url);
            
            if(javascript) webClient2.waitForBackgroundJavaScript(20000);
           
            
        } catch(FailingHttpStatusCodeException exception) {
            //exception.printStackTrace();
            
            if(exception.getStatusCode()==404||exception.getStatusCode()==400){
                System.out.println("[ RetrivePage ] ASIN page not found. "  +exception.getMessage());
                 
                throw new IOException("ASIN_NOT_FOUND");
                
            }  else          if(exception.getStatusCode()==500||exception.getStatusCode()==504){
                System.out.println("[ RetrivePage ] Exception, but still proceeding. "  +exception.getMessage());
                 throw exception;
                
            }
            else{
                System.out.println("[ RetrivePage ] Status Code " + exception.getStatusCode());
                System.out.println("[ RetrivePage ] PROXY_NOT_WORKED " +exception.getMessage());
                
                 if(!failedProxy.contains(strIP)) {
                    faildProxyCount++;
                    failedProxy.add(strIP);
                 }
                throw new IOException("PROXY_DID_NOT_WORKED");
                
            }
        } catch(Exception exception) {
            //exception.printStackTrace();
            faildProxyCount++;
            System.out.println("[ RetrivePage ] " + exception.getMessage());
            System.out.println("[ RetrivePage ] PROXY_NOT_WORKED " +exception.getMessage());
            if(exception.getMessage().indexOf("java.io.FileNotFoundException")>=0||exception.getMessage().indexOf("com.gargoylesoftware.htmlunit.UnexpectedPag")>=0){
                exception.printStackTrace();;
                return mypage;
            }
                
             if(!failedProxy.contains(strIP)) {
                    faildProxyCount++;
                    failedProxy.add(strIP);
                 }
            throw new IOException("PROXY_DID_NOT_WORKED");
        } 
        return mypage;
    }
  
    
      public static Connection getConnection() throws Exception{
            String   url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/tajplaza?autoReconnect=true";
          if(RetrievePage.isOffice()){
             url=url.replaceAll("tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306", "localhost:13306");
          }
       Connection con = null;
    PreparedStatement pst = null;
    ResultSet rs = null;
   
    String user = "root";
    String password = "admin123";
 
        
          
           try{
            Properties info = new Properties();
            /*info.put("proxy_type", "4"); // SSL Tunneling
            info.put("proxy_host", "163.241.128.203");
            info.put("proxy_port", "8088");
            info.put("proxy_user", "us083274");
            info.put("proxy_password", "Naduvai28");
            info.put("user", user);
            info.put("ssl","true");
            info.put("password", password);
            */
            con = DriverManager.getConnection(url, user, password) ;
           }catch (Exception e){
             // System.err.println("First exception while getting connection:"+e.getMessage()) ;
                try{
                    con = DriverManager.getConnection("jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/tajplaza", user, password);}catch (Exception e2)
                    {
                      System.err.println("Second exception while getting connection:"+e.getMessage()) ;
                        try{
                            con = DriverManager.getConnection("jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/tajplaza", user, password);
                        }catch (Exception e3){e3.printStackTrace();}
                     }

                        
            }
 
        try{
            PreparedStatement ps=con.prepareStatement("SELECT @@max_allowed_packet;");
             rs=ps.executeQuery();
            if(rs.next()){
                Double max_pkt=rs.getDouble(1);
                if(max_pkt<1677721){
                     pst=con.prepareStatement("SET GLOBAL max_allowed_packet=16777216");
                     pst.executeUpdate();                     
                    
                }
                    
            }
        }catch (Exception e){e.printStackTrace();}           
            return con;
} 
   public static Connection getConnection(String schema) throws Exception{
       Connection con = null;
    PreparedStatement pst = null;
    ResultSet rs = null;
    String   url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/"+schema;
    String user = "root";
    String password = "admin123";
 
        
          
           try{
                
            con = DriverManager.getConnection(url, user, password);
           }catch (Exception e){
               e.printStackTrace();
                con = DriverManager.getConnection("jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/tajplaza", user, password);
                
               
           }
            return con;
} 

 
      public static Connection getQAConnection() throws Exception{
       Connection con = null;
    PreparedStatement pst = null;
    ResultSet rs = null;
    String   url = "jdbc:mysql://162.242.160.179:3306/hb_bc";
    String user = "root";
    String password = "admin123";
 
        
          
           try{
                
            con = DriverManager.getConnection(url, user, password);
           }catch (Exception e){
               e.printStackTrace();
                con = DriverManager.getConnection("jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/tajplaza", user, password);
                
               
           }
            return con;
} 
      public static Connection getLocalConnection() throws Exception{
       Connection con = null;
    PreparedStatement pst = null;
    ResultSet rs = null;
    String   url = "jdbc:mysql://localhost:3307/hb_bc";
    String user = "root";
    String password = "dummy123";
 
        
          
           try{
                
            con = DriverManager.getConnection(url, user, password);
           }catch (Exception e){
               e.printStackTrace();
                con = DriverManager.getConnection("jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/tajplaza", user, password);
                
               
           }
            return con;
} 

  public static String getWebSql(String sno_in) throws Exception{
           BufferedReader br001 = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\Google Drive\\Dropbox\\program\\sql\\web_sql.txt")));
            String s;
            Connection con = RetrievePage.getConnection() ;
            String WEB_SQL = "";

            while ((s = br001.readLine()) != null) {
                if (s.split("\t").length < 3) {
                    continue;
                }
                String sno = s.split("\t")[0];
                String display = s.split("\t")[1];
                String sql = s.split("\t")[5];
               
                if (sno.equals(sno_in) ) {
                    WEB_SQL=sql;

                }
            }
            WEB_SQL=WEB_SQL.trim();
            WEB_SQL=StringUtils.removeStart(WEB_SQL, "\"");
            WEB_SQL=StringUtils.removeEnd(WEB_SQL, "\"");
        return WEB_SQL;    
       }


}
 class MyAjaxController extends com.gargoylesoftware.htmlunit.AjaxController{
    public boolean processSynchron(HtmlPage page, WebRequest request, boolean async) {
      // System.out.println("Request URL inside AJAX: "+request.getUrl());
       return super.processSynchron(page, request, async);
          
     }
    
    
 }
    
    