/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bombayplaza.pricematch;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.bcel.generic.ARRAYLENGTH;
import org.apache.commons.lang.StringUtils;
/**
 *
 * @author prabu
 */
public   class MyWebConnection extends HttpWebConnection{
       MyWebConnection(WebClient webClient) {
           super(webClient);
       }
       
       @Override
       public WebResponse	getResponse(WebRequest request) throws IOException {
          // System.out.println("[ RetrivePage ] Inside getResponse");
        long start_time=System.currentTimeMillis();
             WebResponse wr;
           String url=request.getUrl().toString();
         //System.out.println("Url now is "+url);
           if( url.indexOf("http://ad.doubleclick.net")>=0 ||
              url.indexOf("http://www.amazon.com/gp/compression/base/detail")>=0 ||
              url.indexOf("http://www.amazon.com/gp/gc/widget/product-gc")>=0     ||               
              url.indexOf("http://www.amazon.com/gp/history/external/full-rhf-rec-handler.html")>=0 ||
              url.indexOf("vaw.html")>=0 ||
              url.indexOf("cloundfront.net")>=0 ||
              url.indexOf("/image-block/")>=0 ||
                   url.indexOf("amazon-adsystem.com")>=0 ||
                   url.indexOf("s.amazon-adsystem.com")>=0 ||
              //StringUtils.endsWith(url,".jpg")
                   url.indexOf("jpg")>=0
                     ||url.indexOf(".png")>=0
                   || url.indexOf("aax-us-east-rtb.amazon-adsystem.com")>=0
                   || url.indexOf("blank")>=0
                    || url.indexOf("z-ecx.images-amazon.com")>=0
                   || url.indexOf("sharethis.com")>=0
                   || url.indexOf("aax-us-east.amazon-adsystem.com")>=0
                   
                   ){
               //url=StringUtils.substringBeforeLast(url, "/");
             //  url="http://z-ecx.images-amazon.com/images/G/01/browser-scripts/csm-base/csm-base-min-2892755276._V1_.js";
                 url="http://z-ecx.images-amazon.com/images/dumy.jpg";
               
               //return null;
           }
           
           //String fileName=StringUtils.substringAfterLast(url, "/");
           String fileName=url;
           if(fileName.length()>255)
               fileName=fileName.substring(0,254);
           fileName=fileName.replaceAll("\\\\","").replaceAll(":","_").replaceAll("/","_").replaceAll("\\?","").replaceAll("\\*", "").replaceAll("=","").replaceAll(";","").replaceAll(",","").replaceAll("&","").replaceAll("$","");
           if(StringUtils.substringAfterLast(fileName, "&")!=null &&StringUtils.substringAfterLast(fileName, "&").indexOf("_") ==0){
               fileName=StringUtils.substringBeforeLast(fileName, "&");
           }
            String localSerialFile="C:\\Temp2012\\cache\\"+fileName;
            
           if(  RetrievePage.USE_LOCAL_CACHE &&   url.indexOf("whatismyipaddress.com") <0  && url.indexOf("http://www.amazon.com/s/") <0  && url.indexOf("http://www.amazon.com/gp/offer-listing/") <0  &&   new File(localSerialFile).exists()){
              //  System.out.println("Reading from Local Cache: "+request.getUrl());
               ObjectInput input=null;
                try{
                     
                //use buffering
                InputStream file = new FileInputStream( localSerialFile );
                InputStream buffer = new BufferedInputStream( file );
                input = new ObjectInputStream ( buffer );
               
                  //deserialize the List
                wr = (WebResponse )input.readObject();
                  //display its data
                if(wr!=null)
                    return wr;
                
                }catch (Exception e){
                   // System.out.println("Error in reading from file in RetrievePage..not a big deal since anyway I would be hitting the web to get the page");
                    //e.printStackTrace();
                   //   wr= super.getResponse(request);
                }
                finally{
                 if(input!=null)
                     input.close();
                 long end_time=System.currentTimeMillis();
//                 System.out.println("Time taken to getch url:"+url +" is "+ (end_time-start_time)/1000 +" seconds");
                 
                }
 
               
           }
           //  System.out.println("Getting from Internet :"+url);
            wr= super.getResponse(request);
          
                //if(fileName.indexOf(".js")>=0 ||fileName.indexOf(".png")>=0||fileName.indexOf(".jpg")>=0)
            if(url.indexOf("http://www.amazon.com/dp/")<0&&  url.indexOf("http://www.amazon.com/s/")<0 && url.indexOf("http://www.amazon.com/dp/")<0 && url.indexOf("http://www.amazon.com/gp/offer-listing/")<0 && url.indexOf("parentAsin")<0   )
                {
                try{                
              //use buffering
                        OutputStream file = new FileOutputStream(localSerialFile );
                        OutputStream buffer = new BufferedOutputStream( file );
                        ObjectOutput output = new ObjectOutputStream( buffer );
                        try{
                          output.writeObject(wr);
                        }
                        finally{
                          output.close();
                        }
                      }  
                      catch(IOException ex){
                        System.out.println("Exception : Not able to write cache file:"+localSerialFile +" error messge:"+ex.getMessage());
                 }
                  
                }
  
                 long end_time=System.currentTimeMillis();
                 //System.out.println("Time taken to getch url:"+url +" is "+ (end_time-start_time)/1000 +" seconds");            
           return wr;
       }

}