/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bombayplaza.pricematch;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.gargoylesoftware.htmlunit.xml.XmlPage;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.InputSource;
import org.w3c.dom.Document;
import org.w3c.dom.*;
/**
 *
 * @author US083274
 */
public class VerifyFBASkuRank {
   
     static WebClient webClient1;
     static ArrayList<String> existinginventoryAll_Contents=new ArrayList();
     static ArrayList<String> existinginventoryFBA_Contents=new ArrayList();
     
     static ArrayList<String> myinventory_Contents=new ArrayList();
     static ArrayList<String> inventoryHealth_Contents=new ArrayList();
     static ArrayList<String> VariationEnableJS_Contents=new ArrayList();
     
     
     static HashMap<String,Integer> ItemOccurences=new HashMap();
     static ArrayList<String> ordersProcessed=new ArrayList();
     static  StringBuffer toomuchshipping=new StringBuffer();;
     static HashMap weightTable=new HashMap();
     
    public static void main(String args[]){
        
         MatchThePrice m=new MatchThePrice();
             m.JAVASCRIPTNONO=true;
         m.readAndFillFileContents();
          existinginventoryAll_Contents=MatchThePrice.existinginventoryAll_Contents;
          inventoryHealth_Contents=MatchThePrice.inventoryHealth_Contents;
          VariationEnableJS_Contents=MatchThePrice.VariationEnableJS_Contents;
          myinventory_Contents=MatchThePrice.myinventory_Contents;
        List<DomElement>  elems;
        try{
                     VerifyFBASkuRank v=new VerifyFBASkuRank();

             v.populateRanksForRestockFile_NA();
           // v.populateRanksForCreateFBAShipmentFile();
           
    }catch (Exception e){
        e.printStackTrace();
    }
  }

 public void populateRanksForRestockFile_NA() throws Exception{
     System.out.println("Inside populateRanksForRestockFile in VerifyFBASkuRank file");
     Integer numOfFbaSellers=-1;
         MatchThePrice m=new MatchThePrice();
             m.JAVASCRIPTNONO=true;
         m.readAndFillFileContents();
          existinginventoryAll_Contents=MatchThePrice.existinginventoryAll_Contents;
           existinginventoryFBA_Contents=MatchThePrice.existinginventoryFBA_Contents;

          inventoryHealth_Contents=MatchThePrice.inventoryHealth_Contents;
          VariationEnableJS_Contents=MatchThePrice.VariationEnableJS_Contents;
            myinventory_Contents=MatchThePrice.myinventory_Contents;
        ArrayList<String> runningFBAAnalzer_Contents=m.runningFBAAnalzer_Contents_AVOID;  
        
     
         BufferedReader br0 = new BufferedReader(new
                          InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\restock.txt")));

                    String strLine0;
                WebClient webClient1 ;

                webClient1 = new WebClient(BrowserVersion.FIREFOX_38 );
              if(RetrievePage.isOffice()){
              System.setProperty("java.net.preferIPv4Stack", "true");

                   webClient1 = new WebClient(BrowserVersion.FIREFOX_38,"proxydirect.tycoelectronics.com", 80);
                  DefaultCredentialsProvider credentialsProvider = (DefaultCredentialsProvider) webClient1.getCredentialsProvider();
                  credentialsProvider.addCredentials("us083274",  RetrievePage.PASSWORD1);   

              }              
                System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "error");
                webClient1.getOptions().setThrowExceptionOnScriptError(false);
                webClient1.getOptions().setJavaScriptEnabled(false);
                webClient1.getOptions().setCssEnabled(false);
                 //webClient1.waitForBackgroundJavaScript(15000);                     
                HtmlPage page0 = webClient1.getPage("https://sellercentral.amazon.com/");
                //System.out.println(page.asText());
                ((HtmlTextInput) page0.getElementById("username")).setText("packfba@gmail.com");
                ((HtmlPasswordInput) page0.getElementById("password")).setText("dummy123");
                 System.out.println("Signing In");
                page0=(HtmlPage) ((HtmlButton) page0.getElementById("sign-in-button")).click();

                   String asin="NA",reqAsins ,orderFrom=null;

                   int count=0;
                       m.JAVASCRIPTNONO=true;
                        FileWriter fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\restock_new.txt");
                        BufferedWriter out0 = new BufferedWriter(fstream_out0);
                       String s =br0.readLine();
                        out0.write(s);
                        out0.newLine();                       
                   while ((strLine0 = br0.readLine()) != null)   {
                        orderFrom=null;
                       if(count++>5000){
                           System.out.println("Limit exceeded for populating UPC..I continue tomorrow");
                           System.exit(0);
                       }
                       if(StringUtils.split(strLine0,"\t").length==0)
                           continue;
                       boolean getRank=true;
                     try{
                         numOfFbaSellers=-1;
                      if( false )
                      {
                          //DEBUG
                          if(strLine0.indexOf("B008TAWJ5C")<0     ){
                                  System.out.println("In debug mode so skipping ASIN");
                                  getRank=false;
                            }else{
                               System.out.println("In debug mode current ASIN is"+asin);
                          }
                      }                         
                             System.out.println("Current line while verifying SKU rank:"+strLine0);
                            String rank="9999999";         
                            String sku=strLine0.split("\t")[FbaInventoryAnalyzer_NA.SKU_COLUMN];
                            String soldby=strLine0.split("\t")[FbaInventoryAnalyzer_NA.SOLDBY_COLUMN];
                       asin=strLine0.split("\t")[FbaInventoryAnalyzer_NA.ASIN_COLUMN];
                       rank=strLine0.split("\t")[FbaInventoryAnalyzer_NA.RANK_COLUMN];
                      reqAsins=strLine0.split("\t")[FbaInventoryAnalyzer_NA.REQASINS_COLUMN];
                      String profit=strLine0.split("\t")[FbaInventoryAnalyzer_NA.PROFIT_COLUMN];
                      if(strLine0.split("\t").length>FbaInventoryAnalyzer_NA.ORDERFROM_COLUMN)
                          orderFrom=strLine0.split("\t")[FbaInventoryAnalyzer_NA.ORDERFROM_COLUMN];
                      
                      if(rank !=null  &&  rank.equals("null") ==false    ){
                          System.out.println("Item is good to restock:"+strLine0);
                          getRank=false;
                      }

                      if(rank !=null  &&  rank.equals("null") ==false  &&  Integer.parseInt(rank)> 60000 ){
                          if(VariationEnableJS_Contents.contains(asin)==false){
                                System.out.println("Item is NOT GOOD RANK AND NEIHER JOINED WITH ANY ITEM..SO No point in checking if it is joined with good rank:"+strLine0);
                                 getRank=false;
                              
                          }
                         
                           if(soldby!=null && soldby.equals("null")==false && soldby.equals("NA")==false){
                              System.out.println("Item is already sold by some other seller or me. But rank is still not good. I do not care to go for 1 pack.  Use whatver the rank I have on the multipack.");
                              getRank=false;
                          } 
                              
                      }
                      if(orderFrom!=null && orderFrom.toLowerCase().equals("no")){
                          System.out.println("OrderFrom has NO. So no point in getting rank for it :"+strLine0);
                           getRank=false;
                      }
                      if(asin.equals("NA"))
                         getRank=false;
                      if(reqAsins!=null && reqAsins.equals("null") ==false &&  Integer.valueOf(reqAsins) <5){
                          System.out.println("I dont need atleast 5 ASINS. So no point in getting ranks for them");
                          getRank=false;
                      }
                      if(profit!=null && profit.equals("null")==false &&   profit.equals("profit")==false && Float.valueOf(profit)<0){
                          System.out.println("No rpofit, so not gettign rank with line item :"+strLine0);
                          getRank=false;
                      }
                      
                      
                      //if(getRank && m.todayissunday  )
                      if(getRank  )
                      {
                          System.out.println("In VerifyFBASkuRank . Going to get current item price and rank since I could not get it from my_inventory");
                              System.out.println(strLine0);
                                m.JAVASCRIPTNONO=false;
                                
                                HashMap hm=m.getProductDetails(asin,true,false,false);
                                String notes="Not So Good Rank";
                                 String notes3=" ";
                              rank=(String)(hm.get("rank")!=null?hm.get("rank"):"null");
                              numOfFbaSellers=(Integer)(hm.get("NUMBEROFFBASELLERS")!=null?hm.get("NUMBEROFFBASELLERS"):1);
                              numOfFbaSellers=numOfFbaSellers+1;
                             float bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
                                soldby= (String)(hm.get("soldby")!=null?hm.get("soldby"):"NA") ;
                                 String pageasxml= (String)(hm.get("PAGEASXML")!=null?hm.get("PAGEASXML"):"NA") ;
                                 //Do we need all these restock file now that all coming from table.
                             float profitF=MatchThePrice.calculateProfitN_AVOID(asin,Float.valueOf(bestprice));
                             if(bestprice!=0) 
                                profit=String.valueOf(profitF);
                               
                              if(profitF!=0){
                                  System.out.println("For asin :"+asin +" the current profit is "+profitF +" and best price is "+bestprice);
                              }
                            if(soldby!=null && soldby.length()>0){
                                soldby=StringUtils.substringBefore(soldby,".");
                                soldby=StringUtils.substringBefore(soldby,"\r\n");
                                if(soldby.indexOf(" and fulfilled by amazon")>0)
                                {

                                    soldby=soldby.replaceAll(" and fulfilled by amazon", "");
                                }
                            }  
                            
                            
                                if(rank ==null || rank.equals("null")  ){
                                String pAsXml=pageasxml;
                                String varJSON=StringUtils.substringAfter(pAsXml, "'asin_variation_values',");
                                if(varJSON!=null){
                                    varJSON=StringUtils.substringBefore(varJSON, ");");
                                    if(varJSON!=null && varJSON.length()>0){
                                         System.out.println("VarJSON:"+varJSON);
                                        JSONObject js=JSONObject.fromObject(varJSON);
                                        System.out.println("Length of variaitons ASIN:"+js.size());
                                        for(int x=0;x<js.size();x++){
                                            Iterator i=js.keySet().iterator();
                                            String key;
                                            if(rank !=null  &&  rank.equals("null") ==false  &&  Integer.parseInt(rank)<60000){
                                                    System.out.println("Item is good to restock:"+strLine0);
                                                    break;
                                             }
                                            while(i.hasNext()){
                                                key=(String) i.next();
                                                if(key.equals(asin)){
                                                    System.out.println("Same asin is already tried ..Not going to try again:"+asin);
                                                    continue;
                                                }
                                                    m.JAVASCRIPTNONO=true;
                                                   hm=m.getProductDetails(key,true,false,false);
                                                    rank=(String)(hm.get("rank")!=null?hm.get("rank"):"9999999");
                                                    rank=String.valueOf(Integer.valueOf(rank)*2);
                                                try{
                                                       if(Integer.valueOf(rank)<60000){
                                                          notes="ASIN: "+key+"  that is joined has good rank:"+rank;
                                                          break;
                                                       }
                                                   }catch (Exception e){e.printStackTrace();}                                      

                                                System.out.println("Now check if this ASIN has good rank:"+key);                                
                                            }
                                        }                                
                                    }


                                }                          
                              }                        
          
                            
                      }
                      Integer itemsNeeded=0;
                      itemsNeeded=MatchThePrice.getReqUnits(  asin,  rank, numOfFbaSellers,m  );
         
                  if(profit==null || profit.equals("null")){
                        System.out.println("The profit is null even now..Lets try to get it from fba running analyzer");
                        profit="1";

                        for(int i=0;i<runningFBAAnalzer_Contents.size();i++){
                             String s1=runningFBAAnalzer_Contents.get(i);
                            if(s1==null || s1.split("\t").length<=8)
                                continue;                                    
                            if(s1.indexOf(asin)>=0){
                                   profit=s1.split("\t")[8];
                            }
                        } 
                  }
                  if((orderFrom==null || orderFrom.toUpperCase().equals("NO")==false )
                          
                          ){
                                System.out.println("The order from is null..take it from running analyzer/.");
                              
                                for(int i=0;i<runningFBAAnalzer_Contents.size();i++){
                                     String s1=runningFBAAnalzer_Contents.get(i);
                                    if(s1==null || s1.split("\t").length<=9)
                                        continue;                                    
                                    if(s1.indexOf(asin)>=0){
                                           orderFrom=s1.split("\t")[9];
                                    }
                                } 
                          
                      }
                if(rank==null || rank.equals("null")){
                    System.out.println("The rank is null even now..Just put profit as 100");
                    rank="20000";
                    /*ArrayList<String> runningFBAAnalzer_Contents=m.runningFBAAnalzer_Contents;
                    for(int i=0;i<runningFBAAnalzer_Contents.size();i++){
                        if(runningFBAAnalzer_Contents.get(i).indexOf(asin)>=0){

                        }
                    }*/
                }                    
                
                      String[] arr=strLine0.split("\t");
                      String newstr=arr[0];
                      int i=1;
                      for ( i=1;i<arr.length;i++){
                          if(i==FbaInventoryAnalyzer_NA.RANK_COLUMN){
                              arr[i]=rank;
                          }
                          if(i==FbaInventoryAnalyzer_NA.SOLDBY_COLUMN){
                              arr[i]=soldby;
                          }
                           if(i==FbaInventoryAnalyzer_NA.PROFIT_COLUMN){
                              arr[i]=profit;
                          }
                           if(i==FbaInventoryAnalyzer_NA.ORDERFROM_COLUMN){
                              arr[i]=orderFrom;
                          }                           
                          if(i==FbaInventoryAnalyzer_NA.NOTES_COLUMN  && itemsNeeded!=0){
                              arr[i]= arr[FbaInventoryAnalyzer_NA.NOTES_COLUMN] +" : Items Needed in New calculation (1200000/rank) / FBA sellers:"+itemsNeeded;
                          }                           
                          newstr=newstr+"\t"+arr[i];
                      }
                      if(i==FbaInventoryAnalyzer_NA.ORDERFROM_COLUMN){
                               newstr=newstr+"\t"+orderFrom;
                          }                           

                       
                        
                        
                        out0.write(newstr);
                        out0.newLine();
                        
                        }catch (Exception e){
                         e.printStackTrace();
                     }  
    
                   }
                out0.close();  
           
           FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\fba\\restock_new.txt"),new File("C:\\Google Drive\\Dropbox\\fba\\restock.txt"));
           new File("C:\\Google Drive\\Dropbox\\fba\\restock_new.txt").delete();;
           
          
    }            

 public void populateRanksForCreateFBAShipmentFile() throws Exception{
         MatchThePrice m=new MatchThePrice();
             m.JAVASCRIPTNONO=true;
         m.readAndFillFileContents();
          existinginventoryAll_Contents=MatchThePrice.existinginventoryAll_Contents;
          inventoryHealth_Contents=MatchThePrice.inventoryHealth_Contents;
          VariationEnableJS_Contents=MatchThePrice.VariationEnableJS_Contents;
     
         BufferedReader br0 = new BufferedReader(new
                          InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\createfbashipment.txt")));

                    String strLine0;
                WebClient webClient1 ;

                webClient1 = new WebClient(BrowserVersion.FIREFOX_38 );
              if(RetrievePage.isOffice()){
              System.setProperty("java.net.preferIPv4Stack", "true");

                   webClient1 = new WebClient(BrowserVersion.FIREFOX_38,"proxydirect.tycoelectronics.com", 80);
                  DefaultCredentialsProvider credentialsProvider = (DefaultCredentialsProvider) webClient1.getCredentialsProvider();
                  credentialsProvider.addCredentials("us083274",  RetrievePage.PASSWORD1);   

              }              
                System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "error");
                webClient1.getOptions().setThrowExceptionOnScriptError(false);
                webClient1.getOptions().setJavaScriptEnabled(false);
                webClient1.getOptions().setCssEnabled(false);
                 //webClient1.waitForBackgroundJavaScript(15000);                     
                HtmlPage page0 = webClient1.getPage("https://sellercentral.amazon.com/");
                //System.out.println(page.asText());
                ((HtmlTextInput) page0.getElementById("ap_email")).setText("packfba@gmail.com");
                ((HtmlPasswordInput) page0.getElementById("ap_password")).setText("dummy123");
                 System.out.println("Signing In");
                page0=(HtmlPage) ((HtmlImageInput) page0.getElementById("signInSubmit")).click();
                Float totalWeight=(float)0;
                   String asin="NA",sku;
                   String url="https://catalog.amazon.com/abis/product/DisplayEditProduct?sku=SKUIN&asin=ASININ";
                   int count=0;
                   m.JAVASCRIPTNONO=true;
                        FileWriter fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\createfbashipment_new.txt");
                        BufferedWriter out0 = new BufferedWriter(fstream_out0);
                        
                   while ((strLine0 = br0.readLine()) != null)   {
                      /* if(strLine0.indexOf("B004XTDA3I")<0){
                           continue;
                       }*/ 
                       if(count++>5000){
                           System.out.println("Limit exceeded for populating UPC..I continue tomorrow");
                           System.exit(0);
                       }
                       if(StringUtils.split(strLine0,"\t").length==0)
                           continue;
                       sku=StringUtils.split(strLine0,"\t")[0];
                       

                       if(sku==null || sku.trim().length()==0 || sku.indexOf("MerchantSKU")>=0){
                            out0.write(strLine0);
                            out0.newLine();
                             continue;
                       }

                       sku=sku.trim();
                       for(int j=0;j<existinginventoryAll_Contents.size();j++) {
                           String s=existinginventoryAll_Contents.get(j);
                           if(s.indexOf(sku)>=0){
                               asin=s.split("\t")[16];
                               break;
                           }
                       }                       
                       if(asin.equals("NA") ){
                           if(sku.indexOf("_FBA")>=0){
                               asin=getAsinFromInventory(sku,webClient1);
                           }else{
                               System.out.println("Not a valid sku..Skipping");
                                out0.write(strLine0);
                                out0.newLine();
                                continue;
                           }
                            
                       }
                        if(asin.equals("NA") ){
                            System.out.println("Not a valid sku.TRACE 2.Skipping");
                                out0.write(strLine0);
                                out0.newLine();
                                continue;
                            
                        }
                       System.out.println(strLine0);
                       String tmpUrl="http://www.amazon.com/gp/product/"+asin;
                        HtmlPage page1=webClient1.getPage(tmpUrl);
                        String notes="Not So Good Rank";
                         String notes3="  ";
                          String rank="9999999";
                     if(page1.getByXPath("//li[@id='SalesRank']").size()>0){
                         String text=((DomElement)(page1.getByXPath("//li[@id='SalesRank']").get(0))).asText();
                          rank=StringUtils.substringBetween(text,"#", " ").replaceAll(",", "");
                         String category =StringUtils.substringAfter(text," in ");
                         category=StringUtils.substringBefore(text, " (");
                         try{
                             if(Integer.valueOf(rank)<60000){
                                 notes="ASIN: "+asin+" has good rank:"+rank;
                                 
                             }

                             
                         }catch (Exception e){e.printStackTrace();}
                         
                     }                        
                     if(notes.equals("Not So Good Rank")){
                        String pAsXml=page1.asXml();
                        String varJSON=StringUtils.substringAfter(pAsXml, "'asin_variation_values',");
                        if(varJSON!=null){
                            varJSON=StringUtils.substringBefore(varJSON, ");");
                            if(varJSON!=null && varJSON.length()>0){
                                 System.out.println("VarJSON:"+varJSON);
                                JSONObject js=JSONObject.fromObject(varJSON);
                                System.out.println("Length of variaitons ASIN:"+js.size());
                                for(int x=0;x<js.size();x++){
                                    Iterator i=js.keySet().iterator();
                                    String key;
                                    while(i.hasNext()){
                                        key=(String) i.next();
                                        m.JAVASCRIPTNONO=true;
                                         HashMap hm=m.getProductDetails(key,true,false,false);
                                            rank=(String)(hm.get("rank")!=null?hm.get("rank"):"9999999");
                                        try{
                                               if(Integer.valueOf(rank)<60000){
                                                  notes="ASIN: "+key+"  that is joined has good rank:"+rank;
                                                  break;
                                               }
                                           }catch (Exception e){e.printStackTrace();}                                      

                                        System.out.println("Now check if this ASIN has good rank:"+key);                                
                                    }
                                }                                
                            }
                            

                        }
                        
                       
                     }
                        
                        
                        String notes2=" \t " ;
                        for(int i=0;i<inventoryHealth_Contents.size();i++) {
                            String s=inventoryHealth_Contents.get(i);
                            if(s.indexOf(sku)>=0){
                                System.out.println("Found sku in inventory-health");
                                String remaining=s.split("\t")[9];
                                String weekscover=s.split("\t")[23];
                                if(weekscover.indexOf(".")==0)
                                    weekscover="0"+weekscover;
                                notes2="Remaing QTY in FBA:"+remaining+"\t Weeks that will come for:"+weekscover;
                            }
                        }
                       
                        HashMap hmF=RetrievePage.getDetailsFromFbaCalculatorNewMWS(null,asin,null,true);
                        String  weight=String.valueOf((Float)hmF.get("productInfoWeight"));
                        String length=String.valueOf(hmF.get("productInfoLength"));
                        String width=String.valueOf(hmF.get("productInfoWidth"));
                       String  height=String.valueOf((Float)hmF.get("productInfoHeight") );
                       
                       String quantity=strLine0.split("\t")[1].trim();
                       totalWeight+=(Float.valueOf(weight)*Float.valueOf(quantity));
                       
                       notes3="wieght:"+weight +" pounds";
                

                        strLine0=strLine0+"\t"+notes+"\t"+notes2+"\t"+notes3;
                        out0.write(strLine0);
                        out0.newLine();
    
                   }
                   out0.write("\t\tTOTAL WEIGHT:"+totalWeight);
                out0.close();  
          
    }            

    private String getAsinFromInventory(String sku,WebClient webClient1) throws  Exception {
        String asin="NA";
        String skuSearchUrl="https://sellercentral.amazon.com/myi/search/ProductSummary?keyword=KEYWORD";
         skuSearchUrl=skuSearchUrl.replaceAll("KEYWORD", sku);
         HtmlPage page1=webClient1.getPage(skuSearchUrl);
         String xpath="//a[@target='_blank']";
          if(page1.getByXPath(xpath).size()>0){
             asin=((DomElement) (page1.getByXPath(xpath).get(0))).asText();
             asin=asin.trim();
          }
         return asin;
        
    }
}

