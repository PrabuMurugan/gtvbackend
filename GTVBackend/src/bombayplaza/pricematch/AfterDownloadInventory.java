/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package bombayplaza.pricematch;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.gargoylesoftware.htmlunit.xml.XmlPage;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
/**
 *
 * @author US083274 test
 */
public class AfterDownloadInventory {
    public static boolean todayissunday=false;
    public static StringBuffer fbaFeesNotKnown=new StringBuffer();
    public static StringBuffer originalNotKnown=new StringBuffer();
    public static StringBuffer itemsInLoss1=new StringBuffer();
    public static StringBuffer newItemsInLoss1=new StringBuffer();
    public static TreeMap<Float,String> itemsInLossMap=new TreeMap<Float,String> ();
    private static Date date;
    private static String today;
    private static ArrayList<String> myinventory_Contents;
    private static ArrayList<String> existinginventoryFBA_Contents;
    public static void main(String args[]){
         // createInventoryAdjustmentsReport();
        // reducePricesForMoreThan3Months();
       
        // createCostAnalyzer();
       
      
        if(true)
            return;
        Date date = Calendar.getInstance().getTime();
        DateFormat formatter = new SimpleDateFormat("E");
        String today = formatter.format(date);
        InputStream is;
        OutputStream out;
        int read;
            byte[] bytes;        
        if(today.indexOf("Thur")>=0){
            todayissunday=true;
            System.out.println("Clearing ImOnlySeller file since it is sunday today");
        }       
        AfterDownloadInventory d=new AfterDownloadInventory();
       d.sortActiveListingByDate();;
        List<DomElement>  elems;
        try{
            String process_type="DOWNLOAD_REPORT";
            String report_type="INVENTORY";
            if(args.length>0){
                process_type=args[0];
            }
            if(args.length>1){
                report_type=args[1];
            }

                WebClient webClient1 ;
               webClient1=new WebClient(BrowserVersion.FIREFOX_38 );
                if(RetrievePage.isOffice()){
                     System.setProperty("java.net.preferIPv4Stack", "true");
                    webClient1 = new WebClient(BrowserVersion.getDefault(),"proxydirect.tycoelectronics.com", 80);
                  //  DefaultCredentialsProvider credentialsProvider = (DefaultCredentialsProvider) webClient1.getCredentialsProvider();
                 // credentialsProvider.addCredentials("us083274",  RetrievePage.PASSWORD1);   
                  
                }    
            
                System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "error");
                webClient1.getOptions().setThrowExceptionOnScriptError(false);
                webClient1.getOptions().setJavaScriptEnabled(true);
                webClient1.getOptions().setCssEnabled(false);
                 webClient1.waitForBackgroundJavaScript(15000);

                HtmlPage page = webClient1.getPage("https://sellercentral.amazon.com/gp/homepage.html");
                //System.out.println(page.asText());
                if(page.getElementById("username")!=null)
                    ((HtmlTextInput) page.getElementById("username")).setText("packfba@gmail.com");
                else
                    ((HtmlTextInput) page.getElementById("ap_email")).setValueAttribute("packfba@gmail.com");
                if(page.getElementById("password")!=null)
                    ((HtmlPasswordInput) page.getElementById("password")).setText("dummy123");
                else
                    ((HtmlPasswordInput) page.getElementById("ap_password")).setValueAttribute("dummy123");
                 System.out.println("Signing In");
                 if(page.getElementById("sign-in-button")!=null)
                    page=(HtmlPage) ((HtmlButton) page.getElementById("sign-in-button")).click();
                 else
                     page=(HtmlPage) ((HtmlImageInput) page.getElementById("signInSubmit")).click();
                HtmlSelect select;
                HtmlOption option ;
                     //make sure we set amazon.com in top marketplace name
                                        //SElect 15 days of report.
                elems=  (List<DomElement>) page.getByXPath("//select[@id='marketplaceSelect']");
                  select = (HtmlSelect) elems.get(0);
                  option = select.getOptionByText("www.amazon.com");
               page= select.setSelectedAttribute(option, true);
                date = Calendar.getInstance().getTime();
              String month = new SimpleDateFormat("MM").format(date);
              String day = new SimpleDateFormat("dd").format(date);              
              String year = new SimpleDateFormat("yyyy").format(date);
              String lastyear=String.valueOf(Integer.valueOf(year)-1);                     
                
                 if(process_type.equals("REQUEST_REPORT")){
                     
                     if(report_type.equals("INVENTORY")){
                      
                     if(true || todayissunday){
                         //Only on sunday request inventory-health report and received-inventory report
                            System.out.println("Clicking On Received Inventory report");
                            //XmlPage xPage=  webClient1.getPage("https://sellercentral.amazon.com/gp/ssof/reports/request-download.html/ref=ag_xx_cont_fbafulrpts?recordType=INVENTORY_RECEIPT&eventDateTypeFilterOption=orderDate&eventDateOption=0&fromDate=05%2F01%2F2013&toDate=08%2F21%2F2013&startDate=1%2F1%2F12&endDate=12%2F31%2F12&eventMonthOption=1&fromMonth=1&fromYear=2011&toMonth=12&toYear=2013&startMonth=&startYear=&endMonth=&endYear=&_=1345562043674");
                            XmlPage xPage=  webClient1.getPage("https://sellercentral.amazon.com/gp/ssof/reports/request-download.html/ref=ag_xx_cont_fbafulrpts?recordType=INVENTORY_RECEIPT&eventDateTypeFilterOption=orderDate&eventDateOption=0&fromDate=01%2F01%2F2013&toDate=12%2F31%2F2013&startDate=1%2F1%2F13&endDate=12%2F31%2F13&eventMonthOption=1&fromMonth=1&fromYear=2012&toMonth=1&toYear=2013&startMonth=&startYear=&endMonth=&endYear=&_=1357835899395");
                            System.out.println("Result from requesting inventory report:"+xPage.asXml());

                            xPage=  webClient1.getPage("https://sellercentral.amazon.com/gp/ssof/reports/request-download.html/ref=ag_xx_cont_fbafulrpts?recordType=INVENTORY_HEALTH&eventDateTypeFilterOption=orderDate&eventDateOption=1&fromDate=mm%2Fdd%2Fyyyy&toDate=mm%2Fdd%2Fyyyy&startDate=&endDate=&eventMonthOption=1&fromMonth=1&fromYear=2013&toMonth=12&toYear=2013&startMonth=&startYear=&endMonth=&endYear=&_=1345562644476");
                            System.out.println("Result from requesting inventory report:"+xPage.asXml());

                            
                     }            
                     try{
                            System.out.println("Changing to post for active listign report");
                           WebRequest requestSettings = new WebRequest(
                             new URL("https://sellercentral.amazon.com/gp/upload-download-utils/requestReport.html"), HttpMethod.POST);

                           requestSettings.setRequestParameters(new ArrayList());
                           requestSettings.getRequestParameters().add(new NameValuePair("marketplaceID", "1"));
                           requestSettings.getRequestParameters().add(new NameValuePair("type","MerchantListingReport"));

                           // Finally, we can get the page
                             page = webClient1.getPage(requestSettings);
                         
                     }catch (Exception e2){
                         e2.printStackTrace();
                     }
                     


                            /*page=  webClient1.getPage("https://sellercentral.amazon.com/gp/item-manager/ezdpc/openPickup.html/ref=ag_invreport_dnav_home_");
                             elems =  (List<DomElement>) page.getByXPath("//select[@name='type']");
                             select = (HtmlSelect) elems.get(0);
                             option = select.getOptionByText("Active Listings Report");
                            select.setSelectedAttribute(option, true);

                             elems=  (List<DomElement>) page.getByXPath("//button[@id='Request ReportID']");
                            if(elems.size()>0){
                                System.out.println("Clicking on request report for Active Listings");

                                page=elems.get(0).click();
                            }*/
                            if(todayissunday) {
                            System.out.println("Going to download fba fees report");
                            page=  webClient1.getPage("https://sellercentral.amazon.com/gp/ssof/reports/search.html/ref=ag_fbafulrpts_cont_fbareports?ie=UTF8&recordType=ESTIMATED_FBA_FEES");
 

                             elems=  (List<DomElement>) page.getByXPath("//button[@name='Request Download']");
                            if(elems.size()>0){
                                System.out.println("Clicking on request   fba fees report");

                                page=elems.get(0).click();
                            }
                            
                            System.out.println("Going to inventory adjustments report");
                            XmlPage xmlpage=  webClient1.getPage("https://sellercentral.amazon.com/gp/ssof/reports/request-download.html/ref=ag_xx_cont_fbafulrpts?recordType=INVENTORY_ADJUSTMENT&eventDateTypeFilterOption=orderDate&eventDateOption=0&fromDate="+month+"/"+day+"/"+lastyear+"&toDate="+month+"/"+day+"/"+year+"&startDate="+month+"/"+day+"/"+lastyear+"&endDate="+month+"/"+day+"/"+year+"&eventMonthOption=1&fromMonth="+month+"&fromYear="+lastyear+"&toMonth="+month+"&toYear="+year+"&startMonth=&startYear=&endMonth=&endYear=&_=1367714570755");
 
                            System.out.println("Inventory adjustment reports XML output during request:"+xmlpage.asXml());
                            }
                             System.out.println("Downloading FBA report");


                            XmlPage  xmlpage=webClient1.getPage("https://sellercentral.amazon.com/gp/ssof/reports/request-download.html/ref=ag_xx_cont_fbafulrpts?recordType=FBA_MYI_UNSUPPRESSED_INVENTORY&eventDateTypeFilterOption=orderDate&eventDateOption=1&fromDate=mm%2Fdd%2Fyyyy&toDate=mm%2Fdd%2Fyyyy&startDate=&endDate=&eventMonthOption=1&fromMonth="+month+"&fromYear="+lastyear+"&toMonth="+month+"&toYear="+year+"&startMonth=&startYear=&endMonth=&endYear=&_=1335885002285");
                            System.out.println("XML page:"+xmlpage.asText());

                     }else if (report_type.equals("ORDERS")){
                           System.out.println("Clicking On Request Order Report");


                            page=  webClient1.getPage("https://sellercentral.amazon.com/gp/transactions/orderPickup.html");

                            //SElect 15 days of report.
                            elems=  (List<DomElement>) page.getByXPath("//select[@name='days']");
                              select = (HtmlSelect) elems.get(0);
                              option = select.getOptionByText("120");
                            select.setSelectedAttribute(option, true);

                             elems=  (List<DomElement>) page.getByXPath("//button[@id='Request ReportID']");
                            if(elems.size()>0){
                                System.out.println("Clicking on request report 3 : Order Report");

                                page=elems.get(0).click();
                            }

 


                     }//end of orders report
                     else if (report_type.equals("UNSHIPPED_ORDERS")){
                           System.out.println("Clicking On Unshipped orders report");
                           page=  webClient1.getPage("https://sellercentral.amazon.com/gp/transactions/actionableOrderPickup.html");
                             elems=  (List<DomElement>) page.getByXPath("//button[@id='Request ReportID']");
                            if(elems.size()>0){
                                System.out.println("Clicking on request report 4 : Order Unshipped Report");

                                page=elems.get(0).click();
                            }
                            try{
                                System.out.println("Now waiting for 2 minutes to download the unshipped orders");
                                Thread.sleep(120*1000);;
                            }catch (Exception e){e.printStackTrace();}

                            System.out.println(" Now going to process Unshipped Report");

                            page=  webClient1.getPage("https://sellercentral.amazon.com/gp/transactions/actionableOrderPickup.html");
                            webClient1.waitForBackgroundJavaScript(15000);
                            elems=  (List<DomElement>) page.getByXPath("//div[@id='reportStatusDataDiv']//tr");
                            if(elems.size()>1){
                                System.out.println("THERE ARE MULTIPLE TRs..trace 1");
                                if(elems.get(1).getTextContent().indexOf("Not Ready")>=0){
                                    System.err.println("Even after 30 minutes report is not ready..Ending");


                                }else{
                                        elems=  (List<DomElement>) page.getByXPath("//a[@class='buttonImage']");
                                        if(elems.size()>0){
                                            System.out.println("Clicking on download button");

                                            is =elems.get(0).click().getWebResponse().getContentAsStream();
                                            out = new FileOutputStream(new File("C:\\Google Drive\\Dropbox\\order-unshipped.txt"));
                                            read=0;
                                            bytes = new byte[1024];

                                            while((read = is.read(bytes))!= -1){
                                                out.write(bytes, 0, read);
                                            }

                                            is.close();
                                            out.flush();
                                            out.close();

                                            System.out.println("New file created! --trace 1. Completed Inventory report");
                                        }                     
                                }

                                EmailUnShippedOrders e=new EmailUnShippedOrders();
                                e.emailUnshippedOrderReport();
                            } //There are multiple TRs and CLICKED ON THE TR.




                     }//end of orders report                     
                     


                 }//end of request_report
                 else  {
                    //DOWNLOAD THE TWO REPORTS

                    if(report_type.equals("INVENTORY")){
                    // if(true||todayissunday){
                        if(true){
                         //Only on sunday download inventory-health report and received-inventory report
                         System.out.println("Only on sunday download  received-inventory report");

                        XmlPage xPage=  webClient1.getPage("https://sellercentral.amazon.com/gp/ssof/reports/download-history.html/ref=ag_xx_cont_fbafulrpts?reports=INVENTORY_RECEIPT&_=1345569823608");
                        String t=xPage.asText();
                        t=StringUtils.substringAfter(t, "href");
                        t=StringUtils.substringBetween(t, "\"","\"");
                        if(t.length()>0){

                                System.out.println("Clicking on download button");
                                String href=t;
                                 FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\fba\\received-inventory_2012.txt"), new File("C:\\Google Drive\\Dropbox\\fba\\received-inventory.txt"));
                                
                                is =webClient1.getPage(href).getWebResponse().getContentAsStream();
                                out = new FileOutputStream(new File("C:\\Google Drive\\Dropbox\\fba\\received-inventory.txt"),true);
                                read=0;
                                bytes = new byte[1024];

                                while((read = is.read(bytes))!= -1){
                                out.write(bytes, 0, read);
                                }

                                is.close();
                                out.flush();
                                out.close();

                                System.out.println("New file created!");



                            }
                            
                         System.out.println("Only on sunday download inventory-health report  ");
                          xPage=  webClient1.getPage("https://sellercentral.amazon.com/gp/ssof/reports/download-history.html/ref=ag_xx_cont_fbafulrpts?reports=INVENTORY_HEALTH&_=1345570605388");
                          t=xPage.asText();
                        t=StringUtils.substringAfter(t, "href");
                        t=StringUtils.substringBetween(t, "\"","\"");
                        if(t.length()>0){

                                System.out.println("Clicking on download button");
                                String href=t;
                                is =webClient1.getPage(href).getWebResponse().getContentAsStream();
                                out = new FileOutputStream(new File("C:\\Google Drive\\Dropbox\\fba\\inventory-health.txt"));
                                read=0;
                                bytes = new byte[1024];

                                while((read = is.read(bytes))!= -1){
                                out.write(bytes, 0, read);
                                }

                                is.close();
                                out.flush();
                                out.close();

                                System.out.println("New file created!");



                           }                         
                  }//end of today is sunday
                   
                     if(todayissunday){
                      System.out.println("Only on sunday download   fbs fees report");

                        XmlPage xPage=  webClient1.getPage("https://sellercentral.amazon.com/gp/ssof/reports/download-history.html/ref=ag_xx_cont_fbafulrpts?reports=ESTIMATED_FBA_FEES&_=1361993974400");
                        String t=xPage.asText();
                        t=StringUtils.substringAfter(t, "href");
                        t=StringUtils.substringBetween(t, "\"","\"");
                        if(t.length()>0){

                                System.out.println("Clicking on download button");
                                String href=t;
                               
                                is =webClient1.getPage(href).getWebResponse().getContentAsStream();
                                out = new FileOutputStream(new File("C:\\Google Drive\\Dropbox\\fba\\fba-fees.txt"),true);
                                read=0;
                                bytes = new byte[1024];

                                while((read = is.read(bytes))!= -1){
                                out.write(bytes, 0, read);
                                }

                                is.close();
                                out.flush();
                                out.close();

                                System.out.println("New file created!");



                            }  
                        System.out.println("Going to download inventory adjustments report");
                         xPage=  webClient1.getPage("https://sellercentral.amazon.com/gp/ssof/reports/download-history.html/ref=ag_xx_cont_fbafulrpts?reports=INVENTORY_ADJUSTMENT&_=1367715936553");
                         t=xPage.asText();
                        t=StringUtils.substringAfter(t, "href");
                        t=StringUtils.substringBetween(t, "\"","\"");
                        if(t.length()>0){

                                System.out.println("Clicking on download button");
                                String href=t;
                               
                                is =webClient1.getPage(href).getWebResponse().getContentAsStream();
                                out = new FileOutputStream(new File("C:\\Google Drive\\Dropbox\\fba\\inventory-adjustments.txt"),true);
                                read=0;
                                bytes = new byte[1024];

                                while((read = is.read(bytes))!= -1){
                                out.write(bytes, 0, read);
                                }

                                is.close();
                                out.flush();
                                out.close();

                                System.out.println("New file created!");

                                createInventoryAdjustmentsReport();

                            }                   
                        
                     }

                     page=  webClient1.getPage("https://sellercentral.amazon.com/gp/item-manager/ezdpc/openPickup.html");
                    webClient1.waitForBackgroundJavaScript(15000);
                     elems=  (List<DomElement>) page.getByXPath("//div[@id='reportStatusDataDiv']//tr");
                     boolean download_inventory_report=true;

                     if(elems.size()>1){

                         System.out.println("THERE ARE MULTIPLE TRs..trace 1");
                         if(elems.get(1).getTextContent().indexOf("Not Ready")>=0){
                             System.err.println("Even after 30 minutes active listings report is not ready..");
                            // System.exit(1);
                             download_inventory_report=false;

                         }
                         if(elems.get(1).getTextContent().indexOf("Active Listings Report")<0){
                             System.err.println("First downloade is not Active Listings Report.");
                             //System.exit(1);
                             download_inventory_report=false;
                         }
                         if(download_inventory_report){

                             elems=  (List<DomElement>) page.getByXPath("//a[@class='buttonImage']");
                             if(elems.size()>0){
                                 System.out.println("Clicking on download button");

                                   is =elems.get(0).click().getWebResponse().getContentAsStream();
                                   out = new FileOutputStream(new File("C:\\Google Drive\\Dropbox\\existing-all-inventory.txt"));
                                  read=0;
                                  bytes = new byte[1024];

                                while((read = is.read(bytes))!= -1){
                                    out.write(bytes, 0, read);
                                }

                                is.close();
                                out.flush();
                                out.close();
                                try{
                                        FileWriter writer = new FileWriter("C:\\Google Drive\\Dropbox\\SkyDrive\\Swami\\existing-all-inventory.txt");
                                                                     
                                    String strLine0;
                                     BufferedReader   br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\existing-all-inventory.txt")));
                                     while ((strLine0 = br0.readLine()) != null)   {
                                         if(strLine0.trim().length()>0 && strLine0.indexOf("AMAZON_NA")<0){
                                                writer.write(strLine0);
                                               writer.write('\n');                                                      
                                            
                                         }
   

                                     } 
                                     writer.close();

                                   // FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\existing-all-inventory.txt"), new File("C:\\Google Drive\\Dropbox\\SkyDrive\\Swami\\existing-all-inventory.txt"));
                                }catch (Exception e){e.printStackTrace();
                                    
                                }
                                System.out.println("New file created! --trace 1. Completed Inventory report");
                             }
                         }

                     } //There are multiple TRs and CLICKED ON THE TR.
                       d.sortActiveListingByDate();
                     System.out.println("Going to download FBA report");

                            page=  webClient1.getPage("https://sellercentral.amazon.com/gp/ssof/reports/search.html/ref=ag_fbareports_cont_fbalist?ie=UTF8&recordType=FBA_MYI_UNSUPPRESSED_INVENTORY");
                            elems=  (List<DomElement>) page.getByXPath("//a[@id='tab_download']");
                            if(elems.size()>0){
                                System.out.println("Clicking on download report 2 FBA report");
                                elems.get(0).click();
                            }

                            webClient1.waitForBackgroundJavaScript(15000);
                            elems=  (List<DomElement>) page.getByXPath("//div[@id='downloadArchive']//tr");
                            if(elems.size()>1){
                                System.out.println("Multiple TRs exist for FBA report..Good");
                                if(elems.get(1).getTextContent().indexOf("Not Ready")>=0){
                                    System.err.println("Even after 30 minutes report is not ready..NOT Ending");
                                    //System.exit(1);

                                }

                                elems=  (List<DomElement>) page.getByXPath("//a[@class='buttonImage']");
                                if(elems.size()>0){
                                    System.out.println("Clicking on download button");

                                    is =elems.get(0).click().getWebResponse().getContentAsStream();
                                    out = new FileOutputStream(new File("C:\\Google Drive\\Dropbox\\existing-inventory-FBA.txt"));
                                    read=0;
                                    bytes = new byte[1024];

                                    while((read = is.read(bytes))!= -1){
                                    out.write(bytes, 0, read);
                                    }

                                    is.close();
                                    out.flush();
                                    out.close();

                                    System.out.println("New file created!");
                                    
                                    createCostAnalyzer();
                                    reducePricesForMoreThan3Months();


                        }

                     }//end of elems >1
                     
                    }//end of report_type inventory
                    else if (report_type.equals("ORDERS")){

                    System.out.println("Done with FBA report download.. Now going to process Order Report");

                     page=  webClient1.getPage("https://sellercentral.amazon.com/gp/transactions/orderPickup.html");
                    webClient1.waitForBackgroundJavaScript(15000);
                      elems=  (List<DomElement>) page.getByXPath("//div[@id='reportStatusDataDiv']//tr");
                     if(elems.size()>1){
                         System.out.println("THERE ARE MULTIPLE TRs..trace 1");
                         if(elems.get(1).getTextContent().indexOf("Not Ready")>=0){
                             System.err.println("Even after 30 minutes report is not ready..Ending");
                             

                         }else{
                                elems=  (List<DomElement>) page.getByXPath("//a[@class='buttonImage']");
                                if(elems.size()>0){
                                    System.out.println("Clicking on download button");

                                    is =elems.get(0).click().getWebResponse().getContentAsStream();
                                    out = new FileOutputStream(new File("C:\\Google Drive\\Dropbox\\order-report.txt"));
                                    read=0;
                                    bytes = new byte[1024];

                                    while((read = is.read(bytes))!= -1){
                                        out.write(bytes, 0, read);
                                    }

                                    is.close();
                                    out.flush();
                                    out.close();

                                    System.out.println("New file created! --trace 1. Completed Inventory report");
                                }                             
                         }

                     } //There are multiple TRs and CLICKED ON THE TR.
 

                    }


           
         }
        
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private static void  createInventoryAdjustmentsReport(){
          String str=null;
        try{
            System.out.println("Inside  createInventoryAdjustmentsReport");
        FileInputStream fstream = new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\analyze-inventory-adjustments.txt");
          // Get the object of DataInputStream
          DataInputStream in = new DataInputStream(fstream);
          BufferedReader br = new BufferedReader(new InputStreamReader(in));
        
          HashMap<String,String> existingInventoryAdjustments=new HashMap( );
          
          HashMap<String,Integer> newRunningInventoryAdjustments=new HashMap( );
          HashMap<String,Integer> newRunningInventoryAdjustmentsWithAdjustmenets=new HashMap( );
          
          br.readLine();
           while ((str = br.readLine()) != null)   {
            //  if(str.indexOf("MK2042013009_FBA")>=0)
             //      System.out.println("debug");               
               str=str.replaceAll("\"","");
               if(str==null || str.trim().length()==0 || str.split("\t").length<2)
                   continue;
               if(existingInventoryAdjustments.containsKey(str.split("\t")[0]+"-"+str.split("\t")[1]) && existingInventoryAdjustments.get(str.split("\t")[0]+"-"+str.split("\t")[1]).length()>str.length())
                continue;
               if(existingInventoryAdjustments.containsKey(str.split("\t")[0]+"-"+str.split("\t")[1]) && existingInventoryAdjustments.get(str.split("\t")[0]+"-"+str.split("\t")[1]).length()>str.length())
                existingInventoryAdjustments.remove(str.split("\t")[0]+"-"+str.split("\t")[1]);
                
               existingInventoryAdjustments.put(str.split("\t")[0]+"-"+str.split("\t")[1],str);
               
               
      
           }
           in.close();
           br.close();
            FileWriter fstream_out1 = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\analyze-inventory-adjustments.txt");
            BufferedWriter out1 = new BufferedWriter(fstream_out1);
            out1.write("sku-item name\t total adjustment quantity from inventory adjustments report\treimbursed quantity\tnot reimbursed quantity\tcomments");
            out1.newLine();
            out1.close();;
           
            System.out.println("Inside  createCostAnalyzer");
         fstream = new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\inventory-adjustments.txt");
          // Get the object of DataInputStream
           in = new DataInputStream(fstream);
           br = new BufferedReader(new InputStreamReader(in));
          
         
          String sku,item_name;
           Integer adjst_qty;
          br.readLine();
            date = Calendar.getInstance().getTime();
          Integer month = Integer.valueOf(new SimpleDateFormat("MM").format(date));
          Integer lastmonth =month-1;
          String monthS=String.format("%02d", month);
          String lastmonthS=String.format("%02d", lastmonth);
          String day = new SimpleDateFormat("dd").format(date);              
          String year = new SimpleDateFormat("yyyy").format(date);

          
           while ((str = br.readLine()) != null)   {
               if( str.indexOf(year+"-"+monthS)>=0 ||  str.indexOf(year+"-"+lastmonthS)>=0)
                   continue;
               sku=str.split("\t")[3];
               str=str.replaceAll("\"","");
              // if(sku.indexOf("MK2042013009_FBA")>=0)
              //     System.out.println("debug");
               item_name=str.split("\t")[4];
            adjst_qty= Integer.valueOf(str.split("\t")[6]);
               
               if(newRunningInventoryAdjustments.containsKey(sku+" : "+item_name)){
                   adjst_qty=newRunningInventoryAdjustments.get(sku+" : "+item_name)+adjst_qty;
               }
               newRunningInventoryAdjustments.put(sku+" : "+item_name, adjst_qty);
               newRunningInventoryAdjustmentsWithAdjustmenets.put(sku+" : "+item_name+"-"+adjst_qty, adjst_qty);
      
           }
           in.close();
           br.close();
           
           newRunningInventoryAdjustments=sortByValue(newRunningInventoryAdjustments);
           
           Iterator iter=newRunningInventoryAdjustments.keySet().iterator();
           String sku_item_name;
           
           while(iter.hasNext()){
               sku_item_name=(String) iter.next();
               adjst_qty=newRunningInventoryAdjustments.get(sku_item_name);
               str=sku_item_name+"\t"+adjst_qty;
                ///  if(str.indexOf("MK2042013009_FBA")>=0)
                  //    System.out.println("debug");               
               if(adjst_qty<0 ){
                  if(existingInventoryAdjustments.containsKey(sku_item_name+"-"+adjst_qty)){
                      str=existingInventoryAdjustments.get(sku_item_name+"-"+adjst_qty);
                      
                  } 

                  //write to adjustmenets file
                  fstream_out1 = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\analyze-inventory-adjustments.txt",true);
                  out1 = new BufferedWriter(fstream_out1);
                 out1.write(str);
                 out1.newLine();
                 out1.close();;
                 
               }
           }
           
           //Then at the end, write the items that are not in new adjustments.
            iter=existingInventoryAdjustments.keySet().iterator();
         
           while(iter.hasNext()){
               String sku_item_name_withqty=(String) iter.next();
 
                  if(newRunningInventoryAdjustmentsWithAdjustmenets.containsKey(sku_item_name_withqty)==false){
                      str=existingInventoryAdjustments.get(sku_item_name_withqty);
                  //write to adjustmenets file
                  fstream_out1 = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\analyze-inventory-adjustments.txt",true);
                  out1 = new BufferedWriter(fstream_out1);
                 out1.write(str+"\tOLD");
                 out1.newLine();
                 out1.close();;
                }
           }
           
           
                    
        }catch (Exception  e){
            System.out.println("Current error line at str:"+str);
            e.printStackTrace();
        }
    }public static HashMap<String, Integer> sortByValue(HashMap<String, Integer> map) {
        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(map.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {

            public int compare(Map.Entry<String, Integer> m1, Map.Entry<String, Integer> m2) {
                return (m1.getValue()).compareTo(m2.getValue());
            }
        });

        HashMap<String, Integer> result = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
    private static void createCostAnalyzer() {
        try{
            System.out.println("Inside  createCostAnalyzer");
        FileInputStream fstream = new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\FBA-RunningCostAnalyzer.txt");
          // Get the object of DataInputStream
          DataInputStream in = new DataInputStream(fstream);
          BufferedReader br = new BufferedReader(new InputStreamReader(in));
          String str;
          ArrayList existingAnalyzedItemsSKU=new ArrayList();
         
          String existingProfit="";
          br.readLine();
           while ((str = br.readLine()) != null)   {
               existingAnalyzedItemsSKU.add(str.split("\t")[0]);
               
      
           }
           in.close();
           br.close();

          fstream = new FileInputStream("C:\\Google Drive\\Dropbox\\existing-inventory-FBA.txt");
          // Get the object of DataInputStream
            in = new DataInputStream(fstream);
            br = new BufferedReader(new InputStreamReader(in));
            Integer afnfulfillablequantity;
            String sku;
            FileWriter fstream_out1 = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\FBA-RunningCostAnalyzer.txt",true);
            BufferedWriter out1 = new BufferedWriter(fstream_out1);
            br.readLine();
            MatchThePrice m=new MatchThePrice();
            m.readAndFillFileContents();
            ArrayList<String> restock_Contents=m.restock_Contents;
            myinventory_Contents=m.myinventory_Contents;      
            existinginventoryFBA_Contents=m.existinginventoryFBA_Contents;
            
            HashMap<String,Float> currentPriceMaByAsins=new HashMap();
            HashMap<String,String> currentQtyBySku=new HashMap();
            HashMap<String,Float> myPriceBySku=new HashMap();
             for(int sk=1;sk<myinventory_Contents.size();sk++){
                 str=myinventory_Contents.get(sk);
                 String tmpPrice=str.split("\t")[7];
                 if(tmpPrice!=null&& tmpPrice.length()>0 && tmpPrice.trim().equals("null")==false && tmpPrice.equals("bestprice")==false)
                      currentPriceMaByAsins.put(str.split("\t")[2], Float.valueOf(tmpPrice));
               }
             for(int sk=1;sk<existinginventoryFBA_Contents.size();sk++){
                 str=existinginventoryFBA_Contents.get(sk);
                 String curQty=str.split("\t")[10];
                 String myPriceS=str.split("\t")[5];
                 if(curQty!=null&& curQty.length()>0 && curQty.trim().equals("null")==false  ){
                     currentQtyBySku.put(str.split("\t")[0], curQty);
                 }
                     
              if(myPriceS!=null&& myPriceS.length()>0 && myPriceS.trim().equals("null")==false  ){
                       myPriceBySku.put(str.split("\t")[0], Float.valueOf(myPriceS));
                      
               }             
             }
             
           while ((str = br.readLine()) != null)   {
               
               afnfulfillablequantity=Integer.parseInt(str.split("\t")[10]);
               sku=str.split("\t")[0];
               if(str.split("\t")[5].length()<=0)
                   continue;
               
               System.out.println("Sku now is :"+sku +" price is "+str.split("\t")[5]);
 

             // if(afnfulfillablequantity>0)
               //   currentPriceMap.put(sku, Float.valueOf(str.split("\t")[5]));
               if(afnfulfillablequantity>=5 &&existingAnalyzedItemsSKU.contains(sku)==false){
                   //write to the RunnCost file
                    out1.write( sku+"\t"+str.split("\t")[2]+"\t"+str.split("\t")[3]+"\t"+str.split("\t")[10]+"\t"+str.split("\t")[5]);
                    out1.newLine();
                   
               }
           }
            out1.close();              

            
           in.close();
           br.close();

           
           //Now lets go one by one.
           fstream = new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\FBA-RunningCostAnalyzer.txt");
          // Get the object of DataInputStream
          in = new DataInputStream(fstream);
          br = new BufferedReader(new InputStreamReader(in));
        
              fstream_out1 = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\FBA-RunningCostAnalyzer_new.txt" );
              out1 = new BufferedWriter(fstream_out1);
          
           String title= br.readLine();
                out1.write(title);
                out1.newLine();     
          //  newItemsInLoss.append("<table border=2>");
            int curRow=2;
           while ((str = br.readLine()) != null)   {
               try{
                //   if(str.indexOf("B0005ZHPFI")>=0)
                 //      System.out.println("Debug");
                sku=str.split("\t")[0];
                String asin=str.split("\t")[1];
                
                String itemName=str.split("\t")[2];
                if(itemName.trim().length()==0)
                    itemName="Item Name";
                String qty=str.split("\t")[3];
                String price=str.split("\t")[4];
                String fbafees=str.split("\t").length>5?str.split("\t")[5]:"";
                String originalCost=str.split("\t").length>7?str.split("\t")[7]:"";
                String orderFromS=str.split("\t").length>9?str.split("\t")[9]:"";
                
                String yourPriceCell="E"+curRow;
                String fbaFeesCell="F"+curRow;
                String fbaMarginCell="G"+curRow;
                String originalCostCell="H"+curRow;
                String profitCell="I"+curRow;
                curRow++;
           //    if(str.indexOf("RP1025120237313_FBA")>=0)
              //     System.out.println("Debug");
                if(currentQtyBySku.get(sku)!=null)
                   qty= currentQtyBySku.get(sku);
                else
                    qty="0";                
                if(Integer.valueOf(qty)<=0){
                    out1.write(sku+"\t"+asin+"\t"+itemName+"\t"+qty+"\t"+price+"\t"+fbafees+"\t0\t"+originalCost+"\t0"+"\t"+orderFromS);
                    out1.newLine();
                     continue;                    
                }
                String fbaMarginS="0"; String profitS="";
                
                Float priceF=(float)0,fbafeesF=(float)0,originaCostF=(float)0;
                if(price!=null&& price.equals("null") ==false &&  price.length()>0){
                    priceF=Float.valueOf(price);
                }
                   priceF= currentPriceMaByAsins.get(asin);
                if(priceF==null || priceF==0){
                                       
                     System.out.println("Price not known for item:"+str +" LETS GET CURRENT BEST PRICE");
                      if(todayissunday == false && price!=null && price.length()>0){
                         System.out.println("item not in inventory, but best price is already known and today is not sunday. So use existing price");
                          priceF=Float.valueOf(price);
                         
                     }else
                      {
                         

                    
                        String orderFrom="YES";
                        /*for(int i=0;i<restock_Contents.size();i++){
                            if(restock_Contents.get(i).indexOf(asin)>=0){
                                //Read orderFromColumn from restock file for this asin. If that column contains NO, then no need to get current best price.
                                String s=restock_Contents.get(i);
                                if(s.split("\t").length> FbaInventoryAnalyzer.ORDERFROM_COLUMN)
                                {
                                    orderFrom= s.split("\t")[FbaInventoryAnalyzer.ORDERFROM_COLUMN].toUpperCase();
                                    System.out.println("Order from column from restock file for item : "+asin +" is " +orderFrom);
                                }
                            }
                                
                        }*/
                    if(orderFrom.equals("NO")){
                        priceF=(float)-100000;
                    }else{
                         HashMap hm=m.getProductDetails(asin,true,false,false);
                         if(hm.containsKey("bestprice") && (Float)hm.get("bestprice")>0){
                            priceF=(Float)hm.get("bestprice");
                           }
                         }
                         
                     } 
                 }                   
                   price=String.valueOf(priceF);
 
 
                if(fbafees!=null&& fbafees.length()>0){
                    fbafeesF=Float.valueOf(fbafees);
                }else{
                    HashMap fbaMap=null;
                    
                    try{
                        fbaMap=RetrievePage.getDetailsFromFbaCalculatorNewMWS(null,asin.trim(), price,true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if(fbaMap!=null){
                        fbafeesF=(Float) fbaMap.get("fbaFees");
                       
                    }
                        if(fbafeesF==0)
                            fbafeesF=(float)1000; 
                   // fbafeesF+=(float)2.00;
                                    
                    
                    fbafees=String.valueOf(fbafeesF);
                    //Prabu, increasing fba fees by $2
                    
                   // fbaFeesNotKnown.append(itemName+"<br/>");
                    
                }
                if(originalCost!=null&& originalCost.length()>0){
                    originaCostF=Float.valueOf(originalCost);
                }else{
                    originalNotKnown.append(itemName+"<br/>");
                }
                if(priceF  !=null && priceF >0 &&fbafeesF >0   ){
                    fbaMarginS="=ROUND("+yourPriceCell+"*0.85-"+fbaFeesCell+",2)";
                    
                }
                if(priceF  !=null &&priceF >0 &&fbafeesF >0  &&originaCostF >0   ){
                     profitS="=ROUND("+fbaMarginCell+"-"+originalCostCell+",2)-"+MatchThePrice.MY_FBA_LABOR_CHARGE;
                    if(currentQtyBySku.get(sku)!=null){
                        Float myPrice=(Float)myPriceBySku.get(sku);
                        if(myPrice==null)
                            System.out.println("myPrice is null");
                         Float myfbaMargin=(myPrice*(float).85)-fbafeesF-MatchThePrice.MY_FBA_LABOR_CHARGE;
                         Float myProfit=myfbaMargin-originaCostF;
                          myProfit=Math.round(myProfit*(float)100)/(float)100; 
                         
                        if(myProfit<0){

                            float dummykey=myProfit*Float.valueOf(qty) +((float)1/(System.currentTimeMillis()%100000));
                            if(itemsInLossMap.containsKey(dummykey))
                                itemsInLossMap.put(-(float)System.currentTimeMillis(), "<tr> <td><a href='http://www.amazon.com/gp/product/"+asin+"'>"+ itemName +"</a></td><td>"+qty+"</td><td>"+myPrice+"</td><td>"+myProfit+"</td></tr>");
                            else
                                itemsInLossMap.put(dummykey, "<tr> <td><a href='http://www.amazon.com/gp/product/"+asin+"'>"+ itemName +"</a></td><td>"+qty+"</td><td>"+myPrice+"</td><td>"+myProfit+"</td></tr>");
                            if(asin.indexOf("B006HT4W4S")>=0)
                                System.out.println("break");
                

                        }                        
                    }

                }

                out1.write(sku+"\t"+asin+"\t"+itemName+"\t"+qty+"\t"+price+"\t"+fbafees+"\t"+fbaMarginS+"\t"+originalCost+"\t"+profitS+"\t"+orderFromS);
                out1.newLine();
                 
                 
                
               }catch (Exception e){
                   e.printStackTrace();
               }
               
               
           }
           //newItemsInLoss.append("</table>");
           Iterator iter=itemsInLossMap.keySet().iterator();
           itemsInLoss1=new StringBuffer();
           itemsInLoss1.append("<table border=2>");
           boolean itemsInLossFound=false;
           while(iter.hasNext()){
               Float key=(Float) iter.next();
               String value=itemsInLossMap.get(key);
               itemsInLoss1.append(value);
               
               itemsInLossFound=true;
                       
           }
           itemsInLoss1.append("</table>");
           in.close();
           br.close();
           out1.close();
            
           
           FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\fba\\FBA-RunningCostAnalyzer_new.txt"),new File("C:\\Google Drive\\Dropbox\\fba\\FBA-RunningCostAnalyzer.txt"));
           new File("C:\\Google Drive\\Dropbox\\fba\\FBA-RunningCostAnalyzer_new.txt").delete();;
           

           //Finally lets send emails.
           StringBuffer email=new StringBuffer();
           if(itemsInLossFound){
               email.append("<h2> PRABU:::::: Following NEW Items in Loss:<br/></h2>");
              // email.append(newItemsInLoss);
               
               email.append("<h2> Following are ALL  Items in Loss:<br/></h2>");
               email.append(itemsInLoss1);
           }
           
           if(originalNotKnown.length()>0){
               email.append("<h2> Following Items Original cost not known:<br/></h2>");
               email.append(originalNotKnown);
           }
           
           if(fbaFeesNotKnown.length()>0){
               email.append("<h2> Following Items FBA fees not known:<br/></h2>");
               email.append(fbaFeesNotKnown);
           }
            //sortRunCostAnalyzerByQuantity();
           if(email.length()>0){
               email.append("<br>Go to dropbox\\fba\\FBA-RunningCostAnalyzer file and fill in the values for original cost.");
             String[] to={"mprabagar80@gmail.com","vi_himanshu@varuninfotech.com" ,"a2020patel@gmail.com "};
                String[] cc={};
                String[] bcc={};
             System.out.println("Sending email with body:"+email.toString());
                                     Mail.sendMail( "smtpout.secureserver.net","465","true",
                                        "true",true,"javax.net.ssl.SSLSocketFactory","false",to,cc,bcc,
                            "FBA Running Analyzer",email.toString() ,false,false,false,false,null);
             
               
           }
           
        }catch (Exception e){
            e.printStackTrace();;
        }

    }        

    private static Float getFBAFeesOLD(String asin) {
         Float fbaFees=(float)0;
         try{
             System.out.println("Going to calculate FBA Fees for ASIN:"+asin);
                WebClient webClient1 ;
               webClient1=new WebClient(BrowserVersion.FIREFOX_38 );
                if(RetrievePage.isOffice()){
                    System.setProperty("java.net.preferIPv4Stack", "true");
                    webClient1 = new WebClient(BrowserVersion.getDefault(),"proxydirect.tycoelectronics.com", 80);
                    DefaultCredentialsProvider credentialsProvider = (DefaultCredentialsProvider) webClient1.getCredentialsProvider();
                    credentialsProvider.addCredentials("us083274",  RetrievePage.PASSWORD1);                      
                  
                }    
            
                System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "error");
                webClient1.getOptions().setThrowExceptionOnScriptError(false);
                webClient1.getOptions().setJavaScriptEnabled(true);
                webClient1.getOptions().setCssEnabled(false);
                 webClient1.waitForBackgroundJavaScript(5000);

                HtmlPage page = webClient1.getPage("https://sellercentral.amazon.com/gp/fbacalc/fba-calculator.html");
                if(page.getByXPath("//input[@id='searchString']").size()>0){
                    DomElement elem=(DomElement) page.getByXPath("//input[@id='searchString']").get(0);
                    elem.setAttribute("value", asin);
                    
                     elem=(DomElement) page.getByXPath("//button[@name='Search']").get(0);
                     page=elem.click();
                     
                     elem=(DomElement) page.getByXPath("//input[@id='AFNSellingPrice']").get(0);
                     elem.setAttribute("value", "100");
                     
                     elem.blur();
                     Thread.sleep(2000);
                     elem=(DomElement) page.getByXPath("//input[@id='AFNFulfillmentSubtotal']").get(0);
                     System.out.println("FBA fees:"+elem.getAttribute("value"));
                    fbaFees=Float.valueOf(elem.getAttribute("value"));
                    if(fbaFees==0){
                        System.out.println("Something is wrong: break");
                    }
                }
                
                
                
           }catch (Exception e){
               e.printStackTrace();
           }
                  
         return fbaFees;
    }
    
    public void sortActiveListingByDate(){
        //Put the rank as additional column in the existing-all-inventory file
        if(true)
            return;
        try{
        FileInputStream fstream = new FileInputStream("C:\\Google Drive\\Dropbox\\my_inventory.txt");
          // Get the object of DataInputStream
          DataInputStream in = new DataInputStream(fstream);
          BufferedReader br = new BufferedReader(new InputStreamReader(in));
          String str;
          HashMap <String,String> rankMap=new HashMap();
           while ((str = br.readLine()) != null)   {
               rankMap.put(str.split("\t")[2],str.split("\t")[1]+"\t"+str.split("\t")[4]);
           }
           in.close();
           br.close();
          fstream = new FileInputStream("C:\\Google Drive\\Dropbox\\existing-all-inventory.txt");
          // Get the object of DataInputStream
          in = new DataInputStream(fstream);
          br = new BufferedReader(new InputStreamReader(in));
          
          TreeMap<String, String> items=new TreeMap();
          String open_date;

          String header=null;
           String asin,rank;
           while ((str = br.readLine()) != null)   {
               if(header==null){
                   header=str;
                   continue;
               }
                open_date=str.split("\t")[6]+str.split("\t")[3];
                if(items.containsKey(open_date)){
                    System.out.println("how is it possible");
                }
                
                asin=str.split("\t")[16];
                rank=rankMap.get(asin);
                str=str+"\t"+rank;
                items.put(open_date,str);
            }
          br.close();
        FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\existing-all-inventory.txt"),new File("C:\\Google Drive\\Dropbox\\existing-all-inventory-unsorted.txt"));

        FileWriter writer = new FileWriter("C:\\Google Drive\\Dropbox\\existing-all-inventory.txt");
        writer.write(header);
        writer.write('\n');

        for(String val : items.values()){
                writer.write(val);
                writer.write('\n');
        }
        writer.close();


        }catch (Exception e){
            e.printStackTrace();;
        }

    }
   public static void sortRunCostAnalyzerByQuantity(){
        //Put the rank as additional column in the existing-all-inventory file
        
        try{
          FileInputStream fstream = new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\FBA-RunningCostAnalyzer.txt");
          // Get the object of DataInputStream
         DataInputStream in = new DataInputStream(fstream);
         BufferedReader br = new BufferedReader(new InputStreamReader(in));
          String str;
          TreeMap<Long, String> items=new TreeMap();
          Long qty;
           String header=null;
           String asin,rank;
           while ((str = br.readLine()) != null)   {
               if(header==null){
                   header=str;
                   continue;
               }
                if(str.split("\t")[3]==null||str.split("\t")[3].length()==0)
                    qty=(long)0;
                else
                    qty=Long.parseLong(str.split("\t")[3]);
                long t=System.currentTimeMillis()%1000000;
                Thread.sleep(100);
                qty=(qty*10000000)+(int)t;
                if(items.containsKey(qty)){
                    qty+=1;
                    System.out.println("how is it possible");
                    
                }
                 qty=(999999999-qty);
                
                items.put(qty,str);
            }
          br.close();
 
        FileWriter writer = new FileWriter("C:\\Google Drive\\Dropbox\\fba\\FBA-RunningCostAnalyzer_new.txt");
        writer.write(header);
        writer.write('\n');

        for(String val : items.values()){
                writer.write(val);
                writer.write('\n');
        }
        writer.close();

           FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\fba\\FBA-RunningCostAnalyzer_new.txt"),new File("C:\\Google Drive\\Dropbox\\fba\\FBA-RunningCostAnalyzer.txt"));
           new File("C:\\Google Drive\\Dropbox\\fba\\FBA-RunningCostAnalyzer_new.txt").delete();;

        }catch (Exception e){
            e.printStackTrace();;
        }

    }    
    public static void reducePricesForMoreThan3Months() {
         try{
               date = Calendar.getInstance().getTime();
             SimpleDateFormat  formatter = new SimpleDateFormat("E");
              today = formatter.format(date);
              ArrayList skuAlreadyReducedInThisSession=new ArrayList();
              
             if( today.trim().equals("Sun") ==false)
               return;
              System.out.println("Inside reducePricesForMoreThan3Months today is not sunday");
                  //Now open health file
                  BufferedReader  br0 = new BufferedReader(new
                  InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\fba\\inventory-health.txt")));
                   String strLine0;
                   br0.readLine();  

                    FileWriter fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\amazon_feeds\\production\\reviewandsend\\reduce-prices.txt");
                    BufferedWriter out0 = new BufferedWriter(fstream_out0);
                    out0.write("sku	product-id	product-id-type	price	item-condition	quantity	add-delete	will-ship-internationally	expedited-shipping	standard-plus	item-note	fulfillment-center-id	product-tax-code	leadtime-to-ship");
                    out0.newLine();
                   String sku, inagemorethan90days,inagemorethan180days,inagemorethan271days;
                   boolean mailtobesent=false;
             MatchThePrice m=new MatchThePrice();
                     m.readAndFillFileContents();                                      
                  while ((strLine0 = br0.readLine()) != null)   {
                    sku=strLine0.split("\t")[1];
                    if(skuAlreadyReducedInThisSession.contains(sku))
                        continue;
                    skuAlreadyReducedInThisSession.add(sku);
                    String asin=strLine0.split("\t")[3];
                    inagemorethan90days=strLine0.split("\t")[12];
                    inagemorethan180days=strLine0.split("\t")[13];
                    inagemorethan271days=strLine0.split("\t")[14];
                    
                    String lotofweekscoverByWeek=strLine0.split("\t")[23];
                    String lotofweekscoverByMonth=strLine0.split("\t")[23];
                    Float price=Float.valueOf(strLine0.split("\t")[32]);
                    String title=strLine0.split("\t")[4];
                 //   if(strLine0.indexOf("B008NYWE4A")>=0)
                   //     System.out.println("debug");
                    if((Integer.parseInt(inagemorethan90days)>0 ||Integer.parseInt(inagemorethan180days)>0 ||Integer.parseInt(inagemorethan271days)>0 ) && (lotofweekscoverByWeek.equals("Infinite")|| Float.valueOf(lotofweekscoverByWeek)>=16) && (lotofweekscoverByMonth.equals("Infinite")|| Float.valueOf(lotofweekscoverByMonth)>=16)){
                        //In store for more than 90 days and no items shipped in last 30 days
                   try{

                             ArrayList<String> myinventory_Contents=m.myinventory_Contents;
                             for(int i=0;i<myinventory_Contents.size();i++){
                                 if(myinventory_Contents.get(i).indexOf(asin)>=0){
                                     try{
                                        Float bestprice=Float.valueOf(myinventory_Contents.get(i).split("\t")[7]);
                                        if(price==0 || bestprice<price)
                                            price=bestprice;
                                     }catch (Exception e2){}
                                     
                                 }
                             }
            
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                   
                         Float reducedprice=price*(float).9;
                         String t=StringUtils.substringBefore(String.valueOf(reducedprice),".");
                         t=t+".96";
                         reducedprice=Float.valueOf(t);
                       //  if(reducedprice<6)
                         //    reducedprice=(float)6.96;
                         if(price<2)
                             continue;
                          String str=sku+"	"+asin+"	1	"+reducedprice+"	11	11	a					AMAZON_NA		5	"+title;
                        out0.write(str);
                        out0.newLine();
                        mailtobesent=true;
                        
                    }
                  }
                  out0.close();           
                  if(mailtobesent==false)
                      return;
             String[] to={ "a2020patel@gmail.com","vi_himanshu@varuninfotech.com","mprabagar80@gmail.com"};
                String[] cc={};
                String[] bcc={};
              String subject=" Items in Inventory more than 90 days";
                Mail.sendMail("smtpout.secureserver.net","465","true",
                "true",true,"javax.net.ssl.SSLSocketFactory","false",to,cc,bcc,
                subject,"Verify and upload file from C:\\Google Drive\\Dropbox\\amazon_feeds\\production\\reviewandsend\\reduce-prices.txt..Move it to outgoing folder. They are in inventory for more than 90 days",false,false,false,false,"C:\\Google Drive\\Dropbox\\amazon_feeds\\production\\reviewandsend\\reduce-prices.txt");
                FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\amazon_feeds\\production\\reviewandsend\\reduce-prices.txt"), new File("C:\\Google Drive\\Dropbox\\amazon_feeds\\production\\reviewandsend\\reduce-prices_"+System.currentTimeMillis()+".txt"));                                     

                  
             }catch (Exception e){e.printStackTrace();}
    }
    
}
