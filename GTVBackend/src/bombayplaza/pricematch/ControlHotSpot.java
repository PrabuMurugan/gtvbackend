package bombayplaza.pricematch;


import java.io.BufferedReader;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author US083274
 */
public class ControlHotSpot {
    public static String HOTSPOTCOMMAND="C:\\Temp2012\\hotspotcommand.txt";
public static void main(String args[]){
      try {
            Date date0 = Calendar.getInstance().getTime();
        DateFormat formatter0 = new SimpleDateFormat("kk");
        String today0 = formatter0.format(date0);
        String command=null;
        FileInputStream fstream0 = new FileInputStream(HOTSPOTCOMMAND);
        // Get the object of DataInputStream
        DataInputStream in0 = new DataInputStream(fstream0);
        BufferedReader br00 = new BufferedReader(new InputStreamReader(in0));
        String strLine00;
          while ((strLine00 = br00.readLine()) != null)   {
              if(strLine00.trim().length()>1)
                command=strLine00;
         }
        in0.close();
        
        if(command==null){
            System.out.println("No command found to run");
            return;
        }
        //Empty the file
        FileWriter fstream_out = new FileWriter(HOTSPOTCOMMAND,false);
        BufferedWriter out = new BufferedWriter(fstream_out);
        out.write("");
        
        out.close();

        Runtime rt = Runtime.getRuntime();
      
            //Process pr = rt.exec("cmd /c dir");
            Process p = rt.exec(command);
            String s;
        BufferedReader stdInput = new BufferedReader(new
                 InputStreamReader(p.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                 InputStreamReader(p.getErrorStream()));

            // read the output from the command
            System.out.println("Here is the standard output of the command:\n");
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }

            // read any errors from the attempted command
            System.out.println("Here is the standard error of the command (if any):\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }

        } catch (Exception ex) {
           ex.printStackTrace();
        }

}
}
