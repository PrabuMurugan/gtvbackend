/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bombayplaza;

import amazon.product.Product;
import amazon.product.QueryProduct;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Acer
 */
public class Inventory_Amazon_Manu {
   static  int collectedProducts=0;
   static ArrayList visitedAsins2=new ArrayList();
   //static String SELLERID="A11EIBJBV5KQME";
   //static String SELLERID="A20RGF2W7MYP64";
  // static String SELLERID="A3GO3ZGMFW0B3F";
   static String SELLERID="";
  
  //  static String SELLERID="A1TAVADKTUUTLO";
 
   static String FILE="C:\\Temp2012\\inventory_mine.txt";
   static String TOTALASINFILE="C:\\Temp2012\\vistedAsins.txt";
    static MatchThePrice m=null;
    public static int main(String args[]){
      m= new MatchThePrice();
        m.readAndFillFileContents();
          RetrievePage.USE_PROXIES=true;
          
        
            
     openProducts( TOTALASINFILE);
        try {
            //for(int count=2;count<=2;count++)
            {
                //http://www.amazon.com/s/qid=1328777704/ref=sr_pg_2?ie=UTF8&me=A30ZA0TJNSOPSP&rh=&page=40
                        //page = webClient.getPage("http://www.amazon.com/s/qid=1328777704/ref=sr_pg_2?ie=UTF8&me=A30ZA0TJNSOPSP&rh=&page="+count);
                
                //String url="http://www.amazon.com/s/qid=1329064281/ref=sr_pg_2?ie=UTF8&me=SELLERID&rh=&page="+count;
                //String url="http://www.amazon.com/s/ref=bl_sr_grocery?_encoding=UTF8&node=16310101&field-brandtextbin=Swad#/ref=sr_pg_2?rh=n%3A16310101%2Cp_4%3ASwad&ie=UTF8&qid=1329422363&page="+args[0];
            String url=args[0];
                url=url.replaceAll("SELLERID",SELLERID);
                System.out.println("Opening url : "+url);
                RetrievePage.USE_PROXIES=true;
                WebClient webClient1 = new WebClient(BrowserVersion.FIREFOX_38); 
               Page page = webClient1.getPage(url);
               //  webClient1.waitForBackgroundJavaScriptStartingBefore(10000);
             //   http://www.amazon.com/gp/browse.html?ie=UTF8&marketplaceID=ATVPDKIKX0DER&me=A33HRFTZ9IMKIK
                
            //           System.out.println("PAge : "+page.asText());

                   // DomElement div=page.getElementById("searchResults");
                  //  DomNodeList<DomElement> aTags = page.getElementsByTagName("a");
               String[] links= StringUtils.substringsBetween(page.getWebResponse().getContentAsString(), "dp/", "/");
               int  prevCollectedProducts=collectedProducts;            
//                    System.out.println("Got page1. LEngth of aTags:"+aTags.size());
                if(links!=null){
                 for(int i=0;i<links.length;i++){
                        System.out.println("Link si "+links[i]);
                        //HtmlAnchor a=(HtmlAnchor) aTags.get(i);
                        //HtmlPage page2=a.click();
                         

                        collectAttributes(links[i]);
                    }     
                }
                    
                  
                    links= StringUtils.substringsBetween(page.getWebResponse().getContentAsString(), "/gp/product/", "/");
                
//                    System.out.println("Got page1. LEngth of aTags:"+aTags.size());
                    if(links!= null){
                        for(int i=0;i<links.length;i++){
                            System.out.println("Link is "+links[i]);
                            //HtmlAnchor a=(HtmlAnchor) aTags.get(i);
                            //HtmlPage page2=a.click();


                            collectAttributes(links[i]);
                        }                        
                    }

                    System.out.println(" ITEMS COLLECTED prev:"+prevCollectedProducts+",collectedProducts:"+collectedProducts);
                    if(collectedProducts<=prevCollectedProducts+2)
                    {
                        System.out.println("NO ITEMS COLLECTED. EXITING");
                       return -1;
                        
                    }
                
            }
        
 
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }
 

    private static void collectAttributes(String asin) throws IOException {
        if(asin.indexOf("B")!=0)
            return;
       //  if(url.indexOf("/gp/product/")<0 &&url.indexOf("/dp")<0 ){
          //   System.out.println("/gp/product & /dp missing so skipping, url:"+url);
         //    return;
         //}
        // String  asin=url;
        // if(url.indexOf("/gp/product") <0 &&url.indexOf("/dp") <0)
          //   return;
          
        // if(url.indexOf("/gp/product/")>=0)
          //   asin=StringUtils.substringBetween(url, "/gp/product/","/");
         //else 
           // asin=StringUtils.substringBetween(url, "/dp/","/");
         if(asin==null || asin.trim().length()==0)
             return;
         if(visitedAsins2.contains(asin)){
              collectedProducts++;
             return;
             
         }
        /*if(StringUtils.countMatches(a.getParentNode().getParentNode().asText(),"Subscribe & Save")==1){
             System.out.println("No need to visit the product. Amazon has subscrive and save");
            writeToCsv("NA\t"+asin+"\tNA\tNA\tY\tamazon\tNA\tNA\tNA\t");             
         }*/
         System.out.println("Going to get product details for asin"+asin);
         MatchThePrice.JAVASCRIPTNONO=true;
         HashMap hm=MatchThePrice.getProductDetails(asin, false,false,false);
         QueryProduct.I_WANT_ORIGINAL_BESTPRICE_AT_NOOTHER_FBA_SELLERS=true;
        // Product p=QueryProduct.getProductListings1("B013S27CLQ");
         
         
            System.out.println("Size of visitedAsins2:"+visitedAsins2.size());
         visitedAsins2.add(asin);
         writevisitedAsin(asin);
        
        float bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
       //  if(isCurrentItemFBA==false)
         //   bestprice+=5;
        float toppriceonpage=bestprice;
         float myExistingP=(float)0;
           
         String title=(String)(hm.get("title")!=null?hm.get("title"):"NA");;
         String manu=(String)(hm.get("manu")!=null?hm.get("manu"):"NA");
         String soldby=(String)(hm.get("soldby")!=null?hm.get("soldby"):"NA");
         String model_number=(String)(hm.get("model_number")!=null?hm.get("model_number"):"NA");
         String nootherFbaSeller=(String)(hm.get("NOOTHERFBASELLER")!=null?hm.get("NOOTHERFBASELLER"):"NA");
        String nReviews=(String)(hm.get("CustomerReviewCount")!=null?hm.get("CustomerReviewCount"):"0");
          
         boolean wesell=false;
         for(int x=0;x<m.existinginventoryAll_Contents.size();x++){
             if(m.existinginventoryAll_Contents.get(x).indexOf(asin)>=0 ){
                 wesell=true;
             }
         }
         //Hit the getProductDetails
        
         String strWToWrite=title+"\t"+asin+"\t"+bestprice+"\t"+nReviews+"\t"+nootherFbaSeller+"\t"+soldby+"\t"+manu+"\t"+wesell+"\t"+model_number;
          Connection con =  null;
        
        try {
            //url="http://www.amazon.com/Fenugreek-Methi-Powder-Indian-Grocery/dp/B003CI8GU8";
      
           String local_img_file="";
          
            try{
                
                writeToCsv(strWToWrite.replaceAll("\n","").replaceAll("\r", ""));
            }catch (Exception e){e.printStackTrace();}

           // writeToCsv(url,title,desc,manu,features,price,local_img_file,asin,upc,rank,soldbyamazon);
           System.out.println("Product name:"+title+",asin:"+asin);
           if(false && collectedProducts++>1000)
           {
               System.out.println("COLLECTION LIMIT REACHED");
                System.exit(0);
           }
            
           // HtmlPage page=al.click();
            //System.out.println("pAGE IS"+page.asText());
        } catch (Exception ex) {    
            //System.err.println("ERROR WHILE COLLECTING URL:"+((HtmlAnchor)a).getHrefAttribute());
           ex.printStackTrace();
        }
    }
    

static String getfileName(String s){
StringBuffer str=new StringBuffer();
for(int i=0;i<s.length();i++){
    if((s.charAt(i)>='0' &&s.charAt(i)<='9') ||(s.charAt(i)>='A' && s.charAt(i)<='Z') || (s.charAt(i)>='a' &&s.charAt(i)<='z' ) )
    {
        str.append(s.charAt(i));
    }  else{
        str.append("_");
    }
}       




return str.toString();
}
private static void writevisitedAsin(String url) throws IOException {
        
        if(true)
            return;
         FileWriter fstream = new FileWriter(TOTALASINFILE,true);
         /*url=StringUtils.substringBefore(url, "?");
         url=StringUtils.substringBeforeLast(url, "/");*/
  BufferedWriter out = new BufferedWriter(fstream);
  out.write(url);
  out.newLine();
  //Close the output stream
  out.close();
    }

    public static void writeToCsv(String str) 
            throws IOException {
    
         FileWriter fstream = new FileWriter(FILE,true);
          
         
  BufferedWriter out = new BufferedWriter(fstream);
  //out.write(url+","+title+","+desc+","+manu+","+price+","+local_img_file+","+features);
  out.write(str);
  out.newLine();
  //Close the output stream
  out.close();
    }

  
    private static boolean openProducts( String file2) {
  
    try {
        if(new File(file2).exists()==false)
            return false;
        Scanner scanner = new Scanner(new FileInputStream(file2));
    String line;
      while (scanner.hasNextLine()){
          line=scanner.nextLine();
           
        //if(visitedURLS2.contains(StringUtils.substringBefore(url, "/ref"))==false)
            visitedAsins2.add(line);
     
         
      }
    }
    catch (Exception e){
        e.printStackTrace();
    }

    
         return false;
    }
}
