    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bombayplaza.catalogcreation;
import static bombayplaza.catalogcreation.CamelCamelCamel.isAmazonSellingInLast3Month;
import bombayplaza.pricematch.VerifyFBASkuRank;
import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.w3c.dom.Document;
import org.w3c.dom.*;
import bombayplaza.pricematch.*;
import harrisburgstore.CreateItem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException; 
 
import java.util.*;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
 
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.InputSource;


/**
 *
 * @author us083274
 */
public class CreateCatalog {
    static ArrayList manuUrlsProcessed=new ArrayList();
    static ArrayList manuUrlsToBeProcessed=new ArrayList();
    static ArrayList ASINSProcessed=new ArrayList();
    static ArrayList UPCsProcessed=new ArrayList();
    static String originalHeader="";
    static String DONOTPROCESS_MANUURLS="true";
     static String WHOLESALER_NAME="true";
    static ArrayList<String> EMAIL_IDS=new ArrayList();
    static boolean writeToMasterCatalog=false;
    static boolean DOWNLOAD_IMAGE=true;
    String PRODUCTION = System.getProperty("PRODUCTION", "false");
    public static ArrayList harrisburgItems=new ArrayList();
    public static int MAXROWS_COUNT=200;
    public static int MAXROWS_SLEEP_TIME=0;
    public static int ROBOT_SLEEP_TIME=0;
    public static int SLEEP_FOR_EACH_VISIT=0;
    public static boolean CLEANRUN=false;
    public static boolean ERROR_MOVE_TO_NEXT_PAGE=false;
    
    private static final String MODULE = "[ CreateCatalog ] ";
     public static void main(String args[]){
         
            
           
        // if(args.length>0 && args[0].equals("DONOTPROCESS_MANU_URLS"))
          //      DONOTPROCESS_MANUURLS = "true";

         if(args.length>0 && args[0].equals("WRITE_TO_MASTER_CATALOG"))
                writeToMasterCatalog = true;
         RetrievePage.USE_PROXIES=true;
         try{
             if(System.getProperty("MAXROWS_COUNT")!=null)
                 MAXROWS_COUNT=Integer.valueOf(System.getProperty("MAXROWS_COUNT"));
             if(System.getProperty("MAXROWS_SLEEP_TIME")!=null)
                MAXROWS_SLEEP_TIME=Integer.valueOf(System.getProperty("MAXROWS_SLEEP_TIME"));
             if(System.getProperty("ROBOT_SLEEP_TIME")!=null)
                ROBOT_SLEEP_TIME=Integer.valueOf(System.getProperty("ROBOT_SLEEP_TIME"));
             if(System.getProperty("CLEANRUN")!=null)
                CLEANRUN=Boolean.valueOf(System.getProperty("CLEANRUN"));
            if(System.getProperty("CLEANRUN")!=null)
                CLEANRUN=Boolean.valueOf(System.getProperty("CLEANRUN"));
            if(System.getProperty("SLEEP_FOR_EACH_VISIT")!=null)
                SLEEP_FOR_EACH_VISIT=Integer.valueOf(System.getProperty("SLEEP_FOR_EACH_VISIT"));
            
            if(System.getProperty("USE_VPN")!=null){
                System.out.println("USE_VPN IS TRUE");
                
                RetrievePage.USE_VPN=true;
                RetrievePage.USE_PROXIES=false;
            }
                
            
            if(System.getProperty("SLEEP_TIME_ALL_PROXIES_USED_ONCE")!=null)
                RetrievePage.SLEEP_TIME_ALL_PROXIES_USED_ONCE =Integer.parseInt(System.getProperty("SLEEP_TIME_ALL_PROXIES_USED_ONCE"));
            if(System.getProperty("NUMBER_TIMES_PROXIES_BEFORE_SLEEP")!=null)
                RetrievePage.NUMBER_TIMES_PROXIES_BEFORE_SLEEP =Integer.parseInt(System.getProperty("NUMBER_TIMES_PROXIES_BEFORE_SLEEP"));
             if(writeToMasterCatalog)
                 CLEANRUN=true;
             
             System.out.println("[ CreateCatalog ] MAXROWS_COUNT : " + MAXROWS_COUNT);
             System.out.println("[ CreateCatalog ] SLEEP_TIME_ALL_PROXIES_USED_ONCE : " + RetrievePage.SLEEP_TIME_ALL_PROXIES_USED_ONCE);
             System.out.println("[ CreateCatalog ] NUMBER_TIMES_PROXIES_BEFORE_SLEEP : " + RetrievePage.NUMBER_TIMES_PROXIES_BEFORE_SLEEP);
            Connection con=RetrievePage.getConnection();
             PreparedStatement pst = null;
             ResultSet rs = null;
             pst=con.prepareStatement("select tajplaza.cleanupc(upc) from hb_bc.product ");
             rs=pst.executeQuery();
             while(rs.next()){
                harrisburgItems.add( rs.getString(1));
             }
             con.close();
           
         }catch (Exception e2){
             e2.printStackTrace();
         }
        /* if(args.length==0){
             System.out.println("Enter writeToMasterCatalog : 1)true 2:false");
              DataInputStream in = new DataInputStream(System.in);
              try{
                  
              
              Integer intnumber = Integer.parseInt(in.readLine());
              if(intnumber==1){
                  writeToMasterCatalog=true;
              }
              }catch (Exception e){
                  e.printStackTrace();
              }
              
              
              
         }*/
           System.out.println("[ CreateCatalog ] In the beginning of program, DONOT IS (trace 3):" + DONOTPROCESS_MANUURLS);
           
       /* try{
            System.setOut(new PrintStream(new File("C:\\Google Drive\\Dropbox\\logs\\out_CreateCatalog.txt")));
            System.setErr(new PrintStream(new File("C:\\Google Drive\\Dropbox\\logs\\error_CreateCatalog.txt")));
        }catch (Exception e){e.printStackTrace();
                
                }
         
         * /
        /*try{
                 String fileName="C:\\Google Drive\\Dropbox\\program\\catalog\\working\\input_catalog.txt";
            String outputfileName=fileName.replaceAll("input","output");
           
            String completedInFile=fileName.replaceAll("working", "completed");
            String completedOutFile=outputfileName.replaceAll("working", "completed");
            
            FileUtils.copyFile(new File(fileName),new File( completedInFile));
            FileUtils.copyFile(new File(outputfileName),new File( completedOutFile));
            
            new File(fileName).delete();
            new File(outputfileName).delete();
             
         }catch (Exception e){
             e.printStackTrace();
         } */
            // sortFile( "C:\\Google Drive\\Dropbox\\program\\catalog\\working\\output_catalog.txt",15,false);
                try{
                    String strLine0;
                    BufferedReader  br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\catalog\\manuUrlsProcessed.txt")));
                    while ((strLine0 = br0.readLine()) != null)   {
                        if(strLine0.trim().length()>0)
                            manuUrlsProcessed.add(strLine0);
                    }
                    
                }catch (Exception e){
                    e.printStackTrace();
                    // Mail.emailException(CreateCatalog.class,e);
                }
               CreateCatalog c=new CreateCatalog();
               
               c.makeCatalog();
                  
     }
     public void makeCatalog(){
        try {
            LinkedHashMap<String,String> masterCatalog= new LinkedHashMap();
             ArrayList currentCatalog= new ArrayList();

                  
            MatchThePrice m=new MatchThePrice();
            m.readAndFillFileContents();;
            String header="";
            
            String fileNameInDropbox="C:\\Google Drive\\Dropbox\\program\\catalog\\working\\input_catalog.txt";
            if(new File(fileNameInDropbox).exists()==false){
                System.out.println("[ CreateCatalog ] Input file does not exist : " + fileNameInDropbox);
                return;
            }
              HashMap <String,String> asinPkgsQtyMap=new  HashMap <String,String> ();
              Connection con = null;
            try{
                 con= bombayplaza.pricematch.RetrievePage.getConnection();
                PreparedStatement ps=con.prepareStatement("select asin,packing_qty from tajplaza.final_packingqty ");
                ResultSet rs=ps.executeQuery();
                
                while(rs.next()){
                     asinPkgsQtyMap.put(rs.getString(1),rs.getString(2));
                }
                con.close();;
                
            }catch (Exception e4){e4.printStackTrace();
            
                if(con!= null && !con.isClosed()) {
                    con.close();
                }
            }
            
            /*
                    BufferedReader br000 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\catalog\\masterCatalog_PackingQty.txt")));
                  String strLine0;
                     br000.readLine();
                  
                    while ((strLine0 = br000.readLine()) != null)   {
                        if(strLine0.split("\t").length<3)
                            continue;
                        String asin=strLine0.split("\t")[1];
                        String pkgQty=strLine0.split("\t")[2];
                        asinPkgsQtyMap.put(asin,pkgQty);
                    }

                 br000.close();
                 */
            
            
            String fileName="C:\\Temp2012\\working\\input_catalog.txt";
            //Change the file so it has 11 columns
            FileWriter fstream_out = new FileWriter(fileName);
            BufferedWriter out = new BufferedWriter(fstream_out);

            
              BufferedReader br0 = new BufferedReader(new
                    InputStreamReader( new FileInputStream(fileNameInDropbox)));
              
              String s0;

              BufferedReader brTemplate = new BufferedReader(new
                    InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\catalog\\input_catalog.txt")));
              
              String expectedHeader=brTemplate.readLine();
              if(expectedHeader.split("\t").length<2)
                  expectedHeader=brTemplate.readLine();
              Integer expectedHeaderTabCount=StringUtils.countMatches(expectedHeader,"\t");
              
              s0=br0.readLine();
              if(expectedHeader.trim().equals(s0.trim())==false){
                  System.out.println("[ CreateCatalog ] The header is not as same as expected as in C:\\Google Drive\\Dropbox\\program\\catalog\\input_catalog.txt. Please make sure it is in same format");
                  return;
              }
              //now skip header from input_catalog
             // br0.readLine();
               
              out.write(expectedHeader);
              out.newLine();  
               
            while ((s0 = br0.readLine()) != null)   {
                if(s0.trim().length()==0)
                    continue;
                Integer curTabCount=StringUtils.countMatches(s0,"\t");
                for(int i=curTabCount;i<expectedHeaderTabCount;i++)
                    s0=s0+"\t"+"NA";
                out.write(s0);
                out.newLine();
            }
            br0.close();
            out.close();
              
            
            //FileUtils.copyFile(new File(fileNameInDropbox), new File(fileName));
            
            String outputfileName=fileName.replaceAll("input","output");
            
            if(new File(fileName).exists()==false){
                System.out.println("[ CreateCatalog ] Input file does not exist : " + fileName);
                return;
            }
                   
            if(PRODUCTION.equals("true") && new File(fileName.replaceAll("input","output")).exists() ){
                System.out.println("[ CreateCatalog ] output file already exists in C:\\Temp2012\\working folder. I think there is another program running.. Wait for it to get over.");
                 return;
            }            
            
             br0 = new BufferedReader(new
                    InputStreamReader( new FileInputStream(fileName)));
            String originalStr,currentStr;
            //sortFile( outputfileName,13,false);
             header=br0.readLine();
  
           originalHeader=header;
           int tabCount=StringUtils.countMatches(originalHeader, "\t");
            try{
           if(writeToMasterCatalog)
                FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\program\\catalog\\reference_masterCatalog.txt"),new File( "C:\\Google Drive\\Dropbox\\program\\catalog\\masterCatalog.txt"));              
            Long t=System.currentTimeMillis();
            FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\program\\catalog\\masterCatalog.txt"),new File( "C:\\Google Drive\\Dropbox\\program\\catalog\\backup\\masterCatalog"+t+".txt"));              
            FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\program\\catalog\\working\\input_catalog.txt"),new File( "C:\\Google Drive\\Dropbox\\program\\catalog\\backup\\input_catalog"+t+".txt"));              
              BufferedReader  br00 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\catalog\\masterCatalog.txt")));
              String strLine0;
              while ((strLine0 = br00.readLine()) != null)   {
                  if(StringUtils.countMatches(strLine0,"\t")<2)
                      continue;
                 String asin=strLine0.split("\t")[tabCount+2];

                  if(strLine0.trim().length()>0)
                      masterCatalog.put(asin,strLine0);
              }
              br00.close();
          }catch (Exception e){
              e.printStackTrace();
              // Mail.emailException(CreateCatalog.class,e);
          }           
           
         //  sortCatalogFile(tabCount);
           originalHeader="";
           int asinColumn=-1;
           for(int t1=0;t1<tabCount;t1++){
               originalHeader+="\t";
               if(asinColumn==-1 && header.split("\t")[t1].toLowerCase().indexOf("asin")>=0)
                   asinColumn=t1;
           }
            String arr[]=header.split("\t");
            Integer rank_column=(arr.length-1)+6;
             
             fstream_out = new FileWriter(outputfileName);
             out = new BufferedWriter(fstream_out);
            header+="\ttitle\tasin\tmanu\tsoldby\tbestprice\trank\tcategory\texistsinmyinventory\tEverExistedInMyInventory\tPacking Qty\tNumber of FBA sellers\toriginalCostOf1Asin\tfbaFees\tfbaMargin\tprofit\tprofit percentage\tmy units\tweeks covered\treq ASINS(multiply by 3 if it is pack of 3 to get how many units)(Also, minus units in hand)\tCases Required\tPrice\tNo Other Fba seller Present\tsort key=profit*req asins\tItems Sold In Last Month\tweight\tCustomer Review\tHBG Profit\t3 pack profit\t6 packs profit\tAmazon selling current ASIN in last 3 months in camelcamel\tAmazon selling any variation ASIN of the current upc in last 3 months in camelcamel\tAmazon selling any variation of the ASIN right now in amazon";
            out.write(header);
            out.newLine();
            out.close();
            int productCount=0;
            int curRowIdx=1;
            String curRow="CUR_ROW";
           // FileUtils.copyFile(new File(outputfileName), new File("C:\\Temp2012\\output_catalog.txt"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MMM");
                Date date = new Date();
                String today=dateFormat.format(date);
                String originalUPC="";
            
            while ((originalStr= br0.readLine()) != null)   {
                System.out.println("[ CreateCatalog ]");
                System.out.println("[ CreateCatalog ] Processing record number : " + productCount);
                try{
                    if(productCount>0 && productCount%MAXROWS_COUNT==0){
                        System.out.println("[ CreateCatalog ] Product count reacched multiple of "+MAXROWS_COUNT+"..Sleep for "+MAXROWS_SLEEP_TIME+" minutes....");
                        Thread.sleep(MAXROWS_SLEEP_TIME*60*1000);
                    }
                    if(originalStr==null||originalStr.trim().length()==0)
                        continue;
      
                    StringBuffer trimmeOoriginalStr=new StringBuffer();
                    int i=0;
                    for(i=0;i<expectedHeaderTabCount && i<originalStr.split("\t").length;i++){
                        trimmeOoriginalStr.append(originalStr.split("\t")[i]);
                        trimmeOoriginalStr.append("\t");
                    }
                     for(int j=i;j<expectedHeaderTabCount;j++){
                         
                         trimmeOoriginalStr.append("\t");
                     }
                    originalStr=trimmeOoriginalStr.toString();
                                String upc=originalStr.split("\t")[0];
                                String tempUPC = new String(upc);
                                upc=upc.trim();
                                
                                if(upc==null||upc.length()<5 || upc.indexOf("NOT POSSIBLE")>=0)
                                    continue;
                                System.out.println("[ CreateCatalog ] Current line from input catalog:"+originalStr);
         
                                    
                                WHOLESALER_NAME=originalStr.split("\t")[6];
                                upc=upc.replaceAll("UPC:","");
                                upc=upc.replaceAll("upc:","");
                                upc=upc.replaceAll("-","");
                                upc=upc.replaceAll("'","");
                                upc=upc.replaceAll(" ","");
                                upc=upc.trim();
                                if(upc.length() ==11) {
                                    System.out.println("[ CreateCatalog ] Before check digit " + upc);
                                    System.out.println("[ CreateCatalog ] Now before adding check digit. Let us check if adding leading Zero gives any result? If yes the no need to add check digit");
                                    ArrayList lst = getUPCDetails1("0"+upc);
                                    if(lst != null && !lst.isEmpty()) {
                                          upc= "0"+upc; 
                                    } else {
                                        upc  = addCheckDigit(upc);
                                    }
                                    
                                    System.out.println("[ CreateCatalog ] after check digit " + upc);
                                }
                                originalUPC=upc;
                                
                                // Added logic here to put 12 digit new upc into output catalog file. Just simple repaced old upc with new.
                                System.out.println("[ CreateCatalog ] originalStr " + originalStr);
                                if(tempUPC.toLowerCase().contains("upc:"))
                                    
                                    originalStr = originalStr.replaceAll(tempUPC, "UPC:"+upc);
                                else
                                    originalStr = originalStr.replaceAll(tempUPC, upc);
                                System.out.println("[ CreateCatalog ] after originalStr " + originalStr);
                                if(upc.length()>13)
                                    upc=upc.substring(1,14);
                                if(asinColumn!=-1 && originalStr.split("\t").length>asinColumn &&  originalStr.split("\t")[asinColumn]!=null &&  originalStr.split("\t")[asinColumn].length()>0)
                                    upc=originalStr.split("\t")[asinColumn];
                                else if(writeToMasterCatalog){
                                    System.out.println("[ CreateCatalog ] For writing to master catlaog, I need first time asin..Otherwise, continue");
                                    continue;
                                }
                                if(UPCsProcessed.contains(upc))
                                    continue;
                                UPCsProcessed.add(upc);
                                  boolean processUPC=true;
                                 try{
                                     if(CLEANRUN ==false){
                                             System.out.println("[ CreateCatalog ] Lets see if UPC :"+upc+" exists in final_outputcatalog or final_outputcatlaogprofit");
                                            Connection con2= bombayplaza.pricematch.RetrievePage.getConnection();
                                            PreparedStatement ps2=con2.prepareStatement(" select asin,rank from tajplaza.final_outputcatalog_noprofit where lpad(replace(replace(replace(replace(upc,'\\'',''),' ',''),'-',''),'UPC:',''),14,'0')= lpad(replace(replace(replace(replace(?,'\\'',''),' ',''),'-',''),'UPC:',''),14,'0')   and  updation_date > (curdate() - interval 180  day)"
                                                    + " union "
                                                    + "select asin,rank from tajplaza.final_outputcatalog  where lpad(replace(replace(replace(replace(upc,'\\'',''),' ',''),'-',''),'UPC:',''),14,'0')= lpad(replace(replace(replace(replace(?,'\\'',''),' ',''),'-',''),'UPC:',''),14,'0')   and  updation_date > (curdate() - interval 180  day)  ");
                                            ps2.setString(1, upc);
                                            ps2.setString(2, upc);
                                            ResultSet rs2=ps2.executeQuery();

                                            while(rs2.next()){
                                                processUPC=false;
                                                String asin=rs2.getString(1);
                                                Integer rank=rs2.getInt(2);
                                                System.out.println("[ CreateCatalog ] Existing asin details : "+asin +" , rank "+rank);
                                                if(rank<100000){
                                                    processUPC=true;
                                                    break;
                                                }
                                            }
                                            rs2.close();
                                            ps2.close();
                                            con2.close();
                                         
                                     }


                                 }catch (Exception e3){
                                     e3.printStackTrace();
                                 }
                                 if(processUPC == false){
                                     System.out.println("[ CreateCatalog ] All asins of UPC :"+upc+" exists in final_outputcatalog_noprofit  have rank  more than 100000. So not processing this UPC");
                                     continue;
                                 }
                                 try{
                                     if(CLEANRUN ==false){
                                             System.out.println("[ CreateCatalog ] Lets see if UPC :"+upc+" exists in final_outputcatalog or final_outputcatlaogprofit in LAST 14 days");
                                            Connection con2= bombayplaza.pricematch.RetrievePage.getConnection();
                                            PreparedStatement ps2=con2.prepareStatement(" select asin,rank from tajplaza.final_outputcatalog  where lpad(replace(replace(replace(replace(upc,'\\'',''),' ',''),'-',''),'UPC:',''),14,'0')= lpad(replace(replace(replace(replace(?,'\\'',''),' ',''),'-',''),'UPC:',''),14,'0')   and  updation_date > (curdate() - interval 14  day)");
                                            ps2.setString(1, upc);
                                            ResultSet rs2=ps2.executeQuery();
                                            if(rs2.next()==false){
                                                System.out.println("[ CreateCatalog ] UPC was not processed and not found in both final_outputcatalog and final_outputcatalog_noprofit in last 14 days. So lets process it ");
                                                processUPC=true;
                                            }
                                         
                                            rs2.close();
                                            ps2.close();
                                            con2.close();
                                         
                                     }


                                 }catch (Exception e3){
                                     e3.printStackTrace();
                                 }
                                 if(processUPC == false){
                                     System.out.println("[ CreateCatalog ] UPC :"+upc+" exists in final_outputcatalog or final_outputcatalog_noprofit in last 14 days. So not processing it again");
                                     continue;
                                 }                                 
                                 productCount++;
                                ArrayList items=getUPCDetails1(upc);
                                /* if(items.size()==0){
                                      BufferedReader  brNA = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Temp2012\\itemsNOTfoundonamazon.txt")));
                                      String s1; 
                                      boolean itemAlreadyEntered=false;
                                      while ((s1 = brNA.readLine()) != null)   {
                                           if(s1.indexOf(upc)>=0)
                                               itemAlreadyEntered=true;
                                               
                                       }
                                      if(itemAlreadyEntered==false){
                                            FileWriter fstream_outNA = new FileWriter("C:\\Temp2012\\itemsNOTfoundonamazon.txt",true);
                                            BufferedWriter outNA = new BufferedWriter(fstream_outNA);
                                            outNA.write(originalStr);
                                            outNA.newLine();
                                            outNA.close();
                                          
                                      }
                                }*/
                                if(items.size()>0){
                                  
                                    int lowRank=99999999;
                                    ArrayList<String > upcSoldByAmazon = new ArrayList<String>();
                                    ArrayList<String > stringListToWriteInFile = new ArrayList<String>();
                                    boolean amazonsellinganyvriationofasinlast3month = false;
                                    boolean amazonsellinganyvriationofasinlast = false;
                                    for(int x=0;x<items.size();x++){
                                        currentStr=originalStr;
                                      try{     
                                         
                                        HashMap hm=(HashMap) items.get(x);
                                        
                                         String title=(String)(hm.get("title")!=null?hm.get("title"):"NA");
                                    
                                         String manu=(String)(hm.get("manu")!=null?hm.get("manu"):"NA");
                                         String manu_url=(String)(hm.get("manu_url")!=null?hm.get("manu_url"):"NA");
                                         String soldby= (String)(hm.get("soldby")!=null?hm.get("soldby"):"NA") ;
                                         String ANYONEBYAMAZON = (String)( hm.get("ANYONEBYAMAZON")!= null? hm.get("ANYONEBYAMAZON") : "");
                                         if(ANYONEBYAMAZON.equalsIgnoreCase("Y")) {
                                            upcSoldByAmazon.add(upc);
                                            System.out.println("[ CreateCatalog ] This item is sold by amazon");
                                            amazonsellinganyvriationofasinlast=true;
                                         } else {
                                            System.out.println("[ CreateCatalog ] This item is not sold by amazon");
                                         }
                                        float bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
                                         String asin=(String)(hm.get("asin")!=null?hm.get("asin"):"NA");
                                        if(bestprice==0 && masterCatalog.containsKey(asin)){
                                            System.out.println("[ CreateCatalog ] Skipping item:"+title+" since the bestprice is 0 to preserve the old captured data");
                                            continue;
                                        }
                                        boolean panttryItem=(Boolean)(hm.get("PANTRY_ITEM")!=null?hm.get("PANTRY_ITEM"):false);
                                        if(panttryItem){
                                            System.out.println("[ CreateCatalog ] Skipping item:"+title+" since it is pantry item");
                                            continue;
                                        }                                        
                                          String rank=(String)(hm.get("rank")!=null?hm.get("rank"):"999999");
                                          
                                         System.out.println("[ CreateCatalog ] If rank < 80000 and Current Seller is Not Amazon. Then Visit Camel");
                                         String append = "\t";
                                         if(rank != null) {
                                             try {
                                                 if(Integer.parseInt(rank) < 80000   ) {
                                                     if(soldby.toLowerCase().contains("amazon".toLowerCase())){
                                                                    append += "Y" + "\t";
                                                                    amazonsellinganyvriationofasinlast3month=true;
                                                         
                                                     }else{
                                                                System.out.println("[ CreateCatalog ] Visting CamelCamelCamel to deternime lowest price");
                                                                boolean strLowestPrice = CamelCamelCamel.isAmazonSellingInLast3Month(asin);
                                                                System.out.println("[ CreateCatalog ] Price returned from CamelCamelCamel : " + strLowestPrice);
                                                                if(!strLowestPrice) {
                                                                   append += "N" + "\t";
                                                                } else {
                                                                    append += "Y" + "\t";
                                                                    amazonsellinganyvriationofasinlast3month=true;

                                                                }
           //                                                     if(amazonsellinganyvriationofasinlast3month) {
           //                                                        append += "Y" + "\t";
           //                                                     } else {
           //                                                        append += "N" + "\t";
           //                                                     }
                                                                System.out.println("[ CreateCatalog ] strLowestPrice : " + strLowestPrice);
           //                                                     System.out.println("[ CreateCatalog ] Now check for any form of ASIN is sell by Amazon?");
           //                                                     String anyotherformsold = upcSoldByAmazon.contains(upc)?"Y":"N";
           //                                                     append += anyotherformsold + "\t";
                                                         
                                                     }
                                                 } else {
                                                    append += "NA\t";
                                                 }
                                             } catch(Exception e) {
                                                 System.out.println("[ CreateCatalog ] Exception while trying to determine rank. " + e.getMessage());
                                                 append += "NA\t";
                                             }
                                         } else {
                                               append += "NA" + "\t";
                                         }
                                          String NOOTHERFBASELLER=(String)(hm.get("NOOTHERFBASELLER")!=null?hm.get("NOOTHERFBASELLER"):"NA");
                                         if(manu_url.equals("NA")==false && manuUrlsProcessed.contains(manu_url)==false && manuUrlsToBeProcessed.contains(manu_url)==false){
                                             manuUrlsToBeProcessed.add(manu_url);
                                         }
                                         ASINSProcessed.add(asin);
                                          String category=(String)(hm.get("category")!=null?hm.get("category"):"NA");
                                          if(
                                                   category.indexOf("Industrial")>=0
                                                   //||category.indexOf("Appliances")>=0
                                                  )
                                          {
                                              try{rank=String.valueOf(Integer.valueOf(rank)*10);}catch (Exception e2){e2.printStackTrace();}
                                          }
                                              String VARIATION=(String)(hm.get("VARIATION")!=null?hm.get("VARIATION"):"NA");
                                           String CustomerReviewCount=(String)(hm.get("CustomerReviewCount")!=null?hm.get("CustomerReviewCount"):"NA");
                                          Integer numOfFbaSellers=(Integer)(hm.get("NUMBEROFFBASELLERS")!=null?hm.get("NUMBEROFFBASELLERS"):1);;
                                          boolean asinExists=MatchThePrice.asinExistsInFBAWithMoreThanZeroQuantity(asin);
                                          boolean asinEverExisted=MatchThePrice.skuExistsinFBAFileWithZeroOrMoreQuantity(asin);
                                            if(soldby.indexOf("timely goods")<0 &&  NOOTHERFBASELLER.equals("Y")){
                                               bestprice=bestprice*(float)1.1; 
                                                bestprice=(float)(Math.round(bestprice*100))/100;
                                            }

                                          currentStr=originalStr+"\t"+title+"\t"+asin+"\t"+manu+"\t"+soldby+"\t"+bestprice+"\t"+rank+"\t"+category+"\t"+asinExists+"\t"+asinEverExisted;
                                          curRowIdx++;
                                          if(rank==null || rank.equals("null"))
                                              rank="999999";
                                          String weight="2";
                                          boolean itemExistsInHarrisburgstore=false;
                                          
                                          
                                          if(harrisburgItems.contains(StringUtils.leftPad(originalUPC,14,"0")))
                                              itemExistsInHarrisburgstore=true;
                                          if(asinExists || itemExistsInHarrisburgstore||writeToMasterCatalog || (rank.equals("NA") ==false && Integer.parseInt(rank)<80000)){
                                              /*if(VARIATION.contains("Y")){

                                                  System.out.println("ASIN:"+asin+" contains variation. Now I need the correct price..I can not use the price when MatchThePrice.JAVASCRIPTNONO set to true");
                                                    MatchThePrice m=new MatchThePrice();
                                                    m.JAVASCRIPTNONO=false;

                                                    HashMap productmap=m.getProductDetails(asin, true, false,false);                          
                                                    bestprice=(Float)(productmap.get("bestprice")!=null?productmap.get("bestprice"):(float)0);

                                              }*/
                                                System.out.println("Lets do profit calculation for best rank items");
                                                System.out.println("Lets see how many packs it is made of");
                                                int pkgQnt=1;
                                                try{
                                                    if(title.toLowerCase().indexOf("pack of")>=0){
                                                        String t=StringUtils.substringAfter(title.toLowerCase(), "pack of");
                                                        t=t.trim();
                                                        String nStr="";
                                                        for(int i2=0;i2<t.length();i2++){
                                                            if(i2>=2)
                                                                break;
                                                            if(t.charAt(i2) >='0' && t.charAt(i2) <='9'){
                                                                nStr+=t.charAt(i2);
                                                            }
                                                        }
                                                        if(nStr.length()>0)
                                                            pkgQnt=Integer.parseInt(nStr);


                                                    }
                                                }catch (Exception e){
                                                    e.printStackTrace();
                                                    //Mail.emailException(CreateCatalog.class,e);
                                                }
                                                
                                                try{
                                                    //Take it from masterCatalog_PackingQty file
                                                     if(asinPkgsQtyMap.containsKey(asin) && asinPkgsQtyMap.get(asin).equals("NA") ==false){
                                                         pkgQnt=Integer.parseInt(asinPkgsQtyMap.get(asin));
                                                     }
                                                }catch (Exception e){
                                                    e.printStackTrace();
                                                   // Mail.emailException(CreateCatalog.class,e);
                                                }
                                                
                                                String onepackPrice="99999";
                                                Float originalCostOf1Asin=(float)0;
                                                if(originalStr.split("\t").length>5 && originalStr.split("\t")[5].trim().length()>0){
                                                    onepackPrice=currentStr.split("\t")[5].replaceAll("\\$","");
                                                     originalCostOf1Asin =Float.valueOf(onepackPrice)*pkgQnt;

                                                }
                                                if(originalCostOf1Asin==0 && originalStr.split("\t").length>4 && originalStr.split("\t")[4].trim().length()>0){
                                                    String onecaseprice=currentStr.split("\t")[4].replaceAll("\\$","");
                                                    String packsin1case=currentStr.split("\t")[3].replaceAll("\\$","");
                                                    
                                                     originalCostOf1Asin =Float.valueOf(onecaseprice)/Float.valueOf(packsin1case)*pkgQnt;

                                                }

                                                        
 
                                                HashMap fbaMap=RetrievePage.getDetailsFromFbaCalculatorNewMWS(con,asin, String.valueOf(bestprice),true);
                                                String fbaMargin="0";
                                                String fbaFees="";
                                                Float profit=(float)0;
                                                Float profitPercent=(float)0;
                                                  
                                                if(fbaMap.containsKey("fbaMargin")){
                                                    fbaMargin=String.valueOf(fbaMap.get("fbaMargin"));
                                                    fbaFees=String.valueOf(fbaMap.get("fbaFees"));
                                                    weight=String.valueOf(fbaMap.get("productInfoWeight")  );
                                                    //if(weight==null || weight.equals("null")==false)
//                                                        weight="2";
                                                    if(Float.valueOf(fbaFees)==0)
                                                        fbaFees="10";
                                                        if(onepackPrice.equals("99999")==false){
                                                        profit=(Float.valueOf(fbaMargin)-Float.valueOf(originalCostOf1Asin));
                                                        //reducing $2 for labor work

                                                        profit=Math.round(profit*(float)100)/(float)100;
                                                        profitPercent=(profit/originalCostOf1Asin)*100;
                                                        profitPercent=Math.round(profitPercent*(float)100)/(float)100;
                                                    }

                                                }
                                                  Integer itemsNeeded=0;
                                                  Integer myUnits=0;
                                                   Float casesNeeded=(float)0;
                                                   Float price=(float)0;
                                                     Integer packsInCase=  1;
                                                  itemsNeeded=MatchThePrice.getReqUnits(  asin,  rank,  numOfFbaSellers ,m );
                                                      myUnits=MatchThePrice.getMyTotalAsins(asin,   m);    
                                                      //myUnits=myUnits*pkgQnt;
                                               if(m.asinExistsInFBAWithMoreThanZeroQuantity(asin)==false  && soldby.indexOf("timely goods")<0 )
                                                        //i do not have it..so i would be joining the group to divde the profit.
                                                        numOfFbaSellers=numOfFbaSellers+1;   
                                               if(numOfFbaSellers==0)
                                                   numOfFbaSellers=1;
                                                if(profit>1 && profitPercent>5 && rank.equals("NA") ==false && Integer.valueOf(rank)<40000){
     
    
                                                    Float oneCasePrice=(float)1;
                                                    try{
                                                         if(currentStr.split("\t")[3].length()>0)
                                                                 packsInCase=Integer.valueOf(currentStr.split("\t")[3].replaceAll("\\$",""));
                                                          if(currentStr.split("\t")[4].length()>0)
                                                                  oneCasePrice=Float.valueOf(currentStr.split("\t")[4].replaceAll("\\$",""));
                                                    }
                                                    catch (Exception e3){e3.printStackTrace();}
                                                   casesNeeded= Float.valueOf(itemsNeeded*pkgQnt) /packsInCase;
                                                    casesNeeded=casesNeeded+(casesNeeded%5);
                                                    price=(float)casesNeeded*(float)oneCasePrice;

                                                                                                            
                                                }
                                                String weeksCovered="NA",soldInLastMonth="0" ;
                                               ArrayList inventoryHealth_Contents= m.inventoryHealth_Contents;
                                               for(int t1=0;t1<inventoryHealth_Contents.size();t1++){
                                                   String s1=(String) inventoryHealth_Contents.get(t1);
                                                   if(s1.indexOf(asin)>=0){
                                                       weeksCovered=s1.split("\t")[23];
                                                       soldInLastMonth=s1.split("\t")[18];
                                                      
                                                   }
                                               }
                                                String[] columnIndices={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS"};
                                                String soldbyIdx=columnIndices[ (tabCount+4)];
                                                String bestPriceIdx=columnIndices[ (tabCount+5)];
                                                String rankIdx=columnIndices[ (tabCount+6)];
                                                String packingQtyIdx=columnIndices[ (tabCount+10)];
                                                String numOfFBASellersIdx=columnIndices[ (tabCount+11)];
                                                String originalCostOf1AsinIdx=columnIndices[ (tabCount+12)];
                                                 
                                                String fbaFeesIdx=columnIndices[ (tabCount+13)];
                                                String fbaMarginIdx=columnIndices[ (tabCount+14)];
                                                String profitIdx=columnIndices[ (tabCount+15)];
                                                String profitPercentageIdx=columnIndices[ (tabCount+16)];
                                                
                                                String myUnitsIdx=columnIndices[ (tabCount+17)];
                                                String weeksCoveredIdx=columnIndices[ (tabCount+18)];
                                                String reqAsinsIdx=columnIndices[ (tabCount+19)];
                                                String reqCasesIdx=columnIndices[ (tabCount+20)];
                                                String priceIdx=columnIndices[ (tabCount+21)];
                                                 String noOtherSellerIdx=columnIndices[ (tabCount+22)];
                                                 String sortKeyIdx=columnIndices[ (tabCount+23)];
                                                 String itemsSoldLastMonthIdx=columnIndices[ (tabCount+24)];
                                                 String wtIdx=columnIndices[ (tabCount+25)];
                                                 String reviewIdx=columnIndices[ (tabCount+26)];
                                                 String hbgProfitIdx=columnIndices[ (tabCount+27)];

                                                 
                                                 String threepckIdx=columnIndices[ (tabCount+28)];
                                                 String sixpackIdx=columnIndices[ (tabCount+29)];
                                                 
                                                  
                                                 
                                                String originalCostOf1AsinS="=ROUND("+packingQtyIdx+curRow+"*E"+curRow+"/"+"D"+curRow+",2)";
                                                 String fbaMarginS="=ROUND("+bestPriceIdx+curRow+"*0.85-"+fbaFeesIdx+curRow+",2)";
                                                 String profitS="="+fbaMarginIdx+curRow+"-"+originalCostOf1AsinIdx+curRow;
                                                //if(NOOTHERFBASELLER.equals("Y")==false||soldby.indexOf("timely goods")>=0)
                                                    //profitS=profitS+"-2";
                                                //else
                                                    profitS=profitS+"-"+MatchThePrice.MY_FBA_LABOR_CHARGE;
                                                String profitPercentS="=ROUND(100*"+profitIdx+curRow+"/"+originalCostOf1AsinIdx+curRow+",2)";
                                                if(numOfFbaSellers==0)
                                                    numOfFbaSellers=1;
                                                
                                               // String myUnitsStr="="+myUnits+"*"+packingQtyIdx+curRow;
                                              //  String itemsNeededS="=IF(AND("+profitPercentageIdx+curRow+">12,"+profitIdx+curRow+">1,"+soldbyIdx+curRow+"<>\"amazon\"),CEILING(600000/("+numOfFBASellersIdx+curRow+"*"+rankIdx+curRow+"),1)-"+myUnitsIdx+curRow+",0)";
                                                 String itemsNeededS ="=IF(AND("+profitPercentageIdx+curRow+">5,"+profitIdx+curRow+">1,"+soldbyIdx+curRow+"<>\"amazon\"),IF( ("+itemsSoldLastMonthIdx+curRow+"*1) > 1,("+itemsSoldLastMonthIdx+curRow+"*2    -"+myUnitsIdx+curRow+"),(CEILING(1200000/("+numOfFBASellersIdx+curRow+"*"+rankIdx+curRow+"),1)-"+myUnitsIdx+curRow+")),0)";
                                             //   if(rank.equals("999999") && Integer.valueOf(soldInLastMonth)>=0)
                                                 //   itemsNeededS="="+String.valueOf(Integer.valueOf(soldInLastMonth)*2)+"-"+myUnitsIdx+curRow;
                                                String casesNeededS="=IF("+reqAsinsIdx+curRow+"*"+packingQtyIdx+curRow+">0,CEILING("+packingQtyIdx+curRow+"*"+reqAsinsIdx+curRow+"/D"+curRow+",1)-MOD(CEILING("+packingQtyIdx+curRow+"*"+reqAsinsIdx+curRow+"/D"+curRow+",1),5)+5,0)";
                                                
                                                 String priceS="="+reqCasesIdx+curRow+"*E"+curRow;
                                                 String sortKeyS="=round(if("+reqCasesIdx+curRow+">0,"+reqAsinsIdx+curRow+"*"+profitIdx+curRow+"*"+profitPercentageIdx+curRow+"/100"+",0),2)";
                                                Float review=(Float) hm.get("review");
                                                String shippingChargeS="IF("+wtIdx+curRow+"<=1,3,IF(AND("+wtIdx+curRow+">1,"+wtIdx+curRow+"<=2),6,CEILING("+wtIdx+curRow+",1)*1.5))";
                                                String hbg_profit="=ROUND("+bestPriceIdx+curRow+"*"+CreateItem.AMAZON_PRICE_DISCOUNT_FOR_CREATECATALOG+"-"+originalCostOf1AsinIdx+curRow+"*"+CreateItem.MY_PROFIT_PERCENT+"-"+shippingChargeS+",2)";
                                                //currentStr+="\t"+weight+"\t"+review+"\t"+hbg_profit;
                                                if(weight==null || weight.equals("null"))
                                                weight="2";
                                                
                                                try{
                                               // if(Float.valueOf(weight)<.5)
                                                 //   weight="2";
                                                }catch (Exception e3){
                                                    e3.printStackTrace();
                                                }
                                                String dynamicStr=pkgQnt+"\t"+numOfFbaSellers+"\t"+originalCostOf1AsinS+"\t"+fbaFees+"\t"+fbaMarginS+"\t"+profitS+"\t"+profitPercentS+"\t"+myUnits+"\t"+weeksCovered+"\t"+itemsNeededS+"\t"+casesNeededS+"\t"+priceS+"\t" +NOOTHERFBASELLER+"\t"+sortKeyS+"\t"+soldInLastMonth+"\t"+weight+"\t"+review+"\t"+hbg_profit;
                                                
                                                currentStr=currentStr+"\t"+dynamicStr;
                                                
                                                if(Integer.parseInt(rank)<2000 && title.toLowerCase().indexOf("pack of")<0   ){
                                                    try{
                                                    System.out.println("Lets do project multi pack calculation");
                                                    Float productInfoWeight=(Float) fbaMap.get("productInfoWeight")!=null?(Float) fbaMap.get("productInfoWeight"):(float)1;
                                                    float packsNeeded=(float)3;
                                                    
                                                    float projectedProfit=(float) (bestprice*.9*packsNeeded*(float).85);
                                                    //For order handling, pick & pack
                                                    projectedProfit-=1;
                                                    projectedProfit=(float) (projectedProfit-(Math.ceil(productInfoWeight*packsNeeded)*(float).41));
                                                    projectedProfit=projectedProfit-(originalCostOf1Asin*packsNeeded);
                                                   //if(bestprice*.9*packsNeeded>100) 
                                                    //    projectedProfit=0;
                                                    currentStr=currentStr+"\t"+projectedProfit;
                                                    projectedProfit=(float)0;
                                                    if(Integer.parseInt(rank)<500){
                                                        packsNeeded=(float)6;
                                                         projectedProfit=(float) (bestprice*.8*packsNeeded*(float).85);
                                                      // if(bestprice*.8*packsNeeded>100) 
                                                        //  projectedProfit=0;                                                         
                                                       //For order handling, pick & pack
                                                        projectedProfit-=1;
                                                        projectedProfit=(float) (projectedProfit-(Math.ceil(productInfoWeight*packsNeeded)*(float).41));
                                                        projectedProfit=projectedProfit-(originalCostOf1Asin*packsNeeded);
                                                        
                                                    }
                                                            
                                                    currentStr=currentStr+"\t"+projectedProfit;                                                    
                                                    }catch (Exception e){
                                                        e.printStackTrace();
                                                        // Mail.emailException(CreateCatalog.class,e);
                                                    }
                                                    
                                                }else{
                                                    currentStr=currentStr+"\tNA\tNA";
                                                }
                                            //if( profit>0 || asinExists)     
                                                
                                            
                                     
                                            currentCatalog.add(currentStr);
                                          }
           
                                       if(writeToMasterCatalog)
                                                 masterCatalog.put(asin,currentStr+"\t"+today);                           
                                      
                                      currentStr=currentStr.replaceAll(curRow, String.valueOf(curRowIdx));
                                      currentStr += append;
                                      System.out.println("[ CreateCatalog ] final String " + currentStr);
                                    }catch (Exception e0){
                                        e0.printStackTrace();
                                       // Mail.emailException(CreateCatalog.class,e0);
                                        
                                    }

                                    stringListToWriteInFile.add(currentStr);
                                  
//                                    fstream_out = new FileWriter("C:\\Temp2012\\working\\output_catalog.txt",true);
//                                    out = new BufferedWriter(fstream_out);
//                                    out.write(currentStr);
//                                    out.newLine();
//                                    out.close();                

                              }
                                     System.out.println("[ CreateCatalog ] Now here all of ASIN rows are created in stringListToWriteInFile arraylist");
                                     System.out.println("[ CreateCatalog ] We will iterate each list and check in upcSoldByAmazon weather upc exist in this arraylist for current row");
                                     System.out.println("[ CreateCatalog ] If yes then put Y at the end else put N");
                                     fstream_out = new FileWriter("C:\\Temp2012\\working\\output_catalog.txt",true);
                                     out = new BufferedWriter(fstream_out);
                                      
                                     for(String string :stringListToWriteInFile ) {
                                         System.out.println("[ CreateCatalog ] Current Row : " + string);
                                         String upc1=string.split("\t")[0];
                                         Integer cols=string.split("\t").length;
                                         System.out.println("Number of columns : "+cols);
                                             
                                         upc1=upc1.trim();
                                         upc1=upc1.replaceAll("UPC:","");
                                         upc1=upc1.replaceAll("upc:","");
                                         upc1=upc1.replaceAll("-","");
                                         upc1=upc1.replaceAll("'","");
                                         upc1=upc1.replaceAll(" ","");
                                         upc1=upc1.trim();
                                        if(upc1.length() ==11) {
                                           upc1  = addCheckDigit(upc1);
                                        }
                                        System.out.println("[ CreateCatalog ] amazonsellinganyvriationofasin : " + amazonsellinganyvriationofasinlast3month);
                                         if(cols>25){

                                            if(amazonsellinganyvriationofasinlast3month) {
                                                string+="Y\t";
                                            } else {
                                                string+="N\t";
                                            }                                             
                                         

                                        System.out.println("[ CreateCatalog ] upc1 : " + upc1);
                                        //if(upcSoldByAmazon.contains(upc1)) {
                                            if(amazonsellinganyvriationofasinlast) {
                                            
                                            System.out.println("[ CreateCatalog ] UPC : " + upc1 + " is sold by amazon ");
                                            string+="Y";
                                        } else {
                                            System.out.println("[ CreateCatalog ] Amazon is not selling any variation of this UPC : " + upc1);
                                            string+="N";
                                        }
                                      }
                                        out.write(string);
                                        out.newLine();
                                     }
                                     out.close();                
                        }
             }catch (Exception e){
                        e.printStackTrace();
                       // Mail.emailException(CreateCatalog.class,e);
                    }                
                
            }
            br0.close();
            
            if(writeToMasterCatalog){
                Iterator iter=masterCatalog.keySet().iterator();
                fstream_out = new FileWriter("C:\\Google Drive\\Dropbox\\program\\catalog\\masterCatalog.txt");
                out = new BufferedWriter(fstream_out);
                int masterCatalogCurRow=1; 
                 ArrayList<String> existinginventoryFBA_Contents=m.existinginventoryFBA_Contents;

                while(iter.hasNext()){
                    
                    String key=(String) iter.next();
                   // if(key.equals("B001HTOZEA"))
                    //    System.out.println("debug");
                    String s=masterCatalog.get(key);
                    int headCount=StringUtils.countMatches(header,"\t");
                    int curCount=StringUtils.countMatches(s,"\t");

                    boolean putwholesalername=false;
                    boolean existedInMyInventory=false;
                    s=s.replaceAll(curRow, String.valueOf(masterCatalogCurRow));
                   masterCatalogCurRow++;
                     out.write(s);
                     out.newLine();

                }
                out.close();  
               if(writeToMasterCatalog)
                 FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\program\\catalog\\masterCatalog.txt"),new File( "C:\\Google Drive\\Dropbox\\program\\catalog\\reference_masterCatalog.txt"));
                
            }
                  
                   System.out.println("Value of DONOTPROCESS_MANUURLS INPUT is :"+DONOTPROCESS_MANUURLS);
                    if(DONOTPROCESS_MANUURLS.equals("false")){
                        try{

                            //LEts process manu URLs
                            for(int i=0;i<manuUrlsToBeProcessed.size();i++){
                                String manu_url=(String) manuUrlsToBeProcessed.get(i);

                                fstream_out = new FileWriter("C:\\Google Drive\\Dropbox\\program\\catalog\\manuUrlsProcessed.txt",true);
                                out = new BufferedWriter(fstream_out);
                                out.write(manu_url);
                                out.newLine();
                                out.close();                     

                                for (int pageN=1;pageN<=1;pageN++){
                                    String curPageManuUrl=manu_url.replaceAll("PAGENUM", String.valueOf(pageN));
                                    WebClient webClient1 = new WebClient(BrowserVersion.FIREFOX_38);
                                   webClient1.getOptions().setThrowExceptionOnScriptError(false);
                                   webClient1.getOptions().setJavaScriptEnabled(true);
                                   webClient1.getOptions().setCssEnabled(false);
                                    int collectedProducts=0;
                                    System.out.println("Opening url : "+curPageManuUrl);
                                    HtmlPage page = webClient1.getPage(curPageManuUrl);
                                    webClient1.waitForBackgroundJavaScript(10000);
                                    DomNodeList<DomElement> aTags = page.getElementsByTagName("a");
                                    System.out.println("Got page1. LEngth of aTags:"+aTags.size());

                                    for(int aN=0;aN<aTags.size();aN++){
                                        DomElement a=aTags.get(aN);
                                        HtmlAnchor al1=(HtmlAnchor)a;
                                        String asinUrl=al1.getHrefAttribute();
                                        if(asinUrl.indexOf("/gp/product/")<0 &&asinUrl.indexOf("/dp")<0 ){
                                            //System.out.println("/gp/product & /dp missing so skipping, url:"+asinUrl);
                                            continue;
                                        }
                                        String  asin="NA" ;
                                        if(asinUrl.indexOf("/gp/product") <0 &&asinUrl.indexOf("/dp") <0)
                                            continue;
                                        if(asinUrl.indexOf("/gp/product/")>=0)
                                            asin=StringUtils.substringBetween(asinUrl, "/gp/product/","/");
                                        else 
                                           asin=StringUtils.substringBetween(asinUrl, "/dp/","/");
                                        if(asin==null || asin.trim().length()==0)
                                            continue;
                                        if(ASINSProcessed.contains(asin)){
                                            System.out.println("ASIN is already processed:"+asin);
                                            continue;
                                        }
                                        if(StringUtils.countMatches(a.getParentNode().getParentNode().asText(),"Subscribe & Save")==1){
                                            System.out.println("No need to visit the product. Amazon has subscrive and save");
                                           continue;
                                        }
                                        System.out.println("Going to get product details for asin"+asin);
                                        collectedProducts++;
                                        MatchThePrice.JAVASCRIPTNONO=true;
                                        HashMap hm=MatchThePrice.getProductDetails(asin, false,false,false);

                                        String title=(String)(hm.get("title")!=null?hm.get("title"):"NA");
                                        String manu=(String)(hm.get("manu")!=null?hm.get("manu"):"NA");
                                        String soldby= (String)(hm.get("soldby")!=null?hm.get("soldby"):"NA") ;
                                       float bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
                                         String rank=(String)(hm.get("rank")!=null?hm.get("rank"):"999999");
                                        ASINSProcessed.add(asin);
                                         String category=(String)(hm.get("category")!=null?hm.get("category"):"NA");
                                         String VARIATION=(String)(hm.get("VARIATION")!=null?hm.get("VARIATION"):"NA");
                                         

                                         boolean asinExists=MatchThePrice.asinExistsInFBAWithMoreThanZeroQuantity(asin);
                                         currentStr=originalHeader+"\t"+title+"\t"+asin+"\t"+manu+"\t"+soldby+"\t"+bestprice+"\t"+rank+"\t"+category+"\t"+asinExists;
                                         if(Integer.valueOf(rank)<20000){
                                             fstream_out = new FileWriter("C:\\Temp2012\\working\\output_catalog.txt",true);
                                            out = new BufferedWriter(fstream_out);
                                            out.write(currentStr);
                                            out.newLine();
                                            out.close();                              
                                             
                                         }


                                    }
                                    if(collectedProducts<20){
                                        System.out.println("Not enough  products on page :"+pageN +" So stopping.");
                                        break;
                                    }

                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            //Mail.emailException(CreateCatalog.class,e);

                        }
                        
                    }
 
            
         // sortCatalogFile(tabCount);
         Long t=System.currentTimeMillis();
            FileUtils.copyFile(new File("C:\\Temp2012\\working\\output_catalog.txt"), new File("C:\\Google Drive\\Dropbox\\program\\catalog\\completed\\output_catalog_"+WHOLESALER_NAME+"_"+t+".txt"));
            FileUtils.copyFile(new File("C:\\Temp2012\\working\\output_catalog.txt"), new File("C:\\Google Drive\\Dropbox\\program\\catalog\\completed\\output_catalog.txt"));
            
            FileUtils.copyFile(new File("C:\\Temp2012\\working\\input_catalog.txt"), new File("C:\\Google Drive\\Dropbox\\program\\catalog\\completed\\input_catalog_"+WHOLESALER_NAME+"_"+t+".txt"));
          //  new File("C:\\Temp2012\\working\\input_catalog.txt").delete();
            new File("C:\\Temp2012\\working\\output_catalog.txt").delete();
            //new File("C:\\Temp2012\\output_catalog.txt").delete();
           new File("C:\\Temp2012\\working\\output_catalog-unsorted.txt").delete();
           // sortFile( outputfileName,rank_column,false);
            
          
           
 
                                     
            
        } catch (Exception ex) {
            ex.printStackTrace();
           // Mail.emailException(CreateCatalog.class,ex);
        }
        
     }
public static  void sortCatalogFile_OLD(Integer origColumns){
        //Put the rank as additional column in the existing-all-inventory file
        
        try{

          FileInputStream fstream0 = new FileInputStream("C:\\Temp2012\\working\\output_catalog.txt");
          // Get the object of DataInputStream
         DataInputStream  in0 = new DataInputStream(fstream0);
         BufferedReader  br0 = new BufferedReader(new InputStreamReader(in0));
         String s;
         ArrayList<String> wholeFileLines=new ArrayList();
         while ((s = br0.readLine()) != null)   {
             wholeFileLines.add(s);
         }
         br0.close();
         in0.close();
         
          FileInputStream fstream = new FileInputStream("C:\\Temp2012\\working\\output_catalog.txt");
          // Get the object of DataInputStream
         DataInputStream  in = new DataInputStream(fstream);
         BufferedReader  br = new BufferedReader(new InputStreamReader(in));
          
          TreeMap<Float, String> items=new TreeMap();
          
           String header=null;
           String asin,rank;
           String str;
           Integer  threepackProfitColumn=-1;
           Integer sixpackProfitColumn=-1;
           while ((str = br.readLine()) != null)   {
               if(header==null){
                   header=str;
                  
                   for(int t1=0;t1<header.split("\t").length;t1++){
                     if(header.split("\t")[t1].indexOf("3 pack profit")>=0)  
                         threepackProfitColumn=t1;
                     if(header.split("\t")[t1].indexOf("6 packs profit")>=0)  
                         sixpackProfitColumn=t1;
                     
                   }
                    continue;
               }
               
               Thread.sleep(100);
                Float keyF=1/Float.valueOf(System.currentTimeMillis()%1000000 );
               if(str.split("\t").length>origColumns+14){
                    String profit=str.split("\t")[origColumns+13];
                    String reqPacks=str.split("\t")[origColumns+15];
                    if(profit==null || profit.length()==0|| profit.trim().equals("null") || Float.valueOf(profit)==0)
                        profit="0."+String.valueOf(System.currentTimeMillis()%1000000);
                    if(reqPacks==null || reqPacks.length()==0|| reqPacks.trim().equals("null") || Float.valueOf(reqPacks)==0)
                        reqPacks="0."+String.valueOf(System.currentTimeMillis()%1000000);
                    
                    if(Float.valueOf(reqPacks)==0)
                        reqPacks="0.001";

                    Thread.sleep(100);
                   if(profit.indexOf(".")<0) profit=profit +"."+ System.currentTimeMillis()%1000000;
                   if(reqPacks.indexOf(".")<0) reqPacks=reqPacks +"."+ System.currentTimeMillis()%1000000;

                    keyF=Float.valueOf(reqPacks)*Float.valueOf(profit) ;
                    
               }

              
               
            
                if(items.containsKey(keyF)){
                    
                    System.out.println("ERROR::::::::::::how is it possible , items already content keyF:"+keyF +" items:"+items.get(keyF) +" current item:"+str);
                    throw new Exception ("Duplicate key values");
                }
                      
                
                if(str.split("\t").length>threepackProfitColumn && str.split("\t")[threepackProfitColumn]!=null &&str.split("\t")[threepackProfitColumn].length()>0 ){
                    String upc0=str.split("\t")[0];
                    boolean multiplePacksAlreadyExistsOnAmazon=false;
                    for(int t1=0;t1<wholeFileLines.size();t1++){
                        if(wholeFileLines.get(t1).indexOf(upc0)>=0){
                            if(wholeFileLines.get(t1).toLowerCase().indexOf("pack")>0){
                                multiplePacksAlreadyExistsOnAmazon=true;
                                 System.out.println("upc:"+upc0+" alread has multiple packs on amazon..so removing 3 pack and 6 packs profits");                                
                                break;
                            }
                        }
                    }
                    StringBuffer newStr=new StringBuffer();
                    if(multiplePacksAlreadyExistsOnAmazon){

                        for(int t2=0;t2<str.split("\t").length;t2++){
                            String ts=str.split("\t")[t2];
                            if(t2==threepackProfitColumn || t2==sixpackProfitColumn){
                               ts=" ";
                            }
                            newStr.append(ts);
                            newStr.append("\t");
                        }
                         str=newStr.toString();
                    }
                }
                
                items.put(keyF,str );
            }
          br.close();
          
         NavigableMap<Float,String>  itemsN= items.descendingMap();
        FileUtils.copyFile(new File("C:\\Temp2012\\working\\output_catalog.txt"),new File("C:\\Temp2012\\working\\output_catalog-unsorted.txt"));

        FileWriter writer = new FileWriter("C:\\Temp2012\\working\\output_catalog.txt");
        writer.write(header);
        writer.write('\n');

        for(String val : itemsN.values()){
                writer.write(val);
                writer.write('\n');
        }
        writer.close();


        }catch (Exception e){
            e.printStackTrace();
        }

    }
     
      public static String DEPARTMENT="aps";
      public static int vpnRetriedCount=0;
      public static boolean JAVASCRIPT=false;
      public static HtmlPage   getPage1(String url) throws Exception{
          HtmlPage page=null;
          iProxyRetryCount=0;
          while(page==null){
            try{
                page= RetrievePage.getPageInternal(url,true,JAVASCRIPT);
                
                if(page!=null && ERROR_MOVE_TO_NEXT_PAGE==false &&  page.getTitleText().toLowerCase().indexOf("robot check")>=0){
                     System.out.println("Amazon sent Robot Check in title");
                     page=null;
                       iProxyRetryCount++;
                       if(RetrievePage.prxies.length  >0 && iProxyRetryCount>RetrievePage.prxies.length/2  ){
                           System.out.println("[ CreateCatalog ] Exception : Number of proxies retry is exeeded RetrievePage.prxies.length/2. ");
                           System.exit(1);
                       }                    
                       System.out.println("[ CreateCatalog ] Current proxy did not worked. Trying another");
                     
                }
               /* if(page==null){
                    System.out.println("Got null page, but no exception");
                    break;
                }*/
            }catch (Exception e){
                if(ERROR_MOVE_TO_NEXT_PAGE)
                    return page;
                   if(e.getMessage() !=null && e.getMessage().contains("PROXY_DID_NOT_WORKED")) {
                       if(RetrievePage.USE_VPN){
                           if(vpnRetriedCount++>50){
                               System.out.println("Retried 50 times , proxy did not work. exiting");
                               System.exit(1);
                           }
                           System.out.println(" In CreateCatalog, PROXY DID NOT WORK");
                           MatchThePrice.restartVPN();
                           System.out.println("VPN Restarted");
                       }else{
                               iProxyRetryCount++;
                               if(RetrievePage.prxies.length  >0 && iProxyRetryCount>RetrievePage.prxies.length/2  ){
                                   System.out.println("[ CreateCatalog ] Number of proxies retry is exeeded RetrievePage.prxies.length/2. ");
                                   System.exit(1);
                               }                    
                               System.out.println("[ CreateCatalog ] Current proxy did not worked. Trying another");
                           
                       }
                   } else if(e.getMessage().contains("ASIN_NOT_FOUND")) {
                        System.out.println("[ CreateCatalog ] ASIN_NOT_FOUND");
                        throw e;
                   }   else{
                       throw e;
                   }          
            }              
          }

          return page;
           //getPage(url);
      }
      static int iProxyRetryCount=0;
     
         
    public ArrayList getUPCDetails1(String upc ) {
        System.out.println(MODULE+"Inside getUPCDetails. UPC : "+ upc);
        ArrayList jsarr=new ArrayList();
        if(upc==null || upc.length()==0) {
            System.out.println(MODULE+"UPC is passed null or zero length. Returning.");
            return  jsarr;
        }       
        String ret_string="";
        String url="http://www.amazon.com/s/?field-keywords=XXX";
         
        try {
           
            HashMap FbaPrice=new HashMap();
            String fba_asin, fba_sku,sku="NA",fba_fnsku;
            
            url=url.replaceAll("XXX",upc);
            System.out.println(MODULE+"Hitting URL (Trace 0):"+url );
            
                  
            ArrayList asins=new ArrayList();
            System.out.println(MODULE+"Value of upc is :"+upc);

            if(upc.indexOf("B")!=0){ 
                
                HtmlPage page = CreateCatalog.getPage1(url);
                ArrayList<DomElement> divs=new ArrayList();
                divs=(ArrayList<DomElement>) page.getByXPath("//div[@class='list results apsList']/div");
                System.out.println("[ CreateCatalog ] Vsize of div is :"+divs.size());
                
                if(divs.size()==0){
                    //for books
                    divs=(ArrayList<DomElement>) page.getByXPath("//div[@class='list results twister']/div");
                    if(divs.size()==0){
                    //SEE IF WE ARE ON PRODUCT DETAIL PAGE
                        return jsarr;  
                    }
                }
              
               for(int i=0;i<divs.size();i++){
                 if(i>5 || (i>0 && DEPARTMENT.equals("stripbooks")) )
                     break;
                 if(i>0)
                     break;
                 
                 String asin=divs.get(i).getAttribute("name");
                System.out.println("[ CreateCatalog ]  Adding ASIN : " + asin);
                asins.add(asin);
               }
              
        
               }else{
                   System.out.println("[ CreateCatalog ]  Adding UPC : " + upc);
                   asins.add(upc);
                   
               }
               String asin="";
             for(int i=0;i<asins.size();i++){
                 asin=(String) asins.get(i);
                 
                 if(asin==null || asin.length()==0  ){
                     continue;
                 }
                 System.out.println("[ CreateCatalog ]  Value of ASIN : " + asin);
                 //ASINSProcessed.add(asin);
                 String rank="", bestprice="";
                 if(asin!=null && asin.length()>0){
                    MatchThePrice m=new MatchThePrice();
                    m.JAVASCRIPTNONO=false;
            
                    boolean javascript=false;

                    try{
                        if(m.VariationEnableJS_Contents.contains(asin)){
                           ;// javascript=true;
                        }

                }catch (Exception e){e.printStackTrace();}  
                System.out.println("Sleeping for SLEEP_FOR_EACH_VISIT:"+SLEEP_FOR_EACH_VISIT)    ;
                if(SLEEP_FOR_EACH_VISIT>0)
                    Thread.sleep(SLEEP_FOR_EACH_VISIT*1000);
                   
                    HashMap productmap=m.getProductDetails(asin, true, javascript,false);
                        /*if(writeToMasterCatalog){
                            MatchThePrice.writeToMyInventory(asin, productmap, true,m);
                        }*/
                    
                    productmap.put("asin", asin);
                    jsarr.add(productmap);
                    try{
                        
                    
                    /*if(upc.indexOf("B")<0 &&   productmap.containsKey("varAsinArry") && ((ArrayList)productmap.get("varAsinArry")).size()>0){
                        ArrayList varAsinArry=(ArrayList)productmap.get("varAsinArry");
                       
                        for(int v=0;v<varAsinArry.size();v++){
                            //Problem..while searching by upc, the multi pack results are not cominng in search resullts..
                            //hence get multi pack items from one apck item asins, hit them also
                            String varAsin=(String) varAsinArry.get(v);
                             if(asins.contains(varAsin)==false){
                                    HashMap productmapV=m.getProductDetails(varAsin, true, false,false);

                                    productmapV.put("asin", varAsin);
                                    jsarr.add(productmapV);
                                 
                             }
                           
                        }
                    }  */                  
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                     
                 }
                 
                 if(bestprice==null || bestprice.length()==0)
                     bestprice="9999.9";
 
             }
         }catch (Exception e){
             e.printStackTrace();
            // Mail.emailException(CreateCatalog.class,e);
             
         }
         System.out.println(MODULE+"Returnin from getUPCDetails. Number of ASINs : " + jsarr.size() );
          return jsarr;
    }    
    public static ArrayList getJustUPCList(String upc ) {
        ArrayList jsarr=new ArrayList();
        String ret_string="";
        String url="http://www.amazon.com/s/?field-keywords=XXX";
         ArrayList asins=new ArrayList();
        try {
    
            HashMap FbaPrice=new HashMap();
            String fba_asin, fba_sku,sku="NA",fba_fnsku;
            
            url=url.replaceAll("XXX",upc);
             
 
                  
            if(upc.indexOf("B")!=0){
                HtmlPage page = CreateCatalog.getPage1(url);
                ArrayList<DomElement> divs=new ArrayList();
                divs=(ArrayList<DomElement>) page.getByXPath("//div[@class='list results apsList']/div");
               
                
                if(divs.size()==0){
                    //lets see if s-results-list-atf is found
                    //for books
                    divs=(ArrayList<DomElement>) page.getByXPath("//ul[@id='s-results-list-atf']/li");
                     System.out.println("[ InsertCostcoCatalog ] Vsize of div is :"+divs.size());
                    if(divs.size()==0){
                    //SEE IF WE ARE ON PRODUCT DETAIL PAGE
                        return jsarr;  
                    }
                }
  
                for(int i=0;i<divs.size();i++){
                 if(i>0)
                     break;
                String asin=divs.get(i).getAttribute("name");
                if(asin==null || asin.length()==0)
                    asin=divs.get(i).getAttribute("data-asin");
                System.out.println("ADDED ASIN:"+asin);
                        
                asins.add(asin);
               }
            }
        }catch (Exception e2){e2.printStackTrace();}
             return asins;   
    }    
    public  static String addCheckDigit(String upc)  {
        try{
            
        
        System.out.println("[ addCheckDigit ] UPC : " + upc);
        int [] input =  {0,0,0,0,0,0,0,0,0,0,0,0};
        int [] mul =  {3,1,3,1,3,1,3,1,3,1,3,0};
        int [] out =  {0,0,0,0,0,0,0,0,0,0,0,0};
        if(upc.length()==11) {
            for(int i=0;i<11;i++) {
                input[i] = Integer.parseInt(""+upc.charAt(i));
           }
        }
           for(int i=0;i<11;i++) {
                out[i]=input[i]*mul[i];
           }
        int sum=0;
        for(int i=0;i<11;i++) {
                sum+=out[i];
           }
        
        int chkdig = 10-(sum%10);
        if(chkdig ==10 ) chkdig=0;
        System.out.println("[ addCheckDigit ] Check Digit : " + chkdig);
        return (upc+chkdig);
    } catch(Exception e2)
    {e2.printStackTrace();}
       return upc;
    }
}

