/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bombayplaza.catalogcreation;

import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.RetrievePage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author US083274
 */
public class FixPackingQty {
    
    public static void main (String args[]) throws Exception{
             Connection con=RetrievePage.getConnection();
             PreparedStatement pst = null;
             ResultSet rs1 = null;
              ResultSet rs2 = null;
             pst=con.prepareStatement("select asin,title from tajplaza.final_outputcatalog where ( packing_qty is null or packing_qty = 'null' or length(trim(packing_qty))=0) ");            
             PreparedStatement pst2=con.prepareStatement(" update  tajplaza.final_outputcatalog set packing_qty=? where asin=?");            
              
              rs1=pst.executeQuery();   
              while(rs1.next()){
                  
              
                    int pkgQnt=1;
                    try{
                        String title=rs1.getString("title");
                        String asin=rs1.getString("asin");
                        System.out.println("Processing asin:"+asin);
                    if(title.toLowerCase().indexOf("pack of")>=0){
                    String t=StringUtils.substringAfter(title.toLowerCase(), "pack of");
                    t=t.trim();
                    String nStr="";
                    for(int i2=0;i2<t.length();i2++){
                        if(i2>=2)
                            break;
                        if(t.charAt(i2) >='0' && t.charAt(i2) <='9'){
                            nStr+=t.charAt(i2);
                        }
                    }
                    if(nStr.length()>0)
                        pkgQnt=Integer.parseInt(nStr);


                    }
                    pst2.setInt(1, pkgQnt);
                    pst2.setString(2, asin);
                    pst2.executeUpdate();
                    }catch (Exception e){
                    e.printStackTrace();
                    
                    }
              }
              if( !con.isClosed()) con.close();
    }
}
