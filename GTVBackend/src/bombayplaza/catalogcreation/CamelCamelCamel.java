package bombayplaza.catalogcreation;

import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlCheckBoxInput;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlHeading3;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTableDataCell;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
 


/**
 *
 * @author Raj
 */
public class CamelCamelCamel {
    public static void main(String [] args) {
        System.out.println("Inside main of CamelCamelCamel.");
        System.out.println(isAmazonSellingInLast3Month("B00B09EH72"));
//        System.out.println("Lowest Price : " + strLowest);
//        System.out.println();
//        strLowest= getLowestAmazonPriceCamel("B003H83SMY");
//        System.out.println("Lowest Price : " + strLowest);
//        System.out.println();
//        strLowest= getLowestAmazonPriceCamel("B004727NK2");
//        System.out.println("Lowest Price : " + strLowest);
//        System.out.println();
//        strLowest= getLowestAmazonPriceCamel("B007ZXBSGU");
//        System.out.println("Lowest Price : " + strLowest);
//        System.out.println();
//        strLowest= getLowestAmazonPriceCamel("B000F6UOOQ");
//        System.out.println("Lowest Price : " + strLowest);
//        System.out.println(); 
//        strLowest= getLowestAmazonPriceCamel("B000FIY3H8");
//        System.out.println("Lowest Price : " + strLowest);
//        System.out.println();
//        strLowest= getLowestAmazonPriceCamel("B00473TTYO");
//        System.out.println("Lowest Price : " + strLowest);
//        System.out.println();
//        strLowest= getLowestAmazonPriceCamel("B00B09EH72");
//        System.out.println("Lowest Price : " + strLowest);
//        System.out.println();
//        strLowest= getLowestAmazonPriceCamel("B004H6MV28");
//        System.out.println("Lowest Price : " + strLowest);
//        System.out.println();
//        
        System.out.println("Leaving main of CamelCamelCamel.");
    }
    
    public static Float getLowestAmazonPriceCamel(String strASIN) {
        //System.out.println("Inside getLowe" + "stAmazonPriceCamel of CamelCamelCamel.");
        System.out.println("[ CamelCamelCamel ] ASIN : " + strASIN);
        Float strLowest = (float)0;
        
        try {
           
            String url="http://camelcamelcamel.com/product/"+strASIN;
           RetrievePage.USE_PROXIES=true;
           HtmlPage page = CreateCatalog.getPage1(url);
            
            List<HtmlForm> htmlForms = page.getForms();
            for(HtmlForm form : htmlForms) {
                String strFormId = form.getAttribute("id");
                //System.out.println("Form Id : " + strFormId);
                if(strFormId.equalsIgnoreCase("summary_chart_form")) {
                    //System.out.println("Found Form that we are expecting");
                    HtmlCheckBoxInput htmlCheckBoxInput = form.getInputByValue("amazon");
                    boolean bAmazonSale = htmlCheckBoxInput.isDisabled();
                    if(bAmazonSale) {
                        System.out.println("[ CamelCamelCamel ] Amazon selleing information is not available to returning ZERO");
                    } else {
                        
                        HtmlAnchor htmlAnchor = (HtmlAnchor)page.getElementById("toggle_active_sub_amazon");
                        if(htmlAnchor.getTextContent().contains("Not in Stock")) {
                            System.out.println("[ CamelCamelCamel ] Currently item is not in stock .returning ZERO");
                            //return strLowest;
                        }
                        //System.out.println("Amazon had sell this item");
                        htmlCheckBoxInput.setChecked(true);
                        //System.out.println("Amazon is marked as checked. Now Clicking 3M link to show 3 month review");
                        page = page.getElementById("toggle_tp_summary_3m").click();
                        //System.out.println("Last three month's values is set. Now getting lowest price");
                        List listHtmlDivision = page.getByXPath("//div[@class='yui3-u-1-2']");
                        for(Object o : listHtmlDivision) {
                           HtmlDivision htmlDivision = (HtmlDivision) o;
                           List listHeading = htmlDivision.getByXPath("//div/div/h3");
                                for(Object o1 : listHeading) {
                                    HtmlHeading3 htmlHeading3 = (HtmlHeading3)o1;
                                    if(htmlHeading3.getTextContent().contains("Amazon Price History")) {
                                        List listTD = htmlDivision.getByXPath("//div/div/table/tbody/tr[@class='lowest_price']/td");
                                        for(Object o2 : listTD) {
                                            HtmlTableDataCell ttmlTableDataCell = (HtmlTableDataCell)o2;
                                            for(DomNode domNode : ttmlTableDataCell.getChildren()) {
                                                if(domNode.asText().contains("$")) {
                                                    String str=domNode.asText();
                                                    str=str.replaceAll("$", "");
                                                    strLowest = Float.valueOf(str);
                                                    break;
                                                }
                                            }
                                            
                                        }
                                        break;
                                    }
                                }
                        }
                         
                    }
                    break;
                }
            }
            //System.out.println("Leaving getLowestAmazonPriceCamel of CamelCamelCamel. strLowest : " + strLowest);
            
        } catch (Exception  ex) {
            ex.printStackTrace();
        } 
        return strLowest;
    }
    
     public static boolean isAmazonSellingInLast3Month(String strASIN) {
        //System.out.println("Inside getLowe" + "stAmazonPriceCamel of CamelCamelCamel.");
        System.out.println("[ CamelCamelCamel-05/22 ] ASIN : " + strASIN);
        boolean amazonSellingInLast3Months=false;
        try {
            
            WebClient webClient = new WebClient(BrowserVersion.FIREFOX_38);
            webClient.getOptions().setJavaScriptEnabled(true);
            webClient.getOptions().setRedirectEnabled(true);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.getOptions().setTimeout(10*1000);
            
          //  System.out.println("Wenclient initialized");
        if(RetrievePage.isOffice()){
                System.setProperty("java.net.preferIPv4Stack", "true");
          
             webClient  = new WebClient(BrowserVersion.FIREFOX_38,"proxydirect.tycoelectronics.com", 80);
        }            
            HtmlPage page = webClient.getPage("http://camelcamelcamel.com/product/"+strASIN);
            if(page.getByXPath("//div[@id='section_amazon']//table[@class='history_list product_pane']//tr//td").size()>0){
                String lastDateSoldByAmazon=((DomElement)page.getByXPath("//div[@id='section_amazon']//table[@class='history_list product_pane']//tr//td").get(0)).asText()    ;
                lastDateSoldByAmazon=StringUtils.substringBeforeLast(lastDateSoldByAmazon, " ");
                System.out.println("From CCC, lastDateSoldByAmazon is "+lastDateSoldByAmazon);
                Date date = new SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH).parse(lastDateSoldByAmazon);
                  System.out.println(date); // Sat Jan 02 00:00:00 BOT 2010                
                  Date today = new Date();  
                  today.setMonth(today.getMonth()-3);
                  
                  if(today.compareTo(date)<0){
                      System.out.println(" date : "+  date + " is greater than today:"+today);
                      amazonSellingInLast3Months=true;
                  }
                
            }
            
       
            
        } catch ( Exception ex) {
          ex.printStackTrace();
      
    }
      return amazonSellingInLast3Months;
     }
}

