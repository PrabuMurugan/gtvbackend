/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ie.moguntia.webcrawler.dao;

/**
 *
 * @author us083274
 */
public class url_structure_DAO {

    public String getHtml_structure() {
        return html_structure;
    }

    public void setHtml_structure(String html_structure) {
        this.html_structure = html_structure;
    }

    public int getLevenDis() {
        return levenDis;
    }

    public void setLevenDis(int levenDis) {
        this.levenDis = levenDis;
    }

    public String getTarget_html_structure() {
        return target_html_structure;
    }

    public void setTarget_html_structure(String target_html_structure) {
        this.target_html_structure = target_html_structure;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
 
    String url;
    int urlssubmitted_ID;

    public int getUrlssubmitted_ID() {
        return urlssubmitted_ID;
    }

    public void setUrlssubmitted_ID(int urlssubmitted_ID) {
        this.urlssubmitted_ID = urlssubmitted_ID;
    }
    String urltobecrawled_URL;

    public String getUrltobecrawled_URL() {
        return urltobecrawled_URL;
    }

    public void setUrltobecrawled_URL(String urltobecrawled_URL) {
        this.urltobecrawled_URL = urltobecrawled_URL;
    }

    String html_structure;
    int levenDis;
    String target_html_structure;
    String target_url;
    String url_prefix;

    public String getUrl_prefix() {
        return url_prefix;
    }

    public void setUrl_prefix(String url_prefix) {
        this.url_prefix = url_prefix;
    }

    public String getTarget_url() {
        return target_url;
    }

    public void setTarget_url(String target_url) {
        this.target_url = target_url;
    }

     
}
