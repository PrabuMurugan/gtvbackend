package queries;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author US083274
 */
public class DAO {
    public static void main(String[] args) throws Exception{
        System.out.println("inside DAO1");
        DAO d=new DAO();
        String s=d.getAndExecuteQuery("186","40062",null,null,null);
        
        System.out.println(s);
    }
    public static String getQuery(String n) throws Exception{
        
        String s,sql="";
         BufferedReader  br001 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\sql\\web_sql.txt")));
          try{
               
                while ((s = br001.readLine()) != null)   {
                    if(s.split("\t").length<4)
                        continue;
                        String sno=s.split("\t")[0];
                        sql=s.split("\t")[5];
                        String in_header=null;
                        if(s.split("\t").length>6)
                            in_header=s.split("\t")[6];
                        if(sno.equals(n)){
                            sql=sql.trim();
                            if(sql.indexOf("\"")==0)
                                sql=StringUtils.substring(sql, 1);
                            if(sql.charAt(sql.length()-1)=='\"')
                                sql=StringUtils.substring(sql, 0,sql.length()-1);
                            
                          
                            break;
                        }
                           
                }
             
          }catch (Exception e){
              e.printStackTrace();
              return e.getMessage();
          }finally{
               br001.close();
          }

        

        return sql;
    }
    public String getAndExecuteQuery(String n,String q1,String q2,String q3,String q4) throws Exception{
        String s;
         BufferedReader  br001 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\sql\\web_sql.txt")));
          try{
               
                while ((s = br001.readLine()) != null)   {
                    if(s.split("\t").length<4)
                        continue;
                        String sno=s.split("\t")[0];
                        String sql=s.split("\t")[5];
                        String in_header=null;
                        if(s.split("\t").length>6)
                            in_header=s.split("\t")[6];
                        if(sno.equals(n)){
                            sql=sql.trim();
                            if(sql.indexOf("\"")==0)
                                sql=StringUtils.substring(sql, 1);
                            if(sql.charAt(sql.length()-1)=='\"')
                                sql=StringUtils.substring(sql, 0,sql.length()-1);
                            
                            s= executeQuery(sql,q1,q2,q3,q4,in_header,true);
                            break;
                        }
                           
                }
             
          }catch (Exception e){
              e.printStackTrace();
              return e.getMessage();
          }finally{
               br001.close();
          }

        

        return s;
    }
    public  String executeQuery(String sql,String q1,String q2,String q3,String q4,String in_header,boolean retry) {
        StringBuffer ret=new StringBuffer();
        Connection con= null;
         try{
          con =getConnection();
           PreparedStatement prest = con.prepareStatement(sql);    
           if(q1!=null && q1.trim().length()>0){
               prest.setString(1, q1);
           }
           if(q2!=null && q2.trim().length()>0){
               prest.setString(2, q2);
           }
         /*  if(StringUtils.countMatches(sql, "?")==2 && &&(  q2==null || q2.length()==0)){
                prest.setString(2, q1);
           }*/
             
           if(q3!=null && q3.trim().length()>0){
               prest.setString(3, q3);
           }
           if(q4!=null && q4.trim().length()>0){
               prest.setString(4, q4);
           }
           
           String header=StringUtils.substringBeforeLast(sql.toLowerCase(), "from");
           header=StringUtils.substringAfter (header.toLowerCase(), "select");
            /*String[] paras=StringUtils.substringsBetween(header, "(",")");
           if(paras!=null){
           for (int i=0;i<paras.length;i++){
                
             header=header.replaceAll(paras[i],"");
             }
               
           } */
           
         // int len=header.split(",").length;
           if(in_header!=null){
               header=in_header;
           }
             ResultSet rs= prest.executeQuery();
             int len=rs.getMetaData().getColumnCount();
             ret.append("<table width='90%'>");
             ret.append("<tr>");
             for(int i=0;i<len;i++){
                 ret.append("<th>");
                 String hd=header.split(",")[i];
                 hd=hd.trim();
                 if(in_header==null && hd.indexOf(" ")>=0)
                     hd=StringUtils.substringAfterLast(hd, " ");
                 ret.append(hd);
                 ret.append("</th>");
             }
             ret.append("</tr>");
             while(rs.next()){
                ret.append("<tr>");
                for(int i=1;i<=len;i++){
                    ret.append("<td>");
                    String data=rs.getString(i);
                    ret.append(data);
                    ret.append("</td>");
                }
                ret.append("</tr>");
                 
             }
              ret.append("</table>");
              con.close();
         }catch (Exception e){
             System.out.println("EXCEPTION MSG:"+e.getMessage());
             if(e.getMessage().indexOf("ResultSet is from UPDATE")>=0){
                 try{
                     PreparedStatement pst=con.prepareStatement(sql);
                     pst.executeUpdate();                     
                 }catch (Exception e2){e2.printStackTrace();}

                
             }   
             if(e.getMessage().indexOf("max_allowed_packet")>=0 && retry){
                 try{
                     System.out.println("Excced maximum package, resettimg it");
                     PreparedStatement pst=con.prepareStatement("SET GLOBAL max_allowed_packet=16777216");
                     pst.executeUpdate();                     
                    executeQuery(sql,q1,q2,q3,q4,in_header,false);                     
                     
                 }catch (Exception e2){e2.printStackTrace();}

                
             }
             else{
                  e.printStackTrace();
             }
            
         try {
            if(con != null && !con.isClosed()) con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
            ret.append(e.toString() +" while trying to execute sql:"+sql);
         }
        
        return ret.toString();
    }
public static Connection getConnection() throws Exception  {
       Connection con = null;
    PreparedStatement pst = null;
    ResultSet rs = null;
    String   url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/harrisburgstore";
    String user = "root";
    String password = "admin123";
 
        
          
           try{
               Class.forName("com.mysql.jdbc.Driver"); 
             //  System.getProperties().put( "proxySet", "true" );
              //  System.setProperty("http.proxyHost", "http.proxyHost");
              //  System.setProperty("http.proxyPort", "80");

                 //  url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/tajplaza";
            con = DriverManager.getConnection(url, user, password);
           }catch (Exception e){
                
                con = DriverManager.getConnection("jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/harrisburgstore", user, password);
                
               
           }
            return con;
}    
    
}
