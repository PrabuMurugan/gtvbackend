 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jet;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.StringWebResponse;
import com.gargoylesoftware.htmlunit.WebClient;

import com.gargoylesoftware.htmlunit.html.HTMLParser;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetLowestPricedOffersForASINSample;
import com.tajplaza.products.GetProductCategoriesForASINSample;
import com.tajplaza.products.ListMatchingProductsSample;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.commons.lang3.StringUtils;


/**
 *
 * @author prabu
 */
public class PopulateJetCategoryMapping {
    public static void main(String[] s) throws Exception{
            String jetSql="select  path from hb_bc.jet_categories   ";          
            
            String sql="select  p.product_id, code,o.asin,description from hb_bc.exported_products_from_hb p, hb_bc.original_cost o where p.code= o.sku and code not in (select sku from hb_bc.hb_jet_categories_mapping)  order by case when p.code like '%AMA%' then 1 else 2 end  limit 500 ";          
            String insertSql=" insert into  hb_bc.hb_jet_categories_mapping (product_id, sku,jetpath,amazonpath,levenDistance,jet_description) values (?,?,?,?,?,?)";  
            
            System.out.println("Executing SQL:"+sql);
            Connection con1=RetrievePage.getConnection();
            PreparedStatement ps1=con1.prepareStatement(jetSql);
           PreparedStatement psInsert=con1.prepareStatement(insertSql);
            ResultSet rs1=ps1.executeQuery();
            ArrayList<String>  jetCategories=new ArrayList();
            while(rs1.next()){
               jetCategories.add(rs1.getString("path") );
            }
            PreparedStatement pst=con1.prepareStatement(sql);
            ResultSet rs=pst.executeQuery();   
            while(rs.next()){
                String asin=rs.getString("asin");
                Integer product_id=rs.getInt("product_id");
                 String code=rs.getString("code");
                 String description=rs.getString("description");
                String category="";
                try{category=GetProductCategoriesForASINSample.getAmazonCategory(asin);}catch (Exception e2){e2.printStackTrace();}
                 if(category==null || category.length()==0 ){
                     category="Jet Wholesale|Grocery|Pantry";
                 }
             category=category.replaceAll("&", " and ");
             category=category.trim();
             category=category.replaceAll("\\s+", " "); 
              int minLevenDistance=999;
               
              String jetMatchedCat="";
             
             System.out.println("Matched jet cat:"+jetMatchedCat);
             String[] amaCats=category.split(">");
             int overallMinLeventhDistance=999;
             String overalljetMatchedCat="";
             for(int i=amaCats.length-1;i>=0;i--){
                 String amaCat=amaCats[i];
                 
                   minLevenDistance=999;
                  jetMatchedCat="";
                 String jetMatchedCatPath="";
                 for(int j=0;j<jetCategories.size();j++){
                     String jetPath=jetCategories.get(j);
                     String[] jetSubCategories=StringUtils.split(jetPath,"|");
                     for(int k=0;k<jetSubCategories.length;k++){
                         String jetCat=jetSubCategories[k];
                         if(category.indexOf("Grocery")>=0 &&jetPath.indexOf("Grocery")<0 )
                             continue;
                         if(category.indexOf("Home")>=0 &&jetPath.indexOf("Home")<0 )
                             continue;
                         if(category.indexOf("Health")>=0 &&jetPath.indexOf("Health")<0 )
                             continue;
                         if(jetPath.indexOf("Fresh Food")>=0)
                             continue;
                         int levelDistance=org.apache.commons.lang3.StringUtils.getLevenshteinDistance(amaCat, jetCat);

                         if(levelDistance<minLevenDistance ){
                             minLevenDistance=levelDistance;
                             jetMatchedCat=jetCat;
                             jetMatchedCatPath=jetPath;
                             System.out.println("amaCat:"+amaCat+",jetCat:"+jetCat+",minLevenDistance:"+minLevenDistance);
                         }                         
                     }
                             

                 }
                 if(minLevenDistance<overallMinLeventhDistance){
                     overallMinLeventhDistance=minLevenDistance;
                     overalljetMatchedCat=jetMatchedCatPath;
                     
                 }
                 System.out.println("Current Matched jet Cat Path:"+jetMatchedCatPath +",minLevenDistance:" +minLevenDistance);
             }
             System.out.println("OverAll  Matched jet Cat Path:"+overalljetMatchedCat +" for amazon cat :"+category);
             if(overalljetMatchedCat.length()>0){
                 psInsert.setInt(1, product_id);
                 psInsert.setString(2,code);
                 psInsert.setString(3,overalljetMatchedCat);
                 psInsert.setString(4,category);
                 psInsert.setInt(5,overallMinLeventhDistance);
                 
                URL url = new URL("http://www.example.com");
                WebClient client = new WebClient();
                StringWebResponse response = new StringWebResponse(description, url);
                HtmlPage page = HTMLParser.parseHtml(response, client.getCurrentWindow());
                psInsert.setString(6, page.asText());
                 psInsert.executeUpdate();
                 
             }
             
             
             
            }
            
    }
}
