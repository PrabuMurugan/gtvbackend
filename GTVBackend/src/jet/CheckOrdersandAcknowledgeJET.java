/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jet;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 *
 * @author prabu
 */
public class CheckOrdersandAcknowledgeJET {
    public static String id_token="";
    public static void main(String[] args) throws Exception{
        id_token=CreateJETProducts.getToken();
         String ret=CreateJETProducts.sendJSON("https://merchant-api.jet.com/api/orders/created","GET",null);
         System.out.println("Orders created:"+ret);
         autoAcknowledge(ret);
         
        ret=CreateJETProducts.sendJSON("https://merchant-api.jet.com/api/orders/ready","GET",null);
         System.out.println("Orders ready:"+ret);  
          autoAcknowledge(ret);
          
        ret=CreateJETProducts.sendJSON("https://merchant-api.jet.com/api/orders/acknowledged","GET",null);
         System.out.println("Orders acknowledged:"+ret);  
          shipOrder(ret);
          
        ret=CreateJETProducts.sendJSON("https://merchant-api.jet.com/api/orders/inprogress","GET",null);
         System.out.println("Orders inprogress:"+ret);         
         
        ret=CreateJETProducts.sendJSON("https://merchant-api.jet.com/api/orders/complete","GET",null);
         System.out.println("Orders complete:"+ret);         
         
    }

    private static void autoAcknowledge(String ret) throws Exception {
        if(ret!=null && ret.length()>0){
            JSONObject js=JSONObject.fromObject(ret);
            JSONArray order_urls=js.getJSONArray("order_urls");
            for(int i=0;i<order_urls.size();i++){
                String order_url=(String) order_urls.get(i);
                if(order_url.indexOf("http")<0){
                    order_url="https://merchant-api.jet.com/api"+order_url;
                     ret=CreateJETProducts.sendJSON(order_url,"GET",null);
                     JSONObject orderDetail=JSONObject.fromObject(ret);
                     JSONArray order_items=orderDetail.getJSONArray("order_items");
                     
                     JSONArray ackonwledgeOrderItems=new JSONArray();
                     for(int j=0;j<order_items.size();j++){
                         JSONObject tmp=new JSONObject();
                         tmp.put("order_item_id", order_items.getJSONObject(j).get("order_item_id"));
                         tmp.put("order_item_acknowledgement_status", "fulfillable");
                         ackonwledgeOrderItems.add(tmp);
                     }
                             
                     System.out.println("Order Details : "+ret);
                     JSONObject jsObject=new JSONObject();
                     jsObject.put("acknowledgement_status", "accepted");
                     jsObject.put("order_items", ackonwledgeOrderItems);
                     
                     String acknowledgeUrl=order_url+"/acknowledge";
                     acknowledgeUrl=acknowledgeUrl.replaceAll("/withoutShipmentDetail", "");
                     ret=CreateJETProducts.sendJSON(acknowledgeUrl,"PUT",jsObject);
                     System.out.println("Acknowledging order: "+ret);
                     
                     
                    
                }
            }
            
        }
    }
    private static void shipOrder(String ret) throws Exception {
        if(ret!=null && ret.length()>0){
            JSONObject js=JSONObject.fromObject(ret);
            JSONArray order_urls=js.getJSONArray("order_urls");
            for(int i=0;i<order_urls.size();i++){
                String order_url=(String) order_urls.get(i);
                if(order_url.indexOf("http")<0){
                    order_url="https://merchant-api.jet.com/api"+order_url;
                     ret=CreateJETProducts.sendJSON(order_url,"GET",null);
                     JSONObject orderDetail=JSONObject.fromObject(ret);
                     JSONArray order_items=orderDetail.getJSONArray("order_items");
                     
                     JSONArray shipItems=new JSONArray();
                     for(int j=0;j<order_items.size();j++){
                         JSONObject tmp=new JSONObject();
                         tmp.put("merchant_sku",order_items.getJSONObject(j).get("merchant_sku"));
                         tmp.put("response_shipment_sku_quantity",order_items.getJSONObject(j).getInt("request_order_quantity"));
                         shipItems.add(tmp);
                     }
                             
                     System.out.println("Order Details : "+ret);
                     JSONObject jsObject=new JSONObject();
                     jsObject.put("shipment_items", shipItems);
                     
                     String acknowledgeUrl=order_url+"/shipped";
                     acknowledgeUrl=acknowledgeUrl.replaceAll("/withoutShipmentDetail", "");
                     ret=CreateJETProducts.sendJSON(acknowledgeUrl,"PUT",jsObject);
                     System.out.println("Acknowledging order: "+ret);
                     
                     
                    
                }
            }
            
        }
    }
    
}
