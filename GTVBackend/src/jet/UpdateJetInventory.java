/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jet;

import bombayplaza.pricematch.RetrievePage;

import net.sf.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import net.sf.json.JSONArray;
import samsclub.HBAmazonFixShelfQuantity;

/**
 *
 * @author prabu
 */
public class UpdateJetInventory {
    public static  String id_token="";
    static String DEBUG_SQL="";
    public static void main(String[] args) throws Exception{
            String PRODUCTION=System.getProperty("PRODUCTION","false");
             
            if(PRODUCTION.equals("true")){
                DEBUG_SQL="";
            }  
            String JUST_OS_SHELF_ITEMS=System.getProperty("JUST_OS_SHELF_ITEMS","false");
             
             
           /* HBAmazonFixShelfQuantity.con=RetrievePage.getConnection();
           HBAmazonFixShelfQuantity.UPDATE_JET_QTY_ALSO=true;
            HBAmazonFixShelfQuantity.updateHBQty("112-AMA-AMAZON_VARIATIONS");        
            HBAmazonFixShelfQuantity.con.close();*/
           id_token= getToken();
           Connection con=RetrievePage.getConnection();
           /*
           String sql="select j.sku,quantity stock_level from hb_bc.jet_merchantskus j,  tajplaza.existing_all_inventory e  where  j.sku=e.seller_sku and  (   (cast(quantity as decimal)<3))  "+DEBUG_SQL
                   + " union "
                   + " select j.sku,case when p.stock_level is null then 0 else p.stock_level end stock_level from hb_bc.jet_merchantskus j left join hb_bc.exported_products_from_hb p on j.sku=p.code where   ( (inventory='No'  or status in ('Excluded','Archived')) and  cast(stock_level as decimal)>6)   "+DEBUG_SQL
                  +  " union "
                   + " select j.sku,case when p.stock_level is null then 0 else p.stock_level end stock_level from hb_bc.jet_merchantskus j left join hb_bc.exported_products_from_hb p on j.sku=p.code where  ( inventory='Yes' and  (p.stock_level is null or  cast(stock_level as decimal)<6))   "+DEBUG_SQL 
                   ;
           */
           /* Updated by Himanshu */
           String sql = "select sku,stock_level from hb_bc.Tmp_jet_Stock";
           
            if(JUST_OS_SHELF_ITEMS.equals("true")){
                sql=" select j.sku, case when s.qty is null or s.qty/v.pkg_qty=0 then 0 else floor(s.qty/v.pkg_qty)  end stock_level from hb_bc.jet_merchantskus j left join tajplaza.variation_sku_upcs v  on j.sku=v.sku left join  tajplaza.tmp_scanner_data_shelf s on v.sku = s.upc where   (j.sku like 'VAR%' or j.sku like '%AMA%')   and  v.upc in (select upc from tajplaza.scanner_data_shelf_audit_archived  s  where s.ts>(curdate() - interval 1 day)   group by s.upc having count(*)>=6)";
            }           
           
           System.out.println("SQL : "+sql);
           PreparedStatement ps=con.prepareStatement(sql);
           //On 8/15, made all jet  non AMA skus to 0 as per below query and above query is fixed to feed only AMA skus
          // PreparedStatement ps=con.prepareStatement("select j.sku, 0  stock_level from hb_bc.jet_merchantskus j left join hb_bc.exported_products_from_hb p on j.sku=p.code where j.sku not like '%AMA%'");           
           ResultSet  rs=ps.executeQuery();
           while(rs.next()){
               try{
                   String sku=rs.getString("sku");
                   int stock_level=rs.getInt("stock_level");
                 JSONObject jSONObject = new JSONObject();
                jSONObject.put("fulfillment_node_id", "0f7fa5f221524616b2fde3b5e40764a9");
                jSONObject.put("quantity", stock_level);
                if(stock_level<=0){
                  JSONObject jSONObject1 = new JSONObject();
                    jSONObject1.put("is_archived", true);
                    sendJSON("https://merchant-api.jet.com/api/merchant-skus/"+sku+"/status/archive","PUT",jSONObject1);                    
                    stock_level=0;
                    
                }else{
                  JSONObject jSONObject1 = new JSONObject();
                    jSONObject1.put("is_archived", false);
                    sendJSON("https://merchant-api.jet.com/api/merchant-skus/"+sku+"/status/archive","PUT",jSONObject1);                     
                }

                JSONArray jsArr=new JSONArray();
                jsArr.add(jSONObject);

                jSONObject = new JSONObject();
                jSONObject.put("fulfillment_nodes", jsArr);
                 System.out.println("Updating NEW JET Inventory for SKU:"+sku + " to " + stock_level)       ;
                 sendJSON("https://merchant-api.jet.com/api/merchant-skus/"+sku+"/Inventory","PUT",jSONObject);

               }catch (Exception e){e.printStackTrace();}

           }

            System.out.println("Completed UpdateJetInventory program");
             
    }
    public static String sendJSON( String webPage,String reqMethod,JSONObject jSONObject) throws Exception{
        String ret="";
    
     URL uri = new URL(webPage);


     HttpURLConnection httpCon2 = (HttpURLConnection) uri.openConnection();
    httpCon2.setRequestProperty("Content-Type", "application/json");
    httpCon2.setRequestProperty("Accept", "application/json");
    httpCon2.setRequestProperty("Authorization", "Bearer " + id_token);
    httpCon2.setDoOutput(true);
    httpCon2.setRequestMethod(reqMethod);
    



    if(jSONObject!=null){
        OutputStreamWriter out = new OutputStreamWriter(httpCon2.getOutputStream());
         String strJString = jSONObject.toString();
            System.out.println("Request : " + strJString);
            out.write(strJString);
            out.close();
        
    }
    
     InputStream responseIS = httpCon2.getInputStream();
      BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
     String line = "";
     StringBuilder sb = new StringBuilder();
    while ((line = reader.readLine()) != null) {
        sb.append(line);
    }
     ret = new String(sb);
     System.out.println("Result:"+ret);             
        
        return ret;
    }

    public static String getToken() throws Exception{
        
        String webPage=" https://merchant-api.jet.com/api/token";
            URL uri = new URL(webPage);


            HttpURLConnection httpCon0 = (HttpURLConnection) uri.openConnection();
            httpCon0.setRequestProperty("Content-Type", "application/json");
            httpCon0.setRequestProperty("Accept", "application/json");
            httpCon0.setDoOutput(true);
            httpCon0.setRequestMethod("POST");
            OutputStreamWriter out = new OutputStreamWriter(httpCon0.getOutputStream());

            JSONObject jSONObject = new JSONObject();
             jSONObject.put("user", "AE43525AF32235F567CA06734973A799589EB568");
             jSONObject.put("pass", "w6HzVER3c02Ilme6A/7OYRaM9nOjmQkzJG3cEm8mfn2t");
           

            String strJString = jSONObject.toString();
            System.out.println("Request : " + strJString);
            out.write(strJString);
            out.close();

            InputStream responseIS = httpCon0.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = "";
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            String result = new String(sb);
             System.out.println("Result:"+result);
             JSONObject js=JSONObject.fromObject(result);
             String id_token1=js.getString("id_token");
             id_token=id_token1;
             return id_token1;
    }
    
}
