/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 *
 * @author prabu
 */
public class ShipOrderJET{
    public static String id_token="";
    public static void main(String[] args) throws Exception{
        id_token=CreateJETProducts.getToken();
        String order_url="https://merchant-api.jet.com/api/orders/withoutShipmentDetail/712e50961e9d46dca7e967e2a8940383";
         String ret=CreateJETProducts.sendJSON(order_url,"GET",null);
         JSONObject orderDetail=JSONObject.fromObject(ret);
         JSONArray order_items=orderDetail.getJSONArray("order_items");

         JSONArray shipment_items=new JSONArray();
         for(int j=0;j<order_items.size();j++){
             JSONObject tmp=new JSONObject();
            // tmp.put("shipment_item_id", order_items.getJSONObject(j).get("order_item_id"));
             tmp.put("merchant_sku",order_items.getJSONObject(j).get("merchant_sku"));
             tmp.put("response_shipment_sku_quantity",order_items.getJSONObject(j).getInt("request_order_quantity"));
             
             shipment_items.add(tmp);
         }
            
         String webPage=order_url+"/shipped";
         
         
          
         
         DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        
         JSONObject shipment=new JSONObject();
        // shipment.put("shipment_id", System.currentTimeMillis());
         //shipment.put("shipment_tracking_number", "1Z12342452342");
         //shipment.put("response_shipment_date",  "2016-08-01T18:00:00.0000000-04:00");
         //shipment.put("expected_delivery_date",  "2016-08-02T18:00:00.0000000-04:00");
         //shipment.put("ship_from_zip_code",  "17055");
         
         //shipment.put("response_shipment_method", "ups_ground");
         //shipment.put("carrier", "UPS");
         shipment.put("shipment_items", shipment_items);
         
        
         JSONArray shipments=new JSONArray();
         shipments.add(shipment);
        
         JSONObject js=new JSONObject();
         js.put("shipments", shipments);
         
         String shipUrl=order_url+"/shipped";
         shipUrl=shipUrl.replaceAll("/withoutShipmentDetail", "");
         
        
         shipUrl="https://merchant-api.jet.com/api/orders/712e50961e9d46dca7e967e2a8940383/shipped";
         ret=CreateJETProducts.sendJSON(shipUrl,"PUT",js);
         System.out.println("Shipping order: "+ret);
         
          
         
         
          
                 
    }

 
}
