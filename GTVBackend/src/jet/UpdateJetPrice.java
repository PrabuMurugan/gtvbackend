/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jet;

import bombayplaza.pricematch.RetrievePage;

import net.sf.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import net.sf.json.JSONArray;
import samsclub.HBAmazonFixShelfQuantity;

/**
 *
 * @author prabu
 */
public class UpdateJetPrice {
    public static  String id_token="";
    public static void main(String[] args) throws Exception{
           /* HBAmazonFixShelfQuantity.con=RetrievePage.getConnection();
           HBAmazonFixShelfQuantity.UPDATE_JET_QTY_ALSO=true;
            HBAmazonFixShelfQuantity.updateHBQty("112-AMA-AMAZON_VARIATIONS");        
            HBAmazonFixShelfQuantity.con.close();*/
           id_token= CreateJETProducts.getToken();
           Connection con=RetrievePage.getConnection();
           String sql="select sku from tajplaza.amazon_min_price where custom_note in ('Stuck in Shelf 30+days','Stuck in Shelf 60+days','Expiring Soon')";
           ArrayList stuckSkus=new ArrayList();
          PreparedStatement ps=con.prepareStatement(sql) ;
          ResultSet rs=ps.executeQuery();
          while(rs.next()){
              stuckSkus.add(rs.getString(1));;
          }
             sql=" select j.sku,j.status,p.code,p.stock_level,p.price  from hb_bc.jet_merchantskus j, hb_bc.exported_products_from_hb p   where j.sku=p.code   and stock_level>6  and status   in ('Available for Purchase') order by case when p.code like '%AMA%' or p.code like 'VAR_%'  then 1 else 2 end limit 100   ";
           System.out.println("SQL : "+sql);
             ps=con.prepareStatement(sql);
           //On 8/15, made all jet  non AMA skus to 0 as per below query and above query is fixed to feed only AMA skus
          // PreparedStatement ps=con.prepareStatement("select j.sku, 0  stock_level from hb_bc.jet_merchantskus j left join hb_bc.exported_products_from_hb p on j.sku=p.code where j.sku not like '%AMA%'");           
           rs=ps.executeQuery();
           while(rs.next()){
               try{
                   String sku=rs.getString("sku");
                   int stock_level=rs.getInt("stock_level");
                   Double price=rs.getDouble("price");
                      String ret=CreateJETProducts.sendJSON("https://merchant-api.jet.com/api/merchant-skus/"+sku+"/salesdata","GET",null);
                     System.out.println("Return SalesData:"+ret);                
                     JSONObject js=JSONObject.fromObject(ret);
                     if(js.containsKey("my_best_offer") && js.containsKey("best_marketplace_offer")){
                         JSONObject   my_best_offer=js.getJSONArray("my_best_offer").getJSONObject(0) ;
                         JSONObject   best_marketplace_offer=js.getJSONArray("best_marketplace_offer").getJSONObject(0)  ;
                         Double myPrice=(double)0;
                         myPrice=(Double) my_best_offer.get("item_price");
                         Double shipping_price=(Double) my_best_offer.get("shipping_price");
                         myPrice=myPrice+shipping_price;
                         
                         Double marketPrice =(Double) best_marketplace_offer.get("item_price");
                         Double market_shipping_price=(Double) best_marketplace_offer.get("shipping_price");
                         marketPrice=marketPrice+market_shipping_price;

                         System.out.println("My price :"+myPrice+", marketPrice:"+marketPrice);
                         myPrice=marketPrice-shipping_price;
                         
                         Double minPrice=price*.8;
                         if(stuckSkus.contains(sku)){
                             minPrice=.2;
                         }
                         if(myPrice<minPrice)
                             myPrice=minPrice;
                        JSONObject jSONObject = new JSONObject();   
                        jSONObject.put("price", myPrice);
                        System.out.println("Updating NEW JET Price for SKU:"+sku + " to " + myPrice)       ;
                        sendJSON("https://merchant-api.jet.com/api/merchant-skus/"+sku+"/price","PUT",jSONObject);

                     }

               }catch (Exception e){e.printStackTrace();}

           }

            System.out.println("Completed UpdateJetInventory program");
             
    }
    public static String sendJSON( String webPage,String reqMethod,JSONObject jSONObject) throws Exception{
        String ret="";
    
     URL uri = new URL(webPage);


     HttpURLConnection httpCon2 = (HttpURLConnection) uri.openConnection();
    httpCon2.setRequestProperty("Content-Type", "application/json");
    httpCon2.setRequestProperty("Accept", "application/json");
    httpCon2.setRequestProperty("Authorization", "Bearer " + id_token);
    httpCon2.setDoOutput(true);
    httpCon2.setRequestMethod(reqMethod);
    



    if(jSONObject!=null){
        OutputStreamWriter out = new OutputStreamWriter(httpCon2.getOutputStream());
         String strJString = jSONObject.toString();
            System.out.println("Request : " + strJString);
            out.write(strJString);
            out.close();
        
    }
    
     InputStream responseIS = httpCon2.getInputStream();
      BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
     String line = "";
     StringBuilder sb = new StringBuilder();
    while ((line = reader.readLine()) != null) {
        sb.append(line);
    }
     ret = new String(sb);
     System.out.println("Result:"+ret);             
        
        return ret;
    }

    public static String getToken() throws Exception{
        
        String webPage=" https://merchant-api.jet.com/api/token";
            URL uri = new URL(webPage);


            HttpURLConnection httpCon0 = (HttpURLConnection) uri.openConnection();
            httpCon0.setRequestProperty("Content-Type", "application/json");
            httpCon0.setRequestProperty("Accept", "application/json");
            httpCon0.setDoOutput(true);
            httpCon0.setRequestMethod("POST");
            OutputStreamWriter out = new OutputStreamWriter(httpCon0.getOutputStream());

            JSONObject jSONObject = new JSONObject();
             jSONObject.put("user", "AE43525AF32235F567CA06734973A799589EB568");
             jSONObject.put("pass", "w6HzVER3c02Ilme6A/7OYRaM9nOjmQkzJG3cEm8mfn2t");
           

            String strJString = jSONObject.toString();
            System.out.println("Request : " + strJString);
            out.write(strJString);
            out.close();

            InputStream responseIS = httpCon0.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = "";
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            String result = new String(sb);
             System.out.println("Result:"+result);
             JSONObject js=JSONObject.fromObject(result);
             String id_token1=js.getString("id_token");
             id_token=id_token1;
             return id_token1;
    }
    
}
