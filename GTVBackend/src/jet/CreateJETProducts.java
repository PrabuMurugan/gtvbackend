/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jet;

import bombayplaza.pricematch.RetrievePage;
import net.sf.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import net.sf.json.JSONArray;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
/**
 *
 * @author prabu
 */
public class CreateJETProducts {
    public static  String id_token="";
    public static void main(String[] args) throws Exception{
    id_token= getToken();
    Connection con=RetrievePage.getConnection();
    //PreparedStatement ps=con.prepareStatement("select p.code as 'Merchant SKU',p.name as 'Product title','' as 'Parent SKU', round(price*1.25,2) as 'Price' , case when pack is not null then pack else 1 end as 'Multipack quantity', j.jetpath as 'Category path', brand as 'Brand', brand as 'Manufacturer', '' as 'Mfr Part Number', 'USA' as 'Country of origin',  jet_description as 'Description', '' as 'Product tax code',5 as 'Package length (inches)',5 as 'Package width (inches)',5 as 'Package height(inches)','' as 'SKU length (inches)','' as 'SKU width (inches)','' as 'SKU height (inches)',p.weight as 'Shipping weight (lbs)',p.product_upc_ean as 'UPC code','' as 'GTIN-14 code','' as 'ISBN-10 code','' as 'ISBN-13 code','' as 'EAN code',u.asin as 'ASIN',product_images as 'Main image','0f7fa5f221524616b2fde3b5e40764a9' as 'Fulfillment node',stock_level-1 qty from hb_bc.exported_products_from_hb p left join tajplaza.un_full_case_asins u on  p.code=u.code, hb_bc.hb_jet_categories_mapping j where p.code=j.sku and stock_level>0 and p.code like 'SC%' limit 10");
    String sql=" select p.code as 'Merchant SKU',p.name as 'Product title','' as 'Parent SKU', round(price*1.25,2) as 'Price' , case when pack is not null then pack else 1 end as 'Multipack quantity',  j.jetpath as 'Category path', brand as 'Brand', brand as 'Manufacturer', '' as 'Mfr Part Number', 'USA' as 'Country of origin',   jet_description as 'Description', '' as 'Product tax code',5 as 'Package length (inches)',5 as 'Package width (inches)',5 as 'Package height(inches)', '' as 'SKU length (inches)','' as 'SKU width (inches)','' as 'SKU height (inches)',p.weight as 'Shipping weight (lbs)', p.product_upc_ean as 'UPC code','' as 'GTIN-14 code','' as 'ISBN-10 code','' as 'ISBN-13 code','' as 'EAN code',u.asin as 'ASIN', product_images as 'Main image','0f7fa5f221524616b2fde3b5e40764a9' as 'Fulfillment node',stock_level-1 qty from  hb_bc.exported_products_from_hb p , tajplaza.un_full_case_asins u, hb_bc.hb_jet_categories_mapping j  where  p.code=u.code and   p.code=j.sku and cast(stock_level as decimal)>4  and p.code not in (select sku from hb_bc.jet_merchantskus)    and p.code not in (select sku from hb_bc.jet_tried_to_create_and_erroed_skus)  order by case when p.code like '%AMA%' or p.code like 'VAR_%'  then 1 else 2 end   ";
    String insertsql="insert into hb_bc.jet_tried_to_create_and_erroed_skus (sku) values (?)";
    PreparedStatement psInsert=con.prepareStatement(insertsql);
    
    System.out.println("Executing sql: "+sql);
    PreparedStatement ps=con.prepareStatement(sql);;
    ResultSet  rs=ps.executeQuery();
    while(rs.next()){
        try{
        String sku=rs.getString("Merchant SKU");
        int stock_level=rs.getInt("qty");
        Float Price=rs.getFloat("Price");
        if(sku.indexOf("DT_")>=0)
            Price=Price+8;
        Price=Price*(float)1.25;
        Price=Math.round(Price*(float)100)/(float)100; 
            //Create the product
             JSONObject standard_product_code=new JSONObject();
              JSONObject jSONObject = new JSONObject();
             jSONObject.put("product_title", rs.getString("Product title"));
             int multipacks=1;
             try{
                  multipacks=rs.getInt("Multipack quantity");
                 
             }catch (Exception e2){}
             jSONObject.put("multipack_quantity", multipacks);
             if( rs.getString("ASIN")!=null && rs.getString("ASIN").indexOf("B")>=0)
                 jSONObject.put("ASIN", rs.getString("ASIN"));
             jSONObject.put("brand", rs.getString("Brand"));
             jSONObject.put("main_image_url", rs.getString("Main image"));
             if(rs.getString("UPC code").indexOf("0000000")<0 && rs.getString("UPC code").length()>9){
                 standard_product_code.put("standard_product_code", rs.getString("UPC code"));
                 standard_product_code.put("standard_product_code_type", "UPC");
                 JSONArray standard_product_codes=new JSONArray();
                 standard_product_codes.add(standard_product_code);
                 jSONObject.put("standard_product_codes", standard_product_codes);
             }
             
             
             jSONObject.put("category_path", rs.getString("Category path"));
             jSONObject.put("product_description", rs.getString("Description"));
             jSONObject.put("shipping_weight_pounds", rs.getFloat("Shipping weight (lbs)"));
             jSONObject.put("package_length_inches", 5);
             jSONObject.put("package_width_inches", 5);
             jSONObject.put("package_height_inches", 5);
             //jSONObject.put("fulfillment_time", "4");
             
             
             
             
             
             

         System.out.println("Creating SKU:"+sku);
           try{
               sendJSON("https://merchant-api.jet.com/api/merchant-skus/"+sku,"PUT",jSONObject);
           }catch (Exception e){
                try{
                    jSONObject.remove("standard_product_codes");
                    jSONObject.put("product_description",  rs.getString("Product title"));
                    sendJSON("https://merchant-api.jet.com/api/merchant-skus/"+sku,"PUT",jSONObject);
                }catch (Exception e2){
                    psInsert.setString(1, sku);
                    psInsert.executeUpdate();;
                    e2.printStackTrace();
                    continue;
                }
           }
             
             //Price
             jSONObject = new JSONObject();
            jSONObject.put("price", Price);
             sendJSON("https://merchant-api.jet.com/api/merchant-skus/"+sku+"/price","PUT",jSONObject);

             jSONObject = new JSONObject();
            jSONObject.put("fulfillment_node_id", "0f7fa5f221524616b2fde3b5e40764a9");
            jSONObject.put("quantity", stock_level);
            
            JSONArray jsArr=new JSONArray();
            jsArr.add(jSONObject);
            
            jSONObject = new JSONObject();
            jSONObject.put("fulfillment_nodes", jsArr);
                    
             sendJSON("https://merchant-api.jet.com/api/merchant-skus/"+sku+"/Inventory","PUT",jSONObject);                   
            
        }catch (Exception e2){
            e2.printStackTrace();
        }
 
    }


              
             
    }
    public static String sendJSON( String webPage,String reqMethod,JSONObject jSONObject) throws Exception{
        String ret="";
    
     URL uri = new URL(webPage);


     HttpURLConnection httpCon2 = (HttpURLConnection) uri.openConnection();
    httpCon2.setRequestProperty("Content-Type", "application/json");
    httpCon2.setRequestProperty("Accept", "application/json");
    httpCon2.setRequestProperty("Authorization", "Bearer " + id_token);
    httpCon2.setDoOutput(true);
    httpCon2.setRequestMethod(reqMethod);
    



    if(jSONObject!=null){
        OutputStreamWriter out = new OutputStreamWriter(httpCon2.getOutputStream());
         String strJString = jSONObject.toString();
            System.out.println("Request : " + strJString);
            out.write(strJString);
            out.close();
        
    }
    
     InputStream responseIS = httpCon2.getInputStream();
      BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
     String line = "";
     StringBuilder sb = new StringBuilder();
    while ((line = reader.readLine()) != null) {
        sb.append(line);
    }
     ret = new String(sb);
     System.out.println("Result:"+ret);             
        
        return ret;
    }

    public static String getToken() throws Exception{
        
        String webPage=" https://merchant-api.jet.com/api/token";
            URL uri = new URL(webPage);


            HttpURLConnection httpCon0 = (HttpURLConnection) uri.openConnection();
            httpCon0.setRequestProperty("Content-Type", "application/json");
            httpCon0.setRequestProperty("Accept", "application/json");
            httpCon0.setDoOutput(true);
            httpCon0.setRequestMethod("POST");
            OutputStreamWriter out = new OutputStreamWriter(httpCon0.getOutputStream());

            JSONObject jSONObject = new JSONObject();
             jSONObject.put("user", "AE43525AF32235F567CA06734973A799589EB568");
             jSONObject.put("pass", "w6HzVER3c02Ilme6A/7OYRaM9nOjmQkzJG3cEm8mfn2t");
           

            String strJString = jSONObject.toString();
            System.out.println("Request : " + strJString);
            out.write(strJString);
            out.close();

            InputStream responseIS = httpCon0.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = "";
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            String result = new String(sb);
             System.out.println("Result:"+result);
             JSONObject js=JSONObject.fromObject(result);
             String id_token1=js.getString("id_token");
             id_token=id_token1;
             return id_token1;
    }
    
}
