/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package costco;
 
import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
 
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;
 
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import javax.net.ssl.HttpsURLConnection;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

 

/**
 *
 * @author us083274
 */
public class InsertCostcoCatalog {
    public static void main(String args[]) throws Exception{
       updateItemCodesRemoveLastDigitIfStartsWithZeros();
        String id="25204";
        if(System.getProperty("SLEEP_TIME_ALL_PROXIES_USED_ONCE")!=null)
            RetrievePage.SLEEP_TIME_ALL_PROXIES_USED_ONCE =Integer.parseInt(System.getProperty("SLEEP_TIME_ALL_PROXIES_USED_ONCE"));
        if(System.getProperty("NUMBER_TIMES_PROXIES_BEFORE_SLEEP")!=null)
            RetrievePage.NUMBER_TIMES_PROXIES_BEFORE_SLEEP =Integer.parseInt(System.getProperty("NUMBER_TIMES_PROXIES_BEFORE_SLEEP"));        
        System.out.println("Value of NUMBER_TIMES_PROXIES_BEFORE_SLEEP is "+  RetrievePage.NUMBER_TIMES_PROXIES_BEFORE_SLEEP);
        loadUNFIProductsToTable();
        //loadCustomFields(id,null);
         
       // loadUNFIProductsToTable() ;
    }

    public static void updateItemCodesRemoveLastDigitIfStartsWithZeros() throws Exception{
        String sql="select item_code from costco_input_catalog";
        String sql2="update hb_bc.costco_input_catalog set item_code=? where item_code=?";
        Connection con=RetrievePage.getConnection();
        PreparedStatement pst=con.prepareStatement("select item_code    from hb_bc.costco_input_catalog " );
        ResultSet rs=pst.executeQuery();
        while(rs.next()){
            String old_item_code=rs.getString(1);
            String new_item_code=old_item_code.replaceAll("\"", "").replaceAll("'", "");
            if(old_item_code.indexOf("00000")>=0){
                new_item_code=new_item_code.substring(0,new_item_code.length()-1);
                new_item_code=StringUtils.stripStart(new_item_code, "0");
                
            }
            System.out.println("old item code:"+old_item_code+",new_item_code:"+new_item_code);
            
            PreparedStatement pst2=con.prepareStatement(sql2);
            pst2.setString(1, new_item_code);
            pst2.setString(2, old_item_code);
            pst2.executeUpdate();
        }
        
    }
            
        public static void loadUNFIProductsToTable() throws Exception {
            RetrievePage.USE_PROXIES=true;
             Connection con = RetrievePage.getConnection();
             System.out.println("Inside loadUNFIProductsToTable  Processing ALL rows");

             PreparedStatement selectStmt = con.prepareStatement("select  distinct  row_num,upc,item_code,price from hb_bc.costco_input_catalog   where replace(replace(replace(replace('CO_ROWNUM_ITEMCODE','ROWNUM',row_num),'ITEMCODE',item_code),'\\'',''),'\"','')  not in (select item_code from hb_bc.costco_catalog) ");
             PreparedStatement deleteStment=con.prepareStatement("delete from hb_bc.costco_catalog  where item_code=? ");
            PreparedStatement insertStmt_Found=con.prepareStatement(" insert into hb_bc.costco_catalog (item_code,  asin,upc, title, descr, brand, price,image_file,rank,cat1,suggested_price,bestprice,secondbestprice) values (?,?,?,?,?,?,?,?,?,?,?,?,?) ");
             PreparedStatement insertStmt_NF=con.prepareStatement(" insert into hb_bc.costco_catalog (item_code,  upc,asin) values (?,?,'NA') ");

            ResultSet rs=selectStmt.executeQuery();
            ResultSet rs2;
            int itemCount=0;
             
            int count=0;
           
             
            while(rs.next()){
                  String upc=rs.getString("upc").replaceAll("\"", "").replaceAll("\'", "");
                   String oc=rs.getString("price").replaceAll("\"", "").replaceAll("\'", "");
                  String row_num=rs.getString("row_num").replaceAll("\"", "").replaceAll("\'", "");
                  String item_code=rs.getString("item_code").replaceAll("\"", "").replaceAll("\'", "");
                  item_code=StringUtils.stripStart(item_code, "0");
                  item_code="CO_"+row_num+"_"+item_code;
                   
                upc=upc.replaceAll("UPC:","");
                upc=upc.replaceAll("upc:","");
                upc=upc.replaceAll("-","");
                upc=upc.replaceAll("'","");
                upc=upc.replaceAll(" ","");
                upc=upc.trim();
                
                
             
                if(upc.length()>13)
                             upc=upc.substring(1,14);
                if(upc.length()<9)
                    continue;
                if(upc.length()<12)
                    upc=StringUtils.leftPad(upc,12, '0');
                System.out.println("Processing rowLabel:"+item_code +",upc:"+upc);
                MatchThePrice.JAVASCRIPTNONO=false;
                MatchThePrice.DOWNLOAD_IMAGE=true;
                System.out.println("Processing UPC:"+upc);
                String title="NA";
                String manu="NA";
                String asin="NA";
                 float bestprice=(float)0;
                 float secondbestprice=(float)0;

                ArrayList items=CreateCatalog.getJustUPCList(upc);    
                if(items.size()>0){
                    asin= (String) items.get(0);
                    HashMap hm=MatchThePrice.getProductDetails(asin, false,false,false);

                     //pw.storeProduct1(p, con, "amazon_catalog_un_hb");
                     
                    title=(String)(hm.get("title")!=null?hm.get("title"):null);
                    manu=(String)(hm.get("manu")!=null?hm.get("manu"):null);
                    String des=(String)(hm.get("des")!=null?hm.get("des"):null);
                    String bb1=(String)(hm.get("bb1")!=null?hm.get("bb1"):null);
                    String bb2=(String)(hm.get("bb2")!=null?hm.get("bb2"):null);
                    String bb3=(String)(hm.get("bb3")!=null?hm.get("bb3"):null);
                    String bb4=(String)(hm.get("bb4")!=null?hm.get("bb4"):null);
                    String category=(String)(hm.get("category")!=null?hm.get("category"):null);
                     bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
                      secondbestprice=(Float)(hm.get("secondbestprice")!=null?hm.get("secondbestprice"):(float)0);
                     Float suggested_price=bestprice*(float).85;
                    try{
                     
                     Float suggestedPriceFromOC=Float.valueOf(oc)*(float)1.2+(float)2;
                     if(suggested_price>suggestedPriceFromOC)
                         suggested_price=suggestedPriceFromOC;
                 }catch(Exception e3){}
                    String localImage=(String)(hm.get("LOCAL_IMAGE")!=null?hm.get("LOCAL_IMAGE"):null);
                    String rank=(String)(hm.get("rank")!=null?hm.get("rank"):"99999");
                    
                    String descr=des+"<ul>";
                    if(bb1!=null && bb1.toUpperCase().equals("NULL") ==false)
                        descr+="<li>"+bb1+"</li>";
                    if(bb2!=null&& bb1.toUpperCase().equals("NULL") ==false)
                        descr+="<li>"+bb2+"</li>";
                    if(bb3!=null&& bb1.toUpperCase().equals("NULL") ==false)
                        descr+="<li>"+bb3+"</li>";
                    if(bb4!=null&& bb1.toUpperCase().equals("NULL") ==false)
                        descr+="<li>"+bb4+"</li>";
                    
                    descr+="</ul>";
                    
                    descr=StringUtils.substring(descr, 0,9999);
                    
                    deleteStment.setString(1, item_code);
                    deleteStment.executeUpdate();
                    
                    insertStmt_Found.setString(1, item_code);
                    insertStmt_Found.setString(2, asin);
                    insertStmt_Found.setString(3, upc);
                    insertStmt_Found.setString(4, title);
                    insertStmt_Found.setString(5, descr);
                    insertStmt_Found.setString(6, manu);
                    insertStmt_Found.setString(7, oc);
                    insertStmt_Found.setString(8, localImage);
                    insertStmt_Found.setString(9, rank);
                    insertStmt_Found.setString(10, category);
                    insertStmt_Found.setFloat(11, suggested_price);
                    insertStmt_Found.setFloat(12, bestprice);
                    insertStmt_Found.setFloat(13, secondbestprice);
                    
                    insertStmt_Found.executeUpdate();


                   
                }else{
                    deleteStment.setString(1, item_code);
                    deleteStment.executeUpdate();
                    
                   insertStmt_NF.setString(1, item_code); 
                   insertStmt_NF.setString(2, upc); 
                   insertStmt_NF.executeUpdate();
                }
                    Product p=new Product();
                    p.setItem_Code(item_code);
                    p.setTitle(title);
     
                    p.setBrand(manu);                    
                    p.setAsin(asin);
                    p.setUpc(upc);
                    p.setBestprice(bestprice);
                    p.setSecondbestprice(secondbestprice);
                   p.setWHOLESALERNAME("COSTCO");
                    ProcessWorkingCatalog pw=new ProcessWorkingCatalog();
                    pw.storeProduct3(p, "amazon_catalog_costco_hb");
                 
            }
            
           con.close();        
    }
   public static String updateProduct1(String productid, JSONObject jSONObject) {
        String webPage = "https://harrisburgstore.com/api/v2/products/"+productid+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
             
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }            
   public static void loadCustomFields(String productid, JSONObject jSONObject)  throws Exception{
                WebClient webClient1 ;
               webClient1=new WebClient( );
        
            
                System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "debug");
                //webClient1.getOptions().setJ
                webClient1.getOptions().setThrowExceptionOnScriptError(false);
                 webClient1.getOptions().setJavaScriptEnabled(true);
                 webClient1.getOptions().setCssEnabled(false);
                 webClient1.waitForBackgroundJavaScript(15000);
                 
                 
                HtmlPage page = webClient1.getPage("https://store-0p5zzjir.mybigcommerce.com/admin/");
                //System.out.println(page.asText());
                ((HtmlTextInput) page.getElementById("user_email")).setText("mprabagar80@gmail.com");
                ((HtmlPasswordInput) page.getElementById("user_password")).setText("Dummy123");
                
                 System.out.println("Signing In");
                  page=((HtmlSubmitInput)page.getByXPath("//input[@type='submit']").get(0)).click();
                  webClient1.getOptions().setThrowExceptionOnScriptError(true);
                 page= webClient1.getPage("https://store-0p5zzjir.mybigcommerce.com/admin/index.php?ToDo=editProduct&productId=25204&returnUrl=index.php%3FToDo%3DviewProducts&current_tab=custom-fields");
                 System.out.println("page xml:"+page.asXml())   ;
                 
                 page.executeJavaScript("product.attributes.custom_fields[0].value='JUNK';");
                 page=((HtmlButton )page.getByXPath("//button[@id='save-product-button']").get(0)).click();
                 
                  
                  /*
                
                
                
                
                
        String webPage = "https://store-0p5zzjir.mybigcommerce.com/api/v2/products/"+productid+"/custom_fields";
        String token = "807c960064a4ef27b5516c7e53c842c1f41a1f7e";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpsURLConnection  httpCon = (HttpsURLConnection ) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("POST");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
             
//            String strJString = jSONObject.toString();
           // System.out.println("Request : "+strJString);
            out.write("{sn=C02G8416DRJM}");
            out.flush();
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        */
    }  
   public static void updateSkuCostcoTables() throws Exception{
         Connection con = RetrievePage.getConnection();
         PreparedStatement ps=con.prepareStatement("select c1,c2,substring(c1,1,2),cast(substring(c1,3,length(c1)) as decimal) from  hb_bc.costco_input_catalog  order by substring(c1,1,2), cast(substring(c1,3,length(c1)) as decimal)");
         PreparedStatement ps1=con.prepareStatement("update hb_bc.costco_input_catalog set rowlabel=? where tajplaza.cleanupc(c2)=tajplaza.cleanupc(?)");
         PreparedStatement ps2=con.prepareStatement("update  hb_bc.costco_catalog set item_code=? where tajplaza.cleanupc(upc)=tajplaza.cleanupc(?)");
         ResultSet rs=ps.executeQuery();
         int groupLabel=0;
         
          int count=1;
         while(rs.next()){
              String rowLabel=rs.getString("c1");
           String upc=rs.getString("c2");
           String initials=rs.getString(3);
           Integer initials_count=rs.getInt(4);
            if(upc.length()<9 ){
                System.out.println("Changing grouplabel:"+groupLabel +" at upc:"+upc);
                    groupLabel++;
                    continue;
                }
           String item_code="CO_"+String.format("%03d", groupLabel)+"_"+initials+"_"+String.format("%04d", initials_count)+"_"+String.format("%05d", count++);
           ps1.setString(1, String.valueOf(groupLabel));
           ps1.setString(2, upc);
           ps1.executeUpdate();
           
           System.out.println("Updating original c1:"+rowLabel+",c2"+upc+", new item_code:"+item_code);
           ps2.setString(1, item_code);
           ps2.setString(2, upc);
           ps2.executeUpdate();
         }
                     
    }
}
