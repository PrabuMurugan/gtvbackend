/*
 * Processes XHTML content
 * Variables must be passed with tags in a namespace marked in the namespace annotation on the javabean passed
 * The raw tagnames would be used to indentify variable names and they would be replaced by their values
 * 
 * The XHTML is processed using fast StAX api
 */
package mailer;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 *
 * @author debasish
 */
public class MailingTemplateProcessor {
    public void loadTemplate(URL templateURL, Object dataBean){
        Class dataClass = dataBean.getClass();
        MailTemplateData templateDataAnnot =  (MailTemplateData) dataClass.getAnnotation(MailTemplateData.class);
        if(templateDataAnnot == null){
            throw new IllegalArgumentException("The dataBean passed is not a email variable");
        }
        
    }
    
    private static class TemplateProcessorSAXHandler implements ContentHandler{
        private PrintWriter output;
        private Map<String, String> prefixMapping = new HashMap<String, String>();
        public TemplateProcessorSAXHandler(PrintWriter output){
            this.output = output;
        }
        @Override
        public void setDocumentLocator(Locator locator) {
            
        }

        @Override
        public void startDocument() throws SAXException {
            output.println("<?xml version='1.0' encoding='UTF-8' ?>");
        }

        @Override
        public void endDocument() throws SAXException {
            
        }

        @Override
        public void startPrefixMapping(String prefix, String uri) throws SAXException {
            prefixMapping.put(uri, prefix);
        }

        @Override
        public void endPrefixMapping(String prefix) throws SAXException {
            
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
            //output.print();
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            
        }

        @Override
        public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
            
        }

        @Override
        public void processingInstruction(String target, String data) throws SAXException {
           
        }

        @Override
        public void skippedEntity(String name) throws SAXException {
           
        }

       
        
        
    }
}
