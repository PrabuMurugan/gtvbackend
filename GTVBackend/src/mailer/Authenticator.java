/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mailer;

import javax.mail.PasswordAuthentication;

/**
 *
 * @author debasish
 */
public class Authenticator extends javax.mail.Authenticator{
    private String username;
    private String password;
    public Authenticator(String username, String password){
        this.username = username;
        this.password = password;
    }
    
    public PasswordAuthentication getPasswordAuthentication()
    {
        return new PasswordAuthentication(username, password);
    }
}
