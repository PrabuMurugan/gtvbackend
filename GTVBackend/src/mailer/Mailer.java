/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mailer;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author debasish
 */
public class Mailer {

    public static void sendMail(MailData mailData) throws MessagingException {
        Properties properties = System.getProperties();

        Properties props = new Properties();

        System.setProperty("javax.net.debug", "ssl,handshake");
        props.put("mail.smtp.host", mailData.getHost());
        if(mailData.getPort()>0){
            props.put("mail.smtp.host", mailData.getPort());
        }
        props.put("mail.smtp.user", mailData.getUsername());
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.protocols", "SSLv3 TLSv1");

        Session session = Session.getInstance(props, new Authenticator(mailData.getUsername(), mailData.getPassword()));

        session.setDebug(true);


        // Create a default MimeMessage object.
        MimeMessage message = new MimeMessage(session);


        // Set From: header field of the header.
        message.setFrom(new InternetAddress(mailData.getFrom()));

        // Set To: header field of the header.
        for(String to:mailData.getTo()){
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));
        }
        
        for(String to:mailData.getCc()){
            message.addRecipient(Message.RecipientType.CC,
                    new InternetAddress(to));
        }

        // Set Subject: header field
        message.setSubject(mailData.getSubject());

        // Now set the actual message
        message.setContent(mailData.getBody(), mailData.getContentType());

        // Send message
        Transport.send(message);
        System.out.println("Sent message successfully....");

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // Recipient's email ID needs to be mentioned.
        String to = "debasish.raychawdhuri@gmail.com";

        // Sender's email ID needs to be mentioned
        String from = "Harrisburg<storeharrisburg@gmail.com>";

        // Assuming you are sending email from localhost
        String host = "smtp.gmail.com";

        String user = "storeharrisburg@gmail.com";

        String pass = "tp123456";

        MailData mailData = new MailData();
        mailData.setBody("<html><head><style>h1{color:red}</style></head><body><h1>Oh!</h1></body><html>");
        mailData.setContentType("text/html");
        mailData.setFrom(from);
        mailData.setHost(host);
        mailData.setSubject("Test mail");
        mailData.setTo(new String[]{to});
        
        mailData.setUsername(user);
        mailData.setPassword(pass);
        
        sendMail(mailData);
    }
}
