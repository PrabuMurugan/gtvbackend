/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mailer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author debasish
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface MailTemplateData {
    public String uri() default "http://harrisburgstore.com/xhtml/vars";
}
