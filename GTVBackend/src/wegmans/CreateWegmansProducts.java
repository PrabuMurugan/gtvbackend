package wegmans;

 
import amazon.product.CreateCatalogNew;
import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
 
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;
 
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author prabu
 */
public class CreateWegmansProducts {
     public static void main(String args[]) throws Exception{
         Connection con=RetrievePage.getConnection();
         PreparedStatement ps=con.prepareStatement(" select url,aisle,price from hb_bc.wegmans where url not in (select url from hb_bc.wegmans_product) and aisle not in ('0B','Dairy','Fresh Bakery','Market Cafe','Olive Bar','Meat Case')  and length(price) > 0 and price not like '%lb%' "
                 + " ");
         PreparedStatement insert=con.prepareStatement("insert into hb_bc.wegmans_product (url,title,img_src,aisle,desc1,price,gtin,size,"
                 + " ingredients,bb1,bb2,bb3,bb4,weight_lbs) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
         
         ResultSet rs=ps.executeQuery();
          HtmlPage page;
         /*CreateCatalog.JAVASCRIPT=true;
         HtmlPage page=CreateCatalog.getPage("http://www.wegmans.com/webapp/wcs/stores/servlet/SearchResults?N=0");
         
                 
         DomElement elem=((DomElement)page.getByXPath("//input[@id='emailModal']").get(0));
         elem.setAttribute("value", "mprabagar80@gmail.com");

          elem=((DomElement)page.getByXPath("//input[@id='logonPasswordModal']").get(0));
         elem.setAttribute("value", "dummy123");
         
         
         
          elem=((DomElement)page.getByXPath("//input[@id='form-sign-in']").get(0));
         page=elem.click();*/
         CreateCatalog.JAVASCRIPT=false;
         RetrievePage.USE_PROXIES=false;
         int processingCount=0;
         while(rs.next()){
             System.out.println("processingCount:"+processingCount++);
             String title="",img_src="",aisle="",desc1="",price="",gtin="",size="",ingredients="",bb1="",bb2="",bb3="",bb4="";
             int weight_lbs= 5;
            String  url=rs.getString("url");
              aisle=rs.getString("aisle");
              price=rs.getString("price");
              price=price.replaceAll("\\$", "");
              price=price.replaceAll("\\*", "");
             price = price.replaceAll("[^\\x00-\\x7F]", "");
             Double priceF=Double.valueOf("0");
             try{
             if(price.indexOf("/")>0)
                 priceF=Double.valueOf(StringUtils.substringAfter(price, "/"))/Double.valueOf(StringUtils.substringBefore(price, "/"));
              else
                 priceF=Math.ceil(Double.valueOf(price));
             }catch (Exception e2){e2.printStackTrace();}
           // url+="&storeId=10052";
            
           
            page=CreateCatalog.getPage1(url);
            if(page.getByXPath("//h1").size()>0){
                title=((DomElement)page.getByXPath("//h1").get(0)).asText();
            }
            if(page.getByXPath("//div[@id='dialog-modal']//img").size()>0){
                img_src=((DomElement)page.getByXPath("//div[@id='dialog-modal']//img").get(0)).getAttribute("src");
                if(img_src.indexOf("http://wegmans.com")<0)
                    img_src="http://wegmans.com"+img_src;
            }  
            if(page.getByXPath("//td[@class='prodSubhead']").size()>0 && 
                    ((DomElement)page.getByXPath("//td[@class='prodSubhead']").get(0)).asText().indexOf("Aisle")>=0){
                aisle=((DomElement)page.getByXPath("//td[@class='prodSubhead']").get(0)).asText();
                aisle=aisle.replace("Aisle/Location:", "");
            } 
            if(page.getByXPath("//div[@class='sfProductDetail-box-content']").size()>0  ){
                desc1=((DomElement)page.getByXPath("//div[@class='sfProductDetail-box-content']").get(0)).asText();
                desc1=desc1.replaceAll("\r\n"," ");
                desc1=desc1.replaceAll("Visit us at wegmans.com.", "");
                desc1=desc1.replaceAll("Visit us at Wegmans.com.", "");
                desc1=desc1.replaceAll("Go to wegmans.com for recipes.", "");
                
                String[] fullstopArr=StringUtils.split(desc1, ".");
                int fullStopBy4=fullstopArr.length/4;
                for(int i=0;i<fullstopArr.length;i++){
                    if(i<fullStopBy4)
                        bb1+=fullstopArr[i]+".";
                    else if(i<2*fullStopBy4)
                        bb2+=fullstopArr[i]+".";
                    else if(i<3*fullStopBy4)
                        bb3+=fullstopArr[i]+".";
                    else if(i<4*fullStopBy4)
                        bb4+=fullstopArr[i]+".";
                    
                }
            }
            if(page.getByXPath("//div[@class='sfProductDetail-box-content']//b").size()>0  ){
                size=((DomElement)page.getByXPath("//div[@class='sfProductDetail-box-content']//b").get(0)).asText();
                
                    try{
                        if(size.indexOf("oz")>0&& size.indexOf("fl")<0 ){
                            title=title+ " ("+size +")";
                            Float weight_lbsF=Float.valueOf(StringUtils.substringBefore(size, "oz"))/16+1;
                            weight_lbs=(int)Math.ceil(weight_lbsF);
                            
                        }
                        else if(size.indexOf("fl")>0 ){
                            title=title+ " ("+size +")";
                            Float weight_lbsF=Float.valueOf(StringUtils.substringBefore(size, "fl"))/16+1;
                            weight_lbs=(int)Math.ceil(weight_lbsF);
                            
                        }
                        
                }catch (Exception e2){e2.printStackTrace();}
            } 
           /* Price coming from wegmans table through chrome plugin
             * if(page.getByXPath("//span[@class='mrkAfltwd20new']").size()>0 ){
                price=((DomElement)page.getByXPath("//span[@class='mrkAfltwd20new']").get(0)).asText();
                price=price.replace("$", "");
            }*/            
            if(page.getByXPath("//span[@itemprop='gtin14']").size()>0 ){
                gtin=((DomElement)page.getByXPath("//span[@itemprop='gtin14']").get(0)).asText();
            } 
            insert.setString(1, url);
            insert.setString(2, title);
            insert.setString(3, img_src);
            insert.setString(4, aisle);
            if(desc1.length()>1999)
                desc1=desc1.substring(0, 1999);
            insert.setString(5, desc1);
            insert.setString(6, String.valueOf(priceF).replaceAll(".0", ""));

            insert.setString(7, gtin);
            insert.setString(8, size);
            insert.setString(9, ingredients);
            if(bb1.length()>499)
                bb1=bb1.substring(0, 499);
            if(bb2.length()>499)
                bb2=bb2.substring(0, 499);
            if(bb3.length()>499)
                bb3=bb3.substring(0, 499);
            if(bb4.length()>499)
                bb4=bb4.substring(0, 499);
            
            insert.setString(10, bb1);
            insert.setString(11, bb2);
            insert.setString(12, bb3);
            insert.setString(13, bb4);
            insert.setInt(14, weight_lbs);
            
            insert.executeUpdate();;
            
         }
         con.close();
     }
}
