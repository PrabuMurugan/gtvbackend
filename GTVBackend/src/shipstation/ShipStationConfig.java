/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shipstation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;


/**
 *
 * @author Raj
 */
public class ShipStationConfig {
    public static final String USERNAME= "b024b26d59694565a223120bbbc2ecac";
    public static final String PASSWORD = "b0741dc0a06344b59fc7f01feabb3459";
    public static final String SERVICE_REFRESH_STORE = "https://ssapi.shipstation.com/stores/refreshstore/";
    public static final String SERVICE_UPDATE_ORDER_ADD_TAG = "https://ssapi.shipstation.com/orders/addtag/";
    public static final String SERVICE_UPDATE_ORDER_HOLD_UNTIL = "https://ssapi.shipstation.com/orders/holduntil/";
    public static final String SERVICE_ORDER_DETAIL = "https://ssapi.shipstation.com/orders?orderNumber=%s";

    
    private InputStream inputStream;
    private OutputStream outputStream;
    private HttpURLConnection httpURLConnection;
    private URL url;
    private boolean bOutPut = false;
    public ShipStationConfig(String strService, String strRequestMethod, boolean doOutput) {
        try {
            this.bOutPut = doOutput;
            url = new URL(strService);
            
            String authString = ShipStationConfig.USERNAME + ":" + ShipStationConfig.PASSWORD;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
            if(doOutput) httpURLConnection.setDoOutput(doOutput);
            httpURLConnection.setRequestMethod(strRequestMethod);
            
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestProperty("Accept", "application/json");    
                    
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   
    public InputStream getInputStream() {
        try {
            return httpURLConnection.getInputStream();
        } catch (IOException ex) {
            Logger.getLogger(ShipStationConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public OutputStream getOutputStream() {
        try {
            return httpURLConnection.getOutputStream();
        } catch (IOException ex) {
            Logger.getLogger(ShipStationConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void setOutputStream(OutputStream outputStream) {
        this.outputStream = outputStream;
    }
    
}
