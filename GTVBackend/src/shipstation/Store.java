/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shipstation;

/**
 *
 * @author Raj
 */
public class Store {
    private String storeId;
    private String refreshStatusId;
    private String refreshStatus;
    private String refreshDate;
    private String lastRefreshAttempt;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getRefreshStatusId() {
        return refreshStatusId;
    }

    public void setRefreshStatusId(String refreshStatusId) {
        this.refreshStatusId = refreshStatusId;
    }

    public String getRefreshStatus() {
        return refreshStatus;
    }

    public void setRefreshStatus(String refreshStatus) {
        this.refreshStatus = refreshStatus;
    }

    public String getRefreshDate() {
        return refreshDate;
    }

    public void setRefreshDate(String refreshDate) {
        this.refreshDate = refreshDate;
    }

    public String getLastRefreshAttempt() {
        return lastRefreshAttempt;
    }

    public void setLastRefreshAttempt(String lastRefreshAttempt) {
        this.lastRefreshAttempt = lastRefreshAttempt;
    }
    
}
