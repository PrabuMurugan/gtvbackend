/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shipstation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Raj
 */
public class RefreshShipStationStores {

    public static void main(String[] args) {
        try {

            URL uri = new URL(ShipStationConfig.SERVICE_REFRESH_STORE);
            
            String authString = ShipStationConfig.USERNAME + ":" + ShipStationConfig.PASSWORD;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            System.out.println(authStringEnc);
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            //httpCon.setDoOutput(true);
            httpCon.setRequestMethod("GET");
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            
            StringBuilder sb1 = new StringBuilder();
            String line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            System.out.println("Now We will inteate through all stores and will check weather all stores have refresh status is completed?");
            String [] stores={"23632","23633","23636"};
            for(int i=0;i<stores.length;i++) {
                    while(true) {
                        uri = new URL("https://ssapi.shipstation.com/stores/getrefreshstatus?storeId="+stores[i]);
                        httpCon = (HttpURLConnection) uri.openConnection();
                        httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
                        httpCon.setRequestMethod("GET");
                        responseIS = httpCon.getInputStream();
                        reader = new BufferedReader(new InputStreamReader(responseIS));
                        sb1 = new StringBuilder();
                        line = reader.readLine();
                        while (line != null) {
                            sb1.append(line);
                            line = reader.readLine();
                        }
                        System.out.println("Response : " + new String(sb1));
                        Gson gson = new  GsonBuilder().create();
                        Store store =  gson.fromJson(new String(sb1),Store.class);
                        if(store.getRefreshStatusId().equalsIgnoreCase("0"))
                              break;
                        else {
                            System.out.println("Store is refreshing....Please wait");
                            Thread.sleep(30000);
                        }
                    }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
