/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package patel;

 

import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
public class ProcessPatelSiteMapXML {
     static Connection con=null;
    public static void main(String[] args){
try {
        con=RetrievePage.getConnection();
	File fXmlFile = new File("C:\\Google Drive\\Dropbox\\tmp\\patekbros-xmlsitemap.php.xml");
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	Document doc = dBuilder.parse(fXmlFile);
			
	//optional, but recommended
	//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
	doc.getDocumentElement().normalize();

	System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			
	NodeList nList = doc.getElementsByTagName("url");
			
	System.out.println("----------------------------");
        visitPatelProduct ("http://store.patelbros.com/swad-gluten-free-chappati-flour/");
	for (int temp = 0; temp < nList.getLength(); temp++) {

		Node nNode = nList.item(temp);
				
		System.out.println("\nCurrent Element :" + nNode.getNodeName());
				
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {

			Element eElement = (Element) nNode;
                        String url=eElement.getElementsByTagName("loc").item(0).getTextContent();
			System.out.println("First Name : " +url );
			 visitPatelProduct(url);

		}
	}
        con.close();
    } catch (Exception e) {
	e.printStackTrace();
    }
  }
   
    private static void visitPatelProduct(String url) throws Exception {
        String title="",img_src="",aisle="",desc1="",price="",gtin="",size="",ingredients="",bb1="",bb2="",bb3="",bb4="",breadcrumb="",brand="Swad";
        int weight_lbs=3;
        RetrievePage.JAVASCRIPT=true;
       HtmlPage p=CreateCatalog.getPage1(url) ;

       
       title=((DomElement)p.getByXPath("//title").get(0)).asText();
       title=title.replaceAll(" - Patel Brothers, Inc.", "");
       System.out.println("Title:"+title);
       
       //Get all iamges
      
       boolean imageExists=false;
       ArrayList<DomElement> arr=(ArrayList<DomElement>) p.getByXPath("//a");
       for(int i=0;i<arr.size();i++){
           DomElement elem=arr.get(i);
           if(elem.hasAttribute("rel") && elem.getAttribute("rel").indexOf("largeimage")>=0){
               JSONObject json=JSONObject.fromObject(elem.getAttribute("rel"));
               if(img_src.length()>0)
                   img_src=img_src+",";
               img_src=img_src+json.get("largeimage");
               imageExists=true;
           }
       }
       if(imageExists==false)
           return;
               
       desc1=p.getElementById("ProductDescription").asText();
       arr=(ArrayList<DomElement>) p.getByXPath("//div[@id=\"ProductBreadcrumb\"]//li");
       
       for(int x=0;x<arr.size();x++){
           if(breadcrumb.length()>0)
               breadcrumb=breadcrumb+">";
           breadcrumb=breadcrumb+arr.get(x).asText();
           
       }
           
       arr=(ArrayList<DomElement>) p.getByXPath("//span[@itemprop=\"weight\"]");
       size="";
       if(arr.size()>0)
           size=arr.get(0).asText();
       if(size.indexOf("Ounces")>=0){
           Float f=Float.valueOf(size.replaceAll("Ounces", ""));
           weight_lbs=(int) Math.ceil(f/16)+1;
       }
       arr=(ArrayList<DomElement>) p.getByXPath("//em[@itemprop=\"price\"]");
       
       if(arr.size()>0)
           price=arr.get(0).asText();
        arr=(ArrayList<DomElement>) p.getByXPath("//div[@itemprop=\"brand\"]");       
       if(arr.size()>0)
           brand=arr.get(0).asText();

       arr=(ArrayList<DomElement>) p.getByXPath("//span[@itemprop=\"sku\"]");       
       if(arr.size()>0){
           gtin=arr.get(0).asText();
        if(gtin.length() ==10) {
            System.out.println("[ CreateCatalog ] Before check digit " + gtin);
            System.out.println("[ CreateCatalog ] Now before adding check digit. Let us check if adding leading Zero gives any result? If yes the no need to add check digit");
             gtin= "0"+gtin; 
            }
        if(gtin.length() ==11) {
            gtin  = CreateCatalog.addCheckDigit(gtin);            
        }        
            
            System.out.println("UPC:"+gtin);
           
       }
            System.out.println("[ CreateCatalog ] after check digit " + gtin);
            
                
        String[] fullstopArr=StringUtils.split(desc1, ".");
        int fullStopBy4=fullstopArr.length/4;
        if(fullStopBy4==0)
            bb1=desc1;
        for(int i=0;i<fullstopArr.length;i++){
            if(i<fullStopBy4)
                bb1+=fullstopArr[i]+".";
            else if(i<2*fullStopBy4)
                bb2+=fullstopArr[i]+".";
            else if(i<3*fullStopBy4)
                bb3+=fullstopArr[i]+".";
            else if(i<4*fullStopBy4)
                bb4+=fullstopArr[i]+".";

        }
        String origTitle=title;
       if(p.getByXPath("//div[@class=\"productOptionViewRadio\"]//li").size()>0){
           for(int i=0;i<p.getByXPath("//div[@class=\"productOptionViewRadio\"]//li//span").size();i++){
               
               //Lets do a post to the variation
               String product_id=((DomElement)(p.getByXPath("//input[@name=\"product_id\"]").get(0))).getAttribute("value");
               
               String attribute_name=((DomElement)(p.getByXPath("//div[@class=\"productOptionViewRadio\"]//li//input").get(i))).getAttribute("name");
               String attribute_value=((DomElement)(p.getByXPath("//div[@class=\"productOptionViewRadio\"]//li//input").get(i))).getAttribute("value");
               
               
           URL url1 = new URL("http://store.patelbros.com/remote.php");
            WebRequest requestSettings = new WebRequest(url1, HttpMethod.POST);

            requestSettings.setAdditionalHeader("Accept", "*/*");
            requestSettings.setAdditionalHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            requestSettings.setAdditionalHeader("Referer", "REFURLHERE");
            requestSettings.setAdditionalHeader("Accept-Language", "en-US,en;q=0.8");
            requestSettings.setAdditionalHeader("Accept-Encoding", "gzip,deflate,sdch");
            requestSettings.setAdditionalHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3");
            requestSettings.setAdditionalHeader("X-Requested-With", "XMLHttpRequest");
            requestSettings.setAdditionalHeader("Cache-Control", "no-cache");
            requestSettings.setAdditionalHeader("Pragma", "no-cache");
            requestSettings.setRequestParameters(new ArrayList());
           requestSettings.getRequestParameters().add(new NameValuePair("action", "add"));
           requestSettings.getRequestParameters().add(new NameValuePair("product_id", product_id));
           requestSettings.getRequestParameters().add(new NameValuePair(attribute_name, attribute_value));
           requestSettings.getRequestParameters().add(new NameValuePair("qty[]", "1"));
           requestSettings.getRequestParameters().add(new NameValuePair("w", "getProductAttributeDetails"));
           
           
            WebClient webClient=new WebClient();
            Page redirectPage = webClient.getPage(requestSettings);
            System.out.println("Page "+redirectPage.getWebResponse().getContentAsString());
            
             
               DomElement elem=(DomElement) p.getByXPath("//div[@class=\"productOptionViewRadio\"]//li//span").get(i);
               String product_size=elem.asText();
               title=origTitle+" ("+product_size+")";     
               
               JSONObject jsobj=null;
               if(redirectPage.getWebResponse().getContentAsString().length()>5)
               jsobj=JSONObject.fromObject(redirectPage.getWebResponse().getContentAsString());
               if(jsobj!=null && jsobj.getJSONObject("details").containsKey("sku"))
                   gtin=jsobj.getJSONObject("details").getString("sku");
            if(gtin.length() ==10) {
                System.out.println("[ CreateCatalog ] Before check digit " + gtin);
                System.out.println("[ CreateCatalog ] Now before adding check digit. Let us check if adding leading Zero gives any result? If yes the no need to add check digit");
                 gtin= "0"+gtin; 
                }
            if(gtin.length() ==11) {
                gtin  = CreateCatalog.addCheckDigit(gtin);            
            } 
            if(jsobj!=null && jsobj.getJSONObject("details").containsKey("price"))
             price=jsobj.getJSONObject("details").getString("price");
            if(jsobj!=null && jsobj.getJSONObject("details").containsKey("image"))
                img_src=jsobj.getJSONObject("details").getString("image");
        //LEts insert
         PreparedStatement insert=con.prepareStatement("insert into hb_bc.bb_product (url,title,img_src,aisle,desc1,price,gtin,size,"
                 + " ingredients,bb1,bb2,bb3,bb4,weight_lbs,breadcrumb,brand) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            insert.setString(1, url);
            insert.setString(2, title);
            if(img_src.length()>9999)
                img_src=img_src.substring(0,9998);
            insert.setString(3, img_src);
            insert.setString(4, aisle);
            if(desc1.length()>1999)
                desc1=desc1.substring(0, 1999);
            insert.setString(5, desc1);
            insert.setString(6,price);

            insert.setString(7, gtin);
            insert.setString(8, size);
            insert.setString(9, ingredients);
            if(bb1.length()>499)
                bb1=bb1.substring(0, 499);
            if(bb2.length()>499)
                bb2=bb2.substring(0, 499);
            if(bb3.length()>499)
                bb3=bb3.substring(0, 499);
            if(bb4.length()>499)
                bb4=bb4.substring(0, 499);
            
            insert.setString(10, bb1);
            insert.setString(11, bb2);
            insert.setString(12, bb3);
            insert.setString(13, bb4);
            insert.setInt(14, weight_lbs);
            insert.setString(15, breadcrumb);
            brand=brand.replaceAll("Brand:", "").replaceAll("\r", "").replaceAll("\n", "");
            insert.setString(16, brand);
            
            insert.executeUpdate();;               
           }

       }else{
        //LEts insert
         PreparedStatement insert=con.prepareStatement("insert into hb_bc.bb_product (url,title,img_src,aisle,desc1,price,gtin,size,"
                 + " ingredients,bb1,bb2,bb3,bb4,weight_lbs,breadcrumb,brand) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            insert.setString(1, url);
            insert.setString(2, title);
            if(img_src.length()>9999)
                img_src=img_src.substring(0,9998);
            insert.setString(3, img_src);
            insert.setString(4, aisle);
            if(desc1.length()>1999)
                desc1=desc1.substring(0, 1999);
            insert.setString(5, desc1);
            insert.setString(6, price);

            insert.setString(7, gtin);
            insert.setString(8, size);
            insert.setString(9, ingredients);
            if(bb1.length()>499)
                bb1=bb1.substring(0, 499);
            if(bb2.length()>499)
                bb2=bb2.substring(0, 499);
            if(bb3.length()>499)
                bb3=bb3.substring(0, 499);
            if(bb4.length()>499)
                bb4=bb4.substring(0, 499);
            
            insert.setString(10, bb1);
            insert.setString(11, bb2);
            insert.setString(12, bb3);
            insert.setString(13, bb4);
            insert.setInt(14, weight_lbs);
            insert.setString(15, breadcrumb);
            insert.setString(16, brand);
            
            insert.executeUpdate();;           
       }
               


         
    }            
       
       
    
    
}
