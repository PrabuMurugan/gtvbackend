/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.internet.InternetAddress;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import util.DB;

/**
 *
 * @author abanipatra
 */
public class SendShippingConfirmationMail {

    public static void main(String[] args) throws IOException, Exception {
        Connection con = DB.getConnection(null);
        try{
        PreparedStatement prest = con.prepareStatement("select  * from order_email_status where status!=? AND email_type=?");
        prest.setString(1, "sent");
        prest.setString(2, "Shipping_Confirmation");
        ResultSet rs = prest.executeQuery();
        while (rs.next()) {

            long order_id = Long.valueOf(rs.getString("order_id"));
            long order_status_id = rs.getLong("id");
            PreparedStatement order_prest = con.prepareStatement("select * from orders where id=?");
            order_prest.setLong(1, order_id);
            ResultSet order_rs = order_prest.executeQuery();
            while (order_rs.next()) {
                String toEmail = order_rs.getString("email");
                Map<String, Object> rootMap = new HashMap<String, Object>();
                rootMap.put("name", order_rs.getString("name"));

                JSONObject shipping_address_JSON = JSONObject.fromObject(order_rs.getString("shipping_address"));
                rootMap.put("shipping_address", shipping_address_JSON);
                rootMap.put("created_at", order_rs.getDate("created_at"));
                JSONObject customerJSON = JSONObject.fromObject(order_rs.getString("customer"));
                customerJSON.put("name", customerJSON.get("first_name") + " " + customerJSON.get("last_name"));
                rootMap.put("subtotal_price", order_rs.getString("subtotal_price"));
                rootMap.put("total_price", order_rs.getString("total_price"));
                JSONArray shippingJSON = JSONArray.fromObject(order_rs.getString("shipping_lines"));
                JSONObject shipping_JSONObject = (JSONObject) shippingJSON.get(0);
                rootMap.put("shipping_price", shipping_JSONObject.getString("price"));
                String track_url = "#";
                if ("USPS".equals(order_rs.getString("tracking_company"))) {
                    track_url = "https://tools.usps.com/go/TrackConfirmAction_input?origTrackNum=" + order_rs.getString("tracking_number");
                } else if ("UPS".equals(order_rs.getString("tracking_company"))) {
                    track_url = "http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=" + order_rs.getString("tracking_number");
                } else if ("FedEx".equals(order_rs.getString("tracking_company"))) {
                    track_url = "http://www.fedex.com/Tracking?action=track&tracknumbers=" + order_rs.getString("tracking_number");
                } else if ("DHL".equals(order_rs.getString("tracking_company"))) {
                    track_url = "http://www.dhl.com/content/g0/en/express/tracking.shtml?brand=DHL&AWB=" + order_rs.getString("tracking_number");
                }
                rootMap.put("customer", customerJSON);
                rootMap.put("track_url", track_url);
                rootMap.put("shop_name", "Harrisburg Store");
                PreparedStatement line_item_prest = con.prepareStatement("select loi.quantity as quantity, loi.title as title, loi.price as price, pr.image as image from order_line_items as loi, products as pr where order_id=? AND loi.sku=pr.sku");
                line_item_prest.setLong(1, order_id);
                ResultSet orderitem_rs = line_item_prest.executeQuery();
                List<OrderItem> order_items = new ArrayList<OrderItem>();

                while (orderitem_rs.next()) {
                    OrderItem orderItem = new OrderItem();
                    orderItem.setTitle(orderitem_rs.getString("title"));
                    orderItem.setQuantity(orderitem_rs.getLong("quantity"));
                    orderItem.setImage(orderitem_rs.getString("image"));
                    orderItem.setPrice(orderitem_rs.getDouble("price"));
                    order_items.add(orderItem);
                }
                rootMap.put("line_items", order_items);
                boolean result;
                System.out.println("Start Mail Sending.....");
                if(System.getProperty("BCC_Email") != null){
                    Address[] address = new Address[1];
                    address[0] =  new InternetAddress(System.getProperty("BCC_Email"));
                    System.out.println("Mail will be send with BCC:" + System.getProperty("BCC_Email"));
                    result = SendMail.sendMail("html-shipping-confirmation-mail-template.ftl", toEmail, rootMap, "Harrisburgstore.com: Your order has shipped - Order "+order_rs.getString("name"), address);
                }else{
                    result = SendMail.sendMail("html-shipping-confirmation-mail-template.ftl", toEmail, rootMap, "Harrisburgstore.com: Your order has shipped - Order "+order_rs.getString("name"));
                }
                if (result) {
                    PreparedStatement update_status_prest = con.prepareStatement("update order_email_status set  status=? where id=?");
                    update_status_prest.setString(1, "sent");
                    update_status_prest.setLong(2, order_status_id);
                    update_status_prest.executeUpdate();
                }
            }

        }
        }catch(Exception e){
            Logger.getLogger(SendShippingConfirmationMail.class.getName()).log(Level.SEVERE, null, e);
        }finally{
            con.close();
        }
    }
}
