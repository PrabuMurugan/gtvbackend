/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import freemarker.template.Configuration;
import freemarker.template.Template;
import javax.mail.Address;

/**
 *
 * @author abanipatra
 */
public class SendMail_1 {

    public static boolean sendMail(String templateName, String toEmail, Map<String, Object> templateData, String subject) throws Exception {
        final Properties props = new Properties();
        try {
            //props.load(new FileInputStream(new File("mail-config.properties")));
           props.load(SendMail.class.getClassLoader().getResourceAsStream("mail-config.properties"));
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        Session session = Session.getDefaultInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                props.getProperty("mail.smtp.user"), props.getProperty("mail.smtp.password"));
                    }
                });

        try {
            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(props.getProperty("mail.smtp.user")));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toEmail));
            message.setSubject(subject);

            BodyPart body = new MimeBodyPart();

            // freemarker stuff.
            Configuration cfg = new Configuration();
            cfg.setClassForTemplateLoading(SendMail.class, "/");
            Template template = cfg.getTemplate(templateName);

            Writer out = new StringWriter();
            template.process(templateData, out);
            // freemarker stuff ends.

            /* you can add html tags in your text to decorate it. */
            body.setContent(out.toString(), "text/html");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(body);

            message.setContent(multipart, "text/html");

            Transport.send(message);
            System.out.println("Mail Sent....");
            return true;

        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }

       
        
    }
    
    public static boolean sendMail(String templateName, Address[] toEmails, Map<String, Object> templateData, String subject,  MimeBodyPart attachment) throws Exception {
        final Properties props = new Properties();
        try {
            //props.load(new FileInputStream(new File("mail-config.properties")));
           props.load(SendMail.class.getClassLoader().getResourceAsStream("mail-config.properties"));
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        Session session = Session.getDefaultInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                props.getProperty("mail.smtp.user"), props.getProperty("mail.smtp.password"));
                    }
                });

        try {
            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(props.getProperty("mail.smtp.user")));
            message.setRecipients(Message.RecipientType.TO,
                    toEmails);
            message.setSubject(subject);

            BodyPart body = new MimeBodyPart();

            // freemarker stuff.
            Configuration cfg = new Configuration();
            cfg.setClassForTemplateLoading(SendMail.class, "/");
            Template template = cfg.getTemplate(templateName);

            Writer out = new StringWriter();
            template.process(templateData, out);
            // freemarker stuff ends.

            /* you can add html tags in your text to decorate it. */
            body.setContent(out.toString(), "text/html");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(body);

            message.setContent(multipart, "text/html");
            
            if(attachment != null)
                multipart.addBodyPart(attachment);

            Transport.send(message);
            System.out.println("Mail Sent....");
            return true;

        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }

       
        
    }
    
    /*
     * Send mail with BCC
     */    
     public static boolean sendMail(String templateName, String toEmail, Map<String, Object> templateData, String subject,Address[] bccEmail) throws Exception {
        final Properties props = new Properties();
        try {
            //props.load(new FileInputStream(new File("mail-config.properties")));
           props.load(SendMail.class.getClassLoader().getResourceAsStream("mail-config.properties"));
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        Session session = Session.getDefaultInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                props.getProperty("mail.smtp.user"), props.getProperty("mail.smtp.password"));
                    }
                });

        try {
            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(props.getProperty("mail.smtp.user")));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toEmail));
            message.setRecipients(Message.RecipientType.BCC,
                    bccEmail);
            message.setSubject(subject);

            BodyPart body = new MimeBodyPart();

            // freemarker stuff.
            Configuration cfg = new Configuration();
            cfg.setClassForTemplateLoading(SendMail.class, "/");
            Template template = cfg.getTemplate(templateName);

            Writer out = new StringWriter();
            template.process(templateData, out);
            // freemarker stuff ends.

            /* you can add html tags in your text to decorate it. */
            body.setContent(out.toString(), "text/html");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(body);

            message.setContent(multipart, "text/html");

            Transport.send(message);
            System.out.println("Mail Sent....");
            return true;

        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }

       
        
    }
}
