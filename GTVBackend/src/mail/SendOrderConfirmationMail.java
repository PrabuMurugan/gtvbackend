/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.internet.InternetAddress;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import util.DB;

/**
 *
 * @author abanipatra
 */
public class SendOrderConfirmationMail {

    public static void main(String[] args) throws IOException, Exception {
        Connection con = DB.getConnection(null);
        try{
        PreparedStatement prest = con.prepareStatement("select  * from order_email_status where status!=? AND email_type=?");
        prest.setString(1, "sent");
        prest.setString(2, "order_confirmation");
        ResultSet rs = prest.executeQuery();
        while (rs.next()) {

            long order_id = Long.valueOf(rs.getString("order_id"));
            long order_status_id = rs.getLong("id");

            PreparedStatement order_prest = con.prepareStatement("select * from orders where id=?");
            order_prest.setLong(1, order_id);
            ResultSet order_rs = order_prest.executeQuery();
            while (order_rs.next()) {
                String toEmail = order_rs.getString("email");
                Map<String, Object> rootMap = new HashMap<String, Object>();
                rootMap.put("name", order_rs.getString("name"));


                JSONObject shipping_address_JSON = JSONObject.fromObject(order_rs.getString("shipping_address"));
                rootMap.put("shipping_address", shipping_address_JSON);
                rootMap.put("created_at", order_rs.getDate("created_at"));
                JSONObject customerJSON = JSONObject.fromObject(order_rs.getString("customer"));
                customerJSON.put("name", customerJSON.get("first_name") + " " + customerJSON.get("last_name"));
                rootMap.put("subtotal_price", order_rs.getString("subtotal_price"));
                rootMap.put("total_price", order_rs.getString("total_price"));
                JSONArray shippingJSON = JSONArray.fromObject(order_rs.getString("shipping_lines"));
                JSONObject shipping_JSONObject = (JSONObject) shippingJSON.get(0);
                rootMap.put("shipping_price", shipping_JSONObject.getString("price"));
                rootMap.put("customer", customerJSON);
                rootMap.put("shop_name", "Harrisburg Store");
                boolean useBCC = false;
                boolean useTwoBCC = false;
                PreparedStatement line_item_prest = con.prepareStatement("select loi.quantity as quantity, loi.title as title, loi.price as price, pr.image as image from order_line_items as loi, products as pr where order_id=? AND loi.sku=pr.sku ");
                line_item_prest.setLong(1, order_id);
                ResultSet orderitem_rs = line_item_prest.executeQuery();
                List<OrderItem> order_items = new ArrayList<OrderItem>();
                PreparedStatement line_item_prest_for_bcc = con.prepareStatement("select b.total_units as total_units from order_line_items as loi, products as pr, balance_units b where order_id=? AND loi.sku=pr.sku and tajplaza.cleanupc(pr.barcode) = tajplaza.cleanupc(b.upc) and b.total_units>0 ");
                line_item_prest_for_bcc.setLong(1, order_id);
                ResultSet bcc_count_orderitem_rs = line_item_prest_for_bcc.executeQuery();
                if (bcc_count_orderitem_rs.next()) {
                    if (bcc_count_orderitem_rs.getInt("total_units") > 0) {
                        useBCC = true;
                    }
                }

                PreparedStatement line_item_prest_for_bcc2 = con.prepareStatement("select * from orders where id=? AND discount_codes != '[]' ");
                line_item_prest_for_bcc2.setLong(1, order_id);
                ResultSet bcc2_count_orderitem_rs = line_item_prest_for_bcc2.executeQuery();
                if (bcc2_count_orderitem_rs.next()) {
                          useTwoBCC = true;
                }
                while (orderitem_rs.next()) {
                    OrderItem orderItem = new OrderItem();
                    orderItem.setTitle(orderitem_rs.getString("title"));
                    orderItem.setQuantity(orderitem_rs.getLong("quantity"));
                    orderItem.setImage(orderitem_rs.getString("image"));
                    orderItem.setPrice(orderitem_rs.getDouble("price"));
                    order_items.add(orderItem);

                }
                rootMap.put("line_items", order_items);
                System.out.println("Start Mail Sending.....");
                boolean result = false;
                Set<String> bccEmils = new HashSet<String>();
                if (useBCC && useTwoBCC) {
                    bccEmils.add(System.getProperty("Review_BCC1", "vi_himanshu@varuninfotech.com"));
                    bccEmils.add(System.getProperty("DIS_BCC1", "vi_himanshu@varuninfotech.com"));
                    bccEmils.add(System.getProperty("DIS_BCC2", "arjun.patel4411@gmail.com"));
                } else if (useBCC) {
                    bccEmils.add(System.getProperty("Review_BCC1", "vi_himanshu@varuninfotech.com"));
                } else if (useTwoBCC) {
                    bccEmils.add(System.getProperty("DIS_BCC2", "vi_himanshu@varuninfotech.com"));
                    bccEmils.add(System.getProperty("DIS_BCC1", "arjun.patel4411@gmail.com"));
                }

                Address[] address = new Address[bccEmils.size()];
                int i = 0;
                for (String email : bccEmils) {
                    address[i] = new InternetAddress(email);
                    i++;
                }
                if (useBCC || useTwoBCC) {
             
                    System.out.println("Mail will be send with BCC:" + bccEmils+ "address"+address);
                    result = SendMail.sendMail("html-order-mail-template.ftl", toEmail, rootMap, "Harrisburgstore.com: Order Confirmation Email - Order "+order_rs.getString("name"), address);
                } else {
                    result = SendMail.sendMail("html-order-mail-template.ftl", toEmail, rootMap, "Harrisburgstore.com: Order Confirmation Email - Order "+order_rs.getString("name"));
                }
                if (result) {
                    PreparedStatement update_status_prest = con.prepareStatement("update order_email_status set  status=? where id=?");
                    update_status_prest.setString(1, "sent");
                    update_status_prest.setLong(2, order_status_id);
                    update_status_prest.executeUpdate();
                }
            }

        }
        }catch(Exception e){
            Logger.getLogger(SendOrderConfirmationMail.class.getName()).log(Level.SEVERE, null, e);
        }finally{
            con.close();
        }
    }
}
