/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dotfoods;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import util.DB;
//TO SELECT ITEMS FROM DOT TABLE
// select d1.dot_item_number,item_gtin,qty_in_parent,product_line_description,brand,gtin_upc_number,expanded_item_description,pack_size,case5000 from dotfoods_products_item d1 , dotfoods_products d2 where d1.Dot_Item_Number=d2.Dot_Item_Number order by d1.id desc
/**
 * CASE 1 : GET ITEMS WITH ITEM UPC 
 * create table dotfoods_product_each as select  distinct substring(d_each.gtin,-12) item_upc,d1.dot_item_number,expanded_item_description,IFNULL(d_inner.qty_in_parent,1)*IFNULL(d_each.qty_in_parent,1) packs_in_case,replace(case5000,'$',''), '' one_pack_price,'DOT FOODS',brand,pack_size  from harrisburgstore.dotfoods_products d1 left join harrisburgstore.dotfoods_products_level d_inner on d1.Dot_Item_Number = d_inner.Dot_Item_Number  and  d_inner.level='INNER PACK' left join harrisburgstore.dotfoods_products_level d_each on d1.Dot_Item_Number = d_each.Dot_Item_Number  and  d_each.level='EACH'  where d1.Dot_Item_Number in (select dot_item_number from harrisburgstore.dotfoods_products_level)   and d_each.gtin  is not null 
 * @author us083274
 */
public class DotLimitedAvailability {
     static String FILTER_ITEM="B0084LZU1Q";
    static ArrayList proceededDotNumbers=new ArrayList();
    public static void main (String args[]){
        System.out.println("NEW UpdateItemUPC");
      for(int i=0;i<1;i++){
          parse();
          
      }
   }
    public static int ERROR_COUNT=0;
    public static void parse(){
  Connection con =  null;
         String FILTER = System.getProperty("FILTER", "true");
        if(FILTER.equals("false")){
             FILTER_ITEM="";
        }              
        try {
            DB.CONNECT_TO_PROD_DEBUG=true;
            con = DB.getConnection("harrisburgstore");
            //PreparedStatement selectStmt = con.prepareStatement("select   Dot_Item_Number from dotfoods_products  where    temperature not in ('FROZEN GOODS','REFRIGERATED GOODS')  and Dot_Item_Number not in (select Dot_Item_Number from dotfoods_products_level) "                   );
            //PRABU COMMENT BELOW LINE AND UNCOMMENT ABOVE LINE TO GET FULL UPCS UPDATED NOT ONLY FOR INSTOCK
            PreparedStatement selectStmt = con.prepareStatement(" select   d.Dot_Item,ts  from harrisburgstore.dotfoods_instock d, hb_bc.exported_products_from_hb p where  concat('DT_',d.Dot_Item)=p.code     order by ts limit 1000 ");
            PreparedStatement update = con.prepareStatement("update harrisburgstore.dotfoods_instock set avail=?,ts =current_timestamp where Dot_Item=? ");
            ResultSet rs=selectStmt.executeQuery();
            int itemCount=0;
            while(rs.next()){
          

                if(itemCount>0 && itemCount%500==0){

                    con.close();
                    System.out.println("Product count reacched multiple of 500..Sleep for 30 minutes");
                    Thread.sleep(30*60*1000);
                    return;
                }    
                 Thread.sleep(2*1000);
                String Dot_Item_Number=rs.getString(1);
                if(proceededDotNumbers.contains(Dot_Item_Number))
                    continue;
                proceededDotNumbers.add(Dot_Item_Number);
                 Boolean availability=null;
                try{
                    availability=getAvailability(Dot_Item_Number);
               
                }catch (Exception e2){e2.printStackTrace();}
                if(availability!=null ){
                    System.out.println("Updating avail for:"+Dot_Item_Number +" to "+availability);
                    update.setBoolean(1, availability);
                    update.setString(2, Dot_Item_Number );
                    update.executeUpdate();   
                } 
                 
                Thread.sleep(2000);
            }
            con.close();
             } catch (Exception ex) {

            ex.printStackTrace();
            System.exit(1);
        } finally{
            if(con != null){
                try {
                    con.close();
                } catch (SQLException ex) {
                    //
                }
            }
    }        
    }
    static WebClient webClient=null;
    private static boolean getAvailability(String Dot_Item_Number) throws Exception {
        Boolean avail=true;
        ArrayList ret=new ArrayList();
        HashMap <String,String> hm=new HashMap();
        hm.put("item_gtin","NA");
        hm.put("Qty_in_Parent","NA");
        
        
         HtmlPage page = null;
        if(webClient==null){
             webClient = new WebClient(BrowserVersion.INTERNET_EXPLORER);
            webClient.addRequestHeader("X-Requested-With", "XMLHttpRequest");
            webClient.addRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            webClient.addRequestHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0");
            webClient.getOptions().setJavaScriptEnabled(false);
            webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
            webClient.getOptions().setPrintContentOnFailingStatusCode(false);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.getOptions().setCssEnabled(false);
            webClient.getOptions().setRedirectEnabled(true);
        
               page=webClient.getPage("http://www.dotexpressway.com/login.aspx?Logout=True");
            ((HtmlTextInput) page.getHtmlElementById("UserName")).setText("RathnaSivasailam");
            ((HtmlPasswordInput) page.getHtmlElementById("Password")).setText("dummy#123");
             System.out.println("Signing In");
             ;
            page=(HtmlPage) ((HtmlButton ) page.getByXPath("//button[@type='submit']").get(0)).click();
            
        }
        
        String url="https://www.dotexpressway.com/Search?Ntt=XXX&Ntk=Main_Search_IF&Ntx=mode+matchallpartial&Nty=1&NwSrch=1&searchAction=1";
        url=url.replaceAll("XXX",  Dot_Item_Number );
        System.out.println("Visiting URL:"+url);
        HtmlPage page2=webClient.getPage(url);
        int len=page2.getByXPath("//td[@class='availability']").size();
         if(len>0){
             String availStr=((HtmlElement)page2.getByXPath("//td[@class='availability']").get(0)).asText();
             //if(availStr.toLowerCase().indexOf("limited")>=0){
             
             if(availStr.toLowerCase().indexOf("limited")>=0||availStr.toLowerCase().indexOf("discontinued")>=0 || availStr.toLowerCase().indexOf("stocked")<0){
                 avail=false;
             }
             System.out.println("availStr:"+availStr +" avail flag:"+avail);
         }else if(page2.asXml().indexOf("No results were found")<0) {
             System.err.println(" //td[@class='availability'] does not exists, something wrong at "+url);
             ERROR_COUNT++;
            if(ERROR_COUNT>10){
                System.err.println("ERROR_COUNT:"+ERROR_COUNT+", Exception //td[@class='availability'] does not exists, something wrong at "+url);
                System.exit(1);
            }
                
         }else{
              avail=false;
         }
        return avail;
        
    }
}
