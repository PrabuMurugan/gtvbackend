/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dotfoods;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import util.DB;
//TO SELECT ITEMS FROM DOT TABLE
// select d1.dot_item_number,item_gtin,qty_in_parent,product_line_description,brand,gtin_upc_number,expanded_item_description,pack_size,case5000 from dotfoods_products_item d1 , dotfoods_products d2 where d1.Dot_Item_Number=d2.Dot_Item_Number order by d1.id desc
/**
 * CASE 1 : GET ITEMS WITH ITEM UPC 
 * create table dotfoods_product_each as select  distinct substring(d_each.gtin,-12) item_upc,d1.dot_item_number,expanded_item_description,IFNULL(d_inner.qty_in_parent,1)*IFNULL(d_each.qty_in_parent,1) packs_in_case,replace(case5000,'$',''), '' one_pack_price,'DOT FOODS',brand,pack_size  from harrisburgstore.dotfoods_products d1 left join harrisburgstore.dotfoods_products_level d_inner on d1.Dot_Item_Number = d_inner.Dot_Item_Number  and  d_inner.level='INNER PACK' left join harrisburgstore.dotfoods_products_level d_each on d1.Dot_Item_Number = d_each.Dot_Item_Number  and  d_each.level='EACH'  where d1.Dot_Item_Number in (select dot_item_number from harrisburgstore.dotfoods_products_level)   and d_each.gtin  is not null 
 * @author us083274
 */
public class UpdateItemUPC {
     static String FILTER_ITEM="B0084LZU1Q";
    static ArrayList proceededDotNumbers=new ArrayList();
    public static void main (String args[]){
        System.out.println("NEW UpdateItemUPC");
      for(int i=0;i<20;i++){
          parse();
          
      }
   }
    public static int ERROR_COUNT=0;
    public static void parse(){
  Connection con =  null;
         String FILTER = System.getProperty("FILTER", "true");
        if(FILTER.equals("false")){
             FILTER_ITEM="";
        }              
        try {
            DB.CONNECT_TO_PROD_DEBUG=true;
            con = DB.getConnection("harrisburgstore");
            //PreparedStatement selectStmt = con.prepareStatement("select   Dot_Item_Number from dotfoods_products  where    temperature not in ('FROZEN GOODS','REFRIGERATED GOODS')  and Dot_Item_Number not in (select Dot_Item_Number from dotfoods_products_level) "                   );
            //PRABU COMMENT BELOW LINE AND UNCOMMENT ABOVE LINE TO GET FULL UPCS UPDATED NOT ONLY FOR INSTOCK
            PreparedStatement selectStmt = con.prepareStatement(" select   Dot_Item  from harrisburgstore.dotfoods_instock  where    temperature not in ('FROZEN GOODS','REFRIGERATED GOODS')  and Dot_Item not in (select Dot_Item_Number from harrisburgstore.dotfoods_products_level)  ");
            PreparedStatement insertStmt = con.prepareStatement("insert into dotfoods_products_level (Dot_Item_Number,level,gtin,Qty_in_Parent,pack) values (?,?,?,?,?)");
            ResultSet rs=selectStmt.executeQuery();
            int itemCount=0;
            while(rs.next()){
          

                if(itemCount>0 && itemCount%500==0){

                    con.close();
                    System.out.println("Product count reacched multiple of 500..Sleep for 30 minutes");
                    Thread.sleep(30*60*1000);
                    return;
                }    
                 Thread.sleep(2*1000);
                String Dot_Item_Number=rs.getString(1);
                if(proceededDotNumbers.contains(Dot_Item_Number))
                    continue;
                proceededDotNumbers.add(Dot_Item_Number);
                 ArrayList arr=null;
                try{
               arr=getGTINs(Dot_Item_Number);
                }catch (Exception e2){e2.printStackTrace();}
                if(arr==null || arr.size()==0){
                    System.out.println("There is nothing in dotexpress site. Something is wrong for dot item number:"+Dot_Item_Number);
                    
                    
                    insertStmt.setString(1, rs.getString(1));
                    insertStmt.setString(2, "NA");
                    insertStmt.setString(3, "NA");
                    insertStmt.setString(4, "NA");
                    insertStmt.setString(5, "NA");
                    
                    insertStmt.executeUpdate();   
                    if(ERROR_COUNT++>10){
                        System.out.println("Exiting since ERROR_COUNT exceeds 10:"+ERROR_COUNT);
                        // System.exit(1);
                    }
                    continue;
                    //
                } 
                
                for(int i=0;i<arr.size();i++){
                    HashMap hm=(HashMap) arr.get(i);
                    if(hm.containsKey("level") ==false   ){
                        System.out.println("For Dot_Item_Number:"+Dot_Item_Number+" , I can not get item_gtin or Qty_in_Parent. So stopping....");
                        System.exit(1);
                    }
                    insertStmt.setString(1, rs.getString(1));
                    insertStmt.setString(2, (String)(hm.get("level")!=null?hm.get("level"):"NA"));
                    insertStmt.setString(3, (String)(hm.get("gtin")!=null?hm.get("gtin"):"NA"));
                    insertStmt.setString(4, (String)(hm.get("Qty_in_Parent")!=null?hm.get("Qty_in_Parent"):"NA"));
                    insertStmt.setString(5, (String)(hm.get("Pack")!=null?hm.get("Pack"):"0"));
                    
                    insertStmt.executeUpdate();
                    
                }
                Thread.sleep(2000);
            }
            con.close();
             } catch (Exception ex) {

            ex.printStackTrace();
            System.exit(1);
        } finally{
            if(con != null){
                try {
                    con.close();
                } catch (SQLException ex) {
                    //
                }
            }
    }        
    }
    static WebClient webClient=null;
    private static ArrayList getGTINs(String Dot_Item_Number) throws Exception {
        ArrayList ret=new ArrayList();
        HashMap <String,String> hm=new HashMap();
        hm.put("item_gtin","NA");
        hm.put("Qty_in_Parent","NA");
        String url="http://www.dotexpressway.com/ProductDetail?R=XXX&searchAction=20&openTab=GTIN";
        url=url.replaceAll("XXX",  Dot_Item_Number );
         HtmlPage page = null;
        if(webClient==null){
             webClient = new WebClient(BrowserVersion.INTERNET_EXPLORER);
            webClient.addRequestHeader("X-Requested-With", "XMLHttpRequest");
            webClient.addRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            webClient.addRequestHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0");
            webClient.getOptions().setJavaScriptEnabled(false);
            webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
            webClient.getOptions().setPrintContentOnFailingStatusCode(false);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.getOptions().setCssEnabled(false);
            webClient.getOptions().setRedirectEnabled(true);
        
               page=webClient.getPage("http://www.dotexpressway.com/login.aspx?Logout=True");
            ((HtmlTextInput) page.getHtmlElementById("UserName")).setText("RathnaSivasailam");
            ((HtmlPasswordInput) page.getHtmlElementById("Password")).setText("dummy#123");
             System.out.println("Signing In");
             ;
            page=(HtmlPage) ((HtmlButton ) page.getByXPath("//button[@type='submit']").get(0)).click();
            
        }
        System.out.println("Visiting URL:"+url);
        webClient.getOptions().setUseInsecureSSL(true);
        HtmlPage page2=webClient.getPage(url);
        int len=page2.getByXPath("//div[@class='tab_page is_active']//table[@class='tabbed_area_table']").size();
        //page2.asXml().indexOf("No results");
       /* if(len ==0){
            System.out.println("NOT possible to have 0 length while querying for ProductOverview1_uTblGTINUPCInformation");
            System.exit(1);
        }*/
        for(int i=0;i<len;i++){
         String txt=((HtmlElement)page2.getByXPath("//div[@class='tab_page is_active']//table[@class='tabbed_area_table']").get(i) ).asXml();
         String[] tds=StringUtils.substringsBetween(txt,"<td>","</td>");
         
         
          hm=new HashMap();
         for (int n=0;n<tds.length;n++){
            
             if(tds[n].indexOf("Level")>=0 &&tds[n+1].trim().replaceAll("\r\n","").toLowerCase().indexOf("pallet")<0 ){
                 hm.put("level", tds[n+1].trim().replaceAll("\r\n",""));
             }
              if(tds[n].indexOf("GTIN")>=0){
                 hm.put("gtin", tds[n+1].trim().replaceAll("\r\n",""));
             }
             
             if(tds[n].indexOf("Qty in Parent")>=0){
                 hm.put("Qty_in_Parent", tds[n+1].trim().replaceAll("\r\n",""));
             }
             
             if(tds[n].indexOf("Pack")>=0){
                 hm.put("Pack", tds[n+1].trim().replaceAll("\r\n",""));
             }             
             
         }
         
         if(hm.containsKey("level"))
          ret.add(hm);
         /*if(txt.indexOf("GTIN:")>=0 && txt.indexOf("EACH")>=0 ){
             String 
                     tmpTxt=StringUtils.substringAfter(txt, "GTIN");
             tmpTxt=StringUtils.substringBefore(tmpTxt, "Qty in Parent");
             hm.put("item_gtin", tmpTxt.replaceAll(":","").trim()   );
             
            tmpTxt=StringUtils.substringAfter(txt, "Qty in Parent");
            
             
             hm.put("Qty_in_Parent", tmpTxt.replaceAll(":","").trim());
             
         }*/
        
        }
       // System.out.println("page details :"+page2.asXml());
        System.out.println("HM DETAILS for Dot_Item_Number: "+Dot_Item_Number+ " is "+ret.toString());
        return ret;
        
    }
}
