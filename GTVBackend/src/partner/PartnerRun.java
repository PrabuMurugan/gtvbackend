/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package partner;

import bombayplaza.pricematch.RetrievePage;
 
import com.tajplaza.upload.CreateFeedFile;
import com.tajplaza.upload.UploadInventoryLoaderFeedFile;
import net.sf.json.JSONObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import net.sf.json.JSONArray;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.commons.io.FileUtils;
/**
/**
 *
 * @author prabu
 */
public class PartnerRun {
     public static void main(String[] args) throws Exception{
         runForPartner("sailesh","_MS");
           runForPartner("ashok","_AK");

     }
     public static void runForPartner(String partner,String skuSuffix) throws Exception{
         Connection con=RetrievePage.getConnection();
         String sql="update tajplaza.sku_upcs set upc=replace(upc,'\"','')";
         PreparedStatement ps=con.prepareStatement(sql);
         ps.executeUpdate();
         
         ps.executeUpdate();;
           sql="delete from tajplaza.variation_sku_upcs where sku in (select sku from tajplaza.sku_upcs where sku is not null)";
           ps=con.prepareStatement(sql);
         ps.executeUpdate();;
         
          sql="insert into tajplaza.variation_sku_upcs(sku,upc,pkg_qty,oc_singlepack) select sku,upc,pkg_qty,oc_singlepack from  tajplaza.sku_upcs where sku not in (select sku from tajplaza.variation_sku_upcs)";
          ps=con.prepareStatement(sql);
         ps.executeUpdate();

         
         File file = new File("C:\\Google Drive\\Dropbox\\partner\\"+partner+"\\new-inventory.txt");
         if(file.exists()){
             UploadInventoryLoaderFeedFile.uploadFileIntoAmazon(file);
         }
         
         if(new File("C:\\Google Drive\\Dropbox\\partner\\"+partner+"\\new-inventory.txt").exists())
             FileUtils.moveFile(new File("C:\\Google Drive\\Dropbox\\partner\\"+partner+"\\new-inventory.txt"), new File("C:\\Google Drive\\Dropbox\\partner\\"+partner+"\\Done\\new-inventory.txt."+System.currentTimeMillis()));
         if(new File("C:\\Google Drive\\Dropbox\\partner\\"+partner+"\\sku_upcs.csv").exists())
         FileUtils.moveFile(new File("C:\\Google Drive\\Dropbox\\partner\\"+partner+"\\sku_upcs.csv"), new File("C:\\Google Drive\\Dropbox\\partner\\"+partner+"\\Done\\sku_upcs.csv."+System.currentTimeMillis()));
         
         //Write output files
         sql="select seller_sku sku,price,item_name,quantity,(select max(s.ts) from tajplaza.variation_sku_upcs v, tajplaza.scanner_data_shelf_audit s where s.upc=v.upc and s.type='FRESH-INSERT' and v.sku=e.seller_sku) loaded_date from tajplaza.existing_all_inventory e where seller_sku like '%"+skuSuffix+"'";
         ps=con.prepareStatement(sql);
         ResultSet rs=ps.executeQuery();
        FileWriter fstream_out0;
        BufferedWriter out0;
        fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\partner\\"+partner+"\\current_inventory_on_hand.txt");
        out0 = new BufferedWriter(fstream_out0);
        out0.write("sku\tprice\titem_name\tquantity\tloaded_date");
        out0.newLine();
        while(rs.next()){
              out0.write(rs.getString("sku")+"\t"+rs.getString("price")+"\t"+rs.getString("item_name")+"\t"+rs.getString("quantity")+"\t"+rs.getString("loaded_date"));
              out0.newLine();              
         }
        out0.close();
        
         sql="select purchasedate,s.order_number,order_total,carrier_fee, order_total*.85-carrier_fee margin    from harrisburgstore.shipstation_order_export  s,tajplaza.orders o where s.order_number = o.order_id and o.sellersku like '%"+skuSuffix+"'";
         ps=con.prepareStatement(sql);
          rs=ps.executeQuery();
 
        fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\partner\\"+partner+"\\orders.txt");
        out0 = new BufferedWriter(fstream_out0);
        out0.write("purchasedate\torder_number\torder_total\tcarrier_fee\tmargin");
        out0.newLine();
        while(rs.next()){
              out0.write(rs.getString("purchasedate")+"\t"+rs.getString("order_number")+"\t"+rs.getString("order_total")+"\t"+rs.getString("carrier_fee")+"\t"+rs.getString("margin"));
              out0.newLine();              
         }
        out0.close();        
         
     }
     
}
