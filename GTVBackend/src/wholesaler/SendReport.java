/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wholesaler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.mail.MessagingException;
import mailer.MailData;
import static mailer.Mailer.sendMail;
import util.DB;
/**
 *
 * @author debasish
 */
public class SendReport {
    public static void main(String [] args) throws  Exception{
        
        
        
        // Recipient's email ID needs to be mentioned.
        String[] to = new String[]{"debasish.raychawdhuri@gmail.com",
            "Himanshu Patel <vi_himanshu@varuninfotech.com>", "mprabagar80 <mprabagar80@gmail.com>"};

        // Sender's email ID needs to be mentioned
        String from = "Harrisburg<storeharrisburg@gmail.com>";

        // Assuming you are sending email from localhost
        String host = "smtp.gmail.com";

        String user = "storeharrisburg@gmail.com";

        String pass = "tp123456";

        Connection con = DB.getConnection(null);
        PreparedStatement stmt = con.prepareStatement("select Wholesaler_Name, count(*) from  TAJPLAZA.FINAL_OUTPUTCATALOG where date(updation_date) > current_date -7 group by Wholesaler_Name");
        
        ResultSet rs = stmt.executeQuery();
        
        StringBuilder html=new StringBuilder("<html><head><style>h1{color:blue} table{\n" +
            "    margin:  20px;\n" +
            "    border-collapse: collapse;\n" +
            "    border-style: solid;\n" +
            "    border-width: 1px;\n" +
            "}\n" +
            "\n" +
            "td, tr{\n" +
            "    border-collapse: collapse;\n" +
            "    border-style: solid;\n" +
            "    border-width: 1px;\n" +
            "    font-size: 150%;\n" +
            "}</style></head><body><h1>Rows updated for Wholesalers</h1>");
        html.append("<table><tr><td>Wholesaler</td><td>Number of rows inserted/updated</td></tr>");
        while(rs.next()){
            String wholesaler=rs.getString(1);
            String count = rs.getString(2);
            html.append("<tr><td>").append(wholesaler).append("</td><td>").append(count).append("</td></tr>");
        }
        html.append("</table></body>");
        MailData mailData = new MailData();
        mailData.setBody(html.toString());
        mailData.setContentType("text/html");
        mailData.setFrom(from);
        mailData.setHost(host);
        mailData.setSubject("Rows updated for Wholesalers");
        mailData.setTo(to);
        
        mailData.setUsername(user);
        mailData.setPassword(pass);
        
        sendMail(mailData);
    }
}
