package flipkart;

import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.RetrievePage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.logging.LogFactory;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.IncorrectnessListener;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAcronym;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlUnorderedList;
import com.gargoylesoftware.htmlunit.javascript.host.Console;
import com.gargoylesoftware.htmlunit.javascript.host.html.HTMLCollection;
import com.gargoylesoftware.htmlunit.javascript.host.html.HTMLDivElement;
import com.gargoylesoftware.htmlunit.javascript.host.html.HTMLUListElement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;
import org.apache.commons.lang.StringUtils;
 

public class FlickArtCrawler {
    public static Connection con=null;
    public static ArrayList visitedProductsInDB=new ArrayList();
    public static ArrayList existingFKProductsInHB=new ArrayList();
	public static void main(String [] stgs) throws  Exception {
            RetrievePage.NUMBER_TIMES_PROXIES_BEFORE_SLEEP=10;
            RetrievePage.SLEEP_TIME_ALL_PROXIES_USED_ONCE=1;
                con=RetrievePage.getConnection();
                PreparedStatement ps=con.prepareStatement("select distinct pageurl from hb_bc.fk_products_to_create ");
                ResultSet rs=ps.executeQuery();
                while(rs.next()){
                    visitedProductsInDB.add(rs.getString(1));
                }
                ps=con.prepareStatement("select replace(code,'FK_','') from hb_bc.exported_products_from_hb where code like 'FK_%' ");
                rs=ps.executeQuery();
                while(rs.next()){
                    existingFKProductsInHB.add(rs.getString(1));
                }                
		String url = "http://www.flipkart.com/womens-clothing/ethnic-wear/pr?p[]=facets.fulfilled_by%5B%5D=Flipkart+Advantage&p[]=facets.availability%5B%5D=Exclude+Out+of+Stock&sid=2oq,c1r,3pj";
                CreateCatalog c=new CreateCatalog();
                c.ERROR_MOVE_TO_NEXT_PAGE=true;
                RetrievePage.USE_CACHE=false;
                HtmlPage page = c.getPage1(url);
               
                    
                ArrayList<DomElement> facets=(ArrayList<DomElement>) page.getByXPath("//input[@class=\"facetoption\"]");
                ArrayList facetsToUse=new ArrayList();
                for(int i=0;i<facets.size();i++){
                    String facetValue=facets.get(i).getAttribute("value");
                    if(facetValue.indexOf("Flipkart+Advantage")>=0)
                        continue;
                    if(facetValue.indexOf("Exclude+Out+of+Stock")>=0)
                        continue;
                    if(facetsToUse.contains("&p[]="+facetValue))
                        continue;
                    if(facetValue.indexOf("brand")<0)
                        continue;
                    facetsToUse.add("&p[]="+facetValue);
                    
                }
                 ArrayList vistedCatUrls=new ArrayList();
                for(int x=0;x<facetsToUse.size();x++){
                   String newurlTemplate=url+facetsToUse.get(x)+"&start=XX";
                    System.out.println("Visiting url with facet : "+newurlTemplate);
                    for(int n=1;n<=1000;n+=15){
                        String newurl=newurlTemplate.replaceAll("XX", String.valueOf(n));
                        int prodCount=0;
                        try{
                           prodCount= visitProductsInPage(newurl);
                        }catch (Exception e2){e2.printStackTrace();}
                        if(prodCount==0){
                            System.out.println("No products at :"+newurl);
                            break;
                        }
                    }
                    
                }
            con.close();
	}
        public static int visitProductsInPage(String url) throws Exception{
                int productsCollected=0;
                System.out.println("Visiting url3:"+url);
	    CreateCatalog c=new CreateCatalog();
	     HtmlPage page = c.getPage1(url);
	  //  log("Webpage loaded"+page.asText());
	    
	    // Looking for each of SubCategory Under this category
	    ArrayList<DomElement> products = (ArrayList<DomElement>) page.getByXPath("//a[@data-tracking-id=\"prd_title\"]");
	    log("Products list:"+products.size());

	   
	     
	    for(int i=0;i<products.size();i++){
            
	    	HtmlAnchor htmlAnchor =(HtmlAnchor)products.get(i);
                String productUrl= htmlAnchor.getHrefAttribute();
                if(productUrl.charAt(0)=='/')
                    productUrl="http://www.flipkart.com"+productUrl;
               boolean prodCollectedNow= false;
               try{
                //   productUrl="http://www.flipkart.com/fabdeal-crepe-geometric-print-salwar-suit-dupatta-material/p/itme27pzdhwnhznh";                   
                  prodCollectedNow= visitVariation(productUrl);
               }catch (Exception e3){e3.printStackTrace();}
                if(prodCollectedNow)
                    productsCollected++;
	    	
	    }
            return productsCollected;
        }
        public static int productcount=0;
       static ArrayList<String> visitedProducts=new ArrayList();
        public static  boolean visitVariation(String productUrl) throws Exception{
            
            if(productUrl.indexOf("?")>=0)
                productUrl=StringUtils.substringBefore(productUrl, "?");
            if(visitedProducts.contains(productUrl))
                return false;
            if(visitedProductsInDB.contains(productUrl))
                return true;
            log("productcount :"+(productcount++)+"Product   Accessing this product : " +productUrl);
            visitedProducts.add(productUrl);
            CreateCatalog c=new CreateCatalog();
            c.ERROR_MOVE_TO_NEXT_PAGE=true;
            HtmlPage page= c.getPage1(productUrl);
            if(page==null)
                return false;
             ArrayList<DomElement> variations = (ArrayList<DomElement>)page.getByXPath("//div[@class=\"multiSelectionWidget-selectors-wrap\"]");
             String selectedColor=null,selectedSize=null;
             boolean colorVariationsExist=false,sizeVariationsExist=false,fkAdvantage=false;
             TreeMap<String,String> availableColors=new TreeMap();
             TreeMap<String,String> availableSizes=new TreeMap();
              String availSizes=null;
             if(page.getByXPath("//span[@class=\"fk-advantage\"]").size()>0)
                 fkAdvantage=true;
                 
             for(int i=0;i<variations.size();i++){
                 if(variations.get(i).getParentNode().asText().indexOf("Select Color")>=0)
                 {
                     colorVariationsExist=true;
                     System.out.println("Color variations");
                     Iterator<DomElement> childVariations =variations.get(i).getChildElements().iterator();
                     while(childVariations.hasNext()){
                         DomElement color=childVariations.next();
                         if(color.getTagName().equals("div") &&color.getAttribute("class").indexOf("selected")>=0 ){
                          System.out.println("Currently selected color:"+color.asText()); 
                          selectedColor=color.asText();
                         }
                         if(color.getTagName().equals("a") ){
                          String href=color.getAttribute("href");
                          String val=color.asText();
                          availableColors.put(val, href);
                         }                         
                     }
                     
                 }
                 if(variations.get(i).getParentNode().asText().indexOf("Select Size")>=0)
                 {
                     sizeVariationsExist=true;
                    // System.out.println("Size variations");
                     Iterator<DomElement> childVariations =variations.get(i).getChildElements().iterator();
                     while(childVariations.hasNext()){
                         DomElement size=childVariations.next();
                         if(size.getTagName().equals("div") &&size.getAttribute("class").indexOf("selected")>=0 ){
                          System.out.println("Currently selected size:"+size.asText()); 
                          selectedSize=size.asText();
                         }
                         if(size.getAttribute("class")==null || size.getAttribute("class").indexOf("disabled")<0 )
                         {
                          String href="";
                          if(size.hasAttribute("href"))
                              href=size.getAttribute("href");
                          String val=size.asText();
                          availableSizes.put(val, href);
                          if(availSizes==null)
                               availSizes=val;
                          else
                               availSizes=availSizes+","+val;
                         }                         
                         
                     }
                     
                 }                 
             }
            
            String optionsSet=""; 
            selectedColor=null;
             if( selectedColor!=null){
                 optionsSet=selectedColor+":";
             }
             if(availSizes!=null){
                 optionsSet=optionsSet+availSizes;
             }
   
             if(fkAdvantage){
                 insertProduct(productUrl,page,availableColors,optionsSet,selectedColor,availSizes);
              }
             
             if(availableColors.size()>0){
                 Iterator<String> iter=availableColors.values().iterator();
                 while(iter.hasNext()){
                     String url=iter.next();
                     if(url.charAt(0)=='/')
                       url="http://www.flipkart.com"+url;
                     
                    // System.out.println("url : "+url);
                     visitVariation(url);
                     
                     
                 }
                 
             }
             return true;
        }
        public static  void insertProduct(String pageurl,HtmlPage page,TreeMap<String,String> availableColors, String optionsSet,String selectedColor,String availSizes) throws Exception{
                 String productid="NA";
                 String title="NA";
                 String price="NA";
                 String description="";
                 String amazon_desc="";
                 String amazon_bb1="";
                 String amazon_bb2="";
                 String amazon_bb3="";
                 String amazon_bb4="";
                 String amazon_bb5="";
                 String product_image_url="";
                 String product_image_url2="";
                 String product_image_url3="";
                 String product_image_url4="";
                 String category="Misc";
                 if(page.getByXPath("//input[@class=\"btn-buy-now btn-big  current\"]").size()>0){
                     DomElement buyButton =(DomElement)page.getByXPath("//input[@class=\"btn-buy-now btn-big  current\"]").get(0);
                    productid= buyButton.getAttribute("data-pid");
                     
                 }
                 
                 if(page.getByXPath("//h1[@class=\"title\"]").size()>0){
                      title=((DomElement)page.getByXPath("//h1[@class=\"title\"]").get(0)).asText();
                     
                 }    
                 if(page.getByXPath("//span[@class=\"subtitle\"]").size()>0){
                      title+=((DomElement)page.getByXPath("//span[@class=\"subtitle\"]").get(0)).asText();
                     
                 }                    
                 if(page.getByXPath("//span[@class=\"selling-price omniture-field\"]").size()>0){
                      price=((DomElement)page.getByXPath("//span[@class=\"selling-price omniture-field\"]").get(0)).asText();
                     
                 }       
                 if(page.getByXPath("//div[@class=\"keyFeatures specSection\"]").size()>0){
                      description+=((DomElement)page.getByXPath("//div[@class=\"keyFeatures specSection\"]").get(0)).asXml();
                      amazon_desc+=((DomElement)page.getByXPath("//div[@class=\"keyFeatures specSection\"]").get(0)).asText();
                     
                 } 
                 if(page.getByXPath("//div[@class=\"keyFeatures specSection\"]//li").size()>0){
                      amazon_bb1+=((DomElement)page.getByXPath("//div[@class=\"keyFeatures specSection\"]//li").get(0)).asText();
                     
                 }   
                 else if(page.getByXPath("//li[@class=\"key-specification tmargin2 bmargin2\"]").size()>0){
                      amazon_bb1=((DomElement)page.getByXPath("//li[@class=\"key-specification tmargin2 bmargin2\"]").get(0)).asText();
                     
                 }                 
                 
                 if(page.getByXPath("//div[@class=\"keyFeatures specSection\"]//li").size()>1){
                      amazon_bb2=((DomElement)page.getByXPath("//div[@class=\"keyFeatures specSection\"]//li").get(1)).asText();
                     
                 }else if(page.getByXPath("//li[@class=\"key-specification tmargin2 bmargin2\"]").size()>1){
                      amazon_bb2=((DomElement)page.getByXPath("//li[@class=\"key-specification tmargin2 bmargin2\"]").get(1)).asText();
                     
                 }       
                 if(page.getByXPath("//div[@class=\"keyFeatures specSection\"]//li").size()>2){
                      amazon_bb3=((DomElement)page.getByXPath("//div[@class=\"keyFeatures specSection\"]//li").get(2)).asText();
                     
                 }  else if(page.getByXPath("//li[@class=\"key-specification tmargin2 bmargin2\"]").size()>2){
                      amazon_bb3=((DomElement)page.getByXPath("//li[@class=\"key-specification tmargin2 bmargin2\"]").get(2)).asText();
                     
                 }   
                 if(page.getByXPath("//div[@class=\"keyFeatures specSection\"]//li").size()>3){
                      amazon_bb4=((DomElement)page.getByXPath("//div[@class=\"keyFeatures specSection\"]//li").get(3)).asText();
                     
                 }else if(page.getByXPath("//li[@class=\"key-specification tmargin2 bmargin2\"]").size()>3){
                      amazon_bb4=((DomElement)page.getByXPath("//li[@class=\"key-specification tmargin2 bmargin2\"]").get(3)).asText();
                     
                 }   
                 if(page.getByXPath("//div[@class=\"keyFeatures specSection\"]//li").size()>4){
                      amazon_bb5+=((DomElement)page.getByXPath("//div[@class=\"keyFeatures specSection\"]//li").get(4)).asText();
                     
                 }                     
                 if(page.getByXPath("//div[@class=\"description specSection\"]").size()>0){
                      description+=((DomElement)page.getByXPath("//div[@class=\"description specSection\"]").get(0)).asXml();
                      amazon_desc+=((DomElement)page.getByXPath("//div[@class=\"description specSection\"]").get(0)).asText();
                     
                 } 
                 if(page.getByXPath("//div[@class=\"productSpecs specSection\"]").size()>0){
                      description+=((DomElement)page.getByXPath("//div[@class=\"productSpecs specSection\"]").get(0)).asXml();
                      amazon_desc+=((DomElement)page.getByXPath("//div[@class=\"productSpecs specSection\"]").get(0)).asText();
                     
                 }    
                 description = description.replaceAll("\\s+", " ");
                 if(page.getByXPath("//div[@class=\"breadcrumb-wrap line\"]").size()>0){
                      category=((DomElement)page.getByXPath("//div[@class=\"breadcrumb-wrap line\"]").get(0)).asText();
                      if(availableColors.size()==0){
                          category=StringUtils.substringBeforeLast(category, ">");
                      }
                      if(StringUtils.substringBefore(category, ">").indexOf("Home")>=0)
                          category=StringUtils.substringAfter(category, ">");
                      if(StringUtils.substringBefore(category, ">").indexOf("Clothing")>=0)
                          category=StringUtils.substringAfter(category, ">");


                     
                 }  

                 System.out.println("Title of the product:"+title);
                 if(page.getByXPath("//div[@class=\"imgWrapper\"]//img").size()>0){
                      if(((DomElement)page.getByXPath("//div[@class=\"imgWrapper\"]//img").get(0)).hasAttribute("data-zoomimage"))
                          product_image_url=((DomElement)page.getByXPath("//div[@class=\"imgWrapper\"]//img").get(0)).getAttribute("data-zoomimage");
                      else if(((DomElement)page.getByXPath("//div[@class=\"imgWrapper\"]//img").get(0)).hasAttribute("data-src"))
                          product_image_url=((DomElement)page.getByXPath("//div[@class=\"imgWrapper\"]//img").get(0)).getAttribute("data-src");
                      else if(((DomElement)page.getByXPath("//div[@class=\"imgWrapper\"]//img").get(0)).hasAttribute("src"))
                          product_image_url=((DomElement)page.getByXPath("//div[@class=\"imgWrapper\"]//img").get(0)).getAttribute("src");
                     
                 }                 
                 if(page.getByXPath("//div[@class=\"imgWrapper\"]//img").size()>1){
                      if(((DomElement)page.getByXPath("//div[@class=\"imgWrapper\"]//img").get(1)).hasAttribute("data-zoomimage"))
                          product_image_url2=((DomElement)page.getByXPath("//div[@class=\"imgWrapper\"]//img").get(1)).getAttribute("data-zoomimage");
                     
                 }               
                 if(page.getByXPath("//div[@class=\"imgWrapper\"]//img").size()>2){
                      if(((DomElement)page.getByXPath("//div[@class=\"imgWrapper\"]//img").get(2)).hasAttribute("data-zoomimage"))
                          product_image_url3=((DomElement)page.getByXPath("//div[@class=\"imgWrapper\"]//img").get(2)).getAttribute("data-zoomimage");
                     
                 }               
                 if(page.getByXPath("//div[@class=\"imgWrapper\"]//img").size()>3){
                      if(((DomElement)page.getByXPath("//div[@class=\"imgWrapper\"]//img").get(3)).hasAttribute("data-zoomimage"))
                          product_image_url4=((DomElement)page.getByXPath("//div[@class=\"imgWrapper\"]//img").get(3)).getAttribute("data-zoomimage");
                     
                 } 
                 if(description.length()>2999)
                     description=StringUtils.substring(description,0,2999);
                  PreparedStatement ps=null;
                  if(visitedProductsInDB.contains(pageurl)){
                      System.out.println("product already in visitedProductsDB.");
                      return;
                  }
               /*  ps=con.prepareStatement("insert hb_bc.flipkart_products (productid,title,price,pageurl,description,product_image_url,category,product_image_url2,product_image_url3,product_image_url4,optionsset,item_type,amazon_desc,amazon_bb1,amazon_bb2,amazon_bb3,amazon_bb4) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");  
                 ps.setString(1, productid);
                 ps.setString(2, title);
                 ps.setString(3, price);
                 ps.setString(4, pageurl);
                 ps.setString(5, description);
                 ps.setString(6, product_image_url);
                 ps.setString(7, category);
                 ps.setString(8, product_image_url2);
                 ps.setString(9, product_image_url3);
                 ps.setString(10, product_image_url4);
                 ps.setString(11, optionsSet);
                 ps.setString(12, "Product");
                 ps.setString(13, amazon_desc);
                 ps.setString(14, amazon_bb1);
                 ps.setString(15, amazon_bb2);
                 ps.setString(16, amazon_bb3);
                 ps.setString(17, amazon_bb4);
                 ps.executeUpdate();*/
                 
                 /*ps=con.prepareStatement("insert into hb_bc.optionset "
                         + " (sku,Item_Type,Product_ID,Product_Name,Option_Set,Option_Set_Align,Track_Inventory,Current_Stock_Level,Category)"
                         + "  values(?,?,?,?,?,?,?,?,?)");
                ps.setString(1,"FK_"+productid);
                 ps.setString(2, "Product");
                 ps.setString(3, "");
                 ps.setString(4,title);
                 ps.setString(5, optionsSet);
                 ps.setString(6, "Right");
                 ps.setString(7, "by option");
                 ps.setString(8, "0");
                 ps.setString(9, "Product Options");
                 ps.executeUpdate();
                 */
                 if(false && existingFKProductsInHB.contains(productid)){
                     System.out.println("Product is arleady present in HB. Not to create options and insert into fk_products_to_create");
                     return;
                 }
                 ps=con.prepareStatement("insert hb_bc.fk_products_to_create (productid,title,price,pageurl,description,product_image_url,category,product_image_url2,product_image_url3,product_image_url4,optionsset,item_type,amazon_desc,amazon_bb1,amazon_bb2,amazon_bb3,amazon_bb4) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");  
                 ps.setString(1, productid);
                 ps.setString(2, title);
                 ps.setString(3, price);
                 ps.setString(4, pageurl);
                 ps.setString(5, description);
                 ps.setString(6, product_image_url);
                 ps.setString(7, category);
                 ps.setString(8, product_image_url2);
                 ps.setString(9, product_image_url3);
                 ps.setString(10, product_image_url4);
                 ps.setString(11, optionsSet);
                 ps.setString(12, "Product");
                 ps.setString(13, amazon_desc);
                 ps.setString(14, amazon_bb1);
                 ps.setString(15, amazon_bb2);
                 ps.setString(16, amazon_bb3);
                 ps.setString(17, amazon_bb4);                 
                 ps.executeUpdate();
                 
                 
                 if(selectedColor==null &&availSizes==null  ){
                     System.out.println("No color/size option set to insert");
                     return;
                 }

                 
                 String option="";
                 if(selectedColor!=null){
                     option="[RT]Color="+selectedColor+",";
                     //ps.executeUpdate();                         
                 }
                 ps=con.prepareStatement("insert hb_bc.fk_products_to_create (productid,item_type,title) values(?,?,?)");  
 
                 if(availSizes==null){
                     ps.setString(1,  productid+"_"+selectedColor);
                     ps.setString(2, "SKU");
                     ps.setString(3,  option);
                     
                     ps.executeUpdate();                              
                     
                 }
                 else if(availSizes!=null) {
                    String[] availSizesArr=availSizes.split(",");
                    for(int k=0;k<availSizesArr.length;k++){
                         ps.setString(1, productid+"_"+selectedColor+"_"+availSizesArr[k]);
                         ps.setString(2, "SKU");
                         ps.setString(3,  option+"[RT]Size="+availSizesArr[k]);
                         ps.executeUpdate();                              
                    }

             }
                 
                 
        }
	private static void log(String string) {
		System.out.println(string);
		
	}

	private static void configure(WebClient webClient) {

		LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");

	    java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF); 
	    java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);

	    	}
}
