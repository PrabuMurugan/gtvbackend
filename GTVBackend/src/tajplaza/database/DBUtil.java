/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tajplaza.database;

import java.sql.Connection;
import java.sql.DriverManager;
import util.DB;
import static util.DB.CONNECT_TO_PROD_DEBUG;
import util.HBLogger;
/**
 *
 * @author Raj
 */
public class DBUtil {
       public static boolean CONNECT_TO_PROD_DEBUG = false;
    private static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    private static String PORT = "";
    private static String USERNAME = "";
    private static String PASSWORD = "";
    
    public static Connection getConnection(String schemaName) throws Exception {
        
        Connection con = null;
        Class.forName(DATABASE_DRIVER);
        String PORT = System.getProperty("port", "3306");
        String SCHEMA = schemaName != null ? schemaName : "harrisburgstore";
        
        USERNAME = System.getProperty("user", "guest1");
        PASSWORD = System.getProperty("password", "guest1");
        
        CONNECT_TO_PROD_DEBUG = Boolean.parseBoolean(System.getProperty("CONNECT_TO_PROD_DEBUG", String.valueOf(CONNECT_TO_PROD_DEBUG)));
        String connectTo = System.getProperty("CONNECT_TO");
        String url = "jdbc:mysql://localhost:" + PORT + "/" + SCHEMA;
        if (connectTo != null && "PROD".equals(connectTo)) {
            url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/" + SCHEMA;
        } else if (connectTo != null && "QA".equals(connectTo)) {
            url = "jdbc:mysql://162.242.160.179:3306/" + SCHEMA;
        }

        if (CONNECT_TO_PROD_DEBUG) {
            HBLogger.debug("DB.getConnection", "CONNECT_TO_PROD_DEBUG is set true. Connecting to production databse.");
            url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/" + SCHEMA;
        }
        HBLogger.debug("DB.getConnection", "Database URL : " + url);
        try {
            con = DriverManager.getConnection(url, USERNAME, PASSWORD);
        } catch (Exception e) {
            HBLogger.debug("DB.getConnection", "Exception occured while connectind database. Error MSG : " + e.getMessage());    
            url = "jdbc:mysql://162.242.160.179:3306/" + SCHEMA;
            con = DriverManager.getConnection(url, USERNAME, PASSWORD);
            HBLogger.debug("DB.getConnection", "Using QA database");    
        }
        HBLogger.debug("DB.getConnection", "Returning database connection. Connection : " + con); 
        return con;
    }
}
