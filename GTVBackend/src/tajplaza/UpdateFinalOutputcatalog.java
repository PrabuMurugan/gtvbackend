package tajplaza;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.DB;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Abani
 */
public class UpdateFinalOutputcatalog {

    public static void main(String[] args) {
        Connection con = null;
        try {
            con = DB.getConnection("tajplaza");
            PreparedStatement pst = con.prepareStatement("SELECT * FROM tajplaza.final_packingqty");
            ResultSet rs = pst.executeQuery();
            PreparedStatement update_pst = con.prepareStatement("UPDATE tajplaza.final_outputcatalog set Packing_Qty=?, originalcostof1asin = round(? * replace(replace(replace(replace(1_case_price,'\"',''),',',''),'-',0),'call',0)/packs_in_1_case,2) where  asin=?");
            while (rs.next()) {
                System.out.println("Asin:"+rs.getString("asin") + "packing qty:"+rs.getString("packing_qty"));
                update_pst.setString(1, rs.getString("packing_qty"));
                update_pst.setString(2, rs.getString("packing_qty"));
                update_pst.setString(3, rs.getString("asin"));
                update_pst.addBatch();
            }
            update_pst.executeBatch();
            update_pst.close();
            //PreparedStatement update_pst = con.prepareStatement("UPDATE tajplaza.final_outputcatalog set originalcostof1asin = round(packing_qty * replace(replace(replace(replace(1_case_price,\"\"\",\"\"),\",\",\"\"),\"-\",0),\"call\",0)/packs_in_1_case,2)");
            //update_pst.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                Logger.getLogger(UpdateFinalOutputcatalog.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
