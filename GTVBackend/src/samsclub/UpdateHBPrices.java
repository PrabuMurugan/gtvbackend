/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;

/**
 *
 * @author prabu
 */

import unfi.*;
 
import bombayplaza.pricematch.RetrievePage; 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import net.sf.json.JSONObject;
import bombayplaza.pricematch.MatchThePrice;
import java.util.HashMap;
import org.apache.commons.lang.StringUtils;
import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import amazon.product.QueryProduct;
import java.util.Calendar;
import java.util.GregorianCalendar;
import net.sf.json.JSONArray;

public class UpdateHBPrices {
      
    public static void main(String args[]) throws Exception{
     // Product  p = getUPCDetails("B00H9H56QA");
     //console(p);
       
     updatePrices();
     System.out.println("Program completed");

 } 
 
//   public static String DEBUG_SQL=" and p.code='CO_311_181679' ";

 
   public static String DEBUG_SKU="UN_279083";
 
 public static String AMAZON_TABLE_NAME="amazon_catalog_hb";

 public static Float ABSOLUTE_MIN_MARGIN=(float)0;
 public static Float MIN_MARGIN=(float)0;
    public static void updatePrices() throws Exception{
            Connection con=RetrievePage.getConnection();
            PreparedStatement pst = null,pst2=null;
            ResultSet rs = null,rs2=null;
             String sort_order="";
             sort_order=String.valueOf(System.getProperty("sort_order",""));
             String PRODUCTION =String.valueOf(System.getProperty("PRODUCTION",""));
             if(PRODUCTION.equals("true"))
                 DEBUG_SKU="";
             
             
            
             //String sql = "  select upc,item_code sku,c.price reg_cost,p.price current_price, bestprice,secondbestprice,c.asin,p.product_id from hb_bc.hb_catalog c, hb_bc.exported_products_from_hb p where c.item_code=p.code   and  p.price<round(c.price*1.4) and stock_level>0  and ts>(curdate()-7) limit 100000 "  ;
            // String sql = "  select upc,item_code sku,c.price reg_cost,p.price current_price,p.product_id from hb_bc.hb_catalog c, hb_bc.exported_products_from_hb p where c.item_code=p.code   and  p.price<round(c.price*1.4) and stock_level>0  limit 100000 "  ;
             //FOR SAMS products
             //String sql = "  select sku sku,oc  reg_cost,p.price current_price,c.asin,p.product_id from hb_bc.original_cost c, hb_bc.exported_products_from_hb p where c.sku=p.code   and c.sku like 'SC_%' and c.asin is not null and length(c.asin)>0"  ;
            //REPLACE  ABOVE QUERY TO USE SKU IN WHERE CLAUSE
             String sqlBubbleWrapped="select upc from hb_bc.bubble_wrap_products";
             PreparedStatement psBubbleWrapped=con.prepareStatement(sqlBubbleWrapped);
             ResultSet rs0=psBubbleWrapped.executeQuery();
             ArrayList<String> productsBubbleWrapped=new ArrayList();
             while(rs0.next()) {
                 productsBubbleWrapped.add(rs0.getString(1));
             }
             String sqlBuyBoxPrices="select sku,new_buy_box_price_plus_shipping_on_amazon  from tajplaza.sellery_export ";
             PreparedStatement psBuyBoxPrices=con.prepareStatement(sqlBuyBoxPrices);
              rs0=psBuyBoxPrices.executeQuery();
             HashMap<String,Float> buyBoxPrices=new HashMap();
             while(rs0.next()) {
                 buyBoxPrices.put(rs0.getString(1), rs0.getFloat(2));
             }

             String sqlStuckSkus="select code,date_added from hb_bc.exported_products_from_hb where code like '%AMA%' and str_to_date(date_added,'%m/%d/%YY')< (curdate() - interval 3  day) ";
            // String sqlStuckSkus="select code,date_added from hb_bc.exported_products_from_hb where code like '%AMA%'   ";
             PreparedStatement psStuckSkus=con.prepareStatement(sqlStuckSkus);
              rs0=psStuckSkus.executeQuery();
             ArrayList<String > stuckSkus=new ArrayList();
             while(rs0.next()) {
                 stuckSkus.add(rs0.getString(1) );
             }
             String hbpricechanger_ignoreSkussql="select sku from hb_bc.hbpricechanger_ignore where 1=1";
             PreparedStatement pshbpricechanger_ignoreSkus=con.prepareStatement(hbpricechanger_ignoreSkussql);
              rs0=pshbpricechanger_ignoreSkus.executeQuery();
             ArrayList<String > hbpricechanger_ignoreSkus=new ArrayList();
             while(rs0.next()) {
                 hbpricechanger_ignoreSkus.add(rs0.getString(1) );
             }             

             String sqllessThan10ProductsCat="select     c1.id,c1.parent_id  from  hb_bc.categories c1, hb_bc.categories orphan_cat_parents  where c1.parent_id=orphan_cat_parents.id and orphan_cat_parents.parent_id is not null and orphan_cat_parents.parent_id>1000 and c1.id   in (select id from hb_bc.cat_prod_count where    count_products>0 and count_products<5)     and  c1.id  not  in (select c2.parent_id from hb_bc.categories c2,  hb_bc.cat_prod_count pc1 where c2.id=pc1.id and count_products>5) and c1.id  not in (select c3.parent_id from hb_bc.categories c3,hb_bc.categories c2,  hb_bc.cat_prod_count pc1 where c2.id=pc1.id and c2.parent_id=c3.id  and count_products>5) order by  c1.id";
             PreparedStatement psLessThan10ProductsCat=con.prepareStatement(sqllessThan10ProductsCat);
              rs0=psLessThan10ProductsCat.executeQuery();
             HashMap<String,String > lessThan10ProductsCat=new HashMap();
             while(false && rs0.next()) {
                 lessThan10ProductsCat.put(rs0.getString(1),rs0.getString(2) );
             }
             String sqlnotBottles="select   code   from  hb_bc.bottles_review where approved=true and bottle=false   and notsure=false ";
             PreparedStatement psNotBottles=con.prepareStatement(sqlnotBottles);
              rs0=psNotBottles.executeQuery();
             ArrayList<String  > notBottlesApproved=new ArrayList();
             while(rs0.next()) {
                 notBottlesApproved.add(rs0.getString(1));
             }             
               String sqlnotSureBottles="select   code   from  hb_bc.bottles_review where approved=true and notsure=true   ";
             PreparedStatement psNotSureBottles=con.prepareStatement(sqlnotSureBottles);
              rs0=psNotSureBottles.executeQuery();
             ArrayList<String  > notSureBottlesApproved=new ArrayList();
             while(rs0.next()) {
                 notSureBottlesApproved.add(rs0.getString(1));
             }   
              String sqlProductsWithGoodReviewAtHighPriceAmazon=" select p.code,p.name,p.price,p.weight,cast(price as decimal) + cast(weight as decimal)*.4 hb_price_with_weight, s.ASIN_on_amazon,sales_rank,New_Buy_Box_price_plus_shipping_on_amazon, cast(Number_of_new_offers_on_amazon as decimal) amazon_offers,cast(New_Buy_Box_price_plus_shipping_on_amazon*.9-4-weight*.4 as decimal)+.99 suggested_price_on_hb from hb_bc.exported_products_from_hb p, tajplaza.sellery_export s where   p.code=s.sku   and New_Buy_Box_price_plus_shipping_on_amazon>12 and New_Buy_Box_price_plus_shipping_on_amazon<400 and cast(Number_of_new_offers_on_amazon as decimal)>1 and sales_rank>100 and  sales_rank<60000 and new_buy_box_price_plus_shipping_on_amazon*.9-(cast(price as decimal) + 4+cast(weight as decimal)*.4) >5 order by  new_buy_box_price_plus_shipping_on_amazon-(cast(price as decimal) + 4+cast(weight as decimal)*.4) desc  ";
             PreparedStatement psProductsWithGoodReviewAtHighPriceAmazon=con.prepareStatement(sqlProductsWithGoodReviewAtHighPriceAmazon);
              rs0=psProductsWithGoodReviewAtHighPriceAmazon.executeQuery();
             HashMap<String,Float  > productsAtHighPriceOnAmazon=new HashMap();
             while(rs0.next()) {
                 productsAtHighPriceOnAmazon.put(rs0.getString("code"),rs0.getFloat("suggested_price_on_hb"));
             }      
     
             
             
             String sql=
                     //"select item_code sku,c.price reg_cost,p.price current_price,p.product_id,c.asin from hb_bc.hb_catalog c, hb_bc.exported_products_from_hb p where c.item_code=p.code    and stock_level>0  "+
                       // " union "+
                      //  " select sku sku,oc  reg_cost,p.price current_price,p.product_id,c.asin,category_string,category_details,packs_in_1_case,p.name,weight,ui.size,p.product_upc_ean,stock_level from hb_bc.original_cost c, hb_bc.exported_products_from_hb p left join  hb_bc.unfi_input_catalog ui on   replace(p.code,'UN_','') = ui.item_code    where     c.sku=p.code  and p.code not like 'DT%' and c.oc is not null and cast(c.oc as decimal)>0    " ;
                     " select sku sku,oc  reg_cost,p.price current_price,p.product_id,c.asin,category_string,category_details,'' packs_in_1_case,p.name,weight weight,'' size,p.product_upc_ean,stock_level from hb_bc.original_cost c, hb_bc.exported_products_from_hb p   where     c.sku=p.code and c.oc is not null and cast(c.oc as decimal)>0  and stock_level>0   ";
            System.out.println("Exceutung sql:"+sql);
            pst=con.prepareStatement(sql);
            rs=pst.executeQuery();
            int count=0;
            ProcessWorkingCatalog pw=new ProcessWorkingCatalog();
            
            while(rs.next()){
                Product p=new Product();
                String asin=rs.getString("asin");
                String item_name =rs.getString("name");
                String catg_desc=rs.getString("category_string");
                String category_details=rs.getString("category_details");
                
                String size=rs.getString("size");
                int stock_level=rs.getInt("stock_level");
                if(size==null)
                    size="NA";
                
                Integer packs_in_1_case=rs.getInt("packs_in_1_case");
                
                Float curPrice=rs.getFloat("current_price");
                Float weight=rs.getFloat("weight");
                 
                String sku=rs.getString("sku");                                 
                if(DEBUG_SKU!=null && DEBUG_SKU.trim().length()>0 && sku.trim().indexOf(DEBUG_SKU)<0){
                    continue;
                }
                if(sku.indexOf("GIFT")>=0)
                    continue;
                if(hbpricechanger_ignoreSkus.contains(sku)){
                    System.out.println("In HB Pricechnager ignore list table, ignoring");
                    continue;
                }
                String regCostS=rs.getString("reg_cost");

                System.out.println("sku:"+sku+",reg cost:"+regCostS);
                 if(regCostS==null || regCostS.trim().length()==0){
                    regCostS="0";
                   // continue;
                } 
                    
                if(StringUtils.countMatches(regCostS, ".")>1)
                    regCostS=StringUtils.substringBeforeLast(regCostS, ".");
                regCostS=regCostS.replaceAll("\\$","").replaceAll(",","");
                Float reg_cost=Float.valueOf(regCostS);
                
                String id=rs.getString("product_id");
                 
                System.out.println("Processign count:"+count++ +",asin :"+asin+",sku:"+sku);
                MIN_MARGIN=(float)1.35;
                ABSOLUTE_MIN_MARGIN=(float)1.25;
                    


                if(sku.indexOf("DT_")>=0){
                     MIN_MARGIN=(float)1.35;
                }
                if(sku.indexOf("UN_")>=0){
                     MIN_MARGIN=(float)1.35;
                }                

                if(reg_cost>50) {
                    MIN_MARGIN=(float)1.22;
                    ABSOLUTE_MIN_MARGIN=(float)1.22;
                }                
                
                
                /*if(sku.indexOf("SC_")>=0){
                    //There is no pickup cost with Sams
                    MIN_MARGIN=MIN_MARGIN-(float).02;
                    ABSOLUTE_MIN_MARGIN=ABSOLUTE_MIN_MARGIN-(float).02;
                } */
                
                Float retail_price=(float)-1;
                /*if(sku.indexOf("_AMA")>=0   ){
                      retail_price=reg_cost*(MIN_MARGIN*(float)1.4);
                   MIN_MARGIN=(float)1.25;
                    ABSOLUTE_MIN_MARGIN=(float)1;                    
                    System.out.println("Stuck SKU, setting ABSOLUTE_MIN_MARGIN to "+ABSOLUTE_MIN_MARGIN);                    
                    
                } */               
                
                /*if(sku.indexOf("_AMA")>=0 && stuckSkus.contains(sku) ){
                    
                   retail_price=reg_cost*(MIN_MARGIN*(float)1.4);
                   MIN_MARGIN=(float)1.25;
                    ABSOLUTE_MIN_MARGIN=(float)1;                    
                    System.out.println("Stuck SKU, setting ABSOLUTE_MIN_MARGIN to "+ABSOLUTE_MIN_MARGIN);                    
                    
                }     */           
                Float final_suggested_priceNew1=reg_cost*MIN_MARGIN;

                
                /*
                if((sku.indexOf("UN_")>=0 ||sku.indexOf("DT_")>=0) && weight<1){
                    //DT and UN do not support first class.
                    final_suggested_priceNew1=final_suggested_priceNew1+4;
                }                  
                if(sku.indexOf("UN_")>=0 && weight<4){
                    final_suggested_priceNew1=final_suggested_priceNew1+2;
                }   */             
               //  Product pr=getProductFromAmazon(sku,asin, reg_cost,con);
                 
                
                /*if(sku.indexOf("UN_")<0 && asin!=null && asin.indexOf("B")>=0 &&  asin.length()>4){
                     Product pr_NA=getProductFromAmazon(sku,asin, reg_cost,con);
                     if(pr_NA.getBestprice()>reg_cost*1.7+7  && p.getnReviews()>5){
                         System.out.println("Exception , see something is wrong, SKU:"+sku+"asin:"+asin+" best price: "+p.getBestprice() +" reviews:"+p.getnReviews() +" for such best reviewed products why people are selling more than 1.7+7 times than our OC. Is OC really correct? When the reviews are good, most probably people would be competing and selling for less in amazon");
                         
                     }
                }*/
                if(catg_desc.toUpperCase().indexOf("CHOCOLATE")>=0 ||item_name.toUpperCase().indexOf("CHOCOLATE")>=0 ) {

                               final_suggested_priceNew1=reg_cost*(float)1.3;
                               // final_suggested_priceNew1=final_suggested_priceNew1*(float)1.1;

                }
                if(sku.indexOf("DT_")>=0 ){
                    final_suggested_priceNew1=final_suggested_priceNew1+6;
                }                
                boolean productNeedsBubbleWrap=false;
                boolean heavyBottles=false;
                boolean reallyHeaviestBottles=false;
                if( 
                        //catg_desc.toUpperCase().indexOf("WATER")>=0   || item_name.toUpperCase().indexOf("WATER")>=0 || 
                        ( size.trim().indexOf("FZ")>=0  || size.trim().indexOf("ML")>=0  )
                                  //catg_desc.toUpperCase().indexOf("BEVERAGE")>=0  ||
                        ){
                    productNeedsBubbleWrap=true;

                    if(notBottlesApproved.contains(sku) ==false &&
                        packs_in_1_case>=4 && (weight==0 || weight>=10  ) ||catg_desc.toUpperCase().indexOf("PELLEGRINO")>=0  ||item_name.toUpperCase().indexOf("PELLEGRINO")>=0){
                        System.out.println("Heavy bottles, increasing to 1.35%");
                        heavyBottles=true;
                        final_suggested_priceNew1=final_suggested_priceNew1*(float)1.05;
                        if(notSureBottlesApproved.contains(sku) ==false && 
                            (weight>20 && final_suggested_priceNew1<100) ){
                            reallyHeaviestBottles=true;
                          System.out.println("REALLY REALLY Heavy bottles 25_ lbs, increasing to 1.35%");
                          final_suggested_priceNew1=final_suggested_priceNew1*(float)1.05;                            
                        }
                   }else
                    {
                        if(reg_cost<20)
                            final_suggested_priceNew1=final_suggested_priceNew1*(float)1.05;                        
                         System.out.println("Bottles but not so heavy, increasing to 1.35%");
                        final_suggested_priceNew1=final_suggested_priceNew1*(float)1.05;
                    }
                }
                if(sku.indexOf("SC_")>=0 &&( asin == null || asin.indexOf("B")<0)){
                    System.out.println("Sams product no matching asin found. increase price to 50%");                    
                    final_suggested_priceNew1=final_suggested_priceNew1*(float)1.2;                            
                }
                if(reallyHeaviestBottles==false && item_name.toUpperCase().indexOf("Pellegrino".toUpperCase())>=0){
                          System.out.println("Pellegrino, REALLY REALLY Heavy bottles 25_ lbs, increasing to 1.35%");
                          final_suggested_priceNew1=final_suggested_priceNew1*(float)1.4;                     
                }
                if(productNeedsBubbleWrap==false){
                    //Lets check from Singhs bubble wrapped products
                     String product_upc_ean=rs.getString("product_upc_ean");
                     if(product_upc_ean.length() > 10 && productsBubbleWrapped.contains(product_upc_ean)){
                         System.out.println("Product not bottle but Singh bubble wrapped. Increasing price");
                   // if(reg_cost<20)
                   //      final_suggested_priceNew=final_suggested_priceNew*(float)1.1;
                     final_suggested_priceNew1=final_suggested_priceNew1*(float)1.05;
                     

                     }
                }
                 if(buyBoxPrices.containsKey(sku) && buyBoxPrices.get(sku)>0){
                     Float buyBoxPrice=buyBoxPrices.get(sku);
                    // Float maxPrice= buyBoxPrice*(float).8-weight*(float).3;
                   //  if(buyBoxPrice>50)
                      Float maxPrice= buyBoxPrice*(float).9-((float)4+weight*(float).3);
                     if(productNeedsBubbleWrap||heavyBottles||reallyHeaviestBottles)
                         maxPrice= buyBoxPrice*(float).95-((float)4+weight*(float).3);
                     if(reallyHeaviestBottles)
                         maxPrice= buyBoxPrice-((float)4+weight*(float).3);
  
                     if(sku.indexOf("SC_")>=0 &&maxPrice>final_suggested_priceNew1*2 ){
                         System.out.println("Sams product, selling a lot in amazon, may be a bottle or oversize");
                         final_suggested_priceNew1=final_suggested_priceNew1*2;
                     }
                     if(sku.indexOf("DT_")<0 && 
                         buyBoxPrice>0  && final_suggested_priceNew1>maxPrice   ){
                         
                         System.out.println("We do not go above buy box price by non amazon, buy box price:"+buyBoxPrice +" absolute min price:"+reg_cost*ABSOLUTE_MIN_MARGIN);
                         if(maxPrice>reg_cost*ABSOLUTE_MIN_MARGIN){
                                 final_suggested_priceNew1=maxPrice;
                         }
                            
                         else
                             final_suggested_priceNew1=reg_cost*ABSOLUTE_MIN_MARGIN;
                     }
                 }

                 if(sku.indexOf("_FBA")>=0){
                     System.out.println(" FBA SKU, calculating MULTI CHANNEL FEES from reg_cost, 10% + FBA shipping fees (4.75+.75=6)+ $2 added for ad cost");
                     if(reg_cost>40)
                         reg_cost=reg_cost*(float).95;
                     final_suggested_priceNew1= (reg_cost*(float)1.15+(float)6+(float)Math.ceil(weight)*(float).7 )/(float).95;
                     final_suggested_priceNew1-=(float)4.99;
                     //Here, OC * 1.1 + 6 for fba pick pack, storage fees + weight fees *1.05 for google chagges
                 }
                if(reg_cost==0){
                    System.out.println("Original cost is unknown.Skipping");
                     continue;   
                }
               /* if(StringUtils.endsWith(String.valueOf(curPrice),".99")){
                    System.out.println("Current price ends with 99 cents. not touching it.");
                    continue;
                }*/
                 final_suggested_priceNew1=Math.round(final_suggested_priceNew1*(float)100)/(float)100; 
                 boolean productUpdateNeeded=false;
                 JSONObject jSONObject = new JSONObject();
                 String[] cats=StringUtils.substringsBetween(category_details,"Category ID:",",");
                /* if(sku.indexOf("CO_")>=0){
                     System.out.println("Costco product, lets add it to costco wholesale");
                     
                     JSONArray newCats=new JSONArray(); 
                     for (int i=0;i<cats.length;i++){
                         newCats.add(cats[i].trim());

                     }
                     if(newCats.contains("4988")==false){
                         productUpdateNeeded=true;
                         newCats.add("4988");
                         jSONObject.put("categories", newCats);
                     } 
                     
                 }      //End of costco loop*/
                 if(sku.indexOf("_FBA")>=0){
                     System.out.println("FBA  product, lets add it to bulk buy and save");
                     
                     JSONArray newCats=new JSONArray(); 
                     for (int i=0;i<cats.length;i++){
                         newCats.add(cats[i].trim());

                     }
                     if(newCats.contains(HBUpdateCategories.CAT_ID_BULK_AND_SAVE)==false){
                         productUpdateNeeded=true;
                         newCats.add(HBUpdateCategories.CAT_ID_BULK_AND_SAVE);
                         jSONObject.put("categories", newCats);
                     } 
                     
                 }      //End of costco loop                 
                 
               //  if(   new GregorianCalendar().get(Calendar.DAY_OF_WEEK) ==4 && cats.length==1 && lessThan10ProductsCat.containsKey(cats[0].trim())){
                   if(    cats.length==1 && lessThan10ProductsCat.containsKey(cats[0].trim())){
                     System.out.println("Product in category with less than 10 products, moving the product to parent categotry");
                      JSONArray newCats=new JSONArray(); 
                       newCats.add(lessThan10ProductsCat.get(cats[0].trim()));
                       jSONObject.put("categories", newCats);
                       productUpdateNeeded=true;
                 }
                  
                if(productUpdateNeeded==false && Math.abs(curPrice-final_suggested_priceNew1)<.1){
                 
                  continue;
                }        
                
                if(StringUtils.endsWith(String.valueOf(final_suggested_priceNew1),".99")){
                    final_suggested_priceNew1=   (float) (final_suggested_priceNew1 - .01);
                } 

                try{
                
                jSONObject.put("price", final_suggested_priceNew1);
                if(retail_price!=-1 && retail_price>final_suggested_priceNew1){
                      jSONObject.put("retail_price", retail_price);
                     if(stock_level>2) 
                     {
                        // HBUpdateCategories.createCategory(HBUpdateCategories.CAT_ID_ONSALE, catg_desc);
                         JSONArray onsaleCatArr=new JSONArray();
                           onsaleCatArr.add(HBUpdateCategories.CAT_ID_ONSALE);
                        jSONObject.put("categories", onsaleCatArr);
                         

                     }

                }
                BigCommerce.updateProduct(id, jSONObject);                
                    
                }catch (Exception e2){e2.printStackTrace();}

                    
                }
               
                 
                  
                 
                 
   }
    public static int amazonVisitCounter=0;
    public static Product getProductFromAmazon(String sku,String asin, Float oc,Connection con) throws Exception {
                Product p=new Product();
                p.setAsin(asin);
                String WHOLESALERNAME=StringUtils.substringBefore(sku, "_");
                p.setWHOLESALERNAME(WHOLESALERNAME);
                 ProcessWorkingCatalog pw=new ProcessWorkingCatalog();
             
                ArrayList<Product> ar=pw.getProductsFromTable(p, con, AMAZON_TABLE_NAME);
                if(ar.size()==0){
                    if(++amazonVisitCounter>5000){
                        System.out.println("Even though I do not have the product information in "+AMAZON_TABLE_NAME+" amazonVisitCounter exceeded 1000. So not trying today");
                    }else{
                        QueryProduct qp=new QueryProduct();
                        QueryProduct.I_WANT_ORIGINAL_BESTPRICE_AT_NOOTHER_FBA_SELLERS=true;
                        p=qp.getProductListings1(asin);
                        if(p.getBestprice()==0) {
                            System.out.println("ASIN IS CHANGED IN AMAZON. Lets try through getProductDetails API");
                            HashMap hm=MatchThePrice.getProductDetails(asin, true, false, false);
                            float bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);


                            p.setBestprice(bestprice);
                            p.setAsin(asin);

                        }
                        p.setItem_Code(sku);
                        p.setWHOLESALERNAME(WHOLESALERNAME);
                         pw.storeProduct3(p,  AMAZON_TABLE_NAME);
                        
                    }
                }
                 ar=pw.getProductsFromTable(p, con,AMAZON_TABLE_NAME);
                 float AMAZON_DISCOUNT=(float)0;
                for(int i=0;i<ar.size();i++){
                    if(i>0){
                       // System.out.println("Exception. Check Prabu why "+AMAZON_TABLE_NAME+" has more than 1 row for asin "+asin + " for same wholeasler ");
                        continue;
                    }
                    p=ar.get(i);    
                    try {
                         if(p.getBestprice()==0){
                            System.out.println("//PRABU CONTINUE NOW IF BESTPRICE IS UNKNOWN. ROW EXISTS IN "+AMAZON_TABLE_NAME+" but price is 0. May be asin not valid in amazon");
                            p.setBestprice((float)1000);
                           // continue;
                        } 
                            

                        if(p.getBestprice()==0){
                            QueryProduct qp=new QueryProduct();
                            QueryProduct.I_WANT_ORIGINAL_BESTPRICE_AT_NOOTHER_FBA_SELLERS=true;
                            p=qp.getProductListings1(asin);
                            p.setWHOLESALERNAME(WHOLESALERNAME);
                            pw.storeProduct3(p,   AMAZON_TABLE_NAME);
                        }
                    } catch(Exception exception) {
                        if(exception.getMessage().contains("ASIN_NOT_FOUND")) {
                            System.out.println("ASIN_NOT_FOUND");
                        } else {
                            System.out.println("Unusual Exception thwoing it. ");
                            throw exception;
                        }
                    }
                    
                Float bestprice=p.getBestprice();     
               
    }
     return p; 
   
    }
}
