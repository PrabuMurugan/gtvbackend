/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;

import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
 
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;
 
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import javax.net.ssl.HttpsURLConnection;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author prabu
 */
public class UpdateBlankUPCsInHb {
    public static ArrayList weightsUpdated=new ArrayList();
    static int MAX_ROWS=100;
    static int i=0;
     public static void main(String args[]) throws Exception{
             Connection con = RetrievePage.getConnection();
             System.out.println("Inside loadUNFIProductsToTable  Processing 10000 rows");
             //String sql=" select p.product_id,p.code,a.upc from  hb_bc.exported_products_from_hb  p  , hb_bc.original_cost o,tajplaza.amazon_catalog a where p.code like '%_AMA%' and p.code=o.sku and o.asin=a.asin   and p.product_upc_ean!=a.upc ";
             String sql="select 1 as product_id,sku as code, asin from tajplaza.variation_sku_upcs where upc='NA' and asin like 'B%' and asin not in (select asin from tajplaza.upcs_tried_blank_asins)   union select  p.product_id,p.code,u.asin from tajplaza.un_full_case_asins u , hb_bc.exported_products_from_hb p  where u.code=p.code    and length(p.product_upc_ean) < 5   and u.asin like 'B%'   and u.asin not in (select asin from tajplaza.upcs_tried_blank_asins) ";
             
             System.out.println("Executing SQL:"+sql);
             PreparedStatement selectStmt = con.prepareStatement(sql);
             PreparedStatement updateStmt=con.prepareStatement("update tajplaza.un_full_case_asins set product_upc_ean=? where asin=? ");
             PreparedStatement updateStmt2=con.prepareStatement("update tajplaza.variation_sku_upcs set upc=? where asin=? ");
             PreparedStatement insertStmt=con.prepareStatement("insert into tajplaza.upcs_tried_blank_asins (asin) values (?)");
            ResultSet rs=selectStmt.executeQuery();   
             while( rs.next()){
                if(i++>MAX_ROWS)
                    break;
                 String product_id=rs.getString("product_id");
                 String sku=rs.getString("code");
                 String asin=rs.getString("asin");
                 CreateHBProduct.NEED_UPC_FROM_SC=true;
                 String upc=CreateHBProduct.getUPCForAsin(asin);
  
                    System.out.println("Updating product_id:"+product_id+", sku:"+sku );
                    
                   try{
                       if(Integer.valueOf(product_id)>1)
                           updateUPC(product_id ,upc);
                       updateStmt.setString(1, upc); 
                       updateStmt.setString(2, asin); 
                       updateStmt.executeUpdate();

                       updateStmt2.setString(1, upc); 
                       updateStmt2.setString(2, asin); 
                       updateStmt2.executeUpdate();
                       
                       insertStmt.setString(1, asin);
                       insertStmt.executeUpdate();
                   }catch (Exception e4){e4.printStackTrace();}
                    
              }
             con.close();
              
             
     }   
  
      public static String updateUPC(String productid,String upc) {
        String webPage = "https://www.harrisburgstore.com/api/v2/products/"+productid+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
            JSONObject jSONObject = new JSONObject();

          
                jSONObject.put("upc", upc);
                   
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }    
}
