/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;

/**
 *
 * @author prabu
 */

import unfi.*;
import amazon.product.CreateCatalogNew;
import harrisburgstore.*;
import amazon.product.ProcessWorkingCatalog;
 
import bombayplaza.catalogcreation.CamelCamelCamel;
import bombayplaza.pricematch.RetrievePage;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import net.sf.json.JSONObject;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import amazon.product.QueryProduct;

public class UpdatePriceCostcoProducts {
      
    public static void main(String args[]) throws Exception{
     // Product  p = getUPCDetails("B00H9H56QA");
     //console(p);
       
     updatePrices();
     System.out.println("Program completed");

 } 
 
//   public static String DEBUG_SQL=" and p.code='CO_311_181679' ";

   public static String DEBUG_SKU="UN_461723";
 public static String AMAZON_TABLE_NAME="amazon_catalog_hb";
 
    public static void updatePrices() throws Exception{
            Connection con=RetrievePage.getConnection();
            PreparedStatement pst = null,pst2=null;
            ResultSet rs = null,rs2=null;
             String sort_order="";
             sort_order=String.valueOf(System.getProperty("sort_order",""));
             String PRODUCTION =String.valueOf(System.getProperty("PRODUCTION",""));
             if(PRODUCTION.equals("true"))
                 DEBUG_SKU="";
             
            
             //String sql = "  select upc,item_code sku,c.price reg_cost,p.price current_price, bestprice,secondbestprice,c.asin,p.product_id from hb_bc.hb_catalog c, hb_bc.exported_products_from_hb p where c.item_code=p.code   and  p.price<round(c.price*1.4) and stock_level>0  and ts>(curdate()-7) limit 100000 "  ;
            // String sql = "  select upc,item_code sku,c.price reg_cost,p.price current_price,p.product_id from hb_bc.hb_catalog c, hb_bc.exported_products_from_hb p where c.item_code=p.code   and  p.price<round(c.price*1.4) and stock_level>0  limit 100000 "  ;
             //FOR SAMS products
             //String sql = "  select sku sku,oc  reg_cost,p.price current_price,c.asin,p.product_id from hb_bc.original_cost c, hb_bc.exported_products_from_hb p where c.sku=p.code   and c.sku like 'SC_%' and c.asin is not null and length(c.asin)>0"  ;
            //REPLACE  ABOVE QUERY TO USE SKU IN WHERE CLAUSE
             String sql=
                     //"select item_code sku,c.price reg_cost,p.price current_price,p.product_id,c.asin from hb_bc.hb_catalog c, hb_bc.exported_products_from_hb p where c.item_code=p.code    and stock_level>0  "+
                       // " union "+
                        " select sku sku,oc  reg_cost,p.price current_price,p.product_id,c.asin,category_string,packs_in_1_case,p.name,weight,ui.size from hb_bc.original_cost c, hb_bc.exported_products_from_hb p left join  hb_bc.unfi_input_catalog ui on   replace(p.code,'UN_','') = ui.item_code    where   c.sku  like '%_%' and stock_level>0 and c.sku=p.code    and c.asin is not null and length(c.asin)>5 and c.asin like  'B%'   and (upper(p.name ) not like '%GREAT LAKES%'  and upper(brand_name ) not like '%GREAT LAKES%' )" ;
            System.out.println("Exceutung sql:"+sql);
            pst=con.prepareStatement(sql);
            rs=pst.executeQuery();
            int count=0;
            ProcessWorkingCatalog pw=new ProcessWorkingCatalog();
            int amazonVisitCounter=0;
            while(rs.next()){
               
                Product p=new Product();
                String asin=rs.getString("asin");
                String item_name =rs.getString("name");
                String catg_desc=rs.getString("category_string");
                String size=rs.getString("size");
                if(size==null)
                    size="NA";
                else if (size.indexOf("FZ")>=0)
                    System.out.println("debug");
                Integer packs_in_1_case=rs.getInt("packs_in_1_case");
                
                Float curPrice=rs.getFloat("current_price");
                Float weight=rs.getFloat("weight");
                 
                String sku=rs.getString("sku");                                 
                if(DEBUG_SKU!=null && DEBUG_SKU.trim().length()>0 && DEBUG_SKU.trim().equals(sku)==false){
                    continue;
                }
                String regCostS=rs.getString("reg_cost");
                System.out.println("sku:"+sku+",reg cost:"+regCostS);
                 if(regCostS==null || regCostS.trim().length()==0){
                    regCostS="0";
                   // continue;
                } 
                    
                if(StringUtils.countMatches(regCostS, ".")>1)
                    regCostS=StringUtils.substringBeforeLast(regCostS, ".");
                regCostS=regCostS.replaceAll("\\$","").replaceAll(",","");
                Float reg_cost=Float.valueOf(regCostS);
                String id=rs.getString("product_id");
                

                Float suggested_price00=reg_cost*(float)1.3+2;
                
                Float suggested_price01=reg_cost*(float)1.3+(float)2;
                Float suggested_price1=reg_cost*(float)1.4+(float)3;
                Float suggested_price2=reg_cost*(float)1.5+(float)4;
                Float suggested_price3=reg_cost*(float)1.6+(float)6;
                Float suggested_price4=reg_cost*(float)1.8+(float)8;
                System.out.println("Processign count:"+count++ +",asin :"+asin+",sku:"+sku+",suggested_price0:"+suggested_price00);
                
                Float final_suggested_price=suggested_price00;
                 p=UpdateHBPrices.getProductFromAmazon(sku,asin, reg_cost,con);
                    
                Float bestprice=p.getBestprice();
                 /*if(p.getSecondbestprice()>p.getBestprice()*1.2){
                     bestprice=p.getSecondbestprice();
                     System.out.println("Second best price is 20% more. using that price as the best price:"+p.getSecondbestprice());
                 }*/
                     
               
                 if(sku.indexOf("UN_")>=0){
                     int pkgQnt=1;
                     if(p.getPkgQnt()>1)
                         pkgQnt=p.getPkgQnt();
                     else
                        pkgQnt=pw.calculatePackingQntFromTitle(p.getTitle());
                  
                    if(catg_desc.toUpperCase().indexOf("BULK")>=0)
                        packs_in_1_case=1;
                    float casePriceOnAmazon=(bestprice/pkgQnt)*packs_in_1_case;  
                    bestprice=casePriceOnAmazon;
                    System.out.println("For UNFI product, the amazon best price is :"+bestprice);
                 }
                System.out.println("Best price for asin : "+asin +" is "+p.getBestprice()+ " bestprice  is "+bestprice+"sku: "+sku);
               
                //if(casePriceOnAmazon==0 ||   suggested_price1<casePriceOnAmazon*.90  )
                float AMAZON_DISCOUNT=(float).95;
                if(bestprice>100)
                    AMAZON_DISCOUNT=(float).95;
                else
                    AMAZON_DISCOUNT=(float).9;
                    if(   suggested_price01<=bestprice*AMAZON_DISCOUNT )
                    {
                        final_suggested_price=suggested_price01;
                        System.out.println("trace 01: I can increase the price to "+final_suggested_price+ " still less than 90% from "+bestprice+"sku: "+sku);
                        if(   suggested_price1<bestprice*AMAZON_DISCOUNT )
                        {

                                 final_suggested_price=suggested_price1;
                                System.out.println("trace 1: I can increase the price to "+final_suggested_price+ " still less than 90% from "+bestprice+"sku: "+sku);

                            if(   suggested_price2<bestprice*AMAZON_DISCOUNT   ){

                                final_suggested_price=suggested_price2;
                                System.out.println("trace 2 :  I can increase the price to "+final_suggested_price+ " still less than 90% from "+bestprice+"sku: "+sku);

                            }
                             
                    
                            if(   suggested_price3<bestprice*AMAZON_DISCOUNT && ( weight>15 || 
                                catg_desc.toUpperCase().indexOf("WATER")>=0||catg_desc.toUpperCase().indexOf("BULK")>=0 
                                || (catg_desc.toUpperCase().indexOf("BEVERAGE")>=0  ||item_name.toUpperCase().indexOf("WATER")>=0 || size.trim().indexOf("FZ")>=0  ) ) ){

                                final_suggested_price=suggested_price3;
                                System.out.println("trace 3 :  I can increase the price to "+final_suggested_price+ " still less than 90% from "+bestprice+"sku: "+sku);

                            }
                            if(   suggested_price4<bestprice*AMAZON_DISCOUNT   && ( weight>15 || 
                                catg_desc.toUpperCase().indexOf("WATER")>=0||catg_desc.toUpperCase().indexOf("BULK")>=0 
                                || catg_desc.toUpperCase().indexOf("BEVERAGE")>=0  ||item_name.toUpperCase().indexOf("WATER")>=0  || size.trim().indexOf("FZ")>=0 ) ){

                                final_suggested_price=suggested_price4;
                                System.out.println("trace 4 :  I can increase the price to "+final_suggested_price+ " still less than 90% from "+bestprice+"sku: "+sku);

                            }                            

                        }   
                    }
                else{
                      System.out.println("trace 0: I can increase the price ONLY to 25% "+final_suggested_price+ " still less than 90% from "+bestprice+"sku: "+sku);
                }
                if(reg_cost==0){
                    System.out.println("Original cost is unknown. Giving 10% discount from amazon price");
                       final_suggested_price=bestprice*AMAZON_DISCOUNT;
                }
                if(StringUtils.endsWith(String.valueOf(curPrice),".99")){
                    System.out.println("Current price ends with 99 cents. not touching it.");
                    continue;
                }
                if(Math.abs(curPrice-final_suggested_price)<.1){
                    continue;
                }        
                
                if(StringUtils.endsWith(String.valueOf(final_suggested_price),".99")){
                    final_suggested_price=   (float) (final_suggested_price - .01);
                } 

                try{
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("price", final_suggested_price);
                BigCommerce.updateProduct(id, jSONObject);                
                    
                }catch (Exception e2){}

                    
                }
               
                 
                  
                 
                 
             
            if(!con.isClosed()) con.close();
    }
    
     
}
