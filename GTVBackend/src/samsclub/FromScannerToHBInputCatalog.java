/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;

import bombayplaza.pricematch.RetrievePage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author US083274
 */
public class FromScannerToHBInputCatalog {
    public static void main(String args[]) throws Exception{
        convertScannerIntoHBInputCatalog();
    }
    public static boolean STORE_ITEM_CODE_EXISTS=false;
    public static void convertScannerIntoHBInputCatalog() throws Exception{
        String s;
        BufferedReader  br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\tmp\\from_scanner.txt")));
        StoreUPCVo u=new StoreUPCVo();
        LinkedHashMap <String,StoreUPCVo> map=new LinkedHashMap ();
        String rowNum=null;
        String prevS="";
        while ((s = br0.readLine()) != null)   {
            if(s.trim().length()>0){
                if(s.equals(prevS))   
                    continue;
               prevS=s;
                s=s.replaceAll("'", "");
                if(s.indexOf(".")>=0)
                    s=StringUtils.substringBefore(s, ".");
                if(u.getUpc()==null){
                    //u=new StoreUPCVo();
                    try{
                            if(s.indexOf("00")>=0 && Integer.valueOf(s)==0){
                            System.out.println("00 found, setting to new row");
                            u=new StoreUPCVo();
                            u.setRow_num(rowNum);
                            continue;
                        }
                    }catch (Exception e){}
                    if(s.length()<5 ){
                        if( u.getRow_num()==null || s.length()==3){
                            System.out.println("UPC not set, row num not set, current length row less than 5, it should be a row num "+s);
                            u.setRow_num(s);
                            rowNum=s;
                            continue;
                        }else{
                            System.out.println("Something is wrong. I am expecting new row num, but row num already set:"+s);
                            System.exit(1);
                        }
                    }
                    if(StringUtils.removeStart(s.replaceAll("'",""),"0000").length()<10 ){
                        System.out.println("Looking for UPC, but got something else. "+ s);
                        System.exit(1);
                    }else{
                        u.setUpc(s);
                        continue;
                    }
                    
                    
                }else if (STORE_ITEM_CODE_EXISTS && u.getStore_item_code()==null){
                    u.setStore_item_code(s);
                    continue;
                }else{
                    u.setStore_price(s);
                    System.out.println("Got one full row : ROWNUM: "+u.getRow_num()+",UPC: "+u.getUpc()+" ,Item Code: "+u.getStore_item_code()+" , price:"+u.getStore_price());
                    if(map.containsKey(u.getUpc())){
                        map.remove(u.getUpc());
                    }
                    map.put(u.getUpc(), u);
                    u=new StoreUPCVo();
                    u.setRow_num(rowNum);
                    continue;
                }
            }
                 
        }//End of while loop to process  all rows
        Iterator iter=map.keySet().iterator();
 Connection con = RetrievePage.getConnection();
  PreparedStatement pst=con.prepareStatement("truncate table hb_bc.hb_input_catalog");
  pst.executeUpdate();
        while(iter.hasNext()){
               u=(StoreUPCVo)map.get(iter.next());
               System.out.println("Going to insert : ROWNUM: "+u.getRow_num()+",UPC: "+u.getUpc()+" ,Item Code: "+u.getStore_item_code()+" , price:"+u.getStore_price());
         
              insertIntoHbInput(u,con);
        }
        con.close();

    }
    public static void insertIntoHbInput(StoreUPCVo u,Connection con) throws Exception{
        String sql="insert into hb_bc.hb_input_catalog (row_num,upc,store_price,store_item_code) values (?,?,?,?)";
       
        PreparedStatement insertStmt =con.prepareStatement(sql);
        insertStmt.setString(1, u.getRow_num());
        insertStmt.setString(2, u.getUpc());
        insertStmt.setString(3, u.getStore_price());
        insertStmt.setString(4, u.getStore_item_code());
        insertStmt.executeUpdate();
    }
}
