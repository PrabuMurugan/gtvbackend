/*Costco products sometime move around. This program updates the new SKU for the same UPC into hb store*/
package samsclub;

import unfi.*;
import bombayplaza.pricematch.RetrievePage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;



public class UpdateCostcoNewSKUROWS {
    private static String QUERY = "select product_id, code old_sku,item_code new_sku,name from hb_bc.hb_catalog h , hb_bc.exported_products_from_hb where product_upc_ean=upc and  item_code!=code and item_code like 'CO%' and code like 'CO%'";
   // private static String UPDATE_HB_CATALOG  = " update  hb_bc.hb_catalog set item_code=? , price=? where  SUBSTRING_INDEX(item_code, '_', -1)=? and wholesalername='CO'";

    
    
    public static void main(String [] args) throws Exception{
        String WHOLESALERNAME="";
             System.out.println("Enter WHOLESALERNAME : BB/BJ/CO/SC");
            BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
            WHOLESALERNAME=buffer.readLine();
            QUERY=QUERY.replaceAll("CO", WHOLESALERNAME);
        System.out.println("Inside main of UpdateUNFIProductTitle");
        long iStart = System.currentTimeMillis();
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap<String, JSONObject > map = new HashMap<String, JSONObject>();
                
        try {

            connection =  RetrievePage.getConnection();
            System.out.println("Going to execute Query : " + QUERY);
            ps = connection.prepareStatement(QUERY);
            rs = ps.executeQuery();
            System.out.println("After executing Query");
            while(rs.next()) {
                String strProductId = rs.getString("product_id");
                String code = rs.getString("old_sku");
                String item_code = rs.getString("new_sku");
                if(code.equals(item_code)==false){
                    
                System.out.println(strProductId + "\t" + code + "\t" + item_code );
                JSONObject jsono = new JSONObject();
                jsono.put("sku", item_code);
               
                 updateProduct(strProductId, jsono);
                }
                 
            }
            
               
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                //if(rs != null && !rs.isClosed()) rs.close();
                //if(ps != null && !ps.isClosed()) ps.close();
                if(connection != null && !connection.isClosed()) connection.close();
            } catch (SQLException ex) {
                            }
        }
        
     
    }
    
     public static String updateProduct(String productid, JSONObject jSONObject) {
            String webPage = "https://www.harrisburgstore.com/api/v2/products/"+productid+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            httpCon.setConnectTimeout(300000);
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
              
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }    
   
}
