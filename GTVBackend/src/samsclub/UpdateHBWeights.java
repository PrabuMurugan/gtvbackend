/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;

import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
 
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;
 
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import javax.net.ssl.HttpsURLConnection;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author prabu
 */
public class UpdateHBWeights {
    public static String AND_DEBUG_SKU_SQL=" and p.code ='VAR_B007S0QYAU_PRAB'";
    public static HashMap<String,Float> shippedWeights=new HashMap();
     public static void main(String args[]) throws Exception{
       //  updateInitialWeights();
         //updateInitialWeightsDUMMY();
            String PRODUCTION=System.getProperty("PRODUCTION","false");
             
            if(PRODUCTION.equals("true")){
               AND_DEBUG_SKU_SQL="";
            }         
         
             Connection con = RetrievePage.getConnection();
             
             System.out.println("Inside loadUNFIProductsToTable  Processing 10000 rows");
             String sql=" select  distinct p.product_id  , sku  ,title ,  product_weight,p.weight,p.width,p.height,p.depth from hb_bc.shipped_weights  p  where 1=1  " ;
                     //+ "  union "
                     //+ " select  p.product_id  , p.code  ,p.name, t.weight,p.weight from hb_bc.exported_products_from_hb p, tajplaza.un_full_case_asins  t where p.code=t.code and    p.weight like '%.99%' and t.weight !=0 ";
             System.out.println("Executing SQL:"+sql);
             PreparedStatement selectStmt = con.prepareStatement(sql);
            ResultSet rs=selectStmt.executeQuery();   
             while( rs.next()){
               
                 String product_id=rs.getString("product_id");
                 String sku=rs.getString("sku");
                /* if(DEBUG_SKU!=null && DEBUG_SKU.length()>0 && sku.equals(DEBUG_SKU)==false){
                     continue;
                 }*/
                 Float new_weight=rs.getFloat("product_weight");
                 Float weightOnHB=rs.getFloat("weight");
                 Float width=rs.getFloat("width");
                 Float height=rs.getFloat("height");
                 Float length=rs.getFloat("depth");
                 if(sku.equals("UN_428557")){
                     System.out.println("debug");
                 }
                     
                 //Lets increase all the weight by 10% to cover unexpected problems
                  //new_weight=new_weight*(float)1.1;
                   if(Math.abs(new_weight-weightOnHB)>.1 )         {
                       // System.out.println("Updating product_id:"+product_id+", sku:"+sku+", weight:"+new_weight);
                           if(shippedWeights.containsKey(sku) && shippedWeights.get(sku)>new_weight){
                              // System.out.println("Weight is already updated from a bigger shipment, so skipping. weight updated:"+shippedWeights.get(sku)+",current weight:"+new_weight);
                               continue;
                           }
                       try{
                           //BigCommerce.updateProduct(product_id, js);

                           //  updateWeight(product_id, Float.valueOf(new_weight) , width, height,length);
                           //Using thsi weight in later part if we can not get the weight from FBA calculator.
                           shippedWeights.put(sku, new_weight);
                       }catch (Exception e4){e4.printStackTrace();}
                       
                   }
                     
                    
              }
             con.close();
              updateInitialWeights();
             
     }   
     
    public static void updateInitialWeights () throws Exception{
            Connection con=RetrievePage.getConnection();
            PreparedStatement pst = null,pst2=null;
            ResultSet rs0 = null,rs2=null;
             String sort_order="";
             sort_order=String.valueOf(System.getProperty("sort_order",""));
            String un_full_case_sql="select code,asin,weight from tajplaza.un_full_case_asins ";
            String asin_original_cost_sql="select sku, asin from hb_bc.original_cost  ";
            String manual_weights_entered_sqlNew="select sku,new_weight from hb_bc.manual_weights_corrected";
            String manual_weights_entered_override_sqlNew="select sku,new_weight from hb_bc.manual_weights_corrected where override_everything=true";
            String dotShippingWeightsSql="select concat('DT_',dot_item),shipping_weight,length,width,height from harrisburgstore.dotfoods_instock where concat('DT_',dot_item)=?";
            String unfiShippingWeightSQL="select concat('UN_',item_code),dropship_weight from hb_bc.unfi_input_catalog where concat('UN_',item_code)=?";
            
      //    String sql="select p.product_id,p.code,u.asin,p.weight,ui.pack,u.name title,ui.size, category_string,width,p.price  from hb_bc.exported_products_from_hb p, hb_bc.unfi_input_catalog ui, hb_bc.original_cost u where replace(p.code,'UN_','') = ui.item_code and p.code=u.sku and  code not in (select  ho.sku from   HB_BC.tmp_shipstation_order_weights so, HB_BC.tmp_ordered_item_weights ho where so.order_id=ho.order_id) and item_type='Product'    "
        //          + " union  select p.product_id,p.code,o.asin,p.weight,1 as pack,p.name as title,1 as size, category_string,width,p.price  from hb_bc.exported_products_from_hb p, hb_bc.original_cost o  where p.code=sku   and   code not in (select  ho.sku from   HB_BC.tmp_shipstation_order_weights so, HB_BC.tmp_ordered_item_weights ho where so.order_id=ho.order_id) and item_type='Product'    and p.code not like 'UN_%' "
          //         + " union select p.product_id,p.code,'NA',p.weight,1 as pack,p.name as title,1 as size, category_string,width,p.price  from hb_bc.exported_products_from_hb p   where p.code not in (select sku from hb_bc.original_cost)  and item_type='Product'  ";
             
         /* String sql="select p.product_id,p.code,u.asin,p.weight,pack,u.name title,size, category_string,width,p.price  from hb_bc.exported_products_from_hb p, tajplaza.un_full_case_asins u where p.code=u.code and  item_type='Product'   "+AND_DEBUG_SKU_SQL
                  + "   union  select p.product_id,p.code,o.asin,p.weight,1 as pack,p.name as title,1 as size, category_string,width,p.price  from hb_bc.exported_products_from_hb p, hb_bc.original_cost o  where p.code=sku    and item_type='Product'    and p.code  not in (select code from tajplaza.un_full_case_asins )  "+AND_DEBUG_SKU_SQL
          + " union select p.product_id,p.code,'NA',p.weight,1 as pack,p.name as title,1 as size, category_string,width,p.price  from hb_bc.exported_products_from_hb p   where  item_type='Product'   and   p.code  not in (select code from tajplaza.un_full_case_asins )   and p.code  not in (select sku from hb_bc.original_cost)     "+AND_DEBUG_SKU_SQL;
           */
            String sqlSizePacks="select concat('UN_',item_code),size,pack from hb_bc.unfi_input_catalog union select  concat('CM_',item_code),size,pack from hb_bc.wholesaler_input_catalog";
            PreparedStatement pst_sqlSizePacks=con.prepareStatement(sqlSizePacks);
            rs0=pst_sqlSizePacks.executeQuery();
            HashMap<String,String> sizePacksMap=new HashMap();
            while(rs0.next()){
                sizePacksMap.put(rs0.getString(1), rs0.getString(2)+"||"+rs0.getString(3));
            }
                
             String sql=" select p.product_id,p.code,'NA' asin,p.weight,1 as pack,p.name as title,1 as size, category_string,width,p.price  from hb_bc.exported_products_from_hb p   where item_type='Product'   and   cast(stock_level as decimal)>0  and code not like 'DT%' "+AND_DEBUG_SKU_SQL ;
            System.out.println("Executing query INSIDE updateInitialWeights:"+sql);
            pst=con.prepareStatement(sql);
            PreparedStatement pst_un_full_caseNew=con.prepareStatement(un_full_case_sql);
            rs0=pst_un_full_caseNew.executeQuery();
            HashMap<String,String> un_full_caseAsins=new HashMap();
            while(rs0.next())
                un_full_caseAsins.put(rs0.getString(1), rs0.getString(2));
            PreparedStatement pst_original_cost=con.prepareStatement(asin_original_cost_sql);
            HashMap<String,String> original_costMap=new HashMap();
            rs0=pst_original_cost.executeQuery();
            while(rs0.next())
                original_costMap.put(rs0.getString(1), rs0.getString(2));
            
           PreparedStatement pst_wegmans=con.prepareStatement("select weight_lbs from hb_bc.wegmans_product   where replace(replace(replace('WG_AISLE_UPCPACK1_PRICE','AISLE',aisle),'UPC',gtin),'PRICE',price)=?");;
            

            
            HashMap<String,Float> manulWeightsEntered=new HashMap();
            HashMap<String,Float> manulWeightsEnteredOverrideEverything=new HashMap();
            
            PreparedStatement manual_weights_enteredNew=con.prepareStatement(manual_weights_entered_sqlNew);
            rs0=manual_weights_enteredNew.executeQuery();
            while(rs0.next())
                manulWeightsEntered.put(rs0.getString(1), rs0.getFloat(2));
            
              manual_weights_enteredNew=con.prepareStatement(manual_weights_entered_override_sqlNew);
            rs0=manual_weights_enteredNew.executeQuery();
            while(rs0.next())
                manulWeightsEnteredOverrideEverything.put(rs0.getString(1), rs0.getFloat(2));            
             
            PreparedStatement dotShippingWeightsPs=con.prepareStatement(dotShippingWeightsSql);
            PreparedStatement unfiShippingWeightsPs=con.prepareStatement(unfiShippingWeightSQL);
        

            
            rs0=pst.executeQuery();
            int count=0;
                while(rs0.next()){
                try{
                    
                
                String id=rs0.getString("product_id");
                String sku=rs0.getString("code");
                /* if(DEBUG_SKU!=null && DEBUG_SKU.length()>0 && sku.equals(DEBUG_SKU)==false){
                     continue;
                 }*/
                
                System.out.println("Processign count:"+count+++"SKU:"+sku);

                String title=rs0.getString("title");
                String packs=rs0.getString("pack");
                String asin=rs0.getString("asin");
                String size0=rs0.getString("size");
                if(sizePacksMap.containsKey(sku)){
                   size0= StringUtils.substringBefore( sizePacksMap.get(sku),("||"));
                   packs=  StringUtils.substringAfter( sizePacksMap.get(sku),("||"));
                }
                if(size0==null)
                    size0="";
                 
                Float width=(float)1;
                Float length=(float)1;
                Float height=(float)1;                
                Float weightOnHB =rs0.getFloat("weight");
                Float originalWeight=weightOnHB;
                /*if(AND_DEBUG_SKU_SQL.length()==0 && originalWeight!=0 && manulWeightsEntered.containsKey(sku)==false && shippedWeights.containsKey(sku)==false 
                        && new GregorianCalendar().get(Calendar.DAY_OF_WEEK) !=6){
                    System.out.println("Today is not friday and there is already weight present. So skipping it for friday ");
                    continue;
                }*/
                Float price =rs0.getFloat("price");
                
                String category_string=rs0.getString("category_string");
                
      
                String weight1=rs0.getString("weight");
               
                RetrievePage.JUST_DIMENSIONS_NEEDED=true;
                boolean weightFromFBACalculator=false;
                boolean weightFromAmazonPage=false;
                boolean weightFromPreviousShipment=false;
                
                if(asin==null || asin.length()<4){
                 
                    if(original_costMap!=null  && original_costMap.containsKey(sku) && original_costMap.get(sku)!=null && original_costMap.get(sku).indexOf("B")>=0 ){
                        asin=original_costMap.get(sku);
                        System.out.println("Asin not present in un_full_case_asins. Asin from original_cost tabe is  "+asin);
                        
                    }else{
                        if(sku.indexOf("AMA")>=0 && StringUtils.substringBefore(sku, "_")!=null && StringUtils.substringBefore(sku, "_").indexOf("B")>=0){
                            asin=StringUtils.substringBefore(sku, "_");
                        }
                    }
                }
                Boolean weight_from_amazon_page=false;
                if(asin!=null && asin.length()>=5){
                    RetrievePage.USE_PROXIES=true;
                    CreateCatalog.ERROR_MOVE_TO_NEXT_PAGE=true;
                    HashMap fbaMap=RetrievePage.getDetailsFromFbaCalculatorNewMWS(con,asin,"100",true);
                    weight_from_amazon_page=(Boolean) fbaMap.get("weight_from_amazon_page");
                    if(weight_from_amazon_page == null )
                        weight_from_amazon_page=false;
                    if(fbaMap!=null && fbaMap.containsKey("productInfoWeight") &&  (Float)fbaMap.get("productInfoWeight")>0){
                        weight1=String.valueOf(fbaMap.get("productInfoWeight")  );

                        length=(Float)(fbaMap.get("productInfoLength")  );
                        width=(Float)(fbaMap.get("productInfoWidth")  );
                        height=(Float)(fbaMap.get("productInfoHeight")  );

                        if(Float.valueOf(weight1)>0)
                            weightFromFBACalculator=true;
                        
                    }
                    
                }

                if(weightFromFBACalculator==false || weight_from_amazon_page || (shippedWeights.containsKey(sku) && shippedWeights.get(sku)>Float.valueOf(weight1))){
                    //FBA calculator is original . If its not available there then lets try different avenues
                    if(shippedWeights.containsKey(sku )){
                                Float weightF=(Float)shippedWeights.get(sku);                        
                                weight1=String.valueOf(weightF);   
                                
                                  weightFromPreviousShipment =true;
                                  weight_from_amazon_page=false;
                                  weightFromFBACalculator=false;
                                  System.out.println("Using weight from previous shipment on SS: "+weight1);
                    }
                                 
                } 
                

                String approxWeight="0";
   
                    System.out.println("Category is :"+category_string);

                 if(category_string!=null && (category_string.toUpperCase().indexOf("WATER")>=0||category_string.toUpperCase().indexOf("BULK")>=0 
                            || category_string.toUpperCase().indexOf("BEVERAGE")>=0  ||title.toUpperCase().indexOf("WATER")>=0
                         || size0.trim().indexOf("FZ")>=0
                          || title.toUpperCase().trim().indexOf("FL")>=0
                         || title.toUpperCase().trim().indexOf("FZ")>=0
                         )){
                         
                        approxWeight=String.valueOf((price)  );
                 }
                 else {
                     if(sku.indexOf("UN_")>=0)
                          approxWeight=String.valueOf((price/(float)10)*(float)1);
                     else
                        approxWeight=String.valueOf((price/(float)3)*(float)1);
                 }
 
                 Float weight_Un_full_case_asins=(float)0;
                 
                 if(un_full_caseAsins.containsKey(sku) &&un_full_caseAsins.get(sku).indexOf("B")>=0 ){
                     try{
                             String un_full_case_asin=un_full_caseAsins.get(sku);
                            //HashMap fbaMap=RetrievePage.getDetailsFromFbaCalculator(con,un_full_case_asin,"100",true);
                            HashMap fbaMap =RetrievePage.getDetailsFromFbaCalculatorNewMWS(con,un_full_case_asin,"100",true);
                            if( fbaMap!=null && fbaMap.containsKey("productInfoWeight") && (Float)fbaMap.get("productInfoWeight")>0){
                               weight_from_amazon_page=(Boolean) fbaMap.get("weight_from_amazon_page");
                                weight_Un_full_case_asins=(Float)fbaMap.get("productInfoWeight") ;
                             if(weight_Un_full_case_asins>0)
                                 weightFromFBACalculator=true;
                         System.out.println("we have un full case asins weight:"+weight_Un_full_case_asins);
                         Float weightF=weight_Un_full_case_asins;
                         weight1=String.valueOf(weightF);
                         if(weightF<1)
                             weightF=(float)1;
                         approxWeight=String.valueOf(weightF);
                      }
                      }catch (Exception e2){e2.printStackTrace();}
                 }
                 if(Float.valueOf(approxWeight)>50)
                     approxWeight="50";
                 //if(weightFromFBACalculator==false &&  Float.valueOf(weight)>60)
                 if(  Float.valueOf(weight1)>60)
                     weight1="60";

                 boolean approximateCalculation=false;     
                 if((weightFromFBACalculator==false && weightFromAmazonPage==false && weightFromPreviousShipment==false)|| weight1==null || weight1.equals("null") || Float.valueOf(weight1)==0 || Float.valueOf(weight1)==3.99){    
                     weight1=approxWeight;
                     approximateCalculation=true;
                 }
                
               
                Float weightF=Float.valueOf(weight1);
              //  if(title.toLowerCase().indexOf("pack of")<0 && (title.toLowerCase().indexOf("pack")>0) || title.toLowerCase().indexOf("set")>0 )
                  //  continue;
                int pkgQnt=ProcessWorkingCatalog.calculatePackingQntFromTitle(title);
                 if(size0.trim().equals("#")  || category_string.toUpperCase().indexOf("BULK")>=0){
                     pkgQnt=1;
                     packs="1";
                 }
                    
                if(pkgQnt==0)
                    pkgQnt=1;
                Float weightPerPack=weightF/pkgQnt;
                boolean guessedWeight=false;
                int packssI=1;
                try{
                    packssI=Integer.valueOf(packs)    ;
                    if(packssI<pkgQnt)
                        packssI=pkgQnt;
                }catch (Exception e){}
                Float caseWeight=Float.valueOf(weight1);
                        
                if(approximateCalculation==false && weightFromPreviousShipment==false&&  (sku.indexOf("UN_")>=0 || sku.indexOf("CM_")>=0)){
                           caseWeight= weightPerPack*packssI;
                           if(caseWeight>50 || caseWeight>6*Float.valueOf(approxWeight)){
                               caseWeight=weightPerPack;

                           } 
                           if(weight_Un_full_case_asins>0 && weight_Un_full_case_asins<caseWeight){
                               System.out.println("caseWeight is "+caseWeight + " and using it");
                               caseWeight=weight_Un_full_case_asins;
                           }
                            if( shippedWeights.containsKey(sku) && (Float)shippedWeights.get(sku) >caseWeight){
                                System.out.println("weight_Un_full_case_asins is 0, but we have shipped it in the past, lets use that weight ");
                                caseWeight=(Float)shippedWeights.get(sku);
                            }  

    
                }
                Float sizeF=(float)0;
                try{
                    if ( size0.indexOf("LOZ")<0){
                        String tmpsize=size0;
                        if(size0.indexOf("OZ")>=0 )
                             tmpsize=StringUtils.substringBefore(size0, "OZ");
                        if(size0.indexOf("FZ")>=0f )
                             tmpsize=StringUtils.substringBefore(size0, "FZ");
                        
                         if(tmpsize.indexOf("/")>=0){
                            sizeF=Float.valueOf(StringUtils.substringBefore(tmpsize, "/"))*Float.valueOf(StringUtils.substringAfter(tmpsize, "/"));
                            sizeF=sizeF/16;

                         }else{
                             sizeF=Float.valueOf(tmpsize)/16;
                         } 
                         if(size0.indexOf("FZ")>=0 )
                             sizeF=sizeF*(float)1.25;
                        
                    } 
                }catch (Exception e2){
                  //  e2.printStackTrace();
                }

                   if( approximateCalculation && (sku.indexOf("UN_")>=0 ||sku.indexOf("CM_")>=0) ){
                       if (size0.length()>0 &&   size0.indexOf("LOZ")<0){
                         try{
                             System.out.println("Even the pack X Size is great than case weight, fba calculator may be wrong. Using packs*size");
                            Float packI=Float.valueOf(packs);
                            if(sizeF*packI>caseWeight){
                                  guessedWeight=true;
                                caseWeight=sizeF*packI;                                  
                            }


                         }catch (Exception e2){e2.printStackTrace();}
                   
                   }
                    
                  }
                if(sku.indexOf("DT_")>=0 ){
                    if(true){
                        System.out.println("DOT ITEM, ignoring, continue, assuming the weight is set properly at creation time");
                        continue;
                    }
                    
                    dotShippingWeightsPs.setString(1, sku);
                    ResultSet rs1=dotShippingWeightsPs.executeQuery();
                    if(rs1.next()){
                        weight1=rs1.getString("shipping_weight");
                        caseWeight=rs1.getFloat("shipping_weight");
                        length=rs1.getFloat("length");
                        width=rs1.getFloat("width");
                        height=rs1.getFloat("height");
                        approximateCalculation=false;
                        weight_from_amazon_page=false;
                        guessedWeight=false;
                    }
                    System.out.println("Dot foods, getting weight from dot table ");
                    
                }   
                if(sku.indexOf("UN_")>=0 ){
                    if(true){
                      //  System.out.println("UNFI ITEM, ignoring, continue, assuming the weight is set properly at creation time");
                       // continue;
                    }
                    
                    unfiShippingWeightsPs.setString(1, sku);
                    ResultSet rs1=unfiShippingWeightsPs.executeQuery();
                    while(rs1.next() && rs1.getString("dropship_weight") !=null && rs1.getFloat("dropship_weight")>0){
                         System.out.println("UNFI, Dropship weight is "+rs1.getString("dropship_weight") +"  and   using it");
                         weight1=rs1.getString("dropship_weight");
                        caseWeight=rs1.getFloat("dropship_weight");
                        length=(float)1;
                        width=(float)1;
                        height=(float)1;
                        approximateCalculation=false;
                        weight_from_amazon_page=false;
                        guessedWeight=false;
                          
                    }
                    
                    
                }                   
               /* try{

                    
                     if(packssI*sizeF>caseWeight){
                         System.out.println("Even the pack X Size is great than case weight, fba calculator may be wrong. Using packs*size");
                         caseWeight=packssI*sizeF;
                         guessedWeight=true;

                     }
                }catch (Exception e2){}  */                 
                if(approximateCalculation==false && caseWeight<20  ){
                    caseWeight=caseWeight*(float)1.1;
                    //length=length*(float)1.1;
                    
                }
              
                Float manualNewWeight=(float)0;
                if(weight_from_amazon_page==null)
                    weight_from_amazon_page=false;
                if(weight_from_amazon_page){
                    System.out.println("weight_from_amazon_page is true, set .49 at end ");
                }
            if(shippedWeights.containsKey(sku)  &&(Float)shippedWeights.get(sku)>Float.valueOf(weight1) ){
                System.out.println("Shipped weight is more and that is the only truth");
                         weightF=(Float)shippedWeights.get(sku);                        
                        weight1=String.valueOf(weightF);   

                          weightFromPreviousShipment =true;
                          weight_from_amazon_page=false;
                          weightFromFBACalculator=false;
                          System.out.println("Using weight from previous shipment on SS: "+weight1);
            }                
            if(weight1 != null  &&  weightFromPreviousShipment==false &&(approximateCalculation ||weight_from_amazon_page||guessedWeight)) {
                 weight1=StringUtils.substringBefore(String.valueOf(weight1),".")+".49";
                 caseWeight=Float.valueOf(StringUtils.substringBefore(String.valueOf(caseWeight),".")+".49");
  
                if(manulWeightsEntered.containsKey(sku)){
                    manualNewWeight=(float)manulWeightsEntered.get(sku);
                    caseWeight=manualNewWeight;
                     weight1=String.valueOf(manualNewWeight);
                     System.out.println("Using manually updated weight : "+manualNewWeight);

                }                 
            } else if (StringUtils.endsWith(String.valueOf(caseWeight),".49")){
                caseWeight=   (float) (caseWeight - .01);
                caseWeight=Math.round(caseWeight*(float)100)/(float)100;
                System.out.println("Changing price to "+caseWeight+ " so it is not ending in .49");                
            }               
        if(StringUtils.endsWith(String.valueOf(caseWeight),".99") && manualNewWeight==0){
            caseWeight=   (float) (caseWeight - .01);
            caseWeight=Math.round(caseWeight*(float)100)/(float)100;
            System.out.println("Changing price to "+caseWeight+ " so it is not ending in .99");

         }

        /*if(StringUtils.endsWith(String.valueOf(weightOnHB),".99")){
            
            System.out.println("Skipping since weight ends     "+weightOnHB+ "   in .99");
            continue;

         }*/        
        if(approximateCalculation && manualNewWeight==0 && weightFromPreviousShipment==false && sku.indexOf("WG")>=0){
            System.out.println("Wegmans product, take weight from wegmans_product table");
            pst_wegmans.setString(1, sku);
            ResultSet rs1=pst_wegmans.executeQuery();
            if(rs1.next()){
                caseWeight=rs1.getFloat(1);
                weight1=String.valueOf(rs1.getFloat(1));
                weight1=StringUtils.substringBefore(String.valueOf(weight1),".")+".49";
                caseWeight=Float.valueOf(StringUtils.substringBefore(String.valueOf(caseWeight),".")+".49");                
            }
                    
            
        }
        if(manulWeightsEnteredOverrideEverything.containsKey(sku)){
            manualNewWeight=(float)manulWeightsEnteredOverrideEverything.get(sku);
            caseWeight=manualNewWeight;
             weight1=String.valueOf(manualNewWeight);
             System.out.println("Using manually OVERRIDEN updated weight : "+manualNewWeight);

        }         
        if(approximateCalculation && manualNewWeight==0 &&  (weightOnHB >caseWeight  &&weightOnHB<40 ) ){
                 weight1=StringUtils.substringBefore(String.valueOf(weightOnHB),".")+".49";
                 caseWeight=Float.valueOf(StringUtils.substringBefore(String.valueOf(weightOnHB),".")+".49");
            
            System.out.println("Weight is already present in HB, new guessed weight is low. So not using it, but setting to .49 at end so it gets corrected as part of .49 weight review");
          
            
        }
                 if(Float.valueOf(caseWeight)<1.2)
                     caseWeight=(float)1.2;
                   if(weightOnHB==null)
                       weightOnHB=(float)0;
                   if(width==null)
                       width=(float)0;
                   
            //Lets increase all the weight by 10% to cover unexpected problems
           
                //if(originalWeight>0 && String.valueOf(originalWeight).indexOf(".99")<0)
                    //if(originalWeight>0  )
                      //  caseWeight=originalWeight;
            
                if(caseWeight>0 && (Math.abs(weightOnHB-caseWeight)>.1 || width==0))
                {
                    System.out.println("SKU: "+sku+" Case weight :"+caseWeight +" weight from FBA calculator: "+weight1+ " ,pkgQnt:"+pkgQnt+",packs in case:"+packs);
                    updateWeight(id, caseWeight,length,width,height);
                    
                }
                //The whole while loop inside  atry catch so we continue with next one
               }catch (Exception e2){e2.printStackTrace();}
            }
            if(!con.isClosed()) con.close();
    }   
      public static String updateWeight(String productid, Float weight,Float length,Float width,Float height) {
        String webPage = "https://www.harrisburgstore.com/api/v2/products/"+productid+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            //System.out.println(System.getProperty("java.home"));
            //When the c
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
            JSONObject jSONObject = new JSONObject();
                    
            if(width==null|| Float.valueOf(width)==0){
               width=(float)1;
               length=(float)1;
               height=(float)1;
                
            }     
             jSONObject.put("weight", Float.valueOf(weight));
             jSONObject.put("width", Float.valueOf(length));
             jSONObject.put("height", Float.valueOf(width));
             jSONObject.put("depth", Float.valueOf(height));

                   
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }    
}
