/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;

import bombayplaza.pricematch.RetrievePage;
import com.opencsv.CSVReader;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
 

/**
 *
 * @author US083274
 */
public class UPCToItemNames {
    public static void main(String args[]) throws Exception{
        //convertUPCToItemNames();
        formatTheReportFromSkuvault();
    }
    public static void formatTheReportFromSkuvault() throws Exception{
       /* File file = new File("C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\skuvault.csv");
        if(file.exists()==false){
            System.out.println("\n\n\n\n\nSORRY, FILE C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\skuvault.csv DOES NOT EXISTS. Press any key to continue.");
            System.in.read();
            System.exit(1);
        }
        Date today=new Date();
        long diff=((today.getTime()-file.lastModified()))/(1000*60*60);
        
          if(diff>5){
              System.out.println("\n\n\n\n\nLooks like the file C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\skuvault.csv is OLD and modified few days back");
              System.out.println(" Press any key to continue.");
              System.in.read();

          }
              
        */
         String s;
        HashMap<String,String> hm=new HashMap();
        String sku,rowNum;
        if(new File("C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\skuvault.csv").exists()){
             CSVReader reader = new CSVReader(new FileReader("C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\skuvault.csv"));
            String [] nextLine;
            nextLine = reader.readNext();
            int skuIdx=7;
            int locIdx=9;
            int orderIdx=2;
           for(int i=0;i<nextLine.length;i++){
               if(nextLine[i].equals("SKU")){
                   skuIdx=i;
               }
               if(nextLine[i].equals(" LOCATION")){
                   locIdx=i;
               }
               if(nextLine[i].equals(" MARKETPLACE ID")){
                   orderIdx=i;
               }               
           }
            
            while ((nextLine = reader.readNext()) != null &&nextLine.length>1 ) {
               // nextLine[] is an array of values from the line
                     
            
                
            sku=nextLine[skuIdx];
            rowNum=nextLine[locIdx];  
            String orderId=nextLine[orderIdx];  
            if(rowNum==null || rowNum.length()==0||rowNum.indexOf("Not Found")>=0  ||rowNum.indexOf("Out of Stock")>=0)
                continue;
            //System.out.println("Processing line:"+nextLine + "length:"+nextLine+"hm.get(sku).split(t).length:"+hm.get(sku).split("\t").length);
            System.out.println(sku+"==>"+rowNum +"\n"+orderId+"==>"+rowNum);
            if(hm.containsKey(sku) && hm.get(sku).split("\t")[1].equals(rowNum)==false){
                rowNum=hm.get(sku).split("\t")[1] +"/"+ rowNum;
            }
            
              hm.put(sku, sku+"\t"+rowNum);  
              if(rowNum.indexOf("Not Found")<0)
                hm.put(orderId, orderId+"\t"+rowNum);
            }
            
        }
           CSVReader reader = new CSVReader(new FileReader("C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\skuvault_locations.csv"));
            String [] nextLine = reader.readNext();
            int skuIdx=0;
            int locIdx=3;
            int orderIdx=-1;
            int warehouseIdx=2;
           for(int i=0;i<nextLine.length;i++){
               if(nextLine[i].equals("SKU")){
                   skuIdx=i;
               }
               if(nextLine[i].equals("Location")){
                   locIdx=i;
               }
               if(nextLine[i].equals("Warehouse")){
                   warehouseIdx=i;
               }                       
           }            
            while ((nextLine = reader.readNext()) != null &&nextLine.length>1 ) {
               // nextLine[] is an array of values from the line
                sku=nextLine[skuIdx];
                rowNum=nextLine[locIdx];  
                String warehouse=nextLine[warehouseIdx];  
                if(rowNum==null || rowNum.length()==0  )
                    continue;
                if(warehouse!=null && warehouse.indexOf("CARTS")<0)
                    continue;
                //System.out.println("Processing line:"+nextLine + "length:"+nextLine+"hm.get(sku).split(t).length:"+hm.get(sku).split("\t").length);
                System.out.println(sku+"==>"+rowNum +"\n");
                if(hm.containsKey(sku) && hm.get(sku).split("\t")[1].equals(rowNum)==false){
                    rowNum=hm.get(sku).split("\t")[1] +"/"+ rowNum;
                }
                if(hm.containsKey(sku) && hm.get(sku).split("\t")[1].equals(rowNum)){
                    continue;
                }            
                hm.put(sku, sku+"\t"+rowNum);  
            }
     
 
        FileUtils.cleanDirectory(new File("C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\images\\")); 
        System.out.println("o.Sleeping for 2 mins for  all the images to be deleted");
        Thread.sleep(1*60*1000);
        System.out.println("0.Continuing after 2 mins");
        
        FileWriter fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\upc_list_result.txt");
        BufferedWriter out0 = new BufferedWriter(fstream_out0);
        out0.write("SKU\tWarehouseLocation\tUPC");
        out0.newLine();
        
        SortedSet<String> keys = new TreeSet<String>(hm.keySet());
        for (String key : keys) { 
            
        String value = hm.get(key);
        if(value.split("\t").length>1){
            String loc=value.split("\t")[1];
            System.out.println("writing image for :"+key + " loc is :"+loc);
           BufferedImage bi=SKUToWarehouseLocationDAO.writeImage(loc);
            File outputfile = new File("C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\images\\"+key+".png");
         ImageIO.write(bi, "png", outputfile);          
            
        }
        // do something
        out0.write(value);
        out0.newLine();
        
        }

                                 

        out0.close();       
                 
        
    }
    
    public static void convertUPCToItemNames() throws Exception{
        String s;
        BufferedReader  br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\upc_list.txt")));
        StoreUPCVo u=new StoreUPCVo();
        LinkedHashMap <String,StoreUPCVo> map=new LinkedHashMap ();
        String rowNum=null;
        Connection con = RetrievePage.getConnection();
        PreparedStatement ps=con.prepareStatement("select product_upc_ean, code,name from hb_bc.exported_products_from_hb where product_upc_ean=?");
        PreparedStatement psUpcWithoutCheckDigit=con.prepareStatement("select substring(product_upc_ean,1,length(product_upc_ean)-1), code,name from hb_bc.exported_products_from_hb where substring(product_upc_ean,1,length(product_upc_ean)-1)=?");
        
        PreparedStatement psCleanUpc=con.prepareStatement("select product_upc_ean, code,name from hb_bc.exported_products_from_hb where tajplaza.cleanupc(product_upc_ean)=tajplaza.cleanupc(?)");
        ResultSet rs;
        int sno=0;
        FileWriter fstream_out0;
        BufferedWriter out0;
   
        HashMap<String,String> hm=new HashMap();
        while ((s = br0.readLine()) != null)   {
            sno++;
            if(s.trim().length()>0){
                s=s.replaceAll("'", "");
                if(u.getUpc()==null){
                    //u=new StoreUPCVo();
 
                    if(s.length()<6 ){
                        if( u.getRow_num()==null || s.length()<=5){
                            System.out.println("UPC not set, row num not set, current length row less than 5, it should be a row num "+s);
                            u.setRow_num(s);
                            rowNum=s;
                            continue;
                        } 
                    }
                     if(s.length()==14)    {
                         String elevendigitUPC=StringUtils.substring(s, 2,s.length()-1);
                        psUpcWithoutCheckDigit.setString(1, elevendigitUPC);
                       rs= psUpcWithoutCheckDigit.executeQuery();
                         
                     }else{
                        ps.setString(1, s);
                       rs= ps.executeQuery();
                         
                     }
                       String sku="",name="";
                       boolean itemFound=false;
                       while(rs.next()){
                           itemFound=true;
                           sku=rs.getString("code");
                           name=rs.getString("name");
                           System.out.println("UPC:"+s+"Item Name:"+name);
                           hm.put(sku, sku+"\t"+rowNum+"\t'"+s);                           
                           
                       }
                       if(itemFound==false)
                       {
                           psCleanUpc.setString(1, s);
                           rs= psCleanUpc.executeQuery();
                           while(rs.next()){
                               sku=rs.getString("code");
                               name=rs.getString("name");       
                              System.out.println("UPC:"+s+"Item Name:"+name);
                             hm.put(sku, sku+"\t"+rowNum+"\t'"+s);   
                             
                               
                           }
                       }
 
            }
          }
                 
        }//End of while loop to process  all rows
        br0.close();
        FileUtils.cleanDirectory(new File("C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\images\\")); 
        System.out.println("Sleeping for 2 mins for  all the images to be deleted");
        Thread.sleep(2*60*1000);
        System.out.println("Continuing after 2 mins");
        fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\upc_list_result.txt");
        out0 = new BufferedWriter(fstream_out0);
        //out0.write("SKU\tWarehouseLocation\tUPC");
        out0.newLine();
        
        SortedSet<String> keys = new TreeSet<String>(hm.keySet());
        for (String key : keys) { 
        String value = hm.get(key);
        if(value.split("\t").length>2){
            String loc=value.split("\t")[1];
           BufferedImage bi=SKUToWarehouseLocationDAO.writeImage(loc);
            File outputfile = new File("C:\\Google Drive\\Dropbox\\harrisburgstore\\pickup\\images\\"+key+".png");
         ImageIO.write(bi, "png", outputfile);          
            
        }
        // do something
           out0.write(value);
          out0.newLine();
        
        }

                                 

        out0.close();       
         
       
        con.close();

    }
 
    
}
