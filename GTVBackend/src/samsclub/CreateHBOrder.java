/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;

import bigcommerce.FetchCategories;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.*;
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.gargoylesoftware.htmlunit.xml.XmlPage;
import com.tajplaza.products.ListMatchingProductsSample;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.LogFactory;
/**
 *
 * @author prabu
 */
public class CreateHBOrder {
    public static String DEBUG_ASIN="";
     static  Connection con=null;
    private static List<DomElement> elems;
    private static  PreparedStatement psInsert;
     public static void main(String args[]) throws Exception{
        NEED_UPC_FROM_SC=true;
        String upc=getUPCForAsin("B01KSCRQ7E");         
        String PRODUCTION=System.getProperty("PRODUCTION","false");
            if(PRODUCTION.equals("true"))
                DEBUG_ASIN="";
         
       // String sql=" select   seller_sku,asin1 from tajplaza.existing_all_inventory  where  (seller_sku like '%AMA%' )  and (length(quantity)>0 and cast(quantity as decimal)>4)  and seller_sku not in (select code from hb_bc.exported_products_from_hb)   union select   sku,asin from tajplaza.existing_inventory_fba where afn_fulfillable_quantity>5  and sku not in (select code from hb_bc.exported_products_from_hb)   ";
             String sql="  select distinct t.upc,hb_skus, ceil(((case when upc_shipped_last_14days is null or upc_shipped_last_14days/2<12 then 12 else upc_shipped_last_14days/2 end) -(case when shelf_quantity is not null then  shelf_quantity else  0 end))/6)*6  upc_needed from to_order t where wholesalername='BJ' and to_order='Yes' order by (case when upc_shipped_last_14days is null or upc_shipped_last_14days<18 then 18 else upc_shipped_last_14days end) -(case when shelf_quantity is not null then  shelf_quantity else  0 end) desc ";
        System.out.println("Executing sql to update missing categories for unfi products : "+sql);
        con=RetrievePage.getConnection();
        String productsToDeletesql=" select  product_id,code from hb_bc.exported_products_from_hb where   str_to_date(Date_Modified,'%m/%d/%Y') < (curdate() - interval 90 day)   and  str_to_date(Date_Added,'%m/%d/%Y') < (curdate() - interval 90 day)  and cast(Stock_Level as decimal)=0 and code not in (select sku  from hb_bc.ordered_products op, hb_bc.orders o where o.id=op.order_id and str_to_date(o.date_created,'%a, %d %b %Y %H:%i:%S %f') > (curdate() - interval 90 day) ) ";

        
        PreparedStatement ps=con.prepareStatement(sql);
  
        ps=con.prepareStatement(sql);
        ResultSet rs=ps.executeQuery();
        String product_id,code;
        JSONObject  billing_address=new JSONObject();
        while(rs.next()){
           
            product_id=rs.getString("product_id");
            code=rs.getString("code");
             System.out.println("Deleting sku:"+code);
           // deleteProduct(product_id,code);
        }
        
     }
    public static void deleteProductNotUsed(String product_id,String code) throws Exception{
        String webPage = "https://www.harrisburgstore.com/api/v2/products/"+product_id ;
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("DELETE");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
            JSONObject jSONObject = new JSONObject();
                   
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
          
        } catch(Exception exception) {
            exception.printStackTrace();
            
        }
    } 
    public static boolean DELETE_IF_EXISTS=false;
     public static void createHBProductFromAmazon(String sku, String asin,HashMap hm) throws Exception{
         if(con==null)
                 con=RetrievePage.getConnection();
         RetrievePage.JAVASCRIPT=false;
         if(hm==null || hm.containsKey("title")==false || hm.containsKey("manu")==false){
             //hm=MatchThePrice.getProductDetails(asin, true, false, false);
            ArrayList<HashMap> matchingProducts = null;
            matchingProducts=ListMatchingProductsSample.getMatchingProducts(asin);
            for(int i=0;i<matchingProducts.size();i++){
            HashMap tmpMap=matchingProducts.get(i);
            if(tmpMap.get("asin").equals(asin)){
                System.out.println("Matching asin found throgh MWS API");
                hm=tmpMap;
                //Reason for parsing through here is to see if there is an exisitng multi pack quantity exists.
                }
             }
         }
         if(hm==null)
             return;
             
         String title=(String)(hm.get("title")!=null?hm.get("title"):"NA");
         String manu=(String)(hm.get("manu")!=null?hm.get("manu"):"");
         if(manu.length()==0)
             manu=(String)(hm.get("Brand")!=null?hm.get("Brand"):"");
         String des=(String)(hm.get("des")!=null?hm.get("des"):"");
         String bb1=(String)(hm.get("bb1")!=null?hm.get("bb1"):"");
         String bb2=(String)(hm.get("bb2")!=null?hm.get("bb2"):"");
         String bb3=(String)(hm.get("bb3")!=null?hm.get("bb3"):"");
         String bb4=(String)(hm.get("bb4")!=null?hm.get("bb4"):"");
         String bb5=(String)(hm.get("bb5")!=null?hm.get("bb5"):"");
         des=des+"<ul>";
         if(bb1.length()>0)
             des=des+"<li>"+bb1+"</li>";
         if(bb2.length()>0)
             des=des+"<li>"+bb2+"</li>";
         if(bb3.length()>0)
             des=des+"<li>"+bb3+"</li>";
         if(bb4.length()>0)
             des=des+"<li>"+bb4+"</li>";
         if(bb5.length()>0)
             des=des+"<li>"+bb5+"</li>";
            des=des+"</ul>";
         String imageurl=(String)(hm.get("imageurl")!=null?hm.get("imageurl"):"NA");
        String category= (String)(hm.get("category")!=null?hm.get("category"):"Misc") ;
         category=category.replaceAll(">","/");
         category=category.replaceAll("›","/");
         category=category.replaceAll("\r", "").replaceAll("\n", "");        
        title= title.replaceAll("\r", "").replaceAll("\n", "");        
        if(title.length()>100)
            title=title.substring(0, 99);
        
         HashMap fbaMap=RetrievePage.getDetailsFromFbaCalculatorNewMWS(null,asin, "100",true);
         String weight=String.valueOf(fbaMap.get("productInfoWeight")  );
        
        int cat_id=HBUpdateCategories.createWholeCategoryPath(HBUpdateCategories.CAT_ID_MISC_CATALOG, category);
        int brand_id=createBrand( manu);
        NEED_UPC_FROM_SC=true;
        String upc=getUPCForAsin(asin);
        
        if(DELETE_IF_EXISTS)
            deleteIfExists(title);
        try{
            psInsert.setString(1, sku);
            psInsert.executeUpdate();
            createIndividualProduct(sku,title,des,imageurl,cat_id,weight, brand_id,upc);
        }   catch (Exception e)     {
            System.out.println("Failed first time,Lets try one more time with a tweaked title");
            try{
                createIndividualProduct(sku,manu+":"+title,des,imageurl,cat_id,weight, brand_id,upc);
            }catch (Exception e1){
                //e1.printStackTrace();
            System.out.println("Exception while creating product in HB, trying with desc=title"+e.getMessage());
             try{
                 createIndividualProduct(sku,manu+":"+title,title,imageurl,cat_id,weight, brand_id,upc);
             }catch (Exception e2){e2.printStackTrace();}
            }
        }
        
         
     }
     public static void deleteIfExists(String title)  {
         try{
             PreparedStatement ps=con.prepareStatement("select product_id,code,name from hb_bc.exported_products_from_hb where name=?");
             ps.setString(1, title);
             ResultSet rs=ps.executeQuery();
             if(rs.next()){
                 String product_id=rs.getString("product_id");
                 String code=rs.getString("code");
                  //deleteProduct(product_id,code);
             }
             
         }catch (Exception e){e.printStackTrace();}
         
         
     }
     public static int createIndividualProduct(String sku,String title,String des,String imageurl,int cat_id,String weight, Integer brand_id,String upc ) throws Exception{
        
        String webPage = "https://www.harrisburgstore.com/api/v2/products";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        Integer product_id=-1;
  
            String str;
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("POST");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
            JSONObject jSONObject = new JSONObject();
             jSONObject.put("name",title);
             jSONObject.put("is_visible",true);
             jSONObject.put("inventory_level","0");
             
             jSONObject.put("price","999");
             jSONObject.put("sku",sku);
             jSONObject.put("availability","available");
             jSONObject.put("inventory_tracking","simple");
             
             jSONObject.put("weight",weight);
             jSONObject.put("type","physical");
             //Remove non ascii Characters
            StringBuilder result = new StringBuilder();
            for(char val : des.toCharArray()) {
                if(val < 192) result.append(val);
            }             
             des=result.toString();
             jSONObject.put("description",des);
             jSONObject.put("brand_id",brand_id);
             jSONObject.put("upc",upc);
             

             JSONArray newCats=new JSONArray(); 
             newCats.add(cat_id);
             jSONObject.put("categories",newCats); 
                   
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
             out.flush();
            out.close(); 
           
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            product_id=getProductId(sku);
            System.out.println("Response : " + new String(sb1));
            if(NEW_PRODUCT  && imageurl!=null && imageurl.length()>0){
               uploadImage(product_id,imageurl,true);
            }            
           
    
         return  -1;         
         
     }
public static Integer createBrand(String brand) throws Exception {
 
        String webPage = "https://www.harrisburgstore.com/api/v2/brands";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        
        try {
            String str;
      
            /* webPage = "https://www.harrisburgstore.com/api/v2/brands.json?name="+URLEncoder.encode(brand);
            System.out.println("Querying :"+webPage);
               str=HBUpdateCategories.getJSONString(webPage);
               if( JSONArray.fromObject(str).size()>0){
                    Integer id=JSONArray.fromObject(str).getJSONObject(0).getInt("id")  ;                 

                   return id;

               }*/ 

            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("POST");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
            JSONObject jSONObject = new JSONObject();
 
            
            jSONObject.put("name",brand);
            
                   
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
             out.flush();
            out.close(); 
           
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Line : "+line);
            //Thread.sleep(2000);
             webPage = "https://www.harrisburgstore.com/api/v2/brands.json?name="+URLEncoder.encode(brand);
             System.out.println("Quering:"+webPage);
                 str=HBUpdateCategories.getJSONString(webPage);
               if(str.length()>0 && JSONArray.fromObject(str).size()>0){
                    Integer id=JSONArray.fromObject(str).getJSONObject(0).getInt("id")  ;                 

                   return id;

               }
            
            System.out.println("Response : " + new String(sb1));
            
           
        } catch(Exception exception) {
            
             webPage = "https://www.harrisburgstore.com/api/v2/brands.json?name="+URLEncoder.encode(brand);
             System.out.println("Quering:"+webPage);
                 String str=HBUpdateCategories.getJSONString(webPage);
                   if( str.length()>0 && JSONArray.fromObject(str).size()>0){
                        Integer id=JSONArray.fromObject(str).getJSONObject(0).getInt("id")  ;                 

                       return id;

                   }
           
        }
         return  -1;
        
    }     
    public static  boolean NEW_PRODUCT=true;
     private static Integer getProductId(String sku) throws Exception{
             String webPage = "https://www.harrisburgstore.com/api/v2/products.json?sku="+sku;
             int product_id=-1;
             System.out.println("Quering:"+webPage);
              String   str=HBUpdateCategories.getJSONString(webPage);
               if( JSONArray.fromObject(str).size()>0){
                    product_id=JSONArray.fromObject(str).getJSONObject(0).getInt("id")  ;                 
                    if(JSONArray.fromObject(str).getJSONObject(0).containsKey("images")){
                       // NEW_PRODUCT=true;
                    }
               } 
            return product_id  ;
     }

    public static String uploadImage(Integer product_id,String imageurl,boolean deleteOldImages) {
        String image_id=null;
        String webPage = "https://www.harrisburgstore.com/api/v2/products/"+product_id+"/images";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        
        try {
              
     
            String str;
            URL uri = new URL(webPage);
            
              String authString = username + ":" + token;            
              FetchCategories.REQUEST_TYPE="GET";
              String result=FetchCategories.getResultJSONString(authString, webPage);
              if(deleteOldImages){
                 
                 if(result!=null ){
                     JSONArray arr=JSONArray.fromObject(result);
                     for(int i=0;i<arr.size();i++){
                         JSONObject js=arr.getJSONObject(i);
                       image_id=js.getString("id");
                       FetchCategories.REQUEST_TYPE="DELETE";
                       result=FetchCategories.getResultJSONString(authString, "https://www.harrisburgstore.com/api/v2/products/"+product_id+"/images/"+image_id);
                       System.out.println("Deleted image result:"+result);

                     }
                 }
                  
              }else{
                  //delete if same image dimension exists
              }
              
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("GET");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
            JSONObject jSONObject = new JSONObject();
             jSONObject.put("image_file",imageurl);
                   
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
             out.flush();
            out.close(); 
           
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            
            System.out.println("Response : " + new String(sb1));
            
           
        } catch(Exception exception) {
            
              exception.printStackTrace();
        }
          
        return image_id;
    }
    public static boolean NEED_UPC_FROM_SC=false;
    public static WebClient webClient1 =null;;
    public static String getUPCForAsin(String asin)throws Exception{
    String sql="select upc  from tajplaza.possible_asins_for_shelf_items   where possible_asin=? union (select upc from tajplaza.amazon_catalog where asin=? order by ts desc) union select v.upc from tajplaza.variation_sku_upcs v, tajplaza.existing_all_inventory e where v.sku=e.seller_sku and e.asin1=?  and length(upc)>5";
         
        Connection con=RetrievePage.getConnection();
        PreparedStatement ps=con.prepareStatement(sql);
        ps.setString(1, asin);
        ps.setString(2, asin);
        ps.setString(3, asin);
        ResultSet rs=ps.executeQuery();
        String upc="";
        if(  rs.next()){
             upc=rs.getString("upc");
        }  else if(NEED_UPC_FROM_SC){
                  HtmlPage page;
                if(webClient1==null){
                    webClient1=new WebClient(BrowserVersion.FIREFOX_38 );
                    System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "error");
                    LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
                    java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF); 
                    java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);
                    webClient1.getOptions().setThrowExceptionOnScriptError(false);
                    webClient1.getOptions().setJavaScriptEnabled(false);
                    webClient1.getOptions().setCssEnabled(false);
                     webClient1.waitForBackgroundJavaScript(15000);

                  page = webClient1.getPage("https://sellercentral.amazon.com/gp/homepage.html");
                    //System.out.println(page.asText());
                    if(page.getElementById("username")!=null)
                        ((HtmlTextInput) page.getElementById("username")).setText("packfba@gmail.com");
                    else if(  ((HtmlTextInput) page.getElementById("ap_email")).getValueAttribute()==null  ||((HtmlTextInput) page.getElementById("ap_email")).getValueAttribute().equals("packfba@gmail.com") ==false)
                        ((HtmlTextInput) page.getElementById("ap_email")).setValueAttribute("packfba@gmail.com");
                    if(page.getElementById("password")!=null)
                        ((HtmlPasswordInput) page.getElementById("password")).setText("dummy123");
                    else
                        ((HtmlPasswordInput) page.getElementById("ap_password")).setValueAttribute("dummy123");
                     System.out.println("Signing In");
                     if(page.getElementById("sign-in-button")!=null)
                        page=(HtmlPage) ((HtmlButton) page.getElementById("sign-in-button")).click();
                     else
                         page=(HtmlPage) ((HtmlImageInput) page.getElementById("signInSubmit")).click();            

                }

                 String url="https://sellercentral.amazon.com/productsearch?q=<ASIN>&ref_=xx_prodsrch_cont_prodsrch";
                 url=url.replace("<ASIN>", asin);
                 page=webClient1.getPage(url);
                 elems=  (List<DomElement>) page.getByXPath("//span");
                for(int i=0;i<elems.size();i++){
                    DomElement elem=elems.get(i);
                    if(elem.asText().indexOf("UPC:")>=0){
                         upc=elem.asText();
                        upc=upc.replaceAll("UPC:","").trim();
                        
                    }
                }
                 
        }
        con.close();
        return upc;
    }
    
}
