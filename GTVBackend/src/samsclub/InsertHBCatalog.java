/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;
 
 
import amazon.product.CreateCatalogNew;
import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
 
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;
 
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.net.ssl.HttpsURLConnection;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

 

/**
 *
 * @author us083274
 */
public class InsertHBCatalog {
 
 
   // public static String HB_INPUT_CATALOG_TABLE="hb_bc.hb_input_catalog";
//    public static String  HB_CATALOG_TABLE="hb_bc.hb_catalog";
   public static String AMAZON_TABLE_NAME="amazon_catalog_hb";
   
    public static String WHOLESALERNAMENEW=null;
    public static String TOP_CATEGORY_NAME=null;
    public static void main(String args[]) throws Exception{
 
        //Costco, BJs Sams --Keep top top category as Wholesale so all are under one tree
         TOP_CATEGORY_NAME = System.getProperty("TOP_CATEGORY_NAME", "Wholesale");
         TOP_CATEGORY_NAME=TOP_CATEGORY_NAME.replace("\"", "");
         
        if(System.getProperty("SLEEP_TIME_ALL_PROXIES_USED_ONCE")!=null)
            RetrievePage.SLEEP_TIME_ALL_PROXIES_USED_ONCE =Integer.parseInt(System.getProperty("SLEEP_TIME_ALL_PROXIES_USED_ONCE"));
        if(System.getProperty("NUMBER_TIMES_PROXIES_BEFORE_SLEEP")!=null)
            RetrievePage.NUMBER_TIMES_PROXIES_BEFORE_SLEEP =Integer.parseInt(System.getProperty("NUMBER_TIMES_PROXIES_BEFORE_SLEEP"));        
        System.out.println("Value of NUMBER_TIMES_PROXIES_BEFORE_SLEEP is "+  RetrievePage.NUMBER_TIMES_PROXIES_BEFORE_SLEEP);
        
     
         updateItemCodesRemoveLastDigitIfStartsWithZeros();
        
        loadHBProductsToTable();
        //loadCustomFields(id,null);
         
       // loadUNFIProductsToTable() ;
    }
 
     public static void updateItemCodesRemoveLastDigitIfStartsWithZeros() throws Exception{
        String sql="select store_item_code from hb_bc.hb_input_catalog where store_item_code like '%0000%'";
        String sql2="update hb_bc.hb_input_catalog set store_item_code=? where store_item_code=?";
        Connection con=RetrievePage.getConnection();
        PreparedStatement pst=con.prepareStatement(sql );
        System.out.println("Executing sql1"+sql);
        ResultSet rs=pst.executeQuery();
        while(rs.next()){
            String old_item_code=rs.getString(1);
            String new_item_code=old_item_code.replaceAll("\"", "").replaceAll("'", "").replaceAll("'", "");
            if(old_item_code.indexOf("00000")>=0){
                
                new_item_code=StringUtils.stripStart(new_item_code, "0");
                new_item_code=new_item_code.substring(0,new_item_code.length()-1);
                
            }
                    
            System.out.println("old store_item_code:"+old_item_code+",store_item_code:"+new_item_code);
            
            PreparedStatement pst2=con.prepareStatement(sql2);
            pst2.setString(1, new_item_code);
            pst2.setString(2, old_item_code);
            pst2.executeUpdate();
        }
        
    }
            
        public static void loadHBProductsToTable() throws Exception {
            RetrievePage.USE_PROXIES=true;
             Connection con = RetrievePage.getConnection();
    
             String sql;
             PreparedStatement selectStmt = con.prepareStatement("select  distinct  row_num,upc,store_price,store_item_code from hb_bc.hb_input_catalog c where  tajplaza.cleanupc(upc) not in (select tajplaza.cleanupc(upc) from  hb_bc.hb_catalog  c1 where upc is not null  )    ");
               PreparedStatement selectStmtExistsInHb = con.prepareStatement("select   * from hb_bc.exported_products_from_hb where code=? ");
             //sql="  update hb_bc.hb_catalog  h set price=(select max(price) from hb_bc.hb_input_catalog hi where  tajplaza.cleanupc(h.upc)= tajplaza.cleanupc(hi.upc) ) where wholesalername= '"+WHOLESALERNAME+"'";
             //System.out.println("Executing sql 1:"+sql);
             //PreparedStatement updatePrice_OnHB_Catalog=con.prepareStatement(sql);
             //updatePrice_OnHB_Catalog.executeUpdate();
             sql=" delete from  hb_bc.hb_input_catalog   where upc in (select upc from hb_bc.hb_catalog h , hb_bc.exported_products_from_hb p where h.item_code=p.code and  cast(stock_level as decimal)>0   and cast(h.price as decimal)<cast(store_price  as decimal)) ";
             System.out.println("Executing sql 2:"+sql);
             PreparedStatement delete_ExistingProducts=con.prepareStatement(sql);
             delete_ExistingProducts.executeUpdate();
            
           
             /*sql="delete from hb_bc.hb_catalog where ts< (curdate() - interval 100  day) and upc in (select upc from hb_bc.hb_input_catalog where wholesalername='"+WHOLESALERNAME+"')";
             System.out.println("Executing sql 3:"+sql);
              delete_ExistingProducts=con.prepareStatement(sql);
             delete_ExistingProducts.executeUpdate();             
              */ 
 
             
             System.out.println("Executing sql:"+selectStmt);
              
            ResultSet rs=selectStmt.executeQuery();
            ResultSet rs2;
            int itemCount=0;
             
            int count=0;
           
             int item_count=0;
             ArrayList currentProcessedItemCodes=new ArrayList();
            while(rs.next()){
                  String upc=rs.getString("upc").replaceAll("\"", "").replaceAll("\'", "");
                   String oc=rs.getString("store_price").replaceAll("\"", "").replaceAll("\'", "");
                  String row_num=rs.getString("row_num").replaceAll("\"", "").replaceAll("\'", "");
                  String store_item_code=rs.getString("store_item_code");
                  if(store_item_code!=null)
                      store_item_code=store_item_code.replaceAll("\"", "").replaceAll("\'", "");
                  if(store_item_code!=null && store_item_code.length() >0 && currentProcessedItemCodes.contains(store_item_code))
                      continue;
                  currentProcessedItemCodes.add(store_item_code);
                               
                  String seq_no=String.format("%04d", item_count++);
                upc=upc.replaceAll("UPC:","");
                upc=upc.replaceAll("upc:","");
                upc=upc.replaceAll("-","");
                upc=upc.replaceAll("'","");
                upc=upc.replaceAll(" ","");
                upc=upc.trim();

                if(upc.length()>13)
                             upc=upc.substring(1,14);
                if(upc.length()<9)
                    continue;
                if(upc.length()<12)
                    upc=StringUtils.leftPad(upc,12, '0');
                String item_code="";
                if(upc.equals("025500801179"))
                    System.out.println("Debug");
                if(store_item_code!=null &&  store_item_code.trim().length()>0)
                    WHOLESALERNAMENEW="CO";
                else
                    WHOLESALERNAMENEW="BJ";
                if(store_item_code==null || store_item_code.trim().length()==0)
                  item_code=WHOLESALERNAMENEW+"_"+row_num+"_"+upc+"_"+oc;
                else
                    item_code=WHOLESALERNAMENEW+"_"+row_num+"_"+store_item_code+"_"+oc;
                System.out.println("Processing rowLabel:"+item_code +",upc:"+upc);
                selectStmtExistsInHb.setString(1, item_code);
                ResultSet rsTmp=selectStmtExistsInHb.executeQuery();
                if(rsTmp.next()){
                    System.out.println("Same SKU :"+item_code +" already exists in HB. BUT NOT skipping it");
               //     continue;
                }
                MatchThePrice.JAVASCRIPTNONO=false;
                MatchThePrice.DOWNLOAD_IMAGE=false;
                System.out.println("Processing UPC:"+upc);
                String title="NA";
                String manu="NA";
                String asin="NA";
                 float bestprice=(float)0;
                 float secondbestprice=(float)0;
                 //ArrayList<String> items=CreateCatalog.getJustUPCList(upc);
                 CreateCatalogNew.LOW_PRICED_ASIN1=new Product();
                 Product p=CreateCatalogNew.getUPCDetailsOld(upc);
                // p=CreateCatalogNew.LOW_PRICED_ASIN1;
                 
                if(p.getAsin().equals("NA")==false && p.getAsin().indexOf("B")>=0){
                    asin= p.getAsin();
                     CreateCatalog.ERROR_MOVE_TO_NEXT_PAGE=true;
                    HashMap hm=MatchThePrice.getProductDetails(asin, false,false,false);
                    if(hm==null){
                        System.out.println("hm is null, continuing");
                        continue;
                    }
                     //pw.storeProduct1(p, con, "amazon_catalog_un_hb");
                     String aplusProductDescription=(String)(hm.get("aplusProductDescription")!=null?hm.get("aplusProductDescription"):null);
                    title=(String)(hm.get("title")!=null?hm.get("title"):"");
                    title=title.replaceAll("Amazon.com", "");
                    title=StringUtils.removeStart(title.trim(), ":");
                    manu=(String)(hm.get("manu")!=null?hm.get("manu"):null);
                    String des=(String)(hm.get("des")!=null?hm.get("des"):null);
                    String bb1=(String)(hm.get("bb1")!=null?hm.get("bb1"):null);
                    String bb2=(String)(hm.get("bb2")!=null?hm.get("bb2"):null);
                    String bb3=(String)(hm.get("bb3")!=null?hm.get("bb3"):null);
                    String bb4=(String)(hm.get("bb4")!=null?hm.get("bb4"):null);
                    String category=(String)(hm.get("category")!=null?hm.get("category"):null);
                     bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
                      secondbestprice=(Float)(hm.get("secondbestprice")!=null?hm.get("secondbestprice"):(float)0);
                     Float suggested_price=bestprice*(float).85;
                    try{
                     
                     Float suggestedPriceFromOC=Float.valueOf(oc)*(float)1.2+(float)2;
                     if(suggested_price<suggestedPriceFromOC || bestprice==0)
                         suggested_price=suggestedPriceFromOC;
                 }catch(Exception e3){}
                    String imageurl=(String)(hm.get("imageurl")!=null?hm.get("imageurl"):null);
                    
                    String rankS=(String)(hm.get("rank")!=null?hm.get("rank"):"99999");
                    int rank=99999;
                    try{
                        rank=Integer.parseInt(rankS);
                    }catch (Exception e2){}
                    String descr=des+"<ul>";
                    if(bb1!=null && bb1.toUpperCase().equals("NULL") ==false)
                        descr+="<li>"+bb1+"</li>";
                    if(bb2!=null&& bb1.toUpperCase().equals("NULL") ==false)
                        descr+="<li>"+bb2+"</li>";
                    if(bb3!=null&& bb1.toUpperCase().equals("NULL") ==false)
                        descr+="<li>"+bb3+"</li>";
                    if(bb4!=null&& bb1.toUpperCase().equals("NULL") ==false)
                        descr+="<li>"+bb4+"</li>";
                    
                    descr+="</ul>";
                    if(aplusProductDescription!=null){
                         descr+=aplusProductDescription; 
                    }                    
                    
                    descr=StringUtils.substring(descr, 0,9999);
                    manu=StringUtils.substring(manu, 0,99);
                    
                    PreparedStatement insertStmt_Found=con.prepareStatement(" insert into  hb_bc.hb_catalog (item_code,  asin,upc, title, descr, brand, price,image_file,rank,cat1,suggested_price,bestprice,secondbestprice,wholesalername, top_category_name) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
                    
                    insertStmt_Found.setString(1, item_code);
                    insertStmt_Found.setString(2, asin);
                    insertStmt_Found.setString(3, upc);
                    insertStmt_Found.setString(4, title);
                    insertStmt_Found.setString(5, descr);
                    insertStmt_Found.setString(6, manu);
                    insertStmt_Found.setString(7, oc);
                    insertStmt_Found.setString(8, imageurl);
                    insertStmt_Found.setInt(9, rank);
                    insertStmt_Found.setString(10, category);
                    insertStmt_Found.setFloat(11, suggested_price);
                    insertStmt_Found.setFloat(12, bestprice);
                    insertStmt_Found.setFloat(13, secondbestprice);
                    insertStmt_Found.setString(14, WHOLESALERNAMENEW);
                    insertStmt_Found.setString(15, TOP_CATEGORY_NAME);
                    
                    insertStmt_Found.executeUpdate();


                   
                }else{
                    PreparedStatement insertStmt_NF=con.prepareStatement(" insert into  hb_bc.hb_catalog  (item_code,  upc,asin,wholesalername) values (?,?,'NA',?) ");
                    
                   insertStmt_NF.setString(1, item_code); 
                   insertStmt_NF.setString(2, upc);
                   insertStmt_NF.setString(3, WHOLESALERNAMENEW);
                   insertStmt_NF.executeUpdate();
                }
                     p=new Product();
                    p.setItem_Code(item_code);
                    p.setTitle(title);
     
                    p.setBrand(manu);                    
                    p.setAsin(asin);
                    p.setUpc(upc);
                    p.setBestprice(bestprice);
                    p.setSecondbestprice(secondbestprice);
                   p.setWHOLESALERNAME(WHOLESALERNAMENEW);
                    ProcessWorkingCatalog pw=new ProcessWorkingCatalog();
                    pw.storeProduct3(p, AMAZON_TABLE_NAME);
                 
            }
            
           con.close();        
    }
   public static String updateProduct1(String productid, JSONObject jSONObject) {
        String webPage = "https://harrisburgstore.com/api/v2/products/"+productid+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
             
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }            
   
}
