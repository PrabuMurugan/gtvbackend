/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;

 
 
import amazon.product.GetLastDigitUPC;
import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.AjaxController;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.TextPage;
import com.gargoylesoftware.htmlunit.WebClient;
 
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlParagraph;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
 
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
 

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.ProxyConfig;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebClientOptions;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlHiddenInput;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlListItem;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlParagraph;
import com.gargoylesoftware.htmlunit.html.HtmlSpan;
import com.gargoylesoftware.htmlunit.util.Cookie;
 

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tajplaza.upload.CreateFeedFile;
import com.tajplaza.upload.UploadInventoryLoaderFeedFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
 

/**
 *
 * @author prabu
 */
public class CollecTAllProductsFromSAms {
    public static ArrayList catList=new ArrayList();
    public static ArrayList catListWithInClub=new ArrayList();
    public static ArrayList visitedCatsForCategories=new ArrayList();
    public static ArrayList productURls=new ArrayList();
    public static boolean CLUB_SELECTED=false;
    
    public static String[] prxies;
    public static int iCounter2;
    public static int  catCount;
    static   int iTotalProductFetched=1;
    static int iTotalProductInserted=0;
    static String RUN_MISSING_PRODUCTS_ONLY=System.getProperty("RUN_MISSING_PRODUCTS_ONLY","false");    
    static String RUN_FOR_MISSING_AMAZON=System.getProperty("RUN_FOR_MISSING_AMAZON","false");    
    static String VISIT_ONLY_AMAZON_PRODUCTS=System.getProperty("VISIT_ONLY_AMAZON_PRODUCTS","false");    
    public static String DEBUG_PRODUCT_URL=System.getProperty("DEBUG_PRODUCT_URL",""); 
   
    public static String SAMS_PRODUCTS_TABLE="hb_bc.sams_products";
     private static String strCategory = null;
     public static WebClient webClient =null;
 static {
           try {
               // DEBUG_PRODUCT_URL="https://www.samsclub.com/sams/old-spice-male-deo-5pk-3-0-oz/prod15340383.ip";
               //BufferedReader  br001 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\proxylist.txt")));
               BufferedReader  br001 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\proxylist.txt")));
                String strLine;
                ArrayList <String>list = new ArrayList<String>();
                while (false && (strLine= br001.readLine()) != null )   {
                    if(strLine.trim().indexOf("#")==0)
                        continue;
                    list.add(strLine);
                }
                System.out.println("[ RetrievePage ] Number of proxies in file : "+ list.size());
                prxies = new String[list.size()];
                list.toArray(prxies);
                br001.close();
                System.out.println("[ RetrievePage ] Number of proxies in ary : "+ prxies.length);
           } catch(Exception  e) {
               e.printStackTrace();
               System.exit(1);
           }
          
          
    }     
  
 public static void main(String args[]) throws Exception{
 
        HtmlPage page=null;  
        String strIP=null;
        String strPort=null;
        String proxyUsername=null;
        String proxypassword=null;         
        
        if(VISIT_ONLY_AMAZON_PRODUCTS.equals("true")==false && prxies.length>0){
             if(iCounter2 == (prxies.length-1)) {
                 iCounter2=0;
             }
             strIP= prxies[iCounter2].split(":")[0];
            strPort= prxies[iCounter2].split(":")[1];

            if(prxies[iCounter2].split(":").length>2){
                proxyUsername = prxies[iCounter2].split(":")[2];
                proxypassword = prxies[iCounter2].split(":")[3];
            }     
            iCounter2++;
        }        
       
         if(proxyUsername!=null){
             webClient =new WebClient(BrowserVersion.FIREFOX_38,strIP,Integer.parseInt(strPort));
             //System.out.println("Setting proxy user name:"+proxyUsername+", password:"+proxypassword);
          DefaultCredentialsProvider credentialsProvider1 = (DefaultCredentialsProvider) webClient.getCredentialsProvider();
            credentialsProvider1.addCredentials(  proxyUsername,   proxypassword);   
             
         }         else{
              webClient =new WebClient(BrowserVersion.FIREFOX_38 );
         }
         WebClientOptions wo=webClient.getOptions();
         
        wo.setThrowExceptionOnScriptError(false);
        wo.setCssEnabled(false);
        webClient.getCookieManager().setCookiesEnabled(true);
        wo.setRedirectEnabled(true);
        wo.setThrowExceptionOnFailingStatusCode(true);
        wo.setThrowExceptionOnScriptError(false);
        wo.setPrintContentOnFailingStatusCode(false);
         //webClient.waitForBackgroundJavaScript(10000);
         //webClient.waitForBackgroundJavaScriptStartingBefore(10000);
        wo.setCssEnabled(false);
        if(VISIT_ONLY_AMAZON_PRODUCTS.equals("true"))
            wo.setJavaScriptEnabled(false);
        else
           wo.setJavaScriptEnabled(true);
        wo.setRedirectEnabled(true);   
         webClient.setAjaxController(new AjaxController(){
                @Override
                public boolean processSynchron(HtmlPage page, WebRequest request, boolean async)
                {
                    return true;
                }
            });        
         Connection con=RetrievePage.getConnection();

        // webClient.getCache().setMaxSize(0);
        /* TextPage p0=webClient.getPage("https://www.samsclub.com/sams/common/tp_clubselection.jsp?selectClub=4994");
        List<NameValuePair> responseHeaders = p0.getWebResponse().getResponseHeaders();
        int i=0;
        for(  i=0;i<responseHeaders.size();i++){
         if(responseHeaders.get(i).getName().equals("Location"))
             break;
        }

        String location=p0.getWebResponse().getResponseHeaders().get(i).getValue();
       p0=webClient.getPage(location);
        for(  i=0;i<responseHeaders.size();i++){
            if(responseHeaders.get(i).getName().equals("Location"))
                break;
        }
         webClient.getOptions().setJavaScriptEnabled(false);
        location=p0.getWebResponse().getResponseHeaders().get(i).getValue();
       page=webClient.getPage(location);       
 
      */
        webClient.getCookieManager().setCookiesEnabled(true);
        setCookies(webClient);          
        String url="https://www.samsclub.com/sams/search/searchResults.jsp?searchTerm=ITEMCODE&searchCategoryId=all&selectedFilter=club";
        url=url.replaceAll("ITEMCODE", "100744");
    //     visitProduct(con,url,url);
        
        if(VISIT_ONLY_AMAZON_PRODUCTS.equals("true")){
                PreparedStatement pst0=con.prepareStatement("truncate table tajplaza.sams_products_amazon");
            pst0.executeUpdate();
            SAMS_PRODUCTS_TABLE="tajplaza.sams_products_amazon";
           // PreparedStatement pst=con.prepareStatement(" select distinct substring_index(seller_sku,'_',-1) from tajplaza.existing_all_inventory where seller_sku like 'SC%'  ");
             PreparedStatement pst=con.prepareStatement("select distinct substring_index(seller_sku,'_',-1) ,pageurl from tajplaza.existing_all_inventory left join  hb_bc.sams_products s on  substring_index(seller_sku,'_',-1) =s.itemcode where  seller_sku like 'SC_%'   ");
 
            ResultSet rs=pst.executeQuery();
            while(rs.next()){
               // String itemcode=rs.getString(1);
               //   url="https://www.samsclub.com/sams/search/searchResults.jsp?searchTerm=ITEMCODE&searchCategoryId=all&selectedFilter=club";
               // url=url.replaceAll("ITEMCODE", itemcode);
                url=rs.getString("pageurl");
                // visitProductOld(con,url,url);
                System.out.println("Visiting url:"+url);
                 try{
                    if(url!=null) 
                        visitProductNew(con,url);
                 }catch (Exception e){e.printStackTrace();};
            }
           try{
                UploadInventoryLoaderFeedFile u=new UploadInventoryLoaderFeedFile();               
               CreateFeedFile.FEED_FILE_NAME="inventoryloader_feed_unavailable_at_sams.txt";
               System.setProperty("QUERY_SNO", "184-b");
               u.main(args);

           }catch (Exception e2){e2.printStackTrace();}            
            return;
            
        }
        PreparedStatement pst=con.prepareStatement("select pageurl,caturl from hb_bc.sams_products");
        ResultSet rs=pst.executeQuery();
        while(rs.next()){
            if(productURls.contains(rs.getString(2))==false)
                productURls.add(rs.getString(1));
            
            if(visitedCatsForCategories.contains(rs.getString(2))==false)
                visitedCatsForCategories.add(rs.getString(2));
        }         
         System.out.println("Dont with initialziations");
         wo.setJavaScriptEnabled(false);
         if(DEBUG_PRODUCT_URL.length()>0){
             visitProductNew(con,DEBUG_PRODUCT_URL );
             return;
         }           
            catList.add("https://www.samsclub.com/sams/homepage.jsp?xid=hdr_logo") ;
            catList.add("https://www.samsclub.com/sams/shops-promotions/7130108.cp?navAction=pop");
            if(RUN_MISSING_PRODUCTS_ONLY.equals("false")){
                 visitAllParentCats(con);
                
                                 
            }else{
                con.prepareStatement("truncate table hb_bc.sams_products_instock").executeUpdate();
                SAMS_PRODUCTS_TABLE="hb_bc.sams_products_instock";
            }
              
           System.out.println("Processing missing rows");
           wo.setRedirectEnabled(true);
            PreparedStatement ps= con.prepareStatement("select replace(code,'SC_','') item_code from hb_bc.exported_products_from_hb where cast(stock_level as decimal) >0 and code like 'SC_%' and replace(code,'SC_','') not in (select itemcode from "+ SAMS_PRODUCTS_TABLE+" where itemcode is not null  ) and code !='SC_GIFT_CARD'");
             ResultSet rs2=ps.executeQuery();
              while (rs2.next()){
                  System.out.println("Processing item code : SC_"+rs2.getString("item_code") );
                  String pageurl="https://www.samsclub.com/sams/search/searchResults.jsp?searchTerm=ITEMCODE&searchCategoryId=all&selectedFilter=club";;
                  pageurl=pageurl.replaceAll("ITEMCODE", rs2.getString("item_code"));
                 // String pageurl=rs2.getString("pageurl");
                  String caturl="xxx";
                  visitProductNew(con,pageurl);
              }
          con.close(); 
           System.out.println("Completed!!!!");
    }
    public static void visitAllParentCats(Connection con) throws Exception{
        boolean visitedSubCategory=false;
        for (int i=0;i<catList.size();i++){
           /* if(i>0){
            System.out.println("Not sure if right or wring, breaking");
            break;
            }*/
           String  parentCatUrl=(String)catList.get(i);
            if(visitedCatsForCategories.contains(parentCatUrl))
                continue;
            visitedSubCategory=true;
            visitedCatsForCategories.add(parentCatUrl);
             collectSubCats(parentCatUrl,con);
        }
    }
    public static int retryCount=0;
    public static ArrayList<String> parentCatUrlVisted=new ArrayList();
    public static void collectSubCats(String parentCatUrl,Connection con) throws Exception{
 
        String strIP=null;
        String strPort=null;
        String proxyUsername=null;
        String proxypassword=null;          
        if(  VISIT_ONLY_AMAZON_PRODUCTS.equals("true")==false  &&  prxies.length>0){
             if(iCounter2 == (prxies.length-1)) {
                 iCounter2=0;
             }
             strIP= prxies[iCounter2].split(":")[0];
            strPort= prxies[iCounter2].split(":")[1];

            if(prxies[iCounter2].split(":").length>2){
                proxyUsername = prxies[iCounter2].split(":")[2];
                proxypassword = prxies[iCounter2].split(":")[3];
            }     
            iCounter2++;
        ProxyConfig pr=new ProxyConfig(strIP,  Integer.parseInt( strPort));
        webClient.getOptions().setProxyConfig(pr); 
            
        }         
        //HtmlPage page0=CreateCatalog.getPage(parentCatUrl);
       /* if(parentCatUrl.indexOf("15190956")<0 && parentCatUrl.indexOf("7130108")<0  && parentCatUrl.indexOf("collection-shops")<0 ){
            System.out.println("Debug continue;");
            return;
        }*/
        if(parentCatUrlVisted.contains(parentCatUrl))
            return;
        parentCatUrlVisted.add(parentCatUrl);
        System.out.println("Visiting "+parentCatUrl);
        
        HtmlPage page0=null;
        try{
            webClient.getOptions().setSSLClientProtocols(new String[] { "TLSv1.2", "TLSv1.1", "TLSv1" });
            webClient.getOptions().setUseInsecureSSL(true);
            page0=webClient.getPage(parentCatUrl);
        }catch (Exception e){e.printStackTrace();
        if(retryCount++>50){
            System.err.println("ERROR inside colect subcats");
            System.exit(1);
        }
        collectSubCats(parentCatUrl,con);
        }
        //System.out.println("Link visited is "+page0.getByXPath("//link["));
        if(page0==null)
            return;
        if(page0.getElementById("plp-seemore")!=null){
           page0= (page0.getElementById("plp-seemore")).click();
           webClient.waitForBackgroundJavaScript(10000); 
           //page0.wait(100000);
         //  Thread.sleep(1000);
        }
        if(page0.getElementById("plp-seemore")!=null){
           page0= (page0.getElementById("plp-seemore")).click();
           webClient.waitForBackgroundJavaScript(10000); 
          // page0.wait(100000);
         //  Thread.sleep(1000);
        }       
        boolean productFound=false;
        if(page0.asText().indexOf("Showing ")>=0){
          //  Page p1=webClient.getPage("https://www.samsclub.com/soa/services/v1/catalogsearch/search?searchCategoryId=1088&searchTerm=&selectedFilter=club&xid=hdr_shop3_grocery-household-pets_beverages_bottled-water").getWebResponse().getContentAsString();
           List< DomElement> elems=(List< DomElement>) page0.getByXPath("//my-list");
           for(int i=0;i<elems.size();i++){
               DomElement elem=elems.get(i);
               String productUrl=elem.getAttribute("produrl");
               if(productUrl!=null){
                 if(productUrl.indexOf("samsclub.com")<0)
                     if( productUrl.indexOf("/")==0)
                        productUrl="https://www.samsclub.com"+productUrl;
                 else
                          productUrl="https://www.samsclub.com/"+productUrl; 
                 if(productURls.contains(productUrl)==false){
                     
                     visitProductNew( con,productUrl  ) ;
                     productFound=true;
                     productURls.add(productUrl);
                 }
                     
                 else
                   System.out.println("ProductURls already exists");
                            
               }
           }
        }   
       // if(productFound==false && ( parentCatUrl.indexOf("cat_sub")>=0 || (parentCatUrl.indexOf("hdr_shop")>=0 && page0.asText().indexOf("Showing") >=0 ))){
         if(productFound==false && ( parentCatUrl.indexOf("cat_sub")>=0 || (parentCatUrl.indexOf("hdr_shop")>=0  ))){
            System.out.println("No product found inserting a dummy row");
           PreparedStatement psInsert = con.prepareStatement("insert into hb_bc.sams_products( productid,title,catURL ) values (?,'Dummy Sub Category to prevent visit again',?)");
           psInsert.setLong(1, System.currentTimeMillis());
            psInsert.setString(2, parentCatUrl);
            psInsert.executeUpdate();;
            
        }
        List cats = page0.getByXPath("//a");
        for (int i=0;i<cats.size();i++){
            HtmlAnchor elem=(HtmlAnchor)cats.get(i);
            String href=elem.getHrefAttribute();
            String origHref=href;
             if(href.indexOf("samsclub.com")<0)
                 if( href.indexOf("/")==0)
                    href="https://www.samsclub.com"+href;
             else
                      href="https://www.samsclub.com/"+href;
             if(href.indexOf("hdr_shop")>=0 || href.indexOf("cat_sub")>=0){
                 if(href.indexOf("?")>0)
                      href=href+"&selectedFilter=club";
                 else
                      href=href+"?selectedFilter=club";
                //     href=StringUtils.substringBefore(href, "?");
                 catList.add(href);
                 if(origHref.indexOf("cat_sub")>=0){
                     catListWithInClub.add(origHref);
                 }

                
             }
            
        }        
    }
  /*  private static void visitProduct(Connection con3,String pageURL,String catURL ) throws Exception{ 
            String str="";
         HtmlPage productDetailPage=null;
         String strIP,strPort,proxyUsername,proxypassword;
        if(VISIT_ONLY_AMAZON_PRODUCTS.equals("true")==false  && prxies.length>0){
             if(iCounter2 == (prxies.length-1)) {
                 iCounter2=0;
             }
             strIP= prxies[iCounter2].split(":")[0];
            strPort= prxies[iCounter2].split(":")[1];

            if(prxies[iCounter2].split(":").length>2){
                proxyUsername = prxies[iCounter2].split(":")[2];
                proxypassword = prxies[iCounter2].split(":")[3];
            }     
            iCounter2++;
            ProxyConfig pr=new ProxyConfig(strIP,  Integer.parseInt( strPort));            
            webClient.getOptions().setProxyConfig(pr);          
        }         
        
        
         if(DEBUG_PRODUCT_URL!=null && DEBUG_PRODUCT_URL.length()>0 && pageURL.equals(DEBUG_PRODUCT_URL)==false){
             productDetailPage= webClient.getPage(pageURL);
         }else{
                try{
                    productDetailPage=webClient.getPage(pageURL);
                }catch (Exception e){
                    e.printStackTrace();
                 return;
                }
        }



        String price   = null;
        String strProductStatus = "";
        boolean bIsProductOnline = productDetailPage.getElementById("addtocartsingleajaxonline")!= null ? true : false;
        if(bIsProductOnline==false){
            //sometimes the product may be  o/s online
             bIsProductOnline = productDetailPage.getByXPath("//div[@class='biggraybtn']").size()>0  ? true : false;
        }
        boolean bIsProductInClub = productDetailPage.getElementById("addtocartsingleajaxclub")!= null ? true : false;
        List<HtmlParagraph> list  = null;
        System.out.println("\n"+str+"\tProduct Number : " + (iTotalProductFetched++) + "\t"+pageURL);
        try {
            if(bIsProductOnline && !bIsProductInClub) {
                System.out.println(str+"\tProduct is online but not in store.  .");
                  strProductStatus="Online Not In Store";
                //continue;
            } else if(bIsProductOnline && bIsProductInClub){
                System.out.println(str+"\tProduct is online and also in store. ");
                strProductStatus = "Online and In Club";
                
                try{
                        ArrayList<HtmlSpan> clubPrice =(ArrayList<HtmlSpan>) productDetailPage.getByXPath("//div[@class=\"sc-channel-in-club\"]//span[@class=\"sc-price\"]");
                        
                        if(clubPrice.size()>0){
                            price=clubPrice.get(0).getTextContent();
                        }
                    
                    }catch (Exception e){e.printStackTrace();;}
                
                try{
                        List<HtmlSpan> list1 =(List<HtmlSpan>) productDetailPage.getByXPath("//span");
                        for(HtmlSpan span : list1) {
                            if(span.hasAttribute("itemprop") && span.getAttribute("itemprop").equalsIgnoreCase("price")) {
                                price = span.getTextContent();
                            }
 
                        }                    
                    }catch (Exception e){e.printStackTrace();;}
                try {
                   if(price==null ) price = ((HtmlSpan)productDetailPage.getByXPath("//*[@id=\"itemPageMoneyBox\"]/div[1]/div[2]/ul/li/span[2]").get(0)).asText() + "."+ ((HtmlSpan)productDetailPage.getByXPath("//*[@id=\"itemPageMoneyBox\"]/div[1]/div[2]/ul/li/span[3]").get(0)).asText();                                                

                } catch(Exception e) {
                    try {
                        price = ((HtmlSpan)productDetailPage.getByXPath("//*[@id=\"itemPageMoneyBox\"]/div[1]/ul/li/span[2]").get(0)).asText() + "."+ ((HtmlSpan)productDetailPage.getByXPath("//*[@id=\"itemPageMoneyBox\"]/div[1]/ul/li/span[3]").get(0)).asText();                                                

                    } catch(Exception eee) {
                        try{
                            price = ((HtmlSpan)productDetailPage.getByXPath("//*[@id=\"addtocartsingleajaxclub\"]/span[1]").get(0)).asText();
                        }catch (Exception eeee){
                            price="0";
                        }
                        if(price != null && price.trim().length()>0) {
                            price = price.replace("$", "");
                        } else {
                            System.out.println("There is something wrng. Why price is not available?");
                        }
                    }

                }

                if(price != null && price.contains("$") ) {
                    System.out.println("This product have member price. Considering member price as price ");
                    try {
                        price = ((HtmlSpan)productDetailPage.getByXPath("//*[@id=\"itemPageMoneyBox\"]/div[1]/ul[1]/li[1]/span[2]").get(0)).asText();
                        price = price.replace("$", "");
                    } catch(Exception e) {
                        System.out.println("There is something wrong in consideting member price");
                    }
                }


            } else if(bIsProductInClub){
                System.out.println(str+"\tProduct is in store only.");
                strProductStatus = "In Club";
                try{
                        List<HtmlSpan> list1 =(List<HtmlSpan>) productDetailPage.getByXPath("//span");
                        for(HtmlSpan span : list1) {
                            if(span.hasAttribute("itemprop") && span.getAttribute("itemprop").equalsIgnoreCase("price")) {
                                price = span.getTextContent();
                            }
 
                        }                    
                    }catch (Exception e){e.printStackTrace();;}                
                try {
                   if(price==null ) price = ((HtmlSpan)productDetailPage.getByXPath("//*[@id=\"itemPageMoneyBox\"]/div[1]/ul/li/span[2]").get(0)).asText() + "."+ ((HtmlSpan)productDetailPage.getByXPath("//*[@id=\"itemPageMoneyBox\"]/div[1]/ul/li/span[3]").get(0)).asText();                                                
                } catch(Exception e) {
                    try {
                        price = ((HtmlSpan)productDetailPage.getByXPath("//*[@id=\"itemPageMoneyBox\"]/div[1]/div[1]/ul/li/span[2]").get(0)).asText() + "."+ ((HtmlSpan)productDetailPage.getByXPath("//*[@id=\"itemPageMoneyBox\"]/div[1]/div[1]/ul/li/span[3]").get(0)).asText();                                                
                    } catch(Exception ee) {

                        List<HtmlSpan> listp = (List<HtmlSpan>)productDetailPage.getByXPath("//*[@id=\"itemPageMoneyBox\"]/div/span[1]");
                        if(listp != null && !listp.isEmpty()) {
                            System.out.println("Item is not available at our store.");
                             strProductStatus = "Out Of Stock";
                            //return;
                        }
                    }
                }
            } else {
                System.out.println("Product is out of stock");
                 strProductStatus = "Out Of Stock";
            }
            try{
            float floatPrice=0;
                try{
                    floatPrice=Float.valueOf(price);
                } catch (Exception e2){

                }
                if(floatPrice==0){
                    if(productDetailPage.getByXPath("//div[@id=\"itemPageMoneyBox\"]//span[@class=\"dkGray2Col\"]").size()>0){
                        int priceSize=productDetailPage.getByXPath("//div[@id=\"itemPageMoneyBox\"]//span[@class=\"dkGray2Col\"]").size();
                        String priceDollar=((HtmlSpan)productDetailPage.getByXPath("//div[@id=\"itemPageMoneyBox\"]//span[@class=\"dkGray2Col\"]").get(priceSize-1)).asText();                    
                        price=priceDollar.replace("$","").trim();
                        try{
                            floatPrice=Float.valueOf(price);
                        } catch (Exception e2){e2.printStackTrace();}
                        
                    }
                        
                }
                if(floatPrice==0){
                    int priceSize=productDetailPage.getByXPath("//div[@id=\"itemPageMoneyBox\"]//span[@class=\"price\"]").size();
                    if(priceSize>0)
                        price=((HtmlSpan)(productDetailPage.getByXPath("//div[@id=\"itemPageMoneyBox\"]//span[@class=\"price\"]").get(priceSize-1))).asText();
                }                        
            }catch (Exception e2)   {}


            String strDesc=null;
            if(strDesc == null || strDesc.trim().length()==0) {
                List<HtmlDivision> listDesc = (List<HtmlDivision>)productDetailPage.getByXPath("//div[@id=\"panelAboutItem\"]");
                if(listDesc != null && !listDesc.isEmpty())  {
                    strDesc = listDesc.get(0).asXml();
                }


            }                       
            list  = (List<HtmlParagraph>)productDetailPage.getByXPath("//*[@id=\"tabItemDetails\"]/div[5]/p");
             if(strDesc == null || strDesc.trim().length()==0){
                    for(HtmlParagraph htmlParagraph : list) {
                        if(htmlParagraph.asText()!= null && htmlParagraph.asText().length()>10) {
                            strDesc = htmlParagraph.asXml();
                        }
                    }

             }

            if(strDesc==null || strDesc.trim().length()==0) {
                List<HtmlDivision> listDesc = (List<HtmlDivision>)productDetailPage.getByXPath("//*[@id=\"itemPageMoneyBox\"]/div/span[1]");
                if(listDesc != null && !listDesc.isEmpty())  {
                    strDesc = listDesc.get(0).asXml();
                }
                if(strDesc == null || strDesc.trim().length()==0) {

                    try {
                        strDesc =  ((List<HtmlDivision>)productDetailPage.getByXPath("//*[@id=\"itemDesc\"]")).get(0).asXml();
                    } catch (Exception e) {
                        try{
                            strDesc =  ((List<HtmlDivision>)productDetailPage.getByXPath("//*[@id=\"tabItemDetails\"]/div[6]")).get(0).asXml();
                        }catch (Exception e2){
                            strDesc="";
                        }
                    }   
                }
            }

            if(strDesc == null || strDesc.trim().length()==0) {
                List<HtmlDivision> listDesc = (List<HtmlDivision>)productDetailPage.getByXPath("//*[@id=\"tabItemDetails\"]/div[2]");

                if(listDesc != null && !listDesc.isEmpty())  {
                    strDesc = listDesc.get(0).asXml();
                }
            }
            if(strDesc!=null && strDesc.length()>4000)
                strDesc=strDesc.substring(0,3999);      
            String strhtmlImage ="NA";
             HtmlImage img =null;
            if(productDetailPage.getByXPath("//*[@id=\"plImageHolder\"]/img").size()>0){
              img = (HtmlImage)productDetailPage.getByXPath("//*[@id=\"plImageHolder\"]/img").get(0);
            strhtmlImage= img.getSrcAttribute();
            }
            String strCategorya="";
                if(productDetailPage.getByXPath("//*[@id=\"breadcrumb\"]").size()>0){
                    HtmlDivision Category = ((HtmlDivision)productDetailPage.getByXPath("//*[@id=\"breadcrumb\"]").get(0));
                     strCategorya= format(Category);
                }
                String title ="";
                boolean only_x_in_stock=false;
 
            if(productDetailPage.getByXPath("//h1").size()>0){
                 title = ((DomElement)productDetailPage.getByXPath("//h1").get(0)).asText();
                
            }                
            if(title!=null && title.length()>799)
                title=title.substring(0,799);
            if(strCategorya!=null && strCategorya.length()>1999)
                strCategorya=strCategorya.substring(0,1999);

            String strProductid ="NA"+System.currentTimeMillis();
            if(productDetailPage.getElementById("mbxProductId")!=null)
                strProductid=((HtmlHiddenInput) productDetailPage.getElementById("mbxProductId")).getValueAttribute();

            String strItemCode = "";
            String strBrand   = "";
            List<HtmlSpan> list1 =(List<HtmlSpan>) productDetailPage.getByXPath("//span");
            for(HtmlSpan span : list1) {
                if(span.hasAttribute("itemprop") && span.getAttribute("itemprop").equalsIgnoreCase("brand")) {
                    strBrand = span.getTextContent();
                }
                if(span.hasAttribute("itemprop") && span.getAttribute("itemprop").equalsIgnoreCase("productID")) {
                    strItemCode =  span.getTextContent();
                    strItemCode = strItemCode.trim();
                    strItemCode = strItemCode.substring(strItemCode.indexOf(":")+1).trim();
                }
            }

            if(img!=null){
              //  File imageFile = new File("C:\\temp2012\\images\\"+strProductid+".jpeg");
               // img.saveAs(imageFile);
            }
            if(StringUtils.countMatches(strCategorya, ">")>0)
                    strCategorya=strCategorya.substring(0,strCategorya.lastIndexOf(">")-1);
            if(StringUtils.countMatches(strCategorya, ">")>0)
                strCategorya=strCategorya.substring(0,strCategorya.lastIndexOf(">")-1);
            String fullstrCategorya=strCategorya;
            if(StringUtils.countMatches(strCategorya, ">")>0){
                String strCategoryaNew="";
                String[] arr=strCategorya.split(">");
                for(int i=0;i<arr.length;i++){
                    if(i>4)
                        break;
                    if(i>0)
                        strCategoryaNew+=">";
                    strCategoryaNew+=arr[i];
                }
                strCategorya=strCategoryaNew;
            }
            System.out.println("Inserting : "+pageURL);
            iTotalProductInserted++;
            
    
             HtmlPage p=webClient.getPage("https://www.samsclub.com/sams/shop/product/moneybox/showLowInStockQuantity.jsp?skuId=sku19714467&clubId=4994&channel=club&onlineLegacyItemId=182460444&clubLegacyItemId=182460444&_=1467247163446");
            if(productDetailPage.asXml().indexOf("data-clubstatus=\"lowInStock\"")>=0  ){
                only_x_in_stock=true;
            }
           PreparedStatement psInsert = con3.prepareStatement("insert into "+SAMS_PRODUCTS_TABLE+"(productid, title, price, pageurl, description, product_image_url, brand, category,itemcode,product_available_in_local_club,last_update_date,product_status,catURL,only_x_in_stock ) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    
            psInsert.setString(1, strProductid);
            psInsert.setString(2, title);
            if(price==null)
                price="999";
            int priceFpriceF=(int)Math.ceil(Float.valueOf(price.replaceAll(",", "")));
            psInsert.setInt(3, priceFpriceF);
            psInsert.setString(4, pageURL);
            psInsert.setString(5, strDesc);
            psInsert.setString(6, strhtmlImage);
            psInsert.setString(7, strBrand);
            psInsert.setString(8, strCategorya);

            psInsert.setString(9, strItemCode);
            psInsert.setString(10, ""+bIsProductInClub);
            psInsert.setTimestamp(11, new java.sql.Timestamp(System.currentTimeMillis()));
            psInsert.setString(12, strProductStatus);
            psInsert.setString(13, catURL);
            psInsert.setBoolean(14, only_x_in_stock);

            try {
                psInsert.executeUpdate();
            } catch(Exception e) { 
                e.printStackTrace();
            }
          
            } catch(Exception exceptionwhile){
                exceptionwhile.printStackTrace();
                    System.out.println("Exception while fatching product");
      }
        
    }
     */
 private static void visitProductNew(Connection con3,String pageURL ) throws Exception{ 
            String productID=StringUtils.substringBefore(pageURL, ".ip");
           productID= StringUtils.substringAfterLast(productID, "/");
           if(productID==null){
               productID="NA";
               System.out.println("No  productID present in pageURL:"+pageURL);
               System.exit(1);
           }
           String itemCode="NA";
            String url="https://www.samsclub.com/api/soa/services/v1/catalog/product/"+productID+"?response_group=LARGE&clubId=4994";
            System.out.println("Visiting url:"+url);
            System.setProperty("https.protocols", "TLSv1");
            webClient.getOptions().setSSLClientProtocols(new String[] { "TLSv1.2", "TLSv1.1", "TLSv1" });
            webClient.getOptions().setUseInsecureSSL(true);
            String jsonResponse = webClient.getPage(url).getWebResponse().getContentAsString();
            JSONObject jsobj=JSONObject.fromObject(jsonResponse);
            String title="NA";
            String price="0";
            String upc="";
           
           boolean only_x_in_stock=false;
            String bIsProductInClub="In Club";
            if(jsobj.containsKey("payload")){
                JSONObject payload=jsobj.getJSONObject("payload");
                if(payload.containsKey("productName")){
                    title=payload.getString("productName");
                }
                if(payload.containsKey("skuOptions") &&((JSONObject) payload.getJSONArray("skuOptions").get(0)).containsKey("itemNumber") 
                       ){
                    itemCode=((JSONObject) payload.getJSONArray("skuOptions").get(0)).getString("itemNumber");
 
                }    
                if(payload.containsKey("skuOptions") &&((JSONObject) payload.getJSONArray("skuOptions").get(0)).containsKey("upc") 
                       ){
                    upc=((JSONObject) payload.getJSONArray("skuOptions").get(0)).getString("upc");
                    if(upc.length()==10){
                        upc="0"+upc;
                    }
                    if(upc.length()==11){
                        upc=GetLastDigitUPC.addCheckDigit(upc);
                    } 
                }                 
                if(payload.containsKey("clubPricing") && payload.getJSONObject("clubPricing").containsKey("listPrice") 
                        && payload.getJSONObject("clubPricing").getJSONObject("listPrice").containsKey("currencyAmount")){
                    price=payload.getJSONObject("clubPricing").getJSONObject("listPrice").getString("currencyAmount");
                }                
                if(payload.containsKey("clubInventory") && payload.getJSONObject("clubInventory").containsKey("qtyLeft") 
                        && payload.getJSONObject("clubInventory").containsKey("qtyLeft")){
                    Integer qtyLeft=payload.getJSONObject("clubInventory").getInt("qtyLeft");
                    if(qtyLeft!=-1)
                        only_x_in_stock=true;
                }                 
           
                if(payload.containsKey("clubInventory") && payload.getJSONObject("clubInventory").containsKey("status") 
                        && payload.getJSONObject("clubInventory").containsKey("status")){
                    String status=payload.getJSONObject("clubInventory").getString("status");
                    if(status.toLowerCase().indexOf("outofstock") >=0)
                        only_x_in_stock=true;
                }                 
            }                
           PreparedStatement psInsert = con3.prepareStatement("insert into "+SAMS_PRODUCTS_TABLE+"(productid, title, price, pageurl,itemcode,product_available_in_local_club,last_update_date,only_x_in_stock,upc ) values (?,?,?,?,?,?,?,?,?)");
                    
            psInsert.setString(1, productID);
            psInsert.setString(2, title);
            if(price==null)
                price="999";
            int priceFpriceF=(int)Math.ceil(Float.valueOf(price.replaceAll(",", "")));
            psInsert.setInt(3, priceFpriceF);
            psInsert.setString(4, pageURL);
            psInsert.setString(5, itemCode);
            psInsert.setString(6, ""+bIsProductInClub);
            psInsert.setTimestamp(7, new java.sql.Timestamp(System.currentTimeMillis()));
            psInsert.setBoolean(8, only_x_in_stock);
             psInsert.setString(9, upc);
            try {
                psInsert.executeUpdate();
            } catch(Exception e) { 
                e.printStackTrace();
            }
          
           
    }    
     private static String format(HtmlDivision ttmlDivision)  {
         String strBreadCrumb = "";
             Iterable<DomElement> doms = ttmlDivision.getChildElements();
             Iterator itr = doms.iterator();
             if(itr.hasNext()) itr.next();
            while(itr.hasNext()) {
               
                DomElement htmlSpan = (DomElement)itr.next();
                String strText = htmlSpan.asText();
                if(strText != null && strText.trim().length()>0) {
                   /* if(strText.length()>30 && itr.hasNext())
                    {
                        strText=strText.substring(0,30);
                    }*/
                    strBreadCrumb += strText +" > ";
                } 
            }
            return strBreadCrumb;
        }

    private static void setCookies(WebClient webClient) {
        
        try {  
             Gson gson = new Gson();

    List<DefaultCookies> items = gson.fromJson(new FileReader("C:\\Google Drive\\Dropbox\\program\\samsdefaultcoockeies.json"), new TypeToken<List<DefaultCookies>>(){}.getType());
    
    
            for(DefaultCookies dc : items) { 
                Cookie c = new Cookie(dc.getDomain(), dc.getName(), dc.getValue());
                webClient.getCookieManager().addCookie(c);
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } 
    }    
}
