/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;

import bombayplaza.pricematch.RetrievePage;
 
import bombayplaza.pricematch.RetrievePage; 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import net.sf.json.JSONObject;
import bombayplaza.pricematch.MatchThePrice;
import java.util.HashMap;
import org.apache.commons.lang.StringUtils;
import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import amazon.product.QueryProduct;
import bigcommerce.orders.FetchOrders;
import com.tajplaza.orders._2013_09_01.OrderFetcherSampleNew;
import com.tajplaza.upload.CreateFeedFile;
import com.tajplaza.upload.UploadInventoryLoaderFeedFile;
import java.util.Calendar;
import java.util.GregorianCalendar;
import net.sf.json.JSONArray;
import unfi.BigCommerce;
import com.tajplaza.upload.CreateFeedFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import jet.UpdateJetInventory;
import org.apache.commons.io.FileUtils;
/**
 *
 * @author prabu
 */
public class HBAmazonFixShelfQuantity {
       public static Connection con=null;
    private static String[] args;
       public static void main(String args[]) throws Exception{
           con=RetrievePage.getConnection();
           DeleteOldFiles(new File("C:\\Google Drive\\Dropbox\\logs\\quantityupdate\\"),3);
           // updateHBQty("112-AMA-AMAZON_VARIATIONS-HB");
           //First HB
           //Fetch Amazon orders
           OrderFetcherSampleNew of=new OrderFetcherSampleNew();
           System.setProperty("JUST_YESTERDAY_ORDERS", "true");
           System.setProperty("NUMBER_OF_DAYS", "1");
          try{
              of.main(args);
          }catch (Exception e){
              System.out.println("Some issue first time, lets try one more time");
              of.main(args);
          }
          
          //Fetch HB orders
          FetchOrders hbOrders=new FetchOrders();
          hbOrders.LAST_HOURS_ORDERS_ONLY=1;
          hbOrders.main(args);
          
           initializeTables();
           updateAmazonQty(args);
           Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HH");
            System.out.println( "Current hour:"+Integer.valueOf(sdf.format(cal.getTime())) );
            if(Integer.valueOf(sdf.format(cal.getTime())) ==5    ){
                System.out.println("Setting the inventory for HB also");
                updateHBQty("112-AMA-AMAZON_VARIATIONS");
                updateHBQty("112-AMA-HB");
                
                updateHBQty("112-AMA-AMAZON-DELETENONEXISTING_AMA");
                
                
            }
           
           
       }
       public static void initializeTables() throws Exception{
            
           String line;
             BufferedReader  br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\sql\\pending_orders.sql")));
        while ((line = br0.readLine()) != null)   {
            if(line.trim().length()>5){
                System.out.println("Executing sql:"+line);
                PreparedStatement ps=con.prepareStatement(line.trim());
               try{ ps.executeUpdate();}catch (Exception e){
                   if(line.indexOf("hb_bc.last_week_shipped_profit")<0){
                       throw e;
                   }
               }
                
            }
        }           
           /*
           con.prepareStatement(sql).executeUpdate();
            sql="drop table if exists tajplaza.tmp_scanner_data_shelf";
           con.prepareStatement(sql).executeUpdate();
            //sql="drop table if exists hb_bc.tmp_exported_products_from_hb";
           //con.prepareStatement(sql).executeUpdate();
           
            //sql="create table hb_bc.tmp_exported_products_from_hb as select * from hb_bc.exported_products_from_hb where code like '%AMA%'";
           //con.prepareStatement(sql).executeUpdate();
           
           sql="create table tajplaza.tmp_scanner_data_shelf as select * from tajplaza.scanner_data_shelf  ";
           con.prepareStatement(sql).executeUpdate();
           sql="create table tajplaza.tmp_pending_amazon_orders  select sellersku,p.product_upc_ean,replace(substring_index(substring_index(sellersku,'_',3),'_',-1),'PACK','') pkg_qty,sum(quantityordered) qtyordered,sum(case when upper(orderstatus)='UNSHIPPED' then quantityordered else 0 end) unshipped_qtyordered,group_concat(o.order_id,'<br/>') pending_order_ids from tajplaza.orders o  left join hb_bc.ama_exported_products_from_hb p on o.sellersku=p.code  where  upper(orderstatus) in ('UNSHIPPED','PENDING') and  purchasedate >(curdate() - interval 7 day) group by sellersku ";
           con.prepareStatement(sql).executeUpdate();
            sql="delete from  tajplaza.tmp_pending_amazon_orders  where pkg_qty in('AMA','FBA')";
           con.prepareStatement(sql).executeUpdate();

           
            sql="insert into tajplaza.tmp_pending_amazon_orders  select sellersku,v.upc , v.pkg_qty,sum(quantityordered) qtyordered,sum(case when upper(orderstatus)='UNSHIPPED' then quantityordered else 0 end) unshipped_qtyordered,group_concat(o.order_id)  pending_order_ids from tajplaza.orders o  , tajplaza.variation_sku_upcs v where o.sellersku=v.sku  and  upper(orderstatus) in ('UNSHIPPED','PENDING') and  purchasedate >(curdate() - interval 7 day)  and (sellersku not like 'BJ%' and sellersku not like 'CO%' and sellersku not like 'SC%' )  group by sellersku";
           con.prepareStatement(sql).executeUpdate();

           

           
           
           sql="  update  tajplaza.tmp_scanner_data_shelf s  set s.qty=s.qty-(select case when p.qtyordered is null then 0 else sum(p.qtyordered*pkg_qty) end  from tajplaza.tmp_pending_amazon_orders p where s.upc=p.product_upc_ean )";
           con.prepareStatement(sql).executeUpdate();

            sql="  update  tajplaza.tmp_scanner_data_shelf s  set s.unshipped_order_qty=(select case when p.unshipped_qtyordered is null then 0 else sum(p.unshipped_qtyordered*pkg_qty) end  from tajplaza.tmp_pending_amazon_orders p where s.upc=p.product_upc_ean )";
           con.prepareStatement(sql).executeUpdate();

           
           sql="  update  tajplaza.tmp_scanner_data_shelf s  set pending_order_ids= (select group_concat(p.pending_order_ids,'<br/>')  from tajplaza.tmp_pending_amazon_orders p where s.upc=p.product_upc_ean )";
           con.prepareStatement(sql).executeUpdate();
           
           sql="update tajplaza.tmp_scanner_data_shelf s set qty=0 where qty<0;";
           con.prepareStatement(sql).executeUpdate();
            */
       }
       public static boolean UPDATE_JET_QTY_ALSO=false;
       public static void updateHBQty(String query) throws Exception{

           String sql=RetrievePage.getWebSql(query);
           
           ResultSet rs=con.prepareStatement(sql).executeQuery();
           while (rs.next()){
               String code=rs.getString("sku");
               String product_id=rs.getString("hid");
               String updated_inventory_level=rs.getString("hquantity");
               String current_stock_level=rs.getString("current_stock_level");
               if(updated_inventory_level!=null && current_stock_level!=null && 
                       Integer.parseInt(updated_inventory_level) == Integer.parseInt(current_stock_level))
                   continue;
               JSONObject jSONObject = new JSONObject();
                System.out.println("Setting inventory level "+updated_inventory_level+" current stock level:"+current_stock_level+" for product_id:"+product_id +" , sku:"+code);
                
                jSONObject.put("inventory_level", updated_inventory_level);
                /*boolean is_visible=true;
                if(Integer.valueOf(quantity)>0)
                    is_visible=true;
                jSONObject.put("is_visible", is_visible);*/
                try{
                    BigCommerce.updateProduct(product_id, jSONObject);             
                }catch (Exception e2){e2.printStackTrace();}
               
               if(UPDATE_JET_QTY_ALSO){
                   try{
                       
                    int stock_level=rs.getInt("hquantity");
                    jSONObject = new JSONObject();
                    jSONObject.put("fulfillment_node_id", "0f7fa5f221524616b2fde3b5e40764a9");
                    jSONObject.put("quantity", stock_level);
                    JSONArray jsArr=new JSONArray();
                    jsArr.add(jSONObject);

                    jSONObject = new JSONObject();
                    jSONObject.put("fulfillment_nodes", jsArr);
                    System.out.println("Updating NEW JET Inventory for SKU:"+code + " to " + stock_level)       ;
                    UpdateJetInventory.sendJSON("https://merchant-api.jet.com/api/merchant-skus/"+code+"/Inventory","PUT",jSONObject);
                       
                   }catch (Exception e){
                       e.printStackTrace();
                   }
                   
               }
           }
                   
   }
   public static void updateAmazonQty(String[] args) throws Exception {
         UploadInventoryLoaderFeedFile u=new UploadInventoryLoaderFeedFile();
      try{
       CreateFeedFile.FEED_FILE_NAME="inventoryloader_feed_existingproducts.txt";
       System.setProperty("QUERY_SNO", "112-AMA-HB");
       u.main(args);
       FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\tmp\\"+ CreateFeedFile.FEED_FILE_NAME), new File("C:\\Google Drive\\Dropbox\\logs\\quantityupdate\\"+ CreateFeedFile.FEED_FILE_NAME+"_"+System.currentTimeMillis()));;
          
      }catch (Exception e2){e2.printStackTrace();}
       

       try{
           CreateFeedFile.FEED_FILE_NAME="inventoryloader_feed_variations.txt";
           System.setProperty("QUERY_SNO", "112-AMA-AMAZON_VARIATIONS");
           
           u.main(args);
           FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\tmp\\"+ CreateFeedFile.FEED_FILE_NAME), new File("C:\\Google Drive\\Dropbox\\logs\\quantityupdate\\"+ CreateFeedFile.FEED_FILE_NAME+"_"+System.currentTimeMillis()));;
           
       }catch (Exception e2){e2.printStackTrace();}
       try{
           CreateFeedFile.FEED_FILE_NAME="inventoryloader_feed_NONexistingproducts-AMAZON.txt";
           System.setProperty("QUERY_SNO", "112-AMA-AMAZON-DELETENONEXISTING_AMA");
           u.main(args);
           FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\tmp\\"+ CreateFeedFile.FEED_FILE_NAME), new File("C:\\Google Drive\\Dropbox\\logs\\quantityupdate\\"+ CreateFeedFile.FEED_FILE_NAME+"_"+System.currentTimeMillis()));;
           
       }catch (Exception e1){e1.printStackTrace();}
       try{
           //Create new feed file but do not auto upload
            CreateFeedFile c=new CreateFeedFile();
            c.FEED_FILE_NAME="inventoryloader_feed_newproducts-AMAZON.txt";
            c.createFeedFileFromQuery("112-AMA-AMAZON-NEWPRODUCTS");
           
       }catch (Exception e2){e2.printStackTrace();}
       try{
           //Create new feed file but do not auto upload
          // System.err.println("Sleeping before zeoring 0 quantity so the previous uploads went through");
        //   Thread.sleep(30*1000);
         //  CreateFeedFile.FEED_FILE_NAME="zerosinglepack-multipackexists.txt";
        //   System.setProperty("QUERY_SNO", "112-AMA-zerosinglepack-multipackexists");
           
           u.main(args);
           FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\tmp\\"+ CreateFeedFile.FEED_FILE_NAME), new File("C:\\Google Drive\\Dropbox\\logs\\quantityupdate\\"+ CreateFeedFile.FEED_FILE_NAME+"_"+System.currentTimeMillis()));;
            
           
       }catch (Exception e2){e2.printStackTrace();}       
   }
public static void DeleteOldFiles(File file,int x) {
	//System.out.println("Now will search folders and delete files,");
	if (file.isDirectory()) {
		System.out.println("Date Modified : " + file.lastModified());
		for (File f : file.listFiles()) {
			DeleteOldFiles(f,x);
		}
	} else {
                long diff = new Date().getTime() - file.lastModified();

                if (diff > x * 24 * 60 * 60 * 1000) {
                    file.delete();
                }            
	}
    }
}
