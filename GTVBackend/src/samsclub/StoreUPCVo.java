/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;

/**
 *
 * @author US083274
 */
public class StoreUPCVo {

    public String getRow_num() {
        return row_num;
    }

    public void setRow_num(String row_num) {
        this.row_num = row_num;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getStore_price() {
        return store_price;
    }

    public void setStore_price(String store_price) {
        this.store_price = store_price;
    }

    public String getStore_item_code() {
        return store_item_code;
    }

    public void setStore_item_code(String store_item_code) {
        this.store_item_code = store_item_code;
    }
    String row_num;
    String upc;
    String store_price;
    String store_item_code;
    
}
