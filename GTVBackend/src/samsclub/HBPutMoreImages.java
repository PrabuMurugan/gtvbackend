/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;

/**
 *
 * @author prabu
 */

import unfi.*;
 
import bombayplaza.pricematch.RetrievePage; 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import net.sf.json.JSONObject;
import bombayplaza.pricematch.MatchThePrice;
import java.util.HashMap;
import org.apache.commons.lang.StringUtils;
import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import amazon.product.QueryProduct;
import bombayplaza.catalogcreation.CreateCatalog;
import com.tajplaza.products.ListMatchingProductsSample;
import java.util.Calendar;
import java.util.GregorianCalendar;
import net.sf.json.JSONArray;

public class HBPutMoreImages {
      
    public static void main(String args[]) throws Exception{
     // Product  p = getUPCDetails("B00H9H56QA");
     //console(p);
       
     updatePrices();
     System.out.println("Program completed");

 } 
 
//   public static String DEBUG_SQL=" and p.code='CO_311_181679' ";

 
   public static String DEBUG_SKU="";
 
 public static String AMAZON_TABLE_NAME="amazon_catalog_hb";

 public static Float ABSOLUTE_MIN_MARGIN=(float)0;
 public static Float MIN_MARGIN=(float)0;
    public static void updatePrices() throws Exception{
            Connection con=RetrievePage.getConnection();
            PreparedStatement pst = null,pst2=null;
            ResultSet rs = null,rs2=null;
             String sort_order="";
             sort_order=String.valueOf(System.getProperty("sort_order",""));
             String PRODUCTION =String.valueOf(System.getProperty("PRODUCTION",""));
             if(PRODUCTION.equals("true"))
                 DEBUG_SKU="";
             
             
            
          
             
             
             String sql=
                     //"select item_code sku,c.price reg_cost,p.price current_price,p.product_id,c.asin from hb_bc.hb_catalog c, hb_bc.exported_products_from_hb p where c.item_code=p.code    and stock_level>0  "+
                       // " union "+
                      //  " select sku sku,oc  reg_cost,p.price current_price,p.product_id,c.asin,category_string,category_details,packs_in_1_case,p.name,weight,ui.size,p.product_upc_ean,stock_level from hb_bc.original_cost c, hb_bc.exported_products_from_hb p left join  hb_bc.unfi_input_catalog ui on   replace(p.code,'UN_','') = ui.item_code    where     c.sku=p.code  and p.code not like 'DT%' and c.oc is not null and cast(c.oc as decimal)>0    " ;
                    // " SELECT p.product_id,p.code,o.asin FROM hb_bc.exported_products_from_hb p left join hb_bc.original_cost o on p.code=o.sku where (length(product_images)=0 or product_images is null or product_images like '%no-img%' or product_images = '') and  stock_level>0  and Product_type = 'P'  ";
                      "select p.product_id,p.code,u.asin from hb_bc.exported_products_from_hb p,tajplaza.un_full_case_asins  u where p.code=u.code  and u.asin like 'B%' and stock_level>0  and p.code not in (select code from hb_bc.hb_put_more_images)"
                     + "and (p.code in (select id from harrisburgstore.gts_product_feed)  )  "
                     + " limit 100";
            System.out.println("Exceutung sql:"+sql);
            PreparedStatement psInsert=con.prepareStatement("insert into hb_bc.hb_put_more_images (code) values(?)");
            
            pst=con.prepareStatement(sql);
            rs=pst.executeQuery();
            int count=0;
            ProcessWorkingCatalog pw=new ProcessWorkingCatalog();
            
            while(rs.next()){ 
                Integer product_id=rs.getInt("product_id");
                String code=rs.getString("code");
                String asin=rs.getString("asin");
                if(DEBUG_SKU.length()>0 && DEBUG_SKU.equals(code)==false)
                    continue;                
                //HashMap hm=MatchThePrice.getProductDetails(asin, true, false, false);
                CreateCatalog.ERROR_MOVE_TO_NEXT_PAGE=true;
                 HashMap hm=MatchThePrice.getProductDetails(asin, true, false, false);  
                 if(hm==null)
                     continue;
                 if(hm.containsKey("allImages")){
                     ArrayList allImages=(ArrayList) hm.get("allImages");
                     for(int i=1;i<allImages.size();i++){
                        String imageurl=(String) allImages.get(i);
                        if(imageurl!=null && imageurl.length()>10 && imageurl.indexOf("51m1gdQJW2L")<0){
                        System.out.println("Uploading image for :"+code);
                          String uploadImage = CreateHBProduct.uploadImage(product_id,imageurl,false);


                        }
                         
                     }
                          psInsert.setString(1, code);
                          psInsert.executeUpdate();;                     
                 }
            // BigCommerce.updateProduct(product_id, jSONObject);                
                    
             
                    
                }
               
                 
                  
                 
                 
   }
    public static int amazonVisitCounter=0;
    public static Product getProductFromAmazon(String sku,String asin, Float oc,Connection con) throws Exception {
                Product p=new Product();
                p.setAsin(asin);
                String WHOLESALERNAME=StringUtils.substringBefore(sku, "_");
                p.setWHOLESALERNAME(WHOLESALERNAME);
                 ProcessWorkingCatalog pw=new ProcessWorkingCatalog();
             
                ArrayList<Product> ar=pw.getProductsFromTable(p, con, AMAZON_TABLE_NAME);
                if(ar.size()==0){
                    if(++amazonVisitCounter>5000){
                        System.out.println("Even though I do not have the product information in "+AMAZON_TABLE_NAME+" amazonVisitCounter exceeded 1000. So not trying today");
                    }else{
                        QueryProduct qp=new QueryProduct();
                        QueryProduct.I_WANT_ORIGINAL_BESTPRICE_AT_NOOTHER_FBA_SELLERS=true;
                        p=qp.getProductListings1(asin);
                        if(p.getBestprice()==0) {
                            System.out.println("ASIN IS CHANGED IN AMAZON. Lets try through getProductDetails API");
                            HashMap hm=MatchThePrice.getProductDetails(asin, true, false, false);
                            float bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);


                            p.setBestprice(bestprice);
                            p.setAsin(asin);

                        }
                        p.setItem_Code(sku);
                        p.setWHOLESALERNAME(WHOLESALERNAME);
                         pw.storeProduct3(p,  AMAZON_TABLE_NAME);
                        
                    }
                }
                 ar=pw.getProductsFromTable(p, con,AMAZON_TABLE_NAME);
                 float AMAZON_DISCOUNT=(float)0;
                for(int i=0;i<ar.size();i++){
                    if(i>0){
                       // System.out.println("Exception. Check Prabu why "+AMAZON_TABLE_NAME+" has more than 1 row for asin "+asin + " for same wholeasler ");
                        continue;
                    }
                    p=ar.get(i);    
                    try {
                         if(p.getBestprice()==0){
                            System.out.println("//PRABU CONTINUE NOW IF BESTPRICE IS UNKNOWN. ROW EXISTS IN "+AMAZON_TABLE_NAME+" but price is 0. May be asin not valid in amazon");
                            p.setBestprice((float)1000);
                           // continue;
                        } 
                            

                        if(p.getBestprice()==0){
                            QueryProduct qp=new QueryProduct();
                            QueryProduct.I_WANT_ORIGINAL_BESTPRICE_AT_NOOTHER_FBA_SELLERS=true;
                            p=qp.getProductListings1(asin);
                            p.setWHOLESALERNAME(WHOLESALERNAME);
                            pw.storeProduct3(p,   AMAZON_TABLE_NAME);
                        }
                    } catch(Exception exception) {
                        if(exception.getMessage().contains("ASIN_NOT_FOUND")) {
                            System.out.println("ASIN_NOT_FOUND");
                        } else {
                            System.out.println("Unusual Exception thwoing it. ");
                            throw exception;
                        }
                    }
                    
                Float bestprice=p.getBestprice();     
               
    }
     return p; 
   
    }
}
