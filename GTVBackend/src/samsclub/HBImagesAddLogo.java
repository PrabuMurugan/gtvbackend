/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;

/**
 *
 * @author prabu
 */

import java.awt.image.BufferedImage;
import unfi.*;
 
import bombayplaza.pricematch.RetrievePage; 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import net.sf.json.JSONObject;
import bombayplaza.pricematch.MatchThePrice;
import java.util.HashMap;
import org.apache.commons.lang.StringUtils;
import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import amazon.product.QueryProduct;
import com.tajplaza.products.ListMatchingProductsSample;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.imageio.ImageIO;
import net.sf.json.JSONArray;

import bombayplaza.pricematch.RetrievePage;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
 
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import org.apache.commons.lang.StringUtils;

public class HBImagesAddLogo {
    private static BufferedImage image;
      
    public static void main(String args[]) throws Exception{
     // Product  p = getUPCDetails("B00H9H56QA");
     //console(p);
       if(true){
           System.out.println("Due to google violation, stopping to add logo");
           return;
       }
     updatePrices();
     System.out.println("Program completed");

 } 
 
//   public static String DEBUG_SQL=" and p.code='CO_311_181679' ";

 
   public static String DEBUG_SKU="";
 
 public static String AMAZON_TABLE_NAME="amazon_catalog_hb";
    public static String ORIG_IMAGE_FOLDER="C:\\Temp2012\\images\\";
   public static String OVERLAYED_IMAGE_FOLDER="C:\\apache-tomcat-7-prod\\webapps\\HBImages\\";
 public static Float ABSOLUTE_MIN_MARGIN=(float)0;
 public static Float MIN_MARGIN=(float)0;
    public static void updatePrices() throws Exception{
            Connection con=RetrievePage.getConnection();
            PreparedStatement pst = null,pst2=null;
            ResultSet rs = null,rs2=null;
             String sort_order="";
             sort_order=String.valueOf(System.getProperty("sort_order",""));
             String PRODUCTION =String.valueOf(System.getProperty("PRODUCTION",""));
             if(PRODUCTION.equals("true"))
                 DEBUG_SKU="";
             
             
            
          
             
             
             String sql=
                     //"select item_code sku,c.price reg_cost,p.price current_price,p.product_id,c.asin from hb_bc.hb_catalog c, hb_bc.exported_products_from_hb p where c.item_code=p.code    and stock_level>0  "+
                       // " union "+
                      //  " select sku sku,oc  reg_cost,p.price current_price,p.product_id,c.asin,category_string,category_details,packs_in_1_case,p.name,weight,ui.size,p.product_upc_ean,stock_level from hb_bc.original_cost c, hb_bc.exported_products_from_hb p left join  hb_bc.unfi_input_catalog ui on   replace(p.code,'UN_','') = ui.item_code    where     c.sku=p.code  and p.code not like 'DT%' and c.oc is not null and cast(c.oc as decimal)>0    " ;
                    // " SELECT p.product_id,p.code,o.asin FROM hb_bc.exported_products_from_hb p left join hb_bc.original_cost o on p.code=o.sku where (length(product_images)=0 or product_images is null or product_images like '%no-img%' or product_images = '') and  stock_level>0  and Product_type = 'P'  ";
                      "select p.code,p.name,p.product_id,p.product_images, case when v.upc_pkg_qty>pkg_qty then upc_pkg_qty else pkg_qty end pack,'' size,'' Category_String  from hb_bc.exported_products_from_hb p, tajplaza.variation_sku_upcs v where p.code=v.sku and v.pkg_qty>1  and p.stock_level>0  "
                     + " and code not in (select code from hb_bc.added_multipack_images)  "
                     + " union  "
                     + "select p.code,p.name,p.product_id,p.product_images,ui.pack,ui.size,p.Category_String  from hb_bc.exported_products_from_hb p, hb_bc.unfi_input_catalog ui where p.code=concat('UN_',ui.item_code)  "
                     + "  and code not in (select code from hb_bc.added_multipack_images)    "
                     + " and p.code in (select id from harrisburgstore.gts_product_feed) and p.stock_level>0 limit 1000";
            System.out.println("Exceutung sql:"+sql);
            PreparedStatement psInsert=con.prepareStatement("insert into hb_bc.added_multipack_images (code,pack) values(?,?)");
            
            pst=con.prepareStatement(sql);
            rs=pst.executeQuery();
            int count=0;
            ProcessWorkingCatalog pw=new ProcessWorkingCatalog();
             String code="";
             int pack=0;
            while(rs.next()){ 
                try{
                    Integer product_id=rs.getInt("product_id");
                   code=rs.getString("code");
                    if(DEBUG_SKU.length()>0 && DEBUG_SKU.equals(code)==false)
                        continue;
                    System.out.println("Processing sku:"+code);
                     pack=rs.getInt("pack");
                    if(pack>1){
                        String size=rs.getString("size");
                        String catg_desc=rs.getString("Category_String");
                         String product_images=rs.getString("product_images");
                         if(product_images==null || product_images.length()<5)
                             continue;
                        URL url_image =new URL(product_images);
                        // read the url
                       try{
                           image = ImageIO.read(url_image);
                       }catch (Exception e2){
                           //e2.printStackTrace();
                        psInsert.setString(1, code);
                        psInsert.setInt(2, pack);
                        psInsert.executeUpdate();;                             
                           continue;
                       }

                        String image_file_name=StringUtils.substringAfterLast(product_images, "/") ;
                        image_file_name=URLDecoder.decode(image_file_name);
                        // for jpg
                        ImageIO.write(image, "jpg",new File(ORIG_IMAGE_FOLDER+image_file_name));
                       String newPath=drawPackLogoImage(image_file_name,pack,size,catg_desc);
                        psInsert.setString(1, code);
                        psInsert.setInt(2, pack);
                        psInsert.executeUpdate();;                        
                       if(newPath==null ||newPath.length()==0){
                           continue;
                       }
                       String imageurl="http://98.235.197.242/HBImages/"+image_file_name;
                        if(imageurl!=null && imageurl.length()>10 && imageurl.indexOf("51m1gdQJW2L")<0){
                        System.out.println("Uploading image for :"+code);
                          String uploadImage = CreateHBProduct.uploadImage(product_id,imageurl,true);

                        }                        
                    }
      
                    

                // BigCommerce.updateProduct(product_id, jSONObject);                


                }catch (Exception e){e.printStackTrace();}

                    psInsert.setString(1, code);
                    psInsert.setInt(2, pack);
                    psInsert.executeUpdate();;                    
             }
               
                 
                  
                 
                 
   }
    public static String drawPackLogoImage(String image_file_name, int pack, String size, String catg_desc){
        String newPath="";
        BufferedImage bgImage = null;
        
        bgImage=readImage(ORIG_IMAGE_FOLDER+image_file_name);
        int x=0,y=0;
      int clr=  bgImage.getRGB(x,y); 
      int  red   = (clr & 0x00ff0000) >> 16;
      int  green = (clr & 0x0000ff00) >> 8;
      int  blue  =  clr & 0x000000ff;
      System.out.println("Red Color value = "+ red);
      System.out.println("Green Color value = "+ green);
      System.out.println("Blue Color value = "+ blue);       
      if(red>250 && green<10 && blue<10){
          System.out.println("Image is already has red color Skipping");
          return newPath;
      }
        /**
         * Read a foreground image
         */
        
        if(size.trim().equals("#") || catg_desc.toUpperCase().indexOf("BULK")>=0)
            pack=1;
        
        BufferedImage fgImage = readImage("C:\\Google Drive\\Dropbox\\harrisburgstore\\Multipack logo\\"+pack+"_pack.png");
        if(fgImage==null){
            System.out.println(" MULTI PACK LOGO DOES NOT EXISTS FOR "+ pack +" AT C:\\Temp2012\\images\\multi pack logo\\"+pack+"_pack.png");
            return newPath;
        }
 
        /**
         * Do the overlay of foreground image on background image
         */
        BufferedImage overlayedImage = overlayImages(bgImage, fgImage);
 
        /**
         * Write the overlayed image back to file
         */
        if (overlayedImage != null){
            writeImage(overlayedImage, OVERLAYED_IMAGE_FOLDER+image_file_name, "JPG");
            new File(ORIG_IMAGE_FOLDER+image_file_name).delete();
            System.out.println("Overlay Completed...");
            newPath=OVERLAYED_IMAGE_FOLDER+image_file_name;
        }else
            System.out.println("Problem With Overlay...");        
        return newPath;
    }
    public static BufferedImage overlayImages(BufferedImage bgImage,
            BufferedImage fgImage) {
 
        /**
         * Doing some preliminary validations.
         * Foreground image height cannot be greater than background image height.
         * Foreground image width cannot be greater than background image width.
         *
         * returning a null value if such condition exists.
         */
      /*  if (fgImage.getHeight() > bgImage.getHeight()
                || fgImage.getWidth() > fgImage.getWidth()) {
            JOptionPane.showMessageDialog(null,
                    "Foreground Image Is Bigger In One or Both Dimensions"
                            + "\nCannot proceed with overlay."
                            + "\n\n Please use smaller Image for foreground");
            return null;
        }*/
         BufferedImage finalImg = new BufferedImage(bgImage.getWidth(), bgImage.getHeight()+100,bgImage.getType());
        // BufferedImage finalImg = new BufferedImage(bgImage.getWidth(), bgImage.getHeight(),bgImage.getType());
 
        /**Create a Graphics  from the background image**/
        Graphics2D g = finalImg.createGraphics();
        g.setColor(Color.WHITE);  
        g.fillRect(0, 0,bgImage.getWidth(), bgImage.getHeight()+100);  
 
        //g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, 
          //     RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        /**
         * Draw background image at location (0,0)
         * You can change the (x,y) value as required
         */
        //FIRST DRAW THE BACKGROUND IMAGE
        g.drawImage(bgImage, 0, 100, null);
 
        /**
         * Draw foreground image at location (0,0)
         * Change (x,y) value as required.
         */
        g.drawImage(fgImage, 0, 0,bgImage.getWidth(),100,null);
 
        g.dispose();
        return finalImg;
    }
 
    /**
     * This method reads an image from the file
     * @param fileLocation -- > eg. "C:/testImage.jpg"
     * @return BufferedImage of the file read
     */
    public static BufferedImage readImage(String fileLocation) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(fileLocation));
        } catch (IOException e) {
            if(fileLocation.indexOf("product")<0 && fileLocation.indexOf("Multipack")<0)
                    e.printStackTrace();
        }
        return img;
    }
 
    /**
     * This method writes a buffered image to a file
     * @param img -- > BufferedImage
     * @param fileLocation --> e.g. "C:/testImage.jpg"
     * @param extension --> e.g. "jpg","gif","png"
     */
    public static void writeImage(BufferedImage img, String fileLocation,
            String extension) {
        try {
            BufferedImage bi = img;
            File outputfile = new File(fileLocation);
            ImageIO.write(bi, extension, outputfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }    
}
