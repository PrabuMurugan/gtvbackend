/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub.onetime;

import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
 
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;
 
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import javax.net.ssl.HttpsURLConnection;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

/**
 *
 * @author prabu
 */
public class FixCateogiesWithCAPS {
    public static String AND_DEBUG_SKU_SQL=" and  code='UN_1689546'";
    public static HashMap<String,Float> shippedWeights=new HashMap();
     public static void main(String args[]) throws Exception{
 
             Connection con = RetrievePage.getConnection();
             
             System.out.println("Inside FixCategories  Processing 10000 rows");
             String sql=" select id,name from hb_bc.categories  " ;
                     //+ "  union "
                     //+ " select  p.product_id  , p.code  ,p.name, t.weight,p.weight from hb_bc.exported_products_from_hb p, tajplaza.un_full_case_asins  t where p.code=t.code and    p.weight like '%.99%' and t.weight !=0 ";
             System.out.println("Executing SQL:"+sql);
             PreparedStatement selectStmt = con.prepareStatement(sql);
            ResultSet rs=selectStmt.executeQuery();   
             while( rs.next()){
               
                 String id=rs.getString("id");
                 String name=rs.getString("name");
                 String fix_name=WordUtils.capitalizeFully(name);
                 if(name.equals(fix_name)==false){
                     updateCat(id,fix_name);
                 }
             }
                
             con.close();
             
             
     }   
     
   
      public static String updateCat(String id, String name) {
        String webPage = "https://www.harrisburgstore.com/api/v2/categories/"+id+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
            JSONObject jSONObject = new JSONObject();
                    
            
             jSONObject.put("name", name);
 
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }    
}
