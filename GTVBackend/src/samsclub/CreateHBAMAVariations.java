/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;
import amazon.product.ProcessWorkingCatalog;
import bigcommerce.FetchCategories;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetLowestPricedOffersForASINSample;
import com.tajplaza.products.ListMatchingProductsSample;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
/**
 *
 * @author prabu
 */
public class CreateHBAMAVariations {
     static ArrayList possibleAsinsTried=new ArrayList();
    public static void main(String args[]) throws Exception{
        String sql=" select distinct p.code,s2.possible_asin,p.name,s1.upc from tajplaza.possible_asins_for_shelf_items  s1, tajplaza.possible_asins_for_shelf_items  s2,hb_bc.ama_exported_products_from_hb p where s1.upc=s2.upc and   s1.possible_asin=substring_index(code,'_',1)  and s2.possible_asin not in ( select substring_index(code,'_',1) from hb_bc.ama_exported_products_from_hb where stock_level>0) and p.stock_level>0 and s2.tried_to_create_in_hb=false     ";
        Connection con=RetrievePage.getConnection();
        PreparedStatement ps=con.prepareStatement(sql);
        ResultSet rs=ps.executeQuery();
        String possible_asin=null;
       
        while(rs.next()){
            try{
                String existingCoode=rs.getString("code");
                String existingName=rs.getString("name");
                possible_asin=rs.getString("possible_asin");
                if(possible_asin.indexOf("(")>0){
                    if(possible_asin.toLowerCase().indexOf("pack")<0 || possible_asin.toLowerCase().indexOf("oz")>=0|| possible_asin.toLowerCase().indexOf("ounce")>=0){
                        System.out.println("Size does not contain pack info, skipping");
                        continue;
                    }
                    if(true){
                        System.out.println("Not bothering about variations now, let them being taken care manually");
                        continue;
                    }
                    
                    possible_asin=StringUtils.substringBefore(possible_asin, "(");
                    
                }
                    
                possible_asin=possible_asin.trim();
                if( existingCoode.indexOf("AMA")<0)
                    continue;
                System.out.println("existingCoode:"+existingCoode+", New possible asin:"+possible_asin);
                Float existOC=Float.valueOf(StringUtils.substringAfterLast(existingCoode, "_"));
                String packingInfo=StringUtils.substringBeforeLast(existingCoode, "_");
                //packingInfo=StringUtils.substringBeforeLast(packingInfo, "_");
                packingInfo=StringUtils.substringAfterLast(packingInfo, "_");
                if( packingInfo.indexOf("PACK")<0)
                    continue;
                if(possibleAsinsTried.contains(possible_asin))
                    continue;
                    
                int existingPkgQty=Integer.valueOf(packingInfo.replaceAll("PACK", ""));
                CreateCatalog.ERROR_MOVE_TO_NEXT_PAGE=true;
                HashMap hm=null;
                // hm=MatchThePrice.getProductDetails(possible_asin, true, true, true);
                 ArrayList<HashMap> matchingProducts = null;
                matchingProducts=ListMatchingProductsSample.getMatchingProducts(possible_asin);
                for(int i=0;i<matchingProducts.size();i++){
                    HashMap tmpMap=matchingProducts.get(i);
                    if(tmpMap.get("asin").equals(possible_asin)){
                        System.out.println("Matching asin found throgh MWS API");
                        hm=tmpMap;
                        //Reason for parsing through here is to see if there is an exisitng multi pack quantity exists.
                        
                    }
                } 
                if(hm==null)
                    continue;
                String title=(String)(hm.get("title")!=null?hm.get("title"):"");
                if(title.length()==0)
                    title=(String)(hm.get("Title")!=null?hm.get("Title"):"");
                
                
                
                int newPkgQty=ProcessWorkingCatalog.calculatePackingQntFromTitle(title);
                
                int leventhDistance=StringUtils.getLevenshteinDistance(existingName, title);
                System.out.println("Creating new product asin: "+ possible_asin+" : "+title +"newPkgQty:"+newPkgQty +",leventhDistance:"+leventhDistance);
                if(leventhDistance>existingName.length()*.9){
                    alreadyTriedInHB(possible_asin,con,"Not Created, Item name seems too different");
                    continue;
                }
                 //HashMap fbaMap=RetrievePage.getDetailsFromFbaCalculator(null,possible_asin, "100",true);
                 String weight=String.valueOf(hm.get("productInfoWeight")  );
                 if(weight.equals("null"))
                     weight="5.99";
                Float weightF=Float.valueOf(weight);
                Integer newOC=Math.round((existOC/existingPkgQty)*newPkgQty);
                Float lowPrice=(newOC*(float).8+4+(weightF*(float).4))/(float).85;
                HashMap hm2=GetCompetitivePricingForASINSample.getJustBBPriceandRankMWS(possible_asin );
                float bestprice=(Float)(hm2.get("bestprice")!=null?hm2.get("bestprice"):(float)0);
                
                if(bestprice<lowPrice || bestprice>lowPrice*1.75){
                     alreadyTriedInHB(possible_asin,con,"Not Created, Too low or too high price");
                     continue;
                }
                String newSKU=possible_asin+"_"+newPkgQty+"PACK_"+newOC;
                 CreateHBProduct.createHBProductFromAmazon(newSKU,possible_asin,hm);   
                 
                
            }catch (Exception e){e.printStackTrace();}
             alreadyTriedInHB(possible_asin,con,"Created in HB");

        }
        
    }
        public static void alreadyTriedInHB(String possible_asin,Connection con,String notes) throws Exception{
            possibleAsinsTried.add(possible_asin);
            PreparedStatement ps=con.prepareStatement("update tajplaza.possible_asins_for_shelf_items set tried_to_create_in_hb=true,notes=? where possible_asin like ?");
            ps.setString(1,  notes);
            ps.setString(2,  possible_asin+"%");
            int rowsUpdated=ps.executeUpdate();
            System.out.println("Rows updated:"+rowsUpdated);
        }    
}
