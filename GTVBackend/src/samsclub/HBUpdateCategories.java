/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samsclub;


import amazon.product.ProcessWorkingCatalog;
import amazon.product.Product;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
 
//import com.tajplaza.products.GetProductCategoriesForASINSample;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;
 
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import com.tajplaza.products.GetProductCategoriesForASINSample;
import com.tajplaza.products.ListMatchingProductsSample;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import javax.net.ssl.HttpsURLConnection;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import unfi.BigCommerce;


/**
 *
 * @author prabu
 */ 
public class HBUpdateCategories {
    static String DEBUG_SQL="";
    // static String DEBUG_SQL="";
    static int MAX_AMAZON_CAT_LIMIT=3;
 public static String CAT_ID_BULK_AND_SAVE= "8569";
 //public static String CAT_ID_COSTCO= "4988";
 public static int CAT_ID_ONSALE= 8570;
 public static int CAT_ID_CATALOG= 7709;
 public static int CAT_ID_MISC_CATALOG= 7426;
 public static int CAT_ID_NATURAL_CATALOG= 1889;
     
    public static HashMap<String,String> missingUNFICategories=new HashMap();
    private static ArrayList allProducts;
    public static void main(String args[]) throws Exception{
        String PRODUCTION=System.getProperty("PRODUCTION","false");
         if(PRODUCTION.equals("true"))        
             DEBUG_SQL=" and p.code='SC_232407'";
        //updateUnfiMissingCategories();
        updateAmazonCategories();        
    }
    public static void updateUnfiMissingCategories() throws Exception{
        //String sql=" select  distinct p.code, u.catg_desc,c.id from hb_bc.exported_products_from_hb  p left join hb_bc.unfi_input_catalog u on replace(p.code,'UN_','')=u.item_code left join hb_bc.categories c on upper(c.name)=u.catg_desc where code like 'UN_%' and category_string not like '%Natural%'  and c.parent_id="+HBUpdateCategories.CAT_ID_NATURAL_CATALOG+"   and c.id is not null  limit 2000";
        //String sql="select p.product_id id,code,category_string,category_details,o.asin from hb_bc.exported_products_from_hb p , hb_bc.original_cost o where p.code=o.sku and  o.asin like 'B%' and ( p.category_string not like '%Catalog%'  or   p.category_string='Catalog/Misc'    )     limit 1000";
        String sql=" select  distinct p.code, u.catg_desc,c.id from hb_bc.exported_products_from_hb  p left join hb_bc.unfi_input_catalog u on replace(p.code,'UN_','')=u.item_code left join hb_bc.categories c on upper(c.name)=u.catg_desc where code like 'UN_%' and category_string not like '%Natural%'  and c.parent_id="+HBUpdateCategories.CAT_ID_NATURAL_CATALOG+"   and c.id is not null  limit 2000";
        System.out.println("Executing sql to update missing categories for unfi products "+sql);
        Connection con=RetrievePage.getConnection();
        PreparedStatement ps=con.prepareStatement(sql);
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
            missingUNFICategories.put(rs.getString("code"), rs.getString("id"));
                    
        }
    }
     
    public static void updateAmazonCategories() throws Exception{

         Connection con=RetrievePage.getConnection();
         String sql=" select p.product_id,code,category_string,category_details,o.asin,p.product_upc_ean from hb_bc.exported_products_from_hb p , hb_bc.original_cost o where p.code=o.sku and  o.asin like 'B%' and ( p.category_string not like '%Catalog%'  or   p.category_string='Catalog/Misc'    )  and stock_level>0    limit 1000 ";
         if(DEBUG_SQL.length()>0){
             
             sql = StringUtils.substringBefore(sql, "limit") + DEBUG_SQL;
         }
             
         System.out.println("Executing sql : "+sql);
         PreparedStatement ps=con.prepareStatement(sql);
         ResultSet rs=ps.executeQuery();
         while(rs.next()){
             String product_id=rs.getString("product_id");
             String category_string=rs.getString("category_string");
             String category_details=rs.getString("category_details");
             String product_upc_ean=rs.getString("product_upc_ean");
             String code=rs.getString("code");
             System.out.println("###############\nProcessing SKU: "+code);
             
             String asin=rs.getString("asin");
             MatchThePrice.JAVASCRIPTNONO=true;
             
            // HashMap hm=MatchThePrice.getProductDetails(asin, true, false, false);
             String category= null;
            // category=(String) hm.get("category");
            try{
                category=GetProductCategoriesForASINSample.getAmazonCategory(asin);
               // HashMap hm=ListMatchingProductsSample.getBestRankedProduct(product_upc_ean);
                //System.out.println("hm:"+hm.toString());
                }catch (Exception e2){e2.printStackTrace();
              }
             if(category==null || category.length()==0 ){
                 category="Everything Else";
             }
         category=category.replaceAll("&", " and ");
         category=category.trim();
         category=category.replaceAll("\\s+", " ");
             
             System.out.println("SKU:+"+code+"Updating to category : "+category);
               String subcategory="";
           //  String subcategory= (String)(hm.get("subcategory")!=null?hm.get("subcategory"):"NA") ;
              if(StringUtils.countMatches(subcategory, ">")==0)
                 subcategory="NA";
              subcategory=subcategory.replaceAll(">","/");
             category=category.replaceAll(">","/");
             category=category.replaceAll("›","/");
             category=category.replaceAll("\r", "").replaceAll("\n", "");
             if(subcategory!=null && subcategory.length()>5)
                 category=subcategory;
             JSONArray newCats=new JSONArray(); 
             String[] catsDetailsStr=StringUtils.split(category_details,"|");
             if(catsDetailsStr.length==0)
                 catsDetailsStr[0] =category_details;
            //String[] cats=StringUtils.substringsBetween(category_details,"Category ID:",",");              
             for (int i=0;i<catsDetailsStr.length;i++){
                 if(catsDetailsStr[i].toLowerCase().indexOf("catalog")>=0 && catsDetailsStr[i].toLowerCase().indexOf("natural")<0)
                     continue;
                 if(catsDetailsStr[i].toLowerCase().indexOf("misc")>=0)
                     continue;
                 
                 String cat=StringUtils.substringBetween(catsDetailsStr[i],"Category ID:",",");
                 newCats.add(cat.trim());
                 

             } 
             int id=-1;

             if(category.equals("NA")==false ){
                 
                  if(code.indexOf("_AMA")>0){
                        if(StringUtils.countMatches(category, "/")>1){
                            category=StringUtils.substringAfter(category, "/");
                        }
                       //id=createWholeCategoryPath(HBUpdateCategories.CAT_ID_ONSALE,category);
                        
                        id=HBUpdateCategories.CAT_ID_ONSALE;
                       /*if(id!=-1&&newCats.size()>0){
                          //Make sure we dont have on sale and on sale/sub categoryes..remove on sale
                           for(int ii=0;ii<newCats.size();ii++){
                               if(newCats.get(ii).equals(HBUpdateCategories.CAT_ID_ONSALE)==false){
                                    newCats.remove(ii)                      ;
                                   
                               }
                           }
                       }*/

                    //ENd of _AMA   
                  }else{
                          id=createWholeCategoryPath(HBUpdateCategories.CAT_ID_MISC_CATALOG,category);                 
                  }
                   if(newCats.contains(String.valueOf(id))==false)   {
                       newCats.add(String.valueOf(id));
                   }
                    
             }   
             
             if(missingUNFICategories.containsKey(code)){
                  newCats.add(missingUNFICategories.get(code));
             }
            
             if(id!=-1 || missingUNFICategories.containsKey(code)){
                  JSONObject jSONObject = new JSONObject();                 
                  jSONObject.put("categories", newCats);
                  try{
                  BigCommerce.updateProduct(product_id, jSONObject); 
                  }catch (Exception e2){e2.printStackTrace();}
                  
             }
             
             
         }
         con.close();        
    }
    public static int createWholeCategoryPath(Integer parentID,String categoryPath) throws Exception{
        int id=01;
          categoryPath = categoryPath.replaceAll("[^\\x00-\\x7F]", "");
        if(StringUtils.countMatches(categoryPath, "/")==0){
           id=createCategory(parentID,categoryPath);
        }else{
            String arr[]= StringUtils.split(categoryPath,"/");
             id=parentID;
            for(int i=0;i<arr.length;i++){
                
                if(i>=MAX_AMAZON_CAT_LIMIT)
                    break;
                 try{
                     id=createCategory(id,arr[i]);
                 }catch (Exception e2){
                     System.out.println("Not a big problem: Some catgorie sin breadcrump may not created properly "+e2.getMessage());
                 }
            }
           
            
        }
         return id;
    }
      public static Integer createCategory(Integer parentID,String category) throws Exception {
         category=category.replaceAll("&", " and ");
         category=category.trim();
         category=category.replaceAll("\\s+", " ");
        String webPage0 = "https://www.harrisburgstore.com/api/v2/categories";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        
        try {
            String str;
             int origQueryId=-1;
          try{
                int queryParentId=parentID;
               origQueryId=parentID;
                if(queryParentId==HBUpdateCategories.CAT_ID_MISC_CATALOG)
                    queryParentId=HBUpdateCategories.CAT_ID_CATALOG;
                if(queryParentId==-1)
                    queryParentId=HBUpdateCategories.CAT_ID_MISC_CATALOG;
                 String queryWebPage = "https://www.harrisburgstore.com/api/v2/categories.json?limit=250&parent_id="+queryParentId;
                 System.out.println("Querying 1 :"+queryWebPage);
                  str =getJSONString(queryWebPage);
                  int id=-1;
                  if( str.length() > 0 && JSONArray.fromObject(str).size()>0){
                       JSONArray jsarr=JSONArray.fromObject(str);
                       for(int i=0;i<jsarr.size();i++){
                           String current_name=jsarr.getJSONObject(i).getString("name").trim();
                          // System.out.println("current cat name from response:#"+current_name+"#,cat :#"+category+"#");
                           if(current_name!=null && current_name.equals(category)){
                               return jsarr.getJSONObject(i).getInt("id");
                           }

                       } 
                  }
            }catch (Exception e2){
                e2.printStackTrace();
            }

         try{
          //After trying inside catalog, now try inside MIS Catalog.
                       
          if(origQueryId==HBUpdateCategories.CAT_ID_MISC_CATALOG){
                           
               origQueryId=parentID;
                 String queryWebPage = "https://www.harrisburgstore.com/api/v2/categories.json?limit=250&parent_id="+HBUpdateCategories.CAT_ID_MISC_CATALOG;
                 System.out.println("Querying  2:"+queryWebPage);
                  str =getJSONString(queryWebPage);
                  int id=-1;
                  if( str.length() > 0 && JSONArray.fromObject(str).size()>0){
                       JSONArray jsarr=JSONArray.fromObject(str);
                       for(int i=0;i<jsarr.size();i++){
                           String current_name=jsarr.getJSONObject(i).getString("name").trim();
                          // System.out.println("current cat name from response:#"+current_name+"#,cat :#"+category+"#");
                           if(current_name!=null && current_name.equals(category)){
                               return jsarr.getJSONObject(i).getInt("id");
                           }

                       } 
                  }              
          }

            }catch (Exception e2){
                e2.printStackTrace();
            }        
         //Before creating the node, see if it exists at any node
            URL uri = new URL(webPage0);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("POST");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
            JSONObject jSONObject = new JSONObject();
 
            
            jSONObject.put("parent_id",parentID);
            jSONObject.put("is_visible",true);
           
            
             jSONObject.put("name",category);
             
                   
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
             out.flush();
            out.close(); 
          
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            //Thread.sleep(2000);
          try{
                int queryParentId=parentID;
                 String queryWebPage = "https://www.harrisburgstore.com/api/v2/categories.json?parent_id="+queryParentId;
                 System.out.println("Querying 1:"+queryWebPage);
                  str =getJSONString(queryWebPage);
                  int id=-1;
                  if( JSONArray.fromObject(str).size()>0){
                       JSONArray jsarr=JSONArray.fromObject(str);
                       for(int i=0;i<jsarr.size();i++){
                           String current_name=jsarr.getJSONObject(i).getString("name");
                           if(current_name!=null && current_name.equals(category)){
                               return jsarr.getJSONObject(i).getInt("id");
                           }

                       } 
                  }
            }catch (Exception e2){e2.printStackTrace();}
         
            System.out.println("Response : " + new String(sb1));
            
           
        } catch(Exception exception) {
 
              try{
                    int queryParentId=parentID;
                     String queryWebPage = "https://www.harrisburgstore.com/api/v2/categories.json?limit=250&parent_id="+queryParentId;
                     System.out.println("Querying 2:"+queryWebPage);
                      String str =getJSONString(queryWebPage);
                       int id=-1;
                      if( JSONArray.fromObject(str).size()>0){
                           JSONArray jsarr=JSONArray.fromObject(str);
                           for(int i=0;i<jsarr.size();i++){
                               String current_name=jsarr.getJSONObject(i).getString("name");
                               if(current_name!=null && current_name.equals(category)){
                                   return jsarr.getJSONObject(i).getInt("id");
                               }

                           } 
                      }
                }catch (Exception e2){e2.printStackTrace();}          
            exception.printStackTrace();
            
        }
         return  -1;
        
    } 
public static String getJSONString(String strAPI) throws Exception {
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
         
        String authString = username + ":" + token;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
      
        HttpURLConnection httpCon = (HttpURLConnection) (new URL(strAPI)).openConnection();
        httpCon.setRequestProperty("Content-Type", "application/json");
        httpCon.setRequestProperty("Accept", "application/json");
        httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
        httpCon.setDoOutput(true);
        httpCon.setDoInput(true);
        httpCon.setRequestMethod("GET");

        InputStream responseIS = httpCon.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
        String line ="";
        StringBuilder sb = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        return new String(sb);
    }       
}
