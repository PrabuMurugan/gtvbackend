/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Raj
 */
public class HBLogger {
    private static int prefix = 20;
    public static void debug(String module, String message){
        System.out.println("DEBUG [ " +(module.length() > prefix ? module.substring(0,20) : String.format("%20s",module))+ " ] : " + message);
    }
    public static void error(String module, String message, Throwable t){
        System.out.println("ERROR [ " +(module.length() > prefix ? module.substring(0,20) : String.format("%20s",module))+ " ] : " + message);
        t.printStackTrace();
    }
}
