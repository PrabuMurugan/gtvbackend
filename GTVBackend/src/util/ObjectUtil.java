/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.annotations.Column;
import util.annotations.ID;
import util.annotations.Table;

/**
 *
 * @author debasish
 */
public class ObjectUtil {
    
    public static <E> void saveFile(File file, Class<E> objectType, List<E> objectList, String header, String encoding) throws IOException{
        PrintWriter out = new PrintWriter(file, encoding);
        out.println(header);
        Field[] fields = objectType.getFields();
        Arrays.sort(fields, new ObjectUtil.FieldComparator());
        
        for(E obj:objectList){
            StringBuilder sb = new StringBuilder();
            for(Field f: fields){
                try {
                    Object value = f.get(obj);
                    sb.append(value==null?"":value.toString()).append("\t");
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(ObjectUtil.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(ObjectUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            sb.deleteCharAt(sb.length()-1);
            System.out.println("Date:"+ new Date() + "file content:"+sb.toString());
            out.println(sb.toString());
        }
        out.flush();
        out.close();
    }

    public static <E> List<E> loadFile(File file, Class<E> objectType, String encoding) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
        Field[] fields = objectType.getFields();
        Arrays.sort(fields, new ObjectUtil.FieldComparator());
        
        List<E> objectList = new ArrayList<E>();
        in.readLine();
        while (true) {
            try {
                String line = null;
                line = in.readLine();
                if (line == null) {
                    break;
                }
                String[] fieldValues = line.split("\\t");
                
                E object = objectType.newInstance();
                for (int i = 0; i < fieldValues.length; i++) {
                    String value = fieldValues[i];
                    Field field = fields[i];

                    
                    if (field.getType() == String.class) {
                        field.set(object, value);
                    } else if (field.getType() == Float.class) {
                        if (value.startsWith("$")) {
                            value = value.substring(1);
                        } else if (value.equals("NA") || value.trim().equals("") || value.equals("null")) {
                            value = null;
                        }
                        Float floatVal = null;
                        if (value != null && value.equalsIgnoreCase("infinite")) {
                            floatVal = Float.POSITIVE_INFINITY;
                        } else {
                            floatVal = (value != null) ? Float.valueOf(value) : null;
                        }
                        field.set(object, floatVal);

                    } else if (field.getType() == Integer.class) {
                        if (value.equals("NA") || value.trim().equals("")) {
                            value = null;
                        }
                        field.set(object, value != null ? Integer.valueOf(value) : null);

                    } else {
                        throw new IllegalArgumentException("Unsupported type: " + field.getType());
                    }
                }
                objectList.add(object);
            } catch (InstantiationException ex) {
                ex.printStackTrace();
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            }
        }
        in.close();
        return objectList;
    }
    
    public static <E> void updateObjects(Class<E> objectType, List<E> objects){
        try {
            Connection con = DB.getConnection(null);
           
            
            for(E obj:objects){
                 PreparedStatement stmt = createSelectQuery(con, objectType, obj);
                 ResultSet rs = stmt.executeQuery();
                 System.out.println("Date:"+ new Date() + "obj:"+obj);
                 setObjectValues(rs, objectType, obj);
            }
        } catch (Exception ex) {
            Logger.getLogger(ObjectUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static class FieldComparator implements Comparator<Field> {

        @Override
        public int compare(Field o1, Field o2) {
            util.annotations.Field f1 = o1.getAnnotation(util.annotations.Field.class);
            util.annotations.Field f2 = o2.getAnnotation(util.annotations.Field.class);
            if (f1 == null || f2 == null) {
                return o1.getName().compareTo(o2.getName());
            }
            return f1.index() - f2.index();
        }
    }

    public static <E> Map<Column, Field> createColumnFieldMap(Class<E> objectType) {
        Field[] fields = objectType.getFields();
        Map<Column, Field> cfMap = new HashMap<Column, Field>();
        for (Field field : fields) {
            Column column = field.getAnnotation(Column.class);
            if (column != null) {
                cfMap.put(column, field);
            }
        }
        return cfMap;
    }

    public static <E> PreparedStatement createSelectQuery(Connection con, Class<E> objectType, Object obj) {
        try {
            Map<Column, Field> cfMap = createColumnFieldMap(objectType);
            StringBuilder sb = new StringBuilder("SELECT ");
            for (Column c : cfMap.keySet()) {
                sb.append(c.value()).append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
            String tableName = objectType.getAnnotation(Table.class).value();
            sb.append(" FROM ").append(tableName).append(" WHERE ");
            boolean first = true;
            for (Column c : cfMap.keySet()) {
                ID id = cfMap.get(c).getAnnotation(ID.class);
                if (id != null) {
                    if (!first) {
                        sb.append(" AND ");
                    }
                    sb.append(c.value()).append("= ?");
                    first = false;
                }
            }
            System.out.println(sb);
            PreparedStatement stmt = con.prepareStatement(sb.toString());
            int index = 0;
            for (Column c : cfMap.keySet()) {
                ID id = cfMap.get(c).getAnnotation(ID.class);
                if (id != null) {
                    index++;
                    Field field = cfMap.get(c);
                    if (field.getType() == String.class) {
                        stmt.setString(index, (String) field.get(obj));
                    } else if (field.getType() == Float.class) {
                        stmt.setFloat(index, (Float) field.get(obj));
                    } else if (field.getType() == Integer.class) {
                        stmt.setInt(index, (Integer) field.get(obj));
                    }

                }
            }
            return stmt;

        } catch (Exception ex) {
            Logger.getLogger(ObjectUtil.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public static <E> void setObjectValues(ResultSet rs, Class<E> objectType, E obj) throws SQLException {
        Map<Column, Field> cfMap = createColumnFieldMap(objectType);
        try {
            if(!rs.next()){
                return;
            }
            for (Column c : cfMap.keySet()) {
                Field f = cfMap.get(c);
                System.out.println(f);
                if (f.getType() == Float.class) {
                    Float value = rs.getFloat(c.value());
                    f.set(obj, value);
                }else if(f.getType() == Integer.class){
                    Integer value = rs.getInt(c.value());
                    f.set(obj, value);
                }else if(f.getType() == String.class){
                    String value = rs.getString(c.value());
                    f.set(obj, value);
                }
            }
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ObjectUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
