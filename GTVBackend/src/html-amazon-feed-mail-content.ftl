<html>
<body>
<table style="width: 640px; color: #333333; margin: 0 auto; border-collapse: collapse;">
<tbody>
<tr>
<td style="padding: 0 20px 20px 20px; vertical-align: top; font-size: 13px; line-height: 18px; font-family: Arial,sans-serif;">
        <p>Open and understand the inventory feed file that has to be uploaded to amazon. 
It has "a" for add/delete indicator for items that are to be added to amazon 
(items in shelf more than 60 days, and more than 10 quantity. Check the price and make 
sure they are current best price).Then "d" indicator means we do not have these items more 
than 10 in shelf any more. These items will be deleted with indicator "d".</p>

<p>
Note: Filter for $80 in the price column, go to amazon.com , get the current best selling price
and put that price before uploading</p>
</td>
</tr>
</tbody>
</table>
</body>
</html>