/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package downloadreport;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author debasish
 */
public class DownloadReport {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        int numberOfDays = 15;
        try{
            numberOfDays = Integer.parseInt(args[1]);
        }catch(Exception ex){
            System.out.println("Number of days not mentioned properly, using default 7 days");
        }
        
        DownloadUtils.login();
        Calendar weekBack = Calendar.getInstance();
        weekBack.setTimeInMillis(System.currentTimeMillis());
        weekBack.set(Calendar.DATE, weekBack.get(Calendar.DAY_OF_MONTH)-numberOfDays);
        InputStream content = DownloadUtils.downloadReport(new Date(weekBack.getTimeInMillis()), new Date());
        saveToDisk(content, "C:\\Google Drive\\Dropbox\\harrisburgstore\\orderreport.txt");        
        
    }
    
    private static void saveToDisk(InputStream in, String output) throws IOException{
        //Let's first figure out where to save
        File outputFile = new File(output);
       
        
        OutputStream out = new FileOutputStream(outputFile);
        int c;
        while((c = in.read())>=0){
            out.write(c);
        }
        out.flush();
        out.close();
    }
}
