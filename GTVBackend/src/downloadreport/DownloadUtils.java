/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package downloadreport;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;

import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

/**
 *
 * @author debasish
 */
public class DownloadUtils {

    private static HttpClient httpClient = null;
    private static CookieStore cookieStore = new BasicCookieStore();
    private static HttpContext httpContext = new BasicHttpContext();

    static {
        httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(
                new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
        schemeRegistry.register(
                new Scheme("https", 443, SSLSocketFactory.getSocketFactory()));

        PoolingClientConnectionManager cm = new PoolingClientConnectionManager(schemeRegistry);
        httpClient = new DefaultHttpClient(cm);

    }

    public static void login() throws IOException {

        HttpGet loginPageGet = new HttpGet("https://app4.shipstation.com/Login?ReturnUrl=%2f");
        HttpResponse firstResponse = httpClient.execute(loginPageGet, httpContext);
        System.out.println(firstResponse.getStatusLine());

        HttpResponse loginResponse = makePostRequest("https://app4.shipstation.com/Login/Go", new BasicNameValuePair("user", "mprabagar80"), new BasicNameValuePair("password", "dummy123"));
        InputStream respStream = loginResponse.getEntity().getContent();
        int c;
        while((c = respStream.read())>=0){
            System.out.printf("%c",c);
        }
        
       
    }
    
    public static InputStream downloadReport(Date startDate, Date endDate) throws UnsupportedEncodingException, IOException{
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        System.out.println(df.format(startDate));
        HttpResponse reportResponse = makePostRequest("https://app4.shipstation.com/reports/exporttext", new BasicNameValuePair("StartDate", df.format(startDate)), new BasicNameValuePair("EndDate", df.format(endDate)), new BasicNameValuePair("rpttype", "SS.Reporting.ShippingExtract, SS.Reporting"), new BasicNameValuePair("rptName", "OrderReport"));
        return reportResponse.getEntity().getContent();
    }
    
    private static HttpResponse makePostRequest(String url, NameValuePair ... parameters) throws UnsupportedEncodingException, IOException{
        HttpPost loginPost = new HttpPost(url);
        List<NameValuePair> loginParams = Arrays.asList(parameters);
        
        loginPost.addHeader(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
        loginPost.addHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"));
        loginPost.addHeader(new BasicHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0"));
        //loginPost.addHeader(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
        loginPost.setEntity(new UrlEncodedFormEntity(loginParams));
        
       
        InputStream reqStream = loginPost.getEntity().getContent();
        
        HttpResponse loginResponse = httpClient.execute(loginPost, httpContext);
        System.out.println(loginResponse.getStatusLine());
        
        return loginResponse;
    }
}
