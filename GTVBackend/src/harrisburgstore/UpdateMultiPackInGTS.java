/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package harrisburgstore;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raj
 */
public class UpdateMultiPackInGTS {
    private static String GET_ALL_ROWS = "SELECT ID FROM harrisburgstore.GTS_PRODUCT_FEED WHERE ID LIKE 'UN_%'";
    private static String UPDATE_ROW = "UPDATE harrisburgstore.GTS_PRODUCT_FEED SET MULTIPACK= (SELECT PACK FROM HB_BC.unfi_input_catalog WHERE concat('UN_',item_code) = ?)";
    public static void main(String [] args) {
        print("Started Program UpdateMultiPackInGTS at : " + new Date());
        Connection connection = null;
        PreparedStatement ps = null;
        PreparedStatement PS1 = null;
        ResultSet rs = null;
        try {
            
            connection = PriceChangerHB_NOTUSED.getConnection();
            ps = connection.prepareStatement(GET_ALL_ROWS);
            
            print("Going to fire query");
            rs = ps.executeQuery();
            print("Return From query");
            
            ArrayList <String> list = new ArrayList<String>();
            while(rs.next()) {
                list.add(rs.getString("ID"));
            }
            rs.close();
            ps.close();
            
            print("Number of records to be updated : " + list.size());
            int iRowCount = 1;
            PS1 = connection.prepareStatement(UPDATE_ROW);
            for(String id : list) {
                
                PS1.setString(1, id);
                print("Records Total/Updated/Last Update Result : " + list.size() + "/" + iRowCount++ + "/" + PS1.executeUpdate());
            }
            
        } catch (Exception ex) {
            print(ex.getMessage());
        } finally {
            
            try {
               
               if(PS1 != null)PS1.close();
               
                if(connection != null && !connection.isClosed()) connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UpdateMultiPackInGTS.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        print("Done Program UpdateMultiPackInGTS at : " + new Date());
    }
    
    public static void print(String msg) {
        System.out.println(msg);
    }
}
