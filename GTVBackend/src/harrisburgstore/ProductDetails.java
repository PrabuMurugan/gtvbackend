/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package harrisburgstore;

import java.beans.*;
import java.io.Serializable;

/**
 *
 * @author us083274
 */
public class ProductDetails {

    public int getHb_id() {
        return hb_id;
    }

    public void setHb_id(int hb_id) {
        this.hb_id = hb_id;
    }

    public String getHb_sku() {
        return hb_sku;
    }

    public void setHb_sku(String hb_sku) {
        this.hb_sku = hb_sku;
    }

    public String getHb_item_title() {
        return hb_item_title;
    }

    public void setHb_item_title(String hb_item_title) {
        this.hb_item_title = hb_item_title;
    }

    public String getAmazon_asin() {
        return amazon_asin;
    }

    public void setAmazon_asin(String amazon_asin) {
        this.amazon_asin = amazon_asin;
    }

    public String getAmazon_rank() {
        return amazon_rank;
    }

    public void setAmazon_rank(String amazon_rank) {
        this.amazon_rank = amazon_rank;
    }

    public String getOriginal_cost() {
        return original_cost;
    }

    public void setOriginal_cost(String original_cost) {
        this.original_cost = original_cost;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
     int hb_id  ;
    String hb_sku;
String hb_item_title;
String amazon_asin ;
String   amazon_rank;
String  original_cost;
String  weight;
}
