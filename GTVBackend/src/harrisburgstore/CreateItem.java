/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package harrisburgstore;

import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author us083274
 */
public class CreateItem {
    private static FileWriter fstream_out;
    private static BufferedWriter out;
 
    public static Float MY_PROFIT_PERCENT=(float)1;
        public static Float AMAZON_PRICE_DISCOUNT=(float).9;
 
    public static Float AMAZON_PRICE_DISCOUNT_FOR_CREATECATALOG=(float).8;
    public static Float SHIPPING_CHARGE_PER_POUNDNA=(float)1.5;

    public static boolean FOR_PICKUP_HB=true;
    public static int HB_PICKUP_PROFIT=2;
    /*
     * Run below query to prepare harrisburg_store_input from UNFI cases.
     *
     * 
     * 
     
create table unfi_catalog  as 
select   * from final_outputcatalog where upper(wholesaler_name) like '%UNFI%' and updation_date>(curdate() - interval 30 day)

* 
* *
*
drop table unfi_catalog_final;
create table unfi_catalog_final as 
select * from unfi_catalog where  
upc in 
(select upc   from unfi_catalog
group by upc
having count(*)=1
);
 
insert into  unfi_catalog_final  
select * from unfi_catalog  where   length(title) >1 and
(upc, length(title) )   in (
select upc ,min( length(title) )  from unfi_catalog
group by upc
having count(*)>1
);
 SET SQL_SAFE_UPDATES=0;

 delete  from unfi_catalog_final where 1_case_price +2> ( bestprice*packs_in_1_case/packing_qty);
 
delete from unfi_catalog_final where 
length(trim(item_code)) =0;

select asin,1 original_cost,concat('UPC:',upc) upc,   0 qty_to_add, packs_in_1_case, 1_case_price from unfi_catalog_final  order by cast(rank as decimal) limit 100000;
 
 * Save it as a tab delemiter file at C:\Google Drive\Dropbox\program\catalog\working
     * 
     */
    public static float getEstimatedShippingCharge(float wt){
        float charge=2;
        try{
              if (wt<1){
                charge=(float) 3;
            }    
             else if (wt<=2){
                charge=(float) 6;
            }else if( wt<=5){
                charge=(float) (wt*1.5);
            }else {
                charge=(float) (wt*1.5);
            }            
        }catch (Exception e){
            e.printStackTrace();
            
            
        }
        return charge;
    }
    public static String ADD_TO_EXISTING_INVENTORY;
    static Float  packs_in_1_case=(float)0;
    static Float   one_case_price=(float)0;     
      public static void main(String args[]){
            String PRODUCTION = System.getProperty("PRODUCTION", "false");
              ADD_TO_EXISTING_INVENTORY = System.getProperty("PRODUCTION", "false");
          try{
              System.out.println("PRODUCTION is "+PRODUCTION);
          String strLine0;
          if(new File("C:\\Google Drive\\Dropbox\\program\\catalog\\working\\harrisburg_store_input.txt").exists()==false){
              
            if(PRODUCTION.equals("true")||new File("C:\\Google Drive\\Dropbox\\program\\catalog\\working\\harrisburg_store_test.txt").exists()==false){
                System.out.println("PRODUCTION is set true and the input file does not exist..Return");
                      return;
            }else{
                            FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\program\\catalog\\working\\harrisburg_store_test.txt"), new File("C:\\Temp2012\\working\\harrisburg_store_input.txt"));
            }
                
              
          }else{
            FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\program\\catalog\\working\\harrisburg_store_input.txt"), new File("C:\\Temp2012\\working\\harrisburg_store_input.txt"));
            FileUtils.copyFile(new File("C:\\Google Drive\\Dropbox\\program\\catalog\\working\\harrisburg_store_input.txt"), new File("C:\\Google Drive\\Dropbox\\program\\catalog\\working\\harrisburg_store_processing.txt"));
              
          }
              
          System.out.println("File Exists..Lets start process harrisburg_store_input.txt");
         new File("C:\\Google Drive\\Dropbox\\program\\catalog\\working\\harrisburg_store_input.txt").delete();
          BufferedReader  br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Temp2012\\working\\harrisburg_store_input.txt")));
            fstream_out = new FileWriter("C:\\Temp2012\\working\\harrisburg_store_output.txt");
            out = new BufferedWriter(fstream_out);
            out.write("Handle,Title,Body (HTML),Vendor,Type,Tags,Published,Option1 Name,Option1 Value,Option2 Name,Option2 Value,Option3 Name,Option3 Value,Variant SKU,Variant Grams,Variant Inventory Tracker,Variant Inventory Qty,Variant Inventory Policy,Variant Fulfillment Service,Variant Price,Variant Compare At Price,Variant Requires Shipping,Variant Taxable,Variant Barcode,Image Src,originalCostOf1Asin,shippingCharge,bestprice,Harrisburg profit=bestprice*.80-original cost*"+MY_PROFIT_PERCENT+" + shipping charge   ");
           out.newLine();
           out.close();  
           int rowCount=0;
              while ((strLine0 = br0.readLine()) != null)   {
                  Connection con = null;
            try{
                    if(strLine0.split("\t").length==0)
                        continue;
                      String asin= strLine0.split("\t")[0];
                      String originalCostOf1Asin= strLine0.split("\t")[1];
                      String upc= strLine0.split("\t")[2];
                      String quant= "0";
                        packs_in_1_case=(float)0;
                          one_case_price=(float)0; 
                      
                      try{
                          quant=strLine0.split("\t")[3];
                          int tmp=Integer.valueOf(quant);
                      if(FOR_PICKUP_HB){
                          packs_in_1_case= Float.valueOf(strLine0.split("\t")[4]);
                          one_case_price= Float.valueOf(strLine0.split("\t")[5]);
                          one_case_price=one_case_price+HB_PICKUP_PROFIT;
                          one_case_price=(float)(Math.round((one_case_price)*100))/100;
                      }
                          
                      }catch (Exception e){}
                    
                      
                      upc=upc.replaceAll("UPC:","").replaceAll("-", "");
                      if(upc.indexOf("'")<0)
                          upc="'"+upc;
                      rowCount++;
                    if(asin.equals("asin"))
                        continue;
                    if(asin.indexOf("HB")==0)
                        asin=asin.substring(1);
                    String output=getOutputForHarrisburgStore(asin,originalCostOf1Asin,upc,quant);
                    if(output!=null){
                        fstream_out = new FileWriter("C:\\Temp2012\\working\\harrisburg_store_output.txt",true);
                        out = new BufferedWriter(fstream_out);
                        out.write(output);
                        out.newLine();
                        out.close();                  
                        
                    }
                    
                    System.out.println("Now lets create amazon feed");
                    String sku="H"+asin;
                    String suggestedprice="80";
                    String quantity=quant;
                    String current_quantity="0";
                     con=getConnection();
                    PreparedStatement pst = null;
                    ResultSet rs = null;
                     pst=con.prepareStatement("select price,quantity from tajplaza.existing_all_inventory where seller_sku=?");
                     pst.setString(1, sku);
                    rs=pst.executeQuery();
                    
                    if(rs.next()){
                        suggestedprice=rs.getString(1);
                       current_quantity =rs.getString(2);
                    }
                    if(ADD_TO_EXISTING_INVENTORY.equals("false"))
                        current_quantity="0";
                     quantity=String.valueOf(Integer.valueOf(quantity)+Integer.valueOf(current_quantity));
                     
                    String str=sku+"	"+asin+"	1	"+suggestedprice+"	11	"+quantity+"	a							5	 	http://www.amazon.com/dp/"+asin;

                     FileWriter  fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\program\\catalog\\completed\\amazon_feed.txt");
                     BufferedWriter out0 = new BufferedWriter(fstream_out0);
                     out0.write("sku	product-id	product-id-type	price	item-condition	quantity	add-delete	will-ship-internationally	expedited-shipping	standard-plus	item-note	fulfillment-center-id	product-tax-code	leadtime-to-ship");
                     out0.newLine();
                     out0.write(str);
                     out0.close();                    
                
            }catch (Exception e0){
                e0.printStackTrace();
            } finally {
                if(con!= null &&! con.isClosed()) con.close();
            }
            
          }
          FileUtils.copyFile(new File("C:\\Temp2012\\working\\harrisburg_store_output.txt"), new File(("C:\\Google Drive\\Dropbox\\program\\catalog\\completed\\harrisburg_store_output.txt")));
            FileUtils.copyFile(new File("C:\\Temp2012\\working\\harrisburg_store_input.txt"), new File(("C:\\Google Drive\\Dropbox\\program\\catalog\\completed\\harrisburg_store_input.txt")));
            FileUtils.copyFile(new File("C:\\Temp2012\\working\\harrisburg_store_input.txt"), new File(("C:\\Google Drive\\Dropbox\\program\\catalog\\working\\harrisburg_store_processed.txt")));
            new File("C:\\Google Drive\\Dropbox\\program\\catalog\\working\\harrisburg_store_processing.txt").delete();;
      }catch (Exception e){
          e.printStackTrace();
            
      }
      }

    private static String getOutputForHarrisburgStore(String asin,String originalCostOf1Asin,String upc,String quant_placed_on_shelf) {
        StringBuffer out=new StringBuffer();
       MatchThePrice m=new MatchThePrice();
       m.JAVASCRIPTNONO=true;
       String handle="",title="",tmp_image_url="",qty="",shopify_desc="",manu="",category="",rank="",option3="",weightH="";
       String sku="H"+asin;
       float discountedAmzPrice=(float)80;
          Float shippingCharge=(float)0;
          float bestprice=(float)80;
          float weightF=(float)0;
          boolean existingItem=false;
       try{
             Connection con=getConnection();
             PreparedStatement pst = null;
             ResultSet rs = null;
                pst=con.prepareStatement("select handle,title,body_html,vendor,product_type,tags,option1,option2,option3,grams,inventory_quantity,price,image from products where sku=? ");
             pst.setString(1, sku);
             rs=pst.executeQuery();
               existingItem=false;
                if(rs.next()){
                         existingItem=true;
                         handle=rs.getString("handle");
                         title=rs.getString("title");
                          shopify_desc=rs.getString("body_html");
                         manu=rs.getString("vendor");
                         category=rs.getString("product_type");
                         rank=rs.getString("option2");
                         option3=rs.getString("option3");
                         weightH=rs.getString("grams");
                         qty=rs.getString("inventory_quantity");
                         discountedAmzPrice=rs.getFloat("price");
                         tmp_image_url=rs.getString("image");
                          
                           
                         
                       
                         
                         //ShopifyAPIDAO2.updateProduct(sku, quant_placed_on_shelf,true, null);
                         //return handle+","+" FOR THIS EXISTING ITEM,  QUANTITY IS INCREASED BY "+String.valueOf(quant_placed_on_shelf) +" FOR SKU "+sku +" THIS ROW IS INFORMATION PURPOSE ONLY AND THIS UPDATE IS ALREADY IN HBSTORE. SO JUST DELETE THIS ROW AND SAVE THE FILE AS CSV AND UPLOAD THE FILE TO HBSTORE ADMIN" ;
                        
                }
                

            con.close();    
         }catch (Exception e){
             e.printStackTrace();
         }
       if(true)
       //if(handle==null || handle.length()==0)
       {
           if(one_case_price>0){
               m.JAVASCRIPTNONO=true;
               //I do not want price information for creating cases in pikcup store.
           }
               
           
                HashMap hm0= m.getProductDetails(asin, true, false, false);

                    title=(String)(hm0.get("title")!=null?hm0.get("title"):"NA");
                 /* if(title.length()>65){
                      title=StringUtils.substring(title, 0,67)+"...";
                  }*/

                    manu=(String)(hm0.get("manu")!=null?hm0.get("manu"):"NA");
                    category=(String)(hm0.get("category")!=null?hm0.get("category"):"NA");
                    rank=(String)(hm0.get("rank")!=null?hm0.get("rank"):"NA");
                     bestprice=(Float)(hm0.get("bestprice")!=null?hm0.get("bestprice"):(float)0);
                    if(manu.trim().equals("Details") ||manu.trim().equals("NA") )
                        manu="Miscellaneous";
                  //Lets use the same output catalog for harrisburgstore also
                  String image_url= (String)(hm0.get("imageurl")!=null?hm0.get("imageurl"):"") ;
                  
                    tmp_image_url=image_url;
                    /*
                  if(image_url.length()>0){
                  image_url=image_url.replaceAll("SY300", "SY1500");
                  String decodedFileName=URLDecoder.decode(StringUtils.substringAfterLast(image_url, "/").replaceAll(",", ""));
                  tmp_image_url="http://localhost/images/"+decodedFileName;

                  if( false &&  image_url.length()>0){
                          try{
                              URL url = new URL(image_url);
                             InputStream in = new BufferedInputStream(url.openStream());
                             ByteArrayOutputStream out2 = new ByteArrayOutputStream();
                             byte[] buf = new byte[1024];
                             int n = 0;
                             while (-1!=(n=in.read(buf)))
                             {
                                out2.write(buf, 0, n);
                             }
                             out2.close();
                             in.close();
                             byte[] response = out2.toByteArray();     

                          FileOutputStream fos = new FileOutputStream("C:\\apache-tomcat-6.0.37-windows-x64\\apache-tomcat-6.0.37\\webapps\\ROOT\\images\\"+decodedFileName);
                              fos.write(response);
                              fos.close();

                      }catch (Exception e2){
                          e2.printStackTrace();;
                      }
                     }              
                  }*/


                    shopify_desc="";
                  String tmp= (String)(hm0.get("des")!=null?hm0.get("des"):title) ;
                  shopify_desc="<p>"+tmp+"</p>";
                  tmp= (String)(hm0.get("bb1")!=null?hm0.get("bb1"):"") ;
                  if(tmp.length()>0){
                      shopify_desc+="<p><strong>Features:</strong></p><ul><li> "+tmp+" </li>";
                      shopify_desc=shopify_desc.replaceAll(","," ");
                  }

                  tmp= (String)(hm0.get("bb2")!=null?hm0.get("bb2"):"") ;
                  if(tmp.length()>0){
                      shopify_desc+="<li> "+tmp+" </li>";
                  }
                  tmp= (String)(hm0.get("bb3")!=null?hm0.get("bb3"):"") ;
                  if(tmp.length()>0){
                      shopify_desc+="<li> "+tmp+" </li>";
                  }
                  tmp= (String)(hm0.get("bb4")!=null?hm0.get("bb4"):"") ;
                  if(tmp.length()>0){
                      shopify_desc+="<li> "+tmp+" </li>";
                  }
                   shopify_desc+="</ul>";
                   shopify_desc=shopify_desc.replaceAll("\r","").replaceAll("\n", "").replaceAll("“", "").replaceAll("”","");
                   if(one_case_price>0){
                       float one_pack_price= (float)(Math.round((one_case_price/packs_in_1_case)*100))/100;
                       shopify_desc="<span id='case_details'><ul><li><b style='line-height: 1.5;'><b>Individual packs in a case : "+new Float(packs_in_1_case).intValue() +"</b></b></li><li><b style='line-height: 1.5;'><b>One pack price :  ($"+one_case_price+"/"+ new Float(packs_in_1_case).intValue() +" = $"+one_pack_price+")</b></b></li></ul></span> <span id='compare_details'> <b>Compare At</b> <br /> <br /><a  target='_blank' href='http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&amp;field-keywords="+StringUtils.substring(upc, -12) +"'><img height='30px' src='http://cdn.shopify.com/s/files/1/0311/6513/files/download.jpg?151' /></a>      <a target='_blank' href='http://www.walmart.com/search/search-ng.do?ic=16_0&amp;Find=Find&amp;search_query="+StringUtils.substring(upc, -12) +"&amp;Find=Find&amp;search_constraint=0'><img height='30px' src='http://cdn.shopify.com/s/files/1/0311/6513/files/wmt_logo_2.JPG?151' /></a> <br /><br /> </span>"+shopify_desc;
                   }
                   //Lets create out
                     handle=StringUtils.substring(title, 0,100);
                   handle=handle.replaceAll("[^A-Za-z0-9]", "-").replaceAll("(-)\\1+", "$1");
                   if(handle.charAt(handle.length()-1)=='-')
                       handle=StringUtils.substringBeforeLast(handle, "-");
                   if(handle.charAt(0)=='-')
                       handle=StringUtils.substringAfter(handle, "-");

                   HashMap fbaMap=new HashMap();
                   if(one_case_price==0){
                       fbaMap=RetrievePage.getDetailsFromFbaCalculatorNewMWS(null,asin, String.valueOf(bestprice),true);
                  
                       
                   String  weight=String.valueOf(fbaMap.get("productInfoWeight")  );
          

                   handle=handle.trim();
                     qty="0";

                       weightF=Float.valueOf(weight);
                        weightH="453.59";
                      try{
                          float weightGrams=(float) (weightF*453.59);
                          weightGrams= Math.round(weightGrams*(float)100)/(float)100;
                          weightH=String.valueOf(weightGrams);
                      }catch (Exception e3){
                          e3.printStackTrace();
                      }                     

                     
                        shippingCharge=getEstimatedShippingCharge(weightF);
                }

                        discountedAmzPrice=Math.round(bestprice*AMAZON_PRICE_DISCOUNT*(float)100)/(float)100;
                        if(false && originalCostOf1Asin!=null && originalCostOf1Asin.length()>0 &&Float.valueOf(originalCostOf1Asin)>0){
                            //COMMENT THIS IF WE WANT TO TAKE PRICE FROM AMAZON ONLY.
                            discountedAmzPrice=Float.valueOf(originalCostOf1Asin)*MY_PROFIT_PERCENT+shippingCharge; 
                        }
                      // float mybasicrbeakevenprice=Float.valueOf(originalCostOf1Asin)*MY_PROFIT_PERCENT+shippingCharge; 
                      // mybasicrbeakevenprice=Math.round(mybasicrbeakevenprice *(float)100)/(float)100;
                      if(originalCostOf1Asin!=null && originalCostOf1Asin.length()>0 &&Float.valueOf(originalCostOf1Asin)>0 &&
                              discountedAmzPrice<Float.valueOf(originalCostOf1Asin)*MY_PROFIT_PERCENT+shippingCharge){
                               System.out.println("Item will go loss if when set to :"+discountedAmzPrice +" so setting 2% discount");
                                 discountedAmzPrice=(Float.valueOf(originalCostOf1Asin)*(float)CreateItem.MY_PROFIT_PERCENT)+shippingCharge;
                                 if(discountedAmzPrice>bestprice && bestprice>0){
                                         discountedAmzPrice=bestprice*(float).98;
                                    }
                                
                               //discountedAmzPrice=Float.valueOf(originalCostOf1Asin)*MY_PROFIT_PERCENT+shippingCharge;
                               discountedAmzPrice=Math.round(bestprice*.9*(float)100)/(float)100;
                               discountedAmzPrice=Math.round(discountedAmzPrice *(float)100)/(float)100;



                       } 
                      if(bestprice==0){
                         discountedAmzPrice= Float.valueOf(originalCostOf1Asin)*MY_PROFIT_PERCENT+shippingCharge;
                         discountedAmzPrice=Math.round(discountedAmzPrice *(float)100)/(float)100;
                      }  
                      if(  Float.valueOf(originalCostOf1Asin)==0){
                            discountedAmzPrice=Math.round(bestprice*.75*(float)100)/(float)100;
                             discountedAmzPrice=Math.round(discountedAmzPrice *(float)100)/(float)100;
                      }
                    if(StringUtils.endsWith(String.valueOf(discountedAmzPrice),".99")){
                        discountedAmzPrice=   (float) (discountedAmzPrice - .01);
                        discountedAmzPrice=Math.round(discountedAmzPrice*(float)100)/(float)100;
                     }
                      option3=originalCostOf1Asin.replaceAll(","," ")+"||"+weightF+"||"+"A"+asin.replaceAll("," , " ");
    }
        if(ADD_TO_EXISTING_INVENTORY.equals("false"))
                    qty="0";
         if(one_case_price>0)
             qty="999";
         else
            qty=String.valueOf(Integer.valueOf(qty)+Integer.valueOf(quant_placed_on_shelf));

         out.append(handle);
         out.append(",");
         
         out.append(title.replaceAll("," , " "));
         out.append(",");
 
         out.append(shopify_desc.replaceAll("," , " ").replaceAll("\t", " "));
         out.append(",");

         out.append(manu.replaceAll("," , " "));
         out.append(",");
         
         if(category.equals("NA")){
             if(manu.toLowerCase().indexOf("jewel")>=0){
                 category="Jewelry";
             }
         }
         out.append(category.replaceAll("," , " "));
         out.append(",");
         manu=manu.replaceAll(",", " ");
         String tag="\""+manu+"\"";
         if(title.toLowerCase().indexOf("pack of")>=0){
             tag="\""+manu+", pack\"";
         }
         out=new StringBuffer(out.toString().replaceAll("\"", " "));
          out.append(tag);
         out.append(",");
         ProductDetails p=new ProductDetails();
         p.setAmazon_asin(asin);
         p.setAmazon_rank(rank);
         p.setHb_id(0);
         p.setHb_item_title(title);
         p.setHb_sku(sku);
         p.setOriginal_cost(originalCostOf1Asin);
         p.setWeight(weightH);
         
         insertIntoProductDetails(p);
         out.append("TRUE");
         out.append(",");
         out.append("Title");
         out.append(",");
         out.append("Default Title" );
         out.append(",");      
         out.append("Rank");
         out.append(",");  
         out.append(rank.replaceAll("," , " "));
         out.append(","); 
         out.append("OC||WT||ASIN");
         out.append(",");
         out.append(option3);
         out.append(",");
        // String sku=getSku(asin);
         out.append(sku);
         out.append(",");

         out.append(weightH.replaceAll("," , " "));
         out.append(",shopify,"+qty+",deny,manual,"); 
           

            
            
      //  out.append(mybasicrbeakevenprice);
         if(one_case_price>0){
             //Prabu - for pickup at hb store
             
             discountedAmzPrice=one_case_price;
         }
        out.append(discountedAmzPrice);
         out.append(",");  
         
          
         out.append(",TRUE,FALSE,");
         out.append(upc+",");
         
         
         out.append(tmp_image_url);
        //if(suggestedprice<bestprice && suggestedprice<discountedAmzPrice)
            out=new StringBuffer(out.toString());
        //else
          //  return null;
              shippingCharge=getEstimatedShippingCharge(weightF);
            Float hb_profit=(discountedAmzPrice-Float.valueOf(originalCostOf1Asin)*MY_PROFIT_PERCENT-shippingCharge);
              hb_profit=Math.round(hb_profit*(float)100)/(float)100;
            if(existingItem==false)
                out.append(","+originalCostOf1Asin+","+shippingCharge+","+bestprice+","+ hb_profit);
            else
                out.append(",EXISTING ITEM, JUST UPDATING THE QUANTITY. CHEKC THE QUANTITY BEFORE UPLOAD");
       return out.toString();
    }

public static Connection getConnection() throws Exception{
       Connection con = null;
    PreparedStatement pst = null;
    ResultSet rs = null;
    String   url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/harrisburgstore";
    String user = "root";
    String password = "admin123";
 
        
          
           try{
 
            con = DriverManager.getConnection(url, user, password);
           }catch (Exception e){
                
                con = DriverManager.getConnection("jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/harrisburgstore", user, password);
                
               
           }
            return con;
}    

    private static String getSku_OLD(String asin) {
        String sku="H"+asin;
        try{
            Connection con=getConnection();
             PreparedStatement pst = null;
             ResultSet rs = null;
             pst=con.prepareStatement("select  sku  from tajplaza.existing_inventory_fba where asin=?  and afn_fulfillable_quantity>0  ");
             pst.setString(1, asin);
             //pst.setString(2, asin);
             rs=pst.executeQuery();
             
                if(rs.next()){
                        sku=rs.getString(1);
                }
                

        }catch (Exception e){
            e.printStackTrace();
        }
        
        return sku;
    }

    private static void insertIntoProductDetails(ProductDetails p) {
        try{
                    Connection con=getConnection();
                    PreparedStatement pst = null;
                    ResultSet rs = null;
                     pst=con.prepareStatement("delete from product_details where hb_sku=?");
                     pst.setString(1, p.getHb_sku());
                    pst.executeUpdate();

                     pst=con.prepareStatement("insert into product_details (hb_id, hb_sku, hb_item_title, amazon_asin, amazon_rank, original_cost) values (?,?,?,?,?,?)");
                     pst.setInt(1, p.getHb_id());
                     pst.setString(2, p.getHb_sku());
                     pst.setString(3, p.getHb_item_title());                     
                     pst.setString(4, p.getAmazon_asin());                     
                     pst.setString(5, p.getAmazon_rank());                     
                     pst.setString(6, p.getOriginal_cost());                     
                     
                    pst.executeUpdate();
                    con.close();
        }catch (Exception e){
            e.printStackTrace();
        }         
        
    }
}
