package harrisburgstore;

import bombayplaza.pricematch.MatchThePrice;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import mail.SendMail;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Abani
 */
public class ShelfIItemsToAmazon {

    private static String fileName = "C:/Google Drive/Dropbox/program/shelf_items_to_amazon.txt";

    public static void main(String[] args) {
        BufferedReader br001 = null;
        Connection con = null;
        FileInputStream fileInputStream = null;
        try {
            br001 = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\Google Drive\\Dropbox\\program\\sql\\web_sql.txt")));
            String s;
            con = getConnection();
            ArrayList<String> WEB_SQL = new ArrayList();

            while ((s = br001.readLine()) != null) {
                if (s.split("\t").length < 3) {
                    continue;
                }
                String sno = s.split("\t")[0];
                String display = s.split("\t")[1];
                String sql = s.split("\t")[5];
                System.out.println("display:" + display);
                if (sno.equals("59") || sno.equals("60")) {
                    WEB_SQL.add(sql);

                }
            }
            if (processQuery(WEB_SQL, con)) {
                Map<String, Object> rootMap = new HashMap<String, Object>();
                Set<String> toEmails = new HashSet<String>();
                toEmails.add(System.getProperty("EMAIL_1", "vi_himanshu@varuninfotech.com"));
                toEmails.add(System.getProperty("EMAIL_2", "a2020patel@gmail.com"));
                toEmails.add(System.getProperty("EMAIL_3", "mprabagar80@gmail.com"));
                

                Address[] address = new Address[toEmails.size()];
                int i = 0;
                for (String email : toEmails) {
                    address[i] = new InternetAddress(email);
                    i++;
                }

                MimeBodyPart attachment = new MimeBodyPart();
                File file = new File(fileName);
                byte[] fileData = new byte[(int) file.length()];
                fileInputStream = new FileInputStream(file);
                fileInputStream.read(fileData);
                fileInputStream.close();
                DataSource source = new FileDataSource(file);
                attachment.setDataHandler(new DataHandler(source));
                attachment.setFileName("shelf_items_to_amazon.txt");
                SendMail.sendMail("html-amazon-feed-mail-content.ftl", address, rootMap, "Harrisburgstore.com: Inventory feed file that has to be uploaded to amazon", attachment);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br001.close();
                con.close();
            } catch (Exception ex) {
                Logger.getLogger(ShelfIItemsToAmazon.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static Connection getConnection() throws Exception {
        Connection con = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        String url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/harrisburgstore";
        String user = "root";
        String password = "admin123";



        try {

            con = DriverManager.getConnection(url, user, password);
        } catch (Exception e) {

            con = DriverManager.getConnection("jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/harrisburgstore", user, password);


        }
        return con;
    }

    private static boolean processQuery(List<String> queries, Connection con) {
        PreparedStatement pst = null;
        ResultSet rs = null;
        FileWriter fos = null;
        PrintWriter dos = null;
        boolean haveContent = false;
        try {

            File file = new File(fileName);
            if (file.isFile()) {
                file.delete();
            }
            fos = new FileWriter(fileName);
            dos = new PrintWriter(fos);
            dos.println("sku\tproduct-id\tproduct-id-type\tprice\titem-condition\tquantity\tadd-delete\twill-ship-internationally\texpedited-shipping\tstandard-plus\titem-note\tfulfillment-center-id\tproduct-tax-code\tleadtime-to-ship");
            for (String sql : queries) {
                try {
                    System.out.println("Query:" + sql.replaceAll("\"", ""));
                    pst = con.prepareStatement(sql.replaceAll("\"", ""));
                    rs = pst.executeQuery();

                    while (rs.next()) {

                        boolean noError = true;
                        String price = rs.getString("price");
                        String sku = rs.getString(1);
                        String asin = sku.split("_")[0];
                        if(asin.startsWith("B") && asin.length()>9){
                            /*if (rs.getFloat("price") == 80.00) {
                                MatchThePrice m = new MatchThePrice();
                                try {
                                    HashMap hm = m.getProductDetails(rs.getString("product-id"), true, true, false);
                                    float bestprice = (Float) (hm.get("bestprice") != null ? hm.get("bestprice") : (float) 70);
                                    price = String.valueOf(bestprice);
                                   // price = "80.00";
                                } catch (Exception e) {
                                    noError = false;
                                }
                            }*/
                            if (noError) {
                                //System.out.println("row:"+rs.getString("sku") + "\t" + rs.getString("product-id") + "\t" + rs.getString("product-id-type") + "\t" + price + "\t" + rs.getString("item-condition") + "\t" + rs.getString("quantity") + "\t" + rs.getString("add-delete") + "\t" + rs.getString("will-ship-internationally") + "\t" + rs.getString("expedited-shipping") + "\t" + rs.getString("standard-plus") + "\t" + rs.getString("item-note") + "\t" + rs.getString("fulfillment-center-id") + "\t" + rs.getString("product-tax-code") + "\t" + rs.getString("leadtime-to-ship") + "\t" + rs.getString("title"));
                                dos.println(rs.getString("sku") + "\t" + rs.getString("product-id") + "\t" + rs.getString("product-id-type") + "\t" + price + "\t" + rs.getString("item-condition") + "\t" + rs.getString("quantity") + "\t" + rs.getString("add-delete") + "\t" + rs.getString("will-ship-internationally") + "\t" + rs.getString("expedited-shipping") + "\t" + rs.getString("standard-plus") + "\t" + rs.getString("item-note") + "\t" + rs.getString("fulfillment-center-id") + "\t" + rs.getString("product-tax-code") + "\t" + rs.getString("leadtime-to-ship") + "\t" + rs.getString("title"));
                                haveContent = true;
                            }
                        }

                    }
                } catch (SQLException ex) {
                    Logger.getLogger(ShelfIItemsToAmazon.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println("exception:" + ex.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                dos.close();
                fos.close();
            } catch (Exception ex) {
                Logger.getLogger(ShelfIItemsToAmazon.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return haveContent;

    }

    public static void copy(InputStream input,
            OutputStream output,
            int bufferSize)
            throws IOException {
        byte[] buf = new byte[bufferSize];
        int bytesRead = input.read(buf);
        while (bytesRead != -1) {
            output.write(buf, 0, bytesRead);
            bytesRead = input.read(buf);
        }
        output.flush();
    }
}
