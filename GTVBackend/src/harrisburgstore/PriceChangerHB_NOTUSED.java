/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package harrisburgstore;

import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author US083274
 */
public class PriceChangerHB_NOTUSED {
        private static String MODULE = "[ PriceChangerHB ] ";
       static String FILTER_ITEM=" and sku='HB003DN1WJY'";
       static Integer MAX_ITEMS_PRICE_TO_BE_CHANGED=100;
 
  //  static String FILTER_ITEM="";
    public static void main(String args[]){
        System.out.println(MODULE + "Inside Main method at : " + new Date());
        System.out.println(MODULE + "Number of arguments to program : "+ args.length);
        RetrievePage.USE_PROXIES=true;
        try{
            boolean  PRODUCTION=false;
            if(args.length>0 && args[0].equals("PRODUCTION"))
                PRODUCTION= true;
            if(PRODUCTION){
                FILTER_ITEM="";
        }             
             Connection con=getConnection();
             PreparedStatement pst = null;
             ResultSet rs = null;
              pst=con.prepareStatement("select p.id,sku,name,price,inventory_level,peachtree_gl_account ,upc,weight,is_free_shipping from hb_bc.product  p left join tajplaza.amazon_product a on a.asin=substring_index(peachtree_gl_account,'||',1)  where inventory_level>0  and brand_id!=104    and length(substring_index(peachtree_gl_account,'||',1))>=9  "+FILTER_ITEM +"  order by case when a.ts is null  or  cast(price as decimal)=80 then DATE_SUB(CURDATE(),INTERVAL 365 DAY) else a.ts end  ");
             
             rs=pst.executeQuery();
                FileWriter fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\harrisburgstore\\pricechanger_hb.txt");
                BufferedWriter out0 = new BufferedWriter(fstream_out0);
                out0.write("harrisburg sku\ttitle\tharrisburg price\tamazon price\toriginal cost\tweight\tSuggested Price\tSuggest Price Profit");
                out0.newLine();
                out0.close();     
                int count=0;
                while(rs.next()){
                    if(count++>MAX_ITEMS_PRICE_TO_BE_CHANGED){
                        System.out.println("MAX_ITEMS_PRICE_TO_BE_CHANGED level reached..Stopping for today");
                      //  break;
                    }
                    try{
                         String id=rs.getString("id");
                         String sku=rs.getString("sku");
                         String name=rs.getString("name");
                         Float my_price=rs.getFloat("price");
                         String qty=rs.getString("inventory_level");
                         boolean existing_is_free_shipping=rs.getBoolean("is_free_shipping");
                         
                         String upc=rs.getString("upc");
                         String peachtree_gl_account=rs.getString("peachtree_gl_account");
                         Float weight=rs.getFloat("weight");
                         
                         if(peachtree_gl_account == null || peachtree_gl_account.length() <2) {
                             System.out.println(MODULE+"This product do not have peachtree_gl_account. So continue.");
                             continue;
                         }
                         System.out.println(MODULE+"peachtree_gl_account : " + peachtree_gl_account);
                         String[] t=StringUtils.split(peachtree_gl_account,"||");
                         
                        String  asin=t[0];
                         Float oc=Float.valueOf(t[1]);
                         if(asin==null || asin.indexOf("B")<0){
                             System.out.println("Not checking price for SKU:"+sku+" ,title :"+name +" as I bother only aboout HB0 skus");
                             continue;
                        }
                        if(  StringUtils.endsWith(String.valueOf(my_price),".99")){
                          System.out.println("Trace0,SKU :"+sku +" not touching it since it has .99 maually adjusted price.");
                          continue;
                       }    
                        Date date = Calendar.getInstance().getTime();
                      DateFormat formatter = new SimpleDateFormat("E");
                      String today = formatter.format(date);

                    if( FILTER_ITEM.length()==0 && today.indexOf("Sun")<0 &&  Float.valueOf(my_price)!=80){
                      System.out.println("Trace02,SKU :"+sku +" not touching it since it has price as $80 and today is not Saturday");
                      continue;
                   }  
                         //String asin=StringUtils.substring(sku,1);
                         MatchThePrice m=new MatchThePrice();
                         HashMap hm=new HashMap();
                       //  hm=m.getProductListings(asin, hm);
                         
                         hm=m.getProductDetails(asin, false, false, true);
                        // hm=m.getProductListings(asin,hm);
                         
                         MatchThePrice.insertOrUpdate( asin, hm,m);
                         
           
                           
                         float bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
                         float secondbestprice=(Float)(hm.get("secondbestprice")!=null?hm.get("secondbestprice"):(float)0);
                          float orig_bestprice=bestprice;
                         /*if(bestprice==0){
                            
                             hm=m.getProductListings(asin,hm);
                                     
                         }*/
                         bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
                         if(bestprice ==0 && hm.containsKey("secondbestprice")  ){
                             bestprice=(Float)(hm.get("secondbestprice")!=null?hm.get("secondbestprice"):(float)0);
                         }
                          
                         float google_low_price=getGoogleLowPrice(con,upc);
                        
                         float orig_google_low_price=google_low_price;
                         if(orig_bestprice==0 ||( google_low_price<bestprice && google_low_price > bestprice*.7)){
                             System.out.println("Google price is lower than amazon price and the google price is :"+google_low_price +" and amazon price is :"+bestprice);
                             bestprice=google_low_price;
                         }
                         //float current_profit =(float)my_price-(Float.valueOf(oc)*(float)CreateItem.MY_PROFIT_PERCENT)-CreateItem.getEstimatedShippingCharge(Float.valueOf(wt));
                         float current_profit =(float)my_price-Float.valueOf(oc)-CreateItem.getEstimatedShippingCharge(Float.valueOf(weight));
                         System.out.println("Sku is "+sku +" bestprice is :"+bestprice +", my_price is :"+my_price +" and current profit is : "+current_profit);
                          float suggestedprice=(float)0;
                          float suggestedprofit=(float)0;
                          Boolean is_free_shipping=false;
                          //Just comment below if loop  and uncomment the line below to it. Then we will start to beat amazon price.
                         // if(true && Float.valueOf(oc)>0 &&Float.valueOf(wt)>0 ){
                        // if(  current_profit<-1|| ( my_price>bestprice*.98   )){
                          //       suggestedprice=(Float.valueOf(oc)*(float)CreateItem.MY_PROFIT_PERCENT)+CreateItem.getEstimatedShippingCharge(Float.valueOf(wt))+1;
                            
                                /* if(suggestedprice>bestprice*.98  && bestprice>0){
                                         suggestedprice=bestprice*(float).98;
                                    }*/
                            //      if(orig_google_low_price<bestprice&& orig_google_low_price > bestprice*.6 && suggestedprice>orig_google_low_price*.98  && orig_google_low_price>0){
                              //           suggestedprice=orig_google_low_price*(float).98;
                                //        System.out.println("Even if I go on loss for beating google price, I will beat: google_low_price:"+google_low_price+",suggestedprice:"+suggestedprice);                                         
                                  //  }  
                                 
                               // if(suggestedprice<bestprice*(float)CreateItem.AMAZON_PRICE_DISCOUNT){
                                //suggestedprice =bestprice*(float)CreateItem.AMAZON_PRICE_DISCOUNT;
                               //}                                 
 
                         //} else if(my_price<bestprice*(Float)CreateItem.AMAZON_PRICE_DISCOUNT){
                         //      suggestedprice =bestprice*(Float)CreateItem.AMAZON_PRICE_DISCOUNT;
                               
                         //  }
                              
                         /* if(suggestedprice >0 && Float.valueOf(oc)>0 && Float.valueOf(wt)>0 && current_profit>3){
                              System.out.println("Current profit is greater than 1. Reducing current price to :"+(suggestedprice-current_profit+3));
                              float suggestedprice2=(Float.valueOf(oc)*(float)CreateItem.MY_PROFIT_PERCENT)+CreateItem.getEstimatedShippingCharge(Float.valueOf(wt))+3;
                                
                          }*/
                            float total_value=  100;
                            try{
                            total_value=Float.valueOf(qty) * Float.valueOf(my_price) ;
                            if(total_value<50){
                                is_free_shipping=true;
                               System.out.println("Total value is less than $50. So enabling free shipping");
                                
                            }
                            }catch (Exception e2){e2.printStackTrace();}                               
                          suggestedprice =bestprice*(Float)CreateItem.AMAZON_PRICE_DISCOUNT;
                          if(bestprice==0){
                              System.out.println("bestprice is 0 in google and amazon: Setting the price from OC and weight.");
                              suggestedprice=Float.valueOf(oc)+ Float.valueOf(weight)*2+(float)1;
                          }
                         if(suggestedprice>0 && suggestedprice<50 && total_value>50){
                        
                                                       
                             
                             suggestedprice=(suggestedprice-(float)3.99);
                             //float lowPrice=Float.valueOf(oc)+ Float.valueOf(String.valueOf(Math.ceil( Double.valueOf(wt) )))+(float).5;
                             
                             

                             
                         }
                       // int weightI=(int)Math.ceil(weight);
                            // float shippingFees=weightI;
                             if(weight<1)
                                 weight=(float)1;
                              float shippingFees=weight*(float)1.5;

                             if(weight<10)
                                 shippingFees=weight*(float)2;
                              if(suggestedprice<5)
                                    shippingFees=weight*(float)1;                             
                             //if(shippingFees<1)
                               //  shippingFees=1;
                            float lowPrice=Float.valueOf(oc)+ Float.valueOf(shippingFees)+(float).5;
                            if(lowPrice>suggestedprice)
                            {
                                System.out.println(" Item will go loss at suggested_price :"+(suggestedprice) + " so setting to low possible price:"+lowPrice);
                                suggestedprice=lowPrice;
                            }  
                            if(suggestedprice>lowPrice+5){
                                System.out.println("Something is wrong. The  lowprice is :"+lowPrice +" , suggested price is :"+suggestedprice + " profit >$5. Check original cost, ASIN, google shopping URL and make sure all these 3 are correct.");
                                
                            }
                           /* if(orig_bestprice>0 && secondbestprice>0  && suggestedprice > orig_bestprice-1 && suggestedprice > secondbestprice-1  ){
                                System.out.println(" suggested price is greater than best price and second bestprice. suggestedprice:"+suggestedprice+ ", bestprice:"+ orig_bestprice +", second best price: "+secondbestprice + "setting suggested price to 5% discount from second best price :"+(secondbestprice*.95));
                                suggestedprice=(float) (secondbestprice*.95);
                                
                            }*/
                         suggestedprice=Math.round(suggestedprice*(float)100)/(float)100;
                         if(StringUtils.endsWith(String.valueOf(suggestedprice),".99")){
                             suggestedprice=   (float) (suggestedprice - .01);
                             suggestedprice=Math.round(suggestedprice*(float)100)/(float)100;
                         }                         
                            if( suggestedprice>0 && (Math.abs(suggestedprice-my_price)>.5 || existing_is_free_shipping!=is_free_shipping) )    {
                                System.out.println("Price for SKU:"+sku +" title:"+name +" to be changed. my price is:"+my_price +", current price on amazon is:"+bestprice +" SUGGESTED price:"+suggestedprice);                             
                                  suggestedprofit=(float)suggestedprice-(Float.valueOf(oc)*(float)CreateItem.MY_PROFIT_PERCENT)-CreateItem.getEstimatedShippingCharge(Float.valueOf(weight));
                                   suggestedprofit=Math.round(suggestedprofit*(float)100)/(float)100;
                                    //ShopifyAPIDAO2.updateProduct_NEW( con,sku, null,false,String.valueOf(suggestedprice));

                                   
                                  // bigcommerce.BigCommerce.updateProduct(id, sku, null,String.valueOf(suggestedprice),is_free_shipping);
                                  fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\harrisburgstore\\pricechanger_hb.txt",true);
                                  out0 = new BufferedWriter(fstream_out0);
                                 out0.write(sku+"\t"+name+"\t"+my_price+"\t"+bestprice+"\t"+oc+"\t"+weight+"\t"+suggestedprice+"\t"+suggestedprofit);
                                 out0.newLine();
                                 out0.close();
                                
                            }
                    }catch (Exception e3)       {e3.printStackTrace();}
                     
                        
                }
             String[] to={"mprabagar80@gmail.com", "vi_himanshu@varuninfotech.com"};
                String[] cc={};
                String[] bcc={};
             //System.out.println("Sending email with body:"+warningString.toString());
              String subject="Harrisburg Pricechanger";
               subject+=" TODAY IS SUNDAY";
                                   //  Mail.sendMail( "smtp.gmail.com","465","true",
                                     //   "true",true,"javax.net.ssl.SSLSocketFactory","false",to,cc,bcc,
                            //subject,"Go to C:\\Google Drive\\Dropbox\\harrisburgstore\\pricechanger_hb.txt. Change the price as given in harrisburg store<br> If the new SUGGESTED  price is giving loss, do not change the price.  But if we do not have the item, delete the items that cause loss ",false,false,false,false,"C:\\Google Drive\\Dropbox\\harrisburgstore\\pricechanger_hb.txt");
                                     
                
                
               if(!con.isClosed()) con.close();
                
         }catch (Exception e){
             e.printStackTrace();
         }  
        
      
        System.out.println(MODULE + "Leaving Main method at : " + new Date());
    }
public static Connection getConnection() throws Exception{
       Connection con = null;
    PreparedStatement pst = null;
    ResultSet rs = null;
    String   url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/harrisburgstore";
    String user = "root";
    String password = "admin123";
 
        
          
           try{
 
            con = DriverManager.getConnection(url, user, password);
           }catch (Exception e){
                
                con = DriverManager.getConnection("jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/harrisburgstore", user, password);
                
               
           }
            return con;
}    

    private static float getGoogleLowPrice(Connection con, String upc) throws Exception{
        float google_low_price=(float)0;
        System.out.println("Inside getGoogleLowPrice for upc:"+upc);
        PreparedStatement ps=con.prepareStatement("select min(low_price_other_than_us),top_price_on_google from google_shopping_product  where dt > (curdate() - interval 30  day) and lpad(replace(replace(replace(barcode,'\\'',''),'-',''),'UPC:',''),14,'0')=lpad(replace(replace(replace(?,'\\'',''),'-',''),'UPC:',''),14,'0')");
        ps.setString(1, upc);
        ResultSet rs=ps.executeQuery();
        if(rs.next()){
            google_low_price=rs.getFloat(1);
            if(google_low_price==0 && rs.getFloat(2)!=0){
                google_low_price=rs.getFloat(2);
            }
        }
                
        return google_low_price;
    }
    
}
