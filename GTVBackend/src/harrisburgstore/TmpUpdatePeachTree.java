/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package harrisburgstore;

import bombayplaza.pricematch.RetrievePage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;
import unfi.InsertIntoUNFICatalog;

/**
 *
 * @author us083274
 */
public class TmpUpdatePeachTree {
    
        public static void main(String[] args) throws Exception{
             Connection con=RetrievePage.getLocalConnection();
            PreparedStatement pst = null;
            PreparedStatement pst2 = null;
            PreparedStatement pst3 = null;
            ResultSet rs = null;
        String storeURL = "https://harrisburgstore.com";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
              
          String authString = username + ":" + token;
// update rank
           System.out.println("Updating peachtree in unfi_hb_product ");
            pst=con.prepareStatement("select sku,peachtree_gl_account from product where peachtree_gl_account like '%||%'");
             rs=pst.executeQuery();
             while(rs.next()){
                 String webPage = storeURL+"/api/v2/products.json?limit=200&sku="+rs.getString("sku");
                 String result = getResultJSONString(authString, webPage);
                 JSONArray jsarr=JSONArray.fromObject(result);
                 if(jsarr.size()==0 || jsarr.getJSONObject(0).containsKey("id")==false)
                     continue;
                 System.out.println("ID is"+jsarr.getJSONObject(0).getString("id"));
                 JSONObject jsobj=new JSONObject();
                 jsobj.put("peachtree_gl_account", rs.getString("peachtree_gl_account"));
                 InsertIntoUNFICatalog.updateProduct1(jsarr.getJSONObject(0).getString("id"), jsobj);
                 
             }

        }
        private static String getResultJSONString(String authString, String url) {
        try {
            if (url == null) {
                return null;
            }
            //System.out.println("Url trying to fetch is:" + url);
            URL uri = new URL(url.trim());
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);

            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setDoInput(true);
            httpCon.setRequestMethod("GET");

            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = "";
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            String restult = new String(sb);
            if (restult != null && restult.length() > 0) {
                return restult;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
            
}
