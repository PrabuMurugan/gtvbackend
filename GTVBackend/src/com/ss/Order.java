
package com.ss;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Order {

    @Expose
    private Integer orderId;
    @Expose
    private String orderNumber;
    @Expose
    private String orderKey;
    @Expose
    private String orderDate;
    @Expose
    private String paymentDate;
    @Expose
    private String orderStatus;
    @Expose
    private String customerUsername;
    @Expose
    private String customerEmail;

    /**
     * 
     * @return
     *     The orderId
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 
     * @param orderId
     *     The orderId
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 
     * @return
     *     The orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * 
     * @param orderNumber
     *     The orderNumber
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * 
     * @return
     *     The orderKey
     */
    public String getOrderKey() {
        return orderKey;
    }

    /**
     * 
     * @param orderKey
     *     The orderKey
     */
    public void setOrderKey(String orderKey) {
        this.orderKey = orderKey;
    }

    /**
     * 
     * @return
     *     The orderDate
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     * 
     * @param orderDate
     *     The orderDate
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * 
     * @return
     *     The paymentDate
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * 
     * @param paymentDate
     *     The paymentDate
     */
    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * 
     * @return
     *     The orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * 
     * @param orderStatus
     *     The orderStatus
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * 
     * @return
     *     The customerUsername
     */
    public String getCustomerUsername() {
        return customerUsername;
    }

    /**
     * 
     * @param customerUsername
     *     The customerUsername
     */
    public void setCustomerUsername(String customerUsername) {
        this.customerUsername = customerUsername;
    }

    /**
     * 
     * @return
     *     The customerEmail
     */
    public String getCustomerEmail() {
        return customerEmail;
    }

    /**
     * 
     * @param customerEmail
     *     The customerEmail
     */
    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

}
