/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.product;


import bombayplaza.catalogcreation.CamelCamelCamel;
import bombayplaza.pricematch.RetrievePage;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import net.sf.json.JSONObject;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author prabu
 */
public class ProcessWorkingCatalog {
    public Float LABOR_CHARGE=(float).5;
     static String MODULE="[ProcessWorkingCatalog] ";
     public static int MIN_RANK_NEEDED=90000;
     public static void main(String args[]) throws Exception{
        // calculatePackingQntFromTitle("Huggies Natural Care Fragrance Free Baby Wipes, 552 Total Wipes 184 Count (Pack of 3), Packaging Ma");
        // Product pr=new Product();
        // pr.setAsin("B000EO0XAE");
        // Connection con=RetrievePage.getConnection();
         //getProduct( pr, con,"amazon_catalog");
         //console(pr);
         //con.close();
        
         ProcessWorkingCatalog p = new ProcessWorkingCatalog();
            p.processWorkingCatalogToCatalogTable();
     }
     public  void processWorkingCatalogToCatalogTable() throws Exception{
        String PRODUCTION=System.getProperty("PRODUCTION","false");
        if(PRODUCTION.equals("true"))
            CreateCatalogNew.DEBUG_SQL="";
         
         String and_condition=" ";
         String INPUT_CATALOG="amazon_input_catalog";
         /* if(CreateCatalogNew.RUN_FOR_PRODUCTS_WE_SELL ){
              // and_condition=CreateCatalogNew.ASINS_WE_SELL_SOLD_SQL;
               INPUT_CATALOG="amazon_input_catalog_wesell";
          }
          if(CreateCatalogNew.DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS ){
              // and_condition=CreateCatalogNew.ASINS_WE_SELL_SOLD_SQL;
               INPUT_CATALOG="amazon_catalog";
          }
          if(CreateCatalogNew.DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS){
              CreateCatalogNew.DEBUG_SQL=" and ci.ts<date_sub(curdate(), interval 30 day)    and cw.ts>=date_sub(curdate(), interval 7 day) ";
          }
          if(CreateCatalogNew.RUN_FOR_PRODUCTS_WE_SELL_ONLY_MISSSING_ROWS){
              CreateCatalogNew.DEBUG_SQL=" and ci.firsttime_asin not in (select asin from tajplaza.amazon_catalog) ";
          }*/
          
        if(CreateCatalogNew.DEBUG_SQL.length()==0){
              if(CreateCatalogNew.RUN_FOR_PRODUCTS_WE_SELL==false ){
                 //System.out.println("Pass the and condition to query from amazon_catalog_working table to amazon_catalog table. Example : wholesalername='PB2'");
                // BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
                 // and_condition=buffer.readLine();
                  and_condition=" 1=1 ";
                  // if(and_condition==null || and_condition.toLowerCase().indexOf("wholesalername")<0){
                       //System.out.println("INVALID USAGE. You have to pass wholesalername like ci.wholesalername='DOT' and id>123. The second condition is optional but wholesalername is mandatory");
                       // System.exit(1);
                  //  }
                  // if(and_condition!=null && and_condition.indexOf("ci.")<0)
                   //    and_condition="ci."+and_condition;
                    if(and_condition!=null && and_condition.length()>0)
                        and_condition=" and "+and_condition;

              }else{
                 // and_condition=" and cw.wesell=true";
              }

            
        }else{
                and_condition=CreateCatalogNew.DEBUG_SQL;
            }
            Connection con=RetrievePage.getConnection();
            System.out.println("Inserting all NA asins into noprofit at once");
            
            
            PreparedStatement pst = null,pst2=null;
            String noprofit_sql="insert into   amazon_catalog_noprofit (upc_in,item_code,item_name,packs_in_1_case,1_case_price,1_pack_price,wholesalername,additional_column1,additional_column2,asin,upc,title,brand,star,nReviews,bestprice,ccc_amazon_sells_last_3_months,amazon_sells_any_variation,nootherfbaseller,soldby,myprice,numberoffbasellers,packing_qty_asin,original_cost_asin,fbafees,fbamargin,profit,profit_percentage,my_units,req_asins,cases_required,proposed_cases_price,units_sold_last_month,weight,wesell,secondbestprice,rank)  "+
            " select upc_in,item_code,item_name,packs_in_1_case,1_case_price,1_pack_price,wholesalername,additional_column1,additional_column2,asin,upc,title,brand,star,nReviews,bestprice,ccc_amazon_sells_last_3_months,amazon_sells_any_variation,nootherfbaseller,soldby,myprice,numberoffbasellers,packing_qty_asin,original_cost_asin,fbafees,fbamargin,profit,profit_percentage,my_units,req_asins,cases_required,proposed_cases_price,units_sold_last_month,weight,wesell,secondbestprice,rank   "+
            " from amazon_catalog_working where ( asin='NA'  or rank>"+MIN_RANK_NEEDED+")   and (wholesalername,upc ) not in (select wholesalername,upc from amazon_catalog_noprofit)  ";
            System.out.println("Exeucting noprofit_sql"+noprofit_sql);
            
            pst=con.prepareStatement(noprofit_sql);
            pst.executeUpdate();
        
            ResultSet rs = null,rs2=null;
            String sql_template="select   distinct ci.upc_in, ci.item_code,ci.item_name,ci.packs_in_1_case,ci.1_case_price, ci.1_pack_price,ci.wholesalername,ci.additional_column1, "
                    + " ci.additional_column2,cw.asin,cw.upc, cw.title,  cw.brand,cw.star,cw.nReviews,case when cw.bestprice=0 then 999 else cw.bestprice end bestprice,cw.ccc_amazon_sells_last_3_months,cw.amazon_sells_any_variation,cw.nootherfbaseller,cw.soldby,cw.numberoffbasellers, "
                    + "cw.packing_qty_asin,cw.original_cost_asin,cw.fbafees,cw.fbamargin,cw.profit,cw.profit_percentage,cw.my_units,cw.req_asins,cw.cases_required,cw.proposed_cases_price, cw.units_sold_last_month,cw.weight,cw.rank,cw.productcategoryId"
                    + " from amazon_catalog_working  cw, "+INPUT_CATALOG+" ci  "
                 //   + "where  cw.upc=ci.upc_in  and cw.asin!='NA' and    (cw.wholesalername,cw.upc) not in ( select wholesalername,upc from amazon_catalog a where a.ts>cw.ts)  and rank<"+MIN_RANK_NEEDED+ and_condition ;
                       + "where  cw.upc=ci.upc_in  and cw.asin!='NA' and    (cw.wholesalername,cw.upc) not in ( select wholesalername,upc from amazon_catalog a where a.ts>cw.ts)  and rank<"+MIN_RANK_NEEDED+ and_condition ;
            
           // String no_profit_sql= sql_template + " and nReviews<4 and star<4 ";
            //String  profit_sql=sql_template + " and nReviews>=4 and star>=4 ";
            

            System.out.println("\n\n");
            System.out.println(MODULE+" Executing profit sql : "+sql_template);
           pst=con.prepareStatement(sql_template);
           rs=pst.executeQuery();
           while(rs.next()){
               Product p=new Product();
               p.setUpc_in(rs.getString("upc_in"));
               p.setItem_Code(rs.getString("item_code"));
               p.setPacks_in_1_Case(rs.getInt("packs_in_1_case"));
               p.setOne_case_price(rs.getFloat("1_case_price"));
               p.setOne_pack_price(rs.getFloat("1_pack_price"));
               p.setWHOLESALERNAME(rs.getString("wholesalername"));
               p.setAdditional_column1(rs.getString("additional_column1"));
               p.setAdditional_column2(rs.getString("additional_column2"));
               p.setAsin(rs.getString("asin"));
               p.setUpc(rs.getString("upc"));
               p.setTitle(rs.getString("title"));
               p.setBrand(rs.getString("brand"));
               p.setStar(rs.getFloat("star"));
               p.setnReviews(rs.getInt("nReviews"));
               p.setBestprice(rs.getFloat("bestprice"));
               p.setCcc_amazon_sells_last_3_months(rs.getBoolean("ccc_amazon_sells_last_3_months"));
               p.setAmazon_sells_any_variation(rs.getBoolean("amazon_sells_any_variation"));
               p.setNootherfbaSeller(rs.getBoolean("nootherfbaseller"));
               p.setSoldby(rs.getString("soldby"));
               p.setNumberOfFbaSellers(rs.getInt("numberoffbasellers"));
               p.setPkgQnt(rs.getInt("packing_qty_asin"));
               p.setOriginal_cost_asin(rs.getFloat("original_cost_asin"));
               p.setRank(rs.getInt("rank"));
              p.setWeight(rs.getFloat("weight"));
              p.setProductCategoryId(rs.getString("productcategoryId"));
               //Lets start with packing_qty
               int pkgQnt=calculatePkgQnt(p,con);
               p.setPkgQnt(pkgQnt);
               System.out.println(MODULE+" Processing upc:"+p.getUpc() +" asin:"+p.getAsin());
                       
               //if(p.getOne_pack_price()==null || p.getOne_pack_price()==0   ){
               if(p.getOne_case_price()>0 || p.getPacks_in_1_Case()>0   ){
                   //One pack price is not set, lets calculate
                   Float onePackPrice=p.getOne_case_price()/p.getPacks_in_1_Case();
                   p.setOne_pack_price(onePackPrice);
               }
               
               //Lets calculate original_cost_asin
               Float original_cost_asin=p.getOne_pack_price()*p.getPkgQnt();
               p.setOriginal_cost_asin(original_cost_asin);
               
               //Before going to FBA Calculator to calculte real FBA fees, lets see if current best price gives any profit assuminng minimum FBAFees of 3.5
               Float fbaFeesDefault=(float)3.5;
               Float fbamarginDefault=p.getBestprice() *(float).85  - fbaFeesDefault;
               Float profitDefault=fbamarginDefault -p.getOriginal_cost_asin()  ;
               if( CreateCatalogNew.RUN_FOR_PRODUCTS_WE_SELL==false && profitDefault<0){
                   System.out.println("Product can not sustain even at minimum FBA fees, no need to calculate real weight. ASIN"+p.getAsin() );
                   p.setFbaFees(fbaFeesDefault);
                   p.setFbaMargin(fbamarginDefault);
                   p.setProfit(profitDefault);
                           
               }else{
                   
                   p=calculateProfitForCurrentPriceMFN(p);

                   
                   boolean amazonSellingInLast3Months=false;
                    if(false && p.getProfit()>0 && CreateCatalogNew.RUN_FOR_PRODUCTS_WE_SELL==false){
                        //chec if camelcamelcamel was visited just 1 month back
                          amazonSellingInLast3Months= getIfAmazonSellingInLast3MonthsFromDBOrCCCSite(p,con);

                    }
                   p.setCcc_amazon_sells_last_3_months(amazonSellingInLast3Months);;
                    Float profitPercent=(p.getProfit()/p.getOriginal_cost_asin())*100;
                    p.setProfit_percentage(profitPercent);

                    //Now lets get the items sold last month
                    int itemsSoldLastMonth=getItemsSoldLasthMonth(p,con);
                    p.setUnits_sold_last_month(itemsSoldLastMonth);
                    
                    //Now lets get the units in hand
                    int myUnits=getMyUnits(p,con);
                    p.setMy_units(myUnits);
                    
                    //lets calculate req units
                    int reqUnits=itemsSoldLastMonth*2-myUnits;
                    if(itemsSoldLastMonth>0 & p.getProfit()>0)
                        p.setReq_asins(reqUnits);
                    
                       
               }    
               
              
               if(CreateCatalogNew.RUN_FOR_PRODUCTS_WE_SELL==false && (
                       p.getnReviews()<CreateCatalogNew.MIN_REVIEWS_REQ || p.getStar()<CreateCatalogNew.MIN_STARS_REQ)
                       && ( p.getRank()==0 || p.getRank()>MIN_RANK_NEEDED)
                       //p.getProfit()<-3|| p.getStar()<CreateCatalogNew.MIN_STARS_REQ)
                       )
                storeProduct3(p,"amazon_catalog_noprofit");
               else
                  storeProduct3(p,"amazon_catalog");
               
           }
           con.close();
     }
     public   Product calculateProfitForCurrentPriceMFN(Product p){
            
           //Product sustained at minimum FBA fees. Lets fetch real weight and calculate profit.
         RetrievePage.USE_PROXIES=true;  
         if(p.getWeight()==0){
             //HashMap fbaMap=RetrievePage.getDetailsFromFbaCalculatorNewMWS(null,p.getAsin(), String.valueOf(p.getBestprice()),true);
             //String weight=String.valueOf(fbaMap.get("productInfoWeight")  );
             String weight="3";
             p.setWeight(Float.valueOf(weight));
         }
         
            Float mfnShippingFees=5+p.getWeight()*(float).5;
            Float mfnMargin=p.getBestprice()*(float).85-mfnShippingFees;
            p.setFbaMargin(Float.valueOf(mfnMargin));

            Float profit=p.getFbaMargin()-p.getOriginal_cost_asin()-LABOR_CHARGE;         
             
            p.setProfit(profit);
            Float profitPercent=(p.getProfit()/p.getOriginal_cost_asin())*100;
            p.setProfit_percentage(profitPercent);            
            return p;
     }     
     public   Product calculateProfitForCurrentPriceAFN(Product p){
            
           //Product sustained at minimum FBA fees. Lets fetch real weight and calculate profit.
         RetrievePage.USE_PROXIES=true;  
         HashMap fbaMap=RetrievePage.getDetailsFromFbaCalculatorNewMWS(null,p.getAsin(), String.valueOf(p.getBestprice()),true);

            String fbaFees=String.valueOf(fbaMap.get("fbaFees")  );
            if(fbaFees.equals("null"))
                fbaFees="5.99";
            String weight=String.valueOf(fbaMap.get("productInfoWeight")  );
            if(weight==null || weight.length()==0)    
                weight="0";
            Float fbaMargin=p.getBestprice()*(float).85-Float.valueOf(fbaFees);
            p.setFbaMargin(Float.valueOf(fbaMargin));
            if(Float.valueOf(fbaFees)==0)
                    fbaFees="3.99";
            p.setFbaFees(Float.valueOf(fbaFees));
            p.setWeight(Float.valueOf(weight));


            Float profit=p.getFbaMargin()-p.getOriginal_cost_asin()-LABOR_CHARGE;         
             
            p.setProfit(profit);
            Float profitPercent=(p.getProfit()/p.getOriginal_cost_asin())*100;
            p.setProfit_percentage(profitPercent);            
            return p;
     }
     public static boolean getIfAmazonSellingInLast3MonthsFromDBOrCCCSite(Product p,Connection con) throws Exception{
         Boolean amazonSellingInLast3Months=false;
         String sql="select ccc_amazon_sells_last_3_months from amazon_catalog where asin=? and  ts>(curdate() - interval 30 day) ";
        PreparedStatement pst=con.prepareStatement(sql);         
        pst.setString(1, p.getAsin());
        ResultSet rs=pst.executeQuery();
        if(rs.next()){
                amazonSellingInLast3Months=rs.getBoolean(1);
            }   else{
            amazonSellingInLast3Months= CamelCamelCamel.isAmazonSellingInLast3Month(p.getAsin());
        }      
         return amazonSellingInLast3Months;
         
     }     
    
     
     public static int getItemsSoldLasthMonth(Product p,Connection con) throws Exception{
         int soldLastMonth=0;
         String sql="select sum(quantityshipped) from tajplaza.orders  r	where  r.asin  =? and r.purchasedate >	(select date_sub(max(c.purchasedate), interval 30 day) from tajplaza.orders c where r.asin = c.asin)";
        PreparedStatement pst=con.prepareStatement(sql);         
        pst.setString(1, p.getAsin());
        ResultSet rs=pst.executeQuery();
        if(rs.next()){
                soldLastMonth=rs.getInt(1);
            }         
         return soldLastMonth;
         
     }
     public static int getMyUnits(Product p,Connection con) throws Exception{
         int soldLastMonth=0;
         String sql="select sum(afn_total_quantity) from tajplaza.existing_inventory_fba where asin=?";
        PreparedStatement pst=con.prepareStatement(sql);         
        pst.setString(1, p.getAsin());
        ResultSet rs=pst.executeQuery();
        if(rs.next()){
                soldLastMonth=rs.getInt(1);
            }         
         return soldLastMonth;
         
     }     
     public static int calculatePkgQnt(Product p,Connection con) throws Exception{
         int pkgQnt=1;
         /*if(p.getnReviews()<CreateCatalogNew.MIN_REVIEWS_REQ || p.getStar()<CreateCatalogNew.MIN_STARS_REQ){
             
             return calculatePackingQntFromTitle(p.getTitle());
         }*/
             
            //Fetching the packing qty if it exists in amazon_catalog table
            PreparedStatement pst=con.prepareStatement("select asin,packing_qty from tajplaza.final_packingqty where asin=?");
            pst.setString(1, p.getAsin());
            ResultSet rs=pst.executeQuery();
            if(rs.next() &&rs.getString("packing_qty")!=null &&  rs.getString("packing_qty").equals("NA")==false &&                rs.getInt("packing_qty")>1){
               pkgQnt=rs.getInt("packing_qty");
            }else{
                pkgQnt=calculatePackingQntFromTitle(p.getTitle());
            }
            pst.close();
            return pkgQnt;
         
     }
     static public ArrayList<String> wesellProducts=null;
     static  Connection con1=null;
     static boolean RETRIED_STORE=false;
     public static void storeProduct3(Product p,String table) throws Exception{
         try{
             storeProduct4(p,table);
         }catch (Exception e2){
             e2.printStackTrace();
                    Thread.sleep(10*1000);
                    try{con1.close();}catch (Exception e3){}
                    con1=null;
                  storeProduct4(p,table);
             }
     }
     public static void storeProduct4(Product p,String table) throws Exception{
       // if(con1==null)
         con1=RetrievePage.getConnection();
        if(wesellProducts==null){
            wesellProducts=new ArrayList();
             PreparedStatement pst0= con1.prepareStatement("select item_code,firsttime_asin from amazon_input_catalog_wesell ");
             ResultSet rs0=pst0.executeQuery();
             while(rs0.next()){
                 wesellProducts.add(rs0.getString(1)+"||"+rs0.getString(2));
             }
             pst0.close();
             rs0.close();
             
         }
        PreparedStatement pst=null;
        ResultSet rs=null;
        /*if(CreateCatalogNew.RUN_FOR_PRODUCTS_WE_SELL )
            p.setWesell(true);
        else
            p.setWesell(false);
         * */
        if(p.getAsin().length()>0 && wesellProducts.contains(p.getItem_Code()+"||"+p.getAsin())){
            p.setWesell(true);
        }else{
            p.setWesell(false);
        }
        
        
       // System.out.println(MODULE+" Inside storeProduct:asin:"+p.getAsin() +",upc:"+p.getUpc());
        if(p.getAsin().equals("B0047Z0PAO") )
            System.out.println("Debug B0047Z0PAO");
       /* Product tmpP=getProduct(p,con,table);
        if(tmpP.getOne_pack_price()<p.getOne_pack_price() && p.getWHOLESALERNAME().equals(tmpP.getWHOLESALERNAME())){
            System.out.println(" Product :"+p.getAsin() +" is already available from "+tmpP.getWHOLESALERNAME()+ " at better price");
            return;
        }*/
        if(p.getItem_Code()==null || p.getItem_Code().trim().length()==0){
            String tmpItemcode=""+System.currentTimeMillis();
            p.setItem_Code(tmpItemcode);
           //System.out.println(MODULE+" storeProduct-> Exception item_code is mandatory to store into storeProduct.EXITING");
            //System.exit(1);
        }        
        if(p.getWHOLESALERNAME()==null || p.getWHOLESALERNAME().trim().length()==0){
          // System.out.println(MODULE+" storeProduct-> wholesalername is mandatory to store into storeProduct.EXITING");
//            System.exit(1);
            p.setWHOLESALERNAME("NA");;
        }  
        if(p.getItem_Code() ==null || p.getItem_Code().trim().length()==0){
          // System.out.println(MODULE+" storeProduct-> wholesalername is mandatory to store into storeProduct.EXITING");
//            System.exit(1);
            p.setItem_Code("NA");;
        }        
         /*if(p.getAsin()!=null && p.getAsin().length()>0  && p.getAsin().equals("NA")==false){
            pst=con.prepareStatement(" delete from "+table + " where asin=? and wholesalername=?");
            pst.setString(1, p.getAsin());
            pst.setString(2, p.getWHOLESALERNAME());
            pst.executeUpdate();
        } */

         //if(CreateCatalogNew.RUN_FOR_PRODUCTS_WE_SELL==false || p.getAsin().length()==0 ){
       /* if(p.getItem_Code().length()>0 ){
            pst=con1.prepareStatement(" delete from "+table + " where item_code =? and wholesalername=? and asin=?");
            pst.setString(1, p.getItem_Code());
            pst.setString(2, p.getWHOLESALERNAME());
            pst.setString(3, p.getAsin());
            pst.executeUpdate();     
            if(table!=null  && table.equals("amazon_catalog")){
                pst=con1.prepareStatement(" delete from amazon_catalog_noprofit where item_code =? and wholesalername=? and asin=?");
                pst.setString(1, p.getItem_Code());
                pst.setString(2, p.getWHOLESALERNAME());
                pst.setString(3, p.getAsin());
                pst.executeUpdate();                  
            }
            if(table!=null  && table.equals("amazon_catalog_noprofit")){
                pst=con1.prepareStatement(" delete from amazon_catalog  where item_code =? and wholesalername=? and asin=?");
                pst.setString(1, p.getItem_Code());
                pst.setString(2, p.getWHOLESALERNAME());
                pst.setString(3, p.getAsin());
                pst.executeUpdate();                  
            }
             
         }*/
        if(p.getUpc().length()>0 && p.getAsin().length()>0){
            pst=con1.prepareStatement(" delete from "+table + " where upc =? and wholesalername=? and asin=? ");
            pst.setString(1, p.getUpc());
            pst.setString(2, p.getWHOLESALERNAME());
            pst.setString(3, p.getAsin());
            pst.executeUpdate();     
            if(table!=null  && table.equals("amazon_catalog")){
                pst=con1.prepareStatement(" delete from amazon_catalog where upc =? and wholesalername=?  and asin=?");
                pst.setString(1, p.getUpc());
                pst.setString(2, p.getWHOLESALERNAME());
                pst.setString(3, p.getAsin());
                pst.executeUpdate();                  
            }
            if(table!=null  && table.equals("amazon_catalog_noprofit")){
                pst=con1.prepareStatement(" delete from amazon_catalog  where upc =? and wholesalername=?  and asin=? ");
                pst.setString(1, p.getUpc());
                pst.setString(2, p.getWHOLESALERNAME());
                pst.setString(3, p.getAsin());
                pst.executeUpdate();                  
            }
             
         }         
        if(CreateCatalogNew.DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS){
            pst=con1.prepareStatement(" delete from "+table + " where item_code =? and wholesalername=?  and asin=? and ts<date_sub(curdate(), interval 30 day) ");
            pst.setString(1, p.getItem_Code());
            pst.setString(2, p.getWHOLESALERNAME());
            pst.setString(3, p.getAsin());
            pst.executeUpdate();         
            if(table!=null  && table.equals("amazon_catalog")){
                pst=con1.prepareStatement(" delete from amazon_catalog_noprofit where item_code =? and asin=? and wholesalername=? ");
                pst.setString(1, p.getItem_Code());
                pst.setString(2, p.getWHOLESALERNAME());
                pst.setString(3, p.getAsin());
                
                pst.executeUpdate();                  
            }
            if(table!=null  && table.equals("amazon_catalog_noprofit")){
                pst=con1.prepareStatement(" delete from amazon_catalog  where item_code =? and wholesalername=? and asin=? ");
                pst.setString(1, p.getItem_Code());
                pst.setString(2, p.getWHOLESALERNAME());
                pst.setString(3, p.getAsin());
                pst.executeUpdate();                  
            }            
        }
 
        
/*        if(p.getAsin()!=null && p.getAsin().length()>0  && p.getAsin().equals("NA")==false && p.getUpc().length()==0){
            pst=con.prepareStatement(" delete from "+table + " where item_code=? and wholesalername=?");
            pst.setString(1, p.getAsin());
            pst.setString(2, p.getWHOLESALERNAME());
            pst.executeUpdate();
        }else if (p.getUpc()!=null && p.getUpc().length()>0){
            pst=con.prepareStatement(" select tajplaza.cleanupc(?)  ");
            pst.setString(1,p.getUpc());
            rs=pst.executeQuery();
            rs.next();
            p.setUpc(rs.getString(1));
            
            pst=con.prepareStatement(" delete from "+table + " where tajplaza.cleanupc(upc) =? and wholesalername=?");
            pst.setString(1, p.getUpc());
            pst.setString(2, p.getWHOLESALERNAME());
            pst.executeUpdate();
            
        }else{
            System.out.println(MODULE+" storeProduct-> UPC or asin  is mandatory to store into storeProduct");
            System.exit(1);
            
        }*/
        System.out.println("storeProduct4:Inserting into table : table : "+table);
        if(p.getUpc()==null || p.getUpc().length()==0)
            p.setUpc(p.getAsin());
        String sql="insert into   "+table
                + "(WHOLESALERNAME,Item_Code,asin,upc,title,brand,star,nReviews,bestprice,soldby,ccc_amazon_sells_last_3_months,NUMBEROFFBASELLERS,NOOTHERFBASELLER,packing_qty_asin"
                + ",upc_in,item_name,packs_in_1_case,1_case_price,1_pack_price,additional_column1,additional_column2,amazon_sells_any_variation,"
                + " original_cost_asin,fbafees,fbamargin, profit,profit_percentage,my_units,req_asins,cases_required,proposed_cases_price,"
                +" units_sold_last_month,weight,myprice,wesell,secondbestprice,rank,productcategoryid "
                + ") values "
                + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      //System.out.println("Inside storeProduct..");
       // console(p);
        pst=con1.prepareStatement(sql);
        pst.setString(1, p.getWHOLESALERNAME());
        pst.setString(2, p.getItem_Code());
        pst.setString(3, p.getAsin());
        pst.setString(4, p.getUpc());
        pst.setString(5, p.getTitle());
        pst.setString(6, p.getBrand());
        pst.setFloat(7, p.getStar());     
        pst.setInt(8, p.getnReviews());
        pst.setFloat(9, p.getBestprice());
        pst.setString(10, p.getSoldby());
        pst.setBoolean(11, p.getCcc_amazon_sells_last_3_months());
        pst.setInt(12, p.getNumberOfFbaSellers());
        pst.setBoolean(13, p.getNootherfbaSeller());
        pst.setInt(14, p.getPkgQnt());
        
        pst.setString(15, p.getUpc_in());
        pst.setString(16, p.getItem_Name());
        pst.setInt(17, p.getPacks_in_1_Case());
        pst.setFloat(18, p.getOne_case_price());
        pst.setFloat(19, p.getOne_pack_price());
        pst.setString(20, p.getAdditional_column1());
        pst.setString(21, p.getAdditional_column2());
        pst.setBoolean(22, p.getAmazon_sells_any_variation());
        pst.setFloat(23, p.getOriginal_cost_asin());
        pst.setFloat(24, p.getFbaFees());
        pst.setFloat(25, p.getFbaMargin());
        pst.setFloat(26, p.getProfit());
        pst.setFloat(27, p.getProfit_percentage());
        pst.setInt(28, p.getMy_units());
        pst.setInt(29, p.getReq_asins());
        pst.setInt(30, p.getCases_required());
        pst.setFloat(31, p.getProposed_case_price());
        pst.setInt(32, p.getUnits_sold_last_month());
        pst.setFloat(33, p.getWeight());
        pst.setFloat(34, p.getMyprice());
        pst.setBoolean(35, p.getWesell());
        pst.setFloat(36, p.getSecondbestprice());
        pst.setFloat(37, p.getRank());
        pst.setString(38, p.getProductCategoryId());
        
        pst.executeUpdate();
        pst.close();
        con1.close();
        
     }
     public static ArrayList<Product> getProductsFromTable(Product p_in,Connection con,String table) throws Exception{
         Connection connection_in=con;
         if(con==null){
             con=RetrievePage.getConnection();
         }
             
         ArrayList<Product> ar=new ArrayList();
        PreparedStatement pst=null;
        ResultSet rs=null;
        
              String sql="select "
                      + "WHOLESALERNAME,Item_Code,asin,upc,title,brand,star,nReviews,bestprice,soldby,ccc_amazon_sells_last_3_months,NUMBEROFFBASELLERS,NOOTHERFBASELLER,packing_qty_asin"
                      + ",upc_in,item_name,packs_in_1_case,1_case_price,1_pack_price,additional_column1,additional_column2,amazon_sells_any_variation,"
                      + " original_cost_asin,fbafees,fbamargin, profit,profit_percentage,my_units,req_asins,cases_required,proposed_cases_price,units_sold_last_month,weight, wesell,secondbestprice,rank"
                      + " from "+table + " where 1=1  ";
              if(p_in.getAsin()!=null && p_in.getAsin().length()>0){
                  sql=sql+" and asin=?";
              }
              else if(p_in.getUpc()!=null && p_in.getUpc().length()>0){
                  sql=sql+" and upc=?";
              }
              if(p_in.getWHOLESALERNAME()!=null && p_in.getWHOLESALERNAME().length()>0){
                   sql=sql+" and wholesalername=?";
              }
              sql =sql+ " order by ts desc";
                  
       // System.out.println(MODULE+"Inside getProduct..Executing sql:"+sql);
        pst=con.prepareStatement(sql);
        if(p_in.getAsin()!=null && p_in.getAsin().length()>0){
            pst.setString(1, p_in.getAsin());
        }
        if(p_in.getUpc()!=null && p_in.getUpc().length()>0){
            pst.setString(1, p_in.getUpc());
        }        
          if(p_in.getWHOLESALERNAME()!=null && p_in.getWHOLESALERNAME().length()>0){
               pst.setString(2, p_in.getWHOLESALERNAME());
          }        
       rs=pst.executeQuery();
       while(rs.next()){
          Product p=new Product();
           p.setWHOLESALERNAME(rs.getString("WHOLESALERNAME"));
           p.setItem_Code(rs.getString("Item_Code"));
           p.setAsin(rs.getString("asin"));
           p.setUpc(rs.getString("upc"));
           p.setTitle(rs.getString("title"));
           p.setBrand(rs.getString("brand"));
           p.setStar(rs.getFloat("star"));
           p.setnReviews(rs.getInt("nReviews"));
           p.setBestprice(rs.getFloat("bestprice"));
           p.setSoldby(rs.getString("soldby"));
           p.setCcc_amazon_sells_last_3_months(rs.getBoolean("ccc_amazon_sells_last_3_months"));
           p.setNumberOfFbaSellers(rs.getInt("NUMBEROFFBASELLERS"));
           p.setNootherfbaSeller(rs.getBoolean("NOOTHERFBASELLER"));
           p.setWesell(rs.getBoolean("wesell"));
           p.setPkgQnt(rs.getInt("packing_qty_asin"));
           p.setUpc_in(rs.getString("upc_in"));
           p.setItem_Name(rs.getString("item_name"));
           p.setPacks_in_1_Case(rs.getInt("packs_in_1_case"));
           p.setOne_case_price(rs.getFloat("1_case_price"));
           p.setOne_pack_price(rs.getFloat("1_pack_price"));
           p.setAdditional_column1(rs.getString("additional_column1"));
           p.setAdditional_column2(rs.getString("additional_column2"));
           p.setAmazon_sells_any_variation(rs.getBoolean("amazon_sells_any_variation"));
           p.setOriginal_cost_asin(rs.getFloat("original_cost_asin"));
           p.setFbaFees(rs.getFloat("fbafees"));
           p.setFbaMargin(rs.getFloat("fbamargin"));
           p.setProfit(rs.getFloat("profit"));
           p.setProfit_percentage(rs.getFloat("profit_percentage"));
           p.setMy_units(rs.getInt("my_units"));
           p.setReq_asins(rs.getInt("req_asins"));
           p.setCases_required(rs.getInt("cases_required"));
           p.setProposed_case_price(rs.getFloat("proposed_cases_price"));
           p.setUnits_sold_last_month(rs.getInt("units_sold_last_month"));
           p.setWeight(rs.getFloat("weight"));
           p.setSecondbestprice(rs.getFloat("secondbestprice"));
           p.setRank(rs.getInt("rank"));
           ar.add(p);
       
       }
       
        pst.close();
        if(connection_in==null)
            con.close();
        return ar;
     }
            
 static void console(Product p) throws Exception{
      JSONObject js=JSONObject.fromObject(p);
      System.out.println(js.toString());
}
   public static int calculatePackingQntFromTitle(String title){
        int pkgQnt=1;
        
        try{

    if(title.toLowerCase().indexOf("pack of")>=0){
        String t=StringUtils.substringAfter(title.toLowerCase(), "pack of");
        t=t.trim();
        String nStr="";
        for(int i2=0;i2<t.length();i2++){
            if(i2>=3)
                break;
            if(t.charAt(i2) >='0' && t.charAt(i2) <='9'){
                nStr+=t.charAt(i2);
            }
        }
        if(nStr.length()>0)
            pkgQnt=Integer.parseInt(nStr);


    }
}catch (Exception e){
    e.printStackTrace();
    
} 
      return pkgQnt;  

    } 
}
