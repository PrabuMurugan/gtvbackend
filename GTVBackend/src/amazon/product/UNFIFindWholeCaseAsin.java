/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.product;

import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author US083274
 */
public class UNFIFindWholeCaseAsin {
    public static int MAX_LIMIT=500;
    public static void main(String args[]) throws Exception{
          
            PreparedStatement pst = null,pst2=null;
            ResultSet rs = null,rs2=null;
             
            String sql=
               "select distinct code,name,product_upc_ean,reg_cost,size,pack,'NA' as amazon_asin_matchig_pack_size from   "
                    + "   Tajplaza.UN_Full_case_Asins where asin='NA' and code not like 'WG_%'   ";          
            String updateSql=" update  Tajplaza.un_full_case_asins set asin=?,bestprice=? where code=?";   
            
            System.out.println("Executing SQL:"+sql);
               Connection con1=RetrievePage.getConnection();
            pst=con1.prepareStatement(sql);
            pst2=con1.prepareStatement(updateSql);
           rs=pst.executeQuery();
           int count=0;
           while(rs.next()){
               if(count++>MAX_LIMIT){
               System.out.println("count exceeding MAX_LIMIT ..Exiting");
               System.exit(1);
           }
               String upc =rs.getString("product_upc_ean");
               if(upc.length()==11)
                   upc="0"+upc;
               upc=upc.replaceAll("UPC:", "");
               String code =rs.getString("code");
               int pack =rs.getInt("pack");
               float reg_cost =rs.getFloat("reg_cost");
               CreateCatalogNew.UNFI_FIND_CASE_ASIN=true;
               CreateCatalogNew.MIN_REVIEWS_REQ=0;
               CreateCatalog.ERROR_MOVE_TO_NEXT_PAGE=true;
               
              ArrayList<Product> allAsins=CreateCatalogNew.getUPCDetailsNewMWS(upc);
              Product caseMatchingProduct=new Product();
              for(int i=0;i<allAsins.size();i++){
                  Product p=allAsins.get(i);
                  
                  int pkgQnt=ProcessWorkingCatalog.calculatePkgQnt(p, con1);
                  
                  System.out.println("Asins:"+p.getAsin() +" title:"+p.getTitle() +" review:"+p.getnReviews() +"pkg Qnt:"+pkgQnt);
                  if(pack==pkgQnt ||(code.indexOf("CO")>=0 ||code.indexOf("SC")>=0 ||code.indexOf("BJ")>=0  ) ){
                      
                      if(pack!=1 && ( p.getnReviews()>caseMatchingProduct.getnReviews() || caseMatchingProduct.getnReviews()==0)){
                          caseMatchingProduct=p;
                      }
                      if(pack==1 &&  (p.getBestprice()<caseMatchingProduct.getBestprice()||caseMatchingProduct.getBestprice()==0 )
                            && (p.getTitle().toLowerCase().indexOf("per case")<0 && p.getTitle().toLowerCase().indexOf("pack")<0)
                              ){
                          caseMatchingProduct=p;
                      }
                      if(pack==1 &&  (p.getBestprice()<caseMatchingProduct.getBestprice()||caseMatchingProduct.getBestprice()==0 )
                            && (code.indexOf("CO")>=0 ||code.indexOf("SC")>=0 ||code.indexOf("BJ")>=0 )
                              ){
                          //For costco and BJs even if title contains pack that is okay. take it.
                          caseMatchingProduct=p;
                      }                      
                  }
              }
                  if(caseMatchingProduct.getAsin().length()>0){
                    System.out.println("Asin matching case: "+caseMatchingProduct.getAsin() +" For upc:"+upc +"for code:"+code);
                  pst2.setString(1,caseMatchingProduct.getAsin() );  
                  
                   Float bestprice=caseMatchingProduct.getBestprice();
                 /* if(bestprice <reg_cost*2.5)
                  {
                        //If the price on amazon listing page is less than OCX2, then lets go to product detail page, get the price.        
                      //sometimes the shipping may not have been added
                      //tmpP=QueryProduct.getProductListings1(caseMatchingProduct.getAsin());    
                      Product pr=QueryProduct.getProductListings1( caseMatchingProduct.getAsin() );
                      bestprice=pr.getBestprice();
                      
                  }*/
                      
                  pst2.setFloat(2,bestprice );  
                  pst2.setString(3,code );
                  pst2.executeUpdate();
                      
                  }else{
                    pst2.setString(1,"NO");  
                 
                    pst2.setFloat(2,(float)0);  
                    pst2.setString(3,code );
                    pst2.executeUpdate();                      
                  }
      
              if(allAsins.size()==0){
                    //pst2.setString(1,"NO_RESULT");  
                  pst2.setString(1,"NO");  
                 
                    pst2.setFloat(2,(float)0);  
                    pst2.setString(3,code );
                    pst2.executeUpdate();                      
              }
           }
           con1.close();;   
           
    }
}
