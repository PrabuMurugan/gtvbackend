/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.product;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetLowestPricedOffersForASINSample;
import com.tajplaza.products.ListMatchingProductsSample;
import com.tajplaza.products.GetMatchingProductSample;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;
/**
 *
 * @author prabu
 */

public class InsertPossibleAsinsSamsItems {
    public static String DEBUG_SQL=" and upc='293812717719'";
    public static void main(String[] args) throws Exception{
            String PRODUCTION=System.getProperty("PRODUCTION","false");
            if(PRODUCTION.equals("true")){
                DEBUG_SQL="";
 
            }
        
         Connection con1=RetrievePage.getConnection();
        String sql="select productid,title,upc from  hb_bc.sams_products where productid not in(select productid from tajplaza.possible_asins_for_sams_items ) and length(upc)>0 ";          
        String insertSql="insert into  tajplaza.possible_asins_for_sams_items (productid,possible_asin,title,amazonTitle,rank,leventhDistance,bestprice,upc,category,weight) values (?,?,?,?,?,?,?,?,?,?)";
        
        PreparedStatement ps=con1.prepareStatement(sql);
        PreparedStatement psInsert=con1.prepareStatement(insertSql);
        ResultSet rs=ps.executeQuery();
        
        while(rs.next()){
            try{
            String productid=rs.getString(1);
            String title=rs.getString(2);
             String upc=rs.getString(3);
            System.out.println("Processing1 upc: "+upc);
            ArrayList asinsInserted=new ArrayList();
            title=title.trim();
            ArrayList<HashMap> matchingProducts = ListMatchingProductsSample.getMatchingProducts(upc);
            boolean matchingAsinFound=false;
            int bestRank=-1;
           // String bestRankedAsin="";
             String amazonTitle="";
             String category="";
              HashMap hm=null;
               int rank=0;
                Float productInfoWeight=(float)1;
           for(int i=0;i<matchingProducts.size();i++)
            {
                matchingAsinFound=true;
                hm=matchingProducts.get(i);
                 String asin=(String) hm.get("asin");
                  amazonTitle=(String)hm.get("title");
                  if(hm.containsKey("productCategoryId"))
                      category=(String)hm.get("productCategoryId");
                 rank=(Integer)hm.get("rank");
                 /*  if(bestRank==-1 ||rank<bestRank){
                
             
                      bestRank= rank;
                      bestRankedAsin=asin;
                   }*/
                 HashMap fbaMap=RetrievePage.getDetailsFromFbaCalculatorNewMWS(con1,asin,"100",true);
                
                 if(fbaMap!=null && fbaMap.containsKey("productInfoWeight") &&  (Float)fbaMap.get("productInfoWeight")>0){
                        productInfoWeight=(Float)fbaMap.get("productInfoWeight")  ;
                        
                 }
                 
        
            
           if( asinsInserted.contains(asin)==false){
                System.out.println("Amazon title asin:"+asin);
      
                psInsert.setString(1, productid);
                psInsert.setString(2, asin);
                if(title.length()>100)
                title=title.substring(0, 99);

                psInsert.setString(3, title);
                if(amazonTitle.length()>100)
                amazonTitle= amazonTitle.substring(0, 99);
                psInsert.setString(4, amazonTitle);
                psInsert.setInt(5,(Integer)hm.get("rank"));
                psInsert.setInt(6,0);
                float bestprice=(float)0;
                HashMap hm2=GetCompetitivePricingForASINSample.getJustBBPriceandRankMWS(asin);
                bestprice=(Float)hm2.get("bestprice");
                psInsert.setFloat(7,bestprice);
                psInsert.setString(8, upc);
                psInsert.setString(9, category);
                psInsert.setFloat(10, productInfoWeight);
                psInsert.executeUpdate();
 
                asinsInserted.add(asin);
                
           }
          }
            if(matchingAsinFound == false){
                 psInsert.setString(1, productid);
                 psInsert.setString(2, "NA-"+productid);
                 psInsert.setString(3, "");
                psInsert.setString(4, "");
                 psInsert.setInt(5, 0);
                 psInsert.setInt(6, 0);
                  psInsert.setFloat(7,(float)0);
                psInsert.setString(8, upc);    
                psInsert.setString(9, category);
                psInsert.setFloat(10, 1);
                 psInsert.executeUpdate();
                 
                
            }            
        }catch (Exception e){e.printStackTrace();}

        }
        
    }
}
