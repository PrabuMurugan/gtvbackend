/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.product;

import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetLowestPricedOffersForASINSample;
import com.tajplaza.products.ListMatchingProductsSample;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author US083274
 */
public class ResearchStuckExpiredItems {
    public static int MAX_LIMIT=500;
    public static ArrayList<String> upcsProcessed=new ArrayList();
    public static void main(String args[]) throws Exception{
          
            PreparedStatement pst = null,psInsert=null;
            ResultSet rs = null,rs2=null;
             
            String sql=
               " select distinct v.upc,s.qty  from tajplaza.amazon_min_price a, tajplaza.variation_sku_upcs  v   , tajplaza.scanner_data_shelf s where  a.sku=v.sku and v.upc = s.upc and custom_note in ('Stuck in Shelf 30+days','Expiring Soon', 'Stuck in Shelf 60+days')  and v.upc not in (select upc from tajplaza.research_stuck_expiring_items) order by s.qty desc  limit 100";       
            String insertSql=" insert into  tajplaza.research_stuck_expiring_items (upc,asin,bestprice,notes) values  (?,?,?,?)";   
            
            System.out.println("Executing SQL:"+sql);
               Connection con1=RetrievePage.getConnection();
            pst=con1.prepareStatement(sql);
            psInsert=con1.prepareStatement(insertSql);
           rs=pst.executeQuery();
           int count=0;
            ArrayList asinsProcessed=new ArrayList();
            ArrayList upcsProcessed=new ArrayList();
            while(rs.next()){
                try{
                String upc=rs.getString(1);
                if(upcsProcessed.contains(upc))
                    continue;
                upcsProcessed.add(upc);
                System.out.println("Processing UPC: "+upc);
               
                ArrayList<HashMap> matchingProducts = ListMatchingProductsSample.getMatchingProducts(upc);
                boolean matchingAsinFound=false;
                for(int i=0;i<matchingProducts.size();i++){
                    matchingAsinFound=true;
                     HashMap hm=matchingProducts.get(i);
                     String asin=(String) hm.get("asin");
                     if(asin.indexOf("B01N0A5VCL")>=0)
                         System.out.println("Debug");
                     if(asinsProcessed.contains(asin)==false){
                        HashMap hmB=GetCompetitivePricingForASINSample.getJustBBPriceandRankMWS(asin);
                        Float bestprice=(Float) hmB.get("bbPriceEvenIfMe");
                        System.out.println("Matching asin:"+asin);
                         psInsert.setString(1, upc);
                         psInsert.setString(2, asin);
                         psInsert.setFloat(3, bestprice);
                         psInsert.setString(4, "");
                         psInsert.executeUpdate();
                         asinsProcessed.add(asin);

                     }

 
                }
                if(matchingAsinFound == false){
                     psInsert.setString(1, upc);
                     psInsert.setString(2, "NA-"+upc);
                     psInsert.executeUpdate();


                }            
            }catch (Exception e){e.printStackTrace();}

            }        
           con1.close();;   
           
    }
}
