/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.product;

import bombayplaza.catalogcreation.CamelCamelCamel;
import bombayplaza.pricematch.RetrievePage;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import net.sf.json.JSONObject;
import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.Mail;
import bombayplaza.pricematch.MatchThePrice;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
 
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetLowestPricedOffersForASINSample;
import com.tajplaza.products.ListMatchingProductsSample;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import org.apache.commons.lang.StringUtils;

    /**
 *
 * @author us083274
 */
public class CreateCatalogNew {
    static String MODULE="[CreateCatalogNew] ";
    static int SLEEP_FOR_EACH_VISIT=0;
    static int MAX_ROWS=1000;
    public static int MIN_REVIEWS_REQ=0;
   public  static int MIN_STARS_REQ=3;
   /*MAKE SURE BEFORE COMMITING ALL BELOW VALUES ARE FALSE*/
   public static boolean RUN_FOR_PRODUCTS_WE_SELL=false;
   public static boolean RUN_FOR_PRODUCTS_WE_SELL_ONLY_MISSSING_ROWS=false;
   public static boolean DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS=false;
   public static String QUERY_IN=" select distinct upc_in,item_code,packs_in_1_Case,1_case_price,1_pack_price,WHOLESALERNAME,firsttime_asin   from  tajplaza. amazon_input_catalog  ci  where length(upc_in)>9 and upc_in not like '%%000000000%%' and   (wholesalername, upc_in) not in  (select wholesalername, upc from tajplaza.amazon_catalog_working where wholesalername is not null and item_code is not null) and   (wholesalername, upc_in) not in  (select wholesalername, upc from tajplaza.amazon_catalog  where wholesalername is not null and item_code is not null)  and   (wholesalername, upc_in) not in  (select wholesalername, upc from tajplaza.amazon_catalog_noprofit  where wholesalername is not null and item_code is not null)     order by case   when wholesalername ='CO'  then 1 when wholesalername ='DT'  then 2 else 4   end    limit 100 ";

   public static String DEBUG_SQL= "";
   // and  (wholesalername is not null or wholesalername is null)

   //public static boolean I_NEED_MAX_PRICED_ASIN=false;
 public static void main(String args[]) throws Exception{
    // Product  p = getUPCDetails("073621090026");
     //console(p);
     try{
         processAmazonInputCatalog();
       }catch (Exception e){
         if(e.getMessage().indexOf("max_allowed_packet")>=0 ){
             try{
                 System.out.println("Excced maximum package, resettimg it");
                 Connection con=RetrievePage.getConnection();
                 PreparedStatement pst=con.prepareStatement("SET GLOBAL max_allowed_packet=16777216");
                 pst.executeUpdate();                     
                 con.close();
             }catch (Exception e2){e2.printStackTrace();}
         }  
         else{
             throw e;
         }
       }     
        

 }
 public static ArrayList excludeList_itemcodesNotUsed=new ArrayList();
 public static ArrayList processedUPCs=new ArrayList();
 
public static void  processAmazonInputCatalog() throws Exception{
                Connection con=RetrievePage.getConnection();
            PreparedStatement pst = null,pst2NA=null;
            ResultSet rs = null,rs2NA=null;
            String PRODUCTION=System.getProperty("PRODUCTION","true");
            if(PRODUCTION.equals("true")){
                DEBUG_SQL="";
                RUN_FOR_PRODUCTS_WE_SELL=false;
                DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS=false;
                RUN_FOR_PRODUCTS_WE_SELL_ONLY_MISSSING_ROWS=false;
                QUERY_IN=System.getProperty("QUERY_IN","");
            }
            String DAILY_UPDATES_AMAZON_CATALOG_FRESHNESSStr=System.getProperty("DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS","false");
             
            if(DAILY_UPDATES_AMAZON_CATALOG_FRESHNESSStr.equals("true")){
                DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS=true;
            } 
            String RUN_FOR_PRODUCTS_WE_SELL_ONLY_MISSSING_ROWSStr=System.getProperty("RUN_FOR_PRODUCTS_WE_SELL_ONLY_MISSSING_ROWS","false");
             
            if(RUN_FOR_PRODUCTS_WE_SELL_ONLY_MISSSING_ROWSStr.equals("true")){
                DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS=false;
                RUN_FOR_PRODUCTS_WE_SELL=true;
                RUN_FOR_PRODUCTS_WE_SELL_ONLY_MISSSING_ROWS=true;
                
            }   
           
            if(System.getProperty("RUN_FOR_PRODUCTS_WE_SELL")!=null)
                RUN_FOR_PRODUCTS_WE_SELL=Boolean.valueOf(System.getProperty("RUN_FOR_PRODUCTS_WE_SELL"));   
           // if(System.getProperty("I_NEED_MAX_PRICED_ASIN")!=null)
            //   I_NEED_MAX_PRICED_ASIN=Boolean.valueOf(System.getProperty("I_NEED_MAX_PRICED_ASIN")); 
            
                
            String and_condition="",sort_order="";
 
            String excludeSql="select wholesalername,item_code,asin from amazon_catalog_working where ts>(curdate() - interval 60 day) and length(item_code)>0 and length(wholesalername)>0";
            pst=con.prepareStatement(excludeSql);
            rs=pst.executeQuery();
            while(rs.next()){
                 excludeList_itemcodesNotUsed.add(rs.getString("wholesalername")+":"+rs.getString("item_code"));
                
            }
               
            
            if(DEBUG_SQL.length()==0 && PRODUCTION.equals("true")==false){
                if(QUERY_IN.length()==0 &&  RUN_FOR_PRODUCTS_WE_SELL==false && DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS==false){
                     System.out.println("Enter and condition like and id>10000 and id<20000  or wholesalername='UNFI' etc or you can leave blank also");
                    BufferedReader buffer=new BufferedReader(new InputStreamReader(System.in));
                    and_condition=buffer.readLine();
                    if(and_condition==null || and_condition.toLowerCase().indexOf("wholesalername")<0){
                        System.out.println("INVALID USAGE. You have to pass wholesalername like wholesalername='DOT' and id>123. The second condition is optional but wholesalername is mandatory");
                        System.exit(1);
                    }
                    if(and_condition!=null && and_condition.length()>0)
                        and_condition=" and "+and_condition;
                    
                }
            }else{
                and_condition=DEBUG_SQL;
            }

             SLEEP_FOR_EACH_VISIT=Integer.valueOf(System.getProperty("SLEEP_FOR_EACH_VISIT","0"));
             MAX_ROWS=Integer.valueOf(System.getProperty("MAX_ROWS","1000"));
             sort_order=String.valueOf(System.getProperty("sort_order",""));
             String sql=" select distinct upc_in,item_code,Packs_in_1_Case,1_case_price,1_pack_price,WHOLESALERNAME,firsttime_asin from ";
             if(DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS){
                 sql=" select distinct upc_in,item_code,Packs_in_1_Case,1_case_price,1_pack_price,WHOLESALERNAME,asin,ts from  amazon_catalog  ci where 1=1 and  profit>-2    and rank<60000   and  ts<date_sub(curdate(), interval 7 day)    order by case when wholesalername='DOT FOODS' then 2 else 1 end,    ts ";
                 if(DEBUG_SQL.length()==0)
                     sql=sql+ " limit 1000 ";
                DEBUG_SQL="";
                 and_condition="";
                 sort_order="";
             }                 
             else if(RUN_FOR_PRODUCTS_WE_SELL ){
                 sql=sql + " amazon_input_catalog_wesell where 1=1 ";
                 if(RUN_FOR_PRODUCTS_WE_SELL_ONLY_MISSSING_ROWS){
                     //sql=" select distinct upc_in,item_code,packs_in_1_Case,1_case_price,1_pack_price,WHOLESALERNAME,firsttime_asin from  amazon_input_catalog  ci where length(upc_in)>9 and upc_in not like '%000000000%' and (wholesalername,upc_in) not in (select wholesalername, upc from tajplaza.amazon_catalog_vw where wholesalername is not null and item_code is not null)  and   (wholesalername, upc_in) not in (select wholesalername, upc from tajplaza.amazon_catalog_working where wholesalername is not null and item_code is not null) ";
                     sql=" select distinct upc_in,item_code,packs_in_1_Case,1_case_price,1_pack_price,WHOLESALERNAME,firsttime_asin from  amazon_input_catalog  ci where length(upc_in)>9 and upc_in not like '%000000000%'   and   (wholesalername, upc_in) not in (select wholesalername, upc from tajplaza.amazon_catalog_working where wholesalername is not null and item_code is not null) ";
                     if(DEBUG_SQL.length()==0)
                         sql=sql+ " limit 1000 ";

                }
    
             }else if( DEBUG_SQL.length()>0){
                 sql=sql + " amazon_input_catalog  ci where 1=1 ";
             }
                 
             else{
                 sql=sql + " amazon_input_catalog where 1=1  and (wholesalername,item_code) not in (select wholesalername, item_code from tajplaza.amazon_catalog_working) and length(upc_in)>9 and upc_in not like '%000000000%' ";                 
               // sql=sql+"  and upc_in not in ( select upc_in from amazon_catalog_noprofit where ts>(curdate() - interval 180 day) ) ";                 
             }
             
             
             sql=sql+and_condition +  sort_order;
             if(QUERY_IN!=null && QUERY_IN.length()>0){
                 sql=QUERY_IN;
             }
             System.out.println("PRocessing rows using sql 2 : "+ sql);
             pst=con.prepareStatement(sql);
            //pst2=con.prepareStatement(" select wholesalername,item_code from amazon_catalog_working where item_code=?  and wholesalername=?");
 
            rs=pst.executeQuery();
             
            CreateCatalog oldC=new CreateCatalog();
            int rowCount=0;
            while(rs.next()){
                String item_code=rs.getString("item_code");
                 //System.out.println(" Going to process item_code :"+item_code );
                 
                //if(DEBUG_SQL.length()==0 &&  RUN_FOR_PRODUCTS_WE_SELL==false && excludeList.contains(rs.getString("wholesalername")+":"+rs.getString("item_code")))
                //if(DEBUG_SQL.length()==0 && RUN_FOR_PRODUCTS_WE_SELL==false &&  excludeList_itemcodes.contains(rs.getString("wholesalername")+":"+rs.getString("item_code"))){
                //if(DEBUG_SQL.length()==0 && RUN_FOR_PRODUCTS_WE_SELL==false &&  excludeList_itemcodes.contains(rs.getString("wholesalername")+":"+rs.getString("item_code"))){
                  //  System.out.println("Excluding item_code:"+item_code +"since it exists in amazon_catalog_working" );
                    //continue;
//                }
                    
 
                

                Product p=new Product();
                String upc=rs.getString("upc_in");
                System.out.println("Processing UPC:"+upc);
                String originalUpc=upc;

                String firsttime_asin="NA";
                if(DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS)
                   firsttime_asin= rs.getString("asin");
                else
                    firsttime_asin= rs.getString("firsttime_asin");
                
                String wholesalerName=rs.getString("WHOLESALERNAME");
                if(upc==null)
                   continue;
                upc=upc.replaceAll("UPC:","");
                upc=upc.replaceAll("upc:","");
                upc=upc.replaceAll("-","");
                upc=upc.replaceAll("'","");
                upc=upc.replaceAll(" ","");
                if(upc.length()>=14)
                    upc=upc.substring(2,14);
                upc=upc.trim();
                if(upc.length()<5)
                    continue;
                if(upc.length()<12)
                    upc=StringUtils.leftPad(upc,12, '0');                
               // pst2.setString(1,item_code);
                //pst2.setString(2, wholesalerName);
                //pst2.setString(2,item_code);
                //rs2=pst2.executeQuery();
               // if(DEBUG_SQL.length()==0 &&  rs2.next() && RUN_FOR_PRODUCTS_WE_SELL==false)
               // {
                 //   System.out.println(" item_code :"+item_code + " is already processed, skipping");
                   // continue;
                    
               // }
                 
                /*if(upc.length() ==11) {
                    //NOT HERE. BEFORE COMING TO INPUT_CATALOG TABLE, TRY AND CLEAN IT.
                    System.out.println("[ CreateCatalog ] Before check digit " + upc);
                    System.out.println("[ CreateCatalog ] Now before adding check digit. Let us check if adding leading Zero gives any result? If yes the no need to add check digit");
                    ArrayList lst = oldC.getUPCDetails("0"+upc);
                    if(lst != null && !lst.isEmpty()) {
                          upc= "0"+upc; 
                    } else {
                        upc  = oldC.addCheckDigit(upc);
                    }

                    System.out.println("[ CreateCatalog ] after check digit " + upc);
                }*/
               /* if(firsttime_asin==null || firsttime_asin.equals("B00E0FAGGO")==false)
                    continue;
                 */   
               /* if(upc.indexOf("031604014308")>=0)
                    System.out.println("debug");
                else
                    continue;*/
                if(processedUPCs.contains(upc)){
                    continue;
                }
                processedUPCs.add(upc);
              System.out.println(MODULE+" Processing id :item_code:"+item_code);
                if(rowCount++>MAX_ROWS){
                    System.out.println(MODULE +" MAX_ROWS exceeded. If needed to process more rows pass -DMAX_ROWS=XXX. Currnet MAX_ROWS is "+MAX_ROWS);
                    System.exit(0);
                }       
                ArrayList<Product> matchedProducts=new ArrayList();
              if(firsttime_asin!=null && firsttime_asin.trim().length()>0){
                  matchedProducts = getUPCDetailsNewMWS(firsttime_asin);
              }
                  
              else{
                 matchedProducts = getUPCDetailsNewMWS(upc);
              }
              if(matchedProducts.size()==0){
                  p=new Product();
                  p.setUpc(upc);
                  p.setWHOLESALERNAME(wholesalerName);
                  p.setItem_Code(item_code);
                  p.setAsin("NA");
                  ProcessWorkingCatalog.storeProduct3(p,"amazon_catalog_noprofit");
                  ProcessWorkingCatalog.storeProduct3(p,"amazon_catalog_working");
              }
              for(int i=0;i<matchedProducts.size();i++)    {
                p=matchedProducts.get(i);
                p.setItem_Code(item_code);
                p.setPacks_in_1_Case(rs.getInt("Packs_in_1_Case"));
                p.setOne_case_price(rs.getFloat("1_case_price"));
                //p.setItem_Name(rs.getFloat("1_case_price"));
                p.setUpc(upc);
                p.setUpc_in(originalUpc);
                if(wholesalerName==null || wholesalerName.length()==0)
                    wholesalerName="NA";
                p.setWHOLESALERNAME(wholesalerName);
                p.setItem_Code(item_code);
                if(p.getSoldby().equals("amazon"))
                        p.setCcc_amazon_sells_last_3_months(true);
               
                p.setPkgQnt(0);
                 ProcessWorkingCatalog.storeProduct3(p,"amazon_catalog_working");
                   
              }
                 
                
               
                
      
                
            }
            pst.close();
            con.close();
            //if(RUN_FOR_PRODUCTS_WE_SELL || DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS)
            {
            System.out.println("Lets start ProcessWorkingCatalog");
            ProcessWorkingCatalog pr = new ProcessWorkingCatalog();
            pr.processWorkingCatalogToCatalogTable();
            }
            System.out.println("Processed all rows. Done");

}  

 static void console(Product p) throws Exception{
      JSONObject js=JSONObject.fromObject(p);
      System.out.println(js.toString());
}
    public static ArrayList<Product> allProducts=new ArrayList();
    public static boolean UNFI_FIND_CASE_ASIN=false;
    public static  Product LOW_PRICED_ASIN1=new Product();
    public static ArrayList<Product> getUPCDetailsNewMWS(String upc ) throws Exception {
        Product p=new Product();
        try{
            HashMap hm=null;
            ArrayList<HashMap> products=ListMatchingProductsSample.getMatchingProducts(upc);
              allProducts=new ArrayList();
           for(int i=0;i<products.size();i++){
               p=new Product();
                hm=products.get(i);
                System.out.println("Inside getUPCDetailsNewMWS , hm: "+hm.toString());
                if(hm.containsKey("asin"))
                    p.setAsin((String)hm.get("asin"));
                if(hm.containsKey("Title"))
                    p.setTitle((String)hm.get("Title"));
                if(hm.containsKey("productInfoWeight"))
                    p.setWeight((Float)hm.get("productInfoWeight"));
                if(hm.containsKey("rank"))
                    p.setRank((Integer)hm.get("rank"));
                if(hm.containsKey("Brand"))
                    p.setBrand((String)hm.get("Brand"));
                if(hm.containsKey("productCategoryId"))
                    p.setProductCategoryId((String)hm.get("productCategoryId"));
                
                if(p.getRank()>0 && p.getRank()<100000){
                    //HashMap hm2=GetLowestPricedOffersForASINSample.getLowestPricedOffers(p.getAsin());
                     HashMap hm2=GetCompetitivePricingForASINSample.getJustBBPriceandRankMWS(p.getAsin());
                    p.setBestprice((Float)hm2.get("bestprice"));
                    p.setRank((Integer)hm2.get("rank"));
                }
                 
              //  p.setnReviews(0);
               // p.setStar((float)5);               
                allProducts.add(p);
           }
           

            
        }catch (Exception e){e.printStackTrace();}
         
        return allProducts;
    }
    public static Product getUPCDetailsOld(String upc ) throws Exception {
         allProducts=new ArrayList();
        CreateCatalog oldC=new CreateCatalog();
        QueryProduct qp=new QueryProduct();
        Product p1=new Product();
       
        p1.setUpc(upc);
        p1.setAsin("NA");
        System.out.println(MODULE+"Inside getUPCDetails. UPC : "+ upc);
        ArrayList jsarr=new ArrayList();
        if(upc==null || upc.length()==0 || upc.indexOf("0000000")>=0) {
            System.out.println(MODULE+"UPC is passed null or zero length or 0000000. Returning.");
            return  p1;
        }       
        String ret_string="";
        String url="http://www.amazon.com/s/?field-keywords=XXX";
         
        try {
            if(upc.indexOf("B")<0){
                HashMap FbaPrice=new HashMap();
                String fba_asin, fba_sku,sku="NA",fba_fnsku;

                url=url.replaceAll("XXX",upc);
                System.out.println(MODULE+"Hitting URL (Trace 0):"+url );
                ArrayList asins=new ArrayList();
                System.out.println(MODULE+"Value of upc is :"+upc);
                RetrievePage.USE_PROXIES=true;
            
                HtmlPage page = CreateCatalog.getPage1(url);
                ArrayList<DomElement> divs=new ArrayList();
                divs=(ArrayList<DomElement>) page.getByXPath("//div[@class='list results apsList']/div");
                System.out.println("[ CreateCatalogNew ] Vsize of div is :"+divs.size());

                if(divs.size()==0){
                    //for books
                    divs=(ArrayList<DomElement>) page.getByXPath("//ul[@id='s-results-list-atf']/li");
 
                }
                //Soome results are inside two separate list. Lets combine
                if(page.getByXPath("//li[@class='s-result-item celwidget']").size()>divs.size()){
                    
                     divs=(ArrayList<DomElement>) page.getByXPath("//li[@class='s-result-item celwidget']");
                }
                if(divs.size()==0){
                    //for books
                    divs=(ArrayList<DomElement>) page.getByXPath("//div[@class='s-item-container']//div");
                    if(divs.size()==0){
                    //SEE IF WE ARE ON PRODUCT DETAIL PAGE
                        return p1;  
                    }
                }                
                int maxReviews=MIN_REVIEWS_REQ;
                Float minPrice=(float)10000;
                String asin="NA";
                        
                for(int i=0;i<divs.size();i++){
                     int nReviews=0;
                     Float nStarF=(float)0;
                    String title=StringUtils.substringBetween(divs.get(i).asXml(), "<span class=\"lrg bold\">","</span>");
                    if(title== null || title.length()==0)
                        title=StringUtils.substringBetween(divs.get(i).asXml(), "<h2 class=\"a-size-medium s-inline s-access-title a-text-normal\">","</h2>");
                    if(title== null || title.length()==0)
                        title=StringUtils.substringBetween(divs.get(i).asXml(), "<h2 class=\"a-size-medium a-color-null s-inline s-access-title a-text-normal\">","</h2>");
                    if(title== null || title.length()==0){
                        title=StringUtils.substringAfter(divs.get(i).asXml(), "<h2");
                        title=StringUtils.substringBetween(title, ">", "<");
                        
                    }
                        
                    
                    if(title==null)
                        title="No title available";
                    title=title.trim();
                   // System.out.println(MODULE+" , trace0"  +" title:"+title+" div xml:"+divs.get(i).asXml());
                   // if(divs.get(i).asXml().indexOf("sprPrimePantry")>=0){
                     if(divs.get(i).asXml().indexOf("Pantry")>=0){
                     System.out.println(MODULE+"Skipping asin since it is primepantry: "+divs.get(i).getAttribute("name"))   ;
                     continue;
                    }
                    asin=divs.get(i).getAttribute("name");
                    if(asin==null || asin.length()==0)
                        asin=divs.get(i).getAttribute("data-asin");                    

                    if(p1.getAsin()==null || p1.getAsin().length()==0||p1.getAsin().equals("NA") )
                    {
                       
                
                        p1.setAsin(asin);
                        p1.setnReviews(0);
                        p1.setStar((float)0);
                        p1.setTitle(title.trim());


                    }
                    String price=StringUtils.substringBetween( divs.get(i).asXml(),"<span class=\"a-size-base a-color-price s-price a-text-bold\">","</span>");
                    if(price==null)
                        price=StringUtils.substringBetween( divs.get(i).asXml(),"<span class=\"a-size-base a-color-price a-text-bold\">","</span>");
                     float priceF=(float)0;
                    if(price==null)
                        price=StringUtils.substringBetween(divs.get(i).asText(),"$","\r\n");
                    if(price!=null){
                        price=price.trim();
                        try{
                           if(StringUtils.substringBefore(price,"$")!=null &&StringUtils.substringBefore(price,"$").length()>1 )
                               price=StringUtils.substringBefore(price,"$");
                           if(StringUtils.substringBefore(price,"new")!=null &&StringUtils.substringBefore(price,"new").length()>1 )
                               price=StringUtils.substringBefore(price,"new");                           
                           if(StringUtils.substringBefore(price,"-")!=null &&StringUtils.substringBefore(price,"-").length()>1 )
                               price=StringUtils.substringBefore(price,"-");                           
                           
                        price=price.replaceAll("\\$", "").replaceAll(",", "").replaceAll("Subscribe & Save", "").replaceAll("\\)", "").replaceAll("\\(", "").replaceAll("Other Sellers","").replaceAll("\\+", "").replaceAll("Prime","");
                        if(price.indexOf("used")>0)
                            price=StringUtils.substringBefore(price, "used");
                        if(price.indexOf("Add")>0)
                            price=StringUtils.substringBefore(price, "Add");
                        
                       priceF=Float.valueOf(price);
                        }catch (Exception e2){e2.printStackTrace();}

                    }                    
                    
                    if(divs.get(i).asXml().indexOf("out of 5 stars")>0){
                        String NreviewsStr=StringUtils.substringBetween(divs.get(i).asXml(), "<span class=\"rvwCnt\">","</span>");
                       if(NreviewsStr==null || NreviewsStr.length()==0)
                            NreviewsStr=StringUtils.substringBetween(divs.get(i).asXml(), "showViewpoints=1\">","</a>");
                       if(NreviewsStr==null || NreviewsStr.length()==0)
                            NreviewsStr=StringUtils.substringBetween(divs.get(i).asXml(), "#customerReviews\">","</a>");
                       
                        if(NreviewsStr!=null){
                            if(NreviewsStr.indexOf(">")>=0)
                                NreviewsStr=StringUtils.substringAfter(NreviewsStr, ">");
                            if(NreviewsStr.indexOf("<")>=0)
                                NreviewsStr=StringUtils.substringBefore(NreviewsStr, "<");
                            NreviewsStr=NreviewsStr.trim();
                           
                            try{
                                NreviewsStr=NreviewsStr.replaceAll(",","");
                               if(NreviewsStr.equals("NA")==false){
                                   nReviews= Integer.valueOf(NreviewsStr);
                                   int variationsTwister=page.getByXPath("//div[@id='variationsTwister']//li").size();
                                   if(variationsTwister>0)
                                       nReviews=nReviews/variationsTwister;
                                   
                               }
                                  
                            }catch (Exception e3){
                                e3.printStackTrace();
                            }

                           
                               
                                String starStr=StringUtils.substringBefore(divs.get(i).asXml(), "out of 5 stars");
                                starStr=StringUtils.substringAfterLast(starStr, "\"");
                                if(starStr.indexOf(">")>=0)
                                    starStr=StringUtils.substringAfterLast(starStr, ">");
                                starStr=starStr.trim();
                                float starF=0,maxStar=0;
                                try{    
                                    starF=Float.valueOf(starStr);
                                    nStarF=starF;
                                }catch (Exception e3){e3.printStackTrace();}
                                
                               // System.out.println("starStr : " + starStr);
                            if(   nReviews>maxReviews ||(nReviews== maxReviews && starF>maxStar)){
                                 maxReviews=nReviews;
                                 maxStar=starF;
                                p1.setAsin(asin);
                                p1.setnReviews(nReviews);
                                p1.setStar(starF); 
                                p1.setTitle(title);

                                if(divs.get(i).asXml().indexOf("Add-on Item")>0 || divs.get(i).asXml().indexOf("sprPrime")>0 || divs.get(i).asXml().indexOf("sprAddOn")>0 || divs.get(i).asXml().indexOf("a-icon-prime")>0){
                                    String primePriceStr=StringUtils.substringBetween(divs.get(i).asXml(), "<span class=\"bld lrg red\">","</span>");
                                    if(primePriceStr == null) {
                                        primePriceStr=StringUtils.substringBetween(divs.get(i).asXml(), "<span class=\"a-size-base a-color-price s-price a-text-bold\">","</span>");
                                    }
                                    if(primePriceStr == null) {
                                        primePriceStr=StringUtils.substringBetween(divs.get(i).asXml(), "<span class=\"a-size-medium a-color-price\">","</span>");
                                    }
                                    if(primePriceStr == null) {
                                        primePriceStr="";
                                    }                                    
                                    primePriceStr=primePriceStr.replaceAll("\\$", "").trim();
                                    if(primePriceStr.indexOf("-")>=0)
                                        primePriceStr="0";
                                    Float primePriceF=Float.valueOf(primePriceStr);
                                    p1.setBestprice(primePriceF);
                                    p1.setSoldby("Prime Seller");


                                }
                                if(p1.getBestprice()==0)
                                    p1.setBestprice(priceF);
                            }else{
                                System.out.println(MODULE+" ," +title + "does not have  "+ maxReviews + " reviews");
                            }



                        }else{
                            System.out.println(MODULE+" , trace1" +title + "NreviewsStr is null");
                        }
                    }else{
                       System.out.println(MODULE+" , trace1" +title + "does not have star information"  );
                    }
                    Product allP=new Product();
                  allP.setAsin(asin);
                  allP.setTitle(title);
                  allP.setBestprice(priceF);
                  allP.setnReviews(nReviews);
               allP.setStar(nStarF);
                 allProducts.add(allP);
                if(priceF<minPrice  ){
                    minPrice=priceF;
                    LOW_PRICED_ASIN1=allP;
                }
                 
                }                
            }//UPC does not start with B
            else{
                //UPC has B in it..That is the asin
                p1.setAsin(upc);
            }
                 if(UNFI_FIND_CASE_ASIN)
                     return p1;
            

            if(RUN_FOR_PRODUCTS_WE_SELL==false &&DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS==false){
                //IF I sell I do not care of reviews..I need all the details from product listigns page
                if(p1.getnReviews()<=MIN_REVIEWS_REQ || p1.getStar()<=MIN_STARS_REQ){
                    return p1;
                } 
               if(p1.getSoldby().equals("Prime Seller")){
                   return p1;
               }

                
            }
              System.out.println("[ CreateCatalog ]  Value of ASIN : " + p1.getAsin());

             if(SLEEP_FOR_EACH_VISIT>0)
                 Thread.sleep(SLEEP_FOR_EACH_VISIT*1000);
                if(DAILY_UPDATES_AMAZON_CATALOG_FRESHNESS) {
                    //p1=qp.getJustBBPriceandRank(p1);
                    p1=qp.getProductListings1(p1.getAsin());
                }else{
                    p1=qp.getProductListings1(p1.getAsin());
                }
                
                if(p1.getBestprice()==0 && p1.getnReviews()>MIN_REVIEWS_REQ  &&  p1.getStar()>MIN_STARS_REQ){
                    System.out.println("item exists in amazon, good reviews, no sellers are selling it, lets assume we can sell for $100");
                    HashMap hm=MatchThePrice.getProductDetails(p1.getAsin(),true,false,false);
                    float bestprice=(Float)(hm.get("bestprice")!=null?hm.get("bestprice"):(float)0);
                   
                    p1.setBestprice(bestprice);
                }else{
                    System.out.println(MODULE+" ,"+ p1.getTitle() +" reviews:"+p1.getnReviews() +" , star:"+p1.getStar()+"item exists in amazon, but not good  reviews, ");
                }
                        

                    
                p1.setUpc(upc);
         }catch (IOException e){
             if(e.getMessage().contains("ASIN_NOT_FOUND")) {
                 System.out.println("ASIN_NOT_FOUND Caugth");
                 insertAsinNotFound(p1.getAsin());
             }
         } catch (Exception e) {
             System.out.println("Something is wrong here see blow.");
             e.printStackTrace();
             
         }
       
          return p1;
    }   
   public static void insertAsinNotFound(String asin) {
       try{
           Connection con=RetrievePage.getConnection();
           PreparedStatement ps=con.prepareStatement("insert into tajplaza.asin_not_found (asin) values (?)");
           ps.setString(1, asin);
           ps.executeUpdate();
           con.close();           
       }catch (Exception e2){e2.printStackTrace();}

       
       
   } 
   
}