/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.product;

import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetLowestPricedOffersForASINSample;
import com.tajplaza.products.ListMatchingProductsSample;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author US083274
 */
public class UpdateFBAWeightAmazonMinPrice {
    public static int MAX_LIMIT=1000;
    
    public static void main(String args[]) throws Exception{
            String PRODUCTION=System.getProperty("PRODUCTION","false");
             String DEBUG_SQL=System.getProperty("DEBUG_ASIN"," and sku=''");
            if(PRODUCTION.equals("true")){
                DEBUG_SQL="";
                
            }
            PreparedStatement pst = null,pst2=null;
            ResultSet rs = null,rs2=null;
            //String sql_count="select count(*) from Tajplaza.UN_Full_case_Asins where asin like 'B%' and code in (select code from hb_bc.exported_products_from_hb where stock_level>0) order by case when cast(bestprice as decimal)=0 then 0 else 1 end, ts limit 100000";          
            String sql="select a.sku,asin1,v.onepack_Asin,v.pkg_qty from tajplaza.amazon_min_price a, tajplaza.existing_all_inventory e  left join tajplaza.variation_sku_upcs v on e.seller_sku=v.sku where a.sku=e.seller_sku and a.fba_weight is null limit 1000";          
              String updateSql=" update  tajplaza.amazon_min_price set fba_weight=?  where sku=? ";  
            
            System.out.println("Executing SQL:"+sql);
            Connection con1=RetrievePage.getConnection();
            pst=con1.prepareStatement(sql);
            rs=pst.executeQuery();   
            
            pst2=con1.prepareStatement(updateSql);
           rs=pst.executeQuery();
           int count=0;
           while(rs.next()){

                
               String sku =rs.getString("sku");
               String asin =rs.getString("asin1");
               String onepack_Asin=rs.getString("onepack_asin");
               Integer pkg_qty=rs.getInt("pkg_qty");
               float fbaWeight=(float)9.99;
               HashMap hm=RetrievePage.getDetailsFromFbaCalculatorNewMWS(con1, asin, "100", true);
               if(hm.containsKey("productInfoWeight")){
                   fbaWeight=(Float)hm.get("productInfoWeight");
                           
               }
               if(fbaWeight<1  && pkg_qty>1 && onepack_Asin !=null ){
                   hm=RetrievePage.getDetailsFromFbaCalculatorNewMWS(con1, onepack_Asin, "100", true);
                   if(hm.containsKey("productInfoWeight")){
                       fbaWeight=(Float)hm.get("productInfoWeight")*pkg_qty;

                   }                   
               }
              pst2.setFloat(1, fbaWeight);
              pst2.setString(2, sku);
                pst2.executeUpdate();
                      
 
              }
           con1.close();;   
           System.out.println("Completed UpdateFBAWeightAmazonMinPrice");
    }
}
