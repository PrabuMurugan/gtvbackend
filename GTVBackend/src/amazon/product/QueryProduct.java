/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.product;

import bombayplaza.catalogcreation.CreateCatalog;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
//import com.tajplaza.products.GetCompetitivePricingForASINSample;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author us083274
 */
public class QueryProduct {
    static String MODULE="QueryProduct";
    public static void main(String[] args) throws Exception{
        priceChanger();
    }
        public static void priceChanger() throws Exception{

           // suggestPriceForAsin("B000E130ES");
            Product p=getProductListings1("B000HJXQ9G");
            CreateCatalogNew.console(p);
                    
        }
        public static void suggestPriceForAsin(String asin) throws Exception{
            Connection con=RetrievePage.getLocalConnection();
            PreparedStatement pst = null;
            ResultSet rs = null;
            
            Product p=getProductListings1(asin );
            System.out.println("product map :"+p.toString());
            float suggested_price=0;
            String suggested_reason="NO CHANGE";
                
            //  pst=con.prepareStatement("select seller_sku ,item_name from existing_all_inventory where open_date not like '%open_date%'  and  str_to_date(substring(open_date,1,10),'%Y-%m-%d') < date_sub(curdate(), interval 30 day)  and seller_sku  not in ( select sellersku from orders where   purchaseDate > date_sub(curdate(), interval 30 day)  ) and fulfillment_channel='DEFAULT' ");
            pst=con.prepareStatement("delete from amazon_listing_i_sell where asin=?");
            pst.setString(1, asin);
            pst.executeUpdate();

            pst=con.prepareStatement("insert into  amazon_listing_i_sell ( asin,  upc, title,  brand ,  star , nReviews,bestprice,secondbestprice,myprice,myposition,NUMBEROFFBASELLERS,"
                    + "NOOTHERFBASELLER,soldby,suggested_price,suggested_reason)  values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            pst.setString(1, p.getAsin());
            pst.setString(2, p.getUpc());
            pst.setString(3, p.getTitle());
            pst.setString(4, p.getBrand());
            pst.setFloat(5, p.getStar());
            pst.setInt(6, p.getnReviews());
            pst.setFloat(7, p.getBestprice());
            pst.setFloat(8, p.getSecondbestprice());
            pst.setFloat(9, p.getMyprice());
            pst.setInt(10, p.getMyposition());
            pst.setInt(11, p.getNumberOfFbaSellers());
            pst.setBoolean(12, p.getNootherfbaSeller());
            pst.setString(13, p.getSoldby());
            pst.setFloat(14, suggested_price);
            pst.setString(15, suggested_reason);
            
            pst.executeUpdate();

            pst.close();
            con.close();

                
        }
        public static boolean I_WANT_ORIGINAL_BESTPRICE_AT_NOOTHER_FBA_SELLERS=false;
        public static Product getJustBBPriceandRankNotUsed(Product p  ) throws Exception{
            ArrayList<Product> arr=ProcessWorkingCatalog.getProductsFromTable(p, null, "amazon_catalog");
            if(arr.size()>0)
                p=arr.get(0);
            try{
                    HashMap hm=null;//GetCompetitivePricingForASINSample.getJustBBPriceandRankMWS(p.getAsin());
                   // p.setNootherfbaSeller((Boolean)hm.get("nootherfbaseller")));
                    p.setBestprice((Float)hm.get("bestprice"));
                    p.setRank((Integer)hm.get("rank"));
                
            }catch(Exception e){e.printStackTrace();}

            
            return p;
        }
    public static Product getProductListings1(String asin ){
     Product p = new Product();
    try{
        p.setAsin(asin);
              String url="http://www.amazon.com/gp/offer-listing/XXX/ref=dp_olp_new_mbc?ie=UTF8&condition=new";
            url=url.replaceAll("XXX", asin);

            
            HtmlPage mypage = CreateCatalog.getPage1(url);
            Float bestprice,secondbestprice;
              
             int rowCount=0;
             int numberOfFBASelleers=0;
              String soldby2="";
              String currentSeller="";
              float totalprice_currentrow=0;
              String title="";
              /*if(mypage.getByXPath("//div[@class='pantry-shelf-outer']").size()>0){
                  System.out.println("Prime Pantry");
                  p.setT
              }*/
                if( mypage.getByXPath("//title").size()>0 ){

                    title=((DomElement)(mypage.getByXPath("//title").get(0))).asText();
                    title=title.replaceAll("Amazon.com:", "").replaceAll(" Buying Choices:","").trim();
                  //  title=StringUtils.substringBeforeLast(title, ":");
                 //      title=StringUtils.substringBeforeLast(title, ":").trim();
                    p.setTitle(title);
                }
                if(title.length()==0)
                    return p;
                if(  mypage.getByXPath("//div[@id='olpProductByline']").size()>0 ){
                    //brand
                    String manu=((DomElement)(mypage.getByXPath("//div[@id='olpProductByline']").get(0))).asText();
                    manu=manu.replaceAll("by", "");
                    manu=manu.trim();
                 //      title=StringUtils.substringBeforeLast(title, ":").trim();
                    p.setBrand(manu);
                }  
                  if(  mypage.getByXPath("//div[@id='olpProductDetails']//i").size()>0 ){
                    //brand
                      float star=0;
                    String ratingsClass=((DomElement)(mypage.getByXPath("//div[@id='olpProductDetails']//i").get(0))).getAttribute("class");
                    if(ratingsClass!=null && ratingsClass.indexOf("a-star-4-5")>=0)
                        star=(float)4;
                    if(ratingsClass!=null && ratingsClass.indexOf("a-star-3-5")>=0)
                        star=(float)3;
                    if(ratingsClass!=null && ratingsClass.indexOf("a-star-2-5")>=0)
                        star=(float)2;
                    if(ratingsClass!=null && ratingsClass.indexOf("a-star-1-5")>=0)
                        star=(float)1;
                    
                    if(ratingsClass!=null && ratingsClass.indexOf("a-star-5")>=0)
                        star=5;
                    if(ratingsClass!=null && ratingsClass.indexOf("a-star-4")>=0)
                        star=4;
                    if(ratingsClass!=null && ratingsClass.indexOf("a-star-3")>=0)
                        star=3;
                    if(ratingsClass!=null && ratingsClass.indexOf("a-star-2")>=0)
                        star=2;
                    if(ratingsClass!=null && ratingsClass.indexOf("a-star-1")>=0)
                        star=1;
                    
                    
                    p.setStar(star);
                } 
                   int nReviews=0;
                  if(  mypage.getByXPath("//div[@id='olpProductDetails']//a[@class='a-link-normal']").size()>0 ){
                    // nummber of reviews 
                     try{
                        String sReviews=((DomElement)mypage.getByXPath("//div[@id='olpProductDetails']//a[@class='a-link-normal']").get(0)).asText();
                       if(sReviews!=null){
                           sReviews=sReviews.replaceAll("customer reviews", "").replaceAll("customer review", "").replaceAll(",","").trim();
                           if(sReviews.indexOf("Be the first to review this item")>=0)
                               sReviews="0";
                           
                       }
                       nReviews=Integer.valueOf(sReviews);
                       int variationsTwister=mypage.getByXPath("//div[@id='variationsTwister']//li").size();
                       if(nReviews>10&& variationsTwister>0 )
                           nReviews=nReviews/variationsTwister;
                       
                       
                         
                     }catch (Exception e0){e0.printStackTrace();}
                   
                }      
                 p.setnReviews(nReviews);
            for(int i=0;i<mypage.getByXPath("//div[@class='a-row a-spacing-mini olpOffer']").size();i++){

                          String pricetag= ((DomElement) (mypage.getByXPath("//div[@class='a-row a-spacing-mini olpOffer']").get(i))).asXml();
                          String pricetagasTxt= ((DomElement) (mypage.getByXPath("//div[@class='a-row a-spacing-mini olpOffer']").get(i))).asText();
                       // System.out.println("Price tag from priceLarge:"+pricetag);                          
                          if(i>=mypage.getByXPath("//div[@class='a-row a-spacing-mini olpOffer']//span[@class='a-size-large a-color-price olpOfferPrice a-text-bold']").size())
                              continue;
                          if(pricetag.toLowerCase().indexOf("shop this website")>=0)
                              continue;
                          String price=((DomElement) (mypage.getByXPath("//div[@class='a-row a-spacing-mini olpOffer']//span[@class='a-size-large a-color-price olpOfferPrice a-text-bold']").get(i))).asText();

                          String shipping="0";
                           String shippingnew="";
                            float shippinginfloat=(float)0;
                          try{
                              shipping=StringUtils.substringBetween(pricetag, "<span class=\"olpShippingPrice\">","</span");
                              if(shipping!=null){
                                shipping=shipping.replaceAll("\\+", "").replaceAll("shipping", "").replaceAll("\\$", "").replaceAll("&nbsp;", "").trim();
                                if(shipping.toLowerCase().indexOf("free")<0){
                                    for(int j=0;j<shipping.length();j++){
                                        if( shipping.charAt(j) =='.' || ( shipping.charAt(j) >='0' && shipping.charAt(j) <='9'))
                                            shippingnew+=shipping.charAt(j);
                                        }
                                    if(shippingnew.length()>0)
                                        shippinginfloat=Float.valueOf(shippingnew.replaceAll("\\$", "").trim());

                                }
                                  
                              }
                             totalprice_currentrow=Float.valueOf(price.replaceAll("\\$", "").trim().replaceAll(",",""))+shippinginfloat;
                             totalprice_currentrow=(float)(Math.round(totalprice_currentrow*100))/100;
                             if(p.getBestprice() ==null  || p.getBestprice()==0)
                                 p.setBestprice(totalprice_currentrow);
                             else if(rowCount>0 && (p.getSecondbestprice() ==null || p.getSecondbestprice()==0) ){

                                 p.setSecondbestprice(totalprice_currentrow);
                             }

                          }catch (Exception e1){
                              e1.printStackTrace();

                          }
                          try{
                              //seller name 
                              
                                String sellerTag=StringUtils.substringBetween(pricetag, "<p class=\"a-spacing-small olpSellerName\">","</p");
                                if(sellerTag==null){
                                    sellerTag=StringUtils.substringBetween(pricetag, "<p class=\"a-spacing-mini olpSellerName\">","</p");
                                }
                                if(sellerTag==null){
                                    sellerTag=StringUtils.substringBetween(pricetag, "<div class=\"a-column a-span2 olpSellerColumn\">","</div>");
                                }    
                                //System.out.println("[QueryProduct] sellerTag"+sellerTag);
                                 if(sellerTag!=null && sellerTag.indexOf("href")<0){
                                     currentSeller="amazon";
                                 }    else{
                                      currentSeller=StringUtils.substringAfter(sellerTag, "<a href=\"");
                                      currentSeller=StringUtils.substringBefore(currentSeller, "\"");
                                      if(currentSeller!=null && currentSeller.indexOf("A1E3GOW3768RBB")>0){
                                        currentSeller="timely goods";
                                        p.setMyprice( totalprice_currentrow);
                                         p.setMyposition(i);
                                     }
                                 }
                                 if(p.getSoldby()==null || p.getSoldby().length()==0 || currentSeller==null || currentSeller.equals("amazon"))
                                    p.setSoldby( currentSeller);
                             
                          }catch (Exception e1){
                              e1.printStackTrace();

                          }
               
                          try{
                              //seller name 
                              
                               if(pricetag.indexOf("Fulfillment by Amazon") >=0 || pricetagasTxt.indexOf("on orders over $35.00")>=0 ||pricetagasTxt.indexOf("Fulfillment by Amazon")>=0   || pricetagasTxt.indexOf("Ships with any qualifying order over $25")>=0){
                                   if(currentSeller!=null && currentSeller.equals("timely goods")==false){
                                       numberOfFBASelleers++;
                                   }
                                   
                               }    
                              
                          }catch (Exception e1){
                              e1.printStackTrace();

                          }
                           
                          

                               
                          rowCount++;
                 
            }
            p.setNumberOfFbaSellers( numberOfFBASelleers); 
            if(numberOfFBASelleers==0){
                p.setNootherfbaSeller(true);
                 
            }else{
                 p.setNootherfbaSeller(false);
            }
            
            if(p.getSoldby().equals("timely goods") ==false && p.getNootherfbaSeller() && I_WANT_ORIGINAL_BESTPRICE_AT_NOOTHER_FBA_SELLERS==false){
                System.out.println(MODULE+" product is not sold by us and no other fba seller..increasing the best price by 10%");
                p.setBestprice(p.getBestprice()*(float)1.1);
            }            
                
            
            
    }catch (Exception e){
        if(e.getMessage().contains("ASIN_NOT_FOUND")) {
                System.out.println("[ QueryProduct ] ASIN_NOT_FOUND");
                HashMap hm=MatchThePrice.getProductDetails(asin, true, true, false);
                if(CreateCatalogNew.RUN_FOR_PRODUCTS_WE_SELL_ONLY_MISSSING_ROWS ==false &&
                        hm.containsKey("asinOnPage") && hm.get("asinOnPage").equals(asin)==false){
                    asin= (String)hm.get("asinOnPage");
                    return getProductListings1(asin);
                    
                }
                
                
                       
                        
        }else {
            e.printStackTrace();
        }
       // e.printStackTrace();
    }
    return p;
}
}
