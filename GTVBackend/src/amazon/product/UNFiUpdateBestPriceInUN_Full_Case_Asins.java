/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.product;

import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetLowestPricedOffersForASINSample;
import com.tajplaza.products.ListMatchingProductsSample;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author US083274
 */
public class UNFiUpdateBestPriceInUN_Full_Case_Asins {
    public static int MAX_LIMIT=1000;
    
    public static void main(String args[]) throws Exception{
            String PRODUCTION=System.getProperty("PRODUCTION","false");
             String DEBUG_SQL=System.getProperty("DEBUG_ASIN"," and asin='B007DKILA6'");
            if(PRODUCTION.equals("true")){
                DEBUG_SQL="";
                
            }
            PreparedStatement pst = null,pst2=null;
            ResultSet rs = null,rs2=null;
            //String sql_count="select count(*) from Tajplaza.UN_Full_case_Asins where asin like 'B%' and code in (select code from hb_bc.exported_products_from_hb where stock_level>0) order by case when cast(bestprice as decimal)=0 then 0 else 1 end, ts limit 100000";          
            String sql="select distinct code,asin,bestprice,ts,weight,bestprice,weight_already_tried from Tajplaza.UN_Full_case_Asins where   asin like 'B%' "+DEBUG_SQL+" and      code in (select code from hb_bc.exported_products_from_hb where stock_level>0)    and ts< (curdate() - interval 30  day) order by    ts limit 500  ";          
              String updateSql=" update  Tajplaza.un_full_case_asins set bestprice=?, asin=?,rank=?,ts=curdate() where code=? ";  
            
            System.out.println("Executing SQL:"+sql);
            Connection con1=RetrievePage.getConnection();
            pst=con1.prepareStatement(sql);
            rs=pst.executeQuery();   
            
            pst2=con1.prepareStatement(updateSql);
           rs=pst.executeQuery();
           int count=0;
           while(rs.next()){

                
               String code =rs.getString("code");
               String asin =rs.getString("asin");
               Float weight =rs.getFloat("weight");
               Float origBestPrice =rs.getFloat("bestprice");
               Boolean weight_already_tried=rs.getBoolean("weight_already_tried");
                String ts =rs.getString("ts");
             
              /* if(weight==0){
                   try{
                       System.out.println("Getting weight for asin:"+asin + "code :"+code);
                        HashMap fbaMap=RetrievePage.getDetailsFromFbaCalculator(null,asin, String.valueOf(100),true);
                        String weightS=String.valueOf(fbaMap.get("productInfoWeight")  );
                     
                        weight=Float.valueOf(weightS);
                         Float width=(float)1,length=(float)1,height=(float)1;
                        try{
                       if(fbaMap.containsKey("productInfoWidth") && (Float)(fbaMap.get("productInfoWidth"))>0 )
                           width=(Float)(fbaMap.get("productInfoWidth"));
                       if(fbaMap.containsKey("productInfoLength") && (Float)(fbaMap.get("productInfoLength"))>0 )
                             length=(Float)(fbaMap.get("productInfoLength"));
                       if(fbaMap.containsKey("productInfoHeight") && (Float)(fbaMap.get("productInfoHeight"))>0 )
                             height=(Float)(fbaMap.get("productInfoHeight"));
                       if(weight>0 && width*length*height/166>weight)
                           weight=width*length*height/166;
                        }catch (Exception e2){}                           
                        if(weight==0){
                            pst3.setString(1, code);
                            ResultSet rs3=pst3.executeQuery();
                            if(rs3.next()){
                                weight=rs3.getFloat(1);
                            }
                        }
                        
                       
                   }catch (Exception e2){e2.printStackTrace();}
               }else{
                   //if(weight_already_tried==true && code.indexOf("CO_") <0 &&  code.indexOf("BJ_") <0){
                   if(weight_already_tried==true ){
                       System.out.println("I do not want to update the best price of each item in un full case asins Since sellery will take care of it. But if its BJ or costco, I do check who is selling it so I do not load them by mistake.");
                       continue;
                       
                   }
               }*/
              /* if(count++>MAX_LIMIT){
                   System.out.println("Exiting since the count exceeds MAX_LIMIT");
                   System.exit(1);
               }*/               
               //HashMap hm=GetLowestPricedOffersForASINSample.getLowestPricedOffers(asin);
                 HashMap hm=GetCompetitivePricingForASINSample.getJustBBPriceandRankMWS(asin);
                Float bestprice=(Float) hm.get("bestprice");
                 Integer rank=(Integer) hm.get("rank");
                 if(rank==null || rank==0)
                     rank=99999;
                if(hm.containsKey("asin")  )
                    asin=(String) hm.get("asin");
               System.out.println("Updating best price of code : "+code + " to "+bestprice );
                pst2.setFloat(1,bestprice  );  
                pst2.setString(2,asin  );  
                pst2.setInt(3,rank );
                pst2.setString(4,code );
                
               
                pst2.executeUpdate();
                      
 
              }
           con1.close();;   
           System.out.println("Completed UNFiUpdateBestPriceInUN_Full_Case_Asins");
    }
}
