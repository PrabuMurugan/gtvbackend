/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.product;
import bombayplaza.pricematch.RetrievePage;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlImageInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author prabu
 */
 
public class GetUPCForAsin {
    static   WebClient webClient1=null;
    public static void main(String args[]) throws Exception{
        Connection con=RetrievePage.getConnection();
        PreparedStatement ps=con.prepareStatement("select substring_index(code,'_',1) asin from hb_bc.exported_products_from_hb where length(product_upc_ean)<10 and stock_level>0 and code like '%AMA%' ");
        ResultSet rs=ps.executeQuery();
        
        while(rs.next()){
            String asin=rs.getString("asin");
           getUPCForAsin(asin);
            
            
        }
        
    }
    public static String getUPCForAsin(String asin) throws Exception{
        String upc=null;
        if(asin==null || asin.indexOf("B")<0)
            return upc;
        HtmlPage page=null;
        if(webClient1==null){
        webClient1=new WebClient(BrowserVersion.FIREFOX_38 );

            System.getProperties().put("org.apache.commons.logging.simplelog.defaultlog", "error");
            webClient1.getOptions().setThrowExceptionOnScriptError(false);
            webClient1.getOptions().setJavaScriptEnabled(true);
            webClient1.getOptions().setCssEnabled(false);
            webClient1.waitForBackgroundJavaScript(15000);

            page = webClient1.getPage("https://sellercentral.amazon.com/gp/homepage.html");
            //System.out.println(page.asText());
                if(page.getElementById("username")!=null)
                    ((HtmlTextInput) page.getElementById("username")).setText("packfba@gmail.com");
                else
                    ((HtmlTextInput) page.getElementById("ap_email")).setValueAttribute("packfba@gmail.com");
                if(page.getElementById("password")!=null)
                    ((HtmlPasswordInput) page.getElementById("password")).setText("dummy123");
                else
                    ((HtmlPasswordInput) page.getElementById("ap_password")).setValueAttribute("dummy123");
                 System.out.println("Signing In");
                 if(page.getElementById("sign-in-button")!=null)
                    page=(HtmlPage) ((HtmlButton) page.getElementById("sign-in-button")).click();
                 else
                     page=(HtmlPage) ((HtmlImageInput) page.getElementById("signInSubmit")).click();
            
        }

        String url="https://sellercentral.amazon.com/productsearch?q=ASIN&ref_=xx_prodsrch_cont_prodsrch";
        url=url.replaceAll("ASIN", asin);
        page=webClient1.getPage(url);
        System.out.println(page.asText());
        return upc;

    }
}
