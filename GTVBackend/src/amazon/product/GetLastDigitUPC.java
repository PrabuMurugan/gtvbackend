/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.product;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

/**
 *
 * @author prabu
 */
public class GetLastDigitUPC {
     public static void main(String args[]) throws Exception{
                 
  FileInputStream fstream = new FileInputStream("C:\\Google Drive\\Dropbox\\tmp\\upcs-without-last-digit.txt");
  // Get the object of DataInputStream
  DataInputStream in = new DataInputStream(fstream);
  BufferedReader br = new BufferedReader(new InputStreamReader(in));
  String strLine0;
             FileWriter fstream_out1 = new FileWriter("C:\\Google Drive\\Dropbox\\tmp\\upcs-with-last-digit.txt" );
             BufferedWriter out1 = new BufferedWriter(fstream_out1);
  
      while ((strLine0 = br.readLine()) != null)   {
        if(strLine0.trim().length()>0){
            String oldUPC=strLine0;
            String newUPC=addCheckDigit(oldUPC);
             out1.write(newUPC+","+newUPC);
            out1.newLine();
            
        }
             
        
            
      }
       out1.close();  
     }
 public static String addCheckDigit(String upc) {
        System.out.println("[ addCheckDigit ] UPC : " + upc);
        int [] input =  {0,0,0,0,0,0,0,0,0,0,0,0};
        int [] mul =  {3,1,3,1,3,1,3,1,3,1,3,0};
        int [] out =  {0,0,0,0,0,0,0,0,0,0,0,0};
        if(upc.length()==11) {
            for(int i=0;i<11;i++) {
                input[i] = Integer.parseInt(""+upc.charAt(i));
           }
        }
           for(int i=0;i<11;i++) {
                out[i]=input[i]*mul[i];
           }
        int sum=0;
        for(int i=0;i<11;i++) {
                sum+=out[i];
           }
        
        int chkdig = 10-(sum%10);
        if(chkdig ==10 ) chkdig=0;
        System.out.println("[ addCheckDigit ] Check Digit : " + chkdig);
        return (upc+chkdig);
    }
         
}
