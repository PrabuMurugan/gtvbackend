/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.product;
import bombayplaza.pricematch.MatchThePrice;
import bombayplaza.pricematch.RetrievePage;
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetLowestPricedOffersForASINSample;
import com.tajplaza.products.ListMatchingProductsSample;
import com.tajplaza.products.GetMatchingProductSample;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
/**
 *
 * @author prabu
 */

public class InsertPossibleAsinsShelfItems {
    public static String DEBUG_SQL=" and upc='293812717719'";
    public static void main(String[] args) throws Exception{
            String PRODUCTION=System.getProperty("PRODUCTION","false");
            if(PRODUCTION.equals("true")){
                DEBUG_SQL="";
 
            }
        
         Connection con1=RetrievePage.getConnection();
        String sql="select distinct upc from tajplaza.scanner_data_shelf where upc not in (select upc from tajplaza.possible_asins_for_shelf_items)  "+DEBUG_SQL+"  order by qty desc ";          
        String insertSql="insert into tajplaza.possible_asins_for_shelf_items (upc,possible_asin) values (?,?)";
        
        PreparedStatement ps=con1.prepareStatement(sql);
        PreparedStatement psInsert=con1.prepareStatement(insertSql);
        ResultSet rs=ps.executeQuery();
        
        while(rs.next()){
            try{
            String upc=rs.getString(1);
            System.out.println("Processing UPC: "+upc);
            ArrayList asinsInserted=new ArrayList();
            ArrayList<HashMap> matchingProducts = ListMatchingProductsSample.getMatchingProducts(upc);
            boolean matchingAsinFound=false;
            for(int i=0;i<matchingProducts.size();i++){
                matchingAsinFound=true;
                 HashMap hm=matchingProducts.get(i);
                 String asin=(String) hm.get("asin");
                 System.out.println("Matching asin:"+asin);
                 if(asinsInserted.contains(asin)==false){
                     psInsert.setString(1, upc);
                     psInsert.setString(2, asin);
                     psInsert.executeUpdate();
                     if(asin.equals("B01IWI6XT8"))
                         System.out.println("Debug");
                     asinsInserted.add(asin);
                     
                 }
                 
                 GetMatchingProductSample childs=new GetMatchingProductSample();
                 
                 ArrayList arr=childs.getAllOtherVariationsBySize(asin);
                 for (int j=0;j<arr.size();j++){
                     HashMap hm2=(HashMap) arr.get(j);
                     String childAsin=(String) hm2.get("childAsin");
                     String childSize="";
                     if(hm2.containsKey("childSize"))
                         childSize=(String) hm2.get("childSize");
                     if(asinsInserted.contains(childAsin)==false 
                            // && (childSize.toLowerCase().indexOf("pack")>=0 || childSize.toLowerCase().indexOf("set")>=0)
                             ){
                         psInsert.setString(1, upc);
                         psInsert.setString(2, childAsin+" ("+childSize+")");
                         psInsert.executeUpdate();
                     if(childAsin.equals("B01IWI6XT8"))
                         System.out.println("Debug");
                         
                         asinsInserted.add(childAsin);
                         
                     }
                     
                 }
            }
            if(matchingAsinFound == false){
                 psInsert.setString(1, upc);
                 psInsert.setString(2, "NA-"+upc);
                 psInsert.executeUpdate();
                 
                
            }            
        }catch (Exception e){e.printStackTrace();}

        }
        
    }
}
