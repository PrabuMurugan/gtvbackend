/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package amazon.product;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author us083274
 */
public class Product {

    public String getItem_Code() {
        return Item_Code;
    }

    public void setItem_Code(String Item_Code) {
        this.Item_Code = Item_Code;
    }

    public String getItem_Name() {
        return Item_Name;
    }

    public void setItem_Name(String Item_Name) {
        this.Item_Name = Item_Name;
    }

    public String getWHOLESALERNAME() {
        return WHOLESALERNAME;
    }

    public void setWHOLESALERNAME(String WHOLESALERNAME) {
        this.WHOLESALERNAME = WHOLESALERNAME;
    }

    public String getAdditional_column1() {
        return additional_column1;
    }

    public void setAdditional_column1(String additional_column1) {
        this.additional_column1 = additional_column1;
    }

    public String getAdditional_column2() {
        return additional_column2;
    }

    public void setAdditional_column2(String additional_column2) {
        this.additional_column2 = additional_column2;
    }

    public Float getOne_case_price() {
        return one_case_price;
    }

    public void setOne_case_price(Float one_case_price) {
        one_case_price=Math.round(one_case_price*(float)100)/(float)100;
        this.one_case_price = one_case_price;
    }

    public Float getOne_pack_price() {
        return one_pack_price;
    }

    public void setOne_pack_price(Float one_pack_price) {
        one_pack_price=Math.round(one_pack_price*(float)100)/(float)100;
        this.one_pack_price = one_pack_price;
    }

    public Integer getPacks_in_1_Case() {
        return Packs_in_1_Case;
    }

    public void setPacks_in_1_Case(Integer Packs_in_1_Case) {
        this.Packs_in_1_Case = Packs_in_1_Case;
    }
        
 
     
    String asin="", upc="" ,  title="",brand="",soldby="",Item_Code="",Item_Name="",WHOLESALERNAME="",additional_column1="",additional_column2="",upc_in="";
    Float star=(float)0,bestprice=(float)0,secondbestprice=(float)0,myprice=(float)0,one_case_price=(float)0,one_pack_price=(float)0,original_cost_asin=(float)0,fbaFees=(float)0,fbaMargin=(float)0,profit=(float)0,profit_percentage=(float)0,proposed_case_price=(float)0,weight=(float)0;
  Integer nReviews=0,myposition=0,numberOfFbaSellers=0,Packs_in_1_Case=0,pkgQnt,my_units=0,req_asins=0,cases_required=0,units_sold_last_month=0;
Boolean nootherfbaSeller=false,ccc_amazon_sells_last_3_months=false,amazon_sells_any_variation=false,wesell=false;
Integer rank;

    public Integer getRank() {
        if(rank==null || rank==0 )
            rank=99999;
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Boolean getWesell() {
        if(wesell==null)
            wesell=false;
        return wesell;
    }

    public void setWesell(Boolean wesell) {
        this.wesell = wesell;
    }

    public Boolean getAmazon_sells_any_variation() {
        return amazon_sells_any_variation;
    }

    public void setAmazon_sells_any_variation(Boolean amazon_sells_any_variation) {
        this.amazon_sells_any_variation = amazon_sells_any_variation;
    }

    public Integer getCases_required() {
        return cases_required;
    }

    public void setCases_required(Integer cases_required) {
        this.cases_required = cases_required;
    }

    public Float getFbaFees() {
        return fbaFees;
    }

    public void setFbaFees(Float fbaFees) {
        fbaFees=Math.round(fbaFees*(float)100)/(float)100;
        this.fbaFees = fbaFees;
    }

    public Float getFbaMargin() {
        return fbaMargin;
    }

    public void setFbaMargin(Float fbaMargin) {
        fbaMargin=Math.round(fbaMargin*(float)100)/(float)100;
        this.fbaMargin = fbaMargin;
    }

    public Integer getMy_units() {
        return my_units;
    }

    public void setMy_units(Integer my_units) {
        this.my_units = my_units;
    }

    public Float getOriginal_cost_asin() {
        return original_cost_asin;
    }

    public void setOriginal_cost_asin(Float original_cost_asin) {
        original_cost_asin=Math.round(original_cost_asin*(float)100)/(float)100;
        this.original_cost_asin = original_cost_asin;
    }

    public Float getProfit() {
        return profit;
    }

    public void setProfit(Float profit) {
        profit=Math.round(profit*(float)100)/(float)100;
        this.profit = profit;
    }

    public Float getProfit_percentage() {
        return profit_percentage;
    }

    public void setProfit_percentage(Float profit_percentage) {
        profit_percentage=Math.round(profit_percentage*(float)100)/(float)100;
        this.profit_percentage = profit_percentage;
    }

    public Float getProposed_case_price() {
        return proposed_case_price;
    }

    public void setProposed_case_price(Float proposed_case_price) {
        proposed_case_price=Math.round(proposed_case_price*(float)100)/(float)100;
        this.proposed_case_price = proposed_case_price;
    }

    public Integer getReq_asins() {
        return req_asins;
    }

    public void setReq_asins(Integer req_asins) {
        this.req_asins = req_asins;
    }

    public Integer getUnits_sold_last_month() {
        return units_sold_last_month;
    }

    public void setUnits_sold_last_month(Integer units_sold_last_month) {
        this.units_sold_last_month = units_sold_last_month;
    }

    public String getUpc_in() {
        return upc_in;
    }

    public void setUpc_in(String upc_in) {
        this.upc_in = upc_in;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        weight=Math.round(weight*(float)100)/(float)100;
        this.weight = weight;
    }
    public Integer getPkgQnt() {
        if(pkgQnt==null)
            pkgQnt=1;
        return pkgQnt;
    }

    public void setPkgQnt(Integer pkgQnt) {
        this.pkgQnt = pkgQnt;
    }
  

    public Boolean getCcc_amazon_sells_last_3_months() {
        if(ccc_amazon_sells_last_3_months==null)
            ccc_amazon_sells_last_3_months=false;
        return ccc_amazon_sells_last_3_months;
    }

    public void setCcc_amazon_sells_last_3_months(Boolean ccc_amazon_sells_last_3_months) {
        this.ccc_amazon_sells_last_3_months = ccc_amazon_sells_last_3_months;
    }

    public String getAsin() {
        return asin;
    }

    public void setAsin(String asin) {
        this.asin = asin;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getTitle() {
        if(title==null)
            title="";
        if(title.length()>199)
            title=StringUtils.substring(title,0,199);
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrand() {
        if(brand==null)
            brand="";
        
        if(brand.length()>99)
            brand=StringUtils.substring(brand,0,99);
        
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSoldby() {
        if(soldby==null)
            soldby="";
        
        if(soldby.length()>99)
            soldby=StringUtils.substring(soldby,0,99);
        
        return soldby;
    }

    public void setSoldby(String soldby) {
        this.soldby = soldby;
    }

    public Float getStar() {
        return star;
    }

    public void setStar(Float star) {
        this.star = star;
    }

    public Float getBestprice() {
        return bestprice;
    }

    public void setBestprice(Float bestprice) {
        bestprice=Math.round(bestprice*(float)100)/(float)100;
        this.bestprice = bestprice;
    }

    public Float getSecondbestprice() {
        return secondbestprice;
    }

    public void setSecondbestprice(Float secondbestprice) {
        secondbestprice=Math.round(secondbestprice*(float)100)/(float)100;
        this.secondbestprice = secondbestprice;
    }

    public Float getMyprice() {
        return myprice;
    }

    public void setMyprice(Float myprice) {
        this.myprice = myprice;
    }

    public Integer getnReviews() {
        return nReviews;
    }

    public void setnReviews(Integer nReviews) {
        this.nReviews = nReviews;
    }

    public Integer getMyposition() {
        return myposition;
    }

    public void setMyposition(Integer myposition) {
        this.myposition = myposition;
    }

    public Integer getNumberOfFbaSellers() {
        return numberOfFbaSellers;
    }

    public void setNumberOfFbaSellers(Integer numberOfFbaSellers) {
        this.numberOfFbaSellers = numberOfFbaSellers;
    }

    public Boolean getNootherfbaSeller() {
        return nootherfbaSeller;
    }

    public void setNootherfbaSeller(Boolean nootherfbaSeller) {
        this.nootherfbaSeller = nootherfbaSeller;
    }
 
    String productCategoryId;

    public String getProductCategoryId() {
        if(productCategoryId==null)
            productCategoryId="";
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }
}
