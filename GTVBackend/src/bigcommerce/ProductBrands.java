/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.util.List;

/**
 *
 * @author Abani
 */
public class ProductBrands {
    private List<ProductBrand> productBrands;

    @Override
    public String toString() {
        return "ProductBrands{" + "productBrands=" + productBrands + '}';
    }

    public List<ProductBrand> getProductBrands() {
        return productBrands;
    }

    public void setProductBrands(List<ProductBrand> productBrands) {
        this.productBrands = productBrands;
    }
   
}
