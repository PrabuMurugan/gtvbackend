/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import com.google.gson.annotations.Expose;
import java.util.List;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * @author Abani
 */
@Generated("com.googlecode.jsonschema2pojo")
public class ProductOptionSku {

    @Expose
    private long id;
    @Expose
    private long product_id;
    @Expose
    private String sku;
    @Expose
    private String upc;
    @Expose
    private String cost_price;
    @Expose
    private long inventory_level;
    @Expose
    private long inventory_warning_level;
    @Expose
    private String bin_picking_number;
    @Expose
    private List<ProductOptionSkuOption> options;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getCost_price() {
        return cost_price;
    }

    public void setCost_price(String cost_price) {
        this.cost_price = cost_price;
    }

    public long getInventory_level() {
        return inventory_level;
    }

    public void setInventory_level(long inventory_level) {
        this.inventory_level = inventory_level;
    }

    public long getInventory_warning_level() {
        return inventory_warning_level;
    }

    public void setInventory_warning_level(long inventory_warning_level) {
        this.inventory_warning_level = inventory_warning_level;
    }

    public String getBin_picking_number() {
        return bin_picking_number;
    }

    public void setBin_picking_number(String bin_picking_number) {
        this.bin_picking_number = bin_picking_number;
    }

    public List<ProductOptionSkuOption> getOptions() {
        return options;
    }

    public void setOptions(List<ProductOptionSkuOption> options) {
        this.options = options;
    }
     
   
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }
}
