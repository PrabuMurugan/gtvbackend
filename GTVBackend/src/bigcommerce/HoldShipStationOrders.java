/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import com.google.gson.Gson;
import com.ss.ShipStationOrders;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.json.JSONException;
import shipstation.ShipStationConfig;

/**
 *
 * @author Raj
 */
public class HoldShipStationOrders {
    public static void main(String [] args) throws IOException, JSONException {
        System.out.println("Starting UpdateOrderStatus");
        ArrayList<String> aryStatus = new ArrayList<String>();
        aryStatus.add("12");
        aryStatus.add("13");
        BigCommerce bc = new BigCommerce();
        
        ArrayList<String> aryList = bc.getOrderByStatus(aryStatus);
        System.out.println("BigCommerce Admin Orders " + aryList);
        aryList = getOrderIds(aryList);
        System.out.println(aryList);
        if(aryList != null && aryList.size() > 0) {
            
            for(String strOrderId :aryList ) {
                ShipStationConfig spShipStationConfig = new ShipStationConfig(ShipStationConfig.SERVICE_UPDATE_ORDER_ADD_TAG, "POST", true);
                BufferedWriter bufferedWitter = new BufferedWriter(new OutputStreamWriter(spShipStationConfig.getOutputStream()));
                String json = String.format("{\"orderId\": %s,\"tagId\":%s}",strOrderId,"19754");
                System.out.println(json);
                bufferedWitter.write(json + "\n");
                bufferedWitter.flush();
                bufferedWitter.close();
                
                StringBuilder sb1 = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(spShipStationConfig.getInputStream()));
                String line = bufferedReader.readLine();
                while (line != null) {
                    sb1.append(line);
                    line = bufferedReader.readLine();
                }
                bufferedReader.close();
                String strResponse  = new String(sb1);
                System.out.println("Response : " + strResponse);
                if(strResponse.contains("Tag added successfully")) {
                    System.out.println("Potential Fradulent Order - Verification Needed Tag Added into Order : " + strOrderId );
                }
                System.out.println();
            }
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            c.setTime(new Date()); // Now use today date.
            c.add(Calendar.DATE, 7); // Adding 5 days
            String output = sdf.format(c.getTime());
            System.out.println("New Date : "+output);

            for(String strOrderId :aryList ) {
                ShipStationConfig spShipStationConfigHold = new ShipStationConfig(ShipStationConfig.SERVICE_UPDATE_ORDER_HOLD_UNTIL, "POST", true);
                BufferedWriter bufferedWitterHold = new BufferedWriter(new OutputStreamWriter(spShipStationConfigHold.getOutputStream()));
                String json = String.format("{\"orderId\": %s,\"holdUntilDate\":\"%s\"}",strOrderId,output);
                bufferedWitterHold.write(json + "\n");
                System.out.println(json);
                bufferedWitterHold.flush();
                
                StringBuilder sb1 = new StringBuilder();
                BufferedReader bufferedReaderHold = new BufferedReader(new InputStreamReader(spShipStationConfigHold.getInputStream()));
                String line = bufferedReaderHold.readLine();
                while (line != null) {
                    sb1.append(line);
                    line = bufferedReaderHold.readLine();
                }
                String strResponse  = new String(sb1);
                System.out.println("Response : " + strResponse);
                if(strResponse.contains("Order held successfully.")) {
                    System.out.println("Order : " + strOrderId + " holded." );
                }
                System.out.println();
            }
            
            
            System.out.println("All Done......" );
        }
        System.out.println("Existing UpdateOrderStatus");
    }

    private static ArrayList<String> getOrderIds(ArrayList<String> aryList) throws IOException, JSONException {
        ArrayList<String> aryString = new ArrayList<String>();
        for(String strOrderId :aryList ) {
                ShipStationConfig spShipStationConfig = new ShipStationConfig(String.format(ShipStationConfig.SERVICE_ORDER_DETAIL,strOrderId), "GET", false);
                
                StringBuilder sb1 = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(spShipStationConfig.getInputStream()));
                String line = bufferedReader.readLine();
                while (line != null) {
                    sb1.append(line);
                    line = bufferedReader.readLine();
                }
                bufferedReader.close();
                String strResponse  = new String(sb1);
                ShipStationOrders myModelList = new Gson().fromJson(strResponse, ShipStationOrders.class);
                
                for ( com.ss.Order o : myModelList.getOrders()) {
                  if(o.getOrderNumber().equalsIgnoreCase(strOrderId)) {
                      aryString.add(""+o.getOrderId());
                      System.out.println("Ordernumber : " + strOrderId + "\t Order ID : " + o.getOrderId());
                  }
                }
                
        }
        return aryString;
        
    }
}
