
package bigcommerce.json;

import bigcommerce.orders.ConfigurableFields;
import bigcommerce.orders.Product_option;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("com.googlecode.jsonschema2pojo")
public class OrderedProduct {

    @Expose
    private int id;
    @Expose
    private int order_id;
    @Expose
    private long product_id;
    @Expose
    private long order_address_id;
    @Expose
    private String name;
    @Expose
    private String sku;
    @Expose
    private String type;
    @Expose
    private String base_price;
    @Expose
    private String price_ex_tax;
    @Expose
    private String price_inc_tax;
    @Expose
    private String price_tax;
    @Expose
    private String base_total;
    @Expose
    private String total_ex_tax;
    @Expose
    private String total_inc_tax;
    @Expose
    private String total_tax;
    @Expose
    private String weight;
    @Expose
    private long quantity;
    @Expose
    private String base_cost_price;
    @Expose
    private String cost_price_inc_tax;
    @Expose
    private String cost_price_ex_tax;
    @Expose
    private String cost_price_tax;
    @Expose
    private boolean is_refunded;
    @Expose
    private String refund_amount;
    @Expose
    private long return_id;
    @Expose
    private String wrapping_name;
    @Expose
    private String base_wrapping_cost;
    @Expose
    private String wrapping_cost_ex_tax;
    @Expose
    private String wrapping_cost_inc_tax;
    @Expose
    private String wrapping_cost_tax;
    @Expose
    private String wrapping_message;
    @Expose
    private long quantity_shipped;
    @Expose
    private Object event_name;
    @Expose
    private String event_date;
    @Expose
    private String fixed_shipping_cost;
    @Expose
    private String ebay_item_id;
    @Expose
    private String ebay_transaction_id;
    @Expose
    private Object option_set_id;
    @Expose
    private Object parent_order_product_id;
    @SerializedName("is_bundled_product ")
    @Expose
    private boolean is_bundled_product_;
    @Expose
    private String bin_picking_number;
    @Expose
    private List<Applied_discount> applied_discounts = new ArrayList<Applied_discount>();
    @Expose
    private List<Product_option> product_options = new ArrayList<Product_option>();
    @Expose
    private List<ConfigurableFields> configurable_fields = new ArrayList<ConfigurableFields>();

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public long getOrder_address_id() {
        return order_address_id;
    }

    public void setOrder_address_id(long order_address_id) {
        this.order_address_id = order_address_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBase_price() {
        return base_price;
    }

    public void setBase_price(String base_price) {
        this.base_price = base_price;
    }

    public String getPrice_ex_tax() {
        return price_ex_tax;
    }

    public void setPrice_ex_tax(String price_ex_tax) {
        this.price_ex_tax = price_ex_tax;
    }

    public String getPrice_inc_tax() {
        return price_inc_tax;
    }

    public void setPrice_inc_tax(String price_inc_tax) {
        this.price_inc_tax = price_inc_tax;
    }

    public String getPrice_tax() {
        return price_tax;
    }

    public void setPrice_tax(String price_tax) {
        this.price_tax = price_tax;
    }

    public String getBase_total() {
        return base_total;
    }

    public void setBase_total(String base_total) {
        this.base_total = base_total;
    }

    public String getTotal_ex_tax() {
        return total_ex_tax;
    }

    public void setTotal_ex_tax(String total_ex_tax) {
        this.total_ex_tax = total_ex_tax;
    }

    public String getTotal_inc_tax() {
        return total_inc_tax;
    }

    public void setTotal_inc_tax(String total_inc_tax) {
        this.total_inc_tax = total_inc_tax;
    }

    public String getTotal_tax() {
        return total_tax;
    }

    public void setTotal_tax(String total_tax) {
        this.total_tax = total_tax;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getBase_cost_price() {
        return base_cost_price;
    }

    public void setBase_cost_price(String base_cost_price) {
        this.base_cost_price = base_cost_price;
    }

    public String getCost_price_inc_tax() {
        return cost_price_inc_tax;
    }

    public void setCost_price_inc_tax(String cost_price_inc_tax) {
        this.cost_price_inc_tax = cost_price_inc_tax;
    }

    public String getCost_price_ex_tax() {
        return cost_price_ex_tax;
    }

    public void setCost_price_ex_tax(String cost_price_ex_tax) {
        this.cost_price_ex_tax = cost_price_ex_tax;
    }

    public String getCost_price_tax() {
        return cost_price_tax;
    }

    public void setCost_price_tax(String cost_price_tax) {
        this.cost_price_tax = cost_price_tax;
    }

    public boolean isIs_refunded() {
        return is_refunded;
    }

    public void setIs_refunded(boolean is_refunded) {
        this.is_refunded = is_refunded;
    }

    public String getRefund_amount() {
        return refund_amount;
    }

    public void setRefund_amount(String refund_amount) {
        this.refund_amount = refund_amount;
    }

    public long getReturn_id() {
        return return_id;
    }

    public void setReturn_id(long return_id) {
        this.return_id = return_id;
    }

    public String getWrapping_name() {
        return wrapping_name;
    }

    public void setWrapping_name(String wrapping_name) {
        this.wrapping_name = wrapping_name;
    }

    public String getBase_wrapping_cost() {
        return base_wrapping_cost;
    }

    public void setBase_wrapping_cost(String base_wrapping_cost) {
        this.base_wrapping_cost = base_wrapping_cost;
    }

    public String getWrapping_cost_ex_tax() {
        return wrapping_cost_ex_tax;
    }

    public void setWrapping_cost_ex_tax(String wrapping_cost_ex_tax) {
        this.wrapping_cost_ex_tax = wrapping_cost_ex_tax;
    }

    public String getWrapping_cost_inc_tax() {
        return wrapping_cost_inc_tax;
    }

    public void setWrapping_cost_inc_tax(String wrapping_cost_inc_tax) {
        this.wrapping_cost_inc_tax = wrapping_cost_inc_tax;
    }

    public String getWrapping_cost_tax() {
        return wrapping_cost_tax;
    }

    public void setWrapping_cost_tax(String wrapping_cost_tax) {
        this.wrapping_cost_tax = wrapping_cost_tax;
    }

    public String getWrapping_message() {
        return wrapping_message;
    }

    public void setWrapping_message(String wrapping_message) {
        this.wrapping_message = wrapping_message;
    }

    public long getQuantity_shipped() {
        return quantity_shipped;
    }

    public void setQuantity_shipped(long quantity_shipped) {
        this.quantity_shipped = quantity_shipped;
    }

    public Object getEvent_name() {
        return event_name;
    }

    public void setEvent_name(Object event_name) {
        this.event_name = event_name;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public String getFixed_shipping_cost() {
        return fixed_shipping_cost;
    }

    public void setFixed_shipping_cost(String fixed_shipping_cost) {
        this.fixed_shipping_cost = fixed_shipping_cost;
    }

    public String getEbay_item_id() {
        return ebay_item_id;
    }

    public void setEbay_item_id(String ebay_item_id) {
        this.ebay_item_id = ebay_item_id;
    }

    public String getEbay_transaction_id() {
        return ebay_transaction_id;
    }

    public void setEbay_transaction_id(String ebay_transaction_id) {
        this.ebay_transaction_id = ebay_transaction_id;
    }

    public Object getOption_set_id() {
        return option_set_id;
    }

    public void setOption_set_id(Object option_set_id) {
        this.option_set_id = option_set_id;
    }

    public Object getParent_order_product_id() {
        return parent_order_product_id;
    }

    public void setParent_order_product_id(Object parent_order_product_id) {
        this.parent_order_product_id = parent_order_product_id;
    }

    public boolean isIs_bundled_product_() {
        return is_bundled_product_;
    }

    public void setIs_bundled_product_(boolean is_bundled_product_) {
        this.is_bundled_product_ = is_bundled_product_;
    }

    public String getBin_picking_number() {
        return bin_picking_number;
    }

    public void setBin_picking_number(String bin_picking_number) {
        this.bin_picking_number = bin_picking_number;
    }

    public List<Applied_discount> getApplied_discounts() {
        return applied_discounts;
    }

    public void setApplied_discounts(List<Applied_discount> applied_discounts) {
        this.applied_discounts = applied_discounts;
    }

    public List<Product_option> getProduct_options() {
        return product_options;
    }

    public void setProduct_options(List<Product_option> product_options) {
        this.product_options = product_options;
    }

    public List<ConfigurableFields> getConfigurable_fields() {
        return configurable_fields;
    }

    public void setConfigurable_fields(List<ConfigurableFields> configurable_fields) {
        this.configurable_fields = configurable_fields;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

}
