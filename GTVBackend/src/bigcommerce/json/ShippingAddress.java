
package bigcommerce.json;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("com.googlecode.jsonschema2pojo")
public class ShippingAddress {

    @Expose
    private long id;
    @Expose
    private long order_id;
    @Expose
    private String first_name;
    @Expose
    private String last_name;
    @Expose
    private String company;
    @Expose
    private String street_1;
    @Expose
    private String street_2;
    @Expose
    private String city;
    @Expose
    private String zip;
    @Expose
    private String country;
    @Expose
    private String country_iso2;
    @Expose
    private String state;
    @Expose
    private String email;
    @Expose
    private String phone;
    @Expose
    private long items_total;
    @Expose
    private long items_shipped;
    @Expose
    private String shipping_method;
    @Expose
    private String base_cost;
    @Expose
    private String cost_ex_tax;
    @Expose
    private String cost_inc_tax;
    @Expose
    private String cost_tax;
    @Expose
    private long cost_tax_class_id;
    @Expose
    private String base_handling_cost;
    @Expose
    private String handling_cost_ex_tax;
    @Expose
    private String handling_cost_inc_tax;
    @Expose
    private String handling_cost_tax;
    @Expose
    private long handling_cost_tax_class_id;
    @Expose
    private long shipping_zone_id;
    @Expose
    private String shipping_zone_name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(long order_id) {
        this.order_id = order_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getStreet_1() {
        return street_1;
    }

    public void setStreet_1(String street_1) {
        this.street_1 = street_1;
    }

    public String getStreet_2() {
        return street_2;
    }

    public void setStreet_2(String street_2) {
        this.street_2 = street_2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry_iso2() {
        return country_iso2;
    }

    public void setCountry_iso2(String country_iso2) {
        this.country_iso2 = country_iso2;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getItems_total() {
        return items_total;
    }

    public void setItems_total(long items_total) {
        this.items_total = items_total;
    }

    public long getItems_shipped() {
        return items_shipped;
    }

    public void setItems_shipped(long items_shipped) {
        this.items_shipped = items_shipped;
    }

    public String getShipping_method() {
        return shipping_method;
    }

    public void setShipping_method(String shipping_method) {
        this.shipping_method = shipping_method;
    }

    public String getBase_cost() {
        return base_cost;
    }

    public void setBase_cost(String base_cost) {
        this.base_cost = base_cost;
    }

    public String getCost_ex_tax() {
        return cost_ex_tax;
    }

    public void setCost_ex_tax(String cost_ex_tax) {
        this.cost_ex_tax = cost_ex_tax;
    }

    public String getCost_inc_tax() {
        return cost_inc_tax;
    }

    public void setCost_inc_tax(String cost_inc_tax) {
        this.cost_inc_tax = cost_inc_tax;
    }

    public String getCost_tax() {
        return cost_tax;
    }

    public void setCost_tax(String cost_tax) {
        this.cost_tax = cost_tax;
    }

    public long getCost_tax_class_id() {
        return cost_tax_class_id;
    }

    public void setCost_tax_class_id(long cost_tax_class_id) {
        this.cost_tax_class_id = cost_tax_class_id;
    }

    public String getBase_handling_cost() {
        return base_handling_cost;
    }

    public void setBase_handling_cost(String base_handling_cost) {
        this.base_handling_cost = base_handling_cost;
    }

    public String getHandling_cost_ex_tax() {
        return handling_cost_ex_tax;
    }

    public void setHandling_cost_ex_tax(String handling_cost_ex_tax) {
        this.handling_cost_ex_tax = handling_cost_ex_tax;
    }

    public String getHandling_cost_inc_tax() {
        return handling_cost_inc_tax;
    }

    public void setHandling_cost_inc_tax(String handling_cost_inc_tax) {
        this.handling_cost_inc_tax = handling_cost_inc_tax;
    }

    public String getHandling_cost_tax() {
        return handling_cost_tax;
    }

    public void setHandling_cost_tax(String handling_cost_tax) {
        this.handling_cost_tax = handling_cost_tax;
    }

    public long getHandling_cost_tax_class_id() {
        return handling_cost_tax_class_id;
    }

    public void setHandling_cost_tax_class_id(long handling_cost_tax_class_id) {
        this.handling_cost_tax_class_id = handling_cost_tax_class_id;
    }

    public long getShipping_zone_id() {
        return shipping_zone_id;
    }

    public void setShipping_zone_id(long shipping_zone_id) {
        this.shipping_zone_id = shipping_zone_id;
    }

    public String getShipping_zone_name() {
        return shipping_zone_name;
    }

    public void setShipping_zone_name(String shipping_zone_name) {
        this.shipping_zone_name = shipping_zone_name;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

}
