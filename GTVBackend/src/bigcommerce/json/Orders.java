
package bigcommerce.json;

import bigcommerce.orders.Billing_address;
import bigcommerce.orders.Coupons;
import bigcommerce.orders.Products;
import bigcommerce.orders.Shipping_addresses;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang3.StringUtils;

@Generated("com.googlecode.jsonschema2pojo")
public class Orders {

    @Expose
    private int id;
    @Expose
    private long customer_id;
    @Expose
    private String date_created;
    @Expose
    private String date_modified;
    @Expose
    private String date_shipped;
    @Expose
    private long status_id;
    @Expose
    private String status;
    @Expose
    private String subtotal_ex_tax;
    @Expose
    private String subtotal_inc_tax;
    @Expose
    private String subtotal_tax;
    @Expose
    private String base_shipping_cost;
    @Expose
    private String shipping_cost_ex_tax;
    @Expose
    private String shipping_cost_inc_tax;
    @Expose
    private String shipping_cost_tax;
    @Expose
    private long shipping_cost_tax_class_id;
    @Expose
    private String base_handling_cost;
    @Expose
    private String handling_cost_ex_tax;
    @Expose
    private String handling_cost_inc_tax;
    @Expose
    private String handling_cost_tax;
    @Expose
    private long handling_cost_tax_class_id;
    @Expose
    private String base_wrapping_cost;
    @Expose
    private String wrapping_cost_ex_tax;
    @Expose
    private String wrapping_cost_inc_tax;
    @Expose
    private String wrapping_cost_tax;
    @Expose
    private long wrapping_cost_tax_class_id;
    @Expose
    private String total_ex_tax;
    @Expose
    private String total_inc_tax;
    @Expose
    private String total_tax;
    @Expose
    private long items_total;
    @Expose
    private long items_shipped;
    @Expose
    private String payment_method;
    @Expose
    private Object payment_provider_id;
    @Expose
    private String payment_status;
    @Expose
    private String refunded_amount;
    @Expose
    private boolean order_is_digital;
    @Expose
    private String store_credit_amount;
    @Expose
    private String gift_certificate_amount;
    @Expose
    private String ip_address;
    @Expose
    private String geoip_country;
    @Expose
    private String geoip_country_iso2;
    @Expose
    private long currency_id;
    @Expose
    private String currency_code;
    @Expose
    private String currency_exchange_rate;
    @Expose
    private long default_currency_id;
    @Expose
    private String default_currency_code;
    @Expose
    private String staff_notes;
    @Expose
    private String customer_message;
    @Expose
    private String discount_amount;
    @Expose
    private String coupon_discount;
    @Expose
    private long shipping_address_count;
    @Expose
    private boolean is_deleted;
    @Expose
    private Billing_address billing_address;
    @Expose
    private Products products;
    @Expose
    private Shipping_addresses shipping_addresses;
    @Expose
    private Coupons coupons;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Orders withId(int id) {
        this.id = id;
        return this;
    }

    public long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(long customer_id) {
        this.customer_id = customer_id;
    }

    public Orders withCustomer_id(long customer_id) {
        this.customer_id = customer_id;
        return this;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public Orders withDate_created(String date_created) {
        this.date_created = date_created;
        return this;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(String date_modified) {
        this.date_modified = date_modified;
    }

    public Orders withDate_modified(String date_modified) {
        this.date_modified = date_modified;
        return this;
    }

    public String getDate_shipped() {
        return date_shipped;
    }

    public void setDate_shipped(String date_shipped) {
        this.date_shipped = date_shipped;
    }

    public Orders withDate_shipped(String date_shipped) {
        this.date_shipped = date_shipped;
        return this;
    }

    public long getStatus_id() {
        return status_id;
    }

    public void setStatus_id(long status_id) {
        this.status_id = status_id;
    }

    public Orders withStatus_id(long status_id) {
        this.status_id = status_id;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Orders withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getSubtotal_ex_tax() {
        return subtotal_ex_tax;
    }

    public void setSubtotal_ex_tax(String subtotal_ex_tax) {
        this.subtotal_ex_tax = subtotal_ex_tax;
    }

    public Orders withSubtotal_ex_tax(String subtotal_ex_tax) {
        this.subtotal_ex_tax = subtotal_ex_tax;
        return this;
    }

    public String getSubtotal_inc_tax() {
        return subtotal_inc_tax;
    }

    public void setSubtotal_inc_tax(String subtotal_inc_tax) {
        this.subtotal_inc_tax = subtotal_inc_tax;
    }

    public Orders withSubtotal_inc_tax(String subtotal_inc_tax) {
        this.subtotal_inc_tax = subtotal_inc_tax;
        return this;
    }

    public String getSubtotal_tax() {
        return subtotal_tax;
    }

    public void setSubtotal_tax(String subtotal_tax) {
        this.subtotal_tax = subtotal_tax;
    }

    public Orders withSubtotal_tax(String subtotal_tax) {
        this.subtotal_tax = subtotal_tax;
        return this;
    }

    public String getBase_shipping_cost() {
        return base_shipping_cost;
    }

    public void setBase_shipping_cost(String base_shipping_cost) {
        this.base_shipping_cost = base_shipping_cost;
    }

    public Orders withBase_shipping_cost(String base_shipping_cost) {
        this.base_shipping_cost = base_shipping_cost;
        return this;
    }

    public String getShipping_cost_ex_tax() {
        return shipping_cost_ex_tax;
    }

    public void setShipping_cost_ex_tax(String shipping_cost_ex_tax) {
        this.shipping_cost_ex_tax = shipping_cost_ex_tax;
    }

    public Orders withShipping_cost_ex_tax(String shipping_cost_ex_tax) {
        this.shipping_cost_ex_tax = shipping_cost_ex_tax;
        return this;
    }

    public String getShipping_cost_inc_tax() {
        return shipping_cost_inc_tax;
    }

    public void setShipping_cost_inc_tax(String shipping_cost_inc_tax) {
        this.shipping_cost_inc_tax = shipping_cost_inc_tax;
    }

    public Orders withShipping_cost_inc_tax(String shipping_cost_inc_tax) {
        this.shipping_cost_inc_tax = shipping_cost_inc_tax;
        return this;
    }

    public String getShipping_cost_tax() {
        return shipping_cost_tax;
    }

    public void setShipping_cost_tax(String shipping_cost_tax) {
        this.shipping_cost_tax = shipping_cost_tax;
    }

    public Orders withShipping_cost_tax(String shipping_cost_tax) {
        this.shipping_cost_tax = shipping_cost_tax;
        return this;
    }

    public long getShipping_cost_tax_class_id() {
        return shipping_cost_tax_class_id;
    }

    public void setShipping_cost_tax_class_id(long shipping_cost_tax_class_id) {
        this.shipping_cost_tax_class_id = shipping_cost_tax_class_id;
    }

    public Orders withShipping_cost_tax_class_id(long shipping_cost_tax_class_id) {
        this.shipping_cost_tax_class_id = shipping_cost_tax_class_id;
        return this;
    }

    public String getBase_handling_cost() {
        return base_handling_cost;
    }

    public void setBase_handling_cost(String base_handling_cost) {
        this.base_handling_cost = base_handling_cost;
    }

    public Orders withBase_handling_cost(String base_handling_cost) {
        this.base_handling_cost = base_handling_cost;
        return this;
    }

    public String getHandling_cost_ex_tax() {
        return handling_cost_ex_tax;
    }

    public void setHandling_cost_ex_tax(String handling_cost_ex_tax) {
        this.handling_cost_ex_tax = handling_cost_ex_tax;
    }

    public Orders withHandling_cost_ex_tax(String handling_cost_ex_tax) {
        this.handling_cost_ex_tax = handling_cost_ex_tax;
        return this;
    }

    public String getHandling_cost_inc_tax() {
        return handling_cost_inc_tax;
    }

    public void setHandling_cost_inc_tax(String handling_cost_inc_tax) {
        this.handling_cost_inc_tax = handling_cost_inc_tax;
    }

    public Orders withHandling_cost_inc_tax(String handling_cost_inc_tax) {
        this.handling_cost_inc_tax = handling_cost_inc_tax;
        return this;
    }

    public String getHandling_cost_tax() {
        return handling_cost_tax;
    }

    public void setHandling_cost_tax(String handling_cost_tax) {
        this.handling_cost_tax = handling_cost_tax;
    }

    public Orders withHandling_cost_tax(String handling_cost_tax) {
        this.handling_cost_tax = handling_cost_tax;
        return this;
    }

    public long getHandling_cost_tax_class_id() {
        return handling_cost_tax_class_id;
    }

    public void setHandling_cost_tax_class_id(long handling_cost_tax_class_id) {
        this.handling_cost_tax_class_id = handling_cost_tax_class_id;
    }

    public Orders withHandling_cost_tax_class_id(long handling_cost_tax_class_id) {
        this.handling_cost_tax_class_id = handling_cost_tax_class_id;
        return this;
    }

    public String getBase_wrapping_cost() {
        return base_wrapping_cost;
    }

    public void setBase_wrapping_cost(String base_wrapping_cost) {
        this.base_wrapping_cost = base_wrapping_cost;
    }

    public Orders withBase_wrapping_cost(String base_wrapping_cost) {
        this.base_wrapping_cost = base_wrapping_cost;
        return this;
    }

    public String getWrapping_cost_ex_tax() {
        return wrapping_cost_ex_tax;
    }

    public void setWrapping_cost_ex_tax(String wrapping_cost_ex_tax) {
        this.wrapping_cost_ex_tax = wrapping_cost_ex_tax;
    }

    public Orders withWrapping_cost_ex_tax(String wrapping_cost_ex_tax) {
        this.wrapping_cost_ex_tax = wrapping_cost_ex_tax;
        return this;
    }

    public String getWrapping_cost_inc_tax() {
        return wrapping_cost_inc_tax;
    }

    public void setWrapping_cost_inc_tax(String wrapping_cost_inc_tax) {
        this.wrapping_cost_inc_tax = wrapping_cost_inc_tax;
    }

    public Orders withWrapping_cost_inc_tax(String wrapping_cost_inc_tax) {
        this.wrapping_cost_inc_tax = wrapping_cost_inc_tax;
        return this;
    }

    public String getWrapping_cost_tax() {
        return wrapping_cost_tax;
    }

    public void setWrapping_cost_tax(String wrapping_cost_tax) {
        this.wrapping_cost_tax = wrapping_cost_tax;
    }

    public Orders withWrapping_cost_tax(String wrapping_cost_tax) {
        this.wrapping_cost_tax = wrapping_cost_tax;
        return this;
    }

    public long getWrapping_cost_tax_class_id() {
        return wrapping_cost_tax_class_id;
    }

    public void setWrapping_cost_tax_class_id(long wrapping_cost_tax_class_id) {
        this.wrapping_cost_tax_class_id = wrapping_cost_tax_class_id;
    }

    public Orders withWrapping_cost_tax_class_id(long wrapping_cost_tax_class_id) {
        this.wrapping_cost_tax_class_id = wrapping_cost_tax_class_id;
        return this;
    }

    public String getTotal_ex_tax() {
        return total_ex_tax;
    }

    public void setTotal_ex_tax(String total_ex_tax) {
        this.total_ex_tax = total_ex_tax;
    }

    public Orders withTotal_ex_tax(String total_ex_tax) {
        this.total_ex_tax = total_ex_tax;
        return this;
    }

    public String getTotal_inc_tax() {
        return total_inc_tax;
    }

    public void setTotal_inc_tax(String total_inc_tax) {
        this.total_inc_tax = total_inc_tax;
    }

    public Orders withTotal_inc_tax(String total_inc_tax) {
        this.total_inc_tax = total_inc_tax;
        return this;
    }

    public String getTotal_tax() {
        return total_tax;
    }

    public void setTotal_tax(String total_tax) {
        this.total_tax = total_tax;
    }

    public Orders withTotal_tax(String total_tax) {
        this.total_tax = total_tax;
        return this;
    }

    public long getItems_total() {
        return items_total;
    }

    public void setItems_total(long items_total) {
        this.items_total = items_total;
    }

    public Orders withItems_total(long items_total) {
        this.items_total = items_total;
        return this;
    }

    public long getItems_shipped() {
        return items_shipped;
    }

    public void setItems_shipped(long items_shipped) {
        this.items_shipped = items_shipped;
    }

    public Orders withItems_shipped(long items_shipped) {
        this.items_shipped = items_shipped;
        return this;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public Orders withPayment_method(String payment_method) {
        this.payment_method = payment_method;
        return this;
    }

    public Object getPayment_provider_id() {
        return payment_provider_id;
    }

    public void setPayment_provider_id(Object payment_provider_id) {
        this.payment_provider_id = payment_provider_id;
    }

    public Orders withPayment_provider_id(Object payment_provider_id) {
        this.payment_provider_id = payment_provider_id;
        return this;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public Orders withPayment_status(String payment_status) {
        this.payment_status = payment_status;
        return this;
    }

    public String getRefunded_amount() {
        return refunded_amount;
    }

    public void setRefunded_amount(String refunded_amount) {
        this.refunded_amount = refunded_amount;
    }

    public Orders withRefunded_amount(String refunded_amount) {
        this.refunded_amount = refunded_amount;
        return this;
    }

    public boolean isOrder_is_digital() {
        return order_is_digital;
    }

    public void setOrder_is_digital(boolean order_is_digital) {
        this.order_is_digital = order_is_digital;
    }

    public Orders withOrder_is_digital(boolean order_is_digital) {
        this.order_is_digital = order_is_digital;
        return this;
    }

    public String getStore_credit_amount() {
        return store_credit_amount;
    }

    public void setStore_credit_amount(String store_credit_amount) {
        this.store_credit_amount = store_credit_amount;
    }

    public Orders withStore_credit_amount(String store_credit_amount) {
        this.store_credit_amount = store_credit_amount;
        return this;
    }

    public String getGift_certificate_amount() {
        return gift_certificate_amount;
    }

    public void setGift_certificate_amount(String gift_certificate_amount) {
        this.gift_certificate_amount = gift_certificate_amount;
    }

    public Orders withGift_certificate_amount(String gift_certificate_amount) {
        this.gift_certificate_amount = gift_certificate_amount;
        return this;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public Orders withIp_address(String ip_address) {
        this.ip_address = ip_address;
        return this;
    }

    public String getGeoip_country() {
        return geoip_country;
    }

    public void setGeoip_country(String geoip_country) {
        this.geoip_country = geoip_country;
    }

    public Orders withGeoip_country(String geoip_country) {
        this.geoip_country = geoip_country;
        return this;
    }

    public String getGeoip_country_iso2() {
        return geoip_country_iso2;
    }

    public void setGeoip_country_iso2(String geoip_country_iso2) {
        this.geoip_country_iso2 = geoip_country_iso2;
    }

    public Orders withGeoip_country_iso2(String geoip_country_iso2) {
        this.geoip_country_iso2 = geoip_country_iso2;
        return this;
    }

    public long getCurrency_id() {
        return currency_id;
    }

    public void setCurrency_id(long currency_id) {
        this.currency_id = currency_id;
    }

    public Orders withCurrency_id(long currency_id) {
        this.currency_id = currency_id;
        return this;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public Orders withCurrency_code(String currency_code) {
        this.currency_code = currency_code;
        return this;
    }

    public String getCurrency_exchange_rate() {
        return currency_exchange_rate;
    }

    public void setCurrency_exchange_rate(String currency_exchange_rate) {
        this.currency_exchange_rate = currency_exchange_rate;
    }

    public Orders withCurrency_exchange_rate(String currency_exchange_rate) {
        this.currency_exchange_rate = currency_exchange_rate;
        return this;
    }

    public long getDefault_currency_id() {
        return default_currency_id;
    }

    public void setDefault_currency_id(long default_currency_id) {
        this.default_currency_id = default_currency_id;
    }

    public Orders withDefault_currency_id(long default_currency_id) {
        this.default_currency_id = default_currency_id;
        return this;
    }

    public String getDefault_currency_code() {
        return default_currency_code;
    }

    public void setDefault_currency_code(String default_currency_code) {
        this.default_currency_code = default_currency_code;
    }

    public Orders withDefault_currency_code(String default_currency_code) {
        this.default_currency_code = default_currency_code;
        return this;
    }

    public String getStaff_notes() {
        return staff_notes;
    }

    public void setStaff_notes(String staff_notes) {
        this.staff_notes = staff_notes;
    }

    public Orders withStaff_notes(String staff_notes) {
        this.staff_notes = staff_notes;
        return this;
    }

    public String getCustomer_message() {
        if(customer_message!=null&& customer_message.length()>1000)
            return StringUtils.substring(customer_message, 0,999);
        else
            return customer_message;
    }

    public void setCustomer_message(String customer_message) {
        this.customer_message = customer_message;
    }

    public Orders withCustomer_message(String customer_message) {
        this.customer_message = customer_message;
        return this;
    }

    public String getDiscount_amount() {
        return discount_amount;
    }

    public void setDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
    }

    public Orders withDiscount_amount(String discount_amount) {
        this.discount_amount = discount_amount;
        return this;
    }

    public String getCoupon_discount() {
        return coupon_discount;
    }

    public void setCoupon_discount(String coupon_discount) {
        this.coupon_discount = coupon_discount;
    }

    public Orders withCoupon_discount(String coupon_discount) {
        this.coupon_discount = coupon_discount;
        return this;
    }

    public long getShipping_address_count() {
        return shipping_address_count;
    }

    public void setShipping_address_count(long shipping_address_count) {
        this.shipping_address_count = shipping_address_count;
    }

    public Orders withShipping_address_count(long shipping_address_count) {
        this.shipping_address_count = shipping_address_count;
        return this;
    }

    public boolean isIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    public Orders withIs_deleted(boolean is_deleted) {
        this.is_deleted = is_deleted;
        return this;
    }

    public Billing_address getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(Billing_address billing_address) {
        this.billing_address = billing_address;
    }

    public Orders withBilling_address(Billing_address billing_address) {
        this.billing_address = billing_address;
        return this;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public Orders withProducts(Products products) {
        this.products = products;
        return this;
    }

    public Shipping_addresses getShipping_addresses() {
        return shipping_addresses;
    }

    public void setShipping_addresses(Shipping_addresses shipping_addresses) {
        this.shipping_addresses = shipping_addresses;
    }

    public Orders withShipping_addresses(Shipping_addresses shipping_addresses) {
        this.shipping_addresses = shipping_addresses;
        return this;
    }

    public Coupons getCoupons() {
        return coupons;
    }

    public void setCoupons(Coupons coupons) {
        this.coupons = coupons;
    }

    public Orders withCoupons(Coupons coupons) {
        this.coupons = coupons;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

}
