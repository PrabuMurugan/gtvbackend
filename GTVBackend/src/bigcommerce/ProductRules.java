/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.util.List;

/**
 *
 * @author Abani
 */
public class ProductRules {
    private List<ProductRule> productRules;

    @Override
    public String toString() {
        return "ProductRules{" + "productRules=" + productRules + '}';
    }

    public List<ProductRule> getProductRules() {
        return productRules;
    }

    public void setProductRules(List<ProductRule> productRules) {
        this.productRules = productRules;
    }
    
}
