/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.util.List;

/**
 *
 * @author Abani
 */
public class ProductSkus {
     private List<ProductSku> productSkus;

    @Override
    public String toString() {
        return "ProductSkus{" + "productSkus=" + productSkus + '}';
    }

    public List<ProductSku> getProductSkus() {
        return productSkus;
    }

    public void setProductSkus(List<ProductSku> productSkus) {
        this.productSkus = productSkus;
    }
    
    
}
