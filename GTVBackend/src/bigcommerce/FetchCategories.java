package bigcommerce;

/**
 *
 * @author Raj
 */
import bombayplaza.pricematch.RetrievePage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;

public class FetchCategories {
    private static String storeURL;
    private static String token;
    static String EXCLUDE_UNFI  =null;
    static ArrayList<Long> processedIds=new ArrayList();
    public static String authString ;
    public static void main(String[] args) throws Exception {
        System.out.println("Starting FetchProducts");
        Properties props = new Properties();
        FetchCategories fetchProducts = new FetchCategories();
        Connection connection = null;
       connection=util.DB.getConnection("hb_bc");
         connection.close();
        /*-DEXCLUDE_UNFI=true need to be passed from batch file for excluding the range of product */
        EXCLUDE_UNFI=  System.getProperty("EXCLUDE_UNFI","true");
        try{
            try {
                if(EXCLUDE_UNFI.equals("true")){
                    
                    props.load(new FileInputStream(new File("C:/Google Drive/Dropbox/program/product_excludes.properties")));
                }
                String ENV = System.getProperty("ENV", "PRD");
                if(ENV.equals("QA")){
                   initQAStore();
                }else{
                   initProdStore(); 
                }
               connection = RetrievePage.getConnection();

            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
          
            System.out.println("Value of EXCLUDE_UNFI is "+EXCLUDE_UNFI);
            String username = "prabu";
              authString = username + ":" + token;
            fetchProducts.addAllCategories(authString, connection);
            fetchProducts.addAllBrands(authString, connection);
                        if(true){
                System.out.println("Only categories is needed");
                return;
            }            
            fetchProducts.tuncateProducts(connection);   
            

            

            if(true){
                System.out.println("This  FetchProducts program just fetch the categories and brands thats it.");
                System.exit(1);
            }
                
            if(EXCLUDE_UNFI.equals("true")){
                String max = props.getProperty("hb.bc.product.exclude.max.id");
                String min = props.getProperty("hb.bc.product.exclude.min.id");
                System.out.println("excluding product between: min:"+min+"max:"+max);
                /*Folowing will fetch all product above max product id */
                System.out.println(fetchProducts.fetchProducts(connection, Integer.valueOf(max), -1));
                /*Folowing will fetch all product win in min product id */
                System.out.println(fetchProducts.fetchProducts(connection, -1, Integer.valueOf(min)));
            }else{
                    System.out.println("THIS PROGRAM WILL RUN ONLY BY EXCLUDING UNFI PRODUCTS. PASS EXCLUDE_UNFI=true");
                    System.exit(1);

                 /*Folowing will fetch all product */
                System.out.println(fetchProducts.fetchProducts(connection, -1,-1));
                updateExcludeUNFIProducts(connection);
                prepareQuickViewProducts(connection);
            }
            //Below two lines can be removed later. This is just to get non UNFI products into hb_bc.product table so queries like http://localhost/HB5/Q?sno=67 can work without any issues
          //  System.out.println("Inserting into hb_bc.product all exported products");
                PreparedStatement pstTemp=connection.prepareStatement("insert into hb_bc.product (id,name,description,sku,custom_url,availability,price,upc)  select product_id,name,description,code,product_url,'available',price,product_upc_ean from hb_bc.exported_products_from_hb where  code not like 'UN_%'  and code not in (select sku from hb_bc.product) and product_id not in (select id from hb_bc.product)");
            pstTemp.executeUpdate();
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            try {
                connection.close();
            } catch (Exception ex) {
                Logger.getLogger(FetchCategories.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
         public static void prepareQuickViewProducts(Connection con) throws Exception{
         System.out.println("Inside prepareQuickViewProducts");
            PreparedStatement pst = null;
            ResultSet rs = null;
            System.out.println("truncate table quickview_product");
            pst=con.prepareStatement(" truncate table  quickview_product");
            pst.executeUpdate();  
            System.out.println("insert into    quickview_product");
            pst=con.prepareStatement(" insert into quickview_product select  p.id,concat('http://www.harrisburgstore.com',p.custom_url) custom_url,  concat('http://www.harrisburgstore.com/product_images/',pi.image_file) image_file,p.name name,p.price price,0,'b.name' brand_desc from product p left join product_images pi  on   p.id=pi.product_id and is_thumbnail='true'");
            pst.executeUpdate();  
            pst=con.prepareStatement(" insert into quickview_product select  p.id,concat('http://www.harrisburgstore.com',p.custom_url) custom_url,  concat('http://www.harrisburgstore.com/product_images/',pi.image_file) image_file,p.name name,p.price price,0,'b.name' brand_desc from unfi_hb_product p left join product_images pi  on   p.id=pi.product_id and is_thumbnail='true'");
            pst.executeUpdate();  
            
            System.out.println("update brand in    quickview_product");
            pst=con.prepareStatement(" update  quickview_product  qp set brand_desc =( select distinct name from brands b, product_brands pb  where  qp.id=pb.product_id and replace(pb.resource,'/brands/','')= b.id )");
            pst.executeUpdate();  
            

            con.close();
     }
    private static void updateExcludeUNFIProducts(Connection connection) throws Exception{
//If we are fetching all products, lets update program/product_excludes file
          PreparedStatement pst=connection.prepareStatement("select min(id),max(id) from hb_bc.unfi_hb_product where sku like 'UN_%'");
          ResultSet rs=pst.executeQuery();
          if(rs.next()){
              Long minUnfiID=rs.getLong(1);
              Long maxUnfiID=rs.getLong(2);
              FileWriter fstream_out = new FileWriter("C:/Google Drive/Dropbox/program/product_excludes.properties");
              BufferedWriter out = new BufferedWriter(fstream_out);
              out.write("# To change this template, choose Tools | Templates\n# and open the template in the editor.\n# hb.bc.product.exclude.min.id is for lower endpoint and hb.bc.product.exclude.max.id is for higher endpoint\nhb.bc.product.exclude.min.id="+minUnfiID+"\nhb.bc.product.exclude.max.id="+maxUnfiID);

              out.close();

          }
    }
   
    private static void initProdStore(){
        storeURL = "https://harrisburgstore.com";
        token = "1c215c55ab2f4a2b20ded8881a3ff16c";
         
    }
    
    private static void initQAStore(){
        storeURL = "https://store-0p5zzjir.mybigcommerce.com";
        token = "807c960064a4ef27b5516c7e53c842c1f41a1f7e";
        
    }
     private void tuncateProducts(Connection connection){
         try{
             
             PreparedStatement delete =null;
            if(EXCLUDE_UNFI.equals("false")){
                delete = connection.prepareStatement("delete from hb_bc.unfi_hb_product");
                delete.executeUpdate();                
            }
             
                delete = connection.prepareStatement("delete from hb_bc.categories");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.brands");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product_brands");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product_categories");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product_discount_rules");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product_configurable_fields");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product_custom_fields");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product_videos");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product_skus");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product_sku_options");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product_rules");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product_rule_conditions");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product_options");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.option_values");
                delete.execute();
                delete = connection.prepareStatement("delete from hb_bc.product_tax_class");
                delete.execute();
                
                delete = connection.prepareStatement("delete from hb_bc.product_images where product_id not in (select id from hb_bc.unfi_hb_product)");
                delete.execute();
         }catch(Exception e){
             e.printStackTrace();
             System.out.println("Exception during delete all:"+ e.getMessage());
         }
    }
     
    /*
     * For all product maxID and minID need to be -1
     */
     public static int totalCount=0;
    public String fetchProducts(Connection connection, int maxID, int minID) {
        
        try {
            Thread.sleep(1000*60*2);
            int i = 1;
           
            String username = "prabu";
            String authString = username + ":" + token;
            while (true) {
                String webPage = storeURL+"/api/v2/products.json?limit=200&page=" + i;
                if(maxID > 0){
                    /* It will set the url to fetch product above the maxID */
                    webPage = storeURL+"/api/v2/products.json?limit=200&min_id="+maxID+"&page=" + i;
                }else if(minID > 0){
                     /* It will set the url to fetch product within the minID */
                    webPage = storeURL+"/api/v2/products.json?limit=200&max_id="+minID+"&page=" + i;
                }
                try {
                    String result = null;
                    try{
                        System.out.println("FetchProducts -> Visiting webPage"+webPage);
                        result=getResultJSONString(authString, webPage);
                    }catch (Exception e2){
                        e2.printStackTrace();
                        if(STATUS_204||totalCount++>10){
                            System.out.println("I do not think I will have 150 pages 200 products each page 30K products..So lets break");
                            break;
                        }
                        if(STATUS_204==false){
                            System.out.println("Even though getResultJSONString failed, still continuing since STATUS_204 is not true");
                            continue;
                        }
                            
                    }
                    
                    String jsonString = "{\"products\":" + result + "}";
                    System.out.println("API response1 completed page:"+i);

                    Gson gson = new GsonBuilder().create();
                    Products products = gson.fromJson(jsonString, Products.class);
                       


                    PreparedStatement product_brands_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_brands (url, resource,product_id) VALUES ( ?, ?, ?)");
                    PreparedStatement product_categories_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_categories (cat_id ,product_id) VALUES ( ?, ?)");
                    PreparedStatement product_images_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_images (url, resource,product_id, id, image_file, is_thumbnail, sort_order, date_created, description) VALUES ( ?, ?, ?, ?, ?,?,?,?,?)");
                    PreparedStatement product_discount_rules_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_discount_rules (id,min, max, type, type_value,product_id) VALUES ( ?, ?, ?,?,?,?)");
                    PreparedStatement product_configurable_fields_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_configurable_fields (id, product_id,name, type,allowed_file_types, max_size, select_options, is_required, sort_order) VALUES ( ?, ?, ?,?,?,?,?,?,?)");
                    PreparedStatement product_custom_fields_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_custom_fields (id, name,product_id,text) VALUES ( ?, ?, ?,?)");
                    PreparedStatement product_videos_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_videos (id, sort_order,product_id, name) VALUES ( ?, ?, ?,?)");
                    PreparedStatement product_skus_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_skus (id, inventory_level,product_id, inventory_warning_level, bin_picking_number, sku, cost_price, upc) VALUES ( ?, ?, ?,?,?,?,?,?)");
                    PreparedStatement product_sku_options_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_sku_options (product_sku_id, option_value_id, product_option_id) VALUES ( ?, ?, ?)");
                    PreparedStatement product_rules_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_rules (id, sort_order,product_id, is_stop, is_enabled, price_adjuster, weight_adjuster,is_purchasing_disabled, is_purchasing_hidden, image_file, purchasing_disabled_message) VALUES ( ?, ?, ?,?,?,?,?,?,?,?,?)");
                    PreparedStatement product_rule_conditions_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_rule_conditions (product_option_id, option_value_id,sku_id, product_rule_id) VALUES ( ?, ?, ?,?)");
                    PreparedStatement product_options_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_options (id, is_required,product_id, option_id, display_name, sort_order) VALUES ( ?, ?, ?,?,?,?)");
                    PreparedStatement product_option_values_inserts = connection.prepareStatement("INSERT INTO hb_bc.option_values (option_value_id, option_id, label, sort_order, value) VALUES ( ?, ?, ?,?,?)");
                    PreparedStatement product_tax_class_inserts = connection.prepareStatement("INSERT INTO hb_bc.product_tax_class (product_taxclass_id, name,product_id) VALUES ( ?, ?, ?)");

                    PreparedStatement inserts=null;;
                                   
                    if (products != null && products.getProducts() != null && !products.getProducts().isEmpty()) {
                        List<Product> listProduct = products.getProducts();
                        System.out.println("Iteration " + (i - 1) + " Size of records " + listProduct.size());
                        for (Object obj : listProduct) {
                            Product product = (Product) obj;
                            System.out.println("SKU:"+product.getSku() +", Price:"+product.getPrice() +", Qty:"+ product.getInventory_level());
                           ;
                           
                           // If its unfi product just skip it because we are not deleting images which are from UNFI if we fetch all product and try to insert all images which are from unfi als
                           // it will give unique constraint exception
                            if(isUNFIProduct(product,connection)) {
                            continue;
                        }
                           if(processedIds.contains(product.getId())){
                               System.out.println("Skipping "+product.getId() +" since it is already processed");
                               continue;
                           }
                           processedIds.add(product.getId());
                           if(inserts!=null)
                               inserts.close();
                           if(product.getSku().indexOf("UN_")>=0){
                                       inserts = connection.prepareStatement("INSERT INTO hb_bc.unfi_hb_product (id,keyword_filter,name,"
                                    + "type,sku,description,search_keywords,availability_description,price,"
                                    + "cost_price,retail_price,sale_price,calculated_price,sort_order,`is_visible`,"
                                    + "`is_featured`,related_products,inventory_level,inventory_warning_level,warranty,"
                                    + "weight,width,height,depth,fixed_cost_shipping_price, `is_free_shipping`, inventory_tracking,"
                                    + "rating_total,rating_count,total_sold,date_created,brand_id,view_count, page_title,"
                                    + "meta_keywords,meta_description,layout_file, `is_price_hidden`, price_hidden_label,"
                                    + "date_modified, event_date_field_name, event_date_type, event_date_start, event_date_end,"
                                    + "myob_asset_account,  myob_income_account, myob_expense_account, `peachtree_gl_account`,"
                                    + "conditionhtml, `is_condition_shown`, preorder_release_date, `is_preorder_only`, `preordermessage`,"
                                    + "order_quantity_minimum,order_quantity_maximum, open_graph_type,open_graph_title,"
                                    + "open_graph_description, `is_open_graph_thumbnail`, upc, date_last_imported, option_set_id,"
                                    + "tax_class_id, option_set_display, bin_picking_number, custom_url, availability,option_set)"
                                    + " VALUES "
                                    + "(?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,?,?,?)");
                           }else{
                                inserts = connection.prepareStatement("INSERT INTO hb_bc.Product (id,keyword_filter,name,"
                                    + "type,sku,description,search_keywords,availability_description,price,"
                                    + "cost_price,retail_price,sale_price,calculated_price,sort_order,`is_visible`,"
                                    + "`is_featured`,related_products,inventory_level,inventory_warning_level,warranty,"
                                    + "weight,width,height,depth,fixed_cost_shipping_price, `is_free_shipping`, inventory_tracking,"
                                    + "rating_total,rating_count,total_sold,date_created,brand_id,view_count, page_title,"
                                    + "meta_keywords,meta_description,layout_file, `is_price_hidden`, price_hidden_label,"
                                    + "date_modified, event_date_field_name, event_date_type, event_date_start, event_date_end,"
                                    + "myob_asset_account,  myob_income_account, myob_expense_account, `peachtree_gl_account`,"
                                    + "conditionhtml, `is_condition_shown`, preorder_release_date, `is_preorder_only`, `preordermessage`,"
                                    + "order_quantity_minimum,order_quantity_maximum, open_graph_type,open_graph_title,"
                                    + "open_graph_description, `is_open_graph_thumbnail`, upc, date_last_imported, option_set_id,"
                                    + "tax_class_id, option_set_display, bin_picking_number, custom_url, availability,option_set)"
                                    + " VALUES "
                                    + "(?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,"
                                    + "?,?,?,?,?,?,?,?)");                               
                           }
                            inserts.setString(1, "" + product.getId());
                            if (product.getKeyword_filter() != null) {
                                inserts.setString(2, product.getKeyword_filter().toString());
                            } else {
                                inserts.setString(2, "");
                            }

                            inserts.setString(3, product.getName());
                            inserts.setString(4, "" + product.getType());
                            inserts.setString(5, "" + product.getSku());
                            if (product.getDescription() != null) {
                                inserts.setString(6, product.getDescription().length() > 5000 ? product.getDescription().substring(0, 5000) : product.getDescription());
                            } else {
                                inserts.setString(6, "");
                            }
                            inserts.setString(7, "" + product.getSearch_keywords());
                            inserts.setString(8, product.getAvailability_description());
                            inserts.setString(9, "" + product.getPrice());
                            inserts.setString(10, product.getCost_price());
                            inserts.setString(11, "" + product.getRetail_price());
                            inserts.setString(12, "" + product.getSale_price());
                            inserts.setString(13, product.getCalculated_price());
                            inserts.setString(14, "" + product.getSort_order());
                            inserts.setString(15, "" + product.isIs_visible());
                            inserts.setString(16, "" + product.isIs_featured());
                            inserts.setString(17, "" + product.getRelated_products());
                            inserts.setString(18, "" + product.getInventory_level());
                            inserts.setString(19, "" + product.getInventory_warning_level());
                            inserts.setString(20, "" + product.getWarranty());
                            inserts.setString(21, "" + product.getWeight());
                            inserts.setString(22, "" + product.getWidth());
                            inserts.setString(23, product.getHeight());

                            inserts.setString(24, product.getDepth());

                            inserts.setString(25, product.getFixed_cost_shipping_price());
                            inserts.setString(26, "" + product.isIs_free_shipping());
                            inserts.setString(27, product.getInventory_tracking());
                            inserts.setString(28, "" + product.getRating_total());
                            inserts.setString(29, "" + product.getRating_count());
                            inserts.setString(30, "" + product.getTotal_sold());
                            inserts.setString(31, product.getDate_created());
                            inserts.setString(32, "" + product.getBrand_id());
                            inserts.setString(33, "" + product.getView_count());
                            inserts.setString(34, "" + product.getPage_title());
                            inserts.setString(35, "" + product.getMeta_keywords());
                            inserts.setString(36, "" + product.getMeta_description());
                            inserts.setString(37, "" + product.getLayout_file());
                            inserts.setString(38, "" + product.isIs_price_hidden());
                            inserts.setString(39, "" + product.getPrice_hidden_label());
                            inserts.setString(40, product.getDate_modified());
                            inserts.setString(41, product.getEvent_date_field_name());
                            inserts.setString(42, product.getEvent_date_type());
                            inserts.setString(43, product.getEvent_date_start());
                            inserts.setString(44, product.getEvent_date_end());
                            inserts.setString(45, "" + product.getMyob_asset_account());
                            inserts.setString(46, "" + product.getMyob_income_account());
                            inserts.setString(47, "" + product.getMyob_expense_account());
                            System.out.println("peachtree_gl_account : " + product.getPeachtree_gl_account());
                            inserts.setString(48, "" + product.getPeachtree_gl_account());
                            if (product.getCondition() != null) {
                                inserts.setString(49, product.getCondition());
                            } else {
                                inserts.setString(49, "");
                            }
                            inserts.setString(50, "" + product.isIs_condition_shown());
                            inserts.setString(51, "" + product.getPreorder_release_date());
                            inserts.setString(52, "" + product.isIs_preorder_only());
                            inserts.setString(53, "" + product.getPreorder_message());
                            inserts.setString(54, "" + product.getOrder_quantity_minimum());
                            inserts.setString(55, "" + product.getOrder_quantity_maximum());
                            inserts.setString(56, "" + product.getOpen_graph_type());
                            inserts.setString(57, "" + product.getOpen_graph_title());
                            inserts.setString(58, "" + product.getOpen_graph_description());
                            inserts.setString(59, "" + product.isIs_open_graph_thumbnail());
                            inserts.setString(60, "" + product.getUpc());
                            inserts.setString(61, product.getDate_last_imported());
                            if (product.getOption_set_id() != null) {
                                inserts.setString(62, "" + product.getOption_set_id());
                            } else {
                                inserts.setString(62, "");
                            }
                            inserts.setString(63, "" + product.getTax_class_id());
                            inserts.setString(64, "" + product.getOption_set_display());
                            inserts.setString(65, product.getBin_picking_number());
                            inserts.setString(66, product.getCustom_url());
                            inserts.setString(67, product.getAvailability());
                            if (product.getOption_set() != null) {
                                inserts.setString(68, "" + product.getOption_set());
                            } else {
                                inserts.setString(68, "");
                            }
                        
                            try{
                             inserts.executeUpdate();
                            if (product.getBrand() != null) {
                                product_brands_inserts.setString(1, product.getBrand().getUrl());
                                product_brands_inserts.setString(2, product.getBrand().getResource());
                                product_brands_inserts.setString(3, "" + product.getId());
                                product_brands_inserts.addBatch();
                            }

                            if (product.getCategories() != null) {
                                for (Long catId : product.getCategories()) {
                                    product_categories_inserts.setString(1, "" + catId);
                                    product_categories_inserts.setString(2, "" + product.getId());
                                    product_categories_inserts.addBatch();
                                }
                            }

                            if (product.getImages() != null) {
                                ProdcutImages prodcutImages = getProdcutImages(product.getImages().getUrl(), authString);
                                if(prodcutImages != null && prodcutImages.getProdcutImages().size() > 0){
                                    for (ProdcutImage prodcutImage : prodcutImages.getProdcutImages()) {
                                        product_images_inserts.setString(1, product.getImages().getUrl());
                                        product_images_inserts.setString(2, product.getImages().getResource());
                                        product_images_inserts.setString(3, "" + product.getId());
                                        product_images_inserts.setString(4, "" + prodcutImage.getId());
                                        product_images_inserts.setString(5, prodcutImage.getImage_file());
                                        product_images_inserts.setString(6, "" + prodcutImage.isIs_thumbnail());
                                        product_images_inserts.setString(7, prodcutImage.getSort_order());
                                        product_images_inserts.setString(8, prodcutImage.getDate_created());
                                        product_images_inserts.setString(9, prodcutImage.getDescription());
                                        product_images_inserts.addBatch();
                                    }
                                }else{
                                    System.out.println("There is no images for product id: " + product.getId() +"Name:"+ product.getName());
                                }

                            }

                            if (false && product.getDiscount_rules() != null) {
                                Discountrules discountrules = getDiscountrules(authString, product.getDiscount_rules().getUrl().trim());
                                if (discountrules != null) {
                                    for (Discountrule discountrule : discountrules.getDiscountrules()) {
                                        product_discount_rules_inserts.setString(1, "" + discountrule.getId());
                                        product_discount_rules_inserts.setString(3, "" + discountrule.getMax());
                                        product_discount_rules_inserts.setString(2, "" + discountrule.getMin());
                                        product_discount_rules_inserts.setString(4, discountrule.getType());
                                        product_discount_rules_inserts.setString(5, "" + discountrule.getType_value());
                                        product_discount_rules_inserts.setString(6, "" + product.getId());
                                        product_discount_rules_inserts.addBatch();
                                    }
                                }
                            }

                            if (false && product.getConfigurable_fields() != null) {

                                Configurablefields configurablefields = getConfigurablefields(product.getConfigurable_fields().getUrl(), authString);
                                if (configurablefields != null) {
                                    for (Configurablefield configurablefield : configurablefields.getConfigurablefields()) {
                                        product_configurable_fields_inserts.setString(1, "" + configurablefield.getId());
                                        product_configurable_fields_inserts.setString(2, "" + configurablefield.getProduct_id());
                                        product_configurable_fields_inserts.setString(3, configurablefield.getName());
                                        product_configurable_fields_inserts.setString(4, configurablefield.getType());
                                        product_configurable_fields_inserts.setString(5, configurablefield.getAllowed_file_types());
                                        product_configurable_fields_inserts.setString(6, "" + configurablefield.getMax_size());
                                        product_configurable_fields_inserts.setString(7, configurablefield.getSelect_options());
                                        product_configurable_fields_inserts.setString(8, "" + configurablefield.isIs_required());
                                        product_configurable_fields_inserts.setString(9, "" + configurablefield.getSort_order());
                                        product_configurable_fields_inserts.addBatch();
                                    }
                                }

                            }

                            if (false && product.getSku().indexOf("UN_")<0 &&
                                product.getCustom_fields() != null) {
                                Customfields customfields = getCustomfields(product.getCustom_fields().getUrl(), authString);
                                if (customfields != null) {
                                    for (Customfield customfield : customfields.getCustomfields()) {
                                        product_custom_fields_inserts.setString(1, "" + customfield.getId());
                                        product_custom_fields_inserts.setString(2, customfield.getName());
                                        product_custom_fields_inserts.setString(3, "" + product.getId());
                                        product_custom_fields_inserts.setString(4, customfield.getText());
                                        product_custom_fields_inserts.addBatch();
                                    }

                                }
                            }

                            if (false && product.getVideos() != null) {
                                ProductVideos productVideos = getProductVideos(product.getVideos().getUrl(), authString);
                                if (productVideos != null) {
                                    for (ProductVideo productVideo : productVideos.getProductVideos()) {
                                        product_videos_inserts.setString(1, "" + productVideo.getId());
                                        product_videos_inserts.setString(2, "" + productVideo.getSort_order());
                                        product_videos_inserts.setString(3, "" + product.getId());
                                        product_videos_inserts.setString(4, "" + productVideo.getName());
                                        product_videos_inserts.addBatch();
                                    }
                                }

                            }

                            if (product.getSku().indexOf("HBGF")>=0 &&product.getSkus() != null) {
                                ProductSkus productSkus = getProductSkus(product.getSkus().getUrl(), authString);
                                if (productSkus != null) {
                                    for (ProductSku productSku : productSkus.getProductSkus()) {
                                        product_skus_inserts.setString(1, "" + productSku.getId());
                                        product_skus_inserts.setString(2, "" + productSku.getInventory_level());
                                        product_skus_inserts.setString(3, "" + product.getId());
                                        product_skus_inserts.setString(4, "" + productSku.getInventory_warning_level());
                                        product_skus_inserts.setString(5, productSku.getBin_picking_number());
                                        product_skus_inserts.setString(6, productSku.getSku());
                                        product_skus_inserts.setString(7, "" + productSku.getCost_price());
                                        product_skus_inserts.setString(8, "" + productSku.getUpc());

                                        product_skus_inserts.addBatch();
                                        System.out.println("total sku option:"+productSku.getOptions());
                                        if(productSku.getOptions() != null )
                                            System.out.println("total sku option size:"+productSku.getOptions().size());
                                        for (ProductSkuOption productSkuOption : productSku.getOptions()) {
                                            product_sku_options_inserts.setString(1, "" + productSku.getId());
                                            product_sku_options_inserts.setString(2, "" + productSkuOption.getOption_value_id());
                                            product_sku_options_inserts.setString(3, "" + productSkuOption.getProduct_option_id());
                                            product_sku_options_inserts.addBatch();
                                             System.out.println("total sku getOption_value_id:"+productSkuOption.getOption_value_id());
                                        }
                                    }
                                }

                            }


                            if (false && product.getRules() != null) {
                                ProductRules productRules = getProductRules(product.getRules().getUrl(), authString);
                                if (productRules != null) {
                                    for (ProductRule productRule : productRules.getProductRules()) {
                                        product_rules_inserts.setString(1, "" + productRule.getId());
                                        product_rules_inserts.setString(2, "" + productRule.getSort_order());
                                        product_rules_inserts.setString(3, "" + product.getId());
                                        product_rules_inserts.setString(4, "" + productRule.isIs_stop());
                                        product_rules_inserts.setString(5, "" + productRule.isIs_enabled());
                                        product_rules_inserts.setString(6, productRule.getPrice_adjuster());
                                        product_rules_inserts.setString(7, productRule.getWeight_adjuster());
                                        product_rules_inserts.setString(8, "" + productRule.isIs_purchasing_disabled());
                                        product_rules_inserts.setString(9, "" + productRule.isIs_purchasing_hidden());
                                        product_rules_inserts.setString(10, productRule.getImage_file());
                                        product_rules_inserts.setString(11, productRule.getPurchasing_disabled_message());
                                        product_rules_inserts.addBatch();
                                        for (ProductRuleCondition productRuleCondition : productRule.getProductRuleConditions()) {
                                            product_rule_conditions_inserts.setString(1, "" + productRuleCondition.getProduct_option_id());
                                            product_rule_conditions_inserts.setString(2, "" + productRuleCondition.getProduct_option_id());
                                            if (productRuleCondition.getSku_id() != null) {
                                                product_rule_conditions_inserts.setString(3, "" + productRuleCondition.getSku_id());
                                            } else {
                                                product_rule_conditions_inserts.setString(3, "");
                                            }
                                            product_rule_conditions_inserts.setString(4, "" + productRule.getId());
                                            product_rule_conditions_inserts.addBatch();
                                        }
                                    }
                                }

                            }

                            if (product.getSku().indexOf("HBGF")>=0 &&product.getOptions() != null) {
                                ProductOptions productOptions = getProductOptions(product.getOptions().getUrl(), authString);
                                if (productOptions != null) {
                                    for (ProductOption productOption : productOptions.getProductOptions()) {
                                        product_options_inserts.setString(1, "" + productOption.getId());
                                        product_options_inserts.setString(2, "" + productOption.isIs_required());
                                        product_options_inserts.setString(3, "" + product.getId());
                                        product_options_inserts.setString(4, "" + productOption.getOption_id());
                                        product_options_inserts.setString(5, productOption.getDisplay_name());
                                        product_options_inserts.setString(6, "" + productOption.getSort_order());
                                        product_options_inserts.addBatch();
                                        
                                        OptionValues optionValues = getOptionValues(storeURL + "/api/v2/options/"+productOption.getOption_id()+"/values.json?limit=200&page=1", authString);
                                        if(optionValues != null){
                                            for(OptionValue optionValue: optionValues.getOptionValues()){
                                                product_option_values_inserts.setString(1, "" + optionValue.getId());
                                                product_option_values_inserts.setString(2, "" + optionValue.getOption_id());
                                                product_option_values_inserts.setString(3, "" + optionValue.getLabel());
                                                product_option_values_inserts.setInt(4,  optionValue.getSort_order());
                                                product_option_values_inserts.setString(5, optionValue.getValue());
                                                try{
                                                    product_option_values_inserts.executeUpdate();
                                                }catch(Exception e){
                                                    System.out.println("Skip Same option label:"+ optionValue.getLabel() +"option ID:"+optionValue.getOption_id());
                                                }
                                            }
                                        }
                                       
                                    }
                                }

                            }
                            if (false && product.getTax_class() != null) {
                                ProductTaxClasses productTaxClasses = getProductTaxClasses(product.getTax_class().getUrl(), authString);
                                if (productTaxClasses != null) {
                                    for (ProductTaxClass productTaxClass : productTaxClasses.getProductTaxClasses()) {
                                        product_tax_class_inserts.setString(1, "" + productTaxClass.getId());
                                        product_tax_class_inserts.setString(2, productTaxClass.getName());
                                        product_tax_class_inserts.setString(3, "" + product.getId());
                                        product_tax_class_inserts.addBatch();
                                    }

                                }

                            }
                           
                            }catch(Exception e){
                                System.out.println("Ignoring since this id :"+ product.getId()+", sku:"+product.getSku()+" already exists in table");
                                e.printStackTrace();
                            }
                        }
                        System.out.println("Searching is completed");
                        //inserts.executeBatch();
                        product_brands_inserts.executeBatch();
                        product_categories_inserts.executeBatch();
                        product_images_inserts.executeBatch();
                        product_discount_rules_inserts.executeBatch();
                        product_configurable_fields_inserts.executeBatch();
                        product_custom_fields_inserts.executeBatch();
                        product_videos_inserts.executeBatch();
                        product_skus_inserts.executeBatch();
                        product_sku_options_inserts.executeBatch();
                        product_rules_inserts.executeBatch();
                        product_rule_conditions_inserts.executeBatch();
                        product_options_inserts.executeBatch();
                        //product_option_values_inserts.executeBatch();
                        product_tax_class_inserts.executeBatch();
                        System.out.println("Batchupdate completed");
                        /*inserts.close();
                        product_brands_inserts.close();
                        product_categories_inserts.close();
                        product_images_inserts.close();
                        product_discount_rules_inserts.close();
                        product_configurable_fields_inserts.close();
                        product_custom_fields_inserts.close();
                        product_videos_inserts.close();
                        product_skus_inserts.close();
                        product_rules_inserts.close();
                        product_rule_conditions_inserts.close();
                        product_options_inserts.close();
                        product_tax_class_inserts.close();*/

                        if (listProduct != null && listProduct.size() < 200) {
                             System.out.println("Batchupdate finished");
                            break;
                        }
                    } else {
                        break;
                    }


                } catch (Exception exception) {
                    exception.printStackTrace();
                   /* if(STATUS_204){
                        System.out.println("status is 204..breaking");
                       break; 
                    }else{
                        System.out.println("[FetchProducts] : Even though there is an exception, lets continue until we hit status code 204");
                         
                    }*/
                        
                }
                i++;
            }
            return "";
        } catch (Exception ex) {
            Logger.getLogger(FetchCategories.class.getName()).log(Level.SEVERE, null, ex);

        }
        return "";
    }
    
    public static Discountrules getDiscountrules(String authString, String url) {
        try {
            String result = getResultJSONString(authString, url);
            if (result == null) {
                return null;
            }
            String jsonString = "{\"discountrules\":" + result + "}";

            System.out.println("API response completed1" + jsonString);

            Gson gson = new GsonBuilder().create();
            Discountrules discountrules = gson.fromJson(jsonString, Discountrules.class);
            return discountrules;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static ProductVideos getProductVideos(String url, String authString) {
        try {
            String result = getResultJSONString(authString, url);
            if (result == null) {
                return null;
            }
            String jsonString = "{\"productVideos\":" + result + "}";

            System.out.println("API response completed" + jsonString);

            Gson gson = new GsonBuilder().create();
            ProductVideos productVideos = gson.fromJson(jsonString, ProductVideos.class);
            return productVideos;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static ProductTaxClasses getProductTaxClasses(String url, String authString) {
        try {
            String result = getResultJSONString(authString, url);
            if (result == null) {
                return null;
            }
            String jsonString = "{\"productTaxClasses\":" + result + "}";

            System.out.println("API response completed" + jsonString);

            Gson gson = new GsonBuilder().create();
            ProductTaxClasses productTaxClasses = gson.fromJson(jsonString, ProductTaxClasses.class);
            return productTaxClasses;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static ProductSkus getProductSkus(String url, String authString) {
        try {
            String result = getResultJSONString(authString, url);
            if (result == null) {
                return null;
            }
            String jsonString = "{\"productSkus\":" + result + "}";

            System.out.println("API response completed" + jsonString);

            Gson gson = new GsonBuilder().create();
            ProductSkus productSkus = gson.fromJson(jsonString, ProductSkus.class);
            return productSkus;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static ProductRules getProductRules(String url, String authString) {
        try {
            String result = getResultJSONString(authString, url);
            if (result == null) {
                return null;
            }
            String jsonString = "{\"productRules\":" + result + "}";

            System.out.println("API response completed" + jsonString);

            Gson gson = new GsonBuilder().create();
            ProductRules productRules = gson.fromJson(jsonString, ProductRules.class);
            return productRules;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static ProductOptions getProductOptions(String url, String authString) {
        try {
            String result = getResultJSONString(authString, url);
            if (result == null) {
                return null;
            }
            String jsonString = "{\"productOptions\":" + result + "}";

            System.out.println("API response completed" + jsonString);

            Gson gson = new GsonBuilder().create();
            ProductOptions productOptions = gson.fromJson(jsonString, ProductOptions.class);
            return productOptions;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
    
    public static OptionValues getOptionValues(String url, String authString) {
        try {
            String result = getResultJSONString(authString, url);
            if (result == null) {
                return null;
            }
            String jsonString = "{\"optionValues\":" + result + "}";

            System.out.println("API response completed" + jsonString);

            Gson gson = new GsonBuilder().create();
            OptionValues optionValues = gson.fromJson(jsonString, OptionValues.class);
            return optionValues;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static ProdcutImages getProdcutImages(String url, String authString) {
        try {
            String result = getResultJSONString(authString, url);
            if (result == null) {
                return null;
            }
            String jsonString = "{\"prodcutImages\":" + result + "}";

            System.out.println("API response completed" + jsonString);

            Gson gson = new GsonBuilder().create();
            ProdcutImages productImages = gson.fromJson(jsonString, ProdcutImages.class);
            return productImages;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static Configurablefields getConfigurablefields(String url, String authString) {
        try {
            String result = getResultJSONString(authString, url);
            if (result == null) {
                return null;
            }
            String jsonString = "{\"configurablefields\":" + result + "}";

            System.out.println("API response completed" + jsonString);

            Gson gson = new GsonBuilder().create();
            Configurablefields configurablefields = gson.fromJson(jsonString, Configurablefields.class);
            return configurablefields;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static Customfields getCustomfields(String url, String authString) {
        try {
            String result = getResultJSONString(authString, url);
            if (result == null) {
                return null;
            }
            String jsonString = "{\"customfields\":" + result + "}";

            System.out.println("API response completed" + jsonString);

            Gson gson = new GsonBuilder().create();
            Customfields customfields = gson.fromJson(jsonString, Customfields.class);
            return customfields;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
    static boolean STATUS_204=false;
    public static String REQUEST_TYPE="GET";
    public static String getResultJSONString(String authString, String url) {
        try {
            if (url == null) {
                return null;
            }
            //System.out.println("Url trying to fetch is:" + url);
            URL uri = new URL(url.trim());
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);

            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setDoInput(true);
            httpCon.setRequestMethod(REQUEST_TYPE);
            if(httpCon.getResponseCode()==204){
                System.out.println("status is 204..");
                STATUS_204 = true;
                return null;
            }
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = "";
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            String restult = new String(sb);
            if (restult != null && restult.length() > 0) {
                return restult;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void addAllCategories(String authString, Connection connection) throws Exception{
        int i = 1;
         PreparedStatement truncate = connection.prepareStatement("truncate table hb_bc.categories");
         truncate.executeUpdate();
         ArrayList categoriesAdded=new ArrayList();
        while (true) {
            
            try {
                String webPage = storeURL + "/api/v2/categories.json?limit=200&page=" + i;

                String result = getResultJSONString(authString, webPage);
                if (result == null) {
                    break;
                }
                String jsonString = "{\"categories\":" + result + "}";

                System.out.println("API response completed" + jsonString);

                Gson gson = new GsonBuilder().create();
                Categories categories = gson.fromJson(jsonString, Categories.class);
                PreparedStatement inserts = connection.prepareStatement("INSERT INTO hb_bc.categories (id, parent_id,sort_order, name, description, page_title, meta_keywords, meta_description, layout_file, image_file, search_keywords, is_visible, url) VALUES ( ?, ?, ?,?,?,?,?,?,?,?,?,?,?)");
                for (Category category : categories.getCategories()) {
                    if(categoriesAdded.contains(category.getId()))
                        continue;
                    categoriesAdded.add(category.getId());
                    inserts.setString(1, "" + category.getId());
                    inserts.setString(2, "" + category.getParent_id());
                    inserts.setString(3, "" + category.getSort_order());
                    inserts.setString(4, category.getName());
                    inserts.setString(5, category.getDescription());
                    inserts.setString(6, category.getPage_title());
                    inserts.setString(7, category.getMeta_keywords());
                    inserts.setString(8, category.getMeta_description());
                    inserts.setString(9, category.getLayout_file());
                    inserts.setString(10, category.getImage_file());
                    inserts.setString(11, category.getSearch_keywords());
                    inserts.setString(12, "" + category.isIs_visible());
                    inserts.setString(13, category.getUrl());
                    inserts.addBatch();
                }
                System.out.println("Searching Categories is completed");
                inserts.executeBatch();
                System.out.println("Batchupdate completed for Categories");
                inserts.close();
                if (categories != null && categories.getCategories().size() < 200) {
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
            i++;
        }
    }

    public void addAllBrands(String authString, Connection connection) {
        int i = 1;
        while (true) {
            try {
                String webPage = storeURL + "/api/v2/brands.json?limit=200&page=" + i;

                String result = getResultJSONString(authString, webPage);
                if (result == null) {
                    break;
                }
                String jsonString = "{\"productBrands\":" + result + "}";

                System.out.println("API response completed");

                Gson gson = new GsonBuilder().create();
                ProductBrands productBrands = gson.fromJson(jsonString, ProductBrands.class);
                System.out.println("API response productBrands" + productBrands);
                PreparedStatement trunct=connection.prepareStatement("truncate table hb_bc.brands");
                trunct.executeUpdate();;
                
                PreparedStatement inserts = connection.prepareStatement("INSERT INTO hb_bc.brands (id, name,page_title, meta_keywords, meta_description, image_file, search_keywords) VALUES ( ?, ?, ?,?,?,?,?)");

                for (ProductBrand productBrand : productBrands.getProductBrands()) {
                    try{
                        inserts.setString(1, "" + productBrand.getId());
                        inserts.setString(2, productBrand.getName());
                        inserts.setString(3, productBrand.getPage_title());
                        inserts.setString(4, productBrand.getMeta_keywords());
                        inserts.setString(5, productBrand.getMeta_description());
                        inserts.setString(6, productBrand.getImage_file());
                        inserts.setString(7, productBrand.getSearch_keywords());
                        inserts.executeUpdate();                        
                    }catch (Exception e2){e2.printStackTrace();}

                    //inserts.addBatch();
                }
                System.out.println("Searching Brands is completed");
               // inserts.executeBatch();
                System.out.println("Batchupdate completed for Brands");
                inserts.close();
                if (productBrands != null && productBrands.getProductBrands().size() < 200) {
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
            i++;
        }
    }

    private boolean isUNFIProduct(Product product, Connection connection) {
        return  product.getSku().startsWith("UN_") ? true : false;
//         ResultSet rs = null;
//        PreparedStatement inserts = null;
//        try {
//            inserts = connection.prepareStatement("select count(1) from  hb_bc.unfi_hb_product where id="+ product.getId());
//            rs = inserts.executeQuery();
//            if(rs.next()) {
//                    iCount  = rs.getInt(1);
//            }
//        } catch (Exception e) {
//                e.printStackTrace();
//        
//         } finally {
//            try {
//                rs.close();
//                inserts.close();
//            } catch (SQLException ex) {
//                Logger.getLogger(FetchProducts.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        
//        return iCount==0?false:true;
    }
}
