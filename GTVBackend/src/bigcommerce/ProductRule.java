/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import com.google.gson.annotations.Expose;
import java.util.List;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * @author Abani
 */
@Generated("com.googlecode.jsonschema2pojo")
public class ProductRule {

    @Expose
    private long id;
    @Expose
    private long product_id;
    @Expose
    private boolean is_stop;
    @Expose
    private boolean is_enabled;
    @Expose
    private long sort_order;
    @Expose
    private String price_adjuster;
    @Expose
    private String weight_adjuster;
    @Expose
    private boolean is_purchasing_disabled;
    @Expose
    private boolean is_purchasing_hidden;
    @Expose
    private String image_file;
    @Expose
    private String purchasing_disabled_message;
    
    @Expose
    private List<ProductRuleCondition> productRuleConditions;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public boolean isIs_stop() {
        return is_stop;
    }

    public void setIs_stop(boolean is_stop) {
        this.is_stop = is_stop;
    }

    public boolean isIs_enabled() {
        return is_enabled;
    }

    public void setIs_enabled(boolean is_enabled) {
        this.is_enabled = is_enabled;
    }

    public long getSort_order() {
        return sort_order;
    }

    public void setSort_order(long sort_order) {
        this.sort_order = sort_order;
    }

    public String getPrice_adjuster() {
        return price_adjuster;
    }

    public void setPrice_adjuster(String price_adjuster) {
        this.price_adjuster = price_adjuster;
    }

    public String getWeight_adjuster() {
        return weight_adjuster;
    }

    public void setWeight_adjuster(String weight_adjuster) {
        this.weight_adjuster = weight_adjuster;
    }

    public boolean isIs_purchasing_disabled() {
        return is_purchasing_disabled;
    }

    public void setIs_purchasing_disabled(boolean is_purchasing_disabled) {
        this.is_purchasing_disabled = is_purchasing_disabled;
    }

    public boolean isIs_purchasing_hidden() {
        return is_purchasing_hidden;
    }

    public void setIs_purchasing_hidden(boolean is_purchasing_hidden) {
        this.is_purchasing_hidden = is_purchasing_hidden;
    }

    public String getImage_file() {
        return image_file;
    }

    public void setImage_file(String image_file) {
        this.image_file = image_file;
    }

    public String getPurchasing_disabled_message() {
        return purchasing_disabled_message;
    }

    public void setPurchasing_disabled_message(String purchasing_disabled_message) {
        this.purchasing_disabled_message = purchasing_disabled_message;
    }

    public List<ProductRuleCondition> getProductRuleConditions() {
        return productRuleConditions;
    }

    public void setProductRuleConditions(List<ProductRuleCondition> productRuleConditions) {
        this.productRuleConditions = productRuleConditions;
    }
    
    

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }
}
