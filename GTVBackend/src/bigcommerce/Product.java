
package bigcommerce;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("com.googlecode.jsonschema2pojo")
public class Product {

    @Expose
    private String availability;
    @Expose
    private String availability_description;
    @Expose
    private String bin_picking_number;
    @Expose
    private Brand brand;
    @Expose
    private long brand_id;
    @Expose
    private String calculated_price;
    @Expose
    private List<Long> categories = new ArrayList<Long>();
    @Expose
    private String condition;
    @Expose
    private Configurable_fields configurable_fields;
    @Expose
    private String cost_price;
    @Expose
    private Custom_fields custom_fields;
    @Expose
    private String custom_url;
    @Expose
    private String date_created;
    @Expose
    private String date_last_imported;
    @Expose
    private String date_modified;
    @Expose
    private String depth;
    @Expose
    private String description;
    @Expose
    private Discount_rules discount_rules;
    @Expose
    private Downloads downloads;
    @Expose
    private String event_date_end;
    @Expose
    private String event_date_field_name;
    @Expose
    private String event_date_start;
    @Expose
    private String event_date_type;
    @Expose
    private String fixed_cost_shipping_price;
    @Expose
    private String height;
    @Expose
    private long id;
    @Expose
    private Images images;
    @Expose
    private long inventory_level;
    @Expose
    private String inventory_tracking;
    @Expose
    private long inventory_warning_level;
    @Expose
    private boolean is_condition_shown;
    @Expose
    private boolean is_featured;
    @Expose
    private boolean is_free_shipping;
    @Expose
    private boolean is_open_graph_thumbnail;
    @Expose
    private boolean is_preorder_only;
    @Expose
    private boolean is_price_hidden;
    @Expose
    private boolean is_visible;
    @Expose
    private Object keyword_filter;
    @Expose
    private String layout_file;
    @Expose
    private String meta_description;
    @Expose
    private String meta_keywords;
    @Expose
    private String myob_asset_account;
    @Expose
    private String myob_expense_account;
    @Expose
    private String myob_income_account;
    @Expose
    private String name;
    @Expose
    private String open_graph_description;
    @Expose
    private String open_graph_title;
    @Expose
    private String open_graph_type;
    @Expose
    private Object option_set;
    @Expose
    private String option_set_display;
    @Expose
    private Object option_set_id;
    @Expose
    private Options options;
    @Expose
    private long order_quantity_maximum;
    @Expose
    private long order_quantity_minimum;
    @Expose
    private String page_title;
    @Expose
    private String peachtree_gl_account;
    @Expose
    private String preorder_message;
    @Expose
    private String preorder_release_date;
    @Expose
    private String price;
    @Expose
    private String price_hidden_label;
    @Expose
    private long rating_count;
    @Expose
    private long rating_total;
    @Expose
    private String related_products;
    @Expose
    private String retail_price;
    @Expose
    private Rules rules;
    @Expose
    private String sale_price;
    @Expose
    private String search_keywords;
    @Expose
    private String sku;
    @Expose
    private Skus skus;
    @Expose
    private long sort_order;
    @Expose
    private Tax_class tax_class;
    @Expose
    private long tax_class_id;
    @Expose
    private long total_sold;
    @Expose
    private String type;
    @Expose
    private String upc;
    @Expose
    private Videos videos;
    @Expose
    private long view_count;
    @Expose
    private String warranty;
    @Expose
    private String weight;
    @Expose
    private String width;

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getAvailability_description() {
        return availability_description;
    }

    public void setAvailability_description(String availability_description) {
        this.availability_description = availability_description;
    }

    public String getBin_picking_number() {
        return bin_picking_number;
    }

    public void setBin_picking_number(String bin_picking_number) {
        this.bin_picking_number = bin_picking_number;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public long getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(long brand_id) {
        this.brand_id = brand_id;
    }

    public String getCalculated_price() {
        return calculated_price;
    }

    public void setCalculated_price(String calculated_price) {
        this.calculated_price = calculated_price;
    }

    public List<Long> getCategories() {
        return categories;
    }

    public void setCategories(List<Long> categories) {
        this.categories = categories;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Configurable_fields getConfigurable_fields() {
        return configurable_fields;
    }

    public void setConfigurable_fields(Configurable_fields configurable_fields) {
        this.configurable_fields = configurable_fields;
    }

    public String getCost_price() {
        return cost_price;
    }

    public void setCost_price(String cost_price) {
        this.cost_price = cost_price;
    }

    public Custom_fields getCustom_fields() {
        return custom_fields;
    }

    public void setCustom_fields(Custom_fields custom_fields) {
        this.custom_fields = custom_fields;
    }

    public String getCustom_url() {
        return custom_url;
    }

    public void setCustom_url(String custom_url) {
        this.custom_url = custom_url;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getDate_last_imported() {
        return date_last_imported;
    }

    public void setDate_last_imported(String date_last_imported) {
        this.date_last_imported = date_last_imported;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(String date_modified) {
        this.date_modified = date_modified;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Discount_rules getDiscount_rules() {
        return discount_rules;
    }

    public void setDiscount_rules(Discount_rules discount_rules) {
        this.discount_rules = discount_rules;
    }

    public Downloads getDownloads() {
        return downloads;
    }

    public void setDownloads(Downloads downloads) {
        this.downloads = downloads;
    }

    public String getEvent_date_end() {
        return event_date_end;
    }

    public void setEvent_date_end(String event_date_end) {
        this.event_date_end = event_date_end;
    }

    public String getEvent_date_field_name() {
        return event_date_field_name;
    }

    public void setEvent_date_field_name(String event_date_field_name) {
        this.event_date_field_name = event_date_field_name;
    }

    public String getEvent_date_start() {
        return event_date_start;
    }

    public void setEvent_date_start(String event_date_start) {
        this.event_date_start = event_date_start;
    }

    public String getEvent_date_type() {
        return event_date_type;
    }

    public void setEvent_date_type(String event_date_type) {
        this.event_date_type = event_date_type;
    }

    public String getFixed_cost_shipping_price() {
        return fixed_cost_shipping_price;
    }

    public void setFixed_cost_shipping_price(String fixed_cost_shipping_price) {
        this.fixed_cost_shipping_price = fixed_cost_shipping_price;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public long getInventory_level() {
        return inventory_level;
    }

    public void setInventory_level(long inventory_level) {
        this.inventory_level = inventory_level;
    }

    public String getInventory_tracking() {
        return inventory_tracking;
    }

    public void setInventory_tracking(String inventory_tracking) {
        this.inventory_tracking = inventory_tracking;
    }

    public long getInventory_warning_level() {
        return inventory_warning_level;
    }

    public void setInventory_warning_level(long inventory_warning_level) {
        this.inventory_warning_level = inventory_warning_level;
    }

    public boolean isIs_condition_shown() {
        return is_condition_shown;
    }

    public void setIs_condition_shown(boolean is_condition_shown) {
        this.is_condition_shown = is_condition_shown;
    }

    public boolean isIs_featured() {
        return is_featured;
    }

    public void setIs_featured(boolean is_featured) {
        this.is_featured = is_featured;
    }

    public boolean isIs_free_shipping() {
        return is_free_shipping;
    }

    public void setIs_free_shipping(boolean is_free_shipping) {
        this.is_free_shipping = is_free_shipping;
    }

    public boolean isIs_open_graph_thumbnail() {
        return is_open_graph_thumbnail;
    }

    public void setIs_open_graph_thumbnail(boolean is_open_graph_thumbnail) {
        this.is_open_graph_thumbnail = is_open_graph_thumbnail;
    }

    public boolean isIs_preorder_only() {
        return is_preorder_only;
    }

    public void setIs_preorder_only(boolean is_preorder_only) {
        this.is_preorder_only = is_preorder_only;
    }

    public boolean isIs_price_hidden() {
        return is_price_hidden;
    }

    public void setIs_price_hidden(boolean is_price_hidden) {
        this.is_price_hidden = is_price_hidden;
    }

    public boolean isIs_visible() {
        return is_visible;
    }

    public void setIs_visible(boolean is_visible) {
        this.is_visible = is_visible;
    }

    public Object getKeyword_filter() {
        return keyword_filter;
    }

    public void setKeyword_filter(Object keyword_filter) {
        this.keyword_filter = keyword_filter;
    }

    public String getLayout_file() {
        return layout_file;
    }

    public void setLayout_file(String layout_file) {
        this.layout_file = layout_file;
    }

    public String getMeta_description() {
        return meta_description;
    }

    public void setMeta_description(String meta_description) {
        this.meta_description = meta_description;
    }

    public String getMeta_keywords() {
        return meta_keywords;
    }

    public void setMeta_keywords(String meta_keywords) {
        this.meta_keywords = meta_keywords;
    }

    public String getMyob_asset_account() {
        return myob_asset_account;
    }

    public void setMyob_asset_account(String myob_asset_account) {
        this.myob_asset_account = myob_asset_account;
    }

    public String getMyob_expense_account() {
        return myob_expense_account;
    }

    public void setMyob_expense_account(String myob_expense_account) {
        this.myob_expense_account = myob_expense_account;
    }

    public String getMyob_income_account() {
        return myob_income_account;
    }

    public void setMyob_income_account(String myob_income_account) {
        this.myob_income_account = myob_income_account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpen_graph_description() {
        return open_graph_description;
    }

    public void setOpen_graph_description(String open_graph_description) {
        this.open_graph_description = open_graph_description;
    }

    public String getOpen_graph_title() {
        return open_graph_title;
    }

    public void setOpen_graph_title(String open_graph_title) {
        this.open_graph_title = open_graph_title;
    }

    public String getOpen_graph_type() {
        return open_graph_type;
    }

    public void setOpen_graph_type(String open_graph_type) {
        this.open_graph_type = open_graph_type;
    }

    public Object getOption_set() {
        return option_set;
    }

    public void setOption_set(Object option_set) {
        this.option_set = option_set;
    }

    public String getOption_set_display() {
        return option_set_display;
    }

    public void setOption_set_display(String option_set_display) {
        this.option_set_display = option_set_display;
    }

    public Object getOption_set_id() {
        return option_set_id;
    }

    public void setOption_set_id(Object option_set_id) {
        this.option_set_id = option_set_id;
    }

    public Options getOptions() {
        return options;
    }

    public void setOptions(Options options) {
        this.options = options;
    }

    public long getOrder_quantity_maximum() {
        return order_quantity_maximum;
    }

    public void setOrder_quantity_maximum(long order_quantity_maximum) {
        this.order_quantity_maximum = order_quantity_maximum;
    }

    public long getOrder_quantity_minimum() {
        return order_quantity_minimum;
    }

    public void setOrder_quantity_minimum(long order_quantity_minimum) {
        this.order_quantity_minimum = order_quantity_minimum;
    }

    public String getPage_title() {
        return page_title;
    }

    public void setPage_title(String page_title) {
        this.page_title = page_title;
    }

    public String getPeachtree_gl_account() {
        return peachtree_gl_account;
    }

    public void setPeachtree_gl_account(String peachtree_gl_account) {
        this.peachtree_gl_account = peachtree_gl_account;
    }

    public String getPreorder_message() {
        return preorder_message;
    }

    public void setPreorder_message(String preorder_message) {
        this.preorder_message = preorder_message;
    }

    public String getPreorder_release_date() {
        return preorder_release_date;
    }

    public void setPreorder_release_date(String preorder_release_date) {
        this.preorder_release_date = preorder_release_date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_hidden_label() {
        return price_hidden_label;
    }

    public void setPrice_hidden_label(String price_hidden_label) {
        this.price_hidden_label = price_hidden_label;
    }

    public long getRating_count() {
        return rating_count;
    }

    public void setRating_count(long rating_count) {
        this.rating_count = rating_count;
    }

    public long getRating_total() {
        return rating_total;
    }

    public void setRating_total(long rating_total) {
        this.rating_total = rating_total;
    }

    public String getRelated_products() {
        return related_products;
    }

    public void setRelated_products(String related_products) {
        this.related_products = related_products;
    }

    public String getRetail_price() {
        return retail_price;
    }

    public void setRetail_price(String retail_price) {
        this.retail_price = retail_price;
    }

    public Rules getRules() {
        return rules;
    }

    public void setRules(Rules rules) {
        this.rules = rules;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getSearch_keywords() {
        return search_keywords;
    }

    public void setSearch_keywords(String search_keywords) {
        this.search_keywords = search_keywords;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Skus getSkus() {
        return skus;
    }

    public void setSkus(Skus skus) {
        this.skus = skus;
    }

    public long getSort_order() {
        return sort_order;
    }

    public void setSort_order(long sort_order) {
        this.sort_order = sort_order;
    }

    public Tax_class getTax_class() {
        return tax_class;
    }

    public void setTax_class(Tax_class tax_class) {
        this.tax_class = tax_class;
    }

    public long getTax_class_id() {
        return tax_class_id;
    }

    public void setTax_class_id(long tax_class_id) {
        this.tax_class_id = tax_class_id;
    }

    public long getTotal_sold() {
        return total_sold;
    }

    public void setTotal_sold(long total_sold) {
        this.total_sold = total_sold;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public Videos getVideos() {
        return videos;
    }

    public void setVideos(Videos videos) {
        this.videos = videos;
    }

    public long getView_count() {
        return view_count;
    }

    public void setView_count(long view_count) {
        this.view_count = view_count;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        if(description != null) {
            sb.append("\"description\":\"").append(description).append("\",");
        }
        if(price != null) {
            sb.append("\"price\":\"").append(price).append("\",");
        }
        sb.append("}");
        String strJSON = new String(sb);
        strJSON = strJSON.substring(0, strJSON.length()-2);
        strJSON += "}";
        return strJSON;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

}
