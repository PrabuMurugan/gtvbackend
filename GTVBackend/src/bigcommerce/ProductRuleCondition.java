/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;

/**
 *
 * @author Abani
 */
@Generated("com.googlecode.jsonschema2pojo")
public class ProductRuleCondition {

    @Expose
    private long product_option_id;
    @Expose
    private long option_value_id;
    @Expose
    private Long sku_id;

    public long getProduct_option_id() {
        return product_option_id;
    }

    public void setProduct_option_id(long product_option_id) {
        this.product_option_id = product_option_id;
    }

    public long getOption_value_id() {
        return option_value_id;
    }

    public void setOption_value_id(long option_value_id) {
        this.option_value_id = option_value_id;
    }

    public Long getSku_id() {
        return sku_id;
    }

    public void setSku_id(Long sku_id) {
        this.sku_id = sku_id;
    }
}
