/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * @author Abani
 */
@Generated("com.googlecode.jsonschema2pojo")
public class Configurablefield {

    @Expose
    private long id;
    @Expose
    private long product_id;
    @Expose
    private long sort_order;
    @Expose
    private String name;
    @Expose
    private boolean is_required;
    @Expose
    private String allowed_file_types;
    @Expose
    private String type;
    @Expose
    private String select_options;
    @Expose
    private long max_size;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public long getSort_order() {
        return sort_order;
    }

    public void setSort_order(long sort_order) {
        this.sort_order = sort_order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIs_required() {
        return is_required;
    }

    public void setIs_required(boolean is_required) {
        this.is_required = is_required;
    }

    public String getAllowed_file_types() {
        return allowed_file_types;
    }

    public void setAllowed_file_types(String allowed_file_types) {
        this.allowed_file_types = allowed_file_types;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSelect_options() {
        return select_options;
    }

    public void setSelect_options(String select_options) {
        this.select_options = select_options;
    }

    public long getMax_size() {
        return max_size;
    }

    public void setMax_size(long max_size) {
        this.max_size = max_size;
    }
    
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }
}
