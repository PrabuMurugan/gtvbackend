/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.util.List;

/**
 *
 * @author Abani
 */
public class ProductOptionSkus {
    
    private List<ProductOptionSku> productOptionSkus;

    @Override
    public String toString() {
        return "productOptionSkus{" + "productOptionSkus=" + productOptionSkus + '}';
    }

    public List<ProductOptionSku> getProductOptionSkus() {
        return productOptionSkus;
    }

    public void setProductOptionSkus(List<ProductOptionSku> productOptionSkus) {
        this.productOptionSkus = productOptionSkus;
    }
     
    
}
