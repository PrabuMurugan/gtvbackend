 package bigcommerce;

 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
 import bigcommerce.BigCommerce;
import bombayplaza.pricematch.RetrievePage;
 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author us083274
 */
public class UpdateInventoryQty {
    public static void main (String[] args){
        try{
                System.out.println("Inside UpdateInventoryQty");
                 
                Connection con = RetrievePage.getConnection();
                System.out.println("Inside UpdateInventoryQty");
                   //String sql="SELECT title,id,image,body_html,price, case  when inventory_quantity=0 then 'Out Of Stock' else 'In Stock' end,vendor,product_type,'','New',sku,'',replace(replace(replace(barcode,'UPC:',''),'-',''),'\\'',''), concat('http://harrisburgstore.com/products/',handle),concat(grams,' g'),vendor,'','','','','', price,id2,'','','',''    FROM harrisburgstore.products ";
                    String pending_sql="select sum(quantity) from pending_orders_to_be_shipped  p where sku=?";
                    PreparedStatement pr_pending=con.prepareCall(pending_sql);
                    String sql="select  p.id id,p.sku sku,b.upc,b.total_units,p.name, f.asin,case when f.packing_qty is null or f.packing_qty='NA' then 1 else f.packing_qty end  packing_qty,  floor(b.total_units/case when f.packing_qty is null or f.packing_qty='NA' then 1 else f.packing_qty end) expected_units_in_hb, p.inventory_level actual_units_in_hb from harrisburgstore.total_available_units b, hb_bc.product p left join tajplaza.final_outputcatalog f on  substring_index(peachtree_gl_account,'||',1)= f.asin  and packing_qty is not null and packing_qty!='NA'  where  lpad(replace(replace(replace(b.upc,'-',''),'UPC:',''),'\\'','') ,14,'0') = lpad(replace(replace(replace(p.upc,'-',''),'UPC:',''),'\\'','') ,14,'0')  and total_units>=0  "
 
                            + " union "
                            + " select  p.id id,p.sku sku,p.upc,p.inventory_level,p.name, 'NA','NA' ,  0 expected_units_in_hb, p.inventory_level actual_units_in_hb from  hb_bc.product p  where p.inventory_level>0   and lpad(replace(replace(replace(p.upc,'-',''),'UPC:',''),'\\'','') ,14,'0') not in (select lpad(replace(replace(replace(upc,'-',''),'UPC:',''),'\\'','') ,14,'0') from harrisburgstore.total_available_units where total_units>0 ) and upc!='NA' and length(upc)>3";
                   PreparedStatement prest = con.prepareStatement(sql);      
                   ResultSet rs=prest.executeQuery();
                   while(rs.next()){
                       try{
                        String sku=rs.getString("sku");
                        pr_pending.setString(1, sku);
                        ResultSet rs2=pr_pending.executeQuery();
                        String expected_units_in_hb=rs.getString("expected_units_in_hb");
                        if(sku.indexOf("BB")==0)
                            expected_units_in_hb="999";
                        
                        if(rs2.next()){
                            try{
                                Integer pending_quantity=rs2.getInt(1);
                                if(pending_quantity>0 ){
                                    System.out.println("sku : "+sku + " has pending order quantity of "+pending_quantity +" and expected_units_in_hb is "+expected_units_in_hb);
                                    expected_units_in_hb=String.valueOf(Integer.valueOf(expected_units_in_hb)-pending_quantity);
                                    
                                }
                            }catch (Exception e3){
                                e3.printStackTrace();
                            }
                            
                        }     
                       /* if(Integer.valueOf(expected_units_in_hb)<=5){
                            expected_units_in_hb=String.valueOf(Integer.valueOf(expected_units_in_hb)/2);
                        }*/
                        String actual_units_in_hb=rs.getString("actual_units_in_hb");
                        String id=rs.getString("id");
                        System.out.println("SKU TRACE: "+rs.getString("sku") +",expected_units_in_hb:"+expected_units_in_hb+",actual_units_in_hb:"+actual_units_in_hb);
                        if(expected_units_in_hb.equals("null") ||Integer.valueOf(expected_units_in_hb)<0)        
                            expected_units_in_hb="0";
                        if(expected_units_in_hb.equals(actual_units_in_hb)==false){
                            System.out.println("GOING TO UPDATE: "+rs.getString("sku") +",expected_units_in_hb:"+expected_units_in_hb+",actual_units_in_hb:"+actual_units_in_hb);
                           // ShopifyAPIDAO2.updateProduct_NEW(con, sku,String.valueOf(expected_units_in_hb),false,null);
                            BigCommerce.updateProduct(id,sku,expected_units_in_hb,null,null);
                          //  Thread.sleep(30000);
                        }
                                  
                           
                       }catch (Exception e2){
                           e2.printStackTrace();
                       }
                   }
            
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
