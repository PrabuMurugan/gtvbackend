/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.util.List;

/**
 *
 * @author Abani
 */
public class Discountrules {

    private List<Discountrule> discountrules;

    @Override
    public String toString() {
        return "Discountrules{" + "discountrules=" + discountrules + '}';
    }

    public List<Discountrule> getDiscountrules() {
        return discountrules;
    }

    public void setDiscountrules(List<Discountrule> discountrules) {
        this.discountrules = discountrules;
    }
}
