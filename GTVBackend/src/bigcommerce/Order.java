
package bigcommerce;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Order {

    @Expose
    private Integer id;
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("date_modified")
    @Expose
    private String dateModified;
    @SerializedName("date_shipped")
    @Expose
    private String dateShipped;
    @SerializedName("status_id")
    @Expose
    private Integer statusId;
    @Expose
    private String status;
    @SerializedName("subtotal_ex_tax")
    @Expose
    private String subtotalExTax;
    @SerializedName("subtotal_inc_tax")
    @Expose
    private String subtotalIncTax;
    @SerializedName("subtotal_tax")
    @Expose
    private String subtotalTax;
    @SerializedName("base_shipping_cost")
    @Expose
    private String baseShippingCost;
    @SerializedName("shipping_cost_ex_tax")
    @Expose
    private String shippingCostExTax;
    @SerializedName("shipping_cost_inc_tax")
    @Expose
    private String shippingCostIncTax;
    @SerializedName("shipping_cost_tax")
    @Expose
    private String shippingCostTax;
    @SerializedName("shipping_cost_tax_class_id")
    @Expose
    private Integer shippingCostTaxClassId;
    @SerializedName("base_handling_cost")
    @Expose
    private String baseHandlingCost;
    @SerializedName("handling_cost_ex_tax")
    @Expose
    private String handlingCostExTax;
    @SerializedName("handling_cost_inc_tax")
    @Expose
    private String handlingCostIncTax;
    @SerializedName("handling_cost_tax")
    @Expose
    private String handlingCostTax;
    @SerializedName("handling_cost_tax_class_id")
    @Expose
    private Integer handlingCostTaxClassId;
    @SerializedName("base_wrapping_cost")
    @Expose
    private String baseWrappingCost;
    @SerializedName("wrapping_cost_ex_tax")
    @Expose
    private String wrappingCostExTax;
    @SerializedName("wrapping_cost_inc_tax")
    @Expose
    private String wrappingCostIncTax;
    @SerializedName("wrapping_cost_tax")
    @Expose
    private String wrappingCostTax;
    @SerializedName("wrapping_cost_tax_class_id")
    @Expose
    private Integer wrappingCostTaxClassId;
    @SerializedName("total_ex_tax")
    @Expose
    private String totalExTax;
    @SerializedName("total_inc_tax")
    @Expose
    private String totalIncTax;
    @SerializedName("total_tax")
    @Expose
    private String totalTax;
    @SerializedName("items_total")
    @Expose
    private Integer itemsTotal;
    @SerializedName("items_shipped")
    @Expose
    private Integer itemsShipped;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("payment_provider_id")
    @Expose
    private String paymentProviderId;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("refunded_amount")
    @Expose
    private String refundedAmount;
    @SerializedName("order_is_digital")
    @Expose
    private Boolean orderIsDigital;
    @SerializedName("store_credit_amount")
    @Expose
    private String storeCreditAmount;
    @SerializedName("gift_certificate_amount")
    @Expose
    private String giftCertificateAmount;
    @SerializedName("ip_address")
    @Expose
    private String ipAddress;
    @SerializedName("geoip_country")
    @Expose
    private String geoipCountry;
    @SerializedName("geoip_country_iso2")
    @Expose
    private String geoipCountryIso2;
    @SerializedName("currency_id")
    @Expose
    private Integer currencyId;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("currency_exchange_rate")
    @Expose
    private String currencyExchangeRate;
    @SerializedName("default_currency_id")
    @Expose
    private Integer defaultCurrencyId;
    @SerializedName("default_currency_code")
    @Expose
    private String defaultCurrencyCode;
    @SerializedName("staff_notes")
    @Expose
    private String staffNotes;
    @SerializedName("customer_message")
    @Expose
    private String customerMessage;
    @SerializedName("discount_amount")
    @Expose
    private String discountAmount;
    @SerializedName("coupon_discount")
    @Expose
    private String couponDiscount;
    @SerializedName("shipping_address_count")
    @Expose
    private Integer shippingAddressCount;
    @SerializedName("is_deleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("ebay_order_id")
    @Expose
    private String ebayOrderId;
    @SerializedName("billing_address")
    @Expose
    private BillingAddress billingAddress;
    @SerializedName("order_source")
    @Expose
    private String orderSource;
    @SerializedName("external_source")
    @Expose
    private Object externalSource;
    @Expose
    private Products_1 products;
    @SerializedName("shipping_addresses")
    @Expose
    private ShippingAddresses shippingAddresses;
    @Expose
    private Coupons coupons;

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The customerId
     */
    public Integer getCustomerId() {
        return customerId;
    }

    /**
     * 
     * @param customerId
     *     The customer_id
     */
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    /**
     * 
     * @return
     *     The dateCreated
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     * 
     * @param dateCreated
     *     The date_created
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * 
     * @return
     *     The dateModified
     */
    public String getDateModified() {
        return dateModified;
    }

    /**
     * 
     * @param dateModified
     *     The date_modified
     */
    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    /**
     * 
     * @return
     *     The dateShipped
     */
    public String getDateShipped() {
        return dateShipped;
    }

    /**
     * 
     * @param dateShipped
     *     The date_shipped
     */
    public void setDateShipped(String dateShipped) {
        this.dateShipped = dateShipped;
    }

    /**
     * 
     * @return
     *     The statusId
     */
    public Integer getStatusId() {
        return statusId;
    }

    /**
     * 
     * @param statusId
     *     The status_id
     */
    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The subtotalExTax
     */
    public String getSubtotalExTax() {
        return subtotalExTax;
    }

    /**
     * 
     * @param subtotalExTax
     *     The subtotal_ex_tax
     */
    public void setSubtotalExTax(String subtotalExTax) {
        this.subtotalExTax = subtotalExTax;
    }

    /**
     * 
     * @return
     *     The subtotalIncTax
     */
    public String getSubtotalIncTax() {
        return subtotalIncTax;
    }

    /**
     * 
     * @param subtotalIncTax
     *     The subtotal_inc_tax
     */
    public void setSubtotalIncTax(String subtotalIncTax) {
        this.subtotalIncTax = subtotalIncTax;
    }

    /**
     * 
     * @return
     *     The subtotalTax
     */
    public String getSubtotalTax() {
        return subtotalTax;
    }

    /**
     * 
     * @param subtotalTax
     *     The subtotal_tax
     */
    public void setSubtotalTax(String subtotalTax) {
        this.subtotalTax = subtotalTax;
    }

    /**
     * 
     * @return
     *     The baseShippingCost
     */
    public String getBaseShippingCost() {
        return baseShippingCost;
    }

    /**
     * 
     * @param baseShippingCost
     *     The base_shipping_cost
     */
    public void setBaseShippingCost(String baseShippingCost) {
        this.baseShippingCost = baseShippingCost;
    }

    /**
     * 
     * @return
     *     The shippingCostExTax
     */
    public String getShippingCostExTax() {
        return shippingCostExTax;
    }

    /**
     * 
     * @param shippingCostExTax
     *     The shipping_cost_ex_tax
     */
    public void setShippingCostExTax(String shippingCostExTax) {
        this.shippingCostExTax = shippingCostExTax;
    }

    /**
     * 
     * @return
     *     The shippingCostIncTax
     */
    public String getShippingCostIncTax() {
        return shippingCostIncTax;
    }

    /**
     * 
     * @param shippingCostIncTax
     *     The shipping_cost_inc_tax
     */
    public void setShippingCostIncTax(String shippingCostIncTax) {
        this.shippingCostIncTax = shippingCostIncTax;
    }

    /**
     * 
     * @return
     *     The shippingCostTax
     */
    public String getShippingCostTax() {
        return shippingCostTax;
    }

    /**
     * 
     * @param shippingCostTax
     *     The shipping_cost_tax
     */
    public void setShippingCostTax(String shippingCostTax) {
        this.shippingCostTax = shippingCostTax;
    }

    /**
     * 
     * @return
     *     The shippingCostTaxClassId
     */
    public Integer getShippingCostTaxClassId() {
        return shippingCostTaxClassId;
    }

    /**
     * 
     * @param shippingCostTaxClassId
     *     The shipping_cost_tax_class_id
     */
    public void setShippingCostTaxClassId(Integer shippingCostTaxClassId) {
        this.shippingCostTaxClassId = shippingCostTaxClassId;
    }

    /**
     * 
     * @return
     *     The baseHandlingCost
     */
    public String getBaseHandlingCost() {
        return baseHandlingCost;
    }

    /**
     * 
     * @param baseHandlingCost
     *     The base_handling_cost
     */
    public void setBaseHandlingCost(String baseHandlingCost) {
        this.baseHandlingCost = baseHandlingCost;
    }

    /**
     * 
     * @return
     *     The handlingCostExTax
     */
    public String getHandlingCostExTax() {
        return handlingCostExTax;
    }

    /**
     * 
     * @param handlingCostExTax
     *     The handling_cost_ex_tax
     */
    public void setHandlingCostExTax(String handlingCostExTax) {
        this.handlingCostExTax = handlingCostExTax;
    }

    /**
     * 
     * @return
     *     The handlingCostIncTax
     */
    public String getHandlingCostIncTax() {
        return handlingCostIncTax;
    }

    /**
     * 
     * @param handlingCostIncTax
     *     The handling_cost_inc_tax
     */
    public void setHandlingCostIncTax(String handlingCostIncTax) {
        this.handlingCostIncTax = handlingCostIncTax;
    }

    /**
     * 
     * @return
     *     The handlingCostTax
     */
    public String getHandlingCostTax() {
        return handlingCostTax;
    }

    /**
     * 
     * @param handlingCostTax
     *     The handling_cost_tax
     */
    public void setHandlingCostTax(String handlingCostTax) {
        this.handlingCostTax = handlingCostTax;
    }

    /**
     * 
     * @return
     *     The handlingCostTaxClassId
     */
    public Integer getHandlingCostTaxClassId() {
        return handlingCostTaxClassId;
    }

    /**
     * 
     * @param handlingCostTaxClassId
     *     The handling_cost_tax_class_id
     */
    public void setHandlingCostTaxClassId(Integer handlingCostTaxClassId) {
        this.handlingCostTaxClassId = handlingCostTaxClassId;
    }

    /**
     * 
     * @return
     *     The baseWrappingCost
     */
    public String getBaseWrappingCost() {
        return baseWrappingCost;
    }

    /**
     * 
     * @param baseWrappingCost
     *     The base_wrapping_cost
     */
    public void setBaseWrappingCost(String baseWrappingCost) {
        this.baseWrappingCost = baseWrappingCost;
    }

    /**
     * 
     * @return
     *     The wrappingCostExTax
     */
    public String getWrappingCostExTax() {
        return wrappingCostExTax;
    }

    /**
     * 
     * @param wrappingCostExTax
     *     The wrapping_cost_ex_tax
     */
    public void setWrappingCostExTax(String wrappingCostExTax) {
        this.wrappingCostExTax = wrappingCostExTax;
    }

    /**
     * 
     * @return
     *     The wrappingCostIncTax
     */
    public String getWrappingCostIncTax() {
        return wrappingCostIncTax;
    }

    /**
     * 
     * @param wrappingCostIncTax
     *     The wrapping_cost_inc_tax
     */
    public void setWrappingCostIncTax(String wrappingCostIncTax) {
        this.wrappingCostIncTax = wrappingCostIncTax;
    }

    /**
     * 
     * @return
     *     The wrappingCostTax
     */
    public String getWrappingCostTax() {
        return wrappingCostTax;
    }

    /**
     * 
     * @param wrappingCostTax
     *     The wrapping_cost_tax
     */
    public void setWrappingCostTax(String wrappingCostTax) {
        this.wrappingCostTax = wrappingCostTax;
    }

    /**
     * 
     * @return
     *     The wrappingCostTaxClassId
     */
    public Integer getWrappingCostTaxClassId() {
        return wrappingCostTaxClassId;
    }

    /**
     * 
     * @param wrappingCostTaxClassId
     *     The wrapping_cost_tax_class_id
     */
    public void setWrappingCostTaxClassId(Integer wrappingCostTaxClassId) {
        this.wrappingCostTaxClassId = wrappingCostTaxClassId;
    }

    /**
     * 
     * @return
     *     The totalExTax
     */
    public String getTotalExTax() {
        return totalExTax;
    }

    /**
     * 
     * @param totalExTax
     *     The total_ex_tax
     */
    public void setTotalExTax(String totalExTax) {
        this.totalExTax = totalExTax;
    }

    /**
     * 
     * @return
     *     The totalIncTax
     */
    public String getTotalIncTax() {
        return totalIncTax;
    }

    /**
     * 
     * @param totalIncTax
     *     The total_inc_tax
     */
    public void setTotalIncTax(String totalIncTax) {
        this.totalIncTax = totalIncTax;
    }

    /**
     * 
     * @return
     *     The totalTax
     */
    public String getTotalTax() {
        return totalTax;
    }

    /**
     * 
     * @param totalTax
     *     The total_tax
     */
    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

    /**
     * 
     * @return
     *     The itemsTotal
     */
    public Integer getItemsTotal() {
        return itemsTotal;
    }

    /**
     * 
     * @param itemsTotal
     *     The items_total
     */
    public void setItemsTotal(Integer itemsTotal) {
        this.itemsTotal = itemsTotal;
    }

    /**
     * 
     * @return
     *     The itemsShipped
     */
    public Integer getItemsShipped() {
        return itemsShipped;
    }

    /**
     * 
     * @param itemsShipped
     *     The items_shipped
     */
    public void setItemsShipped(Integer itemsShipped) {
        this.itemsShipped = itemsShipped;
    }

    /**
     * 
     * @return
     *     The paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * 
     * @param paymentMethod
     *     The payment_method
     */
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * 
     * @return
     *     The paymentProviderId
     */
    public String getPaymentProviderId() {
        return paymentProviderId;
    }

    /**
     * 
     * @param paymentProviderId
     *     The payment_provider_id
     */
    public void setPaymentProviderId(String paymentProviderId) {
        this.paymentProviderId = paymentProviderId;
    }

    /**
     * 
     * @return
     *     The paymentStatus
     */
    public String getPaymentStatus() {
        return paymentStatus;
    }

    /**
     * 
     * @param paymentStatus
     *     The payment_status
     */
    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    /**
     * 
     * @return
     *     The refundedAmount
     */
    public String getRefundedAmount() {
        return refundedAmount;
    }

    /**
     * 
     * @param refundedAmount
     *     The refunded_amount
     */
    public void setRefundedAmount(String refundedAmount) {
        this.refundedAmount = refundedAmount;
    }

    /**
     * 
     * @return
     *     The orderIsDigital
     */
    public Boolean getOrderIsDigital() {
        return orderIsDigital;
    }

    /**
     * 
     * @param orderIsDigital
     *     The order_is_digital
     */
    public void setOrderIsDigital(Boolean orderIsDigital) {
        this.orderIsDigital = orderIsDigital;
    }

    /**
     * 
     * @return
     *     The storeCreditAmount
     */
    public String getStoreCreditAmount() {
        return storeCreditAmount;
    }

    /**
     * 
     * @param storeCreditAmount
     *     The store_credit_amount
     */
    public void setStoreCreditAmount(String storeCreditAmount) {
        this.storeCreditAmount = storeCreditAmount;
    }

    /**
     * 
     * @return
     *     The giftCertificateAmount
     */
    public String getGiftCertificateAmount() {
        return giftCertificateAmount;
    }

    /**
     * 
     * @param giftCertificateAmount
     *     The gift_certificate_amount
     */
    public void setGiftCertificateAmount(String giftCertificateAmount) {
        this.giftCertificateAmount = giftCertificateAmount;
    }

    /**
     * 
     * @return
     *     The ipAddress
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * 
     * @param ipAddress
     *     The ip_address
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * 
     * @return
     *     The geoipCountry
     */
    public String getGeoipCountry() {
        return geoipCountry;
    }

    /**
     * 
     * @param geoipCountry
     *     The geoip_country
     */
    public void setGeoipCountry(String geoipCountry) {
        this.geoipCountry = geoipCountry;
    }

    /**
     * 
     * @return
     *     The geoipCountryIso2
     */
    public String getGeoipCountryIso2() {
        return geoipCountryIso2;
    }

    /**
     * 
     * @param geoipCountryIso2
     *     The geoip_country_iso2
     */
    public void setGeoipCountryIso2(String geoipCountryIso2) {
        this.geoipCountryIso2 = geoipCountryIso2;
    }

    /**
     * 
     * @return
     *     The currencyId
     */
    public Integer getCurrencyId() {
        return currencyId;
    }

    /**
     * 
     * @param currencyId
     *     The currency_id
     */
    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    /**
     * 
     * @return
     *     The currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * 
     * @param currencyCode
     *     The currency_code
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * 
     * @return
     *     The currencyExchangeRate
     */
    public String getCurrencyExchangeRate() {
        return currencyExchangeRate;
    }

    /**
     * 
     * @param currencyExchangeRate
     *     The currency_exchange_rate
     */
    public void setCurrencyExchangeRate(String currencyExchangeRate) {
        this.currencyExchangeRate = currencyExchangeRate;
    }

    /**
     * 
     * @return
     *     The defaultCurrencyId
     */
    public Integer getDefaultCurrencyId() {
        return defaultCurrencyId;
    }

    /**
     * 
     * @param defaultCurrencyId
     *     The default_currency_id
     */
    public void setDefaultCurrencyId(Integer defaultCurrencyId) {
        this.defaultCurrencyId = defaultCurrencyId;
    }

    /**
     * 
     * @return
     *     The defaultCurrencyCode
     */
    public String getDefaultCurrencyCode() {
        return defaultCurrencyCode;
    }

    /**
     * 
     * @param defaultCurrencyCode
     *     The default_currency_code
     */
    public void setDefaultCurrencyCode(String defaultCurrencyCode) {
        this.defaultCurrencyCode = defaultCurrencyCode;
    }

    /**
     * 
     * @return
     *     The staffNotes
     */
    public String getStaffNotes() {
        return staffNotes;
    }

    /**
     * 
     * @param staffNotes
     *     The staff_notes
     */
    public void setStaffNotes(String staffNotes) {
        this.staffNotes = staffNotes;
    }

    /**
     * 
     * @return
     *     The customerMessage
     */
    public String getCustomerMessage() {
        return customerMessage;
    }

    /**
     * 
     * @param customerMessage
     *     The customer_message
     */
    public void setCustomerMessage(String customerMessage) {
        this.customerMessage = customerMessage;
    }

    /**
     * 
     * @return
     *     The discountAmount
     */
    public String getDiscountAmount() {
        return discountAmount;
    }

    /**
     * 
     * @param discountAmount
     *     The discount_amount
     */
    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * 
     * @return
     *     The couponDiscount
     */
    public String getCouponDiscount() {
        return couponDiscount;
    }

    /**
     * 
     * @param couponDiscount
     *     The coupon_discount
     */
    public void setCouponDiscount(String couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    /**
     * 
     * @return
     *     The shippingAddressCount
     */
    public Integer getShippingAddressCount() {
        return shippingAddressCount;
    }

    /**
     * 
     * @param shippingAddressCount
     *     The shipping_address_count
     */
    public void setShippingAddressCount(Integer shippingAddressCount) {
        this.shippingAddressCount = shippingAddressCount;
    }

    /**
     * 
     * @return
     *     The isDeleted
     */
    public Boolean getIsDeleted() {
        return isDeleted;
    }

    /**
     * 
     * @param isDeleted
     *     The is_deleted
     */
    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 
     * @return
     *     The ebayOrderId
     */
    public String getEbayOrderId() {
        return ebayOrderId;
    }

    /**
     * 
     * @param ebayOrderId
     *     The ebay_order_id
     */
    public void setEbayOrderId(String ebayOrderId) {
        this.ebayOrderId = ebayOrderId;
    }

    /**
     * 
     * @return
     *     The billingAddress
     */
    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    /**
     * 
     * @param billingAddress
     *     The billing_address
     */
    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    /**
     * 
     * @return
     *     The orderSource
     */
    public String getOrderSource() {
        return orderSource;
    }

    /**
     * 
     * @param orderSource
     *     The order_source
     */
    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    /**
     * 
     * @return
     *     The externalSource
     */
    public Object getExternalSource() {
        return externalSource;
    }

    /**
     * 
     * @param externalSource
     *     The external_source
     */
    public void setExternalSource(Object externalSource) {
        this.externalSource = externalSource;
    }

    /**
     * 
     * @return
     *     The products
     */
    public Products_1 getProducts() {
        return products;
    }

    /**
     * 
     * @param products
     *     The products
     */
    public void setProducts(Products_1 products) {
        this.products = products;
    }

    /**
     * 
     * @return
     *     The shippingAddresses
     */
    public ShippingAddresses getShippingAddresses() {
        return shippingAddresses;
    }

    /**
     * 
     * @param shippingAddresses
     *     The shipping_addresses
     */
    public void setShippingAddresses(ShippingAddresses shippingAddresses) {
        this.shippingAddresses = shippingAddresses;
    }

    /**
     * 
     * @return
     *     The coupons
     */
    public Coupons getCoupons() {
        return coupons;
    }

    /**
     * 
     * @param coupons
     *     The coupons
     */
    public void setCoupons(Coupons coupons) {
        this.coupons = coupons;
    }

}
