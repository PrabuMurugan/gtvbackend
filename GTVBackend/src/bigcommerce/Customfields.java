/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.util.List;

/**
 *
 * @author Abani
 */
public class Customfields {

    public List<Customfield> getCustomfields() {
        return customfields;
    }

    public void setCustomfields(List<Customfield> customfields) {
        this.customfields = customfields;
    }
     private List<Customfield> customfields;

    @Override
    public String toString() {
        return "Customfields{" + "customfields=" + customfields + '}';
    }
}
