package bigcommerce.orders;

/**
 *
 * @author Raj
 */
import bigcommerce.json.ShippingAddress;
import bigcommerce.json.Applied_discount;
import bigcommerce.jsonwrapper.OrderedProductsWrapper;
import bigcommerce.json.OrderedProduct;
import bigcommerce.json.OrderCoupon;
import bigcommerce.json.Orders;
import bigcommerce.base.BaseBigCommnerce;
import bigcommerce.shipment.FetchShipments;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FetchOrders  {
   public static int LAST_HOURS_ORDERS_ONLY=0;
    public static void main(String  [] args) throws Exception {
        int  iPastDays = 3;
        if(args.length != 1 ) {
            System.out.println("Usage : FetchOrders <past_days>");
           // System.exit(0);
        } else {
            try {
                iPastDays = Integer.parseInt(args[0]);
                if(iPastDays < 0 ) throw new Exception("");
            } catch(Exception exception){
                System.out.println("Invalid parameter");
                System.exit(0);
            }
        }
        String strFileter = "";
        if(iPastDays == 0  && LAST_HOURS_ORDERS_ONLY==0) {
            int iNumber = 99;
            Connection connection;
            try {
                util.DB.CONNECT_TO_PROD_DEBUG=true;
                connection = util.DB.getConnection("hb_bc");
                Statement  statement = connection.createStatement();
                ResultSet rs = statement.executeQuery("select max(id)+1 from orders");
                if(rs.next()) {
                    iNumber = rs.getInt(1);
                }
                if(iNumber==0)iNumber=100;
                System.out.println("First records will be fetched. " + iNumber);
                strFileter="min_id="+iNumber+"";
                rs.close();
                statement.close();
                connection.close();
            } catch (Exception ex) {
                System.out.println("There is some thing wrong : " + ex.getMessage());
                ex.printStackTrace();
            }
            
            
        } else if (LAST_HOURS_ORDERS_ONLY>0){
              String pattern = "yyyy-MM-dd'T'HH:mm:ss";
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, -(LAST_HOURS_ORDERS_ONLY+2));
            SimpleDateFormat format = new SimpleDateFormat(pattern);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            String strDate = format.format(cal.getTime());
            strFileter="min_date_modified="+strDate+"";
            System.out.println("Records will be fetched from " + strDate + " till today.");

        }
           else {
            String pattern = "yyyy-MM-dd'T'HH:mm:ss";
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -(iPastDays));
            SimpleDateFormat format = new SimpleDateFormat(pattern);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            String strDate = format.format(cal.getTime());
            strFileter="min_date_modified="+strDate+"";
            System.out.println("Records will be fetched from " + strDate + " till today.");
        }
        BaseBigCommnerce baseBigCommnerce = BaseBigCommnerce.getInstance();
        ArrayList<Orders> listOrders = baseBigCommnerce.getOrders(strFileter);
        System.out.println("[ FETCH_ORDERS ] : Number of order fetched : " + listOrders.size());
        if(!listOrders.isEmpty()) {
            Connection connection = baseBigCommnerce.getConnection();
            
               
            PreparedStatement  deleteOders = connection.prepareStatement("delete from orders where id=?");
            PreparedStatement  deleteBillingAddress = connection.prepareStatement("delete from billing_address where order_id=?");
            PreparedStatement  deleteOrderedProduct = connection.prepareStatement("delete from ordered_products where order_id=?");
            
            PreparedStatement  insertOrder = connection.prepareStatement("INSERT INTO Orders (customer_id,date_created,date_modified,date_shipped,status_id,"
                            + "status,subtotal_ex_tax,subtotal_inc_tax,subtotal_tax,base_shipping_cost,"
                            + "shipping_cost_ex_tax,shipping_cost_inc_tax,shipping_cost_tax,shipping_cost_tax_class_id,base_handling_cost,"
                            + "handling_cost_ex_tax,handling_cost_inc_tax,handling_cost_tax,handling_cost_tax_class_id,base_wrapping_cost,"
                            + "wrapping_cost_ex_tax,wrapping_cost_inc_tax,wrapping_cost_tax,wrapping_cost_tax_class_id,total_ex_tax,"
                            + "total_inc_tax,total_tax,items_total,items_shipped,payment_method,"
                            + "payment_provider_id,payment_status,refunded_amount,order_is_digital,store_credit_amount,"
                            + "gift_certificate_amount,ip_address,geoip_country,geoip_country_iso2,currency_id,"
                            + "currency_code,currency_exchange_rate,default_currency_id,default_currency_code,staff_notes,"
                            + "customer_message,discount_amount,coupon_discount,shipping_address_count,is_deleted,id) "
                            + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    
            for(Orders order:listOrders) {
                System.out.println("[ FETCH_ORDERS ] : Processing order number : " + order.getId());
                deleteOrderedProduct.setLong(1, order.getId());
                deleteBillingAddress.setLong(1, order.getId());
                deleteOders.setLong(1, order.getId());
                
                deleteOrderedProduct.execute();
                deleteBillingAddress.execute();
                deleteOders.execute();
                
                insertOrder.setString(1, ""+order.getCustomer_id());
                insertOrder.setString(2, order.getDate_created());
                insertOrder.setString(3, order.getDate_modified());
                insertOrder.setString(4, order.getDate_shipped());
                insertOrder.setString(5, ""+order.getStatus_id());

                insertOrder.setString(6, order.getStatus());
                insertOrder.setString(7, order.getSubtotal_ex_tax());
                insertOrder.setString(8, order.getSubtotal_inc_tax());
                insertOrder.setString(9, order.getSubtotal_tax());
                insertOrder.setString(10, order.getBase_shipping_cost());

                insertOrder.setString(11, order.getShipping_cost_ex_tax());
                insertOrder.setString(12, order.getShipping_cost_inc_tax());
                insertOrder.setString(13, order.getShipping_cost_tax());
                insertOrder.setString(14, ""+order.getShipping_cost_tax_class_id());
                insertOrder.setString(15, order.getBase_handling_cost());

                insertOrder.setString(16, order.getHandling_cost_ex_tax());
                insertOrder.setString(17, order.getHandling_cost_inc_tax());
                insertOrder.setString(18, order.getHandling_cost_tax());
                insertOrder.setString(19, ""+order.getHandling_cost_tax_class_id());
                insertOrder.setString(20, order.getBase_wrapping_cost());

                insertOrder.setString(21, order.getWrapping_cost_ex_tax());
                insertOrder.setString(22, order.getWrapping_cost_inc_tax());
                insertOrder.setString(23, order.getWrapping_cost_tax());
                insertOrder.setString(24, ""+order.getWrapping_cost_tax_class_id());
                insertOrder.setString(25, order.getTotal_ex_tax());

                insertOrder.setString(26, order.getTotal_inc_tax());
                insertOrder.setString(27, order.getTotal_tax());
                insertOrder.setString(28, ""+order.getItems_total());
                insertOrder.setString(29, ""+order.getItems_shipped());
                insertOrder.setString(30, order.getPayment_method());

                insertOrder.setString(31, ""+order.getPayment_provider_id());
                insertOrder.setString(32, order.getPayment_status());
                insertOrder.setString(33, order.getRefunded_amount());
                insertOrder.setString(34, ""+order.isOrder_is_digital());
                insertOrder.setString(35, order.getStore_credit_amount());

                insertOrder.setString(36, order.getGift_certificate_amount());
                insertOrder.setString(37, order.getIp_address());
                insertOrder.setString(38, order.getGeoip_country());
                insertOrder.setString(39, order.getGeoip_country_iso2());
                insertOrder.setString(40, ""+order.getCurrency_id());

                insertOrder.setString(41, order.getCurrency_code());
                insertOrder.setString(42, order.getCurrency_exchange_rate());
                insertOrder.setString(43, ""+order.getDefault_currency_id());
                insertOrder.setString(44, order.getDefault_currency_code());
                insertOrder.setString(45, order.getStaff_notes());
        
                insertOrder.setString(46, order.getCustomer_message());
                insertOrder.setString(47, order.getDiscount_amount());
                insertOrder.setString(48, order.getCoupon_discount());
                insertOrder.setString(49, ""+order.getShipping_address_count());
                insertOrder.setString(50, ""+order.isIs_deleted());
                insertOrder.setInt(51, order.getId());
                insertOrder.execute();
            
                populateOrderUsedCoupons(baseBigCommnerce,connection, order.getId(),order.getCoupons());
                populateBillingAddress(connection, order.getId(),order.getBilling_address());
                populateOrderedProducts(baseBigCommnerce, connection, order.getProducts() );
                populateShippingAddresses(baseBigCommnerce, connection,order.getShipping_addresses());
                
                System.out.println("[ FETCH_ORDERS ] : Order processes successfully\n");     
            }
            deleteOrderedProduct.close();
            deleteBillingAddress.close();
            deleteOders.close();
            insertOrder.close();
            connection.commit();
            connection.close();
        } else{
            System.out.println("[ FETCH_ORDERS ] : No new orders");
        }
    }
    
   
    private static void populateShippingAddresses(BaseBigCommnerce baseBigCommnerce, Connection connection, Shipping_addresses shipping_addresses) {
        System.out.println("[ FETCH_ORDERS ] : Populating shipping addresses");
        if(shipping_addresses != null) {
            
            try {
                String jSONString = baseBigCommnerce.getJSONString(shipping_addresses.getUrl());
                
                String jsonString = "{\"shipping_addresses\":"+jSONString+"}";
                Gson gson = new  GsonBuilder().create();
                ShippingAddressesWrapper shippingAddressesWrapper = null;
                try{
                    shippingAddressesWrapper=gson.fromJson(jsonString, ShippingAddressesWrapper.class);
                }catch (Exception e2){System.out.println("Prabu small problem not a big problem in shippingAddressesWrapper");};
                
                if(shippingAddressesWrapper != null && shippingAddressesWrapper.getShipping_addresses() != null && !shippingAddressesWrapper.getShipping_addresses().isEmpty()) {
                   
                    List<ShippingAddress> listshippingAddresses = shippingAddressesWrapper.getShipping_addresses();
                    System.out.println("[ FETCH_ORDERS ] : Shipping Address Count : " + listshippingAddresses.size());
                    
                    PreparedStatement  deleteShippingAddress = connection.prepareStatement("delete from shipping_addresses where id=?");
                    PreparedStatement  insertsShippingAddress = connection.prepareStatement("INSERT INTO shipping_addresses (id,order_id,first_name,last_name,company,"
                            + " street_1, street_2,city, zip,country,"
                            + "country_iso2,state,email,phone,items_total,"
                            + "items_shipped,shipping_method,base_cost,cost_ex_tax,cost_inc_tax,"
                            + "cost_tax,cost_tax_class_id,base_handling_cost,handling_cost_ex_tax,handling_cost_inc_tax,"
                            + "handling_cost_tax,handling_cost_tax_class_id,shipping_zone_id,shipping_zone_name) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    
                    for(Object obj:listshippingAddresses) {
                        ShippingAddress shippingAddress = (ShippingAddress)obj;
                        deleteShippingAddress.setLong(1, shippingAddress.getId());
                        deleteShippingAddress.addBatch();
                        
                        insertsShippingAddress.setLong(1, shippingAddress.getId());
                        insertsShippingAddress.setLong(2, shippingAddress.getOrder_id());
                        insertsShippingAddress.setString(3, shippingAddress.getFirst_name());
                        insertsShippingAddress.setString(4, shippingAddress.getLast_name());
                        insertsShippingAddress.setString(5, ""+shippingAddress.getCompany());
                        
                        insertsShippingAddress.setString(6, shippingAddress.getStreet_1());
                        insertsShippingAddress.setString(7, shippingAddress.getStreet_2());
                        insertsShippingAddress.setString(8, shippingAddress.getCity());
                        insertsShippingAddress.setString(9, shippingAddress.getZip());
                        insertsShippingAddress.setString(10, shippingAddress.getCountry());
                        
                        insertsShippingAddress.setString(11, shippingAddress.getCountry_iso2());
                        insertsShippingAddress.setString(12, shippingAddress.getState());
                        insertsShippingAddress.setString(13, shippingAddress.getEmail());
                        insertsShippingAddress.setString(14, ""+shippingAddress.getPhone());
                        insertsShippingAddress.setString(15, ""+shippingAddress.getItems_total());
                        
                        insertsShippingAddress.setString(16, ""+shippingAddress.getItems_shipped());
                        insertsShippingAddress.setString(17, shippingAddress.getShipping_method());
                        insertsShippingAddress.setString(18, shippingAddress.getBase_cost());
                        insertsShippingAddress.setString(19, ""+shippingAddress.getCost_ex_tax());
                        insertsShippingAddress.setString(20, shippingAddress.getCost_inc_tax());
                        
                        insertsShippingAddress.setString(21, shippingAddress.getCost_tax());
                        insertsShippingAddress.setString(22, ""+shippingAddress.getCost_tax_class_id());
                        insertsShippingAddress.setString(23, shippingAddress.getBase_handling_cost());
                        insertsShippingAddress.setString(24, ""+shippingAddress.getHandling_cost_ex_tax());
                        insertsShippingAddress.setString(25, shippingAddress.getHandling_cost_inc_tax());
                        
                        insertsShippingAddress.setString(26, shippingAddress.getHandling_cost_tax());
                        insertsShippingAddress.setString(27, ""+shippingAddress.getHandling_cost_tax_class_id());
                        insertsShippingAddress.setString(28, ""+shippingAddress.getShipping_zone_id());
                        insertsShippingAddress.setString(29, ""+shippingAddress.getShipping_zone_name());
                        insertsShippingAddress.addBatch();
                    }
                    deleteShippingAddress.executeBatch();
                    insertsShippingAddress.executeBatch();
                    insertsShippingAddress.close();
                    deleteShippingAddress.close();
                    
                }   
            } catch(Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    private static void populateOrderedProducts(BaseBigCommnerce baseBigCommnerce,Connection connection, Products products) {
        System.out.println("[ FETCH_ORDERS ] : Populate ordered products");
        if(products != null) {
            try {
                String jSONString = baseBigCommnerce.getJSONString(products.getUrl());
                
                String jsonString = "{\"ordered_products\":"+jSONString+"}";
                Gson gson = new  GsonBuilder().create();
                OrderedProductsWrapper orderedProductsWrapper = null;
                try {
                    orderedProductsWrapper = gson.fromJson(jsonString, OrderedProductsWrapper.class);
                } catch(Exception exception){}
                
                
                if(orderedProductsWrapper != null && orderedProductsWrapper.getOrdered_products() != null && !orderedProductsWrapper.getOrdered_products().isEmpty()) {
                   
                    List<OrderedProduct> listOrderedProduct = orderedProductsWrapper.getOrdered_products();
                    System.out.println("[ FETCH_ORDERS ] : Number of products ordered : " + listOrderedProduct.size());
                    
                    
                    
                    PreparedStatement  insertOrderedProduct = connection.prepareStatement("INSERT INTO ordered_products (id,order_id,product_id,order_address_id,name,"
                            + "sku,type,base_price, price_ex_tax, price_inc_tax,"
                            + "price_tax, base_total,total_ex_tax, total_inc_tax,total_tax,"
                            + "weight,quantity,base_cost_price,cost_price_inc_tax,cost_price_ex_tax,"
                            + "cost_price_tax,is_refunded,refund_amount,return_id,wrapping_name,"
                            + "base_wrapping_cost,wrapping_cost_ex_tax,wrapping_cost_inc_tax,wrapping_cost_tax,wrapping_message,"
                            + "quantity_shipped,event_name,event_date,fixed_shipping_cost,ebay_item_id,"
                            + "ebay_transaction_id,option_set_id,parent_order_product_id,is_bundled_product_,bin_picking_number) values (?,?,?,?,?,"
                            + "?,?,?,?,?,"
                            + "?,?,?,?,?,"
                            + "?,?,?,?,?,"
                            + "?,?,?,?,?,"
                            + "?,?,?,?,?,"
                            + "?,?,?,?,?,"
                            + "?,?,?,?,?)");
                    
                    for(OrderedProduct orderedProduct:listOrderedProduct) {
                        System.out.println("[ FETCH_ORDERS ] : Ordered Product id : " + orderedProduct.getId());
                        
                        insertOrderedProduct.setLong(1, orderedProduct.getId());
                        insertOrderedProduct.setLong(2, orderedProduct.getOrder_id());
                        // I removed foreign key constraint from here. As if product is deleted then it will give exception
                        insertOrderedProduct.setString(3, ""+orderedProduct.getProduct_id());
                        insertOrderedProduct.setString(4, ""+orderedProduct.getOrder_address_id());
                        insertOrderedProduct.setString(5, ""+orderedProduct.getName());
                        
                        insertOrderedProduct.setString(6, orderedProduct.getSku());
                        insertOrderedProduct.setString(7, orderedProduct.getType());
                        insertOrderedProduct.setString(8, orderedProduct.getBase_price());
                        insertOrderedProduct.setString(9, orderedProduct.getPrice_ex_tax());
                        insertOrderedProduct.setString(10, orderedProduct.getPrice_inc_tax());
                        
                        insertOrderedProduct.setString(11, orderedProduct.getPrice_tax());
                        insertOrderedProduct.setString(12, orderedProduct.getBase_total());
                        insertOrderedProduct.setString(13, orderedProduct.getTotal_ex_tax());
                        insertOrderedProduct.setString(14, ""+orderedProduct.getTotal_inc_tax());
                        insertOrderedProduct.setString(15, ""+orderedProduct.getTotal_tax());
                        
                        insertOrderedProduct.setString(16, ""+orderedProduct.getWeight());
                        insertOrderedProduct.setString(17, ""+orderedProduct.getQuantity());
                        insertOrderedProduct.setString(18, orderedProduct.getBase_cost_price());
                        insertOrderedProduct.setString(19, ""+orderedProduct.getCost_price_inc_tax());
                        insertOrderedProduct.setString(20, orderedProduct.getCost_price_ex_tax());
                        
                        insertOrderedProduct.setString(21, orderedProduct.getCost_price_tax());
                        insertOrderedProduct.setString(22, ""+orderedProduct.isIs_refunded());
                        insertOrderedProduct.setString(23, orderedProduct.getRefund_amount());
                        insertOrderedProduct.setString(24, ""+orderedProduct.getReturn_id());
                        insertOrderedProduct.setString(25, orderedProduct.getWrapping_name());
                        
                        insertOrderedProduct.setString(26, orderedProduct.getBase_wrapping_cost());
                        insertOrderedProduct.setString(27, ""+orderedProduct.getWrapping_cost_ex_tax());
                        insertOrderedProduct.setString(28, ""+orderedProduct.getWrapping_cost_inc_tax());
                        insertOrderedProduct.setString(29, ""+orderedProduct.getWrapping_cost_tax());
                        insertOrderedProduct.setString(30, ""+orderedProduct.getWrapping_message());
                        
                        insertOrderedProduct.setString(31, ""+orderedProduct.getQuantity_shipped());
                        insertOrderedProduct.setString(32, ""+orderedProduct.getEvent_name());
                        insertOrderedProduct.setString(33, ""+orderedProduct.getEvent_date());
                        insertOrderedProduct.setString(34, ""+orderedProduct.getFixed_shipping_cost());
                        insertOrderedProduct.setString(35, ""+orderedProduct.getEbay_item_id());
                        
                        insertOrderedProduct.setString(36, orderedProduct.getEbay_transaction_id());
                        insertOrderedProduct.setString(37, ""+orderedProduct.getOption_set_id());
                        insertOrderedProduct.setString(38, ""+orderedProduct.getParent_order_product_id());
                        insertOrderedProduct.setString(39, ""+orderedProduct.isIs_bundled_product_());
                        insertOrderedProduct.setString(40, ""+orderedProduct.getBin_picking_number());
                    
                        insertOrderedProduct.execute();
                        populateAppliedDiscount(connection,orderedProduct);    
                        populateOrderedProductOptions(connection,orderedProduct.getId(), orderedProduct.getProduct_options());
                        populateOrderedProductConfifuratrionFields(connection,orderedProduct.getId(),orderedProduct.getConfigurable_fields());
                        
                        FetchShipments.setConnection(connection);
                        FetchShipments.fetchSipments(baseBigCommnerce,orderedProduct.getOrder_id());
                    
                    }
                    
                    insertOrderedProduct.close();
                }   
            } catch(Exception exception) {
                exception.printStackTrace();
            }
        }
        System.out.println("[ FETCH_ORDERS ] : Ordered products populated successfully");
    }

    private static void populateBillingAddress(Connection connection, int orderid, Billing_address billing_address) {
        System.out.println("[ FETCH_ORDERS ] : Populate billing address");
        PreparedStatement  delete;
        try {
            delete = connection.prepareStatement("delete from billing_address where order_id=?");
            PreparedStatement  inserts = connection.prepareStatement("INSERT INTO billing_address (order_id,first_name,last_name,company,street_1,"
                    + "street_2,city,state,zip,country,country_iso2,phone,email) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
                    delete.setLong(1, orderid);
                    billing_address.setOrder_id(orderid);
                    inserts.setLong(1, billing_address.getOrder_id());
                    inserts.setString(2, billing_address.getFirst_name());
                    inserts.setString(3, billing_address.getLast_name());
                    inserts.setString(4, billing_address.getCompany());
                    inserts.setString(5, billing_address.getStreet_1());
                    inserts.setString(6, billing_address.getStreet_2());
                    inserts.setString(7, billing_address.getCity());
                    inserts.setString(8, billing_address.getState());
                    inserts.setString(9, billing_address.getZip());
                    inserts.setString(10, billing_address.getCountry());
                    inserts.setString(11, billing_address.getCountry_iso2());
                    inserts.setString(12, billing_address.getPhone());
                    inserts.setString(13, billing_address.getEmail());
                    
                    delete.execute();
                    inserts.execute();
                    inserts.close();
                    delete.close();           
        } catch (SQLException ex) {
            Logger.getLogger(FetchOrders.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("[ FETCH_ORDERS ] : Billing address populated successfully.");
    }

    private static void populateOrderUsedCoupons(BaseBigCommnerce baseBigCommnerce, Connection connection, long id, Coupons coupons) throws Exception {
        
       ArrayList<OrderCoupon> listOrderCoupon = baseBigCommnerce.getOderUsedCoupons(coupons.getUrl());
       
       if(listOrderCoupon != null && ! listOrderCoupon.isEmpty()) {
           PreparedStatement  deleteOrderUsedCoupons = connection.prepareStatement("delete from order_used_coupons where order_id=?");
           PreparedStatement  insertsOrderUsedCoupons = connection.prepareStatement("INSERT INTO order_used_coupons (id,coupon_id,order_id,code,amount,type,discount) values (?,?,?,?,?,?,?)");
           
           deleteOrderUsedCoupons.setLong(1, id);
           deleteOrderUsedCoupons.execute();
           for(OrderCoupon orderUsedCoupon :listOrderCoupon ) {
                insertsOrderUsedCoupons.setLong(1, orderUsedCoupon.getId());
                insertsOrderUsedCoupons.setLong(2, orderUsedCoupon.getCoupon_id());
                insertsOrderUsedCoupons.setString(3, ""+orderUsedCoupon.getOrder_id());
                insertsOrderUsedCoupons.setString(4, orderUsedCoupon.getCode());
                insertsOrderUsedCoupons.setString(5, orderUsedCoupon.getAmount());
                insertsOrderUsedCoupons.setString(6, ""+orderUsedCoupon.getType());
                insertsOrderUsedCoupons.setString(7, orderUsedCoupon.getDiscount());

                insertsOrderUsedCoupons.addBatch();
           }
           
           insertsOrderUsedCoupons.executeBatch();
           insertsOrderUsedCoupons.close();
           deleteOrderUsedCoupons.close();
       }         
    }

    private static void populateAppliedDiscount(Connection connection, OrderedProduct orderedProduct) {
         System.out.println("[ FETCH_ORDERS ] : Populating applied discounts");
         if(orderedProduct.getApplied_discounts() != null && !orderedProduct.getApplied_discounts().isEmpty() ) {
             try {
                PreparedStatement  deleteAppliedDiscount;
                deleteAppliedDiscount = connection.prepareStatement("delete from applied_discounts where order_product_id=?");
                deleteAppliedDiscount.setLong(1, orderedProduct.getId());
                deleteAppliedDiscount.execute();
                
                PreparedStatement  insertAppliedDiscount = null;    
                for(Applied_discount applied_discount : orderedProduct.getApplied_discounts()) {
                    try{
                        insertAppliedDiscount = connection.prepareStatement("INSERT INTO applied_discounts (order_product_id,discount_id,amount) values (?,?,?)");
                        insertAppliedDiscount.setLong(1,orderedProduct.getId());
                        insertAppliedDiscount.setString(2, applied_discount.getId());
                        insertAppliedDiscount.setString(3, ""+applied_discount.getAmount());
                        insertAppliedDiscount.execute();                        
                    }catch (Exception e2){}

                }
                insertAppliedDiscount.close();
                deleteAppliedDiscount.close();
            } catch (SQLException ex) {
                Logger.getLogger(FetchOrders.class.getName()).log(Level.SEVERE, null, ex);
            }     
         }
         System.out.println("[ FETCH_ORDERS ] : Applied discounts populated successfully");
    }

    private static void populateOrderedProductOptions(Connection connection, long order_product_id, List<Product_option> product_options) {
        System.out.println("[ FETCH_ORDERS ] : Populating ordered product options");
         if(product_options != null && !product_options .isEmpty() ) {
             try {
                PreparedStatement  delete;
                delete = connection.prepareStatement("delete from order_product_options where order_product_id=?");
                delete.setLong(1, order_product_id);
                delete.execute();
                
                PreparedStatement  inserts = null;    
                for(Product_option product_option : product_options) {

                    inserts = connection.prepareStatement("INSERT INTO order_product_options (id,option_id,order_product_id,product_option_id,display_name,display_value,value,type,name,display_style) values (?,?,?,?,?,?,?,?,?,?)");
                    inserts.setLong(1,product_option.getId());
                    inserts.setLong(2, product_option.getOption_id());
                    inserts.setLong(3, order_product_id);
                    inserts.setLong(4, product_option.getProduct_option_id());
                    inserts.setString(5, product_option.getDisplay_name());
                    inserts.setString(6, product_option.getDisplay_value());
                    inserts.setString(7, product_option.getValue());
                    inserts.setString(8, product_option.getType());
                    inserts.setString(9, product_option.getName());
                    inserts.setString(10, product_option.getDisplay_style());
                    inserts.execute();
                }
                if(inserts != null) inserts.close();
                delete.close();
            } catch (SQLException ex) {
                Logger.getLogger(FetchOrders.class.getName()).log(Level.SEVERE, null, ex);
            }     
         }
         System.out.println("[ FETCH_ORDERS ] : Ordered product options populated successfully");
    }

    private static void populateOrderedProductConfifuratrionFields(Connection connection, long id, List<ConfigurableFields> configurable_fields) {
        System.out.println("[ FETCH_ORDERS ] : Populating ordered product configurable fields");
         if(configurable_fields != null && !configurable_fields .isEmpty() ) {
             try {
                PreparedStatement  deleteOrderedProductConfifuratrionFields;
                deleteOrderedProductConfifuratrionFields = connection.prepareStatement("delete from order_product_configurable_fields where order_product_id=?");
                deleteOrderedProductConfifuratrionFields.setLong(1, id);
                deleteOrderedProductConfifuratrionFields.execute();
                
                PreparedStatement  insertsOrderedProductConfifuratrionFields = null;    
                for(ConfigurableFields configurableFields : configurable_fields) {

                    insertsOrderedProductConfifuratrionFields = connection.prepareStatement("INSERT INTO order_product_configurable_fields (id,order_product_id,configurable_field_id,name,"
                            + "fieldtype,value,original_filename,select_box_options) "
                            + "values (?,?,?,?,?,?,?,?)");
                    insertsOrderedProductConfifuratrionFields.setLong(1,configurableFields.getId());
                    insertsOrderedProductConfifuratrionFields.setLong(2,id);
                    insertsOrderedProductConfifuratrionFields.setString(3,""+configurableFields.getConfigurable_field_id());
                    insertsOrderedProductConfifuratrionFields.setString(4,configurableFields.getName());
                    insertsOrderedProductConfifuratrionFields.setString(5,configurableFields.getFieldtype());
                    insertsOrderedProductConfifuratrionFields.setString(6,configurableFields.getValue());
                    insertsOrderedProductConfifuratrionFields.setString(7,configurableFields.getOriginal_filename());
                    insertsOrderedProductConfifuratrionFields.setString(8,""+configurableFields.getSelect_box_options());
                    
                    insertsOrderedProductConfifuratrionFields.execute();
                }
                if(insertsOrderedProductConfifuratrionFields != null) insertsOrderedProductConfifuratrionFields.close();
                deleteOrderedProductConfifuratrionFields.close();
            } catch (SQLException ex) {
                Logger.getLogger(FetchOrders.class.getName()).log(Level.SEVERE, null, ex);
            }     
         }
         System.out.println("[ FETCH_ORDERS ] : Ordered product configurable fields populated successfully");
    }
}

