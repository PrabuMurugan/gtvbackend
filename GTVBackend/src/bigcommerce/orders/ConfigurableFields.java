
package bigcommerce.orders;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("com.googlecode.jsonschema2pojo")
public class ConfigurableFields {

    @Expose
    private long id;
    private long order_product_id;
    @Expose
    private long configurable_field_id;
    @Expose
    private String name;
    @Expose
    private String fieldtype;
    @Expose
    private String value;
    @Expose
    private String original_filename;
    @Expose
    private List<String> select_box_options = new ArrayList<String>();

    public long getOrder_product_id() {
        return order_product_id;
    }

    public void setOrder_product_id(long order_product_id) {
        this.order_product_id = order_product_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getConfigurable_field_id() {
        return configurable_field_id;
    }

    public void setConfigurable_field_id(long configurable_field_id) {
        this.configurable_field_id = configurable_field_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFieldtype() {
        return fieldtype;
    }

    public void setFieldtype(String fieldtype) {
        this.fieldtype = fieldtype;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getOriginal_filename() {
        return original_filename;
    }

    public void setOriginal_filename(String original_filename) {
        this.original_filename = original_filename;
    }

    public List<String> getSelect_box_options() {
        return select_box_options;
    }

    public void setSelect_box_options(List<String> select_box_options) {
        this.select_box_options = select_box_options;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

}
