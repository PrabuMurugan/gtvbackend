/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce.orders;

import bigcommerce.json.ShippingAddress;
import bigcommerce.*;
import java.util.List;

/**
 *
 * @author Raj
 */
public class ShippingAddressesWrapper {
    private List<ShippingAddress> shipping_addresses;

    public List<ShippingAddress> getShipping_addresses() {
        return shipping_addresses;
    }

    public void setShipping_addresses(List<ShippingAddress> shipping_addresses) {
        this.shipping_addresses = shipping_addresses;
    }

    @Override
    public String toString() {
        return "ShippingAddressesWrapper{" + "shipping_addresses=" + shipping_addresses + '}';
    }

       
}
