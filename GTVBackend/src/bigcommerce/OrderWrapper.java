
package bigcommerce;

import javax.annotation.Generated;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class OrderWrapper {
    private List<Order> orders;

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
    
   
}
