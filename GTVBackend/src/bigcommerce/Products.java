/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Raj
 */
public class Products {
    private List<Product> products;
    public Products(){
        products = new ArrayList<Product>();
    }
    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Products{" + "products=" + products + '}';
    }
    
}
