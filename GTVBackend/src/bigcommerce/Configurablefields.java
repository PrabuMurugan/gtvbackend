/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.util.List;

/**
 *
 * @author Abani
 */
public class Configurablefields {

    private List<Configurablefield> configurablefields;

    public List<Configurablefield> getConfigurablefields() {
        return configurablefields;
    }

    public void setConfigurablefields(List<Configurablefield> configurablefields) {
        this.configurablefields = configurablefields;
    }

    @Override
    public String toString() {
        return "Configurablefields{" + "configurablefields=" + configurablefields + '}';
    }
}
