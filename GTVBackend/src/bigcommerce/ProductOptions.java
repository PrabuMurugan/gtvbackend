/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.util.List;

/**
 *
 * @author Abani
 */
public class ProductOptions {
    private List<ProductOption> productOptions;

    @Override
    public String toString() {
        return "ProductOptions{" + "productOptions=" + productOptions + '}';
    }

    public List<ProductOption> getProductOptions() {
        return productOptions;
    }

    public void setProductOptions(List<ProductOption> productOptions) {
        this.productOptions = productOptions;
    }
    
    
}
