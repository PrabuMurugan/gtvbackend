/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.util.List;

/**
 *
 * @author Abani
 */
public class ProdcutImages {
    private List<ProdcutImage> prodcutImages;

    public List<ProdcutImage> getProdcutImages() {
        return prodcutImages;
    }

    public void setProdcutImages(List<ProdcutImage> prodcutImages) {
        this.prodcutImages = prodcutImages;
    }
    
    @Override
    public String toString() {
        return "ProdcutImages{" + "prodcutImages=" + prodcutImages + '}';
    }
}
