/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce.data;

import java.util.ArrayList;

/**
 *
 * @author Raj
 */
public class AppliesTo {
    private String entity;
    private ArrayList ids;
    
    public AppliesTo(String entity) {
        this.entity = entity;
        ids = new ArrayList();
        
    }
    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public ArrayList getIds() {
        return ids;
    }

    public void setIds(ArrayList ids) {
        this.ids = ids;
    }
    
}
