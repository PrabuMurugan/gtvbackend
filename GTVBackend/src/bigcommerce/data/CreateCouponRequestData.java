/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce.data;

/**
 *
 * @author Raj
 */
public class CreateCouponRequestData {
    private String name;
    private String type;
    private String code;
    private boolean enabled;
    private int amount;
    private AppliesTo applies_to;

    public CreateCouponRequestData(String name, String type, String code, boolean enabled, int amount, AppliesTo applies_to) {
        this.name = name;
        this.type = type;
        this.code = code;
        this.enabled = enabled;
        this.amount = amount;
        this.applies_to = applies_to;
    }
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public AppliesTo getApplies_to() {
        return applies_to;
    }

    public void setApplies_to(AppliesTo applies_to) {
        this.applies_to = applies_to;
    }
    
}
