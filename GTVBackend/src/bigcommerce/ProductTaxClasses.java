/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.util.List;

/**
 *
 * @author Abani
 */
public class ProductTaxClasses {
    private List<ProductTaxClass> productTaxClasses;

    @Override
    public String toString() {
        return "ProductTaxClasses{" + "productTaxClasses=" + productTaxClasses + '}';
    }

    public List<ProductTaxClass> getProductTaxClasses() {
        return productTaxClasses;
    }

    public void setProductTaxClasses(List<ProductTaxClass> productTaxClasses) {
        this.productTaxClasses = productTaxClasses;
    }
    
    
}
