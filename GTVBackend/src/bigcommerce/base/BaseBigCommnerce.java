/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce.base;

import bigcommerce.Product;
import bigcommerce.Products;
import bigcommerce.json.OrderCoupon;
import bigcommerce.jsonwrapper.OrdereUsedCouponProductsWrapper;
import bigcommerce.jsonwrapper.OrdersWrapper;
import java.sql.Connection;
import java.util.ArrayList;
import bigcommerce.json.Orders;
import bigcommerce.json.Shipmants;
import bigcommerce.jsonwrapper.ShipmentsWrapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
 
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Raj
 */
public class BaseBigCommnerce {
    private String username;
    private String token;
    private String baseurl;
    private Connection connection;
    private static BaseBigCommnerce baseBigCommnerce;
    Gson gson = new  GsonBuilder().create();
    
    private BaseBigCommnerce(String username, String token, String baseurl, String schema, boolean bConnectAlwaysPrd) throws RuntimeException {
        this.username = username;
        this.token = token;
        this.baseurl = baseurl;
        
        try {
            util.DB.CONNECT_TO_PROD_DEBUG=bConnectAlwaysPrd;
            if(schema != null && !schema.equalsIgnoreCase("")) {
                connection = util.DB.getConnection(schema);
                connection.setAutoCommit(false);
            }
            
        } catch(Exception exception) {
            throw new RuntimeException(exception.getMessage());
        }
        
    }

    public static BaseBigCommnerce getInstance(){
        if(baseBigCommnerce != null)
            return baseBigCommnerce;
                return baseBigCommnerce = new BaseBigCommnerce("prabu", "1c215c55ab2f4a2b20ded8881a3ff16c", "https://www.harrisburgstore.com/api/v2/", "hb_bc", true);
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    
   public ArrayList<Orders> getOrders(String strFilter) {
        ArrayList<Orders> listOrders = new ArrayList<Orders>();
        try {
            int iPageNo=1, iLimit=50;
            while(true) {
                String strURL = baseurl+"orders.json?"+strFilter+"&page="+(iPageNo++)+"&limit="+iLimit;
                System.out.println("[ BASE_BC ] : Final url to fetch orders : " + strURL);
                String strJSONString = getJSONString(strURL);
                strJSONString = "{\"orders\":"+strJSONString+"}";
                OrdersWrapper ordersWrapper = gson.fromJson(strJSONString, OrdersWrapper.class);
                if(ordersWrapper != null && ordersWrapper.getOrders() != null && !ordersWrapper.getOrders().isEmpty()) {
                    for(Orders odrs: ordersWrapper.getOrders()) {
                        listOrders.add(odrs);
                    }
                    if(ordersWrapper.getOrders().size() < iLimit) {
                        break;
                    }
                } else {
                    break;
                }
            }
        } catch(Exception e) {}
        System.out.println("[ BASE_BC ] : Number of order fetched : " + listOrders.size());
        return listOrders;
    }
    @Override
    public String toString() {
        return "BaseBigCommnerce{" + "username=" + username + ", token=" + token + ", baseurl=" + baseurl + ", connection=" + connection + '}';
    }

    public String getJSONString(String strAPI) throws Exception {
        String authString = username + ":" + token;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
      
        HttpURLConnection httpCon = (HttpURLConnection) (new URL(strAPI)).openConnection();
        httpCon.setRequestProperty("Content-Type", "application/json");
        httpCon.setRequestProperty("Accept", "application/json");
        httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
        httpCon.setDoOutput(true);
        httpCon.setDoInput(true);
        httpCon.setRequestMethod("GET");

        InputStream responseIS = httpCon.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
        String line ="";
        StringBuilder sb = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        return new String(sb);
    }

    public ArrayList<OrderCoupon> getOderUsedCoupons(String url) throws Exception{
        
        System.out.println("[ BASE_BC ] : Final url to fetch order used coupons : " + url);
        ArrayList<OrderCoupon> listOrderCoupon = new ArrayList<OrderCoupon>();
          try {
        String strJSONString = getJSONString(url);
        String jsonString = "{\"ordere_used_coupons\":"+strJSONString+"}";
        OrdereUsedCouponProductsWrapper ordereUsedCouponProductsWrapper = null;
      
            ordereUsedCouponProductsWrapper =  gson.fromJson(jsonString, OrdereUsedCouponProductsWrapper.class);
            if(ordereUsedCouponProductsWrapper != null && ordereUsedCouponProductsWrapper.getOrdere_used_coupons() != null && !ordereUsedCouponProductsWrapper.getOrdere_used_coupons().isEmpty()) {   
                for(OrderCoupon orderCoupon : ordereUsedCouponProductsWrapper.getOrdere_used_coupons()){
                    listOrderCoupon.add(orderCoupon);
                }
            }
            System.out.println("[ BASE_BC ] : Number of coupons used : " + listOrderCoupon.size());
        } catch(Exception exception) {}
        
        return listOrderCoupon;
    }
    
    public ArrayList<Shipmants> getShipments(long strOrderId) throws Exception{
        ArrayList<Shipmants> alistShipments = new ArrayList<Shipmants>();
        String strJSONString = getJSONString(baseurl+"orders/"+strOrderId+"/shipments.json");
        strJSONString = "{\"shipments\":"+strJSONString+"}";
        ShipmentsWrapper shipmentsWrapper = null; 
        try {
            shipmentsWrapper = gson.fromJson(strJSONString, ShipmentsWrapper.class);
            if(shipmentsWrapper != null && shipmentsWrapper.getShipments() != null && !shipmentsWrapper.getShipments().isEmpty()) {
                for(Shipmants shipmants : shipmentsWrapper.getShipments() ) {
                    alistShipments.add(shipmants);
                }
            }
        }catch(Exception exception) {}
        return alistShipments;
    }

    public String updateProducts(Products products) {
        System.out.println("[ BASE_BC ] : Inside updateProducts");
        String strAPI = baseurl+"products/%s";
        System.out.println("[ BASE_BC ] : strAPI :" + strAPI);
        
        if(products.getProducts() != null && products.getProducts().size()>0) {
            try {
                List<Product> listProducts = products.getProducts();
                System.out.println("[ BASE_BC ] : Number of products to be update : " + listProducts.size());
                 String authString = username + ":" + token;
                byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
                String authStringEnc = new String(authEncBytes);                                                    
                
                Iterator iterator = products.getProducts().iterator();
                StringBuilder sb = new StringBuilder();
                while(iterator.hasNext()) {
                    sb = new StringBuilder();
                    Product product = (Product)iterator.next();
                    strAPI = baseurl+"products/%s";
                    strAPI = String.format(strAPI, product.getId());
                    System.out.println("[ BASE_BC ] : strAPI :" + strAPI);
                    HttpURLConnection httpCon = (HttpURLConnection) (new URL(strAPI)).openConnection();
                    httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
                    httpCon.setRequestProperty("Content-Type", "application/json");
                    httpCon.setRequestProperty("Accept", "application/json");
                    
                    httpCon.setRequestMethod("PUT");
                    httpCon.setDoOutput(true);
                    OutputStream out = httpCon.getOutputStream();
                    out.write((""+product).getBytes("UTF-8"));
                    out.flush();
                    System.out.println("[ BASE_BC ] : strJSON : " + product);    
                    
                    InputStream responseIS = httpCon.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
                    String line ;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    out.close();
                    responseIS.close();
                    Thread.sleep(7000);
                    System.out.println("[ BASE_BC ] : Response : " + new String(sb)+"\n");  
                }
               
                return new String(sb);
            } catch (MalformedURLException ex) {
                System.out.println("[ BASE_BC ] : Exception occured. Error MSG : " + ex.getMessage());
                ex.printStackTrace();
            } catch (IOException ex) {
                System.out.println("[ BASE_BC ] : Exception occured. Error MSG : " + ex.getMessage());
                ex.printStackTrace();
            } catch (InterruptedException ex) {
                Logger.getLogger(BaseBigCommnerce.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        System.out.println("[ BASE_BC ] : Leaving updateProducts");
        return null;
    }
    
    
    public int post(String url,String data) {
        System.out.println("[ BASE_BC ] : Inside post");
        String strAPI = baseurl+url;
        System.out.println("[ BASE_BC ] : strAPI :" + strAPI);
        String authString = username + ":" + token;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        HttpURLConnection httpCon;
        try {
            httpCon = (HttpURLConnection) (new URL(strAPI)).openConnection();
        
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");

            httpCon.setRequestMethod("POST");
            httpCon.setDoOutput(true);
            OutputStream out = httpCon.getOutputStream();
            out.write((data).getBytes("UTF-8"));
            out.flush();
            out.close();
            System.out.println("[ BASE_BC ] : strJSON : " + data);    

            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line ;
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            
            responseIS.close();
            System.out.println("[ BASE_BC ] : Response : " + new String(sb)+"\n");  
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        System.out.println("[ BASE_BC ] : Leaving post");
        return 0;
    }
}
