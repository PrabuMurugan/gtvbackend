/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce.base;

import bigcommerce.data.AppliesTo;
import bigcommerce.data.CreateCouponRequestData;
import com.google.gson.Gson;
import java.util.ArrayList;

/**
 *
 * @author Raj
 */
public class CreateDiscountCoupon {
    private static BaseBigCommnerce baseBigCommnerce = BaseBigCommnerce.getInstance();
    private static final String MODULE = "[ CreateDiscountCoupon ] :";
    
    private static final int iNumberiofCoupon = 10;
    private static final int iCouponCodeLenth = 9;
    private static final int iCouponCodePrefixLength = 4;
    private static final String strCouponCodePrefix = "3OFF";
    
    
    public static void main(String [] args){
        System.out.println(MODULE+ "Inside main");
        ArrayList <String> aryListCouponCodes = new ArrayList<String>();
        for(int i=0; i<iNumberiofCoupon; i++) {
            String strCode = strCouponCodePrefix + gen();
            while(strCode.length()!=iCouponCodeLenth) {
                strCode = strCouponCodePrefix + gen();
            }
            System.out.println(strCode);
            aryListCouponCodes.add(strCode);
        }
        System.out.println(MODULE+ "aryListCouponCodes : " +  aryListCouponCodes);
        for(String code :aryListCouponCodes ) {
            AppliesTo appliesTo = new AppliesTo("categories");
            appliesTo.getIds().add(0);
            CreateCouponRequestData createCouponRequestData = new CreateCouponRequestData("$3 off Referal Candy "+code, "per_total_discount", code, true, 3,appliesTo);
            String strJSON  = new Gson().toJson(createCouponRequestData);
            baseBigCommnerce.post("coupons",strJSON);
        }
        System.out.println(MODULE+ "Leaving main");
    }
    
    public static int gen() {
        return (int)(Math.round(Math.random() * Math.pow(10, (iCouponCodeLenth-iCouponCodePrefixLength))));
    }
}


