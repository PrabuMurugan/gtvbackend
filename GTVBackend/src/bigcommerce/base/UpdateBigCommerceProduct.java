/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce.base;

import bigcommerce.Product;
import bigcommerce.Products;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raj
 */
public class UpdateBigCommerceProduct {
    private static BaseBigCommnerce baseBigCommnerce = BaseBigCommnerce.getInstance();
    private static final String MODULE = "[ UpdateBigCommerceProduct ] :";
    
    public static void main(String [] args){
        System.out.println(MODULE+ "Inside main");
        Products products = new Products();
        
        Connection connection = baseBigCommnerce.getConnection();
        PreparedStatement prepareStatement = null;
        PreparedStatement updateDescription = null;
        
        ResultSet resultSet = null;
        try {
            prepareStatement = connection.prepareStatement("SELECT ID,SKU,DESCRIPTION FROM PRODUCT WHERE SKU LIKE 'MYNTRA%'");
            updateDescription = connection.prepareStatement("UPDATE PRODUCT SET DESCRIPTION=? WHERE SKU=?");
            resultSet = prepareStatement.executeQuery();
            while(resultSet.next()) {
                String strID = resultSet.getString("ID");
                String strSKU = resultSet.getString("SKU");
                String strDescription = resultSet.getString("DESCRIPTION");
                
                System.out.println("strID : " + strID);
                System.out.println("SKU : " + strSKU);
                System.out.println("");
              //  System.out.println("strDescription : " + strDescription+" ");                
                String strNewDescription = "<p><strong>SIZE CHART<br /></strong></p> " +
                                                "<p><br />&nbsp;<img class=\\\"__mce_add_custom__\\\" title=\\\"kurta-size.jpg\\\" src=\\\"%%GLOBAL_ShopPathSSL%%/product_images/uploaded_images/kurta-size.jpg\\\" alt=\\\"kurta-size.jpg\\\" width=\\\"839\\\" height=\\\"367\\\" /></p> " +
                                                "<p><strong>OUR INDIAN ETHNIC FASHION DESIGNS&nbsp;</strong></p> " +
                                                "<p style=\\\"padding-left: 30px;\\\">We have partnered with leading fashion retailer in India - Myntra &lt;<a href=\\\"http://myntra.com/ <http://myntra.com/>\\\" target=\\\"_blank\\\">http://myntra.com/ <http://myntra.com/></a>&gt; to bring all latest fashion designs all over USA. Our products are shipped from Harrisburg, Pennsylvania. Please click on \\\"Google Trusted Store\\\" badge on bottom left of the screen to view our shipping and customer service experience metrics and you order protection from Google. Please allow 7 business days handling time to process your order. You can return them back to our warehouse in  Harrisburg, Pennsylvania in case you do not like the product within 30 days.<br> </p> " +
                                                "<p><strong>STYLE NOTE</strong></p> " +
                                                "<p style=\\\"padding-left: 30px;\\\">Achieve a great ethnic look with this churidar, kurta and dupatta set from Anouk. The beautiful embroidery work and the trendy design will make you look the quintessential modern Indian woman.</p> " +
                                                "<p><br /><strong>PRODUCT DETAILS</strong></p> " +
                                                "<p style=\\\"padding-left: 30px;\\\">Black and purple churidar, kurta and dupatta set<br />Black and purple embroidered kurta, woven, contrast purple embroidery throughout with sequin work, has a stylised round neck, cap sleeves, multi-coloured embroidery at the front hem, straight vented hem with purple taping along the front hem<br />Purple churidar bottom, woven, has drawstring fastening at waist, pleats beneath yoke, tapered and vented hems with press buttoned cclosure<br />Black and purple embroidered dupatta, has a lace border<br /><br /></p> " +
                                                "<p><strong>MATERIAL &amp; CARE</strong></p> " +
                                                "<p style=\\\"padding-left: 30px;\\\">Cotton<br />Hand wash cold<br /><br /></p> " +
                                                "<p><strong>SIZE &amp; FIT</strong></p> " +
                                                "<p style=\\\"padding-left: 30px;\\\">Dupatta length: 2 metres<br />Dupatta width: 0.85 metres<br />The model (height 5''8\\\" and chest 33\\\") is wearing a size S</p>";
                updateDescription.setString(1, strNewDescription);
                
                updateDescription.setString(2, strSKU);
                updateDescription.addBatch();
                Product product = new Product();
                product.setId(Integer.parseInt(strID));
                product.setDescription(strNewDescription);
                product.setSku(strSKU);
                products.getProducts().add(product);
                
            }
            baseBigCommnerce.updateProducts(products);
            updateDescription.executeBatch();
            connection.commit();
        } catch (SQLException ex) {
            Logger.getLogger(UpdateBigCommerceProduct.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            
            try {
                if(resultSet != null ) resultSet.close();
                if(prepareStatement != null) prepareStatement.close();
                if(connection != null) connection.close();
            
            } catch (SQLException ex) {
                Logger.getLogger(UpdateBigCommerceProduct.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        System.out.println(MODULE+ "Leaving main");
    }
}


