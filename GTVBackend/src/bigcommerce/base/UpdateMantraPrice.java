/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce.base;

import bigcommerce.Product;
import bigcommerce.Products;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raj
 */
public class UpdateMantraPrice {
    private static BaseBigCommnerce baseBigCommnerce = BaseBigCommnerce.getInstance();
    private static final String MODULE = "[ UpdateMantraPrice ] :";
    
    public static void main(String [] args){
        System.out.println(MODULE+ "Inside main");
        Products products = new Products();
        
        Connection connection = baseBigCommnerce.getConnection();
        PreparedStatement prepareStatement = null;
        PreparedStatement updateDescription = null;
        
        ResultSet resultSet = null;
        try {
            prepareStatement = connection.prepareStatement("SELECT ID,SKU,PRICE FROM PRODUCT WHERE SKU LIKE 'MYNTRA%'");
            updateDescription = connection.prepareStatement("UPDATE PRODUCT SET PRICE=? WHERE SKU=?");
            resultSet = prepareStatement.executeQuery();
            while(resultSet.next()) {
                String strID = resultSet.getString("ID");
                String strSKU = resultSet.getString("SKU");
                String strPrice = resultSet.getString("PRICE");
                
                System.out.println("strID : " + strID);
                System.out.println("SKU : " + strSKU);
                System.out.println("strPrice : " + strPrice);
              
                double dPrice = Double.parseDouble(strPrice);
                double dNewPrice = ((dPrice * 20)/100);
                dNewPrice += dPrice;
                dNewPrice += 3;
                System.out.println("dNewPrice : " + dNewPrice);
                updateDescription.setDouble(1, dNewPrice);
                
                updateDescription.setString(2, strSKU);
                updateDescription.addBatch();
                Product product = new Product();
                product.setId(Integer.parseInt(strID));
                product.setPrice(""+dNewPrice);
                product.setSku(strSKU);
                products.getProducts().add(product);
                
            }
            baseBigCommnerce.updateProducts(products);
            updateDescription.executeBatch();
            connection.commit();
        } catch (SQLException ex) {
            Logger.getLogger(UpdateMantraPrice.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            
            try {
                if(resultSet != null ) resultSet.close();
                if(prepareStatement != null) prepareStatement.close();
                if(connection != null) connection.close();
            
            } catch (SQLException ex) {
                Logger.getLogger(UpdateMantraPrice.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        System.out.println(MODULE+ "Leaving main");
    }
}


