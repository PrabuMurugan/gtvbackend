/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import java.util.List;

/**
 *
 * @author Abani
 */
public class ProductVideos {
     private List<ProductVideo> productVideos;

    @Override
    public String toString() {
        return "productVideos{" + "productVideos=" + productVideos + '}';
    }

    public List<ProductVideo> getProductVideos() {
        return productVideos;
    }

    public void setProductVideos(List<ProductVideo> productVideos) {
        this.productVideos = productVideos;
    }
    
}
