/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce;

import bombayplaza.pricematch.RetrievePage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
 

/**
 *
 * @author Raj
 * 
 * How to use this program.
 * 
 * 1) Make sure you run below query before starting program.
 * 
 *      drop table temp_attributes
 *      create table temp_attributes as select  c.category_details,c.product_id, a.low_carb,a.Low_Sodium,a.Dairy_Free,a.Food_Service_Items,a.Gluten_Free,a.Vegan, a.Wheat_Free, a.Yeast_Free, a.Kosher, a.Fair_Trade, a.Organic, a.Specialty, a.Natural1, a.GND from  hb_bc.UNFI_pricelist_all a, hb_bc.exported_products_from_hb c where c.code like 'UN_%' and replace(a.item_code,'-','') = replace(c.code,'UN_','');
 * 
 * 2) Second query can be changed to update some products only. i.e. next time if we need to update product attributes for 3 products then fetch only those products
 */
public class UpdateProductTag {
    private static String QUERY = "select * from hb_bc.temp_attributes";
    
    
    public static void main(String [] args) {
        System.out.println("Inside main of UpdateProductTag");
        System.out.println("1) Make sure you run below query before starting program.");
        System.out.println("\tdrop table temp_attributes");
        System.out.println("\tcreate table temp_attributes as select  c.code sku, concat('http://www.harrisburgstore.com/',c.product_url) url,c.category_details,c.product_id, a.gmo,a.low_carb,a.Low_Sodium,a.Dairy_Free,a.Food_Service_Items,a.Gluten_Free,a.Vegan, a.Wheat_Free, a.Yeast_Free, a.Kosher, a.Fair_Trade, a.Organic, a.Specialty, a.Natural1, a.GND from  hb_bc.UNFI_pricelist_all a, hb_bc.exported_products_from_hb c where c.code like 'UN_%' and replace(a.item_code,'-','') = replace(c.code,'UN_','')");
        System.out.println("2) Second query can be changed to update some products only. i.e. next time if we need to update product attributes for 3 products then fetch only those products");
        
        long iStart = System.currentTimeMillis();
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap<String, JSONObject > map = new HashMap<String, JSONObject>();
        HashMap<String, Integer> productCatIDs = new HashMap<String, Integer>();
        HashMap<String, String> catid = new HashMap<String, String>();
        
        productCatIDs.put("Dairy_Free", 2048);
        catid.put("2048", "Dairy_Free");
        
        productCatIDs.put("Gluten_Free", 2077);
        catid.put("2077", "Gluten_Free");
        
        productCatIDs.put("Fair_Trade", 2074);
        catid.put("2074", "Fair_Trade");
        
        productCatIDs.put("low_carb", 2069);
        catid.put("2069", "low_carb");
        
        productCatIDs.put("Low_Sodium", 2070);
        catid.put("2070", "Low_Sodium");
        
        productCatIDs.put("Organic", 2076);
        catid.put("2076", "Organic");
        
        productCatIDs.put("Vegan", 2071);
        catid.put("2071", "Vegan");
        
        productCatIDs.put("Wheat_Free", 2072);
        catid.put("2072", "Wheat_Free");
        
        productCatIDs.put("Yeast_Free", 2073);
        catid.put("2073", "Yeast_Free");
        
        productCatIDs.put("Kosher", 2047);
        catid.put("2047", "Kosher");
        
        productCatIDs.put("gmo", 2085);
        catid.put("2085", "gmo");
        
        productCatIDs.put("natural1", 2086);
        catid.put("2086", "natural1");
        
        
        try {
            
            connection = RetrievePage.getConnection();
            System.out.println("Going to execute Query : " + QUERY);
            ps = connection.prepareCall(QUERY);
            rs = ps.executeQuery();
            System.out.println("After executing Query");
            while(rs.next()) {
                String strProductId = rs.getString("product_id");
                JSONObject jsono = new JSONObject();
                JSONArray jsona = new JSONArray();
                
                String Dairy_Free = rs.getString("Dairy_Free");
                String Gluten_Free = rs.getString("Gluten_Free");
                String Fair_Trade = rs.getString("Fair_Trade");
                String low_carb = rs.getString("low_carb");
                String Low_Sodium = rs.getString("Low_Sodium");
                String Organic = rs.getString("Organic");
                String Vegan = rs.getString("Vegan");
                String Wheat_Free = rs.getString("Wheat_Free");
                String Yeast_Free = rs.getString("Yeast_Free");
                String Kosher = rs.getString("Kosher");
                String gmo = rs.getString("gmo");
             //   String natural1 = rs.getString("natural1");
                String sku = rs.getString("sku");
                String url = rs.getString("url");
                
                if(Dairy_Free != null && !Dairy_Free.trim().equalsIgnoreCase("")) jsona.add(productCatIDs.get("Dairy_Free").intValue());
                if(Gluten_Free != null && !Gluten_Free.trim().equalsIgnoreCase("")) jsona.add(productCatIDs.get("Gluten_Free").intValue());
                if(Fair_Trade != null && !Fair_Trade.trim().equalsIgnoreCase("")) jsona.add(productCatIDs.get("Fair_Trade").intValue());
                if(low_carb != null && !low_carb.trim().equalsIgnoreCase("")) jsona.add(productCatIDs.get("low_carb").intValue());
                if(Low_Sodium != null && !Low_Sodium.trim().equalsIgnoreCase("")) jsona.add(productCatIDs.get("Low_Sodium").intValue());
                if(Organic != null && !Organic.trim().equalsIgnoreCase("")) jsona.add(productCatIDs.get("Organic").intValue());
                if(Vegan != null && !Vegan.trim().equalsIgnoreCase("")) jsona.add(productCatIDs.get("Vegan").intValue());
                if(Wheat_Free != null && !Wheat_Free.trim().equalsIgnoreCase("")) jsona.add(productCatIDs.get("Wheat_Free").intValue());
                if(Yeast_Free != null && !Yeast_Free.trim().equalsIgnoreCase("")) jsona.add(productCatIDs.get("Yeast_Free").intValue());
                if(Kosher != null && !Kosher.trim().equalsIgnoreCase("")) jsona.add(productCatIDs.get("Kosher").intValue());
                if(gmo != null && !gmo.trim().equalsIgnoreCase("")) jsona.add(productCatIDs.get("gmo").intValue());
              //  if(natural1 != null && !natural1.trim().equalsIgnoreCase("")) jsona.add(productCatIDs.get("natural1").intValue());
                
                if(jsona.size() > 0) {
                    
                    String strCategoryid = rs.getString("category_details");
                    strCategoryid = strCategoryid.substring(strCategoryid.indexOf("ID: ")+4,strCategoryid.indexOf(","));
                    jsona.add(Integer.parseInt(strCategoryid));
                    jsono.put("categories", jsona);
                    jsono.put("sku", sku);
                    jsono.put("url", url);
                    map.put(strProductId,jsono);
                } else {
                    System.out.println("Product skipped as no spcific attribute found : " + strProductId);
                }
                
            }
            
               
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(rs != null && !rs.isClosed()) rs.close();
                if(ps != null && !ps.isClosed()) ps.close();
                if(connection != null && !connection.isClosed()) connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(UpdateProductTag.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        System.out.println("No of products to be updated : " + map.size());
        int iremain = map.size();
        for(Iterator<String> flavoursIter = map.keySet().iterator(); flavoursIter.hasNext();) {
            String strPrd = (String) flavoursIter.next();
            JSONObject json = (JSONObject) map.get(strPrd);
            System.out.println("Updating product ID : " + strPrd + "\nCategories : " + print(json,catid));
            System.out.println("SKU : " + json.getString("sku"));
            System.out.println("Product URL : " + json.getString("url"));
            json.remove("sku");json.remove("url");
            BigCommerce.updateProduct(strPrd, json);
            System.out.println("Product Updated.  \nNumber of Product Remains : " + (iremain--));
            System.out.println();
       }
        long iEnd = System.currentTimeMillis() - iStart;
        System.out.println("Program took no of Millis . : " + iEnd);
        System.out.println("Leaving main of UpdateProductTag");
    }
    private static String print(JSONObject jo,HashMap<String, String> map) {
        String strCategories = "";
        JSONArray ja = jo.getJSONArray("categories");
        for(int i=0;i<ja.size()-1;i++) {
            java.lang.Integer str = (java.lang.Integer)ja.get(i);
            strCategories += (String)map.get(str.toString()) + ", ";
        }
        return strCategories;
    }
}
