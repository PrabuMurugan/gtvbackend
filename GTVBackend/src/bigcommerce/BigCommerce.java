package bigcommerce;

/**
 *
 * @author Raj
 */
import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.google.gson.Gson;
import com.google.gson.stream.MalformedJsonException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;

public class BigCommerce {
    
    public static void main(String  [] args) {
        
        String productid = null;
        String sku = null;
        String qty = null;
        String price = null;
        createProduct( ) ;
        if(args.length <= 2) {
            System.out.println("Usage : BigCommerce <productid> <sku> <quantity> <price>");
            System.exit(0);
        }
        
        productid = args[0];
        if(! args[1].equalsIgnoreCase("-1")) {
            sku = args[1];
        }
        if(! args[2].equalsIgnoreCase("-1")) {
            qty = args[2];
        }
        if(! args[3].equalsIgnoreCase("-1")) {
            price = args[3];
        }
        System.out.println(BigCommerce.updateProduct(productid, sku, qty, price,null));
    }
    
    public static String updateProduct(String productid, JSONObject jSONObject) {
            String webPage = "https://www.harrisburgstore.com/api/v2/products/"+productid+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            httpCon.setConnectTimeout(300000);
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
              
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }    
    
    public static String updateProduct(String productid, String sku, String qty, String price,Boolean is_free_shipping) {
        String webPage = "https://www.harrisburgstore.com/api/v2/products/"+productid+".json";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("PUT");
            OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
            
            JSONObject jSONObject = new JSONObject();
            if(sku != null && ! sku.equalsIgnoreCase("")) {
                jSONObject.put("sku", sku);
            }
            if(price != null && ! price.equalsIgnoreCase("")) {
                jSONObject.put("price", price);
            }
            if(qty != null && ! qty.equalsIgnoreCase("")) {
                jSONObject.put("inventory_level", qty);
            }
            if(is_free_shipping != null ) {
                jSONObject.put("is_free_shipping", is_free_shipping);
            }            
            String strJString = jSONObject.toString();
            System.out.println("Request : "+strJString);
            out.write(strJString);
            out.close(); 
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }
   public static String createProduct( ) {
        String webPage = "https://www.harrisburgstore.com/api/v2/products";
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            Properties systemProperties = System.getProperties();
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setDoOutput(true);
            httpCon.setRequestMethod("POST");
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            OutputStream os = httpCon.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(params));
            writer.flush();
            writer.close();
            os.close();
            httpCon.connect();
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }  
   
   public static String getCategoryId(String categoryDesc) {
        String webPage = "https://www.harrisburgstore.com/api/v2/categories?name="+(categoryDesc.replaceAll("\"", "").trim());
        String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
        String username="prabu";
        try {
            URL uri = new URL(webPage);
            
            String authString = username + ":" + token;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            
            HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
            httpCon.setRequestProperty("Content-Type", "application/xml");
           httpCon.setRequestProperty("Accept", "application/xml");
           httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
            httpCon.setRequestMethod("GET");
           
            InputStream responseIS = httpCon.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
            String line = reader.readLine();
            StringBuilder sb1 = new StringBuilder();
            line = reader.readLine();
            while (line != null) {
                sb1.append(line);
                line = reader.readLine();
            }
            System.out.println("Response : " + new String(sb1));
            
            return  new String(sb1);
        } catch(Exception exception) {
            exception.printStackTrace();
            return exception.getMessage();
        }
        
    }  
   
    public ArrayList<String> getOrderByStatus(ArrayList<String> statusary) {
        ArrayList<String> aryOrders = new ArrayList<String>();
        for (String status : statusary) {
            String webPage = "https://www.harrisburgstore.com/api/v2/orders?status_id=" + status;
            String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
            String username = "prabu";
            try {
                URL uri = new URL(webPage);

                String authString = username + ":" + token;
                byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
                String authStringEnc = new String(authEncBytes);

                HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
                httpCon.setRequestProperty("Content-Type", "application/json");
                httpCon.setRequestProperty("Accept", "application/json");
                httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
                httpCon.setRequestMethod("GET");

                InputStream responseIS = httpCon.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
                String line = reader.readLine();
                StringBuilder sb1 = new StringBuilder();
                // line = reader.readLine();
                while (line != null) {
                    sb1.append(line);
                    line = reader.readLine();
                }
                String strResponse = new String(sb1);
               // System.out.println("Response : " + strResponse);
                if(strResponse.trim().length() ==0) {
                    System.out.println("No Orders with status 'Manual Verification Required' or 'Disputed'");    
                    continue;
                }
                OrderWrapper myModelList = new Gson().fromJson("{\"orders\":" + new String(sb1) + "}", OrderWrapper.class);
                //System.out.println("Response : " + myModelList.getOrders().size());
                for (Order o : myModelList.getOrders()) {
                  //  System.out.println(o.getStatus());
                    aryOrders.add(""+o.getId());
                }
            } catch (Exception exception) {
               System.out.println(exception.getMessage());
               exception.printStackTrace();
            }
        }

        return aryOrders;
    }
static String getQuery(List<NameValuePair> params) throws  Exception
{
    StringBuilder result = new StringBuilder();
    boolean first = true;

    for (NameValuePair pair : params)
    {
        if (first)
            first = false;
        else
            result.append("&");

        result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
        result.append("=");
        result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
    }

    return result.toString();
}   
}
