package bigcommerce.shipment;

/**
 *
 * @author Raj
 */
import bigcommerce.json.Item;
import bigcommerce.json.ShipmentShippingAddress;
import bigcommerce.jsonwrapper.ShipmentsWrapper;
import bigcommerce.json.Shipmants;
import bigcommerce.base.BaseBigCommnerce;
import bigcommerce.orders.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;

public class FetchShipments {
    private static String token = "1c215c55ab2f4a2b20ded8881a3ff16c";
    private static String username="prabu";
    private static Connection connection = null;
    public static void main(String  [] args) {
        if(args.length != 1) {
            System.out.println("Usage : FetchShipments <orderid>");
            System.exit(0);
        }
        System.out.println(FetchShipments.fetchSipments(args[0]));
        
    }
    public static void setConnection(Connection con) {
        connection = con;
    }
    public static String fetchSipments(String orderid) {
        System.out.println("[ SHIPMENTS ] : Fetching shipemtns for order id " + orderid );   
        
            String webPage = "https://harrisburgstore.com/api/v2/orders/"+orderid+"/shipments.json";
            try {
                URL uri = new URL(webPage);
                String authString = username + ":" + token;
                byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
                String authStringEnc = new String(authEncBytes);

                HttpURLConnection httpCon = (HttpURLConnection) uri.openConnection();
                httpCon.setRequestProperty("Content-Type", "application/json");
                httpCon.setRequestProperty("Accept", "application/json");
                httpCon.setRequestProperty("Authorization", "Basic " + authStringEnc);
                httpCon.setDoOutput(true);
                httpCon.setDoInput(true);
                httpCon.setRequestMethod("GET");

                InputStream responseIS = httpCon.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(responseIS));
                String line ="";
                StringBuilder sb = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                
                String jsonString = "{\"shipments\":"+new String(sb)+"}";
                System.out.println("API response completed.");
                
                Gson gson = new  GsonBuilder().create();
                ShipmentsWrapper shipmentsWrapper = null; 
                try {
                    shipmentsWrapper = gson.fromJson(jsonString, ShipmentsWrapper.class);
                }catch(Exception exception) {
                    System.out.println("[ SHIPMENT ] : No shipment were found");
                }
                
                
                if(shipmentsWrapper != null && shipmentsWrapper.getShipments() != null && ! shipmentsWrapper.getShipments().isEmpty()) {
                   
                    
                    List<Shipmants> listShipmants = shipmentsWrapper.getShipments();
                    System.out.println("Number of Shipements for order " + orderid + " is " + listShipmants.size());
                    util.DB.CONNECT_TO_PROD_DEBUG=true;
                    if(connection == null)connection = util.DB.getConnection("hb_bc");
                    PreparedStatement  delete = connection.prepareStatement("delete from shipments where order_id=?");
                    delete.setString(1, orderid);
                    delete.execute();
                    PreparedStatement  inserts = connection.prepareStatement("INSERT INTO shipments (id,order_id,customer_id,order_address_id,date_created,"
                            + "tracking_number,shipping_method,comments) VALUES (?,?,?,?,?,?,?,?)");
                    
                    for(Object obj:listShipmants) {
                        Shipmants shipment = (Shipmants)obj;
                        inserts.setLong(1, shipment.getId());
                        inserts.setLong(2, shipment.getOrder_id());
                        inserts.setLong(3, new Long(shipment.getCustomer_id()));
                        inserts.setLong(4, new Long(shipment.getOrder_address_id()));
                        inserts.setString(5,shipment.getDate_created());
                        inserts.setString(6, shipment.getTracking_number());
                        inserts.setString(7,shipment.getShipping_method());
                        inserts.setString(8, shipment.getComments());
                        
                        populateShipmentShipmentAddress(connection, shipment.getId(), shipment.getShipping_address());
                        populateShipmentBillingAddress(connection, shipment.getId(), shipment.getBilling_address());
                        populateShipmentShipedItems(connection, shipment.getId(), shipment.getItems());
                        inserts.addBatch();
                    }
                    System.out.println("Records for shipments create successfully");
                    inserts.executeBatch();
                    System.out.println("Batchupdate completed");
                    inserts.close();
                    
                }
                    
            } catch(Exception exception) {
                exception.printStackTrace();
               
            }
        
        return "";
    }

    private static void populateShipmentBillingAddress(Connection connection, long id, bigcommerce.json.Billing_address billing_address) {
         System.out.println("[ SHIPMENTS ] : Populating shipment billing addresses.");
         PreparedStatement  deleteShipmentBillingAddress;
         try {
            deleteShipmentBillingAddress = connection.prepareStatement("delete from shipment_billing_address where shipment_id=?");
            PreparedStatement  insertsShipmentBillingAddress = connection.prepareStatement("INSERT INTO shipment_billing_address (shipment_id,first_name,last_name,company,street_1,"
                    + "street_2,city,state,zip,country,country_iso2,phone,email) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
                    deleteShipmentBillingAddress.setLong(1, id);
                    billing_address.setShipment_id(id);
                    insertsShipmentBillingAddress.setLong(1, billing_address.getShipment_id());
                    insertsShipmentBillingAddress.setString(2, billing_address.getFirst_name());
                    insertsShipmentBillingAddress.setString(3, billing_address.getLast_name());
                    insertsShipmentBillingAddress.setString(4, billing_address.getCompany());
                    insertsShipmentBillingAddress.setString(5, billing_address.getStreet_1());
                    insertsShipmentBillingAddress.setString(6, billing_address.getStreet_2());
                    insertsShipmentBillingAddress.setString(7, billing_address.getCity());
                    insertsShipmentBillingAddress.setString(8, billing_address.getState());
                    insertsShipmentBillingAddress.setString(9, billing_address.getZip());
                    insertsShipmentBillingAddress.setString(10, billing_address.getCountry());
                    insertsShipmentBillingAddress.setString(11, billing_address.getCountry_iso2());
                    insertsShipmentBillingAddress.setString(12, billing_address.getPhone());
                    insertsShipmentBillingAddress.setString(13, billing_address.getEmail());
                    
                    deleteShipmentBillingAddress.execute();
                    insertsShipmentBillingAddress.execute();
                    insertsShipmentBillingAddress.close();
                    deleteShipmentBillingAddress.close();
                    
        } catch (SQLException ex) {
            Logger.getLogger(FetchOrders.class.getName()).log(Level.SEVERE, null, ex);
        }
                        
        System.out.println("[ SHIPMENTS ] : Shipment billing address populated successfully");
    }

    private static void populateShipmentShipmentAddress(Connection connection, long id, ShipmentShippingAddress shipping_address) {
         System.out.println("[ SHIPMENTS ] : Populating shipment shipping addresses");
         PreparedStatement  deleteShipmentShippingAddress;
         try {
            deleteShipmentShippingAddress = connection.prepareStatement("delete from shipment_shipping_address where shipment_id=?");
            PreparedStatement  insertShipmentShippingAddress = connection.prepareStatement("INSERT INTO shipment_shipping_address (shipment_id,first_name,last_name,company,street_1,"
                    + "street_2,city,state,zip,country,country_iso2,phone,email) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
            
            deleteShipmentShippingAddress.setLong(1, id);
            shipping_address.setShipment_id(id);
            insertShipmentShippingAddress.setLong(1, shipping_address.getShipment_id());
            insertShipmentShippingAddress.setString(2, shipping_address.getFirst_name());
            insertShipmentShippingAddress.setString(3, shipping_address.getLast_name());
            insertShipmentShippingAddress.setString(4, shipping_address.getCompany());
            insertShipmentShippingAddress.setString(5, shipping_address.getStreet_1());
            insertShipmentShippingAddress.setString(6, shipping_address.getStreet_2());
            insertShipmentShippingAddress.setString(7, shipping_address.getCity());
            insertShipmentShippingAddress.setString(8, shipping_address.getState());
            insertShipmentShippingAddress.setString(9, shipping_address.getZip());
            insertShipmentShippingAddress.setString(10, shipping_address.getCountry());
            insertShipmentShippingAddress.setString(11, shipping_address.getCountry_iso2());
            insertShipmentShippingAddress.setString(12, shipping_address.getPhone());
            insertShipmentShippingAddress.setString(13, shipping_address.getEmail());

            deleteShipmentShippingAddress.execute();
            insertShipmentShippingAddress.execute();
            insertShipmentShippingAddress.close();
            deleteShipmentShippingAddress.close();
                    
        } catch (SQLException ex) {
            Logger.getLogger(FetchOrders.class.getName()).log(Level.SEVERE, null, ex);
        }
                        
        System.out.println("[ SHIPMENTS ] : Shipment Shipping Address Populated Successfully");
    }

    private static void populateShipmentShipedItems(Connection connection, long id, List<Item> items) {
         System.out.println("[ SHIPMENTS ] : Populating shipment shiped items");
         PreparedStatement  deleteShipmentShipAddress;
         try {
            deleteShipmentShipAddress = connection.prepareStatement("delete from shipment_shipped_items where shipment_id=?");
            PreparedStatement insertsShipmentShipItem = connection.prepareStatement("INSERT INTO shipment_shipped_items (shipment_id, order_product_id,product_id,quantity) values (?,?,?,?)");
            
                    deleteShipmentShipAddress.setLong(1, id);
                    for(Item item :items) {
                        insertsShipmentShipItem.setLong(1, id);
                        insertsShipmentShipItem.setLong(2, item.getOrder_product_id());
                        insertsShipmentShipItem.setLong(3, item.getProduct_id());
                        insertsShipmentShipItem.setLong(4, item.getQuantity());
                        insertsShipmentShipItem.addBatch();
                    }
                                       
                    deleteShipmentShipAddress.execute();
                    insertsShipmentShipItem.executeBatch();
                    insertsShipmentShipItem.close();
                    deleteShipmentShipAddress.close();
                    
        } catch (SQLException ex) {
            Logger.getLogger(FetchOrders.class.getName()).log(Level.SEVERE, null, ex);
        }
                        
        System.out.println("[ SHIPMENTS ] : Shipment shiped stems populated successfully");
    }

    public static void fetchSipments(BaseBigCommnerce baseBigCommnerce, int order_id) {
        System.out.println("[ SHIPMENTS ] : Populating shipment information");
        try {
            ArrayList<Shipmants> listShipments = baseBigCommnerce.getShipments(order_id);
            if(listShipments != null && !listShipments.isEmpty()) {
                
                PreparedStatement deleteShipments = connection.prepareStatement("delete from shipments where id=?");
                PreparedStatement deleteShipmentShippingAddress =  connection.prepareStatement("delete from shipment_shipping_address where shipment_id=?");
                PreparedStatement deleteShipmentBillingAddress = connection.prepareStatement("delete from shipment_billing_address where shipment_id=?");
                PreparedStatement  deleteShipmentShipAddress = connection.prepareStatement("delete from shipment_shipped_items where shipment_id=?");
                PreparedStatement  insertsShipments = connection.prepareStatement("INSERT INTO shipments (id,order_id,customer_id,order_address_id,date_created,"
                            + "tracking_number,shipping_method,comments) VALUES (?,?,?,?,?,?,?,?)");
                    
                for(Shipmants shipment:listShipments) {
                    
                    deleteShipments.setLong(1, shipment.getId());
                    deleteShipmentShippingAddress.setLong(1, shipment.getId());
                    deleteShipmentBillingAddress.setLong(1, shipment.getId());
                    deleteShipmentShipAddress.setLong(1, shipment.getId());
                    
                    insertsShipments.setLong(1, shipment.getId());
                    insertsShipments.setLong(2, shipment.getOrder_id());
                    insertsShipments.setLong(3, new Long(shipment.getCustomer_id()));
                    insertsShipments.setLong(4, new Long(shipment.getOrder_address_id()));
                    insertsShipments.setString(5,shipment.getDate_created());
                    insertsShipments.setString(6, shipment.getTracking_number());
                    insertsShipments.setString(7,shipment.getShipping_method());
                    insertsShipments.setString(8, shipment.getComments());
                    
                    deleteShipmentShipAddress.execute();
                    deleteShipmentBillingAddress.execute();
                    deleteShipmentShippingAddress.execute();
                    deleteShipments.execute();
                    insertsShipments.execute();
                    
                    populateShipmentShipmentAddress(connection, shipment.getId(), shipment.getShipping_address());
                    populateShipmentBillingAddress(connection, shipment.getId(), shipment.getBilling_address());
                    populateShipmentShipedItems(connection, shipment.getId(), shipment.getItems());
                    
                }
                deleteShipmentShipAddress.close();
                deleteShipmentBillingAddress.close();
                deleteShipmentShippingAddress.close();
                deleteShipments.close();
                insertsShipments.close();
                    
            } else {
                System.out.println("[ SHIPMENTS ] : No shipement information available");
            }
        } catch (Exception ex) {
            Logger.getLogger(FetchShipments.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("[ SHIPMENTS ] : Shipment information populated successfully");
    }
    
}

