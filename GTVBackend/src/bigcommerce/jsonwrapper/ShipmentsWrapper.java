/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce.jsonwrapper;

import bigcommerce.json.Shipmants;
import bigcommerce.orders.*;
import bigcommerce.*;
import java.util.List;

/**
 *
 * @author Raj
 */
public class ShipmentsWrapper {
    private List<Shipmants> shipments;

    public List<Shipmants> getShipments() {
        return shipments;
    }

    public void setShipments(List<Shipmants> shipments) {
        this.shipments = shipments;
    }

    @Override
    public String toString() {
        return "ShipmentsWrapper{" + "shipments=" + shipments + '}';
    }

  
}
