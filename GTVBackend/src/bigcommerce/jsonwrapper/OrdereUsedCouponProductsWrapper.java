/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce.jsonwrapper;

import bigcommerce.json.OrderCoupon;
import bigcommerce.*;
import java.util.List;

/**
 *
 * @author Raj
 */
public class OrdereUsedCouponProductsWrapper {
    private List<OrderCoupon> ordere_used_coupons;

    public List<OrderCoupon> getOrdere_used_coupons() {
        return ordere_used_coupons;
    }

    public void setOrdere_used_coupons(List<OrderCoupon> ordere_used_coupons) {
        this.ordere_used_coupons = ordere_used_coupons;
    }

    @Override
    public String toString() {
        return "OrdereUsedCouponProductsWrapper{" + "ordere_used_coupons=" + ordere_used_coupons + '}';
    }

}
