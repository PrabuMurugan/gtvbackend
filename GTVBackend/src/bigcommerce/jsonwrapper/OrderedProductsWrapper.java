/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce.jsonwrapper;

import bigcommerce.json.OrderedProduct;
import bigcommerce.*;
import java.util.List;

/**
 *
 * @author Raj
 */
public class OrderedProductsWrapper {
    private List<OrderedProduct> ordered_products;

    public List<OrderedProduct> getOrdered_products() {
        return ordered_products;
    }

    public void setOrdered_products(List<OrderedProduct> ordered_products) {
        this.ordered_products = ordered_products;
    }

    @Override
    public String toString() {
        return "OrderedProductsWrapper{" + "ordered_products=" + ordered_products + '}';
    }

}
