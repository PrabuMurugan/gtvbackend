/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce.jsonwrapper;

import bigcommerce.json.Orders;
import bigcommerce.*;
import java.util.List;

/**
 *
 * @author Raj
 */
public class OrdersWrapper {
    private List<Orders> orders;

    public List<Orders> getOrders() {
        return orders;
    }

    public void setOrders(List<Orders> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "Order{" + "orders=" + orders + '}';
    }
    
}
