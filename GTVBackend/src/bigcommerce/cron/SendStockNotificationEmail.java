/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bigcommerce.cron;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.internet.InternetAddress;
import mail.OrderItem;
import mail.SendMail;
import mail.SendShippingConfirmationMail;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import util.DB;

/**
 *
 * @author Abani
 */
public class SendStockNotificationEmail {
     public static void main(String[] args) {
         Connection con = null;
        try{
            con = DB.getConnection(null);
        PreparedStatement prest = con.prepareStatement("SELECT bi.id as bi_id,bi.user_email as email, p.id as id, p.name as name , p.custom_url as custom_url FROM hb_bc.product p, hb_bc.back_in_stock_notification bi WHERE bi.sku = p.sku AND p.inventory_level > ? AND bi.user_emailed = ?");
        prest.setInt(1, 5);
        prest.setInt(2, 0);
        ResultSet rs = prest.executeQuery();
        while (rs.next()) {

            long product_id = rs.getLong("id");
            long bi_id = rs.getLong("bi_id");
            String productName = rs.getString("name");
            String customURL = rs.getString("custom_url");
            String toEmail = rs.getString("email");
            Map<String, Object> rootMap = new HashMap<String, Object>();
            rootMap.put("name", productName);
            rootMap.put("custom_url", customURL);
       
                
               
                boolean result;
                System.out.println("Start Mail Sending.....");

                    result = SendMail.sendMail("html-stock-back-notify-mail-template.ftl", toEmail, rootMap, "Harrisburgstore.com: Product is back - "+productName);
                if (result) {
                    PreparedStatement update_status_prest = con.prepareStatement("update hb_bc.back_in_stock_notification set  user_emailed=? where id=?");
                    update_status_prest.setLong(1, 1);
                    update_status_prest.setLong(2, bi_id);
                    update_status_prest.executeUpdate();
                }
            

        }
        }catch(Exception e){
            Logger.getLogger(SendStockNotificationEmail.class.getName()).log(Level.SEVERE, null, e);
        }finally{
            try{
               con.close();
            }catch(Exception e){
                Logger.getLogger(SendStockNotificationEmail.class.getName()).log(Level.SEVERE, null, e);
            }
        }
     }
}
