/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package add_to_inventory;

import amazon.product.ProcessWorkingCatalog;
import bombayplaza.pricematch.RetrievePage;
import com.tajplaza.products.GetCompetitivePricingForASINSample;
import com.tajplaza.products.GetLowestPricedOffersForASINSample;
import com.tajplaza.products.GetMatchingProductSample;
import com.tajplaza.products.ListMatchingProductsSample;
import com.tajplaza.upload.UploadInventoryLoaderFeedFile;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import samsclub.CreateHBAMAVariations;
import samsclub.CreateHBProduct;

/**
 *
 * @author prabu
 */
public class AddAsinsToInventory {
    public static void main(String args[]) throws Exception{
        System.out.println("debug");
        BufferedWriter out= new BufferedWriter(new FileWriter("C:\\Google Drive\\Dropbox\\program\\add_to_inventory\\new-inventory-log.txt"));
        BufferedReader br0 = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\add_to_inventory\\add-to-inventory.txt")));
        Connection con = util.DB.getConnection("hb_bc");
       String strLine0;
       while ((strLine0 = br0.readLine()) != null)   {
           String[] arr=strLine0.split("\t");
           if(arr.length<2)
               continue;
           String upc=arr[0];
           Float oc=Float.valueOf(arr[1]);
            ArrayList ar=addToInventory(upc,oc,con);
            System.out.println("List:"+ar.toString());
            for(int i=0;i<ar.size();i++){
                HashMap hm=(HashMap) ar.get(i);
                System.out.println("hm:"+hm.toString());
                out.write(hm.get("notes")+"\t"+hm.toString());
                out.newLine();
            }            
       }
        
        

        out.close();

    }
    //function 
public static ArrayList asinsInserted=new ArrayList();
    public static ArrayList addToInventory(String upc, float oc,Connection con) throws Exception{
        ArrayList<HashMap> resultProducts=new ArrayList();
        
        {
            writeToNewInventory("SKU	 Name	quantity	price	product_id	 product_id_type	add-delete	item_condition	fulfillment_center_id	minimum-seller-allowed-price	maximum-seller-allowed-price",false);
            ArrayList<HashMap> products=ListMatchingProductsSample.getMatchingProducts(upc);
           int bestRank=9999999;
            for(int i=0;i<products.size();i++){
                HashMap hm=products.get(i);
                String asin=(String)hm.get("asin");
                resultProducts=addAsinToInvenory( hm, oc, resultProducts, con);
                //Add children of asin
                GetMatchingProductSample childs=new GetMatchingProductSample();
                 ArrayList arr=childs.getAllOtherVariationsBySize(asin);
                 for (int j=0;j<arr.size();j++){
                     HashMap hm3=(HashMap) arr.get(j);
                     String childAsin=(String) hm3.get("childAsin");
                     String childSize="";
                     if(hm3.containsKey("childSize")){
                         childSize=(String) hm3.get("childSize");

                     }
                        if(asinsInserted.contains(childAsin)){
                            continue;
                        }                      
                      ArrayList<HashMap> childPproducts=ListMatchingProductsSample.getMatchingProducts(childAsin);
                      HashMap hmChild=childPproducts.get(0);
                      hmChild.put("childSize", childSize);
                    resultProducts=addAsinToInvenory( hmChild, oc, resultProducts, con);

                 } 


            }   
           
            
        }
        File file=new File("C:\\Google Drive\\Dropbox\\program\\add_to_inventory\\new-inventory.txt");
        UploadInventoryLoaderFeedFile.uploadFileIntoAmazon(file);
        return resultProducts;
    }
    public static ArrayList addAsinToInvenory(HashMap hm,float oc,ArrayList resultProducts,Connection con) throws Exception{
                 
                String asin=(String)hm.get("asin");
                if(asinsInserted.contains(asin)){
                    return resultProducts;
                }          
                String title=(String)hm.get("Title");
                String imageurl=(String)hm.get("imageurl");
                System.out.println("Title:"+title);
        
                int newPkgQty=ProcessWorkingCatalog.calculatePackingQntFromTitle(title);
                 HashMap fbaMap=RetrievePage.getDetailsFromFbaCalculatorNewMWS(null,asin, "100",true);
                 String weight=String.valueOf(fbaMap.get("productInfoWeight")  );
                Float weightF=Float.valueOf(weight);
                Integer newOC=Math.round((oc)*newPkgQty);
                HashMap hm3=GetCompetitivePricingForASINSample.getJustBBPriceandRankMWS(asin);
                Float bestprice=(Float) hm3.get("bestprice");
                Float lowPrice=(newOC+4+(weightF*(float).4))/(float).85;
                Float maxPrice=lowPrice*(float)1.5;
                HashMap hmP=new HashMap();
                String newSKU=asin+"_AMA_"+newPkgQty+"PACK_"+newOC;
                hmP.put("sku", newSKU);
                hmP.put("bestprice", bestprice);
                
                hmP.put("title", title);
                hmP.put("imageurl", imageurl);
               lowPrice= Math.round(lowPrice*(float)100)/(float)100;
               maxPrice= Math.round(maxPrice*(float)100)/(float)100;
                if(bestprice<lowPrice || bestprice>lowPrice*1.75){
                     CreateHBAMAVariations.alreadyTriedInHB(asin,con,"Not created, current price too high or too low");
                    hmP.put("notes", "NOT created  Asin: "+asin+", BEst Price "+bestprice +" is in the NOT  range between :"+lowPrice + " and "+maxPrice);
                    resultProducts.add(hmP);
                    asinsInserted.add(asin);
                     return resultProducts;
                }
                   hmP.put("notes", "Created Asin: "+asin+", Price is in possible  range between :"+lowPrice + " and "+maxPrice +" asin:"+asin);
                 try{
                    // CreateHBProduct.DELETE_IF_EXISTS=true;
                     CreateHBProduct.createHBProductFromAmazon(newSKU,asin,hm);
                 }   catch (Exception e2){e2.printStackTrace();}
                 String str="SKU	TITLE	11	99	ASIN	1	a	11	DEFAULT	0	999";
                 str=str.replaceAll("SKU", newSKU);
                 str=str.replaceAll("TITLE", title);
                 str=str.replaceAll("ASIN", asin);
                 writeToNewInventory(str,true);
                 resultProducts.add(hmP);
                 asinsInserted.add(asin);
                 return resultProducts;
        
    }
    private static void writeToNewInventory(String str,boolean append) throws  Exception {
            FileWriter fstream_out1 = new FileWriter("C:\\Google Drive\\Dropbox\\program\\add_to_inventory\\new-inventory.txt",append);
            BufferedWriter out1 = new BufferedWriter(fstream_out1);
            out1.write(str);
            out1.newLine();
            out1.close();
    }    
}
