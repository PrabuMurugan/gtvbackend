package util;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DBMWS {

    public static boolean CONNECT_TO_PROD_DEBUG = true;
    private static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    private static String PORT = "";
    private static String USERNAME = "";
    private static String PASSWORD = "";
    
    public static Connection getConnection(String schemaName) throws Exception {
        
        Connection con = null;
        Class.forName(DATABASE_DRIVER);
        String PORT = System.getProperty("port", "3306");
        String SCHEMA = schemaName != null ? schemaName : "harrisburgstore";
        
        USERNAME = System.getProperty("user", "guest1");
        PASSWORD = System.getProperty("password", "guest1");
        
        String url = "jdbc:mysql://localhost:" + PORT + "/" + SCHEMA;
         url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/" + SCHEMA;

        if (CONNECT_TO_PROD_DEBUG) {
            HBLogger.debug("DBMWS.getConnection", "CONNECT_TO_PROD_DEBUG is set true. Connecting to production databse.");
            url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/" + SCHEMA;
        }
        HBLogger.debug("DBMWS.getConnection", "Database URL : " + url);
        try {
            con = DriverManager.getConnection(url, USERNAME, PASSWORD);
        } catch (Exception e) {
            //HBLogger.debug("DBMWS.getConnection", "Can not connect occured while connectind database. Error MSG : " + e.getMessage());    
            url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/" + SCHEMA;
            try{
                con = DriverManager.getConnection(url, USERNAME, PASSWORD);
            }catch (Exception e2){
                url = "jdbc:mysql://tajplaza.cdlqrs0jhvrj.us-east-1.rds.amazonaws.com:3306/" + SCHEMA;
                con = DriverManager.getConnection(url, USERNAME, PASSWORD);
            }
            HBLogger.debug("DBMWS.getConnection", "Using QA database");    
        }
        try{
            PreparedStatement ps=con.prepareStatement("SELECT @@max_allowed_packet;");
            ResultSet rs=ps.executeQuery();
            if(rs.next()){
                Double max_pkt=rs.getDouble(1);
                if(max_pkt<1677721){
                     PreparedStatement pst=con.prepareStatement("SET GLOBAL max_allowed_packet=16777216");
                     pst.executeUpdate();                     
                    
                }
                    
            }
        }catch (Exception e){e.printStackTrace();}
        HBLogger.debug("DBMWS.getConnection", "Returning database connection. Connection : " + con); 
        return con;
    }
}
