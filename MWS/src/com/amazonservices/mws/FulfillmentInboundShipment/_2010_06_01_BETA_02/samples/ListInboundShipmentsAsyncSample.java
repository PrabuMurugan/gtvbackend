/******************************************************************************* 
 *  Copyright 2009 Amazon Services. All Rights Reserved.
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 * 
 *  FBA Inbound Service MWS  Java Library
 *  API Version: 2010-06-01-BETA-02
 *  Generated: Wed Oct 13 15:44:56 UTC 2010 
 */

package com.amazonservices.mws.FulfillmentInboundShipment._2010_06_01_BETA_02.samples;

import java.util.List;
import java.util.ArrayList;
import com.amazonservices.mws.FulfillmentInboundShipment._2010_06_01_BETA_02.*;
import com.amazonservices.mws.FulfillmentInboundShipment._2010_06_01_BETA_02.model.*;
import java.util.concurrent.Future;

/**
 *
 * List Inbound Shipments  Samples
 *
 *
 */
public class ListInboundShipmentsAsyncSample {

    /**
     * Just add few required parameters, and try the service
     * List Inbound Shipments functionality
     *
     * @param args unused
     */
    public static void main(String... args) {

        /************************************************************************
         * Access Key ID and Secret Acess Key ID, obtained from:
         * http://mws.amazon.com
         ***********************************************************************/
        final String accessKeyId = "<Your Access Key ID>";
        final String secretAccessKey = "<Your Secret Access Key>";

        /************************************************************************
         * Marketplace and Seller IDs are required parameters for all 
         * MWS calls.
         ***********************************************************************/
        final String marketplaceId = "<Your Marketplace ID>";
        final String sellerId = "<Your Seller Id>";

        /************************************************************************
         * The application name and version are included in each MWS call's
         * HTTP User-Agent field. These are required fields.
         ***********************************************************************/
        final String applicationName = "<Your Application Name>";
        final String applicationVersion = "<Your Application Version or Build Number or Release Date>";

        /************************************************************************
         * Instantiate Http Client Implementation of FBA Inbound Service MWS 
         * 
         * Important! Number of threads in executor should match number of connections
         * for http client.
         *
         ***********************************************************************/
         FBAInboundServiceMWSConfig config = new FBAInboundServiceMWSConfig();

        /************************************************************************
         * Uncomment to set the correct MWS endpoint. You can get your Country
         * Code from MWSEndpoint enum class, e.g. US.
         ************************************************************************/
        // config.setServiceURL(MWSEndpoint.<Your Country Code>.toString());

         /************************************************************************
          * The argument (35) set below is the number of threads client should
          * spawn for processing.
          ***********************************************************************/
        config.setMaxAsyncThreads(35);

        /************************************************************************
         * Instantiate Http Client Implementation of FBA Inbound Service MWS 
         ***********************************************************************/
        // FBAInboundServiceMWSAsync service = new FBAInboundServiceMWSAsyncClient(accessKeyId, secretAccessKey, config);
        FBAInboundServiceMWSAsync service = new FBAInboundServiceMWSAsyncClient(accessKeyId, secretAccessKey, applicationName, applicationVersion, config);

        /************************************************************************
         * Setup requests parameters and invoke parallel processing. Of course
         * in real world application, there will be much more than a couple of
         * requests to process.
         ***********************************************************************/
         ListInboundShipmentsRequest requestOne = new ListInboundShipmentsRequest();
         // @TODO: set request parameters here
         requestOne.setSellerId(sellerId);
         requestOne.setMarketplace(marketplaceId);


         ListInboundShipmentsRequest requestTwo = new ListInboundShipmentsRequest();
         // @TODO: set second request parameters here
         requestTwo.setSellerId(sellerId);
         requestTwo.setMarketplace(marketplaceId);


         List<ListInboundShipmentsRequest> requests = new ArrayList<ListInboundShipmentsRequest>();
         requests.add(requestOne);
         requests.add(requestTwo);

         invokeListInboundShipments(service, requests);
    }


                                        
    /**
     * List Inbound Shipments request sample
     * Get the first set of inbound shipments created by a Seller according to
     * the specified ShipmentStatus. A NextToken is also returned to further
     * iterate through the Seller's remaining shipments. The ShipmentStatus must be
     * set, only shipments in the specified status will be returned. There is a special
     * token value to indicate end-of-data.  
     * @param service instance of FBAInboundServiceMWS service
     * @param requests list of requests to process
     */
    public static void invokeListInboundShipments(FBAInboundServiceMWSAsync service, List<ListInboundShipmentsRequest> requests) {
        List<Future<ListInboundShipmentsResponse>> responses = new ArrayList<Future<ListInboundShipmentsResponse>>();
        for (ListInboundShipmentsRequest request : requests) {
            responses.add(service.listInboundShipmentsAsync(request));
        }
        for (Future<ListInboundShipmentsResponse> future : responses) {
            while (!future.isDone()) {
                Thread.yield();
            }
            try {
                ListInboundShipmentsResponse response = future.get();
                // Original request corresponding to this response, if needed:
                ListInboundShipmentsRequest originalRequest = requests.get(responses.indexOf(future));
                System.out.println("Response request id: " + response.getResponseMetadata().getRequestId());
            } catch (Exception e) {
                if (e.getCause() instanceof FBAInboundServiceMWSException) {
                    FBAInboundServiceMWSException exception = FBAInboundServiceMWSException.class.cast(e.getCause());
                    System.out.println("Caught Exception: " + exception.getMessage());
                    System.out.println("Response Status Code: " + exception.getStatusCode());
                    System.out.println("Error Code: " + exception.getErrorCode());
                    System.out.println("Error Type: " + exception.getErrorType());
                    System.out.println("Request ID: " + exception.getRequestId());
                    System.out.print("XML: " + exception.getXML());
                } else {
                    e.printStackTrace();
                }
            }
        }
    }
            
}
