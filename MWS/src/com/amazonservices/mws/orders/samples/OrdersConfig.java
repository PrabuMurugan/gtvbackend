/******************************************************************************* 
 *  Copyright 2008-2012 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 * 
 *  Marketplace Web Service Orders Java Library
 *  API Version: 2011-01-01
 * 
 */





package com.amazonservices.mws.orders.samples;

import java.util.List;
import java.util.ArrayList;
import com.amazonservices.mws.orders.*;
import com.amazonservices.mws.orders.model.*;
import java.util.Arrays;

/**
 * Configuration used in  Marketplace Web Service Orders  samples
 */
final public class OrdersConfig {
	/************************************************************************
	 * Set Access Key ID, Secret Acess Key ID, Seller ID, etc.
	 ***********************************************************************/
	public static final String accessKeyId = "AKIAJ2UHP6UY7UMOG5KQ";
	public static final String secretAccessKey = "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";
	public static final String applicationName = "<Application Name>";
	public static final String applicationVersion = "<Application Version>";

	public static final String sellerId = "A1E3GOW3768RBB";
	public static final String marketplaceId = "<Your Marketplace Id>";
    public static   MarketplaceIdList marketplaceIdList = null;

	public static MarketplaceWebServiceOrdersConfig config = new MarketplaceWebServiceOrdersConfig();

	static {

	    //
	    // IMPORTANT: Uncomment out the appropriate line for the country you wish 
	    // to sell in:
	    // 
             marketplaceIdList = new MarketplaceIdList(Arrays.asList(
        		"ATVPDKIKX0DER" ));
	    // United States:
	     config.setServiceURL("https://mws.amazonservices.com/Orders/2011-01-01");
	    //
	    // Canada:
	    // config.setServiceURL("https://mws.amazonservices.ca/Orders/2011-01-01");
	    //
	    // Europe:
	    // config.setServiceURL("https://mws-eu.amazonservices.com/Orders/2011-01-01");
	    //
	    // Japan:
	    // config.setServiceURL("https://mws.amazonservices.jp/Orders/2011-01-01");
	    //
	    // China:
	    // config.setServiceURL("https://mws.amazonservices.com.cn/Orders/2011-01-01");
	    //
		/************************************************************************
		 * You can also try advanced configuration options. Available options are:
		 * 
		 * - Signature Version - Proxy Host and Proxy Port - User Agent String to be
		 * sent to Marketplace Web Service
		 *************************************************************************/
	}

}