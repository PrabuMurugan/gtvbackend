/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tajplaza.upload;

/**
 *
 * @author prabu
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Raj
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
 

public class CreateFeedFile {
    public static void main(String [] args) throws  Exception   {
         CreateFeedFile c=new CreateFeedFile();
       // c.createFeedFileFromQuery("176-a");
            c.FEED_FILE_NAME="inventoryloader_feed_newproducts-AMAZON.txt";
            c.createFeedFileFromQuery("184-b");        
    }
   
     
    private BufferedReader br = null;
    private boolean useQueryHeader = true;
    private LinkedList <String> fileheaders = null;
    private LinkedList <String> queryheaders = null;
    private String query = null;
    public CreateFeedFile(){
        
    }
   public CreateFeedFile(String query_sno) {
        try {
            this.br = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\sql\\web_sql.txt")));
            
            String line = "";
            while((line = br.readLine()) != null) {
            System.out.println("Line:"+line);
                String strQuerySerialNumber = line.split("\t")[0];
               // System.out.println("Query Serial Number : " + strQuerySerialNumber);
                
                if(strQuerySerialNumber != null && strQuerySerialNumber.equalsIgnoreCase(query_sno)) {
                
                    System.out.println("Date:"+ new Date() + "Found query number 58. No checking weather query line contains headers to use?");
                    
                    // Each line have seven field separaed by tab where seventh header is optional
                    if(line.split("\t").length == 7) {
                    
                        System.out.println("Date:"+ new Date() + "Hears are specified. Using that headers");
                        this.useQueryHeader = false;
                        
                        this.fileheaders = new LinkedList <String>();
                        String strHeaders = line.split("\t")[6];
                        System.out.println("Date:"+ new Date() + "Headers : " + strHeaders);
                        StringTokenizer stringTokenizer = new StringTokenizer(strHeaders, ",");
                        while(stringTokenizer.hasMoreTokens()) {
                            fileheaders.add(stringTokenizer.nextToken().replace("\"", ""));
                        }
                        
                        this.query = line.split("\t")[5].replace("\"", "");
                        System.out.println("Date:"+ new Date() + "Query : " + this.query);
                    }
                    break;
                }
            }
        } catch(Exception exception) {
            exception.printStackTrace();
        } finally {
            if(br != null) try {
                br.close();
            } catch (IOException ex) {
                System.out.println("Date:"+ new Date() + "Error while closing sql file.");
            }
        }
    }

   public boolean isUseQueryHeader() {
        return useQueryHeader;
    }

    public void setUseQueryHeader(boolean useQueryHeader) {
        this.useQueryHeader = useQueryHeader;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public LinkedList<String> getFileheaders() {
        return fileheaders;
    }

    public void setFileheaders(LinkedList<String> fileheaders) {
        this.fileheaders = fileheaders;
    }

    public LinkedList<String> getQueryheaders() {
        return queryheaders;
    }

    public void setQueryheaders(LinkedList<String> queryheaders) {
        this.queryheaders = queryheaders;
    }
    
    public ArrayList<String> getRecords() throws Exception{
        ArrayList<String> list = new ArrayList<String>();
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        System.out.println("Date:"+ new Date() + "Getting Database Connection.");
        util.DBMWS.CONNECT_TO_PROD_DEBUG=true;
        connection = util.DBMWS.getConnection("tajplaza");
        System.out.println("Date:"+ new Date() + "Got database connection. connection : " + connection + "\t" + this.query);
        
        if(connection != null) {
        
            ps = connection.prepareStatement(this.query);
            rs = ps.executeQuery();
            int iNumberofColumns = rs.getMetaData().getColumnCount();
            queryheaders = new LinkedList<String>();
            for(int i=1;i<=iNumberofColumns;i++) {
                queryheaders.add(rs.getMetaData().getColumnLabel(i));
            }
            
            System.out.println("Date:"+ new Date() + "Number of columns in the query. iNumberofColumns : " + iNumberofColumns);
            while(rs.next()) {
            
                StringBuilder sb = new StringBuilder("\n");
                for(int i=1;i<=iNumberofColumns;i++) {
                    if(i>1)
                        sb.append("\t");
                    sb.append(  rs.getString(i) );
                }
               //sb.append("\n" );
                
                list.add(new String(sb));
            }
        } else {
            System.out.println("Date:"+ new Date() + "Database connection is null.");
        }
        
        if(rs != null) rs.close();
        if(ps != null && !ps.isClosed()) ps.close();
        if(connection != null && !connection.isClosed()) connection.close();
        
        return list;
    }
    public static String FEED_FILE_NAME="inventoryloader_feed.txt";
    public void writeToFeedFile(String content) throws Exception { 
        FileWriter fstream_out0;
        BufferedWriter out0;
        fstream_out0 = new FileWriter("C:\\Google Drive\\Dropbox\\tmp\\"+FEED_FILE_NAME);
        out0 = new BufferedWriter(fstream_out0);
        out0.write(content);
        out0.newLine();
        out0.close();
    }
    

      public   void createFeedFileFromQuery(String query) throws Exception{
           System.out.println("Date:"+ new Date() + "Inside main of SendLatestOrderInfo");
         CreateFeedFile sendLatestOrderInfo = new CreateFeedFile(query);
         if(sendLatestOrderInfo.getQuery() == null) {
            System.out.println("No Query is found. Existing");
            System.out.println("Exception,Leaving main of SendLatestOrderInfo");
            System.exit(0);
         } 
           
         ArrayList <String>list = sendLatestOrderInfo.getRecords();
         boolean firstHeader=true;
         if(list.size() > 0) {
              System.out.println("Date:"+ new Date() + "Number of recods fetched : " + list.size());
              StringBuilder sb = new StringBuilder("");
              LinkedList<String> finalHeader = null;
              if(sendLatestOrderInfo.isUseQueryHeader()) {
                  finalHeader = sendLatestOrderInfo.getQueryheaders();
              } else {
                  finalHeader = sendLatestOrderInfo.getFileheaders();
              }
              for(String s:finalHeader) {
                    if(firstHeader==false)
                        sb.append("\t");
                    firstHeader=false;
                  sb.append(s);
                    
                  
              }
              for(String s:list){
                  sb.append(s);
              }
              sb.append("");
               writeToFeedFile(new String(sb));
              
         } else {
            System.out.println("Date:"+ new Date() + "No records fetched.");
         } 
    }
}

