/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tajplaza.upload;

import com.amazonaws.mws.MarketplaceWebService;
import com.amazonaws.mws.MarketplaceWebServiceClient;
import com.amazonaws.mws.MarketplaceWebServiceConfig;
import com.amazonaws.mws.MarketplaceWebServiceException;
import com.amazonaws.mws.model.SubmitFeedRequest;
import com.amazonaws.mws.model.SubmitFeedResponse;
import java.util.GregorianCalendar;

import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.http.HttpStatus; 
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonservices.mws.orders.MarketplaceWebServiceOrders;
import com.amazonservices.mws.orders.MarketplaceWebServiceOrdersClient;
import com.amazonservices.mws.orders.MarketplaceWebServiceOrdersException;
import com.amazonservices.mws.orders.model.FulfillmentChannelEnum;
import com.amazonservices.mws.orders.model.FulfillmentChannelList;
import com.amazonservices.mws.orders.model.GetOrderRequest;
import com.amazonservices.mws.orders.model.ListOrderItemsRequest;
import com.amazonservices.mws.orders.model.ListOrdersByNextTokenRequest;
import com.amazonservices.mws.orders.model.ListOrdersByNextTokenResponse;
import com.amazonservices.mws.orders.model.ListOrdersByNextTokenResult;
import com.amazonservices.mws.orders.model.ListOrdersRequest;
import com.amazonservices.mws.orders.model.ListOrdersResponse;
import com.amazonservices.mws.orders.model.ListOrdersResult;
import com.amazonservices.mws.orders.model.Order;
import com.amazonservices.mws.orders.model.OrderList;
import com.amazonservices.mws.orders.model.OrderStatusEnum;
import com.amazonservices.mws.orders.model.OrderStatusList;
import com.tajplaza.orders.ListOrderItemsSample;
import com.amazonservices.mws.orders.samples.OrdersConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import mail.SendMail;
import util.DBMWS;
 
/**
 *
 * @author prabu
 */
public class UploadInventoryLoaderFeedFile {
    private static FileInputStream fileInputStream;
public static void main(String[] args) throws  Exception {
    String QUERY_SNO=System.getProperty("QUERY_SNO","176-e");
    
 
    CreateFeedFile c=new CreateFeedFile();
    c.createFeedFileFromQuery(QUERY_SNO);
     System.out.println("Inside NEW UploadInventoryLoaderFeedFile :QUERY_SNO:"  +QUERY_SNO );
    File file = new File("C:\\Google Drive\\Dropbox\\tmp\\"+CreateFeedFile.FEED_FILE_NAME);
   try{ 
       uploadFileIntoAmazon(file);
    }catch (Exception e1){
        System.out.println("UploadInventoryLoaderFeedFile : Minor error:"+e1.getMessage());
        try{
            Thread.sleep(40*1000);
            uploadFileIntoAmazon(file);
        }catch (Exception e2){
            throw e2;
        }
        
   }
   //SendEmailNewMWSNew.sendEmailNew("Attaching inventory feed uploaded","abc","mprabagar80@gmail.com,vi_himanshu@varuninfotech.com","C:\\Google Drive\\Dropbox\\tmp\\"+CreateFeedFile.FEED_FILE_NAME);
 
}
public static void uploadFileIntoAmazon(File file) throws Exception{
    System.out.println("Uploading  feed file:"+CreateFeedFile.FEED_FILE_NAME);
    FileInputStream fis = new FileInputStream(file);
    
MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();
config.setServiceURL("https://mws.amazonservices.com");
    MarketplaceWebService mktplace = new MarketplaceWebServiceClient(
                OrdersConfig.accessKeyId,
                OrdersConfig.secretAccessKey,
                OrdersConfig.applicationName,
                OrdersConfig.applicationVersion ,config);;

    System.out.println("market se connecting!");
    String marketplace = "ATVPDKIKX0DER";
    String merchant = OrdersConfig.sellerId;
    String contentMD5 = computeContentMD5HeaderValue(fis);
    String feedType = "_POST_FLAT_FILE_INVLOADER_DATA_";
    boolean purgeAndReplace = false;

    SubmitFeedRequest req = new SubmitFeedRequest(marketplace, merchant, fis, contentMD5, feedType, purgeAndReplace);
    SubmitFeedResponse response = mktplace.submitFeed(req);
       
        System.out.println("submit feed response!");
        
}
public static String computeContentMD5HeaderValue(FileInputStream fis)
        throws Exception {
    DigestInputStream dis = new DigestInputStream(fis,
            MessageDigest.getInstance("MD5"));
    byte[] buffer = new byte[8192];
    while (dis.read(buffer) > 0)
        ;
    String md5Content = new String(
            org.apache.commons.codec.binary.Base64.encodeBase64(dis.getMessageDigest().digest())
            );
    // Effectively resets the stream to be beginning of the file via a
    // FileChannel.
    fis.getChannel().position(0);
    return md5Content;
}


}
