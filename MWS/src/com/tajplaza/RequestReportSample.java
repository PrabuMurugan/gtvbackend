/******************************************************************************* 
 *  Copyright 2009 Amazon Services.
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 *
 *  Marketplace Web Service Java Library
 *  API Version: 2009-01-01
 *  Generated: Wed Feb 18 13:28:48 PST 2009 
 * 
 */



package com.tajplaza;

import java.util.Arrays;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.amazonaws.mws.*;
import com.amazonaws.mws.model.*;
import com.amazonaws.mws.mock.MarketplaceWebServiceMock;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * Request Report  Samples
 *
 *
 */
public class RequestReportSample {
          public static HashMap<String,String> downloadMap=new HashMap ();
 
            
    /**
     * Just add a few required parameters, and try the service
     * Request Report functionality
     *
     * @param args unused
     */
          
     public static void setReportTypes(){
         downloadMap.put("_GET_MERCHANT_LISTINGS_DATA_", "C:\\Google Drive\\Dropbox\\existing-all-inventory.txt");
         downloadMap.put("_GET_MERCHANT_CANCELLED_LISTINGS_DATA_", "C:\\Google Drive\\Dropbox\\cancelled-listings.txt");
         downloadMap.put("_GET_MERCHANT_LISTINGS_DEFECT_DATA_", "C:\\Google Drive\\Dropbox\\defect-listings.txt");
         
            downloadMap.put("_GET_FBA_FULFILLMENT_INVENTORY_HEALTH_DATA_", "C:\\Google Drive\\Dropbox\\fba\\inventory-health.txt");
            downloadMap.put("_GET_FBA_MYI_UNSUPPRESSED_INVENTORY_DATA_", "C:\\Google Drive\\Dropbox\\existing-inventory-FBA.txt");
            downloadMap.put("_GET_FBA_FULFILLMENT_INVENTORY_RECEIPTS_DATA_", "C:\\Google Drive\\Dropbox\\fba\\received-inventory.txt");         
            downloadMap.put("_GET_FBA_FULFILLMENT_INVENTORY_ADJUSTMENTS_DATA_", "C:\\Google Drive\\Dropbox\\fba\\inventory-adjustments.txt");
            downloadMap.put("_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_", "C:\\Google Drive\\Dropbox\\fba\\fullfilled-shipment.txt");
            
          
            
       Date date = Calendar.getInstance().getTime();
        DateFormat formatter = new SimpleDateFormat("E");
        String today = formatter.format(date);
        
       if( today.indexOf("Sat")>=0)
       {
      //  if( today.indexOf("Sun")>=0){
            downloadMap.put("_GET_V2_SETTLEMENT_REPORT_DATA_", "C:\\Google Drive\\Dropbox\\fba\\settlement-data.txt");
            downloadMap.put("_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_", "C:\\Google Drive\\Dropbox\\fba\\fba-fees.txt");
           
            downloadMap.put("_GET_FBA_FULFILLMENT_CUSTOMER_RETURNS_DATA_", "C:\\Google Drive\\Dropbox\\fba\\customer-returns.txt");
            downloadMap.put("_GET_FBA_MYI_ALL_INVENTORY_DATA_", "C:\\Google Drive\\Dropbox\\fba\\archived.txt");
        }            
     }
    public static void main(String... args) {
        setReportTypes();
        /************************************************************************
         * Access Key ID and Secret Access Key ID, obtained from:
         * http://aws.amazon.com
         ***********************************************************************/
      final String accessKeyId = "AKIAJ2UHP6UY7UMOG5KQ";
        final String secretAccessKey = "OfiBjaTZFUrV3uSnl9p9skjzFvyU/e7vWFdpCjJP";

        final String appName = "<Your Application or Company Name>";
        final String appVersion = "<Your Application Version or Build Number or Release Date>";

        MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();

        /************************************************************************
         * Uncomment to set the appropriate MWS endpoint.
         ************************************************************************/
        // US
          config.setServiceURL("https://mws.amazonservices.com");
        // UK
        // config.setServiceURL("https://mws.amazonservices.co.uk");
        // Germany
        // config.setServiceURL("https://mws.amazonservices.de");
        // France
        // config.setServiceURL("https://mws.amazonservices.fr");
        // Italy
        // config.setServiceURL("https://mws.amazonservices.it");
        // Japan
        // config.setServiceURL("https://mws.amazonservices.jp");
        // China
        // config.setServiceURL("https://mws.amazonservices.com.cn");
        // Canada
        // config.setServiceURL("https://mws.amazonservices.ca");
        // India
        // config.setServiceURL("https://mws.amazonservices.in");

        /************************************************************************
         * You can also try advanced configuration options. Available options are:
         *
         *  - Signature Version
         *  - Proxy Host and Proxy Port
         *  - User Agent String to be sent to Marketplace Web Service
         *
         ***********************************************************************/

        /************************************************************************
         * Instantiate Http Client Implementation of Marketplace Web Service        
         ***********************************************************************/
        
        MarketplaceWebService service = new MarketplaceWebServiceClient(
                    accessKeyId, secretAccessKey, appName, appVersion, config);
 
        /************************************************************************
         * Uncomment to try out Mock Service that simulates Marketplace Web Service 
         * responses without calling Marketplace Web Service  service.
         *
         * Responses are loaded from local XML files. You can tweak XML files to
         * experiment with various outputs during development
         *
         * XML files available under com/amazonaws/mws/mock tree
         *
         ***********************************************************************/
        // MarketplaceWebService service = new MarketplaceWebServiceMock();

        /************************************************************************
         * Setup request parameters and uncomment invoke to try out 
         * sample for Request Report 
         ***********************************************************************/

        /************************************************************************
         * Marketplace and Merchant IDs are required parameters for all 
         * Marketplace Web Service calls.
         ***********************************************************************/
        final String merchantId = "A1E3GOW3768RBB";
        // marketplaces from which data should be included in the report; look at the
        // API reference document on the MWS website to see which marketplaces are
        // included if you do not specify the list yourself
        final IdList marketplaces = new IdList(Arrays.asList(
        		"ATVPDKIKX0DER" ));
        
        
        Iterator reportIterator=downloadMap.keySet().iterator();
        while(reportIterator.hasNext()){
            String reportType=(String) reportIterator.next();
            
            RequestReportRequest request = new RequestReportRequest()
		        .withMerchant(merchantId)
		        .withMarketplaceIdList(marketplaces)
		        .withReportType(reportType)
		        .withReportOptions("ShowSalesChannel=true");

        // demonstrates how to set the date range
		DatatypeFactory df = null;
		try {
			df = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		XMLGregorianCalendar startDate = df
				.newXMLGregorianCalendar(new GregorianCalendar(2011, 1, 1));
                
                if(reportType.equalsIgnoreCase("_GET_AMAZON_FULFILLED_SHIPMENTS_DATA_")) {
                    
                    GregorianCalendar gc = new GregorianCalendar();
                    Date date = new Date();
                   
                    gc.setTimeInMillis(new Date().getTime());
                    gc.add(GregorianCalendar.DATE, -3);
                    startDate = df.newXMLGregorianCalendar(gc);
                }
		request.setStartDate(startDate);
		
	    // @TODO: set additional request parameters here
		
		invokeRequestReport(service, request);            
            
            
        }
        
        
    }


                                                        
    /**
     * Request Report  request sample
     * requests the generation of a report
     *   
     * @param service instance of MarketplaceWebService service
     * @param request Action to invoke
     */
    public static void invokeRequestReport(MarketplaceWebService service, RequestReportRequest request) {
        try {
            
            RequestReportResponse response = service.requestReport(request);

            
            System.out.println ("RequestReport Action Response");
            System.out.println ("=============================================================================");
            System.out.println ();

            System.out.print("    RequestReportResponse");
            System.out.println();
            if (response.isSetRequestReportResult()) {
                System.out.print("        RequestReportResult");
                System.out.println();
                RequestReportResult  requestReportResult = response.getRequestReportResult();
                if (requestReportResult.isSetReportRequestInfo()) {
                    System.out.print("            ReportRequestInfo");
                    System.out.println();
                    ReportRequestInfo  reportRequestInfo = requestReportResult.getReportRequestInfo();
                    if (reportRequestInfo.isSetReportRequestId()) {
                        System.out.print("                ReportRequestId");
                        System.out.println();
                        System.out.print("                    " + reportRequestInfo.getReportRequestId());
                        System.out.println();
                    }
                    if (reportRequestInfo.isSetReportType()) {
                        System.out.print("                ReportType");
                        System.out.println();
                        System.out.print("                    " + reportRequestInfo.getReportType());
                        System.out.println();
                    }
                    if (reportRequestInfo.isSetStartDate()) {
                        System.out.print("                StartDate");
                        System.out.println();
                        System.out.print("                    " + reportRequestInfo.getStartDate());
                        System.out.println();
                    }
                    if (reportRequestInfo.isSetEndDate()) {
                        System.out.print("                EndDate");
                        System.out.println();
                        System.out.print("                    " + reportRequestInfo.getEndDate());
                        System.out.println();
                    }
                    if (reportRequestInfo.isSetSubmittedDate()) {
                        System.out.print("                SubmittedDate");
                        System.out.println();
                        System.out.print("                    " + reportRequestInfo.getSubmittedDate());
                        System.out.println();
                    }
                    if (reportRequestInfo.isSetReportProcessingStatus()) {
                        System.out.print("                ReportProcessingStatus");
                        System.out.println();
                        System.out.print("                    " + reportRequestInfo.getReportProcessingStatus());
                        System.out.println();
                    }
                } 
            } 
            if (response.isSetResponseMetadata()) {
                System.out.print("        ResponseMetadata");
                System.out.println();
                ResponseMetadata  responseMetadata = response.getResponseMetadata();
                if (responseMetadata.isSetRequestId()) {
                    System.out.print("            RequestId");
                    System.out.println();
                    System.out.print("                " + responseMetadata.getRequestId());
                    System.out.println();
                }
            } 
            System.out.println();
            System.out.println(response.getResponseHeaderMetadata());
            System.out.println();

           
        } catch (MarketplaceWebServiceException ex) {
            ex.printStackTrace();
            System.out.println("Caught Exception: " + ex.getMessage());
            System.out.println("Response Status Code: " + ex.getStatusCode());
            System.out.println("Error Code: " + ex.getErrorCode());
            System.out.println("Error Type: " + ex.getErrorType());
            System.out.println("Request ID: " + ex.getRequestId());
            System.out.print("XML: " + ex.getXML());
            System.out.println("ResponseHeaderMetadata: " + ex.getResponseHeaderMetadata());
        }
    }
                                                
}
