/******************************************************************************* 
 *  Copyright 2009 Amazon Services. All Rights Reserved.
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 * 
 *  FBA Inbound Service MWS  Java Library
 *  API Version: 2010-10-01
 *  Generated: Fri Oct 22 09:48:04 UTC 2010 
 */

package com.tajplaza.inbound;

import java.util.List;
import java.util.ArrayList;
import com.amazonservices.mws.FulfillmentInboundShipment._2010_10_01.*;
import com.amazonservices.mws.FulfillmentInboundShipment._2010_10_01.model.*;
import com.amazonservices.mws.FulfillmentInboundShipment._2010_10_01.mock.FBAInboundServiceMWSMock;
import com.amazonservices.mws.orders.samples.OrdersConfig;
import com.tajplaza.orders.ListOrderItemsSample;
import com.tajplaza.orders.OrderFetcherSample;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.http.HttpStatus;
import util.DBMWS;

/**
 *
 * List Inbound Shipments  Samples
 *
 *
 */
public class ListInboundShipmentsSample {

    /**
     * Just add few required parameters, and try the service
     * List Inbound Shipments functionality
     *
     * @param args unused
     */
    public static void main(String... args) throws Exception {

        /************************************************************************
         * Access Key ID and Secret Acess Key ID, obtained from:
         * http://mws.amazon.com
         ***********************************************************************/
        final String accessKeyId = OrdersConfig.accessKeyId;
        final String secretAccessKey = OrdersConfig.secretAccessKey;

        /************************************************************************
         * Seller ID is required parameter for all MWS calls.
         ***********************************************************************/
        final String sellerId = OrdersConfig.sellerId;

        /************************************************************************
         * The application name and version are included in each MWS call's
         * HTTP User-Agent field. These are required fields.
         ***********************************************************************/
        final String applicationName = "<Your Application Name>";
        final String applicationVersion = "<Your Application Version or Build Number or Release Date>";

        /************************************************************************
         * Uncomment to try advanced configuration options. Available options are:
         *
         *  - Proxy Host and Proxy Port
         *  - Service URL
         *
         ***********************************************************************/
        FBAInboundServiceMWSConfig config = new FBAInboundServiceMWSConfig();

        /************************************************************************
         * Uncomment to set the correct MWS endpoint. You can get your Country
         * Code from MWSEndpoint enum class, e.g. US.
         ************************************************************************/
        config.setServiceURL(MWSEndpoint.US.toString());

        /************************************************************************
         * Instantiate Http Client Implementation of FBA Inbound Service MWS 
         ***********************************************************************/
        // FBAInboundServiceMWS service = new FBAInboundServiceMWSClient(accessKeyId, secretAccessKey, config);
        FBAInboundServiceMWS service = new FBAInboundServiceMWSClient(accessKeyId, secretAccessKey, applicationName, applicationVersion, config);
 
        /************************************************************************
         * Uncomment to try out Mock Service that simulates FBA Inbound Service MWS 
         * responses without calling FBA Inbound Service MWS  service.
         *
         * Responses are loaded from local XML files. You can tweak XML files to
         * experiment with various outputs during development
         *
         * XML files available under com/amazonservices/mws/FulfillmentInboundShipment/_2010_10_01/mock tree
         *
         ***********************************************************************/
        // FBAInboundServiceMWS service = new FBAInboundServiceMWSMock();

        /************************************************************************
         * Setup request parameters and uncomment invoke to try out 
         * sample for List Inbound Shipments 
         ***********************************************************************/
           GregorianCalendar cafter=null;
        
       


         ListInboundShipmentsRequest request = new ListInboundShipmentsRequest();
            try {
         
        
         
         Date dt = new Date();
        //dt.setYear(dt.getYear());
        //dt.setMonth(9);
        dt.setDate(dt.getDate());
          GregorianCalendar cbefore = new GregorianCalendar();
        cbefore.setTime(dt);
         XMLGregorianCalendar dateafter=null ;
        try {
            XMLGregorianCalendar datebefore = DatatypeFactory.newInstance().newXMLGregorianCalendar(cbefore);
         request.setLastUpdatedBefore(datebefore);
          
        } catch (DatatypeConfigurationException ex) {
           ex.printStackTrace();
        
        }             
             
        } catch ( Exception e) {            
             e.printStackTrace();
         }         
              try{
                  DBMWS.CONNECT_TO_PROD_DEBUG=true;
                java.sql.Connection con =DBMWS.getConnection("tajplaza");
                PreparedStatement pst = null;
                ResultSet rs = null;

                  String sql="select max(last_update_date) from fba_inbound_shipments ";
                 PreparedStatement  prest = con.prepareStatement(sql);
                 ResultSet rs0=prest.executeQuery();
                if(rs0.next()){
                       Date dt=new Date();
                       dt.setYear(dt.getYear()-1);
                       dt.setMonth(1);
                       dt.setDate(20);
                    if(rs0.getDate(1)!=null)
                        dt=rs0.getDate(1);
                   dt.setDate(dt.getDate()-90);
                   System.out.println("Querying shipments from date :"+dt);
                   cafter = new GregorianCalendar();
                   cafter.setTime(dt);
                   
                    XMLGregorianCalendar dateafter = DatatypeFactory.newInstance().newXMLGregorianCalendar(cafter);
                    request.setLastUpdatedAfter(dateafter);
                    

                }
                con.close();
            }catch (Exception e){
                e.printStackTrace();
            }
              
         // @TODO: set request parameters here
         request.setSellerId(sellerId);
         ShipmentStatusList s=new ShipmentStatusList();
         ArrayList a=new ArrayList();
         a.add("WORKING");
         a.add("SHIPPED");
         a.add("IN_TRANSIT");
         a.add("DELIVERED");
         a.add("CHECKED_IN");
         a.add("RECEIVING");
         a.add("CLOSED");
         s.setMember(a);
         
         System.out.println("Querying shipments between :"+request.getLastUpdatedAfter() +" and "+request.getLastUpdatedBefore());
         request.setShipmentStatusList(s);
     
        
           invokeListInboundShipments(service, request);

    }


                            
    /**
     * List Inbound Shipments  request sample
     * Get the first set of inbound shipments created by a Seller according to
     * the specified shipment status or the specified shipment Id. A NextToken
     * is also returned to further iterate through the Seller's remaining
     * shipments. If a NextToken is not returned, it indicates the
     * end-of-data.
     * At least one of ShipmentStatusList and ShipmentIdList must be passed in.
     * if both are passed in, then only shipments that match the specified
     * shipment Id and specified shipment status will be returned.
     * the LastUpdatedBefore and LastUpdatedAfter are optional, they are used
     * to filter results based on last update time of the shipment.  
     * @param service instance of FBAInboundServiceMWS service
     * @param request Action to invoke
     */
    
    
private static void insertOrderIntoDb(InboundShipmentInfo m,FBAInboundServiceMWS service, ListInboundShipmentsRequest request) throws Exception {
    DBMWS.CONNECT_TO_PROD_DEBUG=true;
    java.sql.Connection con = DBMWS.getConnection("tajplaza");
    PreparedStatement pst = null;
    ResultSet rs = null;
 

    try {
        
        
        
           String sql="delete from fba_inbound_shipments where ShipmentId=? ";
           
           
             PreparedStatement prest = con.prepareStatement(sql);
             prest.setString(1, m.getShipmentId());
             prest.executeUpdate();
          
 
 
            sql = "INSERT INTO fba_inbound_shipments (ShipmentId ,ShipmentName ,DestinationFulfillmentCenterId ,ShipmentStatus ,LabelPrepType ) " + "VALUES (?,?,?,?,?)";    
          prest = con.prepareStatement(sql);
       prest.setString(1,m.getShipmentId());
       prest.setString(2,m.getShipmentName());
       prest.setString(3,m.getDestinationFulfillmentCenterId());
       prest.setString(4,m.getShipmentStatus());
       prest.setString(5,m.getLabelPrepType());
           
        prest.executeUpdate();
        con.close();

        ListInboundShipmentItemsSample ls=new ListInboundShipmentItemsSample();
        ListInboundShipmentItemsRequest request2=new ListInboundShipmentItemsRequest();
        request2.setSellerId(request.getSellerId());
        request2.setMarketplace(request.getMarketplace());
        request2.setShipmentId(m.getShipmentId());
        ls.invokeListInboundShipmentItems(service, request2);

        
    }

    catch ( Exception ex) {
    ex.printStackTrace();
    throw ex;

} 
}

    public static void invokeListInboundShipments(FBAInboundServiceMWS service, ListInboundShipmentsRequest request) throws Exception {
        try {
            
             
            ListInboundShipmentsResponse response = service.listInboundShipments(request);

            
            System.out.println ("ListInboundShipments Action Response");
            System.out.println ("=============================================================================");
            System.out.println ();

            System.out.println("    ListInboundShipmentsResponse");
            System.out.println();
            if (response.isSetListInboundShipmentsResult()) {
                System.out.println("        ListInboundShipmentsResult");
                System.out.println();
                ListInboundShipmentsResult  listInboundShipmentsResult = response.getListInboundShipmentsResult();
                if (listInboundShipmentsResult.isSetShipmentData()) {
                    System.out.println("            ShipmentData");
                    System.out.println();
                    InboundShipmentList  shipmentData = listInboundShipmentsResult.getShipmentData();
                    java.util.List<InboundShipmentInfo> memberList = shipmentData.getMember();
                    for (InboundShipmentInfo member : memberList) {
                        System.out.println("Inserting shipment ID"+member.getShipmentId() +", shipment name:"+member.getShipmentName());
                        insertOrderIntoDb(member,service,request);
                     
                    }
                } 
                if (listInboundShipmentsResult.isSetNextToken()) {
                    System.out.println("            NextToken");
                    System.out.println();
                    System.out.println("                " + listInboundShipmentsResult.getNextToken());
                    System.out.println();
                    
                ListInboundShipmentsByNextTokenRequest listInboundShipmentsByNextTokenRequest = new ListInboundShipmentsByNextTokenRequest();
                listInboundShipmentsByNextTokenRequest
                        .setSellerId(OrdersConfig.sellerId);
                String nextToken = listInboundShipmentsResult.getNextToken();
                ListInboundShipmentsByNextTokenResult listInboundShipmentsByNextTokenResult = null;
                while (nextToken != null) {
                    listInboundShipmentsByNextTokenRequest.setNextToken(nextToken);
                    listInboundShipmentsByNextTokenResult = listInboundShipmentsByNextToken(service,listInboundShipmentsByNextTokenRequest,request);
                    nextToken = listInboundShipmentsByNextTokenResult.getNextToken();
                }
                
                }
            } 
            if (response.isSetResponseMetadata()) {
                System.out.println("        ResponseMetadata");
                System.out.println();
                ResponseMetadata  responseMetadata = response.getResponseMetadata();
                if (responseMetadata.isSetRequestId()) {
                    System.out.println("            RequestId");
                    System.out.println();
                    System.out.println("                " + responseMetadata.getRequestId());
                    System.out.println();
                }
            } 
            System.out.println();

           
        } catch (FBAInboundServiceMWSException ex) {
            
            System.out.println("Caught Exception: " + ex.getMessage());
            System.out.println("Response Status Code: " + ex.getStatusCode());
            System.out.println("Error Code: " + ex.getErrorCode());
            System.out.println("Error Type: " + ex.getErrorType());
            System.out.println("Request ID: " + ex.getRequestId());
            System.out.print("XML: " + ex.getXML());
        }
    }
    
    
static ListInboundShipmentsByNextTokenResult listInboundShipmentsByNextToken(FBAInboundServiceMWS service,
            ListInboundShipmentsByNextTokenRequest listOrdersByNextTokenRequest,ListInboundShipmentsRequest origRequest)
            throws FBAInboundServiceMWSException, Exception {
        boolean retry;
        ListInboundShipmentsByNextTokenResponse listInboundShipmentsByNextTokenResponse = null;
        ListInboundShipmentsByNextTokenResult listInboundShipmentsByNextTokenResult = null;
        do {
            retry = false;
            try {

                listInboundShipmentsByNextTokenResponse = service
                        .listInboundShipmentsByNextToken(listOrdersByNextTokenRequest);
            } catch (FBAInboundServiceMWSException ex) {
                if (ex.getStatusCode() == HttpStatus.SC_SERVICE_UNAVAILABLE
                        && "RequestThrottled".equals(ex.getErrorCode())) {
                    retry = true;
                    OrderFetcherSample.requestThrottledExceptionHandler(OrderFetcherSample.LIST_ORDERS_THROTTLE_LIMIT);
                } else {
                    throw ex;
                }
            }
            if (listInboundShipmentsByNextTokenResponse != null
                    && listInboundShipmentsByNextTokenResponse
                            .isSetListInboundShipmentsByNextTokenResult()) {
                listInboundShipmentsByNextTokenResult = listInboundShipmentsByNextTokenResponse
                        .getListInboundShipmentsByNextTokenResult();
                if (listInboundShipmentsByNextTokenResult.isSetShipmentData()) {
                                        System.out.println("            ShipmentData 222222");
                    System.out.println();
                    InboundShipmentList  shipmentData = listInboundShipmentsByNextTokenResult.getShipmentData();
                    java.util.List<InboundShipmentInfo> memberList = shipmentData.getMember();
                    for (InboundShipmentInfo member : memberList) {
                        System.out.println("Inserting  2 shipment ID"+member.getShipmentId() +", shipment name:"+member.getShipmentName());
                        insertOrderIntoDb(member,service,origRequest );
                        
                     
                    }
                }
            }

        } while (retry);
        return listInboundShipmentsByNextTokenResult;
    }    
                        
}
