/******************************************************************************* 
 *  Copyright 2008-2012 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 * 
 *  Marketplace Web Service Orders Java Library
 *  API Version: 2011-01-01
 * 
 */



package com.tajplaza.orders;

import java.util.List;
import java.util.ArrayList;
import com.amazonservices.mws.orders.*;
import com.amazonservices.mws.orders.model.*;
import com.amazonservices.mws.orders.mock.MarketplaceWebServiceOrdersMock;
import com.amazonservices.mws.orders.samples.OrdersConfig;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.http.HttpStatus;
import util.DBMWS;

/**
 *
 * List Order Items  Samples
 *
 *
 */
public class ListOrderItemsSample {

    /**
     * Just add few required parameters, and try the service
     * List Order Items functionality
     *
     * @param args unused
     */
    public static void main(String... args) {
        
        MarketplaceWebServiceOrders service = new MarketplaceWebServiceOrdersClient(
					OrdersConfig.accessKeyId, 
					OrdersConfig.secretAccessKey, 
					OrdersConfig.applicationName, 
					OrdersConfig.applicationVersion, 
					OrdersConfig.config);
 
        /************************************************************************
         * Uncomment to try out Mock Service that simulates Marketplace Web Service Orders 
         * responses without calling Marketplace Web Service Orders  service.
         *
         * Responses are loaded from local XML files. You can tweak XML files to
         * experiment with various outputs during development
         *
         * XML files available under com/amazonservices/mws/orders/mock tree
         *
         ***********************************************************************/
        // MarketplaceWebServiceOrders service = new MarketplaceWebServiceOrdersMock();

        /************************************************************************
         * Setup request parameters and uncomment invoke to try out 
         * sample for List Order Items 
         ***********************************************************************/
         ListOrderItemsRequest request = new ListOrderItemsRequest();
         request.setSellerId(OrdersConfig.sellerId);
        
         // @TODO: set request parameters here

        // invokeListOrderItems(service, request,"");

    }


                                    
    /**
     * List Order Items  request sample
     * This operation can be used to list the items of the order indicated by the
     * given order id (only a single Amazon order id is allowed).
     *   
     * @param service instance of MarketplaceWebServiceOrders service
     * @param request Action to invoke
     */
    public static void invokeListOrderItems(MarketplaceWebServiceOrders service, ListOrderItemsRequest request,Order o) throws Exception {
         boolean retry;
          ListOrderItemsResponse response=null;
          try {
          do{
              retry = false;
                try{
                response= service.listOrderItems(request);
                            } catch (MarketplaceWebServiceOrdersException ex) {
                if (ex.getStatusCode() == HttpStatus.SC_SERVICE_UNAVAILABLE
                        && "RequestThrottled".equals(ex.getErrorCode())) {
                    retry = true;
                    OrderFetcherSample.requestThrottledExceptionHandler(OrderFetcherSample.LIST_ORDERS_THROTTLE_LIMIT);
                } else {
                    throw ex;
                }
            }
           }while(retry);
            if ( response.isSetListOrderItemsResult()) {
                ListOrderItemsResult  listOrderItemsResult = response.getListOrderItemsResult();
                if (listOrderItemsResult.isSetOrderItems()) {
                    OrderItemList  orderItems = listOrderItemsResult.getOrderItems();
                    java.util.List<OrderItem> orderItemList = orderItems.getOrderItem();
                    for (OrderItem orderItem : orderItemList) {
                        insertOrderIntoDb(listOrderItemsResult.getAmazonOrderId(),orderItem,o);

                    }
                } 
            } 


            } catch (MarketplaceWebServiceOrdersException ex) {
                ex.printStackTrace();;
                throw ex;
            }
        
    }

private static void insertOrderIntoDb(String orderId,OrderItem orderItem, Order o) throws Exception {
    DBMWS.CONNECT_TO_PROD_DEBUG=true;
    java.sql.Connection con =DBMWS.getConnection("tajplaza");
    PreparedStatement pst = null;
    ResultSet rs = null;
 

    try {
        
        
        
           String sql="delete  from orders where order_id=? and OrderItemId=? ";
           
           
             PreparedStatement prest = con.prepareStatement(sql);
             prest.setString(1, orderId);
             prest.setString(2,  orderItem.getOrderItemId());
             prest.executeUpdate();
             
            /* ResultSet rs0=prest.executeQuery();
             if(rs0.next()){
                 System.out.println("Order id  :"+orderId+" already inserteted..return");
                 return;
             }*/
             
        Date lastUpDt=o.getLastUpdateDate().toGregorianCalendar().getTime();
        Timestamp sqlLastUpdateTime = new Timestamp(lastUpDt.getTime());
 
         Date purchaseDt=o.getPurchaseDate().toGregorianCalendar().getTime();
        Timestamp  sqlPurTime = new Timestamp(purchaseDt.getTime());
 
        Timestamp sqllatestShipTime=null;
        
         if(o.getLatestShipDate()!=null){
             Date latestShipDate=o.getLatestShipDate().toGregorianCalendar().getTime();
             sqllatestShipTime = new Timestamp(latestShipDate.getTime());
         }
         
        Timestamp sqlearliestShipTime=null;
        /*PRABU COMMENTED NOW BECAUSE THE BELOW ERROR AND I AM REALLY NOT USING THIS COLUMN
         * Order id: 111-3445904-9449813 order purchase:2013-10-27T14:07:51Z order channel:AFNorder channel:CANCELED
com.mysql.jdbc.MysqlDataTruncation: Data truncation: Incorrect datetime value: '1969-12-31 19:00:00' for column 'earliestShipDate' at row 1 

         * if(o.getEarliestShipDate()!=null ){
         Date earliestShipDate=o.getEarliestShipDate().toGregorianCalendar().getTime();
         sqlearliestShipTime= new Timestamp(earliestShipDate.getTime());
            
        }*/
        
        
            sql = "INSERT INTO orders (order_id ,fullfillment_channel ,ASIN ,SellerSKU ,OrderItemId ,Title ,QuantityOrdered ,QuantityShipped ,ItemPrice ,ShippingPrice,purchaseDate,orderStatus,shippingAddress,orderTotal,buyerName,orderType,earliestShipDate,latestShipDate,last_update_date ) " + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";    
          prest = con.prepareStatement(sql);
        prest.setString(1, orderId);
        prest.setString(2, o.getFulfillmentChannel().value());
        prest.setString(3, orderItem.getASIN());
        prest.setString(4, orderItem.getSellerSKU());
        prest.setString(5, orderItem.getOrderItemId());
        prest.setString(6, orderItem.getTitle());
        prest.setString(7, String.valueOf( orderItem.getQuantityOrdered()));
        prest.setString(8, String.valueOf( orderItem.getQuantityShipped()));
        if(orderItem.getItemPrice()!=null)
            prest.setString(9, orderItem.getItemPrice().getAmount());
        else
            prest.setString(9,"0");        
        
        if(orderItem.getShippingPrice()!=null)
            prest.setString(10, orderItem.getShippingPrice().getAmount());
        else
            prest.setString(10,"0");
        prest.setTimestamp(11, sqlPurTime);
        
        prest.setString(12,o.getOrderStatus().value());
        
        if(o.getShippingAddress()!=null)
            prest.setString(13,o.getShippingAddress().toXMLFragment());
        else
            prest.setString(13,null);
        
        if(o.getOrderTotal()!=null)
            prest.setString(14,o.getOrderTotal().getAmount());
        else
            prest.setString(14,null);

         prest.setString(15,o.getBuyerName());
         prest.setString(16,o.getOrderType());
         prest.setTimestamp(17,null);
         prest.setTimestamp(18,null);

        prest.setTimestamp(19, sqlLastUpdateTime);
        
        
        prest.executeUpdate();
        con.close();
    }

    catch ( Exception ex) {
    ex.printStackTrace();
    throw ex;

} 
}
 
            
}
