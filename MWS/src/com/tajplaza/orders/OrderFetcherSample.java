package com.tajplaza.orders;

import java.util.GregorianCalendar;

import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.http.HttpStatus; 
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonservices.mws.orders.MarketplaceWebServiceOrders;
import com.amazonservices.mws.orders.MarketplaceWebServiceOrdersClient;
import com.amazonservices.mws.orders.MarketplaceWebServiceOrdersException;
import com.amazonservices.mws.orders.model.FulfillmentChannelEnum;
import com.amazonservices.mws.orders.model.FulfillmentChannelList;
import com.amazonservices.mws.orders.model.GetOrderRequest;
import com.amazonservices.mws.orders.model.ListOrderItemsRequest;
import com.amazonservices.mws.orders.model.ListOrdersByNextTokenRequest;
import com.amazonservices.mws.orders.model.ListOrdersByNextTokenResponse;
import com.amazonservices.mws.orders.model.ListOrdersByNextTokenResult;
import com.amazonservices.mws.orders.model.ListOrdersRequest;
import com.amazonservices.mws.orders.model.ListOrdersResponse;
import com.amazonservices.mws.orders.model.ListOrdersResult;
import com.amazonservices.mws.orders.model.Order;
import com.amazonservices.mws.orders.model.OrderList;
import com.amazonservices.mws.orders.model.OrderStatusEnum;
import com.amazonservices.mws.orders.model.OrderStatusList;
 
import com.amazonservices.mws.orders.samples.OrdersConfig;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import util.DBMWS;

/**
 * Sample file that fetches orders created during a given time period.
 */

public class OrderFetcherSample {

    /*
     * Add required parameters in OrdersConfig.java before trying out this
     * sample.
     */
    public static final Log log = LogFactory.getLog(OrderFetcherSample.class);

    /*****************************************
     * Throttling Limits in Milliseconds
     *****************************************/
   public static final long LIST_ORDERS_THROTTLE_LIMIT = 600000L; // 1 call/10 mins
 

    protected MarketplaceWebServiceOrders service;
    static FulfillmentChannelList f1;
    static List<FulfillmentChannelEnum> fc1;
   

    public OrderFetcherSample() {
        /*********************************************************************
         * Instantiate Http Client Implementation of Marketplace Web Service *
         * Orders
         *********************************************************************/
        this.service = new MarketplaceWebServiceOrdersClient(
                OrdersConfig.accessKeyId,
                OrdersConfig.secretAccessKey,
                OrdersConfig.applicationName,
                OrdersConfig.applicationVersion,
                OrdersConfig.config);
    }

    /**
     * Fetches all orders created in the given time period and processes them
     * locally. If end is null, it will be ignored (the MWS service will pick an
     * appropriate time, which is now - 2 minutes).
     */
    public void fetchOrders(XMLGregorianCalendar start, XMLGregorianCalendar end,FulfillmentChannelList f1 )throws MarketplaceWebServiceOrdersException, Exception {
        ListOrdersRequest listOrdersRequest = new ListOrdersRequest();
        listOrdersRequest.setSellerId(OrdersConfig.sellerId);
        if (OrdersConfig.marketplaceIdList != null) {
            listOrdersRequest
                    .setMarketplaceId(OrdersConfig.marketplaceIdList);
        }
        listOrdersRequest.setFulfillmentChannel(f1);
        listOrdersRequest.setLastUpdatedAfter(start);
         
        if (start == null) {
            throw new IllegalArgumentException("Start date cannot be null.");
        }
        if (end != null) {
            listOrdersRequest.setLastUpdatedBefore(end);
        }
         if(JUST_YESTERDAY_ORDERS.equals("true")){
            OrderStatusList ol=new OrderStatusList();
             ArrayList<OrderStatusEnum> status=new ArrayList<OrderStatusEnum>();
             status.add(OrderStatusEnum.UNSHIPPED);
             status.add(OrderStatusEnum.PARTIALLY_SHIPPED);
             //I want shipped orders only once in 2 hours
            Date date = new Date();   // given date
            Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
            calendar.setTime(date);   // assigns calendar to given date 
            int hr = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
            if( hr%2==0   ){
                status.add(OrderStatusEnum.SHIPPED);
            }
                 
             
             status.add(OrderStatusEnum.PENDING);
            ol.setStatus(status);
        f1=new FulfillmentChannelList();
        fc1=new ArrayList();
         fc1.add(FulfillmentChannelEnum.MFN);
        f1.setChannel(fc1);
          listOrdersRequest.setFulfillmentChannel(f1);   
            listOrdersRequest.setOrderStatus(ol);

        }
        try {
            ListOrdersResult listOrdersResult = listOrders(listOrdersRequest);

            if (listOrdersResult != null && listOrdersResult.isSetNextToken()) {
                ListOrdersByNextTokenRequest listOrdersByNextTokenRequest = new ListOrdersByNextTokenRequest();
                listOrdersByNextTokenRequest
                        .setSellerId(OrdersConfig.sellerId);
                String nextToken = listOrdersResult.getNextToken();
                ListOrdersByNextTokenResult listOrdersByNextTokenResult = null;
                while (nextToken != null) {
                    listOrdersByNextTokenRequest.setNextToken(nextToken);
                    listOrdersByNextTokenResult = listOrdersByNextToken(listOrdersByNextTokenRequest);
                    nextToken = listOrdersByNextTokenResult.getNextToken();
                }
            }
        } catch (MarketplaceWebServiceOrdersException ex) {
            System.out.println("Caught Exception: " + ex.getMessage());
            System.out.println("Response Status Code: " + ex.getStatusCode());
            System.out.println("Error Code: " + ex.getErrorCode());
            System.out.println("Error Type: " + ex.getErrorType());
            System.out.println("Request ID: " + ex.getRequestId());
            System.out.print("XML: " + ex.getXML());
            throw ex;
        }
      fetchOnlyPendingOrders(  listOrdersRequest );
        
    }
    private void fetchOnlyPendingOrders( ListOrdersRequest listOrdersRequest ) throws Exception{
  //start Now lets delete all pending orders and query those pending orders only that came in last 7 days
        java.sql.Connection con =DBMWS.getConnection("tajplaza");
        PreparedStatement ps=con.prepareStatement("delete from tajplaza.orders where orderstatus='Pending'");
        ps.executeUpdate();;
        con.close();;
        OrderStatusList ol=new OrderStatusList();
        ArrayList<OrderStatusEnum>status=new ArrayList<OrderStatusEnum>();
 
        status.add(OrderStatusEnum.PENDING);
        ol.setStatus(status);
        f1=new FulfillmentChannelList();
        fc1=new ArrayList();
        fc1.add(FulfillmentChannelEnum.MFN);
        f1.setChannel(fc1);
        listOrdersRequest.setFulfillmentChannel(f1);   
        listOrdersRequest.setOrderStatus(ol);
            Date yest_dt = new Date();
          yest_dt.setDate(yest_dt.getDate()-7);
          
          GregorianCalendar cafter = new GregorianCalendar();
          cafter.setTime(yest_dt);
          
          XMLGregorianCalendar dateafter = DatatypeFactory.newInstance().newXMLGregorianCalendar(cafter);
          listOrdersRequest.setLastUpdatedAfter(dateafter);
        
        //end get all pending orders 
        try {
            ListOrdersResult listOrdersResult = listOrders(listOrdersRequest);

            if (listOrdersResult != null && listOrdersResult.isSetNextToken()) {
                ListOrdersByNextTokenRequest listOrdersByNextTokenRequest = new ListOrdersByNextTokenRequest();
                listOrdersByNextTokenRequest
                        .setSellerId(OrdersConfig.sellerId);
                String nextToken = listOrdersResult.getNextToken();
                ListOrdersByNextTokenResult listOrdersByNextTokenResult = null;
                while (nextToken != null) {
                    listOrdersByNextTokenRequest.setNextToken(nextToken);
                    listOrdersByNextTokenResult = listOrdersByNextToken(listOrdersByNextTokenRequest);
                    nextToken = listOrdersByNextTokenResult.getNextToken();
                }
            }
        } catch (MarketplaceWebServiceOrdersException ex) {
            System.out.println("Caught Exception: " + ex.getMessage());
            System.out.println("Response Status Code: " + ex.getStatusCode());
            System.out.println("Error Code: " + ex.getErrorCode());
            System.out.println("Error Type: " + ex.getErrorType());
            System.out.println("Request ID: " + ex.getRequestId());
            System.out.print("XML: " + ex.getXML());
            throw ex;
        }        
    }
    private ListOrdersResult listOrders(ListOrdersRequest request)
            throws MarketplaceWebServiceOrdersException, Exception {
        boolean retry;
        ListOrdersResponse listOrdersResponse = null;
        ListOrdersResult listOrdersResult = null;
        do {
             
            retry = false;
            try {
                listOrdersResponse = service.listOrders(request);

            } catch (MarketplaceWebServiceOrdersException ex) {
                if (ex.getStatusCode() == HttpStatus.SC_SERVICE_UNAVAILABLE
                        && "RequestThrottled".equals(ex.getErrorCode())) {
                    retry = true;
                    requestThrottledExceptionHandler(LIST_ORDERS_THROTTLE_LIMIT);
                } else {
                    throw ex;
                }
            }
            if (listOrdersResponse != null
                    && listOrdersResponse.isSetListOrdersResult()) {
                listOrdersResult = listOrdersResponse.getListOrdersResult();
                if (listOrdersResult.isSetOrders()) {
                    processOrders(listOrdersResult.getOrders());
                }
            }

        } while (retry);
        return listOrdersResult;
    }

    private ListOrdersByNextTokenResult listOrdersByNextToken(
            ListOrdersByNextTokenRequest listOrdersByNextTokenRequest)
            throws MarketplaceWebServiceOrdersException, Exception {
        boolean retry;
        ListOrdersByNextTokenResponse listOrdersByNextTokenResponse = null;
        ListOrdersByNextTokenResult listOrdersByNextTokenResult = null;
        do {
            retry = false;
            try {

                listOrdersByNextTokenResponse = service
                        .listOrdersByNextToken(listOrdersByNextTokenRequest);
            } catch (MarketplaceWebServiceOrdersException ex) {
                if (ex.getStatusCode() == HttpStatus.SC_SERVICE_UNAVAILABLE
                        && "RequestThrottled".equals(ex.getErrorCode())) {
                    retry = true;
                    requestThrottledExceptionHandler(LIST_ORDERS_THROTTLE_LIMIT);
                } else {
                    throw ex;
                }
            }
            if (listOrdersByNextTokenResponse != null
                    && listOrdersByNextTokenResponse
                            .isSetListOrdersByNextTokenResult()) {
                listOrdersByNextTokenResult = listOrdersByNextTokenResponse
                        .getListOrdersByNextTokenResult();
                if (listOrdersByNextTokenResult.isSetOrders()) {
                    processOrders(listOrdersByNextTokenResult.getOrders());
                }
            }

        } while (retry);
        return listOrdersByNextTokenResult;
    }

    public static void requestThrottledExceptionHandler(long throttlingLimit) {
        try {
            System.out.println("Request throttled. Sleeping for " + throttlingLimit
                    + " milliseconds.");
            Thread.sleep(throttlingLimit);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
            return;
        }
    }

    /*
     * TODO: Insert your order processing logic here.
     */
    protected void processOrders(OrderList orders) throws Exception {
        System.out.println("length of orders:"+orders.getOrder().size());
       ArrayList al=(ArrayList) orders.getOrder();
       for (int i=0;i<al.size();i++){
           Order o=(Order) al.get(i);
           if(o.getAmazonOrderId().equals("109-8414898-2003405"))
               System.out.println("Debug");
           System.out.println("Order id: "+o.getAmazonOrderId()+" order purchase:"+o.getPurchaseDate()+" order channel:"+o.getFulfillmentChannel()+"order channel:"+o.getOrderStatus());
          // if(o.getOrderStatus().equals(OrderStatusEnum.CANCELED))
            //   continue;
           
         ListOrderItemsRequest request2 = new ListOrderItemsRequest();
         request2.setSellerId(OrdersConfig.sellerId);
        request2.setAmazonOrderId(o.getAmazonOrderId());
         // @TODO: set request parameters here
       
         ListOrderItemsSample.invokeListOrderItems(service, request2,o);
       }
        
       
    }
    static String JUST_YESTERDAY_ORDERS=null;
    static Date LAST_UPDATED_ORDER_TIME=null;
    public static void main(String... args) {
         JUST_YESTERDAY_ORDERS= System.getProperty("JUST_YESTERDAY_ORDERS", "true");
         String NUMBER_OF_DAYS= System.getProperty("NUMBER_OF_DAYS", "2");
        OrderFetcherSample orderFetcher = new OrderFetcherSample();
        DatatypeFactory df = null;
        try {
            df = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            log.error(e.getMessage(), e);
        }

        /******************************************
         * Uncomment the desired fetchOrders call *
         ******************************************/

        /* Fetch orders for the last 24 hours GMT */
        XMLGregorianCalendar start1 = df
                .newXMLGregorianCalendar(new GregorianCalendar());
        Duration negativeOneDay = df.newDurationDayTime(false, 0, 24, 0, 0);
        start1.add(negativeOneDay);
//        try {
//             orderFetcher.fetchOrders(start1, null);
//        } catch (MarketplaceWebServiceOrdersException e) {
//            log.error(e.getMessage(), e);
//        }

        /*
         * Fetch orders for the last quarter (Oct 1st, 2010 to Dec 31st, 2010
         * PST)
         */
        
         GregorianCalendar cafter=null;
        try{
            if(JUST_YESTERDAY_ORDERS.equals("false")){
                         //Now pending orders for more than 3 days
                      //Now pending orders for more than 3 days
                    ListOrdersRequest listOrdersRequest = new ListOrdersRequest();
                    listOrdersRequest.setSellerId(OrdersConfig.sellerId);
                    if (OrdersConfig.marketplaceIdList != null) {
                        listOrdersRequest
                                .setMarketplaceId(OrdersConfig.marketplaceIdList);
                    }            
                     Date dt = new Date();
                      dt.setYear(dt.getYear()-3);
                    GregorianCalendar cafter0 = new GregorianCalendar();
                    cafter0.setTime(dt);

                     XMLGregorianCalendar dateafter = DatatypeFactory.newInstance().newXMLGregorianCalendar(cafter0);
                     listOrdersRequest.setCreatedAfter(dateafter);
 

                    // String sql="select order_id,purchasedate,OrderStatus from orders o where purchasedate < date_sub(now(), interval 4 day)   and ( upper(OrderStatus) = 'UNSHIPPED' )    union select order_id,purchasedate,OrderStatus from orders o where purchasedate < date_sub(now(), interval 10 day)   and ( upper(OrderStatus) = 'PENDING' ) order by purchasedate desc  ";
                      String sql="  select order_id,purchasedate,OrderStatus from orders o where purchasedate < date_sub(now(), interval 1 day) and purchasedate > date_sub(now(), interval 30 day)  and ( upper(OrderStatus) = 'PENDING' ) order by purchasedate desc  ";
                     System.out.println("Running query :"+sql);
                     DBMWS.CONNECT_TO_PROD_DEBUG=true;
                        java.sql.Connection con =DBMWS.getConnection("tajplaza");
                        PreparedStatement pst = null;
                        ResultSet rs = null;

                         PreparedStatement prest = con.prepareStatement(sql);

                         ResultSet rs0=prest.executeQuery();
                         int rowCount=0;
                         while( rs0.next()){
                             if(rowCount++>20)
                             {
                                 System.out.println("Processing only 20 rows to avoid request throttled too much");
                                 break;
                             }
                             String order_id=rs0.getString(1);
                            System.out.println("Querying ORDER ID:"+order_id);
                               listOrdersRequest.setSellerOrderId(order_id);
                            OrderFetcherSample o=new OrderFetcherSample();
                            ListOrdersResult listOrdersResult = o.listOrders(listOrdersRequest);



                         }
                        con.close();                 
            }
           
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            String sql="select max(last_update_date) from orders ";
            DBMWS.CONNECT_TO_PROD_DEBUG=true;
            java.sql.Connection con = DBMWS.getConnection("tajplaza");
            PreparedStatement pst = null;
            ResultSet rs = null;
             
             PreparedStatement prest = con.prepareStatement(sql);
             
             ResultSet rs0=prest.executeQuery();
             if(rs0.next()){
                    Date dt=new Date();
                    dt.setYear(dt.getYear()-2);
                 if(rs0.getDate(1)!=null){
                     LAST_UPDATED_ORDER_TIME=rs0.getTimestamp(1);
                      dt=rs0.getDate(1);
                 }
                    
                 
                 int iNUMBER_OF_DAYS=2;    
                 if(NUMBER_OF_DAYS!=null &&NUMBER_OF_DAYS.length()>0)
                     iNUMBER_OF_DAYS=Integer.valueOf(NUMBER_OF_DAYS);
                dt.setDate(dt.getDate()-iNUMBER_OF_DAYS);
               //   dt.setMonth(dt.getMonth()-1);
                System.out.println("Querying orders from date :"+dt);
                cafter = new GregorianCalendar();
                cafter.setTime(dt);
    
                 
             }
            con.close();
        }catch (Exception e){
            e.printStackTrace();
        }
         try {
         
        
         
         Date dt = new Date();
          dt.setHours(dt.getHours()-4);
      
      
        if(JUST_YESTERDAY_ORDERS.equals("true")){
            Date yest_dt = new Date();
          yest_dt.setDate(yest_dt.getDate()-4);
          
          cafter = new GregorianCalendar();
          cafter.setTime(yest_dt);
          if(true){
              
              LAST_UPDATED_ORDER_TIME.setHours(LAST_UPDATED_ORDER_TIME.getHours()-4);;
              cafter.setTime(LAST_UPDATED_ORDER_TIME);
              System.out.println("Trying 4 hours before last update time:"+LAST_UPDATED_ORDER_TIME);
          }
          
          
         System.out.println("Querying orders from YESTERDAY date :"+cafter.getTime());
          
        }
         XMLGregorianCalendar dateafter=null,datebeforeNA =null;
        try {
            dateafter = DatatypeFactory.newInstance().newXMLGregorianCalendar(cafter);
          

        
        } catch (DatatypeConfigurationException ex) {
           ex.printStackTrace();
        
        }             
       
        f1=new FulfillmentChannelList();
        fc1=new ArrayList();
        fc1.add(FulfillmentChannelEnum.AFN);
         fc1.add(FulfillmentChannelEnum.MFN);
        f1.setChannel(fc1);
        
       orderFetcher.fetchOrders(dateafter, null,f1);
       
      /*  System.out.println("New logic, fetching MFN orders from past 20 days");
        f1=new FulfillmentChannelList();
        fc1=new ArrayList();
        fc1.add(FulfillmentChannelEnum.MFN);
        f1.setChannel(fc1);
         dt = new Date();
        dt.setDate(dt.getDate()-20);
        cafter = new GregorianCalendar();
        cafter.setTime(dt);
        dateafter = DatatypeFactory.newInstance().newXMLGregorianCalendar(cafter);

        orderFetcher.fetchOrders(dateafter, datebefore,f1);*/
        
   
        
             
        } catch ( Exception e) {            
             e.printStackTrace();
         }
    }

}
