/*******************************************************************************
 * Copyright 2009-2016 Amazon Services. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 *
 * You may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *******************************************************************************
 * Marketplace Web Service Products
 * API Version: 2011-10-01
 * Library Version: 2016-06-01
 * Generated: Mon Jun 13 10:07:47 PDT 2016
 */
package com.tajplaza.products;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;

import com.amazonservices.mws.client.*;
import com.amazonservices.mws.products.*;
import com.amazonservices.mws.products.model.*;
import com.amazonservices.mws.products.samples.MarketplaceWebServiceProductsSampleConfig;


/** Sample call for GetProductCategoriesForASIN. */
public class GetProductCategoriesForASINSample {

    /**
     * Call the service, log response and exceptions.
     *
     * @param client
     * @param request
     *
     * @return The response.
     */
    
    public static GetProductCategoriesForASINResponse invokeGetProductCategoriesForASIN(
            MarketplaceWebServiceProducts client, 
            GetProductCategoriesForASINRequest request)  {
        try {
            // Call the service.
            GetProductCategoriesForASINResponse response = client.getProductCategoriesForASIN(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
          //  System.out.println(responseXml);
            return response;
        } catch (MarketplaceWebServiceProductsException ex) {

                
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }

    /**
     *  Command line entry point.
     */
    public static void main(String[] args) {
        String cat=getAmazonCategory("B009MP0JD2");
        System.out.println("Cat :"+cat);
     
    }
    public static String getAmazonCategory(String asin){
   // Get a client connection.
        // Make sure you've set the variables in MarketplaceWebServiceProductsSampleConfig.
        String category="";
        MarketplaceWebServiceProductsClient client = MarketplaceWebServiceProductsSampleConfig.getClient();

        // Create a request.
        GetProductCategoriesForASINRequest request = new GetProductCategoriesForASINRequest();
         request.setSellerId(MarketplaceWebServiceProductsSampleConfig.sellerId);
        
        String mwsAuthToken = "example";
        request.setMWSAuthToken(mwsAuthToken);
        String marketplaceId = "ATVPDKIKX0DER";
        request.setMarketplaceId(marketplaceId);
       
        request.setASIN(asin);
        // Make the call.
        GetProductCategoriesForASINResponse invokeGetProductCategoriesForASIN = GetProductCategoriesForASINSample.invokeGetProductCategoriesForASIN(client, request);
        GetProductCategoriesForASINResult productCategoriesForASINResult = null;
        try{
            productCategoriesForASINResult=invokeGetProductCategoriesForASIN.getGetProductCategoriesForASINResult();
            Thread.sleep( 5*1000);
        }catch (Exception ex){
                    if(ex.getMessage() !=null && ex.getMessage().equals("Request is throttled")){
                    System.out.println("Request is throttled, Sleeping");
                    try {
                       Thread.sleep( 600000L);
                     } catch (InterruptedException ex1) {
                          Logger.getLogger(GetProductCategoriesForASINSample.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    getAmazonCategory(asin);
                
            }
            
        }
        
        List<Categories> self = productCategoriesForASINResult.getSelf();
        if(self.size()>0)
        {
            
            String cat=self.get(0).getProductCategoryName();
            // System.out.println("cat :"+cat);
             if(cat.equals("Categories")==false && cat.equals("Products")==false && cat.equals("Features")==false)
                category= cat+category;
            Categories parent = self.get(0).getParent();
             String catId=null;
             boolean CategoriesExists=false;
           while(parent!=null){
               String catName=parent.getProductCategoryName();
              catId=parent.getProductCategoryId();
                parent=parent.getParent();
               if(  catName.equals("Categories") ||  catName.equals("Products")||  catName.equals("Features")){
                   CategoriesExists=true;
                   if(catId.equals("16310101")){
                       catName="Grocery & Gourmet Food";
                       catId=null;
                   }else  if(catId.equals("1055398")){
                       catName="Home & Kitchen";
                       catId=null;
                   }else  if(catId.equals("3760911")){
                       catName="Beauty";
                       catId=null;
                   }else  if(catId.equals("16310091")){
                       catName="Industrial and Scientific";
                       catId=null;
                   }else  if(catId.equals("10272111")){
                       catName="Everything Else";
                       catId=null;
                   }else  if(catId.equals("15684181")){
                       catName="Automotive";
                       catId=null;
                   }else  if(catId.equals("3760901")){
                       catName="Health and Household";
                       catId=null;
                   }else  if(catId.equals("1064954")){
                       catName="Office Products";
                       catId=null;
                   }
                   else  if(catId.equals("2619533011")){
                       catName="Pet Supplies";
                       catId=null;
                   }
                   else  if(catId.equals("228013")){
                       catName="Tools and Home Improvement";
                       catId=null;
                   }
                   else  if(catId.equals("172282")){
                       catName="Electronics";
                       catId=null;
                   }
                   else  if(catId.equals("165793011")){
                       catName="Toys and Games";
                       catId=null;
                   }
                   else  if(catId.equals("468642")){
                       catName="Video Games";
                       catId=null;
                   }
                   else  if(catId.equals("2972638011")){
                       catName="Patio, Lawn and Garden";
                       catId=null;
                   } else  if(catId.equals("3375251")){
                       catName="Sports and Outdoors";
                       catId=null;
                   }
                   else  if(catId.equals("165796011")){
                       catName="Baby Store";
                       catId=null;
                   }
                   
                   
                  
                   
                   else{
                       continue;
                   }
                       
               }

               
               category=catName+">"+category;
             //  System.out.println("parent:"+catName);
               
              
           }
           if(CategoriesExists && catId!=null){
               System.out.println("Exception : Looks like unknown category parent exists for asin:"+asin +" the last catID is "+catId);
           }           
        }
        return category;
    }

}
