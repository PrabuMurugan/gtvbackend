/*******************************************************************************
 * Copyright 2009-2016 Amazon Services. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 *
 * You may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *******************************************************************************
 * Marketplace Web Service Products
 * API Version: 2011-10-01
 * Library Version: 2016-06-01
 * Generated: Mon Jun 13 10:07:47 PDT 2016
 */
package com.tajplaza.products;

import java.util.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;

import com.amazonservices.mws.client.*;
import com.amazonservices.mws.products.*;
import com.amazonservices.mws.products.model.*;
import com.amazonservices.mws.products.samples.MarketplaceWebServiceProductsSampleConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import util.DBMWS;


/** Sample call for GetCompetitivePricingForASIN. */
public class GetCompetitivePricingForASINSample {

    /**
     * Call the service, log response and exceptions.
     *
     * @param client
     * @param request
     *
     * @return The response.
     */
    public static GetCompetitivePricingForASINResponse invokeGetCompetitivePricingForASIN(
            MarketplaceWebServiceProducts client, 
            GetCompetitivePricingForASINRequest request) {
        try {
            // Call the service.
            GetCompetitivePricingForASINResponse response = client.getCompetitivePricingForASIN(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MarketplaceWebServiceProductsException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }

    /**
     *  Command line entry point.
     */
    public static void main(String[] args) throws Exception {

            HashMap hm=GetCompetitivePricingForASINSample.getJustBBPriceandRankMWS("B01FN4I9GE");
            System.out.println("HM:"+hm.toString());
              

    }
   public static  boolean I_NEED_NUMBER_OF_FBA_SELLES=false;
    public static HashMap getJustBBPriceandRankMWS(String asin) throws  Exception{
        HashMap hm=new HashMap();
  // Get a client connection.
        // Make sure you've set the variables in MarketplaceWebServiceProductsSampleConfig.
        MarketplaceWebServiceProductsClient client = MarketplaceWebServiceProductsSampleConfig.getClient();

        // Create a request.
        GetCompetitivePricingForASINRequest request = new GetCompetitivePricingForASINRequest();
        String sellerId = MarketplaceWebServiceProductsSampleConfig.sellerId;
        request.setSellerId(sellerId);
        String mwsAuthToken = "example";
        request.setMWSAuthToken(mwsAuthToken);
        String marketplaceId =   "ATVPDKIKX0DER";
        request.setMarketplaceId(marketplaceId);
               
        String itemCondition = "New";
 
        request.setMarketplaceId(marketplaceId);
        ASINListType asinList = new ASINListType();
        ArrayList<String> asinArrayList=new ArrayList();
        asinArrayList.add(asin);
        
        asinList.setASIN(asinArrayList);
        request.setASINList(asinList);
        // Make the call.
         GetCompetitivePricingForASINResponse invokeGetCompetitivePricingForASIN=null;
         try{
            invokeGetCompetitivePricingForASIN= GetCompetitivePricingForASINSample.invokeGetCompetitivePricingForASIN(client, request);
             Thread.sleep( 5*1000);
             }catch (Exception ex){
                if(ex.getMessage()!=null && ex.getMessage().equals("Request is throttled")){
                System.out.println("Request is throttled, Sleeping");
                try {
                   Thread.sleep( 600000L);
                 } catch (InterruptedException ex1) {
                      ex1.printStackTrace();
                }
                getJustBBPriceandRankMWS(asin);

        }
            
        }
        
        List<GetCompetitivePricingForASINResult> competitivePricingForASINResult = invokeGetCompetitivePricingForASIN.getGetCompetitivePricingForASINResult();
        
        for(int i=0;i<competitivePricingForASINResult.size();i++){
            GetCompetitivePricingForASINResult get = competitivePricingForASINResult.get(i);
            Product product = get.getProduct();
           try{
               Float bbPrice=(float)999;
               Boolean nootherfbaseller=true;
               int numberOfSellers=0;
               boolean iSellThis=false;
               float myPrice=(float)0;
               float bbPriceEvenIfMe=(float)999;
               if(product!=null && product.getCompetitivePricing()!=null 
                       && product.getCompetitivePricing().getCompetitivePrices().getCompetitivePrice().size()>0
                       && product.getCompetitivePricing().getCompetitivePrices().getCompetitivePrice().get(0).getBelongsToRequester()==false
                       ){
                    bbPrice= product.getCompetitivePricing().getCompetitivePrices().getCompetitivePrice().get(0).getPrice().getLandedPrice().getAmount().floatValue();
                      
               }
               if(product!=null && product.getCompetitivePricing()!=null 
                       && product.getCompetitivePricing().getCompetitivePrices().getCompetitivePrice().size()>0
                       ){
                    bbPriceEvenIfMe= product.getCompetitivePricing().getCompetitivePrices().getCompetitivePrice().get(0).getPrice().getLandedPrice().getAmount().floatValue();
                   
               }               
               if(product!=null && product.getCompetitivePricing()!=null &&
                       product.getCompetitivePricing().getNumberOfOfferListings()!=null &&
                       product.getCompetitivePricing().getNumberOfOfferListings().getOfferListingCount().size()>0
                       ){
                       numberOfSellers=product.getCompetitivePricing().getNumberOfOfferListings().getOfferListingCount().get(0).getValue();
                       if(product.getCompetitivePricing().getCompetitivePrices().getCompetitivePrice().size()> 0 && product.getCompetitivePricing().getCompetitivePrices().getCompetitivePrice().get(0).getBelongsToRequester()){
                           numberOfSellers=numberOfSellers-1;
                           myPrice=product.getCompetitivePricing().getCompetitivePrices().getCompetitivePrice().get(0).getPrice().getLandedPrice().getAmount().floatValue();
                       }
                   
               }
               String asin1=get.getASIN();
               Integer rank=999999;
               String productCategoryId="";
                       
               try{
                   if(product!=null && product.getSalesRankings() !=null && product.getSalesRankings().getSalesRank()!=null && 
                           product.getSalesRankings().getSalesRank().size()>0){
                           rank=product.getSalesRankings().getSalesRank().get(0).getRank();;
                           productCategoryId=product.getSalesRankings().getSalesRank().get(0).getProductCategoryId();
                   }
                       
               } catch(Exception e2){e2.printStackTrace();}
               if(numberOfSellers>0 && bbPrice==999){
                   HashMap hm2=GetLowestPricedOffersForASINSample.getLowestPricedOffersInternal(asin,myPrice);
                   bbPrice=(Float) hm2.get("lowPrice");
                   hm.put("lowPrice", (Float)hm2.get("lowPrice"));
                   hm.put("lowPriceFulfillmentChannel", (String)hm2.get("lowPriceFulfillmentChannel"));
                   hm.put("NUMBEROFFBASELLERS", (Integer)hm2.get("NUMBEROFFBASELLERS"));
                   
                   
               }else if(I_NEED_NUMBER_OF_FBA_SELLES){
                   HashMap hm2=GetLowestPricedOffersForASINSample.getLowestPricedOffersInternal(asin,myPrice);
                   hm.put("lowPrice", (Float)hm2.get("lowPrice"));
                   hm.put("lowPriceFulfillmentChannel", (String)hm2.get("lowPriceFulfillmentChannel"));
                   hm.put("NUMBEROFFBASELLERS", (Integer)hm2.get("NUMBEROFFBASELLERS"));
               }
               System.out.println("The BB price for asin : "+asin1 +" is "+bbPrice +" and rank is "+ rank);

               hm.put("asin", asin);
               hm.put("bestprice", bbPrice);
               if(bbPriceEvenIfMe<bbPrice)
                hm.put("bbPriceEvenIfMe", bbPriceEvenIfMe);
               else
                   hm.put("bbPriceEvenIfMe", bbPrice);
               hm.put("rank", rank);
               updateAsinBBandRank(asin,bbPrice,rank,productCategoryId);
           }catch (Exception e){e.printStackTrace();}
            
            
        }        
        return hm;
        
    }
    public static void updateAsinBBandRank(String asin,Float bestprice,int rank,String productCategoryId){
       try{
           Connection con=DBMWS.getConnection("tajplaza");
           PreparedStatement ps=con.prepareStatement("update tajplaza.amazon_catalog set bestprice=?,rank=?, productcategoryid=?, ts=CURRENT_TIMESTAMP() where asin=?");
           ps.setFloat(1, bestprice);
           ps.setInt(2, rank);
           ps.setString(3, productCategoryId);
           ps.setString(4, asin);
           ps.executeUpdate();
           con.close();;
       }catch (Exception e2){
           e2.printStackTrace();
       }        
    }
}
