/*******************************************************************************
 * Copyright 2009-2016 Amazon Services. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 *
 * You may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *******************************************************************************
 * Marketplace Web Service Products
 * API Version: 2011-10-01
 * Library Version: 2016-06-01
 * Generated: Mon Jun 13 10:07:47 PDT 2016
 */
package com.tajplaza.products;

import java.util.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;

import com.amazonservices.mws.client.*;
import com.amazonservices.mws.products.*;
import com.amazonservices.mws.products.model.*;
import com.amazonservices.mws.products.samples.MarketplaceWebServiceProductsSampleConfig;


/** Sample call for GetMyFeesEstimate. */
public class GetMyFeesEstimateSample {
    private static List<FeesEstimateRequest> aList=new ArrayList();
    private static FeesEstimateRequest e=new FeesEstimateRequest() ;
    private static PriceToEstimateFees f=new PriceToEstimateFees();
    private static MoneyType lp=new MoneyType();
    private static BigDecimal amt;

    /**
     * Call the service, log response and exceptions.
     *
     * @param client
     * @param request
     *
     * @return The response.
     */
    public static GetMyFeesEstimateResponse invokeGetMyFeesEstimate(
            MarketplaceWebServiceProducts client, 
            GetMyFeesEstimateRequest request) {
        try {
            // Call the service.
            GetMyFeesEstimateResponse response = client.getMyFeesEstimate(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MarketplaceWebServiceProductsException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }

    /**
     *  Command line entry point.
     */
    public static void main(String[] args) throws Exception {
        HashMap hm=GetMyFeesEstimateSample.getFBAFeesMWS("B011LNBJ86","100");
        Float  productInfoWidth=(Float)(hm.get("productInfoWidth")!=null?hm.get("productInfoWidth"):(float)1);
        System.out.println("hm"+hm.toString());
      
    }
    public static HashMap getFBAFeesMWS(String asin,String salePrice) throws Exception{
        HashMap hm=new HashMap();
       // List<HashMap>  arr=ListMatchingProductsSample.getMatchingProducts(asin);
        //if(arr.size()==0)
          //  return hm;
        //hm=arr.get(0);
  // Get a client connection.
        // Make sure you've set the variables in MarketplaceWebServiceProductsSampleConfig.
        if(salePrice==null || salePrice.length() ==0)
            salePrice="100";
        MarketplaceWebServiceProductsClient client = MarketplaceWebServiceProductsSampleConfig.getClient();

        // Create a request.
        GetMyFeesEstimateRequest request = new GetMyFeesEstimateRequest();
         request.setSellerId(MarketplaceWebServiceProductsSampleConfig.sellerId);
        
        String mwsAuthToken = "example";
        request.setMWSAuthToken(mwsAuthToken);
        String marketplaceId = "ATVPDKIKX0DER";
        
        FeesEstimateRequestList feesEstimateRequestList = new FeesEstimateRequestList();
        e.setIdType("ASIN");
        e.setIdValue(asin);
        e.setIsAmazonFulfilled(true);
        e.setMarketplaceId( "ATVPDKIKX0DER");
        e.setIdentifier(String.valueOf(System.currentTimeMillis()));
        amt=new BigDecimal(salePrice);
        
        lp.setAmount(amt);
        lp.setCurrencyCode("USD");
        f.setListingPrice(lp);
        e.setPriceToEstimateFees(f);
        aList.add(e);
        feesEstimateRequestList.setFeesEstimateRequest(aList);
        request.setFeesEstimateRequestList(feesEstimateRequestList);
        // Make the call.
        GetMyFeesEstimateResponse invokeGetMyFeesEstimate = null;
         try{
            invokeGetMyFeesEstimate= GetMyFeesEstimateSample.invokeGetMyFeesEstimate(client, request);
             Thread.sleep(  1000);
             }catch (Exception ex){
                if(ex.getMessage() !=null && ex.getMessage().equals("Request is throttled")){
                System.out.println("Request is throttled, Sleeping");
                try { Thread.sleep( 600000L);} catch (InterruptedException ex1) {ex1.printStackTrace();}
                getFBAFeesMWS(asin,salePrice);

            }
            
        }        
        List<FeesEstimateResult> feesEstimateResult = invokeGetMyFeesEstimate.getGetMyFeesEstimateResult().getFeesEstimateResultList().getFeesEstimateResult();
        if(feesEstimateResult.size()>0 && feesEstimateResult.get(0).getFeesEstimate()!=null){
           List<FeeDetail> feeDetail= feesEstimateResult.get(0).getFeesEstimate().getFeeDetailList().getFeeDetail();
            for(int i=0;i<feeDetail.size();i++){
                FeeDetail get = feeDetail.get(i);
                if(get.getFeeType().equals("FBAFees")){
                    hm.put("fbaFees", get.getFeeAmount().getAmount().floatValue());
                }
                //System.out.println(get.getFeeType() +":"+ get.getFeeAmount().getAmount().floatValue());
            }        
            
        }
        
        return hm;
    }

}
