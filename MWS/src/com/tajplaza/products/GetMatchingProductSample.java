/*******************************************************************************
 * Copyright 2009-2016 Amazon Services. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 *
 * You may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *******************************************************************************
 * Marketplace Web Service Products
 * API Version: 2011-10-01
 * Library Version: 2016-06-01
 * Generated: Mon Jun 13 10:07:47 PDT 2016
 */
package com.tajplaza.products;

import java.util.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;

import com.amazonservices.mws.client.*;
import com.amazonservices.mws.products.*;
import com.amazonservices.mws.products.model.*;
import com.amazonservices.mws.products.samples.MarketplaceWebServiceProductsSampleConfig;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/** Sample call for GetMatchingProduct. */
public class GetMatchingProductSample {

    /**
     * Call the service, log response and exceptions.
     *
     * @param client
     * @param request
     *
     * @return The response.
     */
    public GetMatchingProductSample(){
        arr=new ArrayList();
    }
    public static GetMatchingProductResponse invokeGetMatchingProduct(
            MarketplaceWebServiceProducts client, 
            GetMatchingProductRequest request) {
        try {
            // Call the service.
            GetMatchingProductResponse response = client.getMatchingProduct(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MarketplaceWebServiceProductsException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }

    /**
     *  Command line entry point.
     */
    public static void main(String[] args) throws Exception {

       ArrayList arr=getAllOtherVariationsBySize("B01M02M8W6");
       System.out.println("Return array:"+arr.toString());
    }
    static ArrayList arr=new ArrayList();
    public static boolean FLAVOR_VARIATIONS_SKIP=true;
    public static ArrayList getAllOtherVariationsBySize(String asin) throws Exception{
         // Get a client connection.
        // Make sure you've set the variables in MarketplaceWebServiceProductsSampleConfig.
       
        MarketplaceWebServiceProductsClient client = MarketplaceWebServiceProductsSampleConfig.getClient();

        // Create a request.
        GetMatchingProductRequest request = new GetMatchingProductRequest();
        String sellerId = MarketplaceWebServiceProductsSampleConfig.sellerId;
        request.setSellerId(sellerId);
        String mwsAuthToken = "example";
        request.setMWSAuthToken(mwsAuthToken);
        String marketplaceId =   "ATVPDKIKX0DER";
        request.setMarketplaceId(marketplaceId);
        request.setMarketplaceId(marketplaceId);
        ASINListType asinList = new ASINListType();
        ArrayList<String> asinArrayList=new ArrayList();
        asinArrayList.add(asin);
        asinList.setASIN(asinArrayList);
        
        request.setASINList(asinList);
        
        request.setASINList(asinList);

        // Make the call.
        try{
            GetMatchingProductResponse invokeGetMatchingProduct = GetMatchingProductSample.invokeGetMatchingProduct(client, request);
            List<GetMatchingProductResult> matchingProductResult = invokeGetMatchingProduct.getGetMatchingProductResult();
            if(matchingProductResult!=null){
                for(int i=0;i<matchingProductResult.size();i++){
                    GetMatchingProductResult res = matchingProductResult.get(i);
                    if(res!=null && res.getProduct()!=null && res.getProduct().getRelationships()!=null){
                        for (Object obj : res.getProduct().getRelationships().getAny()) {
                        Node attribute = ((Node) obj);
                        System.out.println("attribute:"+attribute.toString());
                        String nodeName=attribute.getNodeName().replaceAll("ns2:", "");
                        if(nodeName.equals("VariationParent") && attribute.getChildNodes().getLength()>0 && attribute.getChildNodes().item(0).getChildNodes().getLength()>0){
                                NodeList MarketplaceASINs = attribute.getChildNodes().item(0).getChildNodes().item(0).getChildNodes();
                                for(int j=0;j<MarketplaceASINs.getLength();j++)    {
                                    if(MarketplaceASINs.item(j).getNodeName().equals("ASIN")){
                                        String parentAsin=MarketplaceASINs.item(j).getTextContent();
                                         getAllOtherVariationsBySize(parentAsin);
                                        System.out.println("Parent ASIN:"+parentAsin);
                                    }
                                }
                                //System.out.println("Childest child item: "+item.getTextContent());
                        }
                        if(nodeName.equals("VariationChild") && attribute.getChildNodes().getLength()>0 && attribute.getChildNodes().item(0).getChildNodes().getLength()>0){
                                NodeList MarketplaceASINs = attribute.getChildNodes().item(0).getChildNodes().item(0).getChildNodes();
                                String childAsin="";
                                HashMap hm=new HashMap();
                                for(int j=0;j<MarketplaceASINs.getLength();j++)    {

                                    if(MarketplaceASINs.item(j).getNodeName().equals("ASIN")){
                                        childAsin=MarketplaceASINs.item(j).getTextContent();
                                        System.out.println("Child ASIN:"+childAsin);
                                    }
                                }
                                NodeList childNodes = attribute.getChildNodes();
                                for(int j=0;j<childNodes.getLength();j++){
                                    if(FLAVOR_VARIATIONS_SKIP && childNodes.item(j).getNodeName().indexOf("Flavor")>=0){
                                        System.out.println("ASIN Contains flavor variations, not considering.");
                                        arr=new ArrayList();
                                        return arr;
                                    }
                                    if(childNodes.item(j).getNodeName().indexOf("Size")>=0){
                                        String childSize=childNodes.item(j).getTextContent();
                                        hm.put("childAsin", childAsin);
                                        hm.put("childSize", childSize);
                                         
                                        arr.add(hm);
                                    }
                                }
                                //System.out.println("Childest child item: "+item.getTextContent());
                        }                        
                       /* for(int j=0;j<attribute.getChildNodes().getLength();j++){
                            String name=attribute.getChildNodes().item(j).getNodeName().replaceAll("ns2:", "");
                            String content=attribute.getChildNodes().item(j).getTextContent();
                            name=name.replaceAll("ns2:", "");
                            System.out.println("name:"+name);
                    }//end of AttributeSets*/

                    }  // Parsed all attributes for a product     
            /*
                        JSONObject json=JSONObject.fromObject(list.toJSON());
                        if(json.containsKey("VariationParent")){
                            String[] substringsBetween = StringUtils.substringsBetween(list.toJSON(), "<ASIN>", "<\\/ASIN>");
                            if(substringsBetween.length>0){
                                String parentAsin=substringsBetween[0];
                                System.out.println("Parent ASIN:"+parentAsin);
                                getAllOtherVariationsBySize(parentAsin);
                            }
                            
                        }else if(json.containsKey("VariationChild")){
                            String[] substringsBetween = StringUtils.substringsBetween(list.toJSON(), "<ASIN>", "<\\/ASIN>");
                            
                        
                        }*/
                    }
                }
            }
        } catch (Exception ex){
                if(ex.getMessage()!=null && ex.getMessage().equals("Request is throttled")){
                System.out.println("Request is throttled, Sleeping");
                try {
                   Thread.sleep( 600000L);
                 } catch (InterruptedException ex1) {
                      ex1.printStackTrace();
                }
                getAllOtherVariationsBySize(asin);

                }
                else if(ex!=null && ex.getMessage()!=null && ex.getMessage().indexOf("invalid ASIN")>=0){
                    asin="NA";

                }                
                else {
                    ex.printStackTrace();
                }
            }

        return arr;
    }
    

}
