/*******************************************************************************
 * Copyright 2009-2016 Amazon Services. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 *
 * You may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *******************************************************************************
 * Marketplace Web Service Products
 * API Version: 2011-10-01
 * Library Version: 2016-06-01
 * Generated: Mon Jun 13 10:07:47 PDT 2016
 */
package com.tajplaza.products;

import java.util.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;

import com.amazonservices.mws.client.*;
import com.amazonservices.mws.products.*;
import com.amazonservices.mws.products.model.*;
import com.amazonservices.mws.products.samples.MarketplaceWebServiceProductsSampleConfig;


/** Sample call for GetLowestPricedOffersForASIN. */
public class GetLowestPricedOffersForASINSample {

    /**
     * Call the service, log response and exceptions.
     *
     * @param client
     * @param request
     *
     * @return The response.
     */
    public static GetLowestPricedOffersForASINResponse invokeGetLowestPricedOffersForASIN(
            MarketplaceWebServiceProducts client, 
            GetLowestPricedOffersForASINRequest request) {
        try {
            // Call the service.
            GetLowestPricedOffersForASINResponse response = client.getLowestPricedOffersForASIN(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MarketplaceWebServiceProductsException ex) {
                if(ex.getMessage()!=null && ex.getMessage().equals("Request is throttled")){
                System.out.println("Request is throttled, Sleeping");
                try {
                   Thread.sleep( 600000L);
                 } catch (InterruptedException ex1) {
                      ex1.printStackTrace();
                }
                invokeGetLowestPricedOffersForASIN(client,request);

        }            
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }

    /**
     *  Command line entry point.
     */
    public static void main(String[] args) {
        HashMap hm=GetLowestPricedOffersForASINSample.getLowestPricedOffersInternal("B0012BNVE8",0);
        System.out.println("HM:"+hm.toString());
    

    }
    public static HashMap getLowestPricedOffersInternal(String asin,float myPrice){
    // Get a client connection.
        // Make sure you've set the variables in MarketplaceWebServiceProductsSampleConfig.
        HashMap hm=new HashMap();
        Float lowPrice=(float)999;
        int numberoffbasellers =0;
       // boolean lowPriceFullfilledByAmazon=fals
        String lowPriceFulfillmentChannel="";
        try{
             MarketplaceWebServiceProductsClient client = MarketplaceWebServiceProductsSampleConfig.getClient();

            // Create a request.
            GetLowestPricedOffersForASINRequest request = new GetLowestPricedOffersForASINRequest();
            String sellerId = MarketplaceWebServiceProductsSampleConfig.sellerId;
            request.setSellerId(sellerId);
            String mwsAuthToken = "example";
            request.setMWSAuthToken(mwsAuthToken);
            String marketplaceId =   "ATVPDKIKX0DER";
            request.setMarketplaceId(marketplaceId);

            request.setASIN(asin);
            String itemCondition = "New";
            request.setItemCondition(itemCondition);
            // Make the call.
            GetLowestPricedOffersForASINResponse invokeGetLowestPricedOffersForASIN=null;
            try{
                 invokeGetLowestPricedOffersForASIN = GetLowestPricedOffersForASINSample.invokeGetLowestPricedOffersForASIN(client, request);
             }catch (Exception ex){
                if(ex.getMessage()!=null && ex.getMessage().equals("Request is throttled")){
                System.out.println("Request is throttled, Sleeping");
                try {
                   Thread.sleep( 600000L);
                 } catch (InterruptedException ex1) {
                      ex1.printStackTrace();
                }
                getLowestPricedOffersInternal(asin,0);

                }
                else if(ex.getMessage().indexOf("invalid ASIN")>=0){
                    asin="NA";

                }                
                else {
                    ex.printStackTrace();
                }
            }
            //Process Offers to get lowPrice thats not my price
            if(invokeGetLowestPricedOffersForASIN!=null &&
                    invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult()!=null &&
                    invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary()!=null &&
                    invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getLowestPrices() !=null &&
                    invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getLowestPrices().getLowestPrice().size()>0
                    ){
                int lowPriceSize= invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getLowestPrices().getLowestPrice().size();
                for(int i=0;i<lowPriceSize;i++){
                  float listPrice=invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getLowestPrices().getLowestPrice().get(i).getListingPrice().getAmount().floatValue();;
                  float shipPrice=invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getLowestPrices().getLowestPrice().get(i).getShipping().getAmount().floatValue();;
                  String curlowPriceFulfillmentChannel=invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getLowestPrices().getLowestPrice().get(i).getFulfillmentChannel();;
                  float curLowPrice=listPrice+shipPrice;
                  if(curLowPrice!=myPrice && curLowPrice<lowPrice  ){
                     lowPrice=curLowPrice; 
                     lowPriceFulfillmentChannel=curlowPriceFulfillmentChannel;
                  }
                    
                }
                
            }
            //Then go through buy box prices
            /*if(invokeGetLowestPricedOffersForASIN!=null &&
                    invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult()!=null &&
                    invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary()!=null &&
                    invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getBuyBoxPrices() !=null &&
                    invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getBuyBoxPrices().getBuyBoxPrice().size()>0
                    ){
                int lowPriceSize= invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getBuyBoxPrices().getBuyBoxPrice().size();
                for(int i=0;i<lowPriceSize;i++){
                  float listPrice=invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getBuyBoxPrices().getBuyBoxPrice().get(i).getListingPrice().getAmount().floatValue();;
                  float shipPrice=invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getBuyBoxPrices().getBuyBoxPrice().get(i).getShipping().getAmount().floatValue();;
                  float curBBPrice=listPrice+shipPrice;
                  if(curBBPrice==lowPrice){
                      if(lowPriceFulfillmentChannel.toLowerCase().equals("amazon")){
                          hm.put("BB-Fulfilled-By-Amazon", true);
                      }
                     
                  }
                    
                }
                
            }    */        

            if(invokeGetLowestPricedOffersForASIN!=null && invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getLowestPrices()!=null &&  invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getLowestPrices().getLowestPrice()!=null &&
                    invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getLowestPrices().getLowestPrice().size()>0){
                //lowPrice=invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getLowestPrices().getLowestPrice().get(0).getLandedPrice().getAmount().floatValue();
                int offerCount=invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getBuyBoxEligibleOffers().getOfferCount().size();
                for(int i=0;i<offerCount;i++){
                    String fullfillmentChannel=invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getNumberOfOffers().getOfferCount().get(i).getFulfillmentChannel();
                    String condition=invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getNumberOfOffers().getOfferCount().get(i).getCondition();
                    if(fullfillmentChannel!=null && fullfillmentChannel.equals("Amazon") && condition!=null && condition.toLowerCase().equals("new")){
                       numberoffbasellers=invokeGetLowestPricedOffersForASIN.getGetLowestPricedOffersForASINResult().getSummary().getNumberOfOffers().getOfferCount().get(i).getOfferCount();
                    }
                } 
                
            }
            
            
           // System.out.println("invokeGetLowestPricedOffersForASIN:"+invokeGetLowestPricedOffersForASIN.toJSON());        
        }catch (Exception e){e.printStackTrace();}
        hm.put("lowPrice", lowPrice);
        hm.put("lowPriceFulfillmentChannel", lowPriceFulfillmentChannel);
        hm.put("asin", asin);
        hm.put("NUMBEROFFBASELLERS", numberoffbasellers);
        return hm;
    }

}
