/*******************************************************************************
 * Copyright 2009-2016 Amazon Services. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License"); 
 *
 * You may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 * This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 * CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 * specific language governing permissions and limitations under the License.
 *******************************************************************************
 * Marketplace Web Service Products
 * API Version: 2011-10-01
 * Library Version: 2016-06-01
 * Generated: Mon Jun 13 10:07:47 PDT 2016
 */
package com.tajplaza.products;

import com.amazonservices.mws.products.samples.*;
import java.util.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;

import com.amazonservices.mws.client.*;
import com.amazonservices.mws.products.*;
import com.amazonservices.mws.products.model.*;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import util.DBMWS;
 


/** Sample call for ListMatchingProducts. */
public class ListMatchingProductsSample {

    /**
     * Call the service, log response and exceptions.
     *
     * @param client
     * @param request
     *
     * @return The response.
     */
    public static ListMatchingProductsResponse invokeListMatchingProducts(
            MarketplaceWebServiceProducts client, 
            ListMatchingProductsRequest request) {
        try {
            // Call the service.
            ListMatchingProductsResponse response = client.listMatchingProducts(request);
            ResponseHeaderMetadata rhmd = response.getResponseHeaderMetadata();
            // We recommend logging every the request id and timestamp of every call.
            System.out.println("Response:");
            System.out.println("RequestId: "+rhmd.getRequestId());
            System.out.println("Timestamp: "+rhmd.getTimestamp());
            String responseXml = response.toXML();
            System.out.println(responseXml);
            return response;
        } catch (MarketplaceWebServiceProductsException ex) {
            // Exception properties are important for diagnostics.
            System.out.println("Service Exception:");
            ResponseHeaderMetadata rhmd = ex.getResponseHeaderMetadata();
            if(rhmd != null) {
                System.out.println("RequestId: "+rhmd.getRequestId());
                System.out.println("Timestamp: "+rhmd.getTimestamp());
            }
            System.out.println("Message: "+ex.getMessage());
            System.out.println("StatusCode: "+ex.getStatusCode());
            System.out.println("ErrorCode: "+ex.getErrorCode());
            System.out.println("ErrorType: "+ex.getErrorType());
            throw ex;
        }
    }

    /**
     *  Command line entry point.
     */
    public static void main(String[] args) {
        //HashMap hm=ListMatchingProductsSample.getBestRankedProduct("B0013JQNXA");
        ArrayList list=ListMatchingProductsSample.getMatchingProducts("073310263014");
       // System.out.println("Best ranked hm:"+hm.toString());
    }
    public static HashMap getBestRankedProduct(String query ){
       ArrayList<HashMap> products=ListMatchingProductsSample.getMatchingProducts(query);
       HashMap bestRankedhm=new HashMap();         
       int bestRank=9999999;
        for(int i=0;i<products.size();i++){
            HashMap hm=products.get(i);
            if(hm.containsKey("rank") && (Integer)hm.get("rank")>1 && (Integer)hm.get("rank")<bestRank){
                bestRankedhm=hm;
                bestRank=(Integer)hm.get("rank");
            }
                
        }
        return bestRankedhm;
    }
    public static ArrayList<HashMap> getMatchingProducts(String query){
        List<Product> pList=getMatchingProductsMWS(query);
        ArrayList<HashMap> products=new ArrayList();
         
         
        for(int i=0;i<pList.size();i++){
            Product p=pList.get(i);
            HashMap hm=new HashMap();
            int  rank=99999;
            String productCategoryId="";
            try{
                if(p.getSalesRankings().getSalesRank().size()>0){
                    rank=p.getSalesRankings().getSalesRank().get(0).getRank();;
                    productCategoryId=p.getSalesRankings().getSalesRank().get(0).getProductCategoryId();
                    for(int x=1;x<p.getSalesRankings().getSalesRank().size();x++){
                        if((productCategoryId.indexOf("website")<0 || productCategoryId.indexOf("boost")>=0) &&
                            p.getSalesRankings().getSalesRank().get(x).getProductCategoryId().indexOf("website")>0){
                            rank=p.getSalesRankings().getSalesRank().get(x).getRank();;
                            productCategoryId=p.getSalesRankings().getSalesRank().get(x).getProductCategoryId();
                            
                        }
                        
                    }
                    try{
                       /// Connection  con =DBMWS.getConnection("tajplaza");
                       // PreparedStatement ps=con.prepareStatement("select nodepath,query from amazon_categories where nodeid=? ");
                        for(int node=0;node<p.getSalesRankings().getSalesRank().size();node++){
                            if(node>3)
                                break;
                            int curNodeRank=p.getSalesRankings().getSalesRank().get(node).getRank();;
                            String curNodeproductCategoryId=p.getSalesRankings().getSalesRank().get(node).getProductCategoryId();;
                           // ps.setString(1, curNodeproductCategoryId);
                            //ResultSet rs=ps.executeQuery();
                            hm.put("curNodeRank-"+node, curNodeRank);
                            hm.put("curNodeproductCategoryId-"+node, curNodeproductCategoryId);
                           /* if(rs.next()){
                                hm.put("nodepath-"+node, rs.getString(1));
                                hm.put("query-"+node, rs.getString(2));
                                if(hm.containsKey("category")==false)
                                    hm.put("category", rs.getString(1));
                                else  if(hm.containsKey("cat0")==false)
                                    hm.put("cat0", rs.getString(1));
                                else  if(hm.containsKey("cat1")==false)
                                    hm.put("cat1", rs.getString(1));
                                
                            }*/
                            
                        }
                        //con.close();
                    }catch (Exception e2){e2.printStackTrace();}
                }
        
                    
            } catch(Exception e2){e2.printStackTrace();}
             hm.put("rank", rank);
             hm.put("productCategoryId", productCategoryId);
            String asin=p.getIdentifiers().getMarketplaceASIN().getASIN();
            hm.put("asin", asin);
            AttributeSetList attributeSetList = p.getAttributeSets();
            for (Object obj : attributeSetList.getAny()) {
                Node attribute = ((Node) obj);
                for(int j=0;j<attribute.getChildNodes().getLength();j++){
                    String name=attribute.getChildNodes().item(j).getNodeName().replaceAll("ns2:", "");
                    String content=attribute.getChildNodes().item(j).getTextContent();
                    name=name.replaceAll("ns2:", "");
                    if( attribute.getChildNodes().item(j).getChildNodes().getLength()==1){
                        hm.put(name, content);
                    }else{
                        for (int k=0;k< attribute.getChildNodes().item(j).getChildNodes().getLength();k++){
                             Node childNode= attribute.getChildNodes().item(j).getChildNodes().item(k);
                             String childName=childNode.getNodeName().replaceAll("ns2:", "");
                             String childContent=childNode.getTextContent();
                               hm.put(name+":"+childName, childContent);
                        }
                    }
                    if(name.equals("Title")){
                        hm.put("title", content);
                        //Both Title and title will have the same
                    }
                    if(name.equals("Feature")){
                        if(hm.containsKey("bb1")==false)
                            hm.put("bb1", content);
                        else if(hm.containsKey("bb2")==false)
                            hm.put("bb2", content);
                        else if(hm.containsKey("bb3")==false)
                            hm.put("bb3", content);
                        else if(hm.containsKey("bb4")==false)
                            hm.put("bb4", content);
                        else if(hm.containsKey("bb5")==false)
                            hm.put("bb5", content);
                        
                    } else if(name.equals("SmallImage")){
                    NodeList    SmallImageNodes=  attribute.getChildNodes().item(j).getChildNodes();
                        for(int k=0;k<SmallImageNodes.getLength();k++){
                            Node dimension=SmallImageNodes.item(k);
                                if(dimension.getNodeName().replaceAll("ns2:", "").equals("URL")){
                                    String image_url=dimension.getTextContent().replaceAll("_SL75_", "_SL1500_");
                                     hm.put("imageurl", image_url);
                                }

                        }
                    }    else if(name.equals("PackageDimensions")){
                          NodeList    packageDimensions=  attribute.getChildNodes().item(j).getChildNodes();
                        for(int k=0;k<packageDimensions.getLength();k++){
                            Node dimension=packageDimensions.item(k);
                            if(dimension.getNodeName().replaceAll("ns2:", "").equals("Height"))
                             hm.put("productInfoHeight", Float.valueOf(dimension.getTextContent()));
                            if(dimension.getNodeName().replaceAll("ns2:", "").equals("Length"))
                              hm.put("productInfoLength", Float.valueOf(dimension.getTextContent()));
                            if(dimension.getNodeName().replaceAll("ns2:", "").equals("Width"))
                              hm.put("productInfoWidth", Float.valueOf(dimension.getTextContent()));
                            if(dimension.getNodeName().replaceAll("ns2:", "").equals("Weight")){
                                hm.put("productInfoWeight",  Float.valueOf(dimension.getTextContent()));
                                 hm.put("productInfoWeightUnits", dimension.getAttributes().getNamedItem("Units").getNodeValue());

                            }
 
                        }

                    }//end of packageDimensions                    

                }//end of AttributeSets

            }  // Parsed all attributes for a product

           
            if(hm.containsKey("productInfoWeight")==false || hm.get("productInfoWeight").equals("null")){
                hm.put("productInfoWeight", (float)0);
            }
            products.add(hm);                       
        }//end of for for all products
        //Before returning such a way that the best ranked item is on top.
      System.out.println("Returning products:"+products.toString());
      return products;        
    }
    public static  List<Product>   getMatchingProductsMWS(String query){
    // Get a client connection.
        // Make sure you've set the variables in MarketplaceWebServiceProductsSampleConfig.
        MarketplaceWebServiceProductsClient client = MarketplaceWebServiceProductsSampleConfig.getClient();

        // Create a request.
        ListMatchingProductsRequest request = new ListMatchingProductsRequest();
        String sellerId = MarketplaceWebServiceProductsSampleConfig.sellerId;
        request.setSellerId(sellerId);
        String mwsAuthToken = "example";
        request.setMWSAuthToken(mwsAuthToken);
        String marketplaceId =   "ATVPDKIKX0DER";
        request.setMarketplaceId(marketplaceId);
        
        request.setQuery(query);
        String queryContextId = "All";
        request.setQueryContextId(queryContextId);
        // Make the call.
        ListMatchingProductsResponse invokeListMatchingProducts =null;
         try{
            invokeListMatchingProducts=  ListMatchingProductsSample.invokeListMatchingProducts(client, request);
             Thread.sleep(  1000);
             }catch (Exception ex){
                if(ex.getMessage()!=null &&   ex.getMessage().equals("Request is throttled")){
                System.out.println("Request is throttled, Sleeping");
                try { Thread.sleep( 600000L);} catch (InterruptedException ex1) {ex1.printStackTrace();}
                getMatchingProductsMWS(query);

            }
            
        }        
        List<Product> products = new ArrayList();
        if(invokeListMatchingProducts!=null){
            ListMatchingProductsResult listMatchingProductsResult = invokeListMatchingProducts.getListMatchingProductsResult();
            if(listMatchingProductsResult!=null)
               products= listMatchingProductsResult.getProducts().getProduct();
            
        }
              
       return products;       
    }
}
