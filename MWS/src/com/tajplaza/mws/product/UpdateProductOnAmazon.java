
    package com.tajplaza.mws.product;

import com.amazonaws.mws.*;
import com.amazonaws.mws.model.*;
import com.amazonservices.mws.orders.samples.OrdersConfig;
import static com.tajplaza.orders.OrderFetcherSample.LIST_ORDERS_THROTTLE_LIMIT;
import static com.tajplaza.orders.OrderFetcherSample.requestThrottledExceptionHandler;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.codec.binary.Base64; 
import org.apache.http.HttpStatus;
import util.HBLogger;


public class UpdateProductOnAmazon {
    private static final String MODULE = "UpdateProduct";
    private static String SKU = "sku";
    private static String PRICE = "price";
    private static String QUANTITY = "quantity";
    private static String SELLER_SKU = "seller_sku";
    
    private static MarketplaceWebServiceConfig config = null; 
    private static MarketplaceWebService service = null; 
    private static SubmitFeedRequest request = new SubmitFeedRequest();
    private static FileReader updateQtyFile = null;
    private static FileReader priceFile = null;
    private static MessageDigest md = null;
    private static GetFeedSubmissionListRequest getFeedSubmissionListRequest = new GetFeedSubmissionListRequest();
  //  private static  GetFeedSubmissionResultResponse response = null;
    private static boolean retry = false;
    
    public static void main(String [] args) throws InterruptedException, MarketplaceWebServiceException, FileNotFoundException, IOException, Exception {
        HBLogger.debug(MODULE, "Inside main of UpdateProductOnAmazon." + new Date());
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List <AmazonProduct> listProducts = new ArrayList<AmazonProduct>();
        util.DBMWS.CONNECT_TO_PROD_DEBUG=true;
        HBLogger.debug(MODULE, "Using production database.");
        
        HBLogger.debug(MODULE, "Step 1 : Reading database to fetch sku whose quantity and price need to be updated.");
        BufferedReader  bufferedReader = new BufferedReader(new InputStreamReader( new FileInputStream("C:\\Google Drive\\Dropbox\\program\\sql\\web_sql.txt")));
        String line = null;
        while ((line = bufferedReader.readLine()) != null )   {
            if(line.split("\t").length<3) continue;
            String lineNumber = line.split("\t")[0];
            String sqlQuery = line.split("\t")[5];
            
            if(lineNumber.equalsIgnoreCase("59")) {
                
                HBLogger.debug(MODULE, "Found Line Number. lineNumber : " + lineNumber);
                connection = util.DBMWS.getConnection(null);
                HBLogger.debug(MODULE, "Found database connection");
                
                preparedStatement = connection.prepareStatement(sqlQuery.replace("\"", ""));
                resultSet = preparedStatement.executeQuery();
                
                while(resultSet.next()) {
                    String strSKU = resultSet.getString(SKU);
                    String strPrice = resultSet.getString(PRICE);
                    String strQuantity = resultSet.getString(QUANTITY);
                    HBLogger.debug(MODULE, "SKU/PRICE/QUANTITY : " + strSKU + "/" + strPrice + "/" + strQuantity);
                    listProducts.add(new AmazonProduct(strSKU,strPrice,strQuantity));
                }
                resultSet.close();
                preparedStatement.close();
            }
            
            if(lineNumber.equalsIgnoreCase("60")) {
                
                HBLogger.debug(MODULE, "Found Line Number. lineNumber : " + lineNumber);               
                connection = util.DBMWS.getConnection(null);
                HBLogger.debug(MODULE, "Found database connection");

                preparedStatement = connection.prepareStatement(sqlQuery.replace("\"", ""));
                resultSet = preparedStatement.executeQuery();

                while(resultSet.next()) {
                    String strSKU = resultSet.getString(SELLER_SKU);
                    String strPrice = "80";//resultSet.getString(PRICE);
                    String strQuantity = resultSet.getString(QUANTITY);
                    HBLogger.debug(MODULE, "SKU/PRICE/QUANTITY : " + strSKU + "/" + strPrice + "/" + strQuantity);
                    listProducts.add(new AmazonProduct(strSKU,strPrice,strQuantity));
                }
                resultSet.close();
                preparedStatement.close();

            }
            if(connection != null) connection.close();
            
        }
        bufferedReader.close();
        HBLogger.debug(MODULE, "Number of SKUs to be updated : "+ listProducts.size());
        if(listProducts.size() > 0) {
            int iCounter = 0;
            int iPrice=1, iQty=1;
          
            String strPriceReplace="";
            String strQtyReplace="";
            
            for(AmazonProduct product : listProducts) {
                strQtyReplace=strQtyReplace+"\n" +
                "    <Message><MessageID>" + (iQty++) + "</MessageID>\n" +
                "    <OperationType>Update</OperationType>\n" +
                "    <Inventory>\n" +
                "        <SKU>" + (product.getSku()) + "</SKU>\n" +
                "        <Quantity>" + (product.getQuentity()) + "</Quantity>\n" +
                "    </Inventory></Message>";

                                strPriceReplace=strPriceReplace+"\n" +
                "    <Message><MessageID>" + (iPrice++) + "</MessageID>\n" +
                "    <OperationType>Update</OperationType>\n" +
                "    <Price>\n" +
                "        <SKU>" + (product.getSku()) + "</SKU>\n" +
                "        <StandardPrice currency=\"USD\">" + (product.getPrice()) + "</StandardPrice>\n" +
                "    </Price>\n</Message>";
                HBLogger.debug(MODULE, "Processing Product Number  : " + iCounter);
                iCounter++;
                if(((iCounter % 50) == 0) || iCounter == listProducts.size()) {
                    md = MessageDigest.getInstance("MD5");
                    updateQtyFile = new FileReader("C:\\Google Drive\\Dropbox\\program\\UpdateQty.xml");
                    priceFile = new FileReader("C:\\Google Drive\\Dropbox\\program\\Price.xml");
            
                    config = new MarketplaceWebServiceConfig();
                    config.setServiceURL("https://mws.amazonservices.com");
                    service = new MarketplaceWebServiceClient(OrdersConfig.accessKeyId, OrdersConfig.secretAccessKey, OrdersConfig.applicationName, OrdersConfig.applicationVersion,config);
                    request.setMerchant(OrdersConfig.sellerId);

                    List list = new ArrayList();
                    list.add("ATVPDKIKX0DER");
                    IdList idList=new IdList();
                    idList.setId(list);
                    request.setMarketplaceIdList(idList);
                    request.setFeedType("_POST_PRODUCT_DATA_");
        
                    String strUpdateQtyXML = readFile(updateQtyFile);
                      
                    strUpdateQtyXML = strUpdateQtyXML.replace("_MESSAGES_", strQtyReplace);
                    HBLogger.debug("", "File Content After : " + strUpdateQtyXML);
                    byte[] digest = Base64.encodeBase64(md.digest(strUpdateQtyXML.getBytes()));
                      
                    request.setContentMD5(new String(digest,"UTF-8"));
                    request.setFeedContent(new ByteArrayInputStream(strUpdateQtyXML.getBytes()));
                    SubmitFeedResponse response = null;
                    
                    do {
                        try {
                            response= service.submitFeed(request);
                        } catch (MarketplaceWebServiceException ex) {
                            if (ex.getStatusCode() == HttpStatus.SC_SERVICE_UNAVAILABLE
                                    && "RequestThrottled".equals(ex.getErrorCode())) {
                                retry = true;
                                requestThrottledExceptionHandler(LIST_ORDERS_THROTTLE_LIMIT);
                            } else {
                                throw ex;
                            }
                        }
                       
                    } while(retry);
                    
                      
                    String strFeedSubmissionIdQty = null;
                    if (response!= null && response.isSetSubmitFeedResult()) {
                        HBLogger.debug("" , "SubmitFeedResult");
                        SubmitFeedResult submitFeedResult = response.getSubmitFeedResult();

                        if (submitFeedResult.isSetFeedSubmissionInfo()) {
                            HBLogger.debug("", "FeedSubmissionInfo");
                            FeedSubmissionInfo feedSubmissionInfo = submitFeedResult.getFeedSubmissionInfo();
                            if (feedSubmissionInfo.isSetFeedSubmissionId()) {
                                HBLogger.debug("", "FeedSubmissionId : " + feedSubmissionInfo.getFeedSubmissionId());
                                strFeedSubmissionIdQty = feedSubmissionInfo.getFeedSubmissionId();
                            }
                            if (feedSubmissionInfo.isSetFeedType()) {
                                HBLogger.debug("", "FeedType : " + feedSubmissionInfo.getFeedType());
                            }
                            if (feedSubmissionInfo.isSetSubmittedDate()) {
                                HBLogger.debug("", "SubmittedDate : " + feedSubmissionInfo.getSubmittedDate());
                            }
                            if (feedSubmissionInfo.isSetFeedProcessingStatus()) {
                                HBLogger.debug("", "FeedProcessingStatus : " + feedSubmissionInfo.getFeedProcessingStatus());
                            }
                            if (feedSubmissionInfo.isSetStartedProcessingDate()) {
                                HBLogger.debug("", "StartedProcessingDate : " + feedSubmissionInfo.getStartedProcessingDate());
                            }
                            if (feedSubmissionInfo.isSetCompletedProcessingDate()) {
                                HBLogger.debug("", "CompletedProcessingDate : " + feedSubmissionInfo.getCompletedProcessingDate());
                            }
                        }
                    } else {
                        System.out.println("Null Response");
                    }
                    if (response != null && response.isSetResponseMetadata()) {
                        HBLogger.debug("","ResponseMetadata : ");
                        ResponseMetadata responseMetadata = response.getResponseMetadata();
                        if (responseMetadata.isSetRequestId()) {
                            HBLogger.debug("","RequestId : " + responseMetadata.getRequestId());
                        }
                    }else {
                        System.out.println("Null Response");
                    }
                    HBLogger.debug("", "Update Feed("+strFeedSubmissionIdQty +") Submitted Successfully. Waiting till feed processed.");
                      
                    waitTillFeedProcessed(strFeedSubmissionIdQty);
                      
                    HBLogger.debug("","Processing Request to update price.");
                    request.setFeedType("_POST_PRODUCT_PRICING_DATA_");

                    String strUpdatePriceXML = readFile(priceFile);

                    HBLogger.debug("", "File Content Before : " + strUpdatePriceXML);
                    strUpdatePriceXML = strUpdatePriceXML.replace("_MESSAGES_", strPriceReplace);
                    HBLogger.debug("", "File Content After : " + strUpdatePriceXML);

                    digest = Base64.encodeBase64(md.digest(strUpdatePriceXML.getBytes()));
                    HBLogger.debug("", "digest : " + digest);

                    request.setContentMD5(new String(digest,"UTF-8"));
                    request.setFeedContent(new ByteArrayInputStream(strUpdatePriceXML.getBytes()));
                    SubmitFeedResponse priceFeedResponse = service.submitFeed(request);
                      
                    String strFeedSubmissionId = null;
                    if (priceFeedResponse.isSetSubmitFeedResult()) {
                        HBLogger.debug("", "SubmitFeedResult");
                        SubmitFeedResult submitFeedResult = priceFeedResponse.getSubmitFeedResult();

                        if (submitFeedResult.isSetFeedSubmissionInfo()) {
                            HBLogger.debug("", "FeedSubmissionInfo");
                            FeedSubmissionInfo feedSubmissionInfo = submitFeedResult.getFeedSubmissionInfo();
                            if (feedSubmissionInfo.isSetFeedSubmissionId()) {
                                HBLogger.debug("", "FeedSubmissionId : " + feedSubmissionInfo.getFeedSubmissionId());
                                strFeedSubmissionId = feedSubmissionInfo.getFeedSubmissionId();
                            }
                            if (feedSubmissionInfo.isSetFeedType()) {
                                HBLogger.debug("", "FeedType : " + feedSubmissionInfo.getFeedType());
                            }
                            if (feedSubmissionInfo.isSetSubmittedDate()) {
                                HBLogger.debug("", "SubmittedDate : " + feedSubmissionInfo.getSubmittedDate());
                            }
                            if (feedSubmissionInfo.isSetFeedProcessingStatus()) {
                                HBLogger.debug("", "FeedProcessingStatus : " + feedSubmissionInfo.getFeedProcessingStatus());
                            }
                            if (feedSubmissionInfo.isSetStartedProcessingDate()) {
                                HBLogger.debug("", "StartedProcessingDate : " + feedSubmissionInfo.getStartedProcessingDate());
                            }
                            if (feedSubmissionInfo.isSetCompletedProcessingDate()) {
                                HBLogger.debug("", "CompletedProcessingDate : " + feedSubmissionInfo.getCompletedProcessingDate());
                            }
                        }
                    }
                    if (priceFeedResponse.isSetResponseMetadata()) {
                        HBLogger.debug("","ResponseMetadata : ");
                        ResponseMetadata responseMetadata = priceFeedResponse.getResponseMetadata();
                        if (responseMetadata.isSetRequestId()) {
                            HBLogger.debug("","RequestId : " + responseMetadata.getRequestId());
                        }
                    }
                    HBLogger.debug("",""+priceFeedResponse.getResponseHeaderMetadata());
                    HBLogger.debug("", "Update Feed("+strFeedSubmissionId +") Submitted Successfully. Waiting till feed processed.");

                   waitTillFeedProcessed(strFeedSubmissionId);
                   
                   GetFeedSubmissionResultRequest request = new GetFeedSubmissionResultRequest();
                   request.setMerchant(OrdersConfig.sellerId);     
                   request.setFeedSubmissionResultOutputStream(System.out);
                   request.setFeedSubmissionId( strFeedSubmissionIdQty );
                   GetFeedSubmissionResultResponse getFeedSubmissionResultResponse = service.getFeedSubmissionResult(request);
                  
                    System.out.println ("GetFeedSubmissionResult Action Response");
                    System.out.println ("=============================================================================");
                    System.out.println ();

                    System.out.print("    GetFeedSubmissionResultResponse");
                    System.out.println();
                    System.out.print("    GetFeedSubmissionResultResult");
                    System.out.println();
                    System.out.print("            MD5Checksum");
                    System.out.println();
                    System.out.print("                " + getFeedSubmissionResultResponse.getGetFeedSubmissionResultResult().getMD5Checksum());
                    System.out.println();
                    if (response.isSetResponseMetadata()) {
                        System.out.print("        ResponseMetadata");
                        System.out.println();
                        ResponseMetadata  responseMetadata = response.getResponseMetadata();
                        if (responseMetadata.isSetRequestId()) {
                            System.out.print("            RequestId");
                            System.out.println();
                            System.out.print("                " + responseMetadata.getRequestId());
                            System.out.println();
                        }
                    } 
                    System.out.println();

                    System.out.println("Feed Processing Result");
                    System.out.println ("=============================================================================");
                    System.out.println();
                    System.out.println( request.getFeedSubmissionResultOutputStream().toString() );
                    System.out.println(response.getResponseHeaderMetadata());
                    System.out.println();
                    System.out.println();
            
                    request.setFeedSubmissionId( strFeedSubmissionId );
                    getFeedSubmissionResultResponse = service.getFeedSubmissionResult(request);
                  
                    System.out.println ("GetFeedSubmissionResult Action Response");
                    System.out.println ("=============================================================================");
                    System.out.println ();

                    System.out.print("    GetFeedSubmissionResultResponse");
                    System.out.println();
                    System.out.print("    GetFeedSubmissionResultResult");
                    System.out.println();
                    System.out.print("            MD5Checksum");
                    System.out.println();
                    System.out.print("                " + getFeedSubmissionResultResponse.getGetFeedSubmissionResultResult().getMD5Checksum());
                    System.out.println();
                    if (response.isSetResponseMetadata()) {
                        System.out.print("        ResponseMetadata");
                        System.out.println();
                        ResponseMetadata  responseMetadata = response.getResponseMetadata();
                        if (responseMetadata.isSetRequestId()) {
                            System.out.print("            RequestId");
                            System.out.println();
                            System.out.print("                " + responseMetadata.getRequestId());
                            System.out.println();
                        }
                    } 
                    System.out.println();

                    System.out.println("Feed Processing Result");
                    System.out.println ("=============================================================================");
                    System.out.println();
                    System.out.println( request.getFeedSubmissionResultOutputStream().toString() );
                    System.out.println(response.getResponseHeaderMetadata());
                    System.out.println();
                    System.out.println();
                    
                   if(iCounter != listProducts.size()) {
                    HBLogger.debug(MODULE, "50 Records are sent for processing. Sleeping for 10 Minutes");
                    Thread.sleep(1000 * 60 * 10);
                   }
                   
                   iPrice=1; iQty=1;
                   strPriceReplace="";strQtyReplace="";
                }
            }
        }
  
        HBLogger.debug(MODULE, "Leaving main of UpdateProductOnAmazon.");
    }
  
     private static String readFile( FileReader file )  {
        BufferedReader reader = new BufferedReader( file);
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = "\n";
        try {
            while( ( line = reader.readLine() ) != null ) {
                stringBuilder.append( line );
                stringBuilder.append( ls );
            }
        } catch(Exception exception) {
            HBLogger.error("", exception.getMessage(),exception);
        }
        return stringBuilder.toString();
    }
     
    private static void waitTillFeedProcessed(String strFeedSubmissionId)  {
        getFeedSubmissionListRequest.setMerchant( OrdersConfig.sellerId );
            boolean bFreedProcessingDone = true;
            while(bFreedProcessingDone) {
                try {
                    config = new MarketplaceWebServiceConfig();
                    config.setServiceURL("https://mws.amazonservices.com");
                    service = new MarketplaceWebServiceClient(OrdersConfig.accessKeyId, OrdersConfig.secretAccessKey, OrdersConfig.applicationName, OrdersConfig.applicationVersion,config);
                    getFeedSubmissionListRequest.setMerchant(OrdersConfig.sellerId);

                    GetFeedSubmissionListResponse getFeedSubmissionListResponse = service.getFeedSubmissionList(getFeedSubmissionListRequest);

                    if (getFeedSubmissionListResponse.isSetGetFeedSubmissionListResult()) {
                        GetFeedSubmissionListResult  getFeedSubmissionListResult = getFeedSubmissionListResponse.getGetFeedSubmissionListResult();
                        java.util.List<FeedSubmissionInfo> feedSubmissionInfoList = getFeedSubmissionListResult.getFeedSubmissionInfoList();
                        HBLogger.debug("","feedSubmissionInfoList : " + feedSubmissionInfoList);

                        for (FeedSubmissionInfo feedSubmissionInfo : feedSubmissionInfoList) {
                            if (feedSubmissionInfo.isSetFeedSubmissionId() && feedSubmissionInfo.getFeedSubmissionId().equalsIgnoreCase(strFeedSubmissionId) ) {
                                HBLogger.debug("","FeedSubmissionid matched. FeedSubmissionInfo");
                                HBLogger.debug("","FeedSubmissionId : " + feedSubmissionInfo.getFeedSubmissionId());

                                if (feedSubmissionInfo.isSetFeedType()) {
                                    HBLogger.debug("","FeedType : " + feedSubmissionInfo.getFeedType());
                                }
                                if (feedSubmissionInfo.isSetSubmittedDate()) {
                                    HBLogger.debug("","SubmittedDate : " + feedSubmissionInfo.getSubmittedDate());
                                }
                                if (feedSubmissionInfo.isSetFeedProcessingStatus()) {
                                    HBLogger.debug("","FeedProcessingStatus : " + feedSubmissionInfo.getFeedProcessingStatus());
                                }
                                if (feedSubmissionInfo.isSetStartedProcessingDate()) {
                                    HBLogger.debug("","StartedProcessingDate : " + feedSubmissionInfo.getStartedProcessingDate());
                                }
                                if (feedSubmissionInfo.isSetCompletedProcessingDate()) {
                                    HBLogger.debug("","CompletedProcessingDate : " + feedSubmissionInfo.getCompletedProcessingDate());
                                    bFreedProcessingDone = false;
                                }
                                break;
                            } 
                        }
                        if(bFreedProcessingDone) {
                            HBLogger.debug("","Feed is not processed. Sleeping for 4 min");
                            try {
                                Thread.sleep(1000 * 60 * 4);
                            } catch (InterruptedException ex) {
                                HBLogger.error("", ex.getMessage(),ex);
                            }
                            HBLogger.debug("","Out of Sleep");
                        } else {
                            HBLogger.debug("","This feed is proceessed. Existing");
                            break;
                        }
                    } else {
                         HBLogger.debug("","No Feed Response. Existing");
                         break;       
                    }
                } catch(MarketplaceWebServiceException marketplaceWebServiceException){
                    HBLogger.error("", marketplaceWebServiceException.getMessage(),marketplaceWebServiceException);
                    HBLogger.debug("","There is some error. Waiting for 4 minute");
                        try {
                            Thread.sleep(1000 * 60 * 4);
                        } catch (InterruptedException ex) {
                            HBLogger.error("", ex.getMessage(),ex);
                        }
                    HBLogger.debug("","Out of Sleep");
                }
            }
    }
}