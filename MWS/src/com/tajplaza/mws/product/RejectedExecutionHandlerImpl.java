/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tajplaza.mws.product;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 *
 * @author Raj
 */
public class RejectedExecutionHandlerImpl implements RejectedExecutionHandler{
     
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
        System.out.println(r.toString() + " is rejected");
    }
}
