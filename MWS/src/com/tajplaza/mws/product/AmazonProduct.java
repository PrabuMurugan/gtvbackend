/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tajplaza.mws.product;

/**
 *
 * @author Raj
 */
public class AmazonProduct {
    private String sku;
    private String price;
    private String quentity;

    public AmazonProduct(String sku, String price, String quentity) {
        this.sku = sku;
        this.price = price;
        this.quentity = quentity;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuentity() {
        return quentity;
    }

    public void setQuentity(String quentity) {
        this.quentity = quentity;
    }

    @Override
    public String toString() {
        return "AmazonProduct{" + "sku=" + sku + ", price=" + price + ", quentity=" + quentity + '}';
    }
    
}
