/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mail;

/**
 *
 * @author abanipatra
 */
public class OrderItem {

    private long quantity;
    private String title;
    private String image;
    private double price;

    public long getQuantity() {
        return quantity;
    }

    public String getTitle() {
        return title;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
